$(function()
{
    "use strict";

     $("button[name='action'][value='send']").click(function(e)
        {
            $("input[name='vtype[]']").each(function()
            {
                if(!$(this).val())
                {
                    alert("Vehicle Type is required.");
                    e.preventDefault();
                    return false;
                }
            });     

               
        });   

    $("button[name='edit']").click(function()
    {
        if($("#vehicle .active").length)
        {
            $("#pickup-date").val( $("#vehicle .active input[name='pickup-date[]']").val() );
            $("#pickup-time").val( $("#vehicle .active input[name='pickup-time[]']").val() );
            $("#pickup-company").val( $("#vehicle .active input[name='pickup-company[]']").val() );
            $("#pickup-company-other").val( $("#vehicle .active input[name='pickup-company-other[]']").val() );
            $("#pickup-location").val( $("#vehicle .active input[name='pickup-location[]']").val() );
            $("#pickup-location-other").val( $("#vehicle .active input[name='pickup-location-other[]']").val() );
            $("#destin-company").val( $("#vehicle .active input[name='destin-company[]']").val() );
            $("#destin-company-other").val( $("#vehicle .active input[name='destin-company-other[]']").val() );
            $("#destin-location").val( $("#vehicle .active input[name='destin-location[]']").val() );
            $("#destin-location-other").val( $("#vehicle .active input[name='destin-location-other[]']").val() );
            $("#passenger").val( $("#vehicle .active input[name='passenger[]']").val() );
            $("#triptime").val( $("#vehicle .active input[name='triptime[]']").val() );

            $("#vehicletype").val( $("#vehicle .active input[name='vtype[]']").val() );
            $("#plateno").val( $("#vehicle .active input[name='plateid[]']").val() );


            $("#myModalLabel").text("EDIT SERVICE VEHICLE DETAIL");
            $("#service-modal").modal('show');
        }
        else
        {
            alert("Please select specific request to edit.");
        }
    });

   
    $("#save").click(function()
    {
        if( !$("#vehicletype").val() )
        {
            alert("Vehicle Type is required.");
        }
        else if( !$("#plateno").val() )
        {
            alert("Plate No. is required.");
        }
        else
        {
            if( $("#vehicle tbody .active").length )
            {              
                $("#vehicle tbody .active td:nth-child(7)").html( "<input type='hidden' name='vtype[]' value='" + $("#vehicletype").val() + "' />" + $("#vehicletype option:selected").text() );
                $("#vehicle tbody .active td:nth-child(8)").html( "<input type='hidden' name='plateid[]' value='" + $("#plateno").val() + "' /><input type='hidden' name='plateno[]' value='" + $("#plateno option:selected").text() + "' />" + $("#plateno option:selected").text() ); 
            }
            
            $("#service-modal").modal('hide');
        }
    })

    $("#vehicle td").live("click",function()
    {
        $(this).parent().parent().children().removeClass("active");
        $("#vehicle tr td").css("background-color",""); 
        $(this).parent().children().css("background-color","rgb(177, 177, 177)");     
        $(this).parent().addClass("active");
    });

    $("#vehicletype").change(function()
    {
        var type = $(" option:selected" , this).attr("type");
        $("#plateno").val("");
        $("#plateno option").each(function()
        {
            if(($(this).attr("type") == type) || !$(this).attr("type") )
                $(this).removeClass("hide");
            else
                $(this).addClass("hide");

        })
    });
    
});    