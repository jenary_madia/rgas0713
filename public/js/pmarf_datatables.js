$(document).ready(function(){
	var base_url = $("base").attr("href");
	if ( $("#my_pmarf_requests").length != 0 ) {
        var requests_my_table = $('#my_pmarf_requests').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/my-pmarf-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});  

		requests_my_table.fnSort( [[0,"desc"]]); 
    }


	if ( $("#imm_sup_requests").length != 0 ) {
        var requests_is_table = $('#imm_sup_requests').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/for-approval-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		}); 

		requests_is_table.fnSort( [[0,"desc"]]);   
    }
	if ( $("#approver_closed_requests").length != 0 ) {
        var closed_requests_table = $('#approver_closed_requests').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/approver-closed-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});   

		closed_requests_table.fnSort( [[0,"desc"]]); 
    }
	
	if ( $("#approver_closed_requests_dh").length != 0 ) {
        var closed_requests_table = $('#approver_closed_requests_dh').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/approver-closed-requests-dh",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});

		closed_requests_table.fnSort( [[0,"desc"]]);    
    }
	if ( $("#head_requests").length != 0 ) {
        var requests_is_table = $('#head_requests').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/for-head-approval-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});  

		requests_is_table.fnSort( [[0,"desc"]]);  
    }
	if ( $("#npmd_head_requests").length != 0 ) 
	{
        var requests_npmdhead_table = $('#npmd_head_requests').dataTable( {
			//
			"fnInitComplete": function (oSettings, json) {
				
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						console.log('dept selected');
						requests_npmdhead_table.fnFilter( "" );
						requests_npmdhead_table.fnFilter( $(this).val(), 5 );
						
					});
				label.appendTo($("#npmd_head_requests_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/pmarf/departments/for_npmd", function(departments){
					$.each(departments, function(a, b){
						// 1
						select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						// select.append( '<option value="'+b.pmarf_department+'">'+b.pmarf_department+'</option>' );
					});
				});
				
				$('#npmd_head_requests_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						console.log('yay');
						$(select).val('');
					}
					/*
					else{
						$(select).val($(this).val().toUpperCase());
						console.log('yey');
					}
					*/
				});
				
			},
			// "sDom": '<"top"<"#endorsements_new_list_length.dataTables_length" and >f>rt<"bottom"i><"clear">',
			"sDom": '<"top"<"#npmd_head_requests_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			//
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/npmd-head-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});   

		requests_npmdhead_table.fnSort( [[0,"desc"]]); 
    }



    if ( $("#npmd_head_requests_limit").length != 0 ) {
        var requests_npmdhead_table = $('#npmd_head_requests_limit').dataTable( {
			
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sAjaxSource": base_url + "/pmarf/npmd-head-requests-limit",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false     
		});  

		requests_npmdhead_table.fnSort( [[0,"desc"]]); 
    }
	
	if ( $("#all_pmarf_requests").length != 0 ) {
        var all_pmarf_requests_table = $('#all_pmarf_requests').dataTable( {
			//
			"fnInitComplete": function (oSettings, json) {
				
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						console.log('dept selected');
						all_pmarf_requests_table.fnFilter( "" );
						all_pmarf_requests_table.fnFilter( $(this).val(), 5 );
						
					});
				label.appendTo($("#all_pmarf_requests_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/pmarf/departments/for_npmd", function(departments){
					$.each(departments, function(a, b){
						// 1
						select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						// select.append( '<option value="'+b.pmarf_department+'">'+b.pmarf_department+'</option>' );
					});
				});
				
				$('#all_pmarf_requests_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						console.log('yay');
						$(select).val('');
					}
					/*
					else{
						$(select).val($(this).val().toUpperCase());
						console.log('yey');
					}
					*/
				});
				
			},
			// "sDom": '<"top"<"#endorsements_new_list_length.dataTables_length" and >f>rt<"bottom"i><"clear">',
			"sDom": '<"top"<"#all_pmarf_requests_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			//
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/all-pmarf-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});   

		all_pmarf_requests_table.fnSort( [[0,"desc"]]);
    }
	
	if ( $("#npmd_personnel_requests").length != 0 ) 
	{
        var requests_npmdper_table = $('#npmd_personnel_requests').dataTable( {
			//
			"fnInitComplete": function (oSettings, json) {
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						// requests_npmdper_table.fnFilter( $(this).val() );
						requests_npmdper_table.fnFilter( "" );
						requests_npmdper_table.fnFilter( $(this).val(), 5 );
					});
				label.appendTo($("#npmd_personnel_requests_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/pmarf/departments/for_npmd_per", function(departments){
					$.each(departments, function(a, b){
						// 1
						select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						// select.append( '<option value="'+b.pmarf_department+'">'+b.pmarf_department+'</option>' );
					});
				});

				$('#npmd_personnel_requests_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						$(select).val('');
					}
					else{
						$(select).val($(this).val().toUpperCase());
					}
				});
			},
			// "sDom": '<"top"<"#endorsements_new_list_length.dataTables_length" and >f>rt<"bottom"i><"clear">',
			"sDom": '<"top"<"#npmd_personnel_requests_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			//
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/npmd-personnel-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		}); 

		requests_npmdper_table.fnSort( [[0,"desc"]]);   
    }



    if ( $("#npmd_personnel_requests_limit").length != 0 ) {
        var requests_npmdper_table = $('#npmd_personnel_requests_limit').dataTable( {
			
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/npmd-personnel-requests-limit",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false     
		});   

		requests_npmdper_table.fnSort( [[0,"desc"]]); 
    }
	
	
	
	
	
	
	// if ( $("#npmd_personnel_requests").length != 0 ) {
        // var requests_npmdhead_table = $('#npmd_personnel_requests').dataTable( {
			// "sPaginationType": "full_numbers",
			// "sAjaxSource": base_url + "/pmarf/npmd-personnel-requests",
			// "oLanguage": {
				// "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			// },
			// "aLengthMenu": [
				// [10, 20, 50, 100, 200, -1],
				// [10, 20, 50, 100, 200, "All"]
			// ], 
			// "aoColumns": [
				// { "sClass": "td_center", "bSortable": true },
				// { "sClass": "td_center", "bSortable": true },
				// { "sClass": "td_center", "bSortable": true },
				// { "sClass": "td_center", "bSortable": true },
				// { "sClass": "td_center", "bSortable": false },			
				// { "sClass": "td_center", "bSortable": false },			
			// ]
		// });   
    // }
	if ( $("#pmarf_filer").length != 0 ) {
        var pmarf_filer_table = $('#pmarf_filer').dataTable( {
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/pmarf-filer-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false     
		});   

		pmarf_filer_table.fnSort( [[0,"desc"]]); 
    }
	
	if ( $("#pmarf_approver").length != 0 ) {
        var pmarf_filer_table = $('#pmarf_approver').dataTable( {
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/pmarf/pmarf-approver-requests",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
			],'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false     
		});   

		pmarf_filer_table.fnSort( [[0,"desc"]]); 
    }
	
});