$(document).ready(function()
{
    "use strict";

	var base_url = $("base").attr("href");

	var requests_my_table = $('#myrecord').dataTable( {		
			  "sPaginationType": "full_numbers",
			  "bProcessing": true,
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
			 },
			 "aoColumns": [
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },     
              { "sClass": "break", "bSortable": true }, 
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },
              { "sClass": "break", "bSortable": true },     
              { "sClass": "break", "bSortable": true }     
            ]
		});  
               
        $('#module').multiselect({
        includeSelectAllOption: true,
        enableFiltering:true,
        enableCaseInsensitiveFiltering:true ,
        nonSelectedText : "All selected",
        numberDisplayed:0
        });
              
       $(".datepicker").datepicker();         
       
       $(".employee").chosen({width:"100%"});
       
       $.ajax({
             headers: {  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            'type': 'POST',
            'url': base_url + "/audit/report?from=" + $("#date_from").val()
                            + "&to=" + $("#date_to").val() + "&module=" + $("#module").val() + "&employee=" + $("#employee").val(),
            "dataType": "json",
            'success': function (data) 
            {
                requests_my_table.fnClearTable();
                for (var j = 0; j < data.length; j++) 
                {
                  requests_my_table.fnAddData([data[j][0],data[j][1],data[j][2],data[j][3],data[j][4],data[j][5],data[j][6],data[j][7],data[j][8],data[j][9]]);                 
                }
                requests_my_table.fnDraw();
            }
          }); 

       var x=0;
       $("#search").click(function()
       {       
       	 $(".dataTables_processing").css("visibility","");
       	 if(!$("#date_from").val() || !$("#date_to").val())
	       	 {
	       	 	alert("From and To field is required.");
	       	 	$(".dataTables_processing").css("visibility","hidden");
	       	 	return false;
	       	 }
	       	 try
		    {
		       $.datepicker.parseDate( "mm/dd/yy",  $("#date_from").val() );
		       $.datepicker.parseDate( "mm/dd/yy",  $("#date_to").val() );
		    }
		    catch(e)
		    {
		      alert("Invalid inputted date");
		      $(".dataTables_processing").css("visibility","hidden");
		      return false;
		    }

	         $.ajax({
	             headers: {  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	            'type': 'POST',
	            'url': base_url + "/audit/report?from=" + $("#date_from").val()
	                            + "&to=" + $("#date_to").val() + "&module=" + $("#module").val() + "&employee=" + $("#employee").val(),
	            "dataType": "json",
	            'success': function (data) 
	            {
	            	$(".dataTables_processing").css("visibility","hidden");
	                requests_my_table.fnClearTable();
	                for (var j = 0; j < data.length; j++) 
	                {
	                  requests_my_table.fnAddData([data[j][0],data[j][1],data[j][2],data[j][3],data[j][4],data[j][5],data[j][6],data[j][7],data[j][8],data[j][9]]);                   
	                }
	                requests_my_table.fnDraw();
	            }
	          }); 

       });
       
       $("#generate").click(function()
       {
       		if(!$("#date_from").val() || !$("#date_to").val())
	       	 {
	       	 	alert("From and To field is required.");
	       	 	return false;
	       	 }
	       	 try
		    {
		       $.datepicker.parseDate( "mm/dd/yy",  $("#date_from").val() );
		       $.datepicker.parseDate( "mm/dd/yy",  $("#date_to").val() );
		    }
		    catch(e)
		    {
		      alert("Invalid inputted date");
		      return false;
		    }

 
        window.location  = base_url + "/audit/export?from=" + $("#date_from").val()
                            + "&to=" + $("#date_to").val() + "&module=" + $("#module").val() + "&employee=" + $("#employee").val();


           // $.ajax({
           //       headers: {  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
           //      'type': 'GET',
           //      'url': base_url + "/audit/export?from=" + $("#date_from").val()
           //                  + "&to=" + $("#date_to").val() + "&module=" + $("#module").val() + "&employee=" + $("#employee").val(),
           //      "dataType": "json",
           //      'success': function (data) {
           //          window.location = base_url + data.id;
           //      }
           //    });
       });
      
       
});