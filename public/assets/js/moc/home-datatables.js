var base_url = $("base").attr("href");
$(document).ready(function(){
    if ( $("#myMOC").length != 0 ) {
        var requests_my_table = $('#myMOC').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/moc/get_my_moc/5",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }
 

    if ( $("#waitingForApprovalMOC").length != 0 ) {
        var waitingForApprovalMOC = $('#waitingForApprovalMOC').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            //"sAjaxSource": base_url + "/moc/get_my_moc/5",
            "sAjaxSource": base_url + "/moc/get-waiting-for-approval",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    $('body').on('submit','#formMyMOC',function (e) {
        var r = confirm("Are you sure you want to delete this MOC?");
        if (r == true) {
            $("#formMyMOC").submit;
        }else{
            e.preventDefault();
        }
    });

});


var hello = 'asdasd';
if ( $("#forAssessmentMOC").length != 0 ) {
    var forAssessmentMOC = $('#forAssessmentMOC').dataTable( {
        "bDestroy": true,
        "sPaginationType": "full_numbers",
        "sAjaxSource": base_url + "/moc/get_assessment_moc",
        "aaSorting" : [[0, 'desc']],
        "oLanguage": {
            "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
            "sSearch" : "Search : "
        },
        // fnFilter($(this).val())
        "fnDrawCallback": function( oSettings ) {
            if(! $('.generate').length) {
                $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default generate pull-left" id="btnGenerate" style="margin-right: 10px;" id="approveSelected" name="action" value="">GENERATE REPORT</button>');
            }

            if(! $('.export').length) {
                $('div#forAssessmentMOC_filter').append(' <button class="btn btndefault btn-default export pull-left" style="margin-right: 10px;" id="approveSelected" name="action" value="export">EXPORT TABLE TO</button>');
            }
        },
        "aLengthMenu": [
            [10, 20, 50, 100, -1],
            [10, 20, 50, 100, "All"]
        ],
        "aoColumns": [
            { "sWidth": "17%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "30%",'bSortable': false},
            { "sWidth": "30%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false},
            { "sWidth": "15%",'bSortable': false}
        ],

    });

}


