/**
 * Created by user on 12/6/2016.
 */
var base_url = $("base").attr("href");
new Vue({
    el : "#moc_edit",
    filters: {
        uppercase : function (value) {
            if (!value) return '';
            value = value.toString();
            return value.toUpperCase();
        }
    },
    data : {
        formDetails : {
            company : "",
            contactNumber : "",
            dateFiled : "",
            department : "",
            employeeName : "",
            employeeNumber : "",
            referenceNo : "",
            section : "",
            status : ""
        },
        mocDetails : {
            requestTypes : [],
            urgency : "",
            changeIn : [],
            changeInOthersInput : "",
            changeType : "",
            targetDate : "",
            departmentsInvolved : [],
            title : "",
            backgroundInfo : "",
            ssmdActionPlan : "",
            csmdActionPlan : ""
        },
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        status : true,
        attachments : [],
        attachmentsOld : [],
        SSMDattachments : [],
        SSMDattachmentsOld : [],
        CSMDattachments : [],
        CSMDattachmentsOld : [],
        comment : "",
        messages : "",
        assessmentType : "",
        moc_bsa : "",

        /*******FOR REQUEST TABLE DISABLING*********/

        enabledForRequest : false,

        /*****FOR PARSING DEPARTMENT INVOLVED****/
        departments : [],
        selectedDepartment : {
            id : "",
            data : {}
        },

        /*****DYNAMIC RETURN****/
        returnTo : ""

    },
    methods : {
        cancelMOC : function () {
            var that = this;
            var cancelParams = {
                action : "cancel",
                id : mocID,
                comment : this.comment,
                successMessage : "Successfully cancelled",
            };
            Vue.http.post(base_url+'/moc/cancel_moc',cancelParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        returnMOC : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return_to_filer',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        saveMOC : function () {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });

            $(".SSMDattachmentData").each(function() {
                that.SSMDattachments.push(JSON.parse($(this).val()));
            });
            var saveParams = {
                id : mocID,
                CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                CSMDActionPlan : that.mocDetails.csmdActionPlan,
                SSMDattachments : that.SSMDattachments.concat(that.SSMDattachmentsOld),
                SSMDActionPlan : that.mocDetails.ssmdActionPlan
            };
            Vue.http.post(base_url+'/moc/save-by-approver',saveParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        acknowledgeMOC : function () {
            var that = this;
            var sendParams = {
                id: mocID,
                comment: this.comment
            };
            Vue.http.post(base_url + '/moc/acknowledge_moc', sendParams, {
                headers: {
                    "Cache-Control": "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }
            }).then(function (response) {
                    that.$set("submitStatus.submitted", true);
                    that.$set("submitStatus.success", response.data['success']);
                    that.$set("submitStatus.message", response.data['message']);
                    that.$set("submitStatus.errors", response.data['errors']);
                    window.scrollTo(0, 0);
                    if (response.data["success"] == true) {
                        setTimeout(function () {
                            window.location = response.data["url"]
                        }, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });

        },
        
        returnToBSA : function () {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });

            $(".SSMDattachmentData").each(function() {
                that.SSMDattachments.push(JSON.parse($(this).val()));
            });
            var returnParams = {
                id : mocID,
                CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                CSMDActionPlan : that.mocDetails.csmdActionPlan,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return_to_bsa',returnParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        
        returnAction : function () {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });

            $(".SSMDattachmentData").each(function() {
                that.SSMDattachments.push(JSON.parse($(this).val()));
            });
            var returnParams = {
                id : mocID,
                returnTo : that.returnTo,
                CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                CSMDActionPlan : that.mocDetails.csmdActionPlan,
                SSMDattachments : that.SSMDattachments.concat(that.SSMDattachmentsOld),
                SSMDActionPlan : that.mocDetails.ssmdActionPlan,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return-to-other-approver',returnParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        returnSat : function () {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });
            var returnParams = {
                id : mocID,
                returnTo : that.returnTo,
                CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                CSMDActionPlan : that.mocDetails.csmdActionPlan,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/return-to-other-approver-sat',returnParams).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        //general routing
            sendMOCtoMD : function () {
            var that = this;
            var sendParams = {
                id : mocID,
                action : 'sendToMD',
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/send_to_md',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
            sendDHtoADH : function () {
                var that = this;
                $(".CSMDattachmentData").each(function() {
                    that.CSMDattachments.push(JSON.parse($(this).val()));
                });
                var sendParams = {
                    id : mocID,
                    assessmentType : that.assessmentType,
                    comment : this.comment,
                    CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                    CSMDActionPlan : that.mocDetails.csmdActionPlan
                };

                Vue.http.post(base_url+'/moc/head_to_adh',sendParams,{ headers: {
                    "Cache-Control" : "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }} ).then(function (response) {
                        that.$set("submitStatus.submitted",true);
                        that.$set("submitStatus.success",response.data['success']);
                        that.$set("submitStatus.message",response.data['message']);
                        that.$set("submitStatus.errors",response.data['errors']);
                        window.scrollTo(0,0);
                        if(response.data["success"] == true) {
                            setTimeout(function(){window.location = response.data["url"]}, 1000)
                        }else{
                            that.$set('CSMDattachments',[]);
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });
            },
            sendToFilerAck : function () {
                var that = this;
                var sendParams = {
                    id : mocID,
                    comment : this.comment
                };
                Vue.http.post(base_url+'/moc/send_to_filer',sendParams,{ headers: {
                    "Cache-Control" : "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }} ).then(function (response) {
                        that.$set("submitStatus.submitted",true);
                        that.$set("submitStatus.success",response.data['success']);
                        that.$set("submitStatus.message",response.data['message']);
                        that.$set("submitStatus.errors",response.data['errors']);
                        window.scrollTo(0,0);
                        if(response.data["success"] == true) {
                            setTimeout(function(){window.location = response.data["url"]}, 1000)
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });
            },
            sendADHtoDH : function (assessType) {
                var that = this;
                $(".CSMDattachmentData").each(function() {
                    that.CSMDattachments.push(JSON.parse($(this).val()));
                });
                var sendParams = {
                    id : mocID,
                    assessmentType : assessType,
                    comment : this.comment,
                    CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                    CSMDActionPlan : that.mocDetails.csmdActionPlan
                };
                Vue.http.post(base_url+'/moc/adh_to_head',sendParams,{ headers: {
                    "Cache-Control" : "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }} ).then(function (response) {
                        that.$set("submitStatus.submitted",true);
                        that.$set("submitStatus.success",response.data['success']);
                        that.$set("submitStatus.message",response.data['message']);
                        that.$set("submitStatus.errors",response.data['errors']);
                        window.scrollTo(0,0);
                        if(response.data["success"] == true) {
                            setTimeout(function(){window.location = response.data["url"]}, 1000)
                        }else{
                            that.$set('CSMDattachments',[]);
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });
            },
            sendADHtoBSA : function () {
                var that = this;
                $(".CSMDattachmentData").each(function() {
                    that.CSMDattachments.push(JSON.parse($(this).val()));
                });
                var sendParams = {
                    id : mocID,
                    assignedBSA : this.moc_bsa,
                    comment : this.comment,
                    CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                    CSMDActionPlan : that.mocDetails.csmdActionPlan
                };
                Vue.http.post(base_url+'/moc/adh_to_bsa',sendParams,{ headers: {
                    "Cache-Control" : "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }} ).then(function (response) {
                        that.$set("submitStatus.submitted",true);
                        that.$set("submitStatus.success",response.data['success']);
                        that.$set("submitStatus.message",response.data['message']);
                        that.$set("submitStatus.errors",response.data['errors']);
                        window.scrollTo(0,0);
                        if(response.data["success"] == true) {
                            setTimeout(function(){window.location = response.data["url"]}, 1000)
                        }else{
                            that.$set('CSMDattachments',[]);
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });
        },
            sendBSAtoADH : function () {
                var that = this;
                $(".CSMDattachmentData").each(function() {
                    that.CSMDattachments.push(JSON.parse($(this).val()));
                });
                var sendParams = {
                    id : mocID,
                    comment : this.comment,
                    CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                    CSMDActionPlan : that.mocDetails.csmdActionPlan
                };
                Vue.http.post(base_url+'/moc/send_bsa_to_adh',sendParams,{ headers: {
                    "Cache-Control" : "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }} ).then(function (response) {
                        that.$set("submitStatus.submitted",true);
                        that.$set("submitStatus.success",response.data['success']);
                        that.$set("submitStatus.message",response.data['message']);
                        that.$set("submitStatus.errors",response.data['errors']);
                        window.scrollTo(0,0);
                        if(response.data["success"] == true) {
                            setTimeout(function(){window.location = response.data["url"]}, 1000)
                        }else{
                            that.$set('CSMDattachments',[]);
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });

            },
            returntoBSA : function () {
            var that = this;
            var sendParams = {
                id: mocID,
                assignedBSA: this.moc_bsa,
                comment: this.comment
            };
            Vue.http.post(base_url + '/moc/adh_to_bsa', sendParams, {
                headers: {
                    "Cache-Control": "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }
            }).then(function (response) {
                    that.$set("submitStatus.submitted", true);
                    that.$set("submitStatus.success", response.data['success']);
                    that.$set("submitStatus.message", response.data['message']);
                    that.$set("submitStatus.errors", response.data['errors']);
                    window.scrollTo(0, 0);
                    if (response.data["success"] == true) {
                        setTimeout(function () {
                            window.location = response.data["url"]
                        }, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });

        },
            sendtoDivHead : function () {
                var that = this;
                $(".CSMDattachmentData").each(function() {
                    that.CSMDattachments.push(JSON.parse($(this).val()));
                });
                var sendParams = {
                    id : mocID,
                    comment : this.comment,
                    CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                    CSMDActionPlan : that.mocDetails.csmdActionPlan
                };
                Vue.http.post(base_url + '/moc/send_to_div_head', sendParams, {
                    headers: {
                        "Cache-Control": "no-cache, no-store, must-revalidate",
                        "Pragma": "no-cache",
                        "Expires": "0"
                    }
                }).then(function (response) {
                        that.$set("submitStatus.submitted", true);
                        that.$set("submitStatus.success", response.data['success']);
                        that.$set("submitStatus.message", response.data['message']);
                        that.$set("submitStatus.errors", response.data['errors']);
                        window.scrollTo(0, 0);
                        if (response.data["success"] == true) {
                            setTimeout(function () {
                                window.location = response.data["url"]
                            }, 1000)
                        }else{
                            that.$set('CSMDattachments',[]);
                        }
                    },
                    function () {
                        alert("Something went wrong");
                    });

        },

        //Satellite Routing
        sendMOCtoAOM :function () {
            var that = this;
            $(".SSMDattachmentData").each(function() {
                that.SSMDattachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                action : 'sendToAOM',
                comment : this.comment,
                SSMDattachments : that.SSMDattachments.concat(that.SSMDattachmentsOld),
                SSMDActionPlan : that.mocDetails.ssmdActionPlan
            };
            Vue.http.post(base_url+'/moc/send_to_aom',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('SSMDattachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        sendAOMToMD :function () {
            var that = this;
            $(".SSMDattachmentData").each(function() {
                that.SSMDattachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                comment : this.comment,
                SSMDattachments : that.SSMDattachments.concat(that.SSMDattachmentsOld),
                SSMDActionPlan : that.mocDetails.ssmdActionPlan
            };
            Vue.http.post(base_url+'/moc/send_aom_to_csmd',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        sendSatDHtoADH : function () {
            var that = this;
            $(".CSMDattachmentData").each(function() {
                that.CSMDattachments.push(JSON.parse($(this).val()));
            });
            var sendParams = {
                id : mocID,
                assessmentType : 2,
                comment : this.comment,
                CSMDattachments : that.CSMDattachments.concat(that.CSMDattachmentsOld),
                CSMDActionPlan : that.mocDetails.csmdActionPlan
            };
            Vue.http.post(base_url+'/moc/head_to_adh',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        
        sendDivHeadtoSSMD : function () {
            var that = this;
            var sendParams = {
                id : mocID,
                comment : this.comment
            };
            Vue.http.post(base_url+'/moc/send_divhead_to_ssmd',sendParams,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },

        removeAttachment : function (type,index) {
            if(type == 0) {
                this.attachmentsOld.splice(index,1);
            }else if(type == 1){
                this.SSMDattachmentsOld.splice(index,1);
            }else if(type == 2){
                this.CSMDattachmentsOld.splice(index,1);
            }
        },


    },
    computed : {

    },
    watch : {
        'mocDetails.requestTypes' : function () {
            if (this.mocDetails.requestTypes.length > 0) {
                for (var i = 0; i < this.mocDetails.requestTypes.length; i++) {
                    if(this.mocDetails.requestTypes[i] == 'process_design' || this.mocDetails.requestTypes[i] == 'review_change') {
                        this.$set("enabledForRequest",true);
                        break;
                    }else{
                        this.$set("enabledForRequest",false);
                        // break;
                    }
                }
            }else{
                this.$set("enabledForRequest",false);
            }
        },
        'enabledForRequest' : function () {
            if(this.enabledForRequest == false) {
                this.$set("mocDetails.changeIn",[]);
                this.$set("mocDetails.changeInOthersInput","");
                this.$set("mocDetails.changeType","");
                this.$set("mocDetails.targetDate","");
                this.$set("mocDetails.departmentsInvolved",[]);
            }
        }
    },
    ready : function () {
        var that = this;
        Vue.http.get(base_url+'/moc/ajax_get_departments').then(function (response) {
                that.$set("departments",response.data);
                that.departments.sort();
            },
            function () {
                console.log("Something went wrong");
            });

        Vue.http.post(base_url+'/moc/ajax_feed_superior',{ id : mocID }).then(function (response) {
                that.$set('formDetails.employeeName',response.data['firstname']+' '+response.data['middlename']+'. '+response.data['lastname']);
                that.$set('formDetails.referenceNo',response.data['transaction_code']);
                that.$set('formDetails.employeeNumber',response.data['employeeid']);
                that.$set('formDetails.company',response.data['company']);
                that.$set('formDetails.department',JSON.parse(response.data['dept_sec'])[0]);
                that.$set('formDetails.section',JSON.parse(response.data['dept_sec'])[1]);
                that.$set('formDetails.contactNumber',response.data['contact_no']);
                that.$set('formDetails.dateFiled',response.data['date']);
                that.$set('formDetails.status',response.data['status']);

                for(var i in response.data['rcodes']) {
                    that.mocDetails.requestTypes.push(response.data['rcodes'][i]['request_code']);
                }

                that.$set('mocDetails.urgency',response.data['urgency']);
                var rcodesValues = [];
                for(var i = 0; i < response.data['rcodes'].length; i++)
                {
                    rcodesValues.push(response.data['rcodes'][i]['request_code']);
                }

                if (rcodesValues.indexOf("review_change") != -1 || rcodesValues.indexOf("process_design") != -1) {
                    for(var i in response.data['ccodes']) {
                        that.mocDetails.changeIn.push(response.data['ccodes'][i]['changein_code']);
                        if(response.data['ccodes'][i]['changein_code'] == "others") {
                            that.$set('mocDetails.changeInOthersInput',response.data['ccodes'][i]['changein_desc']);
                        }
                    }
                    that.$set('mocDetails.changeType',response.data['change_type']);
                    that.$set('mocDetails.targetDate',response.data['implementation_date']);
                    for(var i in JSON.parse(response.data['dept_involved'])) {
                        that.mocDetails.departmentsInvolved.push(JSON.parse(response.data['dept_involved'])[i]);
                    }
                }
                that.$set('mocDetails.title',response.data['title']);
                that.$set('mocDetails.backgroundInfo',response.data['reason']);
                that.$set('mocDetails.ssmdActionPlan',response.data['ssmd_action_plan']);
                that.$set('mocDetails.csmdActionPlan',response.data['csmd_action_plan']);
                for(var i in response.data['attachments']) {
                    $attachDetails = JSON.parse(response.data['attachments'][i]['fn']);
                    $attachDetails['urlDownload'] = response.data['attachments'][i]['urlDownload'];
                    if(response.data['attachments'][i]['attachment_type'] == 0) {
                        that.attachmentsOld.push($attachDetails);
                    }else if(response.data['attachments'][i]['attachment_type'] == 1) {
                        that.SSMDattachmentsOld.push($attachDetails);
                    }else if(response.data['attachments'][i]['attachment_type'] == 2) {
                        that.CSMDattachmentsOld.push($attachDetails);
                    }
                }

                if(response.data['status'] == 'NEW') {
                    that.$set("comment",response.data['comments'][0]['comment']);
                }else {
                    var messages = [];
                    for (var i in response.data['comments']) {
                        messages.push(response.data['comments'][i]['employee']['firstname']+' '+response.data['comments'][i]['employee']['lastname']+' '+response.data['comments'][i]['ts_comment']+' : '+response.data['comments'][i]['comment']);
                    }
                    that.$set("messages",messages.join('\n'));
                }

            },
            function () {
                alert("Something went wrong");
            });
    },
});