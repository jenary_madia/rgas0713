$(document).ready(function(){
	var base_url = $("base").attr("href");
	if ( $("#endorsements_my_list").length != 0 ) {
        var requests_my_table = $('#endorsements_my_list').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/endorsements-my",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			]
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }

	if ( $("#endorsements_new_list").length != 0 ) {
        var endorsements_new_list = $('#endorsements_new_list').dataTable( {
			"fnInitComplete": function (oSettings, json) {
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						endorsements_new_list.fnFilter( $(this).val() );
					});
				label.appendTo($("#endorsements_new_list_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/te/departments/for_chrd", function(departments){
					$.each(departments, function(a, b){
						// 1
						// select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						select.append( '<option value="'+b.te_department+'">'+b.te_department+'</option>' );
					});
				});

				$('#endorsements_new_list_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						$(select).val('');
					}
					else{
						$(select).val($(this).val().toUpperCase());
					}
				});
			},
			// "sDom": '<"top"<"#endorsements_new_list_length.dataTables_length" and >f>rt<"bottom"i><"clear">',
			"sDom": '<"top"<"#endorsements_new_list_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/endorsements-new",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": true },		
				{ "sClass": "td_center hide hidden", "bSortable": false },		
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			]
		});   

	//requests_my_table.fnSort( [  [0,"desc"]]);
		endorsements_new_list.fnSort( [[5,"asc"], [0,"desc"]]);
    }

	if ( $("#endorsements_chrd_vp_approval").length != 0 ) {
        var requests_my_table = $('#endorsements_chrd_vp_approval').dataTable( {
			"fnInitComplete": function (oSettings, json) {
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						requests_my_table.fnFilter( $(this).val() );
					});
				label.appendTo($("#endorsements_chrd_vp_approval_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/te/departments/for_vp", function(departments){
					$.each(departments, function(a, b){
						// 1
						// select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						select.append( '<option value="'+b.te_department+'">'+b.te_department+'</option>' );
					});
				});

				$('#endorsements_chrd_vp_approval_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						$(select).val('');
					}
					else{
						$(select).val($(this).val().toUpperCase());
					}
				});
			},
			"sDom": '<"top"<"#endorsements_chrd_vp_approval_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/endorsements_chrd_vp_approval",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			]
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }

	if ( $("#endorsements_svp_for_cs_approval").length != 0 ) {
        var requests_my_table = $('#endorsements_svp_for_cs_approval').dataTable( {
			"fnInitComplete": function (oSettings, json) {
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						requests_my_table.fnFilter( $(this).val() );
					});
				label.appendTo($("#endorsements_svp_for_cs_approval_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/te/departments/for_svp", function(departments){
					$.each(departments, function(a, b){
						// 1
						// select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						select.append( '<option value="'+b.te_department+'">'+b.te_department+'</option>' );
					});
				});

				$('#endorsements_svp_for_cs_approval_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						$(select).val('');
					}
					else{
						$(select).val($(this).val().toUpperCase());
					}
				});
			},
			"sDom": '<"top"<"#endorsements_svp_for_cs_approval_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/endorsements_svp_for_cs_approval",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			]
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }

	if ( $("#endorsements_pres_approval").length != 0 ) {
        var requests_my_table = $('#endorsements_pres_approval').dataTable( {
			"fnInitComplete": function (oSettings, json) {
				var label = $('<label></lable>');
				label.append("Departments: ");
				var select = $('<select><option value="">ALL</option></select>')
					.appendTo( label )
					.on( 'change', function () {
						requests_my_table.fnFilter( $(this).val() );
					});
				label.appendTo($("#endorsements_pres_approval_length.dataTables_length"));
				var ds = {};
				$.getJSON(base_url + "/te/departments/for_pres", function(departments){
					$.each(departments, function(a, b){
						// 1
						// select.append( '<option value="'+a+'">'+a+'</option>' );
						
						// 2
						select.append( '<option value="'+b.te_department+'">'+b.te_department+'</option>' );
					});
				});

				$('#endorsements_pres_approval_filter input').bind('keyup', function(e) {
					if(!select.find("option[value='"+$(this).val().toUpperCase()+"']").length){
						$(select).val('');
					}
					else{
						$(select).val($(this).val().toUpperCase());
					}
				});
			},
			"sDom": '<"top"<"#endorsements_pres_approval_length.dataTables_length" and >f>rt<"bottom"ip><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/endorsements_pres_approval",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			]
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }

	if ( $("#filer_dashboard").length != 0 ) {
		var requests_my_table = $('#filer_dashboard').dataTable( {
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/filer_dashboard",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },		
			],
			'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }

	if ( $("#approver_dashboard").length != 0 ) {
		var requests_my_table = $('#approver_dashboard').dataTable( {
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			// "sdom": "<'top'i>rt<'bottom'flp><'clear'>",
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/te/approver_dashboard",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			

			],
			'bInfo':false  
		
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }


});