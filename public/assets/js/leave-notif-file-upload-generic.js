$(function () {
    if($('#fileupload').length){
        var sum_filesize = 0;
        $("#fileupload").click(function()
        {
            alerts = 0;
        });
        $('#fileupload').fileupload({
            beforeSend: function(xhr, settings) {

                if($('.attachment_filesize').length){
                    sum_filesize = 0;
                    $('.attachment_filesize').each(function() {
                        sum_filesize += Number($(this).val());
                    });
                    console.log(sum_filesize);
                }

                if($('#attachments p').length){
                    file_counter = $('#attachments p').length + 1;
                }
                else{
                    file_counter = 1;
                }
                if(file_counter > allowed_file_count){
                    alert('You have already exceeded the allowed number of attachments.');
                    return false;
                }
            },
            dataType: 'json',
            done: function (e, data) {
                console.log(allowed_file_count);
                console.log(file_counter);
                if(file_counter > allowed_file_count){
                    if(alerts==0)
                    {
                        alert('You have already exceeded the allowed number of attachments.');
                        alerts = 1;

                    }
                    return false;
                }
                $.each(data.result.files, function (index, file) {
                    console.log(index);
                    console.log(file);
                    soon_sum_filesize = sum_filesize + file.filesize;
                    console.log(soon_sum_filesize);
                    if(soon_sum_filesize > allowed_total_filesize){
                        alert('File cannot be upload because it will exceed the total allowed filesize.');
                        return false;
                    }

                    row_attachment = "<p>";
                    row_attachment += file.original_filename + ' | ' + convert_size(file.filesize);
                    row_attachment += "<input type='hidden' class='attachment_filesize' name='files["+file_counter+"][filesize]' value='"+file.filesize+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
                    row_attachment += " <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>";
                    row_attachment += "</p>";
                    $("#attachments").append(row_attachment);
                    file_counter += 1;
                });
            }
        });
    }

    if($('#med_fileupload').length){
        var sum_filesize = 0;
        $("#med_fileupload").click(function()
        {
            alerts = 0;
        });
        $('#med_fileupload').fileupload({
            beforeSend: function(xhr, settings) {

                if($('.med_attachment_filesize').length){
                    sum_filesize = 0;
                    $('.med_attachment_filesize').each(function() {
                        sum_filesize += Number($(this).val());
                    });
                    console.log(sum_filesize);
                }

                if($('#med_attachments p').length){
                    med_file_counter = $('#med_attachments p').length + 1;
                }
                else{
                    med_file_counter = 1;
                }
                if(med_file_counter > med_allowed_file_count){
                    alert('You have already exceeded the allowed number of attachments.');
                    return false;
                }
            },
            dataType: 'json',
            done: function (e, data) {
                if(med_file_counter > med_allowed_file_count){
                    if(alerts==0)
                    {
                        alert('You have already exceeded the allowed number of attachments.');
                        alerts = 1;

                    }
                    return false;
                }
                $.each(data.result.files, function (index, file) {
                    console.log(index);
                    console.log(file);
                    soon_sum_filesize = sum_filesize + file.filesize;
                    console.log(soon_sum_filesize);
                    if(soon_sum_filesize > med_allowed_total_filesize){
                        alert('File cannot be upload because it will exceed the total allowed filesize.');
                        return false;
                    }

                    row_attachment = "<p>";
                    row_attachment += file.original_filename + ' | ' + convert_size(file.filesize);
                    row_attachment += "<input type='hidden' class='med_attachment_filesize' name='med_files["+med_file_counter+"][filesize]' value='"+file.filesize+"'>";
                    row_attachment += "<input type='hidden' name='med_files["+med_file_counter+"][mime_type]' value='"+file.mime_type+"'>";
                    row_attachment += "<input type='hidden' name='med_files["+med_file_counter+"][original_extension]' value='"+file.original_extension+"'>";
                    row_attachment += "<input type='hidden' name='med_files["+med_file_counter+"][original_filename]' value='"+file.original_filename+"'>";
                    row_attachment += "<input type='hidden' name='med_files["+med_file_counter+"][random_filename]' value='"+file.random_filename+"'>";
                    row_attachment += " <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>";
                    row_attachment += "</p>";
                    $("#med_attachments").append(row_attachment);
                    med_file_counter += 1;
                });
            }
        });
    }
    
    function convert_size(bytes)
    {
        if (bytes >= 1073741824)
        {
            var a = bytes / 1073741824;
            return a.toFixed(5) + ' GB';
        }
        else if (bytes >= 1048576)
        {
            var a = bytes / 1048576;
            return a.toFixed(5) + ' MB';
        }
        else if (bytes >= 1024)
        {
            var a = bytes / 1024;
            return a.toFixed(5) + ' kB';
        }
        else if (bytes > 1)
        {
            return bytes + ' bytes';
        }
        else if (bytes == 1)
        {
            return bytes + ' byte';
        }
        else
        {
            return '0 bytes';
        }
    }

});

