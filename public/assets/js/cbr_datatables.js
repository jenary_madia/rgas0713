$(document).ready(function(){
	var base_url = $("base").attr("href");
	if ( $("#requests_my_list").length != 0 ) {
        var requests_my_table = $('#requests_my_list').dataTable( {
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-my",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
				{ "sClass": "td_center", "bSortable": false },			
			],
			'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     
		});   

		requests_my_table.fnSort( [[0,"desc"]]); 
    }
	if ( $("#requests_new_list").length != 0 ) {
        var requests_new_table = $('#requests_new_list').dataTable( {		
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-new",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },			
			],
			'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  
		});   

		requests_new_table.fnSort( [[0,"desc"]]); 
    }
	if ( $("#requests_in_process_list").length != 0 ) {
        var requests_in_process_table = $('#requests_in_process_list').dataTable( {		
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-in-process",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },	
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },					
				{ "sClass": "td_center", "bSortable": true },					
			],
			'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  
		});

		requests_in_process_table.fnSort( [[0,"desc"]]); 
    }
    if ( $("#requests_acknowledged_list").length != 0 ) {
        var requests_acknowledged_table = $('#requests_acknowledged_list').dataTable({
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-acknowledged",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },	
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false },
			],
			'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  
		});

		requests_acknowledged_table.fnSort( [[0,"desc"]]); 
    } 
	if ( $("#requests_dashboard_filer_list").length != 0 ) {
        var requests_dashboard_filer_table = $('#requests_dashboard_filer_list').dataTable({
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-dashboard-filer",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },	
			],
			'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  
		});

		requests_dashboard_filer_table.fnSort( [[0,"desc"]]); 
    }
	if ( $("#requests_dashboard_staff_list").length != 0 ) {
        var requests_dashboard_filer_table = $('#requests_dashboard_staff_list').dataTable({
			"sDom": '<"top"i>rt<"bottom"i><"clear">',
			"sPaginationType": "full_numbers",
			"sAjaxSource": base_url + "/cbr/requests-dashboard-staff",
			"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			},
			"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
			], 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },
				{ "sClass": "td_center", "bSortable": false },	
			],
			'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  
		});

		requests_dashboard_filer_table.fnSort( [[0,"desc"]]); 
    }

});