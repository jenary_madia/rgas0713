function convert_size(bytes)
{
	if (bytes >= 1073741824)
	{
		var a = bytes / 1073741824;
		return a.toFixed(5) + ' GB';
	}
	else if (bytes >= 1048576)
	{
		var a = bytes / 1048576;
		return a.toFixed(5) + ' MB';
	}
	else if (bytes >= 1024)
	{
		var a = bytes / 1024;
		return a.toFixed(5) + ' kB';
	}
	else if (bytes > 1)
	{
		return bytes + ' bytes';
	}
	else if (bytes == 1)
	{
		return bytes + ' byte';
	}
	else
	{
		return '0 bytes';
	}
}