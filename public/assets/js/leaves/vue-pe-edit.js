var base_url = $("base").attr("href");
new Vue({
    el : "#peMyViewEdit",
    data : {
        departmentID : "",
        sectionID : "",
        sections  : [],
        employees : [],
        employeeIndex : "",
        toSend : [],
        toAdd : {
            employeeName : "",
            employeeNumber : "",
            notificationType : "",
            date : "",
            timeIn : "",
            timeOut : "",
            reason : "",
            employeeID : "",
            active : false,
        },
        toEdit : {},
        employeesToFile : [],
        employeesToFileOld : [],
        editable : false,
    },
    computed : {
        checkIfAddValid : function() {

            if(this.toAdd.employeeName == "") {
                return false;
            }

            if(this.toAdd.employeeNumber == "") {
                return false;
            }

            if(this.toAdd.fromDate == "") {
                return false;
            }

            if(this.toAdd.fromPeriod == "") {
                return false;
            }

            if(this.toAdd.reason == "") {
                return false;

            }if(this.toAdd.totalDays == "") {
                return false;
            }

            if(this.toAdd.duration == "") {
                return false;
            }else{
                if(this.toAdd.duration == "multiple") {
                    if(this.toAdd.toDate == "") {
                        return false;
                    }
                    if(this.toAdd.toPeriod == "") {
                        return false;
                    }
                }
            }

            return true;

        },
        checkIfEditValid : function() {

            if(this.toEdit.employeeName == "") {
                return false;
            }

            if(this.toEdit.employeeNumber == "") {
                return false;
            }

            if(this.toEdit.fromDate == "") {
                return false;
            }

            if(this.toEdit.fromPeriod == "") {
                return false;
            }

            if(this.toEdit.reason == "") {
                return false;

            }if(this.toEdit.totalDays <= 0) {
                return false;
            }

            if(this.toEdit.duration == "") {
                return false;
            }else{
                if(this.toEdit.duration == "multiple") {
                    if(this.toEdit.toDate == "") {
                        return false;
                    }
                    if(this.toEdit.toPeriod == "") {
                        return false;
                    }
                }
            }

            return true;

        }
    },
    methods : {
        saveEdit : function () {
            var that = this;
            Vue.http.post(base_url+'/lrf/get_employees',this.toEdit).then(function (response) {
                    var refId = response['data']['id'];
                    that.employeesToFile[refId]["date"] =  response['data']['date'];
                    that.employeesToFile[refId]["employeeID"] =  response['data']['employeeID'];
                    that.employeesToFile[refId]["employeeName"] =  response['data']['employeeName'];
                    that.employeesToFile[refId]["employeeNumber"] =  response['data']['employeeNumber'];
                    that.employeesToFile[refId]["leaveType"] =  response['data']['leaveType'];
                    that.employeesToFile[refId]["leaveName"] =  response['data']['leaveName'];
                    that.employeesToFile[refId]["reason"] =  response['data']['reason'];
                    that.employeesToFile[refId]["fromDate"] =  response['data']['fromDate'];
                    that.employeesToFile[refId]["fromPeriod"] =  response['data']['fromPeriod'];
                    that.employeesToFile[refId]["toDate"] =  response['data']['toDate'];
                    that.employeesToFile[refId]["toPeriod"] =  response['data']['toPeriod'];
                    that.employeesToFile[refId]["totalDays"] =  response['data']['totalDays'];
                    that.employeesToFile[refId]["duration"] =  response['data']['duration'];
                    $('#EditLeaveDetails').modal('hide');
                },
                function () {
                    console.log("Something went wrong");
                });

            // $('#EditLeaveDetails').modal('hide');
        },

        deleteOffsetReference : function () {
            var r = confirm("Are you sure you want to delete this?");
            if (r == true) {
                var id = this.toEdit.id;
                this.employeesToFile.splice(id,1);
                this.$set("toEdit",{});
            }
        },
        chooseEmployee : function (reference,index) {
            for (var i = 0; i < this.employeesToFile.length; i++) {
                if(index != i) {
                    this.employeesToFile[i]['active'] = false;
                }else{
                    this.employeesToFile[i]['active'] = true;
                }
            }

            this.$set("toEdit", JSON.parse(JSON.stringify(reference)));
            this.$set("toEdit.id",index);
            this.$set("editable",true);
            this.$set("deletable",true);

        },

        getTotalDays : function () {
            var duration = this.toAdd.duration;
            var startDate = new Date(this.toAdd.fromDate);
            var endDate = new Date(this.toAdd.toDate);
            if (duration == "multiple"){
                if(startDate == 'Invalid Date'|| endDate == 'Invalid Date') {
                    this.$set("toAdd.totalDays", 0);
                }
                var total = 0;
                if ((startDate >= endDate)) {
                    this.$set("toAdd.totalDays", total);
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        total = Math.round(Math.abs((a - b)/(c)));
                }
                this.$set("toAdd.totalDays",total + 1);
            }
        },
        getEditTotalDays : function () {
            var duration = this.toEdit.duration;
            var startDate = new Date(this.toEdit.fromDate);
            var endDate = new Date(this.toEdit.toDate);
            if (duration == "multiple"){
                if(startDate == 'Invalid Date'|| endDate == 'Invalid Date') {
                    this.$set("toEdit.totalDays", 0);
                }
                var total = 0;
                if ((startDate >= endDate)) {
                    this.$set("toEdit.totalDays", total);
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        total = Math.round(Math.abs((a - b)/(c)));
                    this.$set("toEdit.totalDays",total + 1);
                }
            }
        },
        parseToAddData : function () {
            var chosenEmployee = this.employees[this.employeeIndex];
            var fullname = chosenEmployee['firstname']+' '+chosenEmployee['middlename']+' '+chosenEmployee['lastname']
            this.$set('toAdd.employeeNumber',chosenEmployee['employeeid']);
            this.$set('toAdd.employeeName',fullname);
            this.$set('toAdd.employeeID',chosenEmployee['id']);
        },

        getToAddData : function () {
            var that = this;
            Vue.http.post(base_url+'/lrf/get_employees',this.toAdd).then(function (response) {
                    that.employeesToFile.push(response.data);
                    $('#AddLeaveDetails').modal('hide');
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        clear : function () {
            this.$set('toAdd.employeeName',"");
            this.$set('toAdd.employeeNumber',"");
            this.$set('toAdd.leaveType',"");
            this.$set('toAdd.fromDate',"");
            this.$set('toAdd.fromPeriod',"");
            this.$set('toAdd.toDate',"");
            this.$set('toAdd.toPeriod',"");
            this.$set('toAdd.reason',"");
            this.$set('toAdd.employeeID',"");
            this.$set('toAdd.totalDays',"");
            this.$set('toAdd.duration',"");
            this.$set("employeeIndex","");;
        },
        durationLeave : function(index) {
            var employee = this.employeesToFile[index];
            if(employee["duration"] == 'multiple') {
                return employee.fromDate+ ' to ' +employee.toDate;
            }else{
                return employee.fromDate;
            }
        },
    },
    watch : {
        "departmentID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/sections',{ "dept_id" : this.departmentID }).then(function (response) {
                    that.$set("sections",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });

            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        "sectionID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID ,"sect_id" : this.sectionID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
        },
        "toAdd.duration" : function() {
            var duration = this.toAdd.duration;
            this.$set("toAdd.toDate","");
            this.$set("toAdd.toPeriod","");
            if(duration == 'half') {
                this.$set("toAdd.totalDays", 0.5);
                this.$set("toAdd.fromPeriod", "half");
            }else if(duration == 'whole') {
                this.$set("toAdd.totalDays", 1);
                this.$set("toAdd.fromPeriod", "whole");
            }else{
                $('.date_picker').datepicker({
                    dateFormat : 'yy-mm-dd'
                });
            }
        },
        "toAdd.fromDate" : function () {
            this.getTotalDays();
        },
        "toAdd.toDate" : function () {
            this.getTotalDays();
        },
        "toAdd.fromPeriod" : function () {
            if (this.toAdd.duration == "multiple") {
                if(this.toAdd.fromPeriod == "half") {
                    this.$set("toAdd.totalDays",this.toAdd.totalDays - .5);
                }else{
                    if(this.toAdd.toPeriod == "half") {
                        this.$set("toAdd.totalDays",this.toAdd.totalDays + .5);
                    }else{
                        this.getTotalDays();
                    }
                }
            }
        },
        "toAdd.toPeriod" : function () {
            if (this.toAdd.duration == "multiple") {
                if(this.toAdd.toPeriod == "half") {
                    this.$set("toAdd.totalDays",this.toAdd.totalDays - .5);
                }else{
                    if(this.toAdd.fromPeriod == "half") {
                        this.$set("toAdd.totalDays",this.toAdd.totalDays + .5);
                    }else{
                        this.getTotalDays();
                    }
                }
            }
        },
        "employeesToFile" : function () {
            if(this.employeesToFile.length == 0) {
                this.$set("editable",false);
                this.$set("deletable",false);
            }
        },
        "toEdit.duration" : function() {
            var duration = this.toEdit.duration;
            this.$set("toEdit.toDate","");
            this.$set("toEdit.toPeriod","");
            if(duration == 'half') {
                this.$set("toEdit.totalDays", 0.5);
                this.$set("toEdit.fromPeriod", "half");
            }else if(duration == 'whole') {
                this.$set("toEdit.totalDays", 1);
                this.$set("toEdit.fromPeriod", "whole");
            }else{
                $('.date_picker').datepicker({
                    dateFormat : 'yy-mm-dd'
                });
            }
        },
        "toEdit.fromDate" : function () {
            this.getEditTotalDays();
        },
        "toEdit.toDate" : function () {
            this.getEditTotalDays();
        },
        "toEdit.fromPeriod" : function () {
            if (this.toEdit.duration == "multiple") {
                if(this.toEdit.fromPeriod == "half") {
                    this.$set("toEdit.totalDays",this.toEdit.totalDays - .5);
                }else{
                    if(this.toEdit.toPeriod == "half") {
                        this.$set("toEdit.totalDays",this.toEdit.totalDays + .5);
                    }else{
                        this.getEditTotalDays();
                    }
                }
            }
        },
        "toEdit.toPeriod" : function () {
            if (this.toEdit.duration == "multiple") {
                if(this.toEdit.toPeriod == "half") {
                    this.$set("toEdit.totalDays",this.toEdit.totalDays - .5);
                }else{
                    if(this.toEdit.fromPeriod == "half") {
                        this.$set("toEdit.totalDays",this.toEdit.totalDays + .5);
                    }else{
                        this.getEditTotalDays();
                    }
                }
            }
        },
    },

    ready : function () {
        var that = this;
        Vue.http.get(window.location.href).then(function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    response.data[i]["active"] = false;
                    response.data[i]["noted"] = false;
                    that.employeesToFile.push(response.data[i]);
                }
            },
            function () {
                console.log("Something went wrong");
            });
    },
})