"use strict";
var base_url = $("base").attr("href");
new Vue({
    el: '#leave',
    data: {
        form_details : {
            employee_name : "",
            employee_number : "",
            date_filed : "",
            company : "",
            status : "",
            department : "",
            contact_number : "",
            section : "",
        },
        request_details : {
            leave_type : "",
            schedule_type : "",
            rest_day : "",
            duration : "",
            from_date : "",
            from_period : "",
            to_date : "",
            to_period : "",
            total_days : "",
            reason : "",
        },
        comment : "",
        attachments : [],
        med_attachments : [],
        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },
        processing : false,
    },
    computed : {
        getTotalDays: function() {
            var duration = this.request_details.duration;
            var startDate = new Date(this.request_details.from_date);
            var endDate = new Date(this.request_details.to_date);
            var restDays = [22,22];
            if (duration == "multiple") {
                if(this.request_details.schedule_type == "compressed") {
                    restDays = [6,0];
                }
                else if(this.request_details.schedule_type == "regular") {
                    restDays = [0];
                }
                else if(this.request_details.schedule_type == "special") {
                    restDays = this.request_details.rest_day;
                }
                if ((startDate >= endDate) || (startDate == endDate)) {
                    alert("Please check your dates");
                    this.$set("request_details.to_date","");
                    return 0;
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        diffDays = Math.round(Math.abs((a - b)/(c)));
                    var counter = 1;
                    for (var i = 0; i < diffDays; i++) {
                        startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
                        var dateFormated = startDate.toISOString().substr(0,10);
                        var finalDate = new Date(dateFormated);
                        var day = finalDate.getDay();
                        var checkIfrestDay = restDays.indexOf(day);
                        if (checkIfrestDay == -1)
                        {
                            counter += 1;
                        }
                        startDate = new Date(dateFormated);
                    }
                    if(this.request_details.from_period == "half") {
                        counter = counter - .5;
                    }

                    if(this.request_details.to_period == "half") {
                        counter = counter - .5;
                    }
                    this.$set("request_details.total_days",counter);
                }
            }
            else if(duration == "whole") {
                this.$set("request_details.total_days",1);
            }else if(duration == "half"){
                this.$set("request_details.total_days",.5);
            }
        },
    },
    methods : {
        create: function (action) {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });

            $(".med_attachmentData").each(function() {
                that.med_attachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : action,
                form_details : this.form_details,
                request_details : this.request_details,
                attachments : this.attachments,
                med_attachments : this.med_attachments,
                comment : this.comment,
            };

            this.fetch('/lrf/action',createParams);
        },

        recreate: function (action,leave_id) {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });

            $(".med_attachmentData").each(function() {
                that.med_attachments.push(JSON.parse($(this).val()));
            });
            var params = {
                action : action,
                leave_id : leave_id,
                form_details : this.form_details,
                request_details : this.request_details,
                attachments : this.attachments,
                med_attachments : this.med_attachments,
                comment : this.comment,
            };

            this.fetch('/lrf/action',params);
        },

        resetLeaveForm: function () {
            $("#homeVisitError").modal("hide");
            this.$set("request_details.leave_type","");
        },

        changeDisableDays: function(day1,day2)
        {
            $('#lrfDateFrom').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != day1 && day != day2)];
            });

            $('#lrfDateTo').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != day1 && day != day2)];
            });
        },

        fetch : function (actionUrl,params) {
            var that = this;
            Vue.http.post(base_url + actionUrl, params, {
                headers: {
                    "Cache-Control": "no-cache, no-store, must-revalidate",
                    "Pragma": "no-cache",
                    "Expires": "0"
                }
            }).then(function (response) {
                    that.$set("submitStatus.submitted", true);
                    that.$set("submitStatus.success", response.data['success']);
                    that.$set("submitStatus.message", response.data['message']);
                    that.$set("submitStatus.errors", response.data['errors']);
                    window.scrollTo(0, 0);
                    if (response.data["success"] == true) {
                        setTimeout(function () {
                            window.location = response.data["url"]
                        }, 1000)
                    } else {
                        that.$set('attachments', []);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        }

    },
    watch : {

        "request_details.leave_type" : function () {
            if (this.request_details.leave_type == "HomeVisit") {
                Vue.http.post(base_url + '/lrf/validate_leave',{}, {
                    headers: {
                        'Cache-Control': 'no-cache'
                    }
                }).then(function (response) {
                    if (! response.data) {
                        $("#homeVisitError").modal("show");
                    } else {
                        this.changeDisableDays(8, 9);
                    }
                },
                function () {
                    console.log("Something went wrong");
                });
            }
        },

        "request_details.duration" : function () {
            $('#lrfDateFrom').removeAttr("disabled");
            if (this.request_details.duration == 'whole') {
                // checkIfNeedMedCert(1);
                this.$set("request_details.from_period","whole");
            }else if (this.request_details.duration == 'half') {
                $('#lrfTotalLeaveDays').val(0.5);
                // checkIfNeedMedCert(0.5);
                this.$set("request_details.from_period","half");
            }
        }

    },
    ready : function () {
        this.$watch("request_details.schedule_type",function () {
            if (this.request_details.schedule_type === 'special') {
                $('.labelRestday').addClass("required");
                $('#lrfRestDay').val([6,0]);
                $("#lrfRestDay").multiselect('enable');
                this.$set("request_details.rest_day",[6,0]);
                this.changeDisableDays(6,0);
            }else if(this.request_details.schedule_type === 'regular'){
                $('.labelRestday').removeClass("required");
                $('#lrfRestDay').val(0);
                $("#lrfRestDay").multiselect('disable');
                this.$set("request_details.rest_day",[0]);
                this.changeDisableDays(0,9);
            }else if(this.request_details.schedule_type === 'compressed'){
                $('.labelRestday').removeClass("required");
                $('#lrfRestDay').val([6,0]);
                $("#lrfRestDay").multiselect('disable');
                this.$set("request_details.rest_day",[6,0]);
                this.changeDisableDays(6,0);
            }

            $("#lrfRestDay").multiselect("refresh");

        });
        var restDays = $('#lrfRestDay').val();
        var that = this;

        $('body').on('click','#home-visit-btn-error',function () {
            $("#homeVisitError").modal("hide");
            $('.LRFAdditionalFields').fadeOut();
            $('#divLeaveDetails').fadeOut();
            $('.titleLeaveType').fadeIn();
        });

        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd',
            beforeShowDay : function(date) {
                var day = date.getDay();
                return [(day != 6 && day != 0)];
            }
        });

        $('#lrfRestDay').multiselect({
            onInitialized: function () {
                that.changeDisableDays(restDays[0],restDays[1]);
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
            },
            onChange: function(option, checked) {
                restDays = $('#lrfRestDay').val();
                that.changeDisableDays(restDays[0],restDays[1]);
                that.$set("request_details.rest_day",restDays);
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
                else {
                    // Enable all checkboxes.
                    $('#lrfRestDay option').each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', false);
                        input.parent('li').addClass('disabled');
                    });
                }

            },
            onDropdownShown: function () {
                var selectedOptions = $('#lrfRestDay option:selected');

                if (selectedOptions.length >= 2) {
                    // Disable all other checkboxes.
                    var nonSelectedOptions = $('#lrfRestDay option').filter(function() {
                        return !$(this).is(':selected');
                    });

                    nonSelectedOptions.each(function() {
                        var input = $('input[value="' + $(this).val() + '"]');
                        input.prop('disabled', true);
                        input.parent('li').addClass('disabled');
                    });
                }
            }
        });
    }
})/**
 * Created by octal on 27/07/2016.
 */
