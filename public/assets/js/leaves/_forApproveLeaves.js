$(document).ready(function(){
    
	var base_url = $("base").attr("href");
    if ( $("#forApproveLeaves").length != 0 ) {
        var requests_my_table = $('#forApproveLeaves').dataTable( {
			"bDestroy": true,
			"sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/lrf/for_approval_leaves",
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
				"sSearch" : "Search:"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  	$('div#forApproveLeaves_info').append(' <button class="btn btn-default btndefault" id="approveSelected" name="lrfAction" value="batchApprove">APPROVE</button>');
			   		$("#approveSelected").on("click", function(){
			   			var toApproved = [];
				   		$('input:checked.chooseLeave').each(function () {
					       toApproved.push($(this).val());
						});

			   		});

			   		$("#chooseAll").on("click",function(){
			   			$('input:checkbox.chosenLeave').prop('checked', this.checked);  
			   		});
			   },
			  	"aLengthMenu": [
					[10, 20, 50, 100, -1],
					[10, 20, 50, 100, "All"]
				], 
				"aoColumns": [
					{ "sWidth": "15%","bSortable" : true},
					{ "sWidth": "15%","bSortable" : true},
					{ "sWidth": "15%","bSortable" : false},
				    { "bSortable" : false},
				    { "bSortable" : false},
					{ "sWidth": "5%","bSortable" : false},
					{ "sWidth": "15%","bSortable" : false},
			     	{ "sWidth": "18%","bSortable" : false},
				    
				],
		});
        
    }

    $("#formApproveLeave").on("submit", function(e){
		var r = confirm("APPROVED SELECTED LEAVES?");
		if (r == true) {
		    $("#formApproveLeave").submit;
		}else{
			e.preventDefault();
		}
    });
});

    
    

