var base_url = $("base").attr("href");
$(document).ready(function(){
    if ( $("#myMRF").length != 0 ) {
        var requests_my_table = $('#myMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-my-mrf/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#waitingMRF").length != 0 ) {
        var waitingMRF = $('#waitingMRF').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/mrf/fetch-for-endorsement-approval-mrf/99/5",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': true},
                { "sWidth": "15%",'bSortable': true},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }


    $('body').on('submit','#formMyMRF',function (e) {
        var r = confirm("Delete this MRF?");
        if (r != true) {
            e.preventDefault();
        }
    });

});

