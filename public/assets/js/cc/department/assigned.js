$(function () 
    {
        "use strict";
        $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
        
     $("button[name='action'][value='assign'] , button[name='action'][value='save'] , button[name='action'][value='return']").click(function(e)
            {
               
                 $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
            });

         $("button[name='action'][value='send']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }

                $(".requirement_txt").each(function()
                {
                     var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                     if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                     {
                        alert("Options is required on others field.");
                        e.preventDefault();
                        return false;
                     }

                })  

                var index = 0;
                while ( index >= 0)
                {
                    if( $("input[name='availability[" + index + "]']").length )
                    {
                        if( !$("input[name='availability[" + index + "]']:checked").length )
                        {                            
                            alert($("input[name='employeeRequirement[" + index + "]']").val() + " " + " option is required.");
                            index = -1;
                            e.preventDefault();
                            return false;
                        }
                        else
                        {
                            index = index + 1;
                        }


                        // if( $("input[name='availability[" + index + "]']:checked").val() == "yes" && !$("input[name='date[" + index + "]']").val())
                        // {                            
                        //     alert($("input[name='employeeRequirement[" + index + "]']").val() + " " + " date field is required.");
                        //     index = -1;
                        //     e.preventDefault();
                        //     return false;
                        // }
                        // else
                        // {
                        //     index = index + 1;
                        // }
                    }
                    else
                    {
                        index = -1;
                    }
                }
                

                if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }
                else if($(this).val() == "assign")
                {
                    if(!$("select[name='assign']").val())
                    {
                        alert("Assign field is required.");
                        e.preventDefault();
                    }      
                    else
                    {
                        $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                    }             
                }
                else
                {
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }   
            });

         $('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });

        $(".add-row").live("click",function()
            {
                var length = $(".delete-row").length;
                if(length == 5) return false;

                x++
                var dt = '<tr>\
                        <td class="labels2">\
                            <button  type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                            <button  type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                            <input  type="text" class="requirement_txt" name="requirement[' + x + ']" />\
                        </td>\
                        <td align="center" class="labels2" ><input  type="radio" value="yes" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" value="no" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" value="not" name="requirement_app[' + x + ']" checked /></td>\
                        <td align="center" class="labels2" ><input  class="date_picker" type="text" name="requirement_date[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="text" name="requirement_amount[' + x + ']"  class="isDecimal"  /></td>\
                        <td align="center" class="labels2" ><input  type="text" name="requirement_remarks[' + x + ']" /></td>\
                    </tr>';
                $(this).parent().parent().after(dt);
                $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
            });

            $(".delete-row").live("click" , function()
            {
                var length = $(".delete-row").length;
                if(length > 1)
                    $(this).parent().parent().remove();
            });


             $(".isDecimal").live("keypress",function(event)
              {
                return isDecimal(event);
              })

            function isDecimal(evt) 
              {
                  evt = (evt) ? evt : window.event;
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                      return false;
                  }
                  return true;
              }
    });
