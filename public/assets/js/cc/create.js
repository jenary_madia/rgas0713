var base_url = $("base").attr("href");

    $(function () 
    {
            "use strict";
            $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd'  });

            $("button[name='action'][value='send']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }
                
                if(!$("select[name='employee']").val())
                {
                    alert("Employee is required.");
                    e.preventDefault();
                }                
                else if(!$("input[name='effective_date']").val())
                {
                    alert("Effective Date of resignation is required.");
                    e.preventDefault();
                }
                else if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }
                else if($(".newdept").length)
                {
                    $(".newdept").each(function(x)
                    {

                        if(x == 0)
                        {
                            if(!$(this).val())
                            {
                                alert("Other department name is required.");
                                e.preventDefault();
                                return false;
                            }
                            else
                            {

                                $("input[name='employee_" + $(this).val() + "[]']").each(function(y)
                                {
                                    if(y == 0)
                                    {
                                        if(!$(this).val())
                                        {
                                            alert("Employee Requirement is required to fill up.");
                                            e.preventDefault();
                                            return false;
                                        }
                                        else
                                        {
                                            $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                                        }  
                                    }
                                });
                            }
                        }
                    })       

                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");             
                }
                else
                {
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }   
                
                
            });

            $('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });

            $("button[name='action'][value='save']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }
                
                if(!$("select[name='employee']").val())
                {
                    alert("Employee is required.");
                    e.preventDefault();
                }      
                else
                {
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }            
                
            })
		
            $(".employee").chosen({width:"100%"}).change(function() {
               
                var id = $(this).val();
                
                $.ajax({
                 headers: {  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                'type': 'POST',
                "data" : {id:id},
                'url': base_url + "/cc/getemployee",
                "dataType": "json",
                'success': function (data) {

                    $("input[name='emp_no']").val( data.employee_no );
                    $("input[name='division']").val( data.div_code );
                    $("input[name='department']").val( data.dept_name );
                    $("#aside").text( data.dept_name );
                    $("input[name='section']").val( data.section );
                    $("input[name='section_name']").val( data.section_name );
                    $("input[name='position']").val( data.position );
                    $("input[name='div_head']").val( data.div_head_name );
                    $("input[name='dept_head']").val( data.dept_head_name );
                    $("input[name='supervisor']").val( data.superior_name );
                    $("input[name='date_hired']").val( data.date_hired );

                    $("input[name='div_head_id']").val( data.div_head_id );
                    $("input[name='dept_head_id']").val( data.dept_head_id );
                    $("input[name='supervisor_id']").val( data.superior_id );
                    $("input[name='department_id']").val( data.dept_id );

                    $(".dept-cmb").val("");
                    $(".dept-cmb option").css("display","");
                    $(".dept-cmb option[value='" + data.dept_id + "']").css("display","none");
                }
              });

            });;
            $(".btn-delete-dept").live("click",function()
            {
                var con = confirm("Are you sure you want to delete selected department and all its contents?");
                if(con)
                {
                    $(this).parent().parent().parent().prev().remove();
                    $(this).parent().parent().parent().next().remove();
                    $(this).parent().parent().parent().remove();
                }
            });

            $(".add-dept").click(function()
            {
                var cmb = $("#dept-cmb").clone();
                $(cmb).removeClass("hidden");
                var dep = '<div class="clear_20"></div> \
                    <div class="row "> \
                        <div class="col-md-8"> \
                            <div class="col1_form_container"> \
                                <label class="labels">Department Name:</label> \
                            </div> \
                            <div class="col2_form_container cmb-dept-container"> \
                            </div> \
                        </div> \
                        <div class="col-md-3"> \
                            <div class="attachment_container"> \
                                <button type="button" class="btn btnbrowse btn-danger btn-delete-dept" id="">DELETE</button> \
                            </div> \
                        </div> \
                    </div> \
                    <table border="1">\
                    <tr>\
                        <th class="text-center"><span class="labels2" >EMPLOYEE REQUIREMENTS</span></th>\
                        <th class="text-center"><span class="labels2" >YES</span></th>\
                        <th class="text-center"><span class="labels2" >NO</span></th>\
                        <th class="text-center"><span class="labels2" >NOT APPLICABLE</span></th>\
                        <th class="text-center"><span class="labels2" >DATE</span></th>\
                        <th class="text-center"><span class="labels2" >AMOUNT</span></th>\
                        <th class="text-center"><span class="labels2" >REMARKS</span></th>\
                    </tr>\
                    <tr>\
                        <td class="td_style">\
                                    <button type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                                    <button type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                                    <input type="text" class="" name=""  />\
                                </td>\
                        <td align="center"><input type="radio" disabled /></td>\
                        <td align="center"><input type="radio" disabled /></td>\
                        <td align="center"><input type="radio" disabled value="checked" /></td>\
                        <td align="center"><input type="text" disabled /></td>\
                        <td align="center"><input type="text" disabled /></td>\
                        <td class="td_style" align="center"><input type="text" /></td>\
                    </tr>\
                    </table>';

                $(".other_dept").append(dep);
                $(".cmb-dept-container").append(cmb);
                $(cmb).attr("name","other_dept[]");
                $(cmb).addClass("newdept");
                $(".cmb-dept-container").removeClass("cmb-dept-container");
            });

            $(".add-row").live("click",function()
            {
                var dt = '<tr>\
                        <td class="td_style">\
                                    <button type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                                    <button type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                                    <input type="text" class="" name=""  />\
                                </td>\
                        <td align="center"><input type="radio" disabled /></td>\
                        <td align="center"><input type="radio" disabled /></td>\
                        <td align="center"><input type="radio" value="checked" disabled /></td>\
                        <td align="center"><input type="text" disabled /></td>\
                        <td align="center"><input type="text" disabled /></td>\
                        <td align="center"><input type="text" /></td>\
                    </tr>';
                $(this).parent().parent().after(dt);
                $(".dept-cmb").trigger("change");
            });

            $(".delete-row").live("click" , function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length != 1)
                    $(this).parent().parent().remove();
            });

            $(".dept-cmb").live("change" , function()
            {
                var id = $(this).val();
                $(this).parent().parent().parent().next().children().children().children("td:nth-child(7)").children("input").attr("name","remarks_" + id + "[]");
                $(this).parent().parent().parent().next().children().children().children("td:nth-child(1)").children("input").attr("name","employee_" + id + "[]");
            });

         
	});