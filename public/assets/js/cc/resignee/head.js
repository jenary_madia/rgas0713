$(function () 
    {
        "use strict";
        $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
$("button[name='action'][value='assign'] , button[name='action'][value='save'] , button[name='action'][value='return']").click(function(e)
            {
               
                 $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
            });

         $("button[name='action'][value='send']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }


                if(!$("input[name='manual_surrender']:checked").length )
                {
                    alert("MANUALS SURRENDERED option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='work_files']:checked").length)
                {
                    alert("WORK FILES SURRENDERED option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='drawer']:checked").length )
                {
                    alert("DRAWER/CABINET KEYS SURRENDERED option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='assets']:checked").length )
                {
                    alert("FIXED ASSETS SURRENDERED option is required.");
                    e.preventDefault(); 
                }   
                else if(!$("input[name='supplies']:checked").length)
                {
                    alert("OFFICE SUPPLIES SURRENDERED option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='accountable']:checked").length )
                {
                    alert("ACCOUNTABLE FORMS SURRENDERED option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='revolving']:checked").length )
                {
                    alert("REVOLVING FUND/PETTY CASH SURRENDERED option is required.");
                    e.preventDefault();
                }                
                else if(!$("input[name='car']:checked").length )
                {
                    alert("COMPANY CAR SURRENDERED option is required.");
                    e.preventDefault();
                }     
                else if($("input[name='manual_surrender']:checked").val() == "yes" && !$("input[name='manual_surrender_date']").val())
                {
                    alert("MANUALS SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='work_files']:checked").val() == "yes" && !$("input[name='work_files_date']").val())
                {
                    alert("WORK FILES SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='drawer']:checked").val() == "yes" && !$("input[name='drawer_date']").val())
                {
                    alert("DRAWER/CABINET KEYS SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='assets']:checked").val() == "yes" && !$("input[name='assets_date']").val())
                {
                    alert("FIXED ASSETS SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='supplies']:checked").val() == "yes" && !$("input[name='supplies_date']").val())
                {
                    alert("OFFICE SUPPLIES SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='accountable']:checked").val() == "yes" && !$("input[name='accountable_date']").val())
                {
                    alert("ACCOUNTABLE FORMS SURRENDERED date field is required.");
                    e.preventDefault();
                }   
                else if($("input[name='revolving']:checked").val() == "yes" && !$("input[name='revolving_date']").val())
                {
                    alert("REVOLVING FUND/PETTY CASH SURRENDERED date field is required.");
                    e.preventDefault();
                }                
                else if($("input[name='car']:checked").val() == "yes" && !$("input[name='car_date']").val())
                {
                    alert("COMPANY CAR SURRENDERED date field is required.");
                    e.preventDefault();
                }                
                else if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }                
                else if($(this).val() == "assign")
                {
                    if(!$("select[name='assign']").val())
                    {
                        alert("Assign field is required.");
                        e.preventDefault();
                    }        
                    else
                    {
                        $(".requirement_txt").each(function()
                        {
                             var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                             if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                             {
                                alert("Options is required on others field.");
                                e.preventDefault();
                                return false;
                             }

                        })  
                        $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                    }           
                }
                else
                {
                    $(".requirement_txt").each(function()
                    {
                         var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                         if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                         {
                            alert("Options is required on others field.");
                            e.preventDefault();
                            return false;
                         }

                    })  
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }   
             
             
            })

        $('form').submit(function()
        {
            $("button[name='action']").attr("disabled",true);
        });

        var x = 0;
		$(".add-row").live("click",function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length == 14) return false;

                x++
                var dt = '<tr>\
                        <td class="labels2">\
                            <button  type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                            <button  type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                            <input  type="text" class="requirement_txt" name="requirement[' + x + ']" />\
                        </td>\
                        <td align="center" class="labels2" ><input  type="radio" value="yes" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" value="no" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" checked value="not" name="requirement_app[' + x + ']" checked="checked" /></td>\
                        <td align="center" class="labels2" ><input  class="date_picker" type="text" name="requirement_date[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="text" name="requirement_remarks[' + x + ']" /></td>\
                    </tr>';
                $(this).parent().parent().after(dt);
                $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
            });

            $(".delete-row").live("click" , function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length > 10)
                    $(this).parent().parent().remove();
            });

           
	});
