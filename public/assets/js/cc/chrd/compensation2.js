$(function () 
    {
        "use strict";

        $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });

          $("button[name='action'][value='assign'] , button[name='action'][value='save']").click(function(e)
            {
               
                 $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
            });

         $("button[name='action'][value='send']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }

                if(!$("input[name='sss']:checked").length )
                {
                    alert("WITH OUTSTANDING SSS LOAN option is required.");
                    e.preventDefault();
                }   
                else if(!$("input[name='aub']:checked").length )
                {
                    alert("WITH OUTSTANDING AUB LOAN option is required.");
                    e.preventDefault();
                }    
                else if(!$("input[name='hmo']:checked").length )
                {
                    alert("WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS option is required.");
                    e.preventDefault();
                }           
                else if($("input[name='sss']:checked").val() == "yes" && !$("input[name='sss_date']").val())
                {
                    alert("WITH OUTSTANDING SSS LOAN date field is required.");
                    e.preventDefault();
                }           
                else if($("input[name='sss']:checked").val() == "yes" && !$("input[name='sss_amount']").val())
                {
                    alert("WITH OUTSTANDING SSS LOAN amount field is required.");
                    e.preventDefault();
                }  
                else if($("input[name='aub']:checked").val() == "yes" && !$("input[name='aub_date']").val())
                {
                    alert("WITH OUTSTANDING AUB LOAN date field is required.");
                    e.preventDefault();
                }           
                else if($("input[name='aub']:checked").val() == "yes" && !$("input[name='aub_amount']").val())
                {
                    alert("WITH OUTSTANDING AUB LOAN amount field is required.");
                    e.preventDefault();
                }  
                else if($("input[name='hmo']:checked").val() == "yes" && !$("input[name='hmo_date']").val())
                {
                    alert("WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS date field is required.");
                    e.preventDefault();
                }           
                else if($("input[name='hmo']:checked").val() == "yes" && !$("input[name='hmo_amount']").val())
                {
                    alert("WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS amount field is required.");
                    e.preventDefault();
                }               
                else if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }  
                else
                {
                    $(".requirement_txt").each(function()
                    {
                         var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                         if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                         {
                            alert("Options is required on others field.");
                            e.preventDefault();
                            return false;
                         }

                    })  

                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }                 
            });

        $('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });
        
        $(".add-row").live("click",function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length == 9) return false;

                x++
                var dt = '<tr>\
                        <td class="labels2">\
                            <button  type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                            <button  type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                            <input  type="text" class="requirement_txt" name="requirement[' + x + ']" />\
                        </td>\
                        <td align="center"><input  type="radio" value="yes" name="requirement_app[' + x + ']" /></td>\
                        <td align="center"><input  type="radio" value="no" name="requirement_app[' + x + ']" /></td>\
                        <td align="center"><input  type="radio" value="not" checked name="requirement_app[' + x + ']"  /></td>\
                        <td align="center"><input  class="date_picker" type="text" name="requirement_date[' + x + ']" /></td>\
                        <td align="center"><input  type="text" name="requirement_remarks[' + x + ']" /></td>\
                        <td align="center"><input  type="text" name="requirement_amount[' + x + ']" class="isDecimal" /></td>\
                    </tr>';
                $(this).parent().parent().parent().append(dt);
                $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
            });

            $(".delete-row").live("click" , function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length > 5)
                    $(this).parent().parent().remove();
            });

        

             $(".isDecimal").live("keypress",function(event)
              {
                return isDecimal(event);
              })

            function isDecimal(evt) 
              {
                  evt = (evt) ? evt : window.event;
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                      return false;
                  }
                  return true;
              }
    });
