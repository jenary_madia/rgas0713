$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    if($('#cc_assesment_dashboard').length)
    {
        var asses = $('#cc_assesment_dashboard').dataTable( {     
              "sPaginationType": "full_numbers",
              "bProcessing": true,
              "bServerSide": false,
              'iDisplayLength' : 5,
              "sAjaxSource": base_url + "/cc/report/assesment/get",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : true,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false }, 
               { "sClass": "td_center", "bSortable": false }, 
                { "sClass": "td_center", "bSortable": false },    
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  

      asses.fnSort( [[0,"desc"]]);  
    }
     
               
    if($('#cc_resignee_dashboard').length)
    {
      var resignee = $('#cc_resignee_dashboard').dataTable( {     
              "sPaginationType": "full_numbers",
              "bProcessing": true,
              "bServerSide": false,
              'iDisplayLength' : 5,
              "sAjaxSource": base_url + "/cc/report/resignee/get",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : true,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },   
              { "sClass": "td_center", "bSortable": false },   
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  

      resignee.fnSort( [[0,"desc"]]);   
    }

    if($('#cc_approval_dashboard').length)
    {
      $('#cc_approval_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              'iDisplayLength' : 5,
              "sAjaxSource": base_url + "/cc/report/csdh/get",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : true,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },  
              { "sClass": "td_center", "bSortable": false }, 
              { "sClass": "td_center", "bSortable": false },    
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });
    }

    if($('#cc_filer_dashboard').length)
    {
      var filer = $('#cc_filer_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              'iDisplayLength' : 5,
              "sAjaxSource": base_url + "/cc/report/filer/get",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : true,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false }, 
              { "sClass": "td_center", "bSortable": false }, 
              { "sClass": "td_center", "bSortable": false },    
              { "sClass": "td_center", "bSortable": false },      
            ]  
        }); 

      filer.fnSort( [[0,"desc"]]);    
    }
       
    if($('#gd_approval_dashboard').length)
    {
     var gd =  $('#gd_approval_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/gd/get_review",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "hide ", "bSortable": true },
             { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  

          gd.fnSort( [[1,"desc"]]);   
    }  

     if($('#gd_dashboard').length)
    {
        $('#gd_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/gd/get_review_dashboard",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "hide", "bSortable": true },
             { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  

          gd.fnSort( [[1,"desc"]]);   
    }  

     if($('#pmf_approval_dashboard').length)
     {
        $('#pmf_approval_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/pmf/get_review",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });
     }



    if($('#art_approval_dashboard').length)
    {
     var art =  $('#art_approval_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/art/get_review",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });

      art.fnSort( [[0,"desc"]]); 
    }

    if($('#my_design').length)
    {
     var des = $('#my_design').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/art/my_review",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });

        des.fnSort( [[0,"desc"]]); 
    }

    if($('#sv_approval_dashboard').length)
    {
        $('#sv_approval_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/svr/get_review",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  
    }

    if($('#sv_dashboard').length)
    {
        $('#sv_dashboard').dataTable( {     
              "bProcessing": true,
              "bServerSide": false,
              "sAjaxSource": base_url + "/svr/get_dashboard",
              "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
             },
             "bLengthChange":   false,
              "bFilter": false,
              "bInfo":     false,
              "bPaginate" : false,
              "aoColumns": [
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },
              { "sClass": "td_center", "bSortable": false },     
              { "sClass": "td_center", "bSortable": false },      
            ]  
        });  
    }

    $(".confirm").live("click",function(e)
    {
      var con = confirm("Are you sure you want to delete this record?");
      if(con) 
      {
        window.location = $(this).attr("link");
    }
    });

});