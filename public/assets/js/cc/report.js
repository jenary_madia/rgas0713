$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var requests_my_table = $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true,
                'oLanguage': { 'sSearch':'&nbsp;'},
                "aoColumns": [
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },     
              { "sClass": "td_center", "bSortable": false },      
            ],
            "aaSorting": [[ 0, "desc" ]]  
        }); 


     $("#myrecord_filter input").hide();

    var logs = $('#myrecordlogs').dataTable( {     
              "sPaginationType": "full_numbers" ,
              'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false,
                'oLanguage': { 'sSearch':'&nbsp;'},
                "aoColumns": [
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true },
              { "sClass": "td_center", "bSortable": true }, 
              { "sClass": "td_center", "bSortable": true }, 
              { "sClass": "td_center", "bSortable": true },     
              { "sClass": "td_center hide", "bSortable": false },      
            ],
        });  
		
		
    $(".confirm").live("click",function(e)
    {
    	var con = confirm("Are you sure you want to delete this record?");
    	if(con) 
    	{
    		window.location = $(this).attr("link");
		}
    });


     $("#myrecordlogs_filter input").hide();

     $(document).on( 'click', '.view', function () 
    {
        var id = $(this).attr("id");
        view_logs(id);
    })

    $(document).on( 'mouseover', '.view', function () 
    {
        $(this).css("cursor","pointer")
    })

    $(document).on( 'change', '.department1', function () 
    {
        requests_my_table.fnFilter( $("option:selected",this).text() == "All" ? "" : $("option:selected",this).text() );
    })

    $(document).on( 'change', '.department2', function () 
    {
        logs.fnFilter( $("option:selected",this).text() == "All" ? "" : $("option:selected",this).text() );
    })

    
    function view_logs(id)
    {
        $.ajax({
             headers: {  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            'type': 'POST',
            'url': base_url + "/cc/logs/get/" + id,
            "dataType": "json",
            'success': function (data) 
            {
                logs.fnClearTable();
                for (var j = 0; j < data.length; j++) 
                {
                  logs.fnAddData([data[j][0],data[j][1],data[j][2],data[j][3],data[j][4],data[j][5],data[j][6],data[j][7]]);                 
                }
                logs.fnDraw();

                logs.fnSort( [[0,"asc"],[7,"asc"], [2,"asc"], [4,"asc"]]);

                $("#myModal").modal("show");
            }
          });   
    }
    

});