$(function () 
    {
        "use strict";
        $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });

          $("button[name='action'][value='assign'] , button[name='action'][value='save'] , button[name='action'][value='return']").click(function(e)
            {
               
                 $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
            });

         $("button[name='action'][value='send']").click(function(e)
            {
                try
                {
                    $(".date_picker").each(function()
                    {
                        if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                    })                   
                }
                catch(e)
                {
                   alert("Invalid inputted date");
                   return false;
                }

               

                if(!$("input[name='internet']:checked").length )
                {
                    alert("REVOKED ACCESSED FOR INTERNET option is required.");
                    e.preventDefault();
                }
                else if(!$("input[name='intranet']:checked").length )
                {
                    alert("REVOKED ACCESSED FOR INTRANET option is required.");
                    e.preventDefault();
                }
                else if(!$("input[name='internal']:checked").length )
                {
                    alert("REVOKED ACCESSED FOR INTERNAL EMAIL option is required.");
                    e.preventDefault();
                }
                else if(!$("input[name='external']:checked").length )
                {
                    alert("REVOKED ACCESSED FOR EXTERNAL EMAIL option is required.");
                    e.preventDefault();
                }
                else if(!$("input[name='sms']:checked").length )
                {
                    alert("REVOKED ACCESSED SMS BULK MESSAGING option is required.");
                    e.preventDefault();
                }
                else if(!$("input[name='laptop']:checked").length )
                {
                    alert("PC/LAPTOP IN GOOD WORKING CONDITION option is required.");
                    e.preventDefault();
                }
                else if($("input[name='internet']:checked").val() == "yes" && !$("input[name='internet_date']").val())
                {
                    alert("REVOKED ACCESSED FOR INTERNET date field is required.");
                    e.preventDefault();
                }
                else if($("input[name='intranet']:checked").val() == "yes" && !$("input[name='intranet_date']").val())
                {
                    alert("REVOKED ACCESSED FOR INTRANET date field is required.");
                    e.preventDefault();
                }
                else if($("input[name='internal']:checked").val() == "yes" && !$("input[name='internal_date']").val())
                {
                    alert("REVOKED ACCESSED FOR INTERNAL EMAIL date field is required.");
                    e.preventDefault();
                }
                else if($("input[name='external']:checked").val() == "yes" && !$("input[name='external_date']").val())
                {
                    alert("REVOKED ACCESSED FOR EXTERNAL EMAIL date field is required.");
                    e.preventDefault();
                }
                else if($("input[name='sms']:checked").val() == "yes" && !$("input[name='sms_date']").val())
                {
                    alert("REVOKED ACCESSED SMS BULK MESSAGING date field is required.");
                    e.preventDefault();
                }
                else if($("input[name='laptop']:checked").val() == "no" && !$("input[name='laptop_amount']").val())
                {
                    alert("PC/LAPTOP IN GOOD WORKING CONDITION amount field is required.");
                    e.preventDefault();
                }
                else if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }
                else if($(this).val() == "assign")
                {
                    if(!$("select[name='assign']").val())
                    {
                        alert("Assign field is required.");
                        e.preventDefault();
                    }      
                    else
                    {
                         $(".requirement_txt").each(function()
                        {
                             var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                             if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                             {
                                alert("Options is required on others field.");
                                e.preventDefault();
                                return false;
                             }

                        })  
                        $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                    }             
                }
                else
                {
                     $(".requirement_txt").each(function()
                        {
                             var radio = $(this).parent().siblings().children("input[type='radio']").attr("name");
                             if($(this).val() && !$("input[name='" + radio + "']:checked").length)
                             {
                                alert("Options is required on others field.");
                                e.preventDefault();
                                return false;
                             }

                        })  
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }   
            });

$('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });

        $(".add-row").live("click",function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length == 12) return false;

                x++
                var dt = '<tr>\
                        <td class="labels2">\
                            <button  type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>\
                            <button  type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>\
                            <input  type="text" class="requirement_txt" name="requirement[' + x + ']" />\
                        </td>\
                        <td align="center" class="labels2" ><input  type="radio" value="yes" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" value="no" name="requirement_app[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="radio" value="not" name="requirement_app[' + x + ']"  /></td>\
                        <td align="center" class="labels2" ><input  class="date_picker" type="text" name="requirement_date[' + x + ']" /></td>\
                        <td align="center" class="labels2" ><input  type="text" name="requirement_amount[' + x + ']" class="isDecimal"  /></td>\
                        <td align="center" class="labels2" ><input  type="text" name="requirement_remarks[' + x + ']" /></td>\
                    </tr>';
                $(this).parent().parent().after(dt);
                 $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate: 0 });
            });

            $(".delete-row").live("click" , function()
            {
                var length = $(this).parent().parent().siblings("tr").length;
                if(length > 8)
                    $(this).parent().parent().remove();
            });

         

            $(".isDecimal").live("keypress",function(event)
              {
                return isDecimal(event);
              })

            function isDecimal(evt) 
              {
                  evt = (evt) ? evt : window.event;
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                      return false;
                  }
                  return true;
              }
    });