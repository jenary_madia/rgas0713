$(function () 
    {
            "use strict";

            $("button[name='action']").click(function(e)
            {
               if(!$(".remove-fn").length)
               {
                   alert("At least one file need to be attached.");
                   e.preventDefault();
               }       
               else
               {
                   $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
               }         
            })


            $('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });
        
        
    });
