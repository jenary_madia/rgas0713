$(function () 
    {
        "use strict";
     $("button[name='action'][value='assign'] , button[name='action'][value='save'] , button[name='action'][value='return']").click(function(e)
            {
               
                 $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
            });

         $("button[name='action'][value='send']").click(function(e)
            {
                if(!$(".remove-fn").length)
                {
                    alert("At least one file need to be attached.");
                    e.preventDefault();
                }
                else if($(this).val() == "return")
                {
                    if(!$("textarea[name='comment']").val())
                    {
                        alert("Comment field is required.");
                        e.preventDefault();
                    }     
                    else
                    {
                        $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                    }               
                }
                else
                {
                    $("form").append("<input type='hidden' name='action' value='" +  $(this).val() + "' />");
                }   
            });

        $('form').submit(function()
            {
                $("button[name='action']").attr("disabled",true);
            });

    });