$(function()
{
    "use strict";
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

    $('.time_picker').timepicker({
        defaultTime : false,
        showMeridian : true,
        maxHours : 13
    });


     $("#pickup-time").focus(function()
     {
         $('.timepicker span').trigger("click");
     })

     $("button[name='action'][value='send']").click(function(e)
        {
            if(!$("input[name='purpose']").val() )
            {
                alert("Purpose field is required.");
                return false;
            }
            if(!$("input[name='pickup-date[]']").val() )
            {
                alert("At least one vehicle detail is required.");
                return false;
            }            
        });

    $("button[name='add']").click(function()
    {
        $("#destin-company").attr("disabled",false);
        $("#destin-company-other").attr("disabled",true);
        $("#destin-location").attr("disabled",true);
        $("#destin-location-other").attr("disabled",true);
        $("#pickup-company").attr("disabled",false);
        $("#pickup-company-other").attr("disabled",true);
        $("#pickup-location").attr("disabled",true);
        $("#pickup-location-other").attr("disabled",true);
        $("#destin-company").val("");
        $("#destin-company-other").val("");
        $("#destin-location").val("");
        $("#destin-location-other").val("");
        $("#pickup-company").val("");
        $("#pickup-company-other").val("");
        $("#pickup-location").val("");
        $("#pickup-location-other").val("");

        $("#pickup-date").val("");
        $("#pickup-time").val("");
        $("#passenger").val("");
        $("#triptime").val("");


        $("#destin-location-other-tick").attr("disabled",true);
        $("#pickup-location-other-tick").attr("disabled",true);

        $("#destin-location-other-tick").attr("checked",false);
        $("#pickup-location-other-tick").attr("checked",false);
        $("#destin-company-other-tick").attr("checked",false);
        $("#pickup-company-other-tick").attr("checked",false);

        $("#vehicle .active td").css("background-color",""); 
        $("#vehicle .active").removeClass("active");

        $("#myModalLabel").text("ADD SERVICE VEHICLE DETAILS");
        $("#service-modal").modal('show');

        $("#modal").modal('show');
    });

    $("button[name='edit']").click(function()
    {
        if($("#vehicle .active").length)
        {
            $("#pickup-date").val( $("#vehicle .active input[name='pickup-date[]']").val() );
            $("#pickup-time").val( $("#vehicle .active input[name='pickup-time[]']").val() );
            $("#pickup-company").val( $("#vehicle .active input[name='pickup-company[]']").val() );
            $("#pickup-company-other").val( $("#vehicle .active input[name='pickup-company-other[]']").val() );
            $("#pickup-location").val( $("#vehicle .active input[name='pickup-location[]']").val() );
            $("#pickup-location-other").val( $("#vehicle .active input[name='pickup-location-other[]']").val() );
            $("#destin-company").val( $("#vehicle .active input[name='destin-company[]']").val() );
            $("#destin-company-other").val( $("#vehicle .active input[name='destin-company-other[]']").val() );
            $("#destin-location").val( $("#vehicle .active input[name='destin-location[]']").val() );
            $("#destin-location-other").val( $("#vehicle .active input[name='destin-location-other[]']").val() );
            $("#passenger").val( $("#vehicle .active input[name='passenger[]']").val() );
            $("#triptime").val( $("#vehicle .active input[name='triptime[]']").val() );

            if($("#pickup-company-other").val()  && (!$("#pickup-company").val() || $("#pickup-company").val() == 0 ))
            {
                $("#pickup-company").attr("disabled",true);
                $("#pickup-company-other").attr("disabled",false);
                $("#pickup-company-other-tick").attr("checked",true);
            }
            else
            {
                $("#pickup-company-other").val("");
                $("#pickup-company").attr("disabled",false);
                $("#pickup-company-other").attr("disabled",true);
                $("#pickup-company-other-tick").attr("checked",false);
            }

            if($("#pickup-location-other").val()  && (!$("#pickup-location").val() || $("#pickup-location").val() == 0 ) )
            {
                $("#pickup-location").attr("disabled",true);
                $("#pickup-location-other").attr("disabled",false);
                $("#pickup-location-other-tick").attr("checked",true);
            }
            else
            {
                $("#pickup-location-other").val("");

                if($("#pickup-company-other").val())
                 $("#pickup-location").attr("disabled",false);
                else
                 $("#pickup-location").attr("disabled",true);

                $("#pickup-location-other").attr("disabled",true);
                $("#pickup-location-other-tick").attr("checked",false);
            }

            if($("#destin-company-other").val()  && (!$("#destin-company").val() || $("#destin-company").val() == 0 ) )
            {
                $("#destin-company").attr("disabled",true);
                $("#destin-company-other").attr("disabled",false);
                $("#destin-company-other-tick").attr("checked",true);
            }
            else
            {
                $("#destin-company-other").val("");

                $("#destin-company").attr("disabled",false);
                $("#destin-company-other").attr("disabled",true);
                $("#destin-company-other-tick").attr("checked",false);
            }

            if($("#destin-location-other").val() && (!$("#destin-location").val() || $("#destin-location").val() == 0 ) )
            {
                $("#destin-location").attr("disabled",true);
                $("#destin-location-other").attr("disabled",false);
                $("#destin-location-other-tick").attr("checked",true);
            }
            else
            {
                $("#destin-location-other").val("");

                if($("#destin-company-other").val())
                 $("#destin-location").attr("disabled",false);
                else
                 $("#destin-location").attr("disabled",true);

                $("#destin-location-other").attr("disabled",true);
                $("#destin-location-other-tick").attr("checked",false);
            }

            $("#myModalLabel").text("EDIT SERVICE VEHICLE DETAILS");
            $("#service-modal").modal('show');
        }
        else
        {
            alert("Please select specific request to edit.");
        }
    });

    $("button[name='delete']").click(function()
    {
        if($("#vehicle .active").length)
        {
            $("#vehicle .active").remove();
            if( ! $("#vehicle td").length )
            {
                $("button[name='edit']").attr("disabled",true);
                $("button[name='delete']").attr("disabled",true);
            }

        }
        else
        {
            alert("Please select specific request to delete.");
        }
    });

    $("#destin-company-other-tick").click(function()
    {
        $("#destin-location").val("");
        $("#destin-location-other").val("");
        $("#destin-company").val("");
        $("#destin-company-other").val("");

        $("#destin-location-other").attr("disabled",true);
        if($(this).prop("checked"))
        {
           $("#destin-location-other-tick").attr("disabled",false);
           $("#destin-location").attr("disabled",false);
           $("#destin-company-other").attr("disabled",false);
           $("#destin-company").attr("disabled",true);           
        }
        else
        {            
            $("#destin-location-other-tick").attr("checked",false);
            $("#destin-location-other-tick").attr("disabled",true);
            $("#destin-location").attr("disabled",true);
            $("#destin-company").attr("disabled",false);
            $("#destin-company-other").attr("disabled",true);
        }        
    });


    $("#destin-location-other-tick").click(function()
    {
        $("#destin-location").val("");
        $("#destin-location-other").val("");
        if($(this).prop("checked"))
        {
           $("#destin-location").attr("disabled",true);
           $("#destin-location-other").attr("disabled",false);
        }
        else
        {            
            $("#destin-location").attr("disabled",false);
            $("#destin-location-other").attr("disabled",true);
        }        
    });

    $("#pickup-company-other-tick").click(function()
    {
        $("#pickup-location").val("");
        $("#pickup-location-other").val("");
        $("#pickup-company").val("");
        $("#pickup-company-other").val("");

        $("#pickup-location-other").attr("disabled",true);
        if($(this).prop("checked"))
        {
           $("#pickup-location-other-tick").attr("disabled",false);
           $("#pickup-location").attr("disabled",false);
           $("#pickup-company-other").attr("disabled",false);
           $("#pickup-company").attr("disabled",true);           
        }
        else
        {            
            $("#pickup-location-other-tick").attr("checked",false);
            $("#pickup-location-other-tick").attr("disabled",true);
            $("#pickup-location").attr("disabled",true);
            $("#pickup-company").attr("disabled",false);
            $("#pickup-company-other").attr("disabled",true);
        }        
    });


    $("#pickup-location-other-tick").click(function()
    {
        $("#pickup-location").val("");
        $("#pickup-location-other").val("");
        if($(this).prop("checked"))
        {
           $("#pickup-location").attr("disabled",true);
           $("#pickup-location-other").attr("disabled",false);
        }
        else
        {            
            $("#pickup-location").attr("disabled",false);
            $("#pickup-location-other").attr("disabled",true);
        }        
    });

    $("#destin-company").change(function()
    {
        $("#destin-location").val( $(" option:selected",this).attr("loc") );
    });

    $("#pickup-company").change(function()
    {
        $("#pickup-location").val( $(" option:selected",this).attr("loc") );
    });

    $("#save").click(function()
    {
        try
        {
            $(".date_picker").each(function()
            {
                if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
            })                   
        }
        catch(e)
        {
           alert("Invalid inputted date");
           return false;
        }

        if( !$("#pickup-date").val() )
        {
            alert("Pick-up date is required.");
        }
        else if( !$("#pickup-time").val() )
        {
            alert("Pick-up time is required.");
        }
        else if($("#pickup-company-other-tick:checked").length && !$("#pickup-company-other").val())
        {
            alert("Pick-up company is required.");
        }
        else if($("#pickup-company-other-tick:checked").length == 0 && !$("#pickup-company").val())
        {
            alert("Pick-up company is required.");
        }
        else if($("#pickup-location-other-tick:checked").length && !$("#pickup-location-other").val())
        {
            alert("Pick-up location is required.");
        }
        else if($("#pickup-location-other-tick:checked").length == 0 && !$("#pickup-location").val())
        {
            alert("Pick-up location is required.");
        }
         else if($("#destin-company-other-tick:checked").length && !$("#destin-company-other").val())
        {
            alert("Destination company is required.");
        }
        else if($("#destin-company-other-tick:checked").length == 0 && !$("#destin-company").val())
        {
            alert("Destination company is required.");
        }
        else if($("#destin-location-other-tick:checked").length && !$("#destin-location-other").val())
        {
            alert("Destination location is required.");
        }
        else if($("#destin-location-other-tick:checked").length == 0 && !$("#destin-location").val())
        {
            alert("Destination location is required.");
        }
        else if( !$("#passenger").val() )
        {
            alert("Number of passenger is required.");
        }
        else if( !$("#triptime").val() )
        {
            alert("Estimated trip time is required.");
        }
        else
        {
            if( $("#vehicle tbody .active").length )
            {
                 $("#vehicle tbody .active td:nth-child(1)").html( "<input type='hidden' name='pickup-date[]' value='" + $("#pickup-date").val() + "' />" +  $("#pickup-date").val() );
                 $("#vehicle tbody .active td:nth-child(2)").html( "<input type='hidden' name='pickup-time[]' value='" + $("#pickup-time").val() + "' />" + $("#pickup-time").val() );
              
                 $("#vehicle tbody .active td:nth-child(3)").html( "<input type='hidden' name='pickup-company[]' value='" + $("#pickup-company").val() + "' />\
                            <input type='hidden' name='pickup-company-other[]' value='" + ($("#pickup-company-other").val() + $("#pickup-company option:selected").text()) + "' />\
                            <input type='hidden' name='pickup-location[]' value='" + $("#pickup-location").val() + "' />\
                            <input type='hidden' name='pickup-location-other[]' value='" + ($("#pickup-location-other").val() + $("#pickup-location option:selected").text()) + "' />" + $.trim($("#pickup-company option:selected").text() + $("#pickup-company-other").val()) + " - " + $.trim($("#pickup-location option:selected").text() + $("#pickup-location-other").val()) );

                 $("#vehicle tbody .active td:nth-child(4)").html( "<input type='hidden' name='destin-company[]' value='" + $("#destin-company").val() + "' />\
                            <input type='hidden' name='destin-company-other[]' value='" + ($("#destin-company-other").val() + $("#destin-company option:selected").text()) + "' />\
                            <input type='hidden' name='destin-location[]' value='" + $("#destin-location").val() + "' />\
                            <input type='hidden' name='destin-location-other[]' value='" + ($("#destin-location-other").val()  + $("#destin-location option:selected").text())  + "' />" + $.trim($("#destin-company option:selected").text() + $("#destin-company-other").val()) + " - " + $.trim($("#destin-location option:selected").text() + $("#destin-location-other").val()) );
              
                 $("#vehicle tbody .active td:nth-child(5)").html( "<input type='hidden' name='passenger[]' value='" + $("#passenger").val() + "' />" + $("#passenger").val() );
                 $("#vehicle tbody .active td:nth-child(6)").html( "<input type='hidden' name='triptime[]' value='" + $("#triptime").val() + "' />" + $("#triptime").val() );         
            }
            else
            {
                $("#vehicle tbody").append("\
                    <tr>\
                        <td><input type='hidden' name='pickup-date[]' value='" + $("#pickup-date").val() + "' />" + $("#pickup-date").val() + "</td>\
                        <td><input type='hidden' name='pickup-time[]' value='" + $("#pickup-time").val() + "' />" + $("#pickup-time").val() + "</td>\
                        <td>\
                            <input type='hidden' name='pickup-company[]' value='" + $("#pickup-company").val() + "' />\
                            <input type='hidden' name='pickup-company-other[]' value='" + ($("#pickup-company-other").val() + $.trim($("#pickup-company option:selected").text()) ) + "' />\
                            <input type='hidden' name='pickup-location[]' value='" + $("#pickup-location").val() + "' />\
                            <input type='hidden' name='pickup-location-other[]' value='" + ($("#pickup-location-other").val()   + $.trim($("#pickup-location option:selected").text()) ) + "' />\
                            " + $.trim($("#pickup-company option:selected").text() + $("#pickup-company-other").val()) + " - " + $.trim($("#pickup-location option:selected").text() + $("#pickup-location-other").val()) + "\
                        </td>\
                        <td>\
                            <input type='hidden' name='destin-company[]' value='" + $("#destin-company").val() + "' />\
                            <input type='hidden' name='destin-company-other[]' value='" + ($("#destin-company-other").val()  + $.trim($("#destin-company option:selected").text()) ) + "' />\
                            <input type='hidden' name='destin-location[]' value='" + $("#destin-location").val() + "' />\
                            <input type='hidden' name='destin-location-other[]' value='" + ($("#destin-location-other").val()  + $.trim($("#destin-location option:selected").text()) ) + "' />\
                            " + $.trim($("#destin-company option:selected").text() + $("#destin-company-other").val()) + " - " + $.trim($("#destin-location option:selected").text() + $("#destin-location-other").val()) + "\
                        </td>\
                        <td><input type='hidden' name='passenger[]' value='" + $("#passenger").val() + "' />" + $("#passenger").val() + "</td>\
                        <td><input type='hidden' name='triptime[]' value='" + $("#triptime").val() + "' />" + $("#triptime").val() + "</td>\
                        <td></td>\
                        <td></td>\
                    </tr>");
            }

            $("button[name='edit']").attr("disabled",false);
            $("button[name='delete']").attr("disabled",false);

            $("#service-modal").modal('hide');
        }
    })

    $("#vehicle td").live("click",function()
    {
        $(this).parent().parent().children().removeClass("active");
        $("#vehicle tr td").css("background-color",""); 
        $(this).parent().children().css("background-color","rgb(177, 177, 177)");     
        $(this).parent().addClass("active");
    });

     $(".isNumeric").live("keypress",function(event)
      {
        return isNumeric(event);
      })

    function isNumeric(e) 
      {
          // Ensure that it is a number and stop the keypress
            if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
                e.preventDefault();
            }
      }

     $(".isDecimal").live("keypress",function(event)
      {
        return isDecimal(event);
      })

    function isDecimal(evt) 
      {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
              return false;
          }
          return true;
      }
    
});