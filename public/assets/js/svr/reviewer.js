$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var requests_my_table1 = $('#myrecord1').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },  
                  { "sClass": "td_center", "bSortable": false },      
                ]        
               }); 

    requests_my_table1.fnSort( [[0,"desc"]]); 

    var requests_my_table2 = $('#myrecord2').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },  
                  { "sClass": "td_center", "bSortable": false },      
                ]        
               });  

    requests_my_table2.fnSort( [[0,"desc"]]); 

    var requests_my_table3 = $('#myrecord3').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },  
                  { "sClass": "td_center", "bSortable": false },      
                ]        
               }); 

    requests_my_table3.fnSort( [[0,"desc"]]); 

    $("#myrecord3_info").empty().html("<input class='select-all' id='stats-SERVED' type='checkbox' value='' /> <label class='labels'>SELECT ALL</label> : <input class='btn btn-xs' type='submit' value='DELETE' name='action' />");

    var requests_my_table4 = $('#myrecord4').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },     
                  { "sClass": "hide", "bSortable": true },  
                  { "sClass": "td_center", "bSortable": false },      
                ]        
               }); 

    requests_my_table4.fnSort( [[0,"desc"]]); 

    $("#myrecord4_info").empty().html("<input class='select-all' id='stats-CANCELLED' type='checkbox' value='' /> <label class='labels'>SELECT ALL</label> : <input class='btn btn-xs' type='submit' value='DELETE' name='action' />");



    $(".select-all").live("click",function()
    {
       $("." + $(this).attr("id")).prop("checked" ,$(this).attr("checked") ? true : false);
     
    });

    $.fn.dataTableExt.afnFiltering = new Array();
    var rec1 = requests_my_table1.fnGetData();
    var rec2 = requests_my_table2.fnGetData();
    var rec3 = requests_my_table3.fnGetData();
    var rec4 = requests_my_table4.fnGetData();


     $('.department1 , .company1').change( function() 
     {
        requests_my_table1.fnClearTable();
        $(rec1).each(function(val,index)
        {
            if( (!$(".department1").val() || $(".department1").val() == index[5]) && (!$(".company1").val() || $(".company1").val() == index[6]) )
                requests_my_table1.fnAddData([ index ]);  
        });        
         requests_my_table1.fnDraw();
    });


     

     $('.department2 , .company2').change( function() 
     {
        requests_my_table2.fnClearTable();
        $(rec2).each(function(val,index)
        {
            if( (!$(".department2").val() || $(".department2").val() == index[5]) && (!$(".company2").val() || $(".company2").val() == index[6]) )
                requests_my_table2.fnAddData([ index ]);  
        });        
         requests_my_table2.fnDraw();
    });

      $('.department3 , .company3').change( function() 
     {
        requests_my_table3.fnClearTable();
        $(rec3).each(function(val,index)
        {
            if( (!$(".department3").val() || $(".department3").val() == index[5]) && (!$(".company3").val() || $(".company3").val() == index[6]) )
                requests_my_table3.fnAddData([ index ]);  
        });        
         requests_my_table3.fnDraw();
    });

     $('.department4 , .company4').change( function() 
     {
        requests_my_table4.fnClearTable();
        $(rec4).each(function(val,index)
        {
            if( (!$(".department4").val() || $(".department4").val() == index[5]) && (!$(".company4").val() || $(".company4").val() == index[6]) )
                requests_my_table4.fnAddData([ index ]);  
        });        
         requests_my_table4.fnDraw();
    });

     $(".company1").change(function()
    {
        var type = $(this).val()
        $(".department1").val("");
        $(".department1 option").each(function()
        {
            if(($(this).attr("comp") == type) || !$(this).attr("comp") || !type )
                $(this).removeClass("hide");
            else
                $(this).addClass("hide");

        })
    });

     $(".company2").change(function()
    {
        var type2 = $(this).val()
        $(".department2").val("");
        $(".department2 option").each(function()
        {
            if(($(this).attr("comp") == type2) || !$(this).attr("comp") || !type2 )
                $(this).removeClass("hide");
            else
                $(this).addClass("hide");

        })
    });

     $(".company3").change(function()
    {
        var type3 = $(this).val()
        $(".department3").val("");
        $(".department3 option").each(function()
        {
            if(($(this).attr("comp") == type3) || !$(this).attr("comp") || !type3 )
                $(this).removeClass("hide");
            else
                $(this).addClass("hide");

        })
    });

     $(".company4").change(function()
    {
        var type4 = $(this).val()
        $(".department4").val("");
        $(".department4 option").each(function()
        {
            if(($(this).attr("comp") == type4) || !$(this).attr("comp") || !type4 )
                $(this).removeClass("hide");
            else
                $(this).addClass("hide");

        })
    });

});