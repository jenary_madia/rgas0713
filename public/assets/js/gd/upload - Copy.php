$(function () 
{
	var file_counter = 0;
	var file_counter2 = 0;
	var alerts = 0;
	
	if($('#fileupload-attachment').length)
	{
		$("#fileupload-attachment").click(function()
		{
			alerts = 0;
		})
		$('#fileupload-attachment').fileupload({
			dataType: 'json',
			done: function (e, data) {

				console.log(e);
				console.log(data);
				$.each(data.result.files, function (index, file) 
				{
					kb = parseFloat(file.filesize) / 1024 / 1000;
					if(max_filesize && max_filesize < kb + parseFloat(totalsize()) )
					{
						alert("Sorry, can’t attach file. File exceeds attachment limit of " + max_filesize + "MB.");
						return false;
					}
					else if(max_upload && $(".attachment-filesize").length >= max_upload)
					{
						if(alerts==0)
						{
							alert("Maximum attachment is " + max_upload + " only");
							alerts = 1;
						}
						
					}
					else
					{
						row_attachment = "<p>";
						row_attachment += file.original_filename + " " + (parseFloat(file.filesize) / 1024 ).toFixed(1) + "KB";
						row_attachment += "<input class='attachment-filesize' type='hidden' name='files["+file_counter+"][filesize]' value='"+file.filesize+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
						row_attachment += " <button attach-name='" + file.original_filename + "' class='btn btn-sm btn-danger remove-fn'>DELETE</button>";
						row_attachment += "</p>";
						
						$("#attachments").append(row_attachment);
						file_counter += 1;
					}					
				});
			}
		});
	}

	if($('#fileupload-signatory').length)
	{
		$('#fileupload-signatory').fileupload({
			dataType: 'json',
			done: function (e, data) 
			{
				$.each(data.result.files, function (index, file) 
				{
					kb = parseFloat(file.filesize) / 1024 / 1000;
					if(20 < kb + parseFloat(totalsize()) )
					{
						alert("Maximum file size is 20 mb");
					}
					else if($(".signatory-file").length)
					{
						alert("Accept one document only.");
					}
					else if(file.mime_type != "application/pdf")
					{
						alert("Accept pdf format only.");
					}
					else
					{
						
						row_attachment = "<p>";
						row_attachment += file.original_filename + " " + (parseFloat(file.filesize) / 1024).toFixed(1) + "KB";
						row_attachment += "<input class='signatory-file' type='hidden' name='files-signatory["+file_counter2+"][filesize]' value='"+file.filesize+"'>";
						row_attachment += "<input type='hidden' name='files-signatory["+file_counter2+"][mime_type]' value='"+file.mime_type+"'>";
						row_attachment += "<input type='hidden' name='files-signatory["+file_counter2+"][original_extension]' value='"+file.original_extension+"'>";
						row_attachment += "<input type='hidden' name='files-signatory["+file_counter2+"][original_filename]' value='"+file.original_filename+"'>";
						row_attachment += "<input type='hidden' name='files-signatory["+file_counter2+"][random_filename]' value='"+file.random_filename+"'>";
						row_attachment += " <button attach-name='" + file.original_filename + "' class='btn btn-sm btn-danger remove-fn remove-signature'>DELETE</button>";
						row_attachment += "</p>";
						
						$("#attachments-signatory").append(row_attachment);
						file_counter2 += 1;
					}
					
				});
			}
		});
	}

	$(".remove-fn").live("click",function()
    {
    	var con = confirm("Are you sure you want to delete " + $.trim($(this).attr("attach-name")) + "?");
    	if(!con) return false;

        if($(this).attr("attach-id"))
        {
           var id = $(this).attr("attach-id"); 
           $(this).parent().parent().append("<input type='hidden' value='" + id + "' name='attachment_id[]' />");    
           $(this).parent().parent().append("<input type='hidden' value='" + $(this).attr("attach-name") + "' name='attachment_name[]' />");                 
       }
        
        $(this).parent().remove();
    });

	function totalsize()
	{
		var size = 0;
		var kb = 0;
		$(".attachment-filesize").each(function()
		{
			kb = parseFloat($(this).val()) / 1024 / 1000;
			size = size + kb;
		})

		return size;
	}
});