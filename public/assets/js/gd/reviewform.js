var base_url = $("base").attr("href");

$(function () 
{
        "use strict";

        $("select[name='doc_type']").change(function()
        {
            if($(this).val()==4)
                $("input[name='doc_others']").attr("disabled",false);
            else
                $("input[name='doc_others']").attr("disabled",true);
        });

        $(".remove-fn").live("click",function()
        {
            if($(this).attr("attach-id"))
            {
               var id = $(this).attr("attach-id"); 
               $(this).parent().parent().append("<input type='text' value='" + id + "' name='attachment_id[]' />");
               $(this).parent().parent().append("<input type='text' value='" + $(this).attr("attach-name") + "' name='attachment_name[]' />");                 
           }
            
            $(this).parent().remove();
        });
});