$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var forapproval = $('#reviewer_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true   , 
				"aoColumns": [
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center hide", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },			
					{ "sClass": "td_center", "bSortable": true },			
					{ "sClass": "td_center", "bSortable": false },			
				]      
                }); 

//   
 forapproval.fnSort( [[7,"desc"] ,[1,"desc"] ]); 

   var my =  $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  , 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center hide", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },	
				{ "sClass": "td_center hide", "bSortable": false },			
			]      
		}); 

  my.fnSort( [[1,"desc"]]); 
  
});