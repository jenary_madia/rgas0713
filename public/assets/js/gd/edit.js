var base_url = $("base").attr("href");

$(function () 
{
        "use strict";

        $(".employee").chosen({width:"199px"});

        $("button[name='action'][value='send']").click(function(e)
        {
            if(!$("input[name='reference']").val())
            {
                alert("Please indicate the reference number.");
                e.preventDefault();
            }                
            else if(!$("select[name='doc_type']").val())
            {
                alert("Please select a document type.");
                e.preventDefault();
            }
            else if($("select[name='doc_type']").val() == 4 && !$("input[name='doc_others']").val() )
            {
                alert("Please indicate other document type.");
                e.preventDefault();
            }
            else if(!$("input[name='doc_name']").val())
            {
                alert("Please indicate document name.");
                e.preventDefault();
            }
            else if(!$("input[name='txtPrepared[]']").length && !$("input[name='txtReviewed[]']").length && !$("input[name='txtRecommended[]']").length && !$("input[name='txtAcknowledged[]']").length && !$("input[name='txtApproved[]']").length)
            {
                alert("Please add Signatory.");
                e.preventDefault();
            }
             else if(!$("input[name='txtPrepared[]']").length)
            {
                alert("At least one preparer signatory is required.");
                e.preventDefault();
            }
            else if(!$(".remove-signature").length)
            {
                alert("Please attach document for signature.");
                e.preventDefault();
            }
        })

        $("#btnPrepared").click(function()
        {
            if($("#prepared").val())
            {
                var id = $("#prepared").val();
                var name = $("#prepared option:selected").text();
                if(id == 1) $(".confidential").removeClass("hide");

                $("#prepared").val("");
                $('.employee#prepared').trigger("chosen:updated");
               if($("#btnPrepared").parent().siblings(":nth-child(4)").text())
               {
                    var sort = $(".spanPrepared").length + 1;
                    var tbl = "<tr>\
                            <td colspan='3' ></td>\
                            <td class='signatory_list' > <span class='spanPrepared'>" + sort + "</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanPrepared' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>\
                            <td>\
                            <div><select class='employee form-control' name='Prepared_" + id + "' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtPrepared[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div></td>\
                        </tr>";

                    $("#reviewed").parent().parent().before(tbl);
                }
                else
                {
                    $("#btnPrepared").parent().next().append(" <span class='spanPrepared' >1</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanPrepared'  ><i class='fa fa-times-circle fa-xs faplus'></i></button>");
                    $("#btnPrepared").parent().next().next().append("<div><select name='Prepared_" + id + "' class='employee form-control' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtPrepared[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div>");
                }
                $(".employee").chosen({width:"199px"});
            }
            else
            {
                alert("Employee is required.")
            }
        });

        $("#btnReviewed").click(function()
        {
            if($("#reviewed").val())
            {
                var id = $("#reviewed").val();
                var name = $("#reviewed option:selected").text();
                if(id == 1) $(".confidential").removeClass("hide");

                $("#reviewed").val("");
                $('.employee#reviewed').trigger("chosen:updated");
               if($("#btnReviewed").parent().siblings(":nth-child(4)").text())
               {
                    var sort = $(".spanReviewed").length + 1;
                    var tbl = "<tr>\
                            <td colspan='3' ></td>\
                            <td class='signatory_list' > <span class='spanReviewed'>" + sort + "</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanReviewed' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>\
                            <td>\
                            <div><select class='employee form-control' name='Reviewed_" + id + "' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtReviewed[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div></td>\
                        </tr>";

                    $("#recommended").parent().parent().before(tbl);
                }
                else
                {
                    $("#btnReviewed").parent().next().append(" <span class='spanReviewed' >1</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanReviewed'  ><i class='fa fa-times-circle fa-xs faplus'></i></button>");
                    $("#btnReviewed").parent().next().next().append("<div><select name='Reviewed_" + id + "' class='employee form-control' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtReviewed[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div>");
                }
                $(".employee").chosen({width:"199px"});
            }
            else
            {
                alert("Employee is required.")
            }
        });

        $("#btnRecommended").click(function()
        {
            if($("#recommended").val())
            {
                var id = $("#recommended").val();
                var name = $("#recommended option:selected").text();
                if(id == 1) $(".confidential").removeClass("hide");

                $("#recommended").val("");
                $('.employee#recommended').trigger("chosen:updated");
               if($("#btnRecommended").parent().siblings(":nth-child(4)").text())
               {
                    var sort = $(".spanRecommended").length + 1;
                    var tbl = "<tr>\
                            <td colspan='3' ></td>\
                            <td class='signatory_list' > <span class='spanRecommended'>" + sort + "</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanRecommended' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>\
                            <td>\
                            <div><select class='employee form-control' name='Recommended_" + id + "' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtRecommended[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div></td>\
                        </tr>";

                    $("#acknowledged").parent().parent().before(tbl);
                }
                else
                {
                    $("#btnRecommended").parent().next().append(" <span class='spanRecommended' >1</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanRecommended'  ><i class='fa fa-times-circle fa-xs faplus'></i></button>");
                    $("#btnRecommended").parent().next().next().append("<div><select name='Recommended_" + id + "' class='employee form-control' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtRecommended[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div>");
                }
                $(".employee").chosen({width:"199px"});
            }
            else
            {
                alert("Employee is required.")
            }
        });

        $("#btnAcknowledged").click(function()
        {
            if($("#acknowledged").val())
            {
                var id = $("#acknowledged").val();
                var name = $("#acknowledged option:selected").text();
                if(id == 1) $(".confidential").removeClass("hide");

                $("#acknowledged").val("");
                $('.employee#acknowledged').trigger("chosen:updated");
               if($("#btnAcknowledged").parent().siblings(":nth-child(4)").text())
               {
                    var sort = $(".spanAcknowledged").length + 1;
                    var tbl = "<tr>\
                            <td colspan='3' ></td>\
                            <td class='signatory_list' > <span class='spanAcknowledged'>" + sort + "</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanAcknowledged' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>\
                            <td>\
                            <div><select class='employee form-control' name='Acknowledged_" + id + "' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtAcknowledged[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div></td>\
                        </tr>";

                    $("#approved").parent().parent().before(tbl);
                }
                else
                {
                    $("#btnAcknowledged").parent().next().append(" <span class='spanAcknowledged' >1</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanAcknowledged'  ><i class='fa fa-times-circle fa-xs faplus'></i></button>");
                    $("#btnAcknowledged").parent().next().next().append("<div><select name='Acknowledged_" + id + "' class='employee form-control' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtAcknowledged[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div>");
                }
                $(".employee").chosen({width:"199px"});
            }
            else
            {
                alert("Employee is required.")
            }
        });

        $("#btnApproved").click(function()
        {
            if($("#approved").val())
            {
                var id = $("#approved").val();
                var name = $("#approved option:selected").text();
                if(id == 1) $(".confidential").removeClass("hide");

                $("#approved").val("");
                $('.employee#approved').trigger("chosen:updated");
               if($("#btnApproved").parent().siblings(":nth-child(4)").text())
               {
                    var sort = $(".spanApproved").length + 1;
                    var tbl = "<tr>\
                            <td colspan='3' ></td>\
                            <td class='signatory_list' > <span class='spanApproved'>" + sort + "</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanApproved' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>\
                            <td>\
                            <div><select class='employee form-control' name='Approved_" + id + "' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtApproved[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' /></div><div class='signatory_list' ></div></td>\
                        </tr>";

                    $("#approved").parent().parent().parent().append(tbl);
                }
                else
                {
                    $("#btnApproved").parent().next().append(" <span class='spanApproved' >1</span>. " + name + " <button type='button' name='' class='delete-row' data-span='spanApproved'  ><i class='fa fa-times-circle fa-xs faplus'></i></button>");
                    $("#btnApproved").parent().next().next().append("<div><select name='Approved_" + id + "' class='employee form-control' >\
                                                <option value=''></option>" + emp_rec + "</select>\
                                        <input type='hidden' name='txtApproved[]' value='" + id + "' />\
                             <input type='button' value='ADD' class='notify' id ='" + id + "' /></div><div class='signatory_list' ></div>");
                }
                $(".employee").chosen({width:"199px"});
            }
            else
            {
                alert("Employee is required.")
            }
        });


        $(".notify").live("click",function()
        {
            var id = $(this).siblings("select").val();
            var name = $(this).siblings("select").children("option:selected").text();
            var sel_name = $(this).siblings("select").attr("name");

            $(this).siblings("select").val("");
            $(this).siblings("select").trigger("chosen:updated");            

            $(this).parent().addClass("hide");
            $(this).parent().next().empty().append(name + " <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>\
                <input type='hidden' name='" + sel_name + "' value='" + id + "' /><input type='checkbox' name='notify_" + $(this).attr("id") + "' value='1' /> VIEW DOCUMENT");
        });

        $(".remove-fn").live("click",function()
            {
                if($(this).attr("attach-id"))
                {
                   var id = $(this).attr("attach-id"); 
                   $(this).parent().parent().append("<input type='hidden' value='" + id + "' name='attachment_id[]' />");
                   $(this).parent().parent().append("<input type='hidden' value='" + $(this).attr("attach-name") + "' name='attachment_name[]' />");                 
               }
                
                $(this).parent().remove();
            });

        $(".delete-notify").live("click",function()
        {
            $(this).parent().prev().removeClass("hide");
            $(this).parent().empty();
        });

         $(".delete-row").live("click",function()
        {
            if($(this).parent().siblings(":nth-child(1)").text())
            {
                if(!$(this).parent().parent().next().children(":nth-child(1)").text())
                {
                    var row4 = $(this).parent().parent().next().children(":nth-child(2)").html();
                    var row5 = $(this).parent().parent().next().children(":nth-child(3)").html();   
                    $(this).parent().parent().next().remove(); 
                }

                if($(this).attr("id") == 1) $(".confidential").addClass("hide");

               $(this).parent().siblings(":nth-child(5)").empty().append(row5); 
               $(this).parent().empty().append(row4);              
            }
            else
                $(this).parent().parent().remove();

            $("." + $(this).attr("data-span") ).each(function(e)
            {
                $(this).text(e + 1);
            });


        });

        $("select[name='doc_type']").change(function()
        {
            if($(this).val()==4)
                $("input[name='doc_others']").attr("disabled",false);
            else
                $("input[name='doc_others']").attr("disabled",true);
        });

         $(".isNumeric").live("keypress",function(event)
              {
                return isNumeric(event);
              })

            function isNumeric(evt) 
              {
                  evt = (evt) ? evt : window.event;
                  var charCode = (evt.which) ? evt.which : evt.keyCode;
                  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
                      return false;
                  }
                  return true;
              }
});

