$(function () 
{
	var file_counter = 1;
	var alerts = 0;
	if($('#fileupload').length)
	{
		$("#fileupload").click(function()
		{
			alerts = 0;
		})


		var sum_filesize = 0;
		$('#fileupload').fileupload({
			beforeSend: function(xhr, settings) {
				
				// if($('.attachment_filesize').length){
				// 	sum_filesize = 0;
				// 	$('.attachment_filesize').each(function() {
				// 		sum_filesize += Number($(this).val());
				// 	});
				// 	console.log(sum_filesize);
				// }
				
				// if($('#attachments p').length){
				// 	file_counter = $('#attachments p').length + 1;
				// }
				// else{
				// 	file_counter = 1;
				// }

			
            },
			dataType: 'json',
			done: function (e, data) {
				console.log(e);
				console.log(data);
				file_counter = $('#attachments p').length + 1;
				
				$.each(data.result.files, function (index, file) {
					console.log(index);
					console.log(file);
					soon_sum_filesize = sum_filesize + file.filesize;
					console.log(soon_sum_filesize);


					if(soon_sum_filesize > allowed_total_filesize){
						alert('File cannot be upload because it will exceed the total allowed filesize.');
						return false;
					}
					else if(file_counter > allowed_file_count )
					{
						if(alerts==0)
						{
							alerts = 1;
							alert('You have already exceeded to the number allowed of attachments.');		
							return false;						
						}					
					}
					else
					{
						row_attachment = "<p>";
						row_attachment += file.original_filename + ' | ' + convert_size(file.filesize);
						row_attachment += "<input type='hidden' class='attachment_filesize' name='files["+file_counter+"][filesize]' value='"+file.filesize+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
						row_attachment += "<input type='hidden' name='files["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
						row_attachment += " <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>";
						row_attachment += "</p>";
						// $("#attachments").append("<p>"+file.original_filename+"<input type='hidden' name='fn' value='"+file.original_filename+"'> <button class='btn btn-sm btn-danger remove-fn'>Delete</button></p>");
						$("#attachments").append(row_attachment);
						file_counter += 1;
					}
				});
			}
		});
	}
});