$(document).ready(function()
{
    "use strict";


    var base_url = $("base").attr("href");
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });
    
   var my = $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  ,
               "aoColumns": [
                { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },   
                  { "sClass": "td_center " },     
                  { "sClass": "td_center ", "bSortable": false }
                  ]
                }); 

   my.fnSort( [[0,"desc"]]); 

    var requests_my_table1 = $('#myrecord1').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     ,
               "aoColumns": [
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center"},     
                  { "sClass": "td_center " },     
                  { "sClass": "td_center ", "bSortable": false },  
                  { "sClass": "td_center", "bSortable": false },   
                ]  
               }); 

    requests_my_table1.fnSort( [[0,"desc"]]); 

     var requests_my_table2 = $('#myrecord2').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true     ,
               "aoColumns": [
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center"},     
                  { "sClass": "td_center " },     
                  { "sClass": "td_center ", "bSortable": false },  
                  { "sClass": "td_center", "bSortable": false },   
                ]  
               }); 

     requests_my_table2.fnSort( [[1,"desc"]]); 


    $.fn.dataTableExt.afnFiltering = new Array();
    var rec1 = requests_my_table1.fnGetData();
    var rec2 = requests_my_table2.fnGetData();

    $('.department1 , .company1').change( function() 
     {
        requests_my_table1.fnClearTable();
        $(rec1).each(function(val,index)
        {
            if( (!$(".department1").val() || $(".department1").val() == index[5]) && (!$(".company1").val() || $(".company1").val() == index[4]) )
                requests_my_table1.fnAddData([ index ]);  
        });        
         requests_my_table1.fnDraw();
    });

    $('.department2 , .company2').change( function() 
     {
        requests_my_table2.fnClearTable();
        $(rec2).each(function(val,index)
        {
            if( (!$(".department2").val() || $(".department2").val() == index[5]) && (!$(".company2").val() || $(".company2").val() == index[4]) )
                requests_my_table2.fnAddData([ index ]);  
        });        
         requests_my_table2.fnDraw();
    });

});