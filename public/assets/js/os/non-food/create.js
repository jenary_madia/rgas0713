$(function()
{
    "use strict";
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });   

    $("button[name='action'][value='save']").click(function(e)
        {
            try
            {
                $(".date_picker").each(function()
                {
                    if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                })                   
            }
            catch(e)
            {
               alert("Invalid inputted date");
               return false;
            }

            if(!$("input[name='owner']").val())
            {
                alert("Project Owner is required.");
                e.preventDefault();
            }                
            else if(!$("input[name='desc']").val())
            {
                alert("Product Name is required.");
                e.preventDefault();
            }
            else if(!$("input[name='selling_from']").val())
            {
                alert("Selling Price From is required.");
                e.preventDefault();
            }
             else if(!$("input[name='selling_to']").val())
            {
                alert("Selling Price To is required.");
                e.preventDefault();
            }
            else if(!$("input[name='allowable[]']:checked").length)
            {
                alert("Allowable Payment Terms is required.");
                e.preventDefault();
            } 
            else if(!$("input[name='maximum']").val())
            {
                alert("Maximum Order Limit is required.");
                e.preventDefault();
            }                
            else if(!$("input[name='item-code[]']").length)
            {
                alert("At least one item is required.");
                e.preventDefault();
            }       
        })

    $("button[name='add']").click(function()
    {        
        $("#item-code").val("");
        $("#item-desc").val("");
        $("#uom-code").val("");
        $("#selling-price").val("");
        $("#item-status").val(1);

        $("#myModalLabel").text("ADD ITEM");
        $("#product-modal").modal('show');
    });

    $("#save").click(function()
    {
        if( !$("#item-code").val() )
        {
            alert("Item Code is required.");
        }
        else if( $("#item-code").val().replace(/[^0-9]/g,"").length != 2 || $("#item-code").val().toLowerCase().replace(/[^a-z]/g,"").length != 4)
        {
            alert("Item Code format should be: (4 alphabetic characters)(2 numerical characters)");
        }
        else if( !$("#item-desc").val() )
        {
            alert("Item Description is required.");
        }
        else if(!$("#uom-code").val())
        {
            alert("UOM Code is required.");
        }
        else if(!$("#selling-price").val())
        {
            alert("Selling price is required.");
        }
        else if(!$("#item-status").val())
        {
            alert("Status is required.");
        }
        else
        {
            var x = 0;
            $("input[name='item-code[]']").each(function(i)
            {
                if($(this).val() == $("#item-code").val() && x > 0)
                {
                    alert("Item code should not be duplicated.");
                    return false;
                    exit();
                }
                else if($(this).val() == $("#item-code").val() )
                {
                    x = x+ 1;
                }
            });   

            //f( $("#item tbody .active").length )
            if($("#myModalLabel").text() != "ADD ITEM")
            {
                 $("#item tbody .active td:nth-child(1)").html( "<input type='hidden' name='item-code[]' value='" + $("#item-code").val() + "' />" +  $("#item-code").val() );
                 $("#item tbody .active td:nth-child(2)").html( "<input type='hidden' name='item-desc[]' value='" + $("#item-desc").val() + "' />" + $("#item-desc").val() );
                 $("#item tbody .active td:nth-child(3)").html( "<input type='hidden' name='uom-code[]' value='" + $("#uom-code").val() + "' />" + $("#uom-code option:selected").text() );
                 $("#item tbody .active td:nth-child(4)").html( "<input type='hidden' name='selling-price[]' value='" + $("#selling-price").val() + "' />" + $("#selling-price").val() );
                 $("#item tbody .active td:nth-child(5)").html( "<input type='hidden' name='item-status[]' value='" + $("#item-status").val() + "' />" + $("#item-status option:selected").text() );         
            }
            else
            {
                $("#item tbody").append("\
                    <tr>\
                        <td><input type='hidden' name='item-code[]' value='" + $("#item-code").val() + "' />" + $("#item-code").val() + "</td>\
                        <td><input type='hidden' name='item-desc[]' value='" + $("#item-desc").val() + "' />" + $("#item-desc").val() + "</td>\
                        <td><input type='hidden' name='uom-code[]' value='" + $("#uom-code").val() + "' />" + $("#uom-code option:selected").text() + "</td>\
                        <td><input type='hidden' name='selling-price[]' value='" + $("#selling-price").val() + "' />" + $("#selling-price").val() + "</td>\
                        <td><input type='hidden' name='item-status[]' value='" + $("#item-status").val() + "' />" + $("#item-status option:selected").text() + "</td>\
                    </tr>");
            }

            $("#product-modal").modal('hide');
        }
    });

    $("#item td").live("dblclick",function()
    {
        $(this).parent().parent().children().removeClass("active");
        $("#item tr td").css("background-color",""); 
        $(this).parent().children().css("background-color","rgb(177, 177, 177)");     
        $(this).parent().addClass("active");

        $("#item-code").val( $("#item .active input[name='item-code[]']").val() );
        $("#item-desc").val( $("#item .active input[name='item-desc[]']").val() );
        $("#uom-code").val( $("#item .active input[name='uom-code[]']").val() );
        $("#selling-price").val( $("#item .active input[name='selling-price[]']").val() );
        $("#item-status").val( $("#item .active input[name='item-status[]']").val() );

        $("#myModalLabel").text("EDIT ITEM");
        $("#product-modal").modal('show');
    });

     $(".isDecimal").live("keypress",function(event)
      {
        return isDecimal(event);
      })

        function isDecimal(evt) 
      {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
              return false;
          }
          return true;
      }

});