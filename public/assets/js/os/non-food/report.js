$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var requests_my_table = $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':false  ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },   
                  { "sClass": "hide", "bSortable": true }
                ]    
                    }); 

      $( ".dataTables_filter input[type='text']" ).autocomplete({  source: available_tags  ,
                select: function(event,ui) {
                    setTimeout(function()
                    {
                      $( ".dataTables_filter input[type='text']" ).keyup();
                    },500);                  
                  } 
                });

    

   // $("#myrecord_length").html("<input type='button' class='btn btn-sm' value='ADD PROJECT' onClick='window.location=\"" + base_url + "/os/non-food/create" + "\"' />");

    $('#myrecord tr').live("mouseover",function()
    {
    	$(this).css("cursor" , "pointer");
    });
    $('#myrecord tr').live("dblclick",function()
    {
    	var id = $(this).children(":nth-child(4)").text();
    	window.location = base_url + "/os/non-food/edit/" + id;
    })	
});