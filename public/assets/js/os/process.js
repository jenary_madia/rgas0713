$(function()
{
    "use strict";



    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

     $("button[name='action'][value='close']").click(function(e)
        {
            if(!$("select[name='payment_type']").val() )
            {
                alert("Payment Type is required.");
                return false;
            }
            
            if(!$("input[name='payment_date']").val() )
            {
                alert("Payment Date is required.");
                return false;
            }

            if(!$("input[name='payment_amount']").val() )
            {
                alert("Payment Amount is required.");
                return false;
            }
            if($("input[name='payment_amount']").val() < 1)
            {
                alert("Payment Amount should be greater than 0.");
                return false;
            }
            if($("select[name='payment_type']").val() && $("select[name='payment_type']").val() != "Cash" && !$("input[name='payment_reference_num']").val() )
            {
                alert("Payment Reference No. is required.");
                return false;
            }


            if( parseFloat($("input[name='payment_amount']").val()) != parseFloat($("#total-amount").val()) && !$("input[name='payment_remarks']").val() )
            {
                alert("Payment Remarks is required.");
                return false;
            }


        });


    $(".isNumeric").live("keypress",function(event)
    {
      return isNumeric(event);
    })

    function isNumeric(e) 
      {
          // Ensure that it is a number and stop the keypress
            if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
                e.preventDefault();
            }
      }

     $(".isDecimal").live("keypress",function(event)
      {
        return isDecimal(event);
      })

    function isDecimal(evt) 
      {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
              return false;
          }
          return true;
      }
    
});