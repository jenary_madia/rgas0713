$(function()
{
	var cmb = $("#uom-cmb").parent().html();

    $("button[name='action'][value='save']").click(function(e)
        {

            if(!$("input[name='code']").val())
            {
                alert("Code is required.");
                e.preventDefault();
            }                
            else if(!$("input[name='name']").val())
            {
                alert("Product Name is required.");
                e.preventDefault();
            }
            else if(!$("select[name='company']").val())
            {
                alert("Company is required.");
                e.preventDefault();
            }
             else if(!$("select[name='measure']").val())
            {
                alert("Default UOM is required.");
                e.preventDefault();
            }
            else if(!$("select[name='status']").val())
            {
                alert("Status is required.");
                e.preventDefault();
            }    
            else
            {
                $("select[name='uom[]']").each(function(i)
                {
                    if(!$(this).val())
                    {
                        alert("UOM " + (i + 1) + " is required.");
                        e.preventDefault();
                        exit();
                    }
                });   

                $("input[name='factory[]']").each(function(i)
                {
                    if(!$(this).val())
                    {
                        alert("Factory Price " + (i + 1) + " is required.");
                        e.preventDefault();
                        exit();
                    }
                });   

                $("input[name='selling[]']").each(function(i)
                {
                    if(!$(this).val())
                    {
                        alert("Selling Price " + (i + 1) + " is required.");
                        e.preventDefault();
                        exit();
                    }

                });       
            }            
        })

	$("#add-uom").click(function()
	{
		$("#uom-list").append('<div class="clear_10"></div>\
                    <div class="row_form_container">\
                        <div class="col1_form_container">\
                            <label class="labels text-danger">UOM ' + ($("select[name='uom[]']").length + 1 )+ ' :</label>\
                        </div>\
                        <div class="col2_form_container uom-container">\
                        </div>\
                    </div>\
					\
                    <div class="clear_10"></div>\
                    <div class="row_form_container">\
                        <div class="col1_form_container">\
                            <label class="labels text-danger">FACTORY PRICE:</label>\
                        </div>\
                        <div class="col2_form_container">\
                            <input type="text" class="form-control isDecimal" name="factory[]" />\
                        </div>\
                    </div>\
                    <div class="row_form_container">\
                        <div class="col1_form_container">\
                            <label class="labels text-danger">SELLING PRICE:</label>\
                        </div>\
                        <div class="col2_form_container">\
                            <input type="text" class="form-control isDecimal" name="selling[]" value="" />\
                        </div>\
                    </div>');

		$(".uom-container").append( cmb);
		$(".uom-container").removeClass("uom-container");		
	});


     $(".isDecimal").live("keypress",function(event)
      {
        return isDecimal(event);
      })

        function isDecimal(evt) 
      {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
              return false;
          }
          return true;
      }
});