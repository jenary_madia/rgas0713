$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });
    
    var requests_my_table1 = $('#myrecord1').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false     ,
               "aoColumns": [
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center" },
                  { "sClass": "td_center"},     
                  { "sClass": " hidden" },     
                  { "sClass": " hidden", "bSortable": false },  
                  { "sClass": "td_center", "bSortable": false },   
                ]  
               }); 

    requests_my_table1.fnSort( [[0,"desc"]]); 

    var requests_my_table2 = $('#myrecord2').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false ,
               "aoColumns": [
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },     
                  { "sClass": "td_center ", "bSortable": false },     
                  { "sClass": "td_center", "bSortable": false },  
                  { "sClass": "td_center hidden", "bSortable": false },   
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },
                  { "sClass": "td_center", "bSortable": false },     
                  { "sClass": "td_center", "bSortable": false },     
                  { "sClass": "td_center", "bSortable": false },  
                  { "sClass": "td_center", "bSortable": false },      
                  { "sClass": "td_center", "bSortable": false }
                ]  
               });  

  requests_my_table2.fnSort( [[2,"desc"]]); 


    $.fn.dataTableExt.afnFiltering = new Array();
    var rec1 = requests_my_table1.fnGetData();
    var rec2 = requests_my_table2.fnGetData();

    $('.department1 , .company1').change( function() 
     {
        requests_my_table1.fnClearTable();
        $(rec1).each(function(val,index)
        {
            if( (!$(".department1").val() || $(".department1").val() == index[5]) && (!$(".company1").val() || $(".company1").val() == index[4]) )
                requests_my_table1.fnAddData([ index ]);  
        });        
         requests_my_table1.fnDraw();
    });

    var total_out = 0;
    $('.department2 , .company2').change( function() 
     {
        total_out = 0;
        requests_my_table2.fnClearTable();
        $(rec2).each(function(val,index)
        {
            if( (!$(".department2").val() || $(".department2").val() == index[7]) && (!$(".company2").val() || $(".company2").val() == index[6]) )
            {
              requests_my_table2.fnAddData([ index ]);  
              total_out = (total_out) + parseFloat(index[15].replace(",","") ? index[15].replace(",","") : 0);
              //alert(index[15].replace(",",""));
            }
        });        
         requests_my_table2.fnDraw();
         $("#total_out").val(total_out.toFixed(2));
    });

    $("#myrecord2_filter input").live("keyup",function()
    {
         // total_out = 0;
         // var newrec = 
         //  $( requests_my_table2.fnFilter( $(this).val() ).fnGetData() ).each(function(val,index)
         //  {
         //   
         //  });

         // 

         total_out = 0;
         requests_my_table2.$('tr', {"filter":"applied"}).each( function () 
         {
            total_out = (total_out) + parseFloat($(this).find("td:eq(15)").text().replace(",","") ? $(this).find("td:eq(15)").text().replace(",","") : 0);
         });

         $("#total_out").val(total_out.toFixed(2));
       //  alert( total_out.toFixed(2) );
    });

});