$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false   , 
				"aoColumns": [
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },
					{ "sClass": "td_center", "bSortable": true },			
					{ "sClass": "td_center", "bSortable": true },			
					{ "sClass": "td_center", "bSortable": false },			
				]    ,
				"bFilter" :false 
                }); 


    $('#sub_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  , 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },			
			]       ,
				"bFilter" :false 
		}); 

    var requests_my_table1 = $('#approval_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  , 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center", "bSortable": false },	
				{ "sClass": "td_center hide", "bSortable": false },			
			]   ,
				"bFilter" :false     
		}); 

    $.fn.dataTableExt.afnFiltering = new Array();
    var rec1 = requests_my_table1.fnGetData();

    $('.department1').change( function() 
     {
      
        requests_my_table1.fnClearTable();
        $(rec1).each(function(val,index)
        {
            if( (!$(".department1").val() || $(".department1").val().indexOf( index[6] ) > -1 )  )
                requests_my_table1.fnAddData([ index ]);  
        });        
         requests_my_table1.fnDraw();
    });

    $('.department1').multiselect({
        includeSelectAllOption: true,
        enableFiltering:false,
        enableCaseInsensitiveFiltering:false ,
        nonSelectedText : "",
        numberDisplayed:0,
        buttonWidth : "100%",
        templates : {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" style="    height: 35px;"><span class="multiselect-selected-text"></span> <b class="caret" style="float:right" ></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item multiselect-filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
            }
        });

});

