var base_url = $("base").attr("href");
$(function()
{
	$("button[name='action']").live("click",function()
	{
		if(!$("#year-covered").val())
		{
			alert("Year covered is required.");
			return false;
		}

		if( $("input[name='for']:checked").val() == "sub" && !$("select[name='sub']").val() )
		{
			alert("Employee is required.");
			return false;
		}

		if( $("input[name='encode']:checked").val() == "copy" && $("input[name='for']:checked").val() == "sub" && !$("select[name='copy_sub']").val() )
		{
			alert("Employee is required.");
			return false;
		}

		var link = "";
		if($("input[name='for']:checked").val() == "sub" )
			link = base_url + "/pmf/check/sub/" + $("#year-covered").val() + "/" + $("input[name='purpose']:checked").val() + "/" + $("select[name='sub']").val() + ( $("input[name='encode']:checked").val() == "copy" ? "/1/" + $("input[name='content']:checked").val() + "/" +  $("select[name='copy_sub']").val() : "");
		else
			link = base_url + "/pmf/check/" + $("#year-covered").val() + "/" + $("input[name='purpose']:checked").val() +  ( $("input[name='encode']:checked").val() == "copy" ? "/1/" + $("input[name='content']:checked").val()  : "");

		$.get(link , {}, function(data)
		{
			if(data.result == 0)
			{
				alert(data.message);   
			}
			else
			{
				if($("input[name='for']:checked").val() == "sub" )
					 window.location = base_url + "/pmf/create/sub/" + $("#year-covered").val() + "/" + $("input[name='purpose']:checked").val() + "/" + $("select[name='sub']").val() + ( $("input[name='encode']:checked").val() == "copy" ? "/1/" + $("input[name='content']:checked").val() + "/" +  $("select[name='copy_sub']").val() : "");
				else
					 window.location = base_url + "/pmf/create/" + $("#year-covered").val() + "/" + $("input[name='purpose']:checked").val() +  ( $("input[name='encode']:checked").val() == "copy" ? "/1/" + $("input[name='content']:checked").val()  : "");
			}

        }, "json");

	});

	$("#year-covered").live("change",function()
	{
		 $(".purpose input[type='radio']").attr("checked" , false);
		 $(".purpose").addClass("hide");
		 $(".purpose.pmf_" + $(this).val() ).removeClass("hide");

//		  $(".purpose.pmf_" + $(this).val() ).children().children("input[name='purpose'][value='" + $(this).val() + "']").attr("checked",true);
		  $(".purpose.pmf_" + $(this).val() ).children().children("input[name='purpose'][purpose='Yearly Evaluation']").prop("checked",true)
	});

	$("input[name='for']").live("click",function()
	{
		if($(this).val() == "sub")
		{
			$(".purpose").removeClass("hidden");
			$("select[name='sub']").attr("disabled",false);	
			if($("input[name='encode']:checked").val() == "copy" )
				$("select[name='copy_sub']").attr("disabled",false);	
		}
		else
		{
			$(".purpose").addClass("hidden");
			$("select[name='sub']").attr("disabled",true);	
			$("select[name='copy_sub']").attr("disabled",true);	
		}
	});

	$("input[name='encode']").live("click",function()
	{
		if($(this).val() == "copy")
		{
			$("input[name='content']").attr("disabled",false);
			if($("input[name='for']:checked").val() == "sub" )
				$("select[name='copy_sub']").attr("disabled",false);	
		}
		else
		{
			$("input[name='content']").attr("disabled",true);
			$("select[name='copy_sub']").attr("disabled",true);	
		}
	})
});