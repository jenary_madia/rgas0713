$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

   var requests_my_table1 =   $('#approval_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  , 
			"aoColumns": [
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },			
				{ "sClass": "td_center ", "bSortable": false },		
				{ "sClass": "td_center hide", "bSortable": false },			
			]      ,
				"bFilter" :false 
		}); 


    if($('#pmf_record').length)
    {
       var requests_my_table2 = $('#pmf_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':false,
               'iDisplayLength' : 10,
               'bInfo':false  , 
            "aoColumns": [
                { "sClass": "td_center", "bSortable": true },
                { "sClass": "td_center", "bSortable": true },
                { "sClass": "td_center", "bSortable": true },
                { "sClass": "td_center", "bSortable": true },
                { "sClass": "td_center", "bSortable": true },           
                { "sClass": "td_center", "bSortable": false },  
                { "sClass": "td_center hide", "bSortable": false }, 
                { "sClass": "td_center hide", "bSortable": false },         
            ]    ,
                "bFilter" :false   
        }); 

        var rec2 = requests_my_table2.fnGetData(); 
    }
    



     $.fn.dataTableExt.afnFiltering = new Array();
    var rec1 = requests_my_table1.fnGetData();
    

    $('.department1').change( function() 
     {
        requests_my_table1.fnClearTable();
        $(rec1).each(function(val,index)
        {
            if( (!$(".department1").val() || $(".department1").val().indexOf( index[6] ) > -1 )  )
                requests_my_table1.fnAddData([ index ]);  
        });        
         requests_my_table1.fnDraw();
    });

    $('.department1, .form-dropdown').multiselect({
        includeSelectAllOption: true,
        enableFiltering:false,
        enableCaseInsensitiveFiltering:false ,
        nonSelectedText : "All",
        allSelectedText : "selected",
        numberDisplayed:0,
        buttonWidth : "100%",
        templates : {
                button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown" style="    height: 35px;"><span class="multiselect-selected-text"></span> <b class="caret" style="float:right" ></b></button>',
                ul: '<ul class="multiselect-container dropdown-menu"></ul>',
                filter: '<li class="multiselect-item multiselect-filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
                filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
                li: '<li><a tabindex="0"><label></label></a></li>',
                divider: '<li class="multiselect-item divider"></li>',
                liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
            }
        });

    $('#filter').click( function() 
     {
        requests_my_table2.fnClearTable();
        $(rec2).each(function(val,index)
        {
            if( ( (!$("select[name='department']").val() || $("select[name='department']").val().indexOf( index[6] ) > -1 )) &&
            	( (!$("select[name='year']").val() || $("select[name='year']").val().indexOf( index[3] ) > -1 )) &&
            	( (!$("select[name='status']").val() || $("select[name='status']").val().indexOf( index[4] ) > -1 )) &&
            	( (!$("select[name='purpose']").val() || $("select[name='purpose']").val().indexOf( index[7] ) > -1 ))  )
                requests_my_table2.fnAddData([ index ]);  
        });        
         requests_my_table2.fnDraw();
    });

});