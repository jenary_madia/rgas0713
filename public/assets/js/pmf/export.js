$(function()
{
	"use strict";


	$("button[name='action'][value='export']").click(function(e)
    {
    	if(!$("#date_from").val())
    	{
    		alert("Start Date is required.")
    		return false;
    	}

    	if(!$("#date_to").val())
    	{
    		alert("End Date is required.")
    		return false;
    	}

    	if(!$("input[name='purpose[]']:checked").length )
    	{
    		alert("At least one purpose is required.")
    		return false;
    	}

    });
});