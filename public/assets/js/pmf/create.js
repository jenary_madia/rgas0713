$(function()
{
	"use strict";


	$("body").append("<input type='hidden' id='base' value='" + $("base").attr("href") + "' />");
	$("base").remove();

	$( "#tabs" ).tabs();
	$( "#tabs" ).removeClass("hide");


	$(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

	bindFileUpload();

    $("button[name='action'][value='send']  , button[name='action'][value='head'] , button[name='action'][value='hr']").click(function(e)
    {


    	if(!$("textarea[name='kra[]']").length)
    	{
    		alert("KRA is required.")
    		return false;
    	}
    	else if($("textarea[name='kra[]']").length > 5)
    	{
    		alert("Maximum of 5 KRA’s only.")
    		return false;
    	}

    	$(".add-kpi").each(function(a)
    	{
    		var attr_kpi = $(this).attr("kpi");
    		if(!$(".remove_kpi_" + attr_kpi).length)
    		{
    			alert("KPI is Required to add at least one per KRA.")
    			e.preventDefault();
    			return false;
    		}
    		else if($(".remove_kpi_" + attr_kpi).length > 3 )
    		{
    			alert("Maximum of 3 KPI’s for every KRA.")
    			e.preventDefault();
    			return false;
    		}
    		
    	});    	

    	$("input[name='kpi_index[]']").each(function(index , a)
    	{
    		if(!$(this).siblings("input[type='checkbox']:checked").length)
    		{
    			alert("General Measure is required.")
    			e.preventDefault();
    			return false;
    		}


    		if( !$(this).parent().siblings(":nth-child(5)").children("input[type='radio']:checked").length && !$(this).parent().siblings(":nth-child(6)").children("input[type='radio']:checked").length  )
    		{
    			alert("Actual Performance YES/NO is required.");
    			e.preventDefault();
    			return false;
    		}

    		if( !$(this).parent().siblings(":nth-child(8)").children("input[type='text']").val()   )
    		{
    			alert("KPI Weight is required.");
    			e.preventDefault();
    			return false;
    		}
    		else if($(this).parent().siblings(":nth-child(8)").children("input[type='text']").val() < 5 )
			{
				alert("Minimum of 5 weight is required.");
				e.preventDefault();
    			return false;
			}

			if( $(this).parent().siblings(":nth-child(9)").children("input[type='text']").val()   )
    		{
    			if($(this).parent().siblings(":nth-child(9)").children("input[type='text']").val() > 105 || $(this).parent().siblings(":nth-child(9)").children("input[type='text']").val() < 70)
				{
					alert("RATING (SELF) MUST RANGE ONLY FROM 70.00 TO 105.00");
					e.preventDefault();
    				return false;
				}
    		}

    		if( !$(this).parent().siblings(":nth-child(10)").children("input[type='text']").val()   )
    		{
    			if(!$(this).parent().siblings(":nth-child(10)").children("input[type='text']").attr("disabled"))
    			{
    				alert("Superior rate is required.");
	    			e.preventDefault();
	    			return false;
    			}
    		}
    		else
    		{
    			if( (!$(this).parent().siblings(":nth-child(10)").children("input[type='text']").attr("disabled")) && ($(this).parent().siblings(":nth-child(10)").children("input[type='text']").val() > 105 || $(this).parent().siblings(":nth-child(10)").children("input[type='text']").val() < 70))
				{
					alert("RATING (SUPERIOR) MUST RANGE ONLY FROM 70.00 TO 105.00");
					e.preventDefault();
	    			return false;
				}
    		}


    		if( (index + 1) ==  $("input[name='kpi_index[]']").length)
    		{

    			if(parseFloat($("#total-weight").val()) != parseFloat(80))
		    	{
		    		alert("TOTAL WEIGHT MUST BE 80.00");
		    		e.preventDefault();
		    		return false;
		    	}


    			$(".incident").each(function(index2, a)
		    	{
		    		if(!$(this).val())
		    		{
		    			alert("Critical Incident is required.")
		    		 	e.preventDefault();
		    		 	return false;
		    		}

		    		if( (index2 + 1) ==  $(".incident").length)
    				{
    					$(".rating-consistent").each(function(index3 , a)
				    	{
				    		if(!$(this).attr("disabled"))
				    		{
				    			if( !$("input[name='core-rate[" + $(this).attr("id") + "]']:checked").length )
					    		{
					    			alert("Key actions is required.")
					    		 	e.preventDefault();
					    		 	return false;
					    		}	
				    		}

				    		if( (index3 + 1) ==  $(".rating-consistent").length)
    						{
    							if(!$("#discipline-demerit").val())
						    	{
						    		alert("Demerit is required.")
								 	e.preventDefault();
								 	return false;
						    	}
						    	else if(!$("#compliance-rating").val())
						    	{
						    		alert("Compliance rating is required.")
								 	e.preventDefault();
								 	return false;
						    	}
						    	else  if( ($("#equivalent-rating").val() == "A" || $("#equivalent-rating").val() == "A+") &&  !$(".remove-fn").length)
						        {
						            alert("At least one file need to be attached.");
						            e.preventDefault();
						            return false;
						        }
    						}
					    		
				    	});    	
    				}

		    	});
    		}
    	
    	});


    	
    	$(".recommended:checked").each(function(a)
    	{
    		if(!$(this).parent().siblings().children("input").val())
    		{
    			alert($(this).siblings("label").text() + " is required.");
    			e.preventDefault();
    			return false;
    		}    
            else if($(this).parent().siblings().children("input").attr("type") == "text" && $(this).parent().siblings().children("input").val().length < 3)
            {
                alert($(this).siblings("label").text() + " has min of 3 characters.");
                e.preventDefault();
                return false;
            }		
            else if($(this).parent().siblings().children("input").attr("type") == "text" && $(this).parent().siblings().children("input").val().length > 200)
            {
                alert($(this).siblings("label").text() + " has max of 200 characters.");
                e.preventDefault();
                return false;
            }       
    	});    


	   
    
    });

    $(".remove-kra").live("click",function()
    {
    	$(".kpi_specific_" + $(this).siblings("input[name='kpi[]']").val() ).parent().parent().remove();
    	$(this).parent().parent().next().remove();
    	$(this).parent().parent().remove();  

    	var total_weight = 0;
		$(".kpi-weight").each(function()
		{
			total_weight = total_weight + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weight").val( total_weight );

		var total_weighted = 0;
		$(".kpi-weighted").each(function()
		{
			total_weighted = total_weighted + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weighted").val( total_weighted );
		$("#overall-objective").val( total_weighted );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();  	
    })


    $(".remove_kpi").live("click",function()
    {
    	$(this).parent().parent().remove();

    	var total_weight = 0;
		$(".kpi-weight").each(function()
		{
			total_weight = total_weight + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weight").val( total_weight );

		var total_weighted = 0;
		$(".kpi-weighted").each(function()
		{
			total_weighted = total_weighted + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weighted").val( total_weighted );
		$("#overall-objective").val( total_weighted );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();
    })

	var kpi = $(".remove-kra").length;
	var kpi_detail = $(".remove-kpi").length; ;

	$(".add-cluster").live("click",function()
	{

		var index = $( ".add-cluster" ).index( this );

		if($( ".add-cluster:eq(" + (index + 1) + ")" ).length)
		{
			$( ".add-cluster:eq(" + (index + 1) + ")" ).parent().parent().before("<tr> \
                    	<td colspan='11' class='pm_tbl_labels'> \
                    		<button type='buttton' name='expand' class='remove-kra' kpi='" + kpi++ + "' ><i class='fa fa-minus-circle fa-xs faplus'></i></button> \
                    		<input type='hidden' name='kpi[]' value='" + kpi + "' > \
                    		<input type='hidden' name='cluster[]' value='" + $(this).attr("cluster") + "' > \
                    		<textarea class='kpi-textarea' rows='3' name='kra[]' maxlength='5000' ></textarea> \
                    	</td> \
                    	</tr> \
                    	<tr> \
                    	<td></td> \
                    	<td><button type='buttton' name='expand' class='add-kpi' kpi='" + kpi + "' ><i class='fa fa-plus-circle fa-xs faplus'></i></button></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	</tr>");
		}
		else
		{
			$(".pmf-total_weigh").parent().parent().before("<tr> \
                    	<td colspan='11' class='pm_tbl_labels'> \
                    		<button type='buttton' name='expand' class='remove-kra' kpi='" + kpi++ + "' ><i class='fa fa-minus-circle fa-xs faplus'></i></button> \
                    		<input type='hidden' name='kpi[]' value='" + kpi + "' > \
                    		<input type='hidden' name='cluster[]' value='" + $(this).attr("cluster") + "' > \
                    		<textarea class='kpi-textarea' rows='3' name='kra[]' maxlength='5000' ></textarea> \
                    	</td> \
                    	</tr> \
                    	<tr> \
                    	<td></td> \
                    	<td><button type='buttton' name='expand' class='add-kpi' kpi='" + kpi + "' ><i class='fa fa-plus-circle fa-xs faplus'></i></button></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	<td></td> \
                    	</tr>");
		}
		

		return false;
	})


	$(".add-kpi").live("click",function()
	{
		var kpi_id = $(this).attr("kpi");
		kpi_detail++;
		var disable_sup_rate = $(".rating-consistent:disabled").length ? "disabled" : "";
		 
		if($(".remove_kpi_" + kpi_id).length)
		{ 
			$(".remove_kpi_" + kpi_id + ":eq(-1)").parent().parent().after("<tr>\
                    	<td></td>\
                    	<td class='pm_tbl_labels'>\
                    	<input type='hidden' name='kpi_index[]' value='" + kpi_detail + "' />\
                    	<button type='buttton' name='expand' class='remove_kpi remove_kpi_" + kpi_id + "'><i class='fa fa-minus-circle fa-xs faplus'></i></button>\
                    	<input type='checkbox' value=1 name='kpi_quality_" + kpi_id + "[" + kpi_detail + "]' />\
                    	Quality (QL)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_efficient_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Cost Efficient (CE)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_quantity_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Quantity (QL)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_timeliness_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Timeliness (T)\
                    	</td>\
                    	<td class='pm_tbl_labels' ><textarea rows='3' maxlength='5000' class='kpi-textarea  kpi_specific_" + kpi_id + "' name='kpi_specific_" + kpi_id + "[" + kpi_detail + "]' ></textarea></td>\
                    	<td class='pm_tbl_labels' >\
                            <span class='btn btn-default btn-sm  fileinput-button'>\
                            <span >BROWSE</span>\
                                <input class='fileupload-kpi' kpi='" + kpi_id + "' index='" + kpi_detail + "' type='file' name='attachments[]' data-url='" + $("#date-url").val() + "' multiple>\
                            </span>\
                            <div id='attachments-kpi_" + kpi_id + "_" + kpi_detail + "'></div>\
                        </td>\
                    	<td align='center'><input type='radio' value=1 name='kpi_target_" + kpi_id + "[" + kpi_detail + "]'  /></td>\
                    	<td align='center'><input type='radio' value=0 name='kpi_target_" + kpi_id + "[" + kpi_detail + "]' /></td>\
                    	<td class='pm_tbl_labels' ><textarea class='kpi-textarea' rows='3' maxlength='5000' name='kpi_other_" + kpi_id + "[" + kpi_detail + "]' ></textarea></td>\
                    	<td align='center'><input type='text' name='kpi_weight_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-weight isDecimal' /></td>\
                    	<td align='center'><input type='text' name='kpi_self_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-self isDecimal' /></td>\
                    	<td align='center'><input type='text' " + disable_sup_rate + " name='kpi_superior_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-superior isDecimal' /></td>\
                    	<td align='center'><input readonly type='text' name='kpi_weighted_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-weighted' /></td>\
                    	</tr>");
		}
		else
		{
			$(this).parent().parent().after("<tr>\
                    	<td></td>\
                    	<td class='pm_tbl_labels'>\
                    	<input type='hidden' name='kpi_index[]' value='" + kpi_detail + "' />\
                    	<button type='buttton' name='expand' class='remove_kpi remove_kpi_" + kpi_id + "'><i class='fa fa-minus-circle fa-xs faplus'></i></button>\
                    	<input type='checkbox' value=1 name='kpi_quality_" + kpi_id + "[" + kpi_detail + "]' />\
                    	Quality (QL)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_efficient_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Cost Efficient (CE)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_quantity_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Quantity (QL)<br>\
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='checkbox' value=1 name='kpi_timeliness_" + kpi_id + "[" + kpi_detail + "]'  />\
                    	Timeliness (T)\
                    	</td>\
                    	<td class='pm_tbl_labels' ><textarea  rows='3' maxlength='5000' class='kpi-textarea kpi_specific_" + kpi_id + "' name='kpi_specific_" + kpi_id + "[" + kpi_detail + "]' ></textarea></td>\
                    	<td class='pm_tbl_labels' >\
                            <span class='btn btn-default btn-sm  fileinput-button'>\
                            <span >BROWSE</span>\
                                <input class='fileupload-kpi' kpi='" + kpi_id + "' index='" + kpi_detail + "' type='file' name='attachments[]' data-url='" + $("#date-url").val() + "' multiple>\
                            </span>\
                            <div id='attachments-kpi_" + kpi_id + "_" + kpi_detail + "'></div>\
                        </td>\
                    	<td align='center'><input type='radio' value=1 name='kpi_target_" + kpi_id + "[" + kpi_detail + "]'  /></td>\
                    	<td align='center'><input type='radio' value=0 name='kpi_target_" + kpi_id + "[" + kpi_detail + "]' /></td>\
                    	<td class='pm_tbl_labels' ><textarea class='kpi-textarea' rows='3' maxlength='5000' name='kpi_other_" + kpi_id + "[" + kpi_detail + "]' ></textarea></td>\
                    	<td align='center'><input type='text' name='kpi_weight_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-weight isDecimal' /></td>\
                    	<td align='center'><input type='text' name='kpi_self_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-self isDecimal' /></td>\
                    	<td align='center'><input type='text' " + disable_sup_rate + " name='kpi_superior_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-superior isDecimal' /></td>\
                    	<td align='center'><input readonly type='text' name='kpi_weighted_" + kpi_id + "[" + kpi_detail + "]' style='width:40px' class='kpi-weighted' /></td>\
                    	</tr>");
		}

		

		bindFileUpload();
		return false;
	})

	$(".kpi-weight").live("keyup",function()
	{
		var weighted = parseFloat($(this).val() ? $(this).val() : 0) * (parseFloat($(this).parent().siblings().children(".kpi-superior").val() ? $(this).parent().siblings().children(".kpi-superior").val()  : 0) / 100);
		$(this).parent().siblings().children(".kpi-weighted").val( weighted );

		var total_weight = 0;
		$(".kpi-weight").each(function()
		{
			total_weight = total_weight + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weight").val( total_weight );

		var total_weighted = 0;
		$(".kpi-weighted").each(function()
		{
			total_weighted = total_weighted + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weighted").val( total_weighted );
		$("#overall-objective").val( total_weighted );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();
	});

	$("#discipline-demerit").live("keyup",function()
	{
		if($(this).val() > 5 || $(this).val() < 0)
		{
			$(this).val("");
			alert("Allowed demerit value is range from 0-5.");;
		}
	});

	$("#compliance-rating").live("keyup",function()
	{
		if($(this).val() > 100 || $(this).val() < 0)
		{
			$(this).val("");
			alert("Allowed rating value is range from 0-100.");;
		}
	});

	

	$(".kpi-superior").live("keyup",function()
	{
		var weighted = parseFloat($(this).parent().siblings().children(".kpi-weight").val() ? $(this).parent().siblings().children(".kpi-weight").val() : 0) * (parseFloat($(this).val() ? $(this).val() : 0) / 100);
		$(this).parent().siblings().children(".kpi-weighted").val( weighted );

		var total_weighted = 0;
		$(".kpi-weighted").each(function()
		{
			total_weighted = total_weighted + parseFloat($(this).val() ? $(this).val() : 0);
		});

		$("#total-weighted").val( total_weighted );
		$("#overall-objective").val( total_weighted );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();
	});

	$(".rating-consistent , .rating-occasional , .rating-not").live("click",function()
	{
		var item = ($(".rating-consistent:checked").length * 2) + ($(".rating-occasional:checked").length * 1);
		var rate = ( item / 50) * (1-0.7) + 0.7;
		$("#behavior-rating").val( ( rate * 100 ).toFixed(2)  );


		if(rate * 100 >= 95)
		{
			$("#behavior-qualitative").val( "ROLE MODELLING" );
		}
		else if(rate * 100 < 85)
		{
			$("#behavior-qualitative").val( "PROGRESSING" );
		}
		else
		{
			$("#behavior-qualitative").val( "DEMONSTRATING" );
		}

		$("#behavior-score").val( ( (rate * 100)* 0.1 ).toFixed(2) );


		$("#total-behavior").val(( parseFloat($("#behavior-score").val() ? $("#behavior-score").val() : 0) + parseFloat($("#discipline-score").val() ? $("#discipline-score").val() : 0) + parseFloat($("#compliance-score").val() ? $("#compliance-score").val() : 0) ).toFixed(2));

		$("#overall-behavior").val(( parseFloat($("#behavior-score").val() ? $("#behavior-score").val() : 0) + parseFloat($("#discipline-score").val() ? $("#discipline-score").val() : 0) + parseFloat($("#compliance-score").val() ? $("#compliance-score").val() : 0) ).toFixed(2));
		$("#practice-core").val( ( (rate * 100)* 0.1 ).toFixed(2) );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();
	});

	$("#discipline-demerit , #compliance-rating").live("keyup",function()
	{
		$("#discipline-score").val(  $("#discipline-weight").val()  -  parseFloat($("#discipline-demerit").val() ? $("#discipline-demerit").val() : 0) );
		$("#compliance-score").val( ($("#compliance-weight").val() / 100)  *  parseFloat($("#compliance-rating").val() ? $("#compliance-rating").val() : 0) );

		$("#total-behavior").val(( parseFloat($("#behavior-score").val() ? $("#behavior-score").val() : 0) + parseFloat($("#discipline-score").val() ? $("#discipline-score").val() : 0) + parseFloat($("#compliance-score").val() ? $("#compliance-score").val() : 0) ).toFixed(2));
		$("#overall-behavior").val(( parseFloat($("#behavior-score").val() ? $("#behavior-score").val() : 0) + parseFloat($("#discipline-score").val() ? $("#discipline-score").val() : 0) + parseFloat($("#compliance-score").val() ? $("#compliance-score").val() : 0) ).toFixed(2) );
		$("#summary-discipline").val(  $("#discipline-weight").val()  -  parseFloat($("#discipline-demerit").val() ? $("#discipline-demerit").val() : 0) );
		$("#summary-compliance").val( ($("#compliance-weight").val() / 100)  *  parseFloat($("#compliance-rating").val() ? $("#compliance-rating").val() : 0) );

		$("#total-score").text( parseFloat($("#overall-behavior").val() ? $("#overall-behavior").val() : 0)  + parseFloat($("#overall-objective").val() ? $("#overall-objective").val() : 0) );
		e_rating();
	});

	$(".isDecimal").live("keypress",function(event)
      {
        return isDecimal(event);
      })

	$(".recommended").live("click",function()
	{
		$(this).parent().siblings().children("input").attr("disabled" , ($(this).attr("checked") ? false : true ));
		$(this).parent().siblings().children("input").val("");
	});

    function isDecimal(evt) 
      {
          evt = (evt) ? evt : window.event;
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
              return false;
          }
          return true;
      }

	function e_rating()
	{
		var x = $("#total-score").text();
		if(x >= 100.01)
		{
			$("#equivalent-rating").val( "A+" );
		}
		else if(x >= 95)
		{
			$("#equivalent-rating").val( "A" );
		}
		else if(x >= 90)
		{
			$("#equivalent-rating").val( "B+" );
		}
		else if(x >= 85)
		{
			$("#equivalent-rating").val( "B" );
		}
		else if(x >= 80)
		{
			$("#equivalent-rating").val( "C+" );
		}
		else if(x >= 75)
		{
			$("#equivalent-rating").val( "C" );
		}
		else
		{
			$("#equivalent-rating").val( "D" );
		}
	}

	var file_counter3 = 0;
	function bindFileUpload()
	{
		$('.fileupload-kpi').fileupload({
			dataType: 'json',
			done: function (e, data) 
			{
				var attr_kpi_id = ($(this).attr("kpi"));
				var attr_kpi_index = ($(this).attr("index"));
				var row_attachment = "";
				$.each(data.result.files, function (index, file) 
				{						
						row_attachment = "<p>";
						row_attachment += file.original_filename + " " + (parseFloat(file.filesize) / 1024).toFixed(1) + "KB";
						row_attachment += "<input class='signatory-file' type='hidden' name='files_" + attr_kpi_id + "[" + attr_kpi_index + "]["+file_counter3+"][filesize]' value='"+file.filesize+"'>";
						row_attachment += "<input type='hidden' name='files_" + attr_kpi_id + "[" + attr_kpi_index + "]["+file_counter3+"][mime_type]' value='"+file.mime_type+"'>";
						row_attachment += "<input type='hidden' name='files_" + attr_kpi_id + "[" + attr_kpi_index + "]["+file_counter3+"][original_extension]' value='"+file.original_extension+"'>";
						row_attachment += "<input type='hidden' name='files_" + attr_kpi_id + "[" + attr_kpi_index + "]["+file_counter3+"][original_filename]' value='"+file.original_filename+"'>";
						row_attachment += "<input type='hidden' name='files_" + attr_kpi_id + "[" + attr_kpi_index + "]["+file_counter3+"][random_filename]' value='"+file.random_filename+"'>";
						row_attachment += " <button attach-name='" + file.original_filename + "' class='btn btn-sm btn-danger remove-fn remove-signature'>DELETE</button>";
						row_attachment += "</p>";
						
						$("#attachments-kpi_" + attr_kpi_id + "_" + attr_kpi_index).append(row_attachment);
						file_counter3 += 1;
				});
			}
		});
	}
})