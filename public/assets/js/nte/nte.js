var get_month_name;
$(document).ready(function() 
{
  "use strict";

  rename_attachments();
  appendMonthOptions(year, month);
  appendYearOptions();
  appendDayOptions(day);

  $( function() 
  {
    var dateFormat = "yy-mm-dd",
      from = $( "#sus_from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
        })
        .on( "change", function()
        {
          to.datepicker( "option", "minDate", getDate( this ) );
          getDateDiff(false);
        }),

      to = $( "#sus_to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
        getDateDiff(true);
      });
 
    function getDate( element ) 
    {
      var date;
      try 
      {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } 
      catch( error ) 
      {
        date = null;
      }
      return date;
    }
  });

  function appendMonthOptions(year, month)
  {
    var now = new Date();
    var end_now = 0;

    $('#month option').remove();
    if (now.getFullYear() == year)
      end_now = now.getMonth() + 2;
    else
      end_now = 13;

    for (var m = 1; m < end_now; m++)
      $('#month').append('<option value="' + ( m < 10 ? '0' + m : m ) + '" ' + ( month == m ? "selected" : "" ) + '>' + get_month_name(m) + '</option>');
  }

  function appendDayOptions(lD) 
  {
    $('#day option').remove();

    var cM = new Date($("#month").val()+'-01-'+$("#year").val());
    var d = new Date(cM.getFullYear(), cM.getMonth()+1, 0);
    var cD = d.getDate();
    $('#day').append('<option value=""></option>');
    for (var j = 1; j < cD + 1; j++) 
    {
      $('#day').append('<option value="' + (j < 10 ? '0' + j : j) + '"' + (lD == j ? 'selected' : '') + '>' + (j < 10 ? '0' + j : j) + '</option>');
    }
  }

  function appendYearOptions() 
  {
    for (var i = cY; i >= 1980; i--) 
    {
      $('#year').append('<option value="' + i + '"' + (year == i ? "selected" : '') +'>'+i+'</option>');
    }
  }

  function get_month_name(month) 
  {
    var month_name;

    month == '01' ? month_name = 'January' : '';
    month == '02' ? month_name = 'February' : '';
    month == '03' ? month_name = 'March' : '';
    month == '04' ? month_name = 'April' : '';
    month == '05' ? month_name = 'May' : '';
    month == '06' ? month_name = 'June' : '';
    month == '07' ? month_name = 'July' : '';
    month == '08' ? month_name = 'August' : '';
    month == '09' ? month_name = 'September' : '';
    month == '10' ? month_name = 'October' : '';
    month == '11' ? month_name = 'November' : '';
    month == '12' ? month_name = 'December' : '';

    return month_name;
  }

  $(document).on('change', '#month', function () 
  {
    appendDayOptions($('#day').val());
  });

  $(document).on('change', '#year', function () 
  {
    appendMonthOptions($(this).val(), $('#month').val());
    appendDayOptions($('#day').val());
  });

  $(document).on('click', '.sus_from', function() 
  {
    $("#sus_from").focus();
  });

  $(document).on('click', '.sus_to', function() 
  {
    $("#sus_to").focus();
  });

  function getDateDiff(isAlert) 
  {
    if ($("#sus_from").val() != '' && $("#sus_to").val() != '') 
    {
      var start = $('#sus_from').datepicker('getDate');
      var end = $('#sus_to').datepicker('getDate');
      var days = (end - start)/1000/60/60/24;

      if (days < 0) 
      {
        isAlert ? alert("PLEASE SELECT DATE IN \'TO\' FIELD NOT EARLIER THAN THE INPUTTED DATE IN \'FROM\'' FIELD") : '';
        $('#sus_to').val($("#sus_from").val());
        $("#sus_no_days").val(1);
      }
      else
        $("#sus_no_days").val((days+1)+'.00');
    }
  }

  $("#create_nda").on('click', function() {
    $("#bool").val(1);
    document.nte_returned_form.action = urlNdaCreate;
    $("#nte_returned_form").submit();
  });

  $("#save_returned").on('click', function () 
  {
    action = 6;
    $("#nte_returned_form").submit();
  });

  $("#send_chrd_aer").on('click', function () 
  {
    action = 7;
    $("#nte_returned_form").submit();
  });

  $("#save_dh").on('click', function () 
  {
    action = 8;
    $("#nte_returned_form").submit();
  });

  $("#send_dh").on('click', function () 
  {
    action = 9;
    $("#nte_returned_form").submit();
  });

  $("#return_er").on('click', function () 
  {
    action = 10;
    $("#nte_returned_form").submit();
  });

  $("#send_sv").on('click', function () 
  {
    action = 11;
    $("#nte_returned_form").submit();
  });

  $("#return_hr").on('click', function () 
  {
    action = 12;
    $("#nte_returned_form").submit();
  });

  $("#close").on('click', function () 
  {
    action = 13;
    $("#nte_returned_form").submit();
  });

  $("#cancel").on('click', function () 
  {
    action = 24;
    $("#nte_returned_form").submit();
  });  

  $("#print_nte").on('click', function() 
  {
    document.nte_returned_form.action = urlNtePrint;
    $("#nte_returned_form").submit();
  }); 

  $( "#nte_returned_form" ).submit(function( event ) 
  {
    if (stat === '8') 
    {

    }
    else
    {
      event.preventDefault();
      var sData = $( this ).serializeArray();
      var addArray = [
        {name : 'action', value : action},
        {name : '_token', value : token},
      ];
      
      $.ajax({
        method: 'POST',
        url: url,
        data: pushArray(sData, addArray)
      }).done( function(data) {
        var message = $.parseJSON(data);
          var msg_cont = document.getElementById('container_message');
          
          msg_cont.style.display = "block";
          $('.container_message').text("");
          
          if (message.scs) 
          {
            $("#container_message").removeClass('alert-danger');
            $("#container_message").addClass('alert-success');

            if (!(action == 14)) //6,8
            {
              //disable_all(true);
              //hide_all(true);
              window.location = urlNteListCHRD;
            }
            //stat == 7 ? location.reload() : '';
          }
          else 
          {
            $("#container_message").removeClass('alert-success');
            $("#container_message").addClass('alert-danger');
          }

          for (var i = 0; i < message.msg.length; i++) {            
            $('.container_message').append(message.msg[i].toUpperCase() + '<br>');
          };
      });
    }
  });

  function pushArray(origin, addArray)
  {
    for (var i = 0; i < addArray.length; i++) 
    {
      origin.push(addArray[i]);
    };

    return origin;
  }

  function disable_all(bool) 
  {
    var inputBox = document.getElementsByClassName('ib');
    for (var i = 0; i < inputBox.length; i++) 
    {
      bool ? inputBox[i].disabled = true : inputBox[i].disabled = false;
    };
  }

  function hide_all(bool) 
  {
    var btns = document.getElementsByClassName('btn');
    for (var i = 0; i < btns.length-1; i++) 
    {
      bool ? btns[i].className += ' hidden' : btns[i].className = 'btn btn-default btndefault';
    };
  }

  $("#head_act").on('change', function () 
  {
    hs_sus($(this).val());
  });

  $("#head_dec").on('change', function () 
  {
    var h_act = document.getElementsByClassName('act');
    for (var i = 0; i < h_act.length; i++) 
    {
      if ($(this).val() == 1) {
        hs_sus(0);
        document.getElementById('head_act').value = "";
        h_act[i].style.display = 'none';
      } 
      else 
      {
        h_act[i].style.display = 'block';
      }
    };
  });

  function hs_sus(val) 
  {
    var sus = document.getElementsByClassName('sus');
    for (var i = 0; i < sus.length; i++) 
    {
      if (val == 4) 
      {
        sus[i].style.display = 'block';
      } 
      else 
      {
        sus[i].style.display = 'none';
      }
    };
  }
});