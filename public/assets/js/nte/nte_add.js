var add_month_year;

$(document).ready(function () 
{

  "use strict";

  rename_row_uploads();
  appendMonthOptions(year_dd, month_dd);

  isCreate ? $("#department").val(0) : '';

  var udld = urlDownload.split('/');
  var newUrl = udld[0]+'//'+udld[2]+'/'+udld[3]+'/'+udld[4]+'/'+udld[5]+'/';

  /*$( ".emp" ).combobox({ select: function (event, ui) { get_tvy_emp(this); } });*/

  $(".emp").chosen({width:"100%"}).change(function () {
    get_tvy_emp(this);
  })

  initFileBrowse(".flup");
  
  $("#department").multiselect({
    open: function () 
    {
      $(".ui-multiselect-all").hide();
      $(".ui-multiselect-none").hide();
    },
    click: function(event, ui)
    {
      if (ui.checked)
      {
        get_drafted(ui.value);
        enable_buttons();

        var nte_tab = document.getElementById("nte_table_body");
        if (nte_tab.rows.length == 0)
          add_nte_row(null);
      }
      else
        if ($('tr.drafted-' + ui.value).length > 0)
          if (confirm('There are some items under this department do you want to remove them ?'))
          {
            $('tr.drafted-' + ui.value).remove();
            update_nte_row(ui.value);
          }
          else
            return false;
        else
          update_nte_row(ui.value);
    }
  }).multiselectfilter();

  function update_nte_row(department)
  {
    $('select.emp>option[dept="' + department + '"]').remove();
    $('.emp').trigger('chosen:updated');

    var l = employees_selection.length;

    for (var i = l - 1; i >= 0; i--) 
      if (parseInt(employees_selection[i].departmentid) == department)
        employees_selection.splice(i, 1);
  }

  Date.prototype.monthDays= function()
  {
    var d= new Date(this.getFullYear(), this.getMonth()+1, 0);
    return d.getDate();
  }

  function enable_buttons() 
  {
    var selectedDept = $('#department').val();

    if (selectedDept != '' )
      document.getElementById('add_nte').disabled = false;
    else 
    {
      document.getElementById('add_nte').disabled = true;
      document.getElementById('delete_nte').disabled = true;
    }
  }

  function get_month_name(month) 
  {
    var month_name;

    month == '01' ? month_name = 'January' : '';
    month == '02' ? month_name = 'February' : '';
    month == '03' ? month_name = 'March' : '';
    month == '04' ? month_name = 'April' : '';
    month == '05' ? month_name = 'May' : '';
    month == '06' ? month_name = 'June' : '';
    month == '07' ? month_name = 'July' : '';
    month == '08' ? month_name = 'August' : '';
    month == '09' ? month_name = 'September' : '';
    month == '10' ? month_name = 'October' : '';
    month == '11' ? month_name = 'November' : '';
    month == '12' ? month_name = 'December' : '';

    return month_name;
  }

  add_month_year = function add_month_year(day, lbl_date, tim) 
  {

    var dM = get_month_name($('#month_dd').val());
    var dY = $('#year_dd').val();

    var nte_tab = document.getElementById("nte_table_body");
    var last_row = nte_tab.rows.length;

    if (lbl_date === "" || lbl_date === null) 
      for (var b = 0; b < last_row; b++) 
      {
        var lbl_date = $($($($($($("#nte_table_body").children()[b]).children()[1]).children()[0]).children()[0]).children()[0]);
        var tim = $($($($("#nte_table_body").children()[b]).children()[3]).children()[0]);
        var date_comm = $($($($($("#nte_table_body").children()[b]).children()[1]).children()[0]).children()[1]);

        date_comm.val("");
        lbl_date.text(dM+' '+dY);
        tim.val('');
      }
    else 
      if (day === null || day === '') 
      {
        lbl_date.text(dM+' '+dY);
        tim.val('');
      } 
      else 
      {
        lbl_date.text(dM+' '+day+', '+dY);
        tim.val('1');
      }
  }

  function appendMonthOptions(year_dd, smonth)
  {
    var now = new Date();
    var end_now = 0;

    $('#month_dd option').remove();
    if (now.getFullYear() == year_dd)
      end_now = now.getMonth() + 2;
    else
      end_now = 13;

    for (var m = 1; m < end_now; m++)
      $('#month_dd').append('<option value="' + ( m < 10 ? '0' + m : m ) + '" ' + ( smonth == m ? "selected" : "" ) + '>' + get_month_name(m) + '</option>');
  }

  $("#year_dd").on('change', function() 
  {
    appendMonthOptions($(this).val(), $('#month_dd').val());
    add_month_year("", "", "");
    calculate_days_in_month();

    var nte_tab = document.getElementById("nte_table_body");
    var last_row = nte_tab.rows.length;

    for (var i = 0; i < last_row; i++) 
    {
      var emp_id = $($($($($($(nte_tab).children()[i]).children()).children()[0]).children()[0]).children()[1]).val().split('|')[0];
      var crr_id = $($($($(nte_tab).children()[i]).children()[2]).children()[0]).val();
      var tim = $($($(nte_tab).children()[i]).children()[4]).children();
      get_tvy(emp_id, crr_id, tim);
    };
  });

  $("#month_dd").on('change', function() 
  {
    add_month_year("", "", "");
    calculate_days_in_month();
  });

  $('#delete_nte').on('click', function() 
  {
    selected_to_delete(true, true);
  });

  $('#add_nte').on('click', function() 
  {
    var selectedDept = $("#department").val();
    if (selectedDept == "") 
      alert('Please select Department first.');
    else 
      add_nte_row(null);
  });

  $("#chk_all").on('click', function () 
  {
    $("#chk_all").is(":checked") ? $(".sel_box").val(1) : $(".sel_box").val(0);
    $("#chk_all").is(":checked") ? $(".sel_chk").attr("checked", true) : $(".sel_chk").attr("checked", false);
  });

  $('#save').on('click',function(event) 
  {
    action = 2;
    $("#nte_add_form").submit();
  });

  $('#send_chrd_aer').on('click',function(event) 
  {
    action = 3;
    var selected = $('.sel_chk:checkbox:checked');
    selected.length > 0 ? $("#nte_add_form").submit() : alert('PLEASE SELECT A ROW.');
  });

  $( "#nte_add_form" ).submit(function( event ) 
  {
    event.preventDefault();
    var nte_tab = document.getElementById("nte_table_body");

    if (nte_tab.rows.length > 0) 
    {
      var sData = $( this ).serializeArray();
      var addArray = [
        {name : 'action', value : action},
        {name : 'isFromMultiple', value : isFromMultiple},
        {name : '_token', value : token},
      ];

      $.ajax({
        method: 'POST',
        url: url,
        data: pushArray(sData, addArray)
      }).done(function(data) 
      {
        var message = $.parseJSON(data);
        var msg_cont = document.getElementById('container_message');
        
        msg_cont.style.display = "block";
        $('.container_message').text("");
        
        if (message.scs) 
        {
          $("#container_message").removeClass('alert-danger');
          $("#container_message").addClass('alert-success');

          if (action == 3) 
            selected_to_delete(false, true);
          else if (action == 2)
            selected_to_delete(false, false);

          $("#chk_all").attr('checked', false);
          $("#department").val("");

          nte_tab.rows.length > 0 ? '' : window.location = urlNteListCHRD;
        }
        else 
        {
          $("#container_message").removeClass('alert-success');
          $("#container_message").addClass('alert-danger');
        }

        for (var i = 0; i < message.msg.length; i++) 
          $('.container_message').append(message.msg[i].toUpperCase() + '<br>');

      });
    }
    else 
      alert("PLEASE INSERT A ROW.");
  });

  function calculate_days_in_month() 
  {
    cD = new Date($("#month_dd").val()+'-01-'+$("#year_dd").val()).monthDays();

    $('.comm').children().remove();

    $.each($('.comm'), function () 
    {
      var opt = document.createElement('option');
      opt.value = '';
      opt.innerHTML = '';
      this.appendChild(opt);

      for (var j = 0; j < cD; j++) 
      {
        var opt = document.createElement('option');
        j < 9 ? opt.value = '0'+(j+1) : opt.value = j+1;
        j < 9 ? opt.innerHTML = '0'+(j+1) : opt.innerHTML = j+1;
        this.appendChild(opt);
      }
    });
  }

  function get_drafted(departmentid) 
  {
    //var selectedDept = $('#department').val();
    var selectedYear = $('#year_dd').val();
    var selectedMonth = $('#month_dd').val();

    $.ajax({
      method: 'POST',
      url: urlGdft,
      data: {sD : departmentid, sY : selectedYear, sM : selectedMonth, _token : token, s:1},
      beforeSend: function () 
      {
        $("#department").multiselect('disable');
      }
    }).done(function(data) 
    {
      var jData = $.parseJSON(data);
      var emp_sel = $('.emp');

      $.each(jData.employees, function (i, employee) 
      {
        employees_selection.push(employee);

        if (emp_sel.length > 0)
          emp_sel.append('<option value="' + employee.id + '|' + employee.departmentid + '" dept="' + employee.departmentid + '">' + employee.name + '</option>')
                .trigger('chosen:updated');
      });

      if (jData.drafted.length > 0)
        var r = confirm('Would you like to add the retrieved drafted data from this department?');

      r ? add_nte_row(data) : '';

      $("#department").multiselect('enable');

      /*add_nte_row($.parseJSON(data).drafted.length > 0 ? data : null);*/
    });
  }

  function fill_rows() 
  {
    var selectedDept = $("#department").val();
    $.ajax({
      method: 'POST',
      url: urlGdpt,
      data: {data : selectedDept, _token : token},
    }).done(function(data) 
    {
      var jData = $.parseJSON(data);
      selection = jData;
      get_drafted();
    });
  }

  function selected_to_delete(bool, booly) 
  {
    var nte_tab = document.getElementById("nte_table_body");
    var selected = $('.sel_chk:checkbox:checked');
    var unselected = $('.sel_chk:checkbox:not(:checked)');

    if (booly) 
      if (selected.length > 0) 
        delete_nte_rows(selected, bool);
      else 
        alert('PLEASE SELECT A ROW.');
    else 
      if (unselected.length > 0) 
        delete_nte_rows(unselected, bool);
  }

  function delete_nte_rows(selectedBox, bool)
  {
    for (var i=(selectedBox.length - 1); i >= 0; i--) 
    {
      if (bool) 
      {
        var refno = $($(selectedBox[i].parentNode.parentNode.parentNode.parentNode.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling).children()[0]).val()
        if (refno != "") 
          $.ajax({
            method: 'POST',
            url: urlDrow,
            data: {data:refno}
          });
      }

      selectedBox[i].closest('tr').remove();
      i == 0 ? rename_row_uploads() : '';
    }
  }

  function fill_nte_row(nte_employee_id, nte_department_id, nte_day, nte_crr, nte_rda, nte_stats, nte_total_incurred, attachments_size_ctr, attachments_ctr, attachments, nte_reference_number, comments, is_retrieved)
  {
    var status = '';
    var dayoptions = '';
    var crroptions = '';
    var rdaoptions = '';

    for(var y=1;y<=cD;y++) 
      if (y == nte_day) 
      {
        y.toString().length < 2 ? y = '0'+y : y = y;
        dayoptions += "<option value="+y+" selected>"+y+"</option>";
      } 
      else 
      {
        y.toString().length < 2 ? y = '0'+y : y = y;
        dayoptions += "<option value="+y+">"+y+"</option>";
      }

    for (var j=0; j<crrbuildup.length;j++) 
      if (nte_crr == crrbuildup[j]['cb_crr_id']) 
        crroptions += "<option value="+crrbuildup[j]['cb_crr_id']+" Selected>"+crrbuildup[j]['cb_rule_name']+"</option>";
      else 
        crroptions += "<option value="+crrbuildup[j]['cb_crr_id']+">"+crrbuildup[j]['cb_rule_name']+"</option>";

    for (var x=0; x<rda.length;x++) 
      if (nte_rda == rda[x]['id']) 
        rdaoptions += "<option value="+rda[x]['id']+" selected>"+rda[x]['name']+"</option>";
      else 
        rdaoptions += "<option value="+rda[x]['id']+">"+rda[x]['name']+"</option>";

    for (var y=0; y<statuses.length; y++) 
      if (nte_stats == statuses[y]['id']) 
        status = statuses[y]['name'];

    var nte_tab = document.getElementById("nte_table_body");
    var last_row = nte_tab.rows.length;
    var row = nte_tab.insertRow(is_retrieved ? 0 : last_row);

    if (!(nte_department_id == ''))
      $(row).attr('class', 'drafted-' + nte_department_id);

    var cell1 = row.insertCell(0); 
    var cell2 = row.insertCell(1); 
    var cell3 = row.insertCell(2); 
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4); 
    var cell6 = row.insertCell(5); 
    var cell7 = row.insertCell(6); 
    var cell8 = row.insertCell(7);
    var cell9 = row.insertCell(8); 
    var cell10 = row.insertCell(9); 
    var cell11 = row.insertCell(10);

    cell1.innerHTML = '<div class="ui-widget">'+
                        '<div class="input-group">'+
                          '<span class="input-group-addon">'+
                            '<input type="hidden" class="sel_box" name="sel_emp[]" value="0">'+
                            '<input type="checkbox" class="sel_chk">'+
                          '</span>'+
                          '<select class="emp" name="emp_name[]">'+
                            '<option value=""></option>'+
                          '</select>'+
                        '</div>'+
                      '</div>';
    cell2.innerHTML = '<div class="input-group">'+
                        '<span class="input-group-addon">'+
                          '<label class="labels"></label>'+
                        '</span>'+
                        '<select class="comm form-control" name="date_comm[]">'+
                          '<option value=""></option>'+dayoptions+'</select>'+
                        '</div>';
    cell3.innerHTML = '<select name="infraction[]" class="infra form-control">'+
                        '<option value="">--Select--</option>'+crroptions+
                      '</select>';
    cell4.innerHTML = '<input type="text" name="tim[]" class="incc form-control" style="width: 70px;">';
    cell5.innerHTML = '<p class="form-control tvy"></p>';
    cell6.innerHTML = '<button type="button" data-toggle="modal" data-target="#history" class="btn btn-default btndefault history" style="width: 110px;">SHOW HISTORY</button>';
    cell7.innerHTML = '<select name="rda[]" class="recc form-control"><option value="">--Select--</option>"'+rdaoptions+'"</select>';
    cell8.innerHTML = '<div class="attachment_container nte-row-container">'+
                        '<div class="atch" id="attachments">'+
                          '<input type="hidden" value="'+attachments_size_ctr+'">'+
                          '<input type="hidden" value="'+attachments_ctr+'">'+
                          attachments+
                        '</div>'+
                        '<span class="btn btn-success btnbrowse fileinput-button nte-row-upload">'+
                          '<span >+</span>'+
                          '<input class="flup" type="file" name="attachments[]" multiple>'+
                        '</span>'+
                      '</div>';
    cell9.innerHTML = '<input type="text" readonly class="form-control ref inthemiddle" style="width: 80px;" name="ref_num[]" value="'+nte_reference_number+'">';
    cell10.innerHTML = '<label class="labels">'+status+'</label>';
    cell11.innerHTML = '<textarea name="remarks[]" class="rem form-control" style="width: 150px;">'+comments+'</textarea>';

    var lbl_date = $($($($(cell2).children()[0]).children()[0]).children()[0]);
    var tim = $($(cell4).children()[0]);

    get_tvy(nte_employee_id, nte_crr, $(cell5).children());

    add_month_year(nte_day, lbl_date, tim);

    $(tim).val(nte_total_incurred);

    for (var i=0; i<employees_selection.length; i++) 
    {
      var optsel = (nte_employee_id == employees_selection[i]['id'] ? '<option Selected></option>' : '<option></option>');
      $($($($(cell1).children()[0]).children()[0]).children()[1])
      .append($(optsel)
      .attr("value",employees_selection[i]['id']+'|'+employees_selection[i]['departmentid'])
      .attr("dept",employees_selection[i]['departmentid'])
      .text(employees_selection[i]['name']));
    }

    /*$( ".emp" ).combobox({ select: function (event, ui) { get_tvy_emp(this); } });*/

    $(".emp").chosen({width:"100%"}).change(function () {
      get_tvy_emp(this);
    })

    initFileBrowse(".flup");
  }

  function add_nte_row(data) 
  {
    if (data != null) 
      try
      {
        var nData = $.parseJSON(data);
        var jData = nData['drafted'];
        var cData = nData['comments'];
        var aData = nData['attachments'];

        for (var z=jData.length - 1; z>=0; z--)
        {
          var comments = '';
          var attachments = '';
          var attachments_ctr = 0;
          var attachments_size_ctr = 0;
          
          for (var u = 0; u < aData.length; u++) 
            if (aData[u]['nte_id'] == jData[z]['nte_id']) 
            {
              attachments += '<p><input type="hidden" name="files['+z+']['+attachments_ctr+'][filesize]" value="'+aData[u]['filesize']+'">';
              attachments += '<input type="hidden" name="files['+z+']['+attachments_ctr+'][mime_type]" value="'+aData[u]['mime_type']+'">';
              attachments += '<input type="hidden" name="files['+z+']['+attachments_ctr+'][original_extension]" value="'+aData[u]['original_extension']+'">';
              attachments += '<input type="hidden" name="files['+z+']['+attachments_ctr+'][original_filename]" value="'+aData[u]['original_filename']+'">';
              attachments += '<input type="hidden" name="files['+z+']['+attachments_ctr+'][random_filename]" value="'+aData[u]['random_filename']+'">';
              attachments += '<a href="'+newUrl+'nte/'+jData[z]['nte_reference_number']+'/'+aData[u]['random_filename']+'/'+aData[u]['encrypted']+'">'+aData[u]['original_filename']+' | '+convert(aData[u]['filesize'])+'</a>&nbsp; <button type="button" class="btn btn-danger btndanger nte-row-upload nte-delete">-</button></p>';
              attachments_ctr += 1;
              attachments_size_ctr += aData[u]['filesize'];
            };

          for (var v=0; v<cData.length; v++) 
            if (cData[v]['nte_id'] === jData[z]['nte_id']) 
              if (comments == '') 
                comments = cData[v]['comment'];

          fill_nte_row(jData[z]['nte_employee_id'], jData[z]['nte_department_id'], jData[z]['nte_day'], jData[z]['cb_crr_id'], jData[z]['nte_recommended_disciplinary_action']
            , jData[z]['nte_status'], jData[z]['nte_total_incurred'], attachments_size_ctr, attachments_ctr, attachments, jData[z]['nte_reference_number'], comments, true);
        } 
      }
      catch (e){}
    else 
      fill_nte_row('', '', '', '', '', '', '', 0, 0, '', '', '', false);

    disable_button();
  }

  function get_tvy_crr(crr_sel)
  {
    var ref_num = $($($($(crr_sel.parentNode.previousSibling.previousSibling).children()[0]).children()[0]).children()[1]).val();
    var crr = $(crr_sel).val();
    var tvy = $(crr_sel.parentNode.nextSibling.nextSibling).children()[0];

    get_tvy(ref_num, crr, tvy);
  }

  function get_tvy_emp(emp_name)
  { 
    var ref_num = $(emp_name).val();
    var crr = $($(emp_name.parentNode.parentNode.parentNode.nextSibling.nextSibling.nextSibling).children()[0]).val();
    var tvy = $(emp_name.parentNode.parentNode.parentNode.nextSibling.nextSibling.nextSibling.nextSibling).children()[0];
    
    get_tvy(ref_num, crr, tvy);
  } 

  function get_tvy(ref_num, crr, tvy) 
  {
    var year = $("#year_dd").val();
    $.ajax({
      method: 'POST',
      url: urlGtvy,
      data: {ref_num : ref_num, year : year, crr : crr}
    }).done( function (data) 
    {
      var jData = $.parseJSON(data);
      if (jData.length > 0) 
        $(tvy).text(jData[0]['count']);
      else
        $(tvy).text(""); 
    });
  }

  function disable_button() 
  {
    var nte_tab = document.getElementById("nte_table_body");
    if (nte_tab.rows.length > 0) 
      document.getElementById('delete_nte').disabled = false;
    else 
      document.getElementById('delete_nte').disabled = true;
  }

  function pushArray(origin, addArray)
  {
    for (var i = 0; i < addArray.length; i++) 
      origin.push(addArray[i]);

    return origin;
  }

  $(document).on('change', '.comm', function () 
  {
    var day = $(this).val();
    var lbl_date = $($(this.previousSibling).children()[0]);
    var tim = $($($(this.parentNode.parentNode.parentNode).children()[3]).children()[0]);

    add_month_year(day, lbl_date, tim);
  });

  $(document).on('click', '.sel_chk', function () 
  {
    var sel_box = $($(this.parentNode).children()[0]);
    this.checked ? sel_box.val(1) : sel_box.val(0);
  });

  $(document).on('change', '.infra', function ()
  {
    get_tvy_crr(this);
  });
});