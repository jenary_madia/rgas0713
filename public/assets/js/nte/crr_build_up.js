$(document).ready(function(){
	"use strict";

	function pushArray(origin, addArray)
  	{
	    for (var i = 0; i < addArray.length; i++) 
	    {
      		origin.push(addArray[i]);
	    };

	    return origin;
  	}

	$(document).on('click', 'button.removebutton', function () {
		var sec_tab = document.getElementById("section_table_body");
		var last_row = sec_tab.rows.length;
		var crr_id = document.getElementsByClassName('crr_id');
		var sec_no = document.getElementsByClassName('section_no');
		var sec_desc = document.getElementsByClassName('section_desc');
		var sec_status = document.getElementsByClassName('section_status');
		if (last_row <= 1) {
			alert('you cannot delete this row');
		} else {
			$(this).closest('tr').remove();	
			var last_row = sec_tab.rows.length;
			for (var i=0; i<last_row; i++) {
				crr_id[i].setAttribute('name', 'crr_id['+i+']');
				sec_no[i].setAttribute('name', 'section_no['+i+']');
				sec_desc[i].setAttribute('name', 'section_desc['+i+']');
				sec_status[i].setAttribute('name', 'section_status['+i+']');
			}
		}
	     return false;
	});
    
	var base_url = $("base").attr("href");
	var selected = [];
    
    if ( $("#crr_list").length != 0 ) {
        
        var crr_list_table = $('#crr_list').dataTable( {

			  "sPaginationType": "full_numbers",
			 /* "bProcessing": true,
			  "bServerSide": true,*/
			  "sAjaxSource": base_url + "/nte/crr/get",
			  /*"fnServerData": fnDataTablesPipeline,*/
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  /*"fnDrawCallback": function( oSettings ) {
                  
			   },*/
			  /*"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], */
			  "aoColumns": [
			  	{ "sClass": "td_center", "bVisible" : false },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
			],
		});

		$('#crr_list td').live('click',function()
		{
			var aPos = crr_list_table.fnGetPosition(this);
			var aData = crr_list_table.fnGetData(aPos[0],0);

			$('#header_rule').val(aData);
			$('#edit_submit').click();
		});

		$('#crr_list tr').hover(function() {
		    $(this).addClass('hover');
		}, function() {
		    $(this).removeClass('hover');
		});

		crr_list_table.fnSort([[1, 'asc']]);
    }

	$('#save_crr').on('click', function() {
		action = 1;
		if (hId == "") {
			var r = confirm('ADD DETAILS IN COMPANY RULES AND REGULATION BUILD-UP?');
		} else {
			var r = confirm('SAVE CHANGES IN COMPANY RULES AND REGULATION BUILD-UP?');
		}

		if (r == true) {
	       $( "#crr_add_form" ).submit();
	    }	
	});

	$('#add_section').on('click', function() {
		add_section_row();
	});

	function add_section_row() {
		var sec_tab = document.getElementById("section_table_body");
		var last_row = sec_tab.rows.length;
		var row = sec_tab.insertRow(last_row);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);

		cell1.innerHTML = '<td width="20%">' +
						'<input type="hidden" Class="crr_id" Name="crr_id['+last_row+']">' +
						'<input type="text" class="add form-control section_no" name="section_no['+last_row+']" maxlength="5"></input> </td>';
		cell2.innerHTML = '<td width="60%"> <textarea class="add form-control section_desc" name="section_desc['+last_row+']" maxlength="1000"></textarea> </td>';
		cell3.innerHTML = '<td width="20%"> <select class="form-control section_status" name="section_status['+last_row+']">'+
	            					'<option value="1">Active</option>'+
	            					'<option value="0">Inactive</option>'+
	            				'</select> <button type="button" class="removebutton btn btn-danger" title="Remove this row">X</button></td>';
	}

	$("#crr_add_form").submit(function (event) {
		event.preventDefault();
		var sData = $( this ).serializeArray();
		var addArray = [
	        {name : 'action', value : action},
	        {name : 'hId', value : hId},
	        {name : '_token', value : token},
      	];	

		$.ajax({
			method: "POST",
			url: url,
			data: pushArray(sData, addArray)
		}).done( function(data) {
			var added = document.getElementsByClassName('add');
			//$('.removebutton').remove();
			var message = $.parseJSON(data);
	      	var msg_cont = document.getElementById('container_message');
	      	
	      	msg_cont.style.display = "block";
	      	$('.container_message').text("");

	      	if (message.scs) {
	        	$("#container_message").removeClass('alert-danger');
		        $("#container_message").addClass('alert-success');
		      	window.location = urlList;
	      	} else {
		      	$("#container_message").removeClass('alert-success');
		        $("#container_message").addClass('alert-danger');
	      	}
	      	for (var i = 0; i < message.msg.length; i++) {	        	
		        $('.container_message').append(message.msg[i].toUpperCase() + '<br>');
	        };
		});
	});
});