var get_history;
var get_month_name;
var get_history_page;

$(document).ready( function () 
{
	"use strict";	

	$("#history").on('hidden.bs.modal', function () 
  	{
	    clear_history_modal();
  	})

  	get_month_name = function get_month_name(month) 
  	{
	    var month_name;

	    month == '01' ? month_name = 'January' : '';
	    month == '02' ? month_name = 'February' : '';
	    month == '03' ? month_name = 'March' : '';
	    month == '04' ? month_name = 'April' : '';
	    month == '05' ? month_name = 'May' : '';
	    month == '06' ? month_name = 'June' : '';
	    month == '07' ? month_name = 'July' : '';
	    month == '08' ? month_name = 'August' : '';
	    month == '09' ? month_name = 'September' : '';
	    month == '10' ? month_name = 'October' : '';
	    month == '11' ? month_name = 'November' : '';
	    month == '12' ? month_name = 'December' : '';

	    return month_name;
  	}

  	function clear_history_modal () 
  	{
	    var act = document.getElementsByClassName('act');

	    for (var i = 0; i < act.length; i++) 
	      	act[i].classList.add('hidden');

	    var sus = document.getElementsByClassName('sus');

	    for (var j = 0; j < sus.length; j++) 
	      	sus[j].classList.add('hidden');

	    var his = document.getElementsByClassName('his');

	    for (var k = 0; k < his.length; k++) 
	      	his[k].value = "";

	    $("#history_decision").val(0);
	    $("#history_da").val(0);
  	}

  	function get_history_details() 
  	{
	    var year = $("#year_h").val();
	    var months_num = 12;
	    $.ajax({
	      	method: "POST",
	      	url: urlGd,
	      	data: { emp_name : nte_emp, year : year, request : 2 }
	    }).done( function (data) {
	      	var jData = $.parseJSON(data);
	      
	      	$('#history_body tr').remove();
	      	cY == year ? months_num = (cM) : months_num = 12;
	      	for (var i = 0; i < months_num; i++) 
	      	{
		        var rspan = 1;
		        var month = get_month_name(i+1);
		        var inserted = false;

		        if (jData.length > 0) 
		        {
			        for (var j = 0; j < jData.length; j++)
			        {  
			          	var his_tab = document.getElementById("history_body");
			          	var last_row = his_tab.rows.length;
			          	var row = his_tab.insertRow(last_row);
			          	(i+1) % 2 ? row.className = "odd" : row.className = "even";

			          	var disciplinary_action = jData[j]['disact'] === '' || jData[j]['disact'] === null ? '' : jData[j]['disact'];

			          	if (jData[j]['nte_month'] == i+1) 
			          	{
				            inserted = true;
				            if (rspan === 1) 
				            {
				              	var cell1 = row.insertCell(0);
				              	cell1.id="month_"+(i+1);
				              	cell1.innerHTML = month;

				              	var cell = document.getElementById("month_"+(i+1));
				              	cell.setAttribute('rowspan', rspan);

				              	var cell2 = row.insertCell(1);
				              	var cell3 = row.insertCell(2);
				              	var cell4 = row.insertCell(3);
				              
				              	cell2.innerHTML = jData[j]['infraction'];
				              	cell3.innerHTML = jData[j]['recdisact'];
				              	cell4.innerHTML = '<span>'+ disciplinary_action+'</span><label class="hidden">'+jData[j]['nte_id']+'</label>';

				              	cell1.className = "data";
				              	cell2.className = "data";
				              	cell3.className = "data";
				              	cell4.className = "data";
				            }
				            else 
				            {
				              	var cell = document.getElementById("month_"+(i+1));
				              	cell.setAttribute('rowspan', rspan);

				              	var cell1 = row.insertCell(0);
				              	var cell2 = row.insertCell(1);
				              	var cell3 = row.insertCell(2);
				              
				              	cell1.innerHTML = jData[j]['infraction'];
				              	cell2.innerHTML = jData[j]['recdisact'];
				              	cell3.innerHTML = '<span>'+ disciplinary_action+'</span><label class="hidden">'+jData[j]['nte_id']+'</label>';

				              	cell1.className = "data";
				              	cell2.className = "data";
				              	cell3.className = "data";
				            }

			            	rspan = rspan + 1;
			          	}

			          	if (!inserted && j == jData.length - 1)
			          	{
				            inserted = true;

				            var cell1 = row.insertCell(0);
				            var cell2 = row.insertCell(1);
				            var cell3 = row.insertCell(2);
				            var cell4 = row.insertCell(3);

				            cell1.id="month_"+(i+1);
				            cell1.innerHTML = month;

				            var cell = document.getElementById("month_"+(i+1));
				            cell.setAttribute('rowspan', rspan);

				            cell1.innerHTML = month;

				            cell1.className = "data";
				            cell2.className = "data";
				            cell3.className = "data";
				            cell4.className = "data";
			          	}
			        }
		        }
		        else
		        {
		        	var his_tab = document.getElementById("history_body");
		          	var last_row = his_tab.rows.length;
		          	var row = his_tab.insertRow(last_row);
		          	(i+1) % 2 ? row.className = "odd" : row.className = "even";

		        	var cell1 = row.insertCell(0);
		            var cell2 = row.insertCell(1);
		            var cell3 = row.insertCell(2);
		            var cell4 = row.insertCell(3);

		            cell1.id="month_"+(i+1);
		            cell1.innerHTML = month;

		            var cell = document.getElementById("month_"+(i+1));
		            cell.setAttribute('rowspan', rspan);

		            cell1.innerHTML = month;

		            cell1.className = "data";
		            cell2.className = "data";
		            cell3.className = "data";
		            cell4.className = "data";
		        }
	      	};
	    });
  	}

  	$("#show_history_page").on('click', function () {
  		$("#his_emp_name").val("");
	    $("#his_dept_name").val("");
	    $('#history_body tr').remove();
	    var emp_name = $("#emp_name_page").val();
	    if (emp_name != '')
	    {
	      	nte_emp = emp_name;

	      	$.ajax({
	        	method: "POST",
	        	url: urlGh,
	        	data: {emp_name : emp_name},
	      	}).done( function (data) {
	        	var jData = $.parseJSON(data);
	        
      			$("#his_emp_name").val(jData['name']);
         		$("#his_dept_name").val(jData['dept_name']);
	        	get_history_details();
	      	});
	    }
  	});

  	$(document).on('click', '.history', function () 
  	{
	    $("#his_emp_name").val("");
	    $("#his_dept_name").val("");
	    $('#history_body tr').remove();

	    var emp_name = $($($($($(this.parentNode.parentNode).children()[0]).children()[0]).children()[0]).children()[1]).val();
	    if (emp_name != '')
	    {
	      	var split = emp_name.split('|');
	      	nte_emp = split[0];

	      	$.ajax({
	        	method: "POST",
	        	url: urlGh,
	        	data: {emp_name : nte_emp},
	      	}).done( function (data) {
	        	var jData = $.parseJSON(data);

          		$("#his_emp_name").val(jData['name']);
          		$("#his_dept_name").val(jData['dept_name']);
          		get_history_details();
	      	});
	    }
  	});

	$("#year_h").on('change', function () 
  	{
    	get_history_details();
  	});

  	$("#history_body tr").live('click', function (event) 
  	{
	    var ref_num = '';

	    try 
	    {
	      ref_num = this.lastElementChild.childNodes[1].innerHTML;
	    }
	    catch (e)
	    {}
	    
	    if (ref_num != '') 
	    {
	      	$.ajax({
	        	method: 'POST',
	        	url: urlHis,
	        	data: {ref_num : ref_num, bool : 0}
	      	}).done( function (data) 
	      	{
	        	$("#nte").modal('toggle');
	        	var jData = $.parseJSON(data);
	        	console.log(jData['crr']);

	        	var jDataCrr = jData['crr'];
	        	var jDataMessage = jData['message'];

	        	//var history_crr = jDataCrr.split('&#xA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
	        	var history_crr = jDataCrr.replace(/&#xA/g, '\n').replace(/&nbsp;/g, '\u00a0');
	        	var history_message = jDataMessage.split('&#013');

	        	var history_crr_html = '';
	        	var history_message_html = '';


	        	var attchs = [[jData.files_attch_html, jData.files_last_count, 'files'],
	        				[jData.emp_attch_html, jData.emp_last_count, 'emp'],
	        				[jData.dept_attch_html, jData.dept_last_count, 'dept']];
				for (var a = 0; a < attchs.length; a++) 
				{
		        	var attch_html = attchs[a][0].split('><');
		        	var objectCount = attch_html.length / attchs[a][1];
		        	var objectIndex = 6;
		        	$("#his_"+attchs[a][2]+"_attachments").text("");

		        	for (var b = 0; b < attchs[a][1]; b++) 
		        	{
		        		b !== 0 ? objectIndex = objectIndex + objectCount : '';
		        		$("#his_"+attchs[a][2]+"_attachments").append("<"+attch_html[objectIndex]+"><br>");
		        	};
				};

		        $("#history_crr").text("");
		        $("#history_message").text("");
	        
		        $("#history_da").children().remove();

		        for (var c = 0; c < history_crr.length; c++) 
		        	history_crr_html += history_crr[c]+'\n \u00a0 \u00a0 \u00a0 \u00a0';

		        for (var d = 0; d < history_message.length; d++) 
		        	history_message_html += history_message[d]+'\n';

		        document.getElementById("history_crr").value = history_crr;
		        document.getElementById("history_message").value = history_message_html;

		        $("#history_date_filed").val(jData['nte_submitted_chrd_aer']);
		        $("#history_name").val(jData['returned']['name']);
		        $("#history_reference_number").val(jData['returned']['nte_reference_number']);
		        $("#history_number").val(jData['returned']['employeeid']);
		        $("#history_department").val(jData['returned']['dept_name']);
		        $("#history_status").val(jData['returned']['statuses']);
		        $("#history_section").val(jData['returned']['sect_name']);

		        if (jData['returned']['nte_day'] != '') 
		          	$("#history_date_committed").val(jData['month_name']+' '+jData['returned']['nte_day']+', '+jData['returned']['nte_year']);
		        else
		          	$("#history_date_committed").val(jData['month_name']+' '+jData['returned']['nte_year']);  

		        $("#history_tim").val(jData['returned']['nte_total_incurred']);
		        $("#history_tvy").val(jData['tvy'][0]['count']);
		        $("#history_rda").val(jData['returned']['rda']);
		        $("#history_explanation").val(jData['returned']['nte_explanation']);
		        $("#history_remarks").val(jData['returned']['nte_head_remarks']);
		        $("#history_decision").val(jData['returned']['nte_head_decision']);

		        if (jData['returned']['nte_head_decision'] == 2)
		        {
		          	var act = document.getElementsByClassName('act');

		          	for (var i = 0; i < act.length; i++) 
		            	act[i].classList.remove('hidden');

		          	$("#history_da")
		            	.append($("<option></option>")
		            	.attr("value","0")
	            		.text('--Select--'));

		          	for (var i=1; i<jData['rdas'].length; i++) 
		          	{
		            	var optsel = (jData['rdas'][i]['id'] === jData['returned']['nte_disciplinary_action'] ? '<option Selected></option>' : '<option></option>');
		            	$("#history_da")
		            	.append($(optsel)
		            	.attr("value",jData['rdas'][i]['id'])
		            	.text(jData['rdas'][i]['name']));
		          	}

		          	if (jData['returned']['nte_disciplinary_action'] == 4)
		          	{
		            	var sus = document.getElementsByClassName('sus');

		            	for (var i = 0; i < sus.length; i++) 
		            	{
		              		sus[i].classList.remove('hidden');
		            	};

		            	$("#history_from").val(jData['returned']['nte_start_suspension']);
		            	$("#history_to").val(jData['returned']['nte_end_suspension']);
		            	$("#history_nod").val(jData['returned']['nte_no_of_days']);
		          	}
	        	}
	      	});
	    }
  	})

	$(document).on('click', 'button[data-target="#history"]', function () {
		$('#year_h :nth-child(2)').prop('selected', true);
	})
});