$(document).ready(function() {

	"use strict";

	rename_attachments();

 	if ( $("#emp_name").length != 0 ) 
    {
		/*$( "#emp_name" ).combobox({ select: function( event, ui ) { getReferenceNumbers($(this).val()) }, });*/

		$("#emp_name").chosen({width:"140%"}).change(function (argument) {
	      getReferenceNumbers($(this).val());
	    })

	  	$( "#toggle" ).click(function() {
	    	$( "#emp_name" ).toggle();
	  	});
  	}

	function getReferenceNumbers (id) {
		$.ajax({
			method: "POST",
			url: urlGbEI,
			data: {data: id}
		}).done(function (data) {
			var jData = $.parseJSON(data);
			$("#emp_num").val(jData.employee['new_employeeid']);
			$("#dept").val(jData.employee['dept_name']);
			$("#sect").val(jData.employee['sect_name']);
			var select = document.getElementById('ref');
			$('#ref').children().remove();
			 var opt = document.createElement('option');
			    opt.value = '';
			    opt.innerHTML = '--Select--';
			    select.appendChild(opt);
			for (var i = 0; i < jData.references.length; i++){
			    var opt = document.createElement('option');
			    opt.value = jData.references[i]['nte_reference_number'];
			    opt.innerHTML = 'NTE-'+jData.references[i]['nte_reference_number'];
			    select.appendChild(opt);
			}
		});
	}

	function clear_fields()
	{
		$("#ref_num").val('');
		$("#status").val('');
		$("#chrd_date").val('');
		$("#crr").val('');
		$("#da").val('');
		$("#sus_from").val('');
		$("#sus_to").val('');
		$("#sus_no").val('');
		$("#nda_ref").val('');
		$("#nda_date").val('');
		$("#crr_desc").val('');
		var sus = document.getElementsByClassName('sus');
		for (var j = 0; j < sus.length; j++) 
			sus[j].style.display = 'none';
	}

	$("#ref").on('change', function (event) {
		var emp = $("#emp_name").val();
		var ref = $(this).val();

		$.ajax({
			method: 'POST',
			url: urlGbEIRN,
			data: {emp:emp, ref:ref}
		}).done(function (data) {
			clear_fields();
			var dData = $.parseJSON(data)
			var jData = dData.data;
			
			if (jData != null)
			{
				$("#ref_num").val(jData['nte_id']);
				$("#emp_num").val(jData['emp_id']);
				$("#dept").val(jData['dept_name']);
				$("#status").val('NEW');
				$("#sect").val(jData['sect_name']);
				$("#chrd_date").val(jData['nte_submitted_chrd_aer'].split(' ')[0]);
				$("#crr").val(jData['cb_rule_name']);
				$("#da").val(jData['disact']);
				if (jData['nte_disciplinary_action'] == 4) {
					var sus = document.getElementsByClassName('sus');
					for (var i = 0; i < sus.length; i++) {
						sus[i].style.display = 'block';
					}
					$("#sus_from").val(jData['nte_start_suspension']);
					$("#sus_to").val(jData['nte_end_suspension']);
					$("#sus_no").val(jData['nte_no_of_days']);
				}
				jData['nda_reference_number'] != '' ? $("#nda_ref").val(jData['nda_reference_number']) : '';
				jData['nda_submitted_chrd_manager'] != '0000-00-00 00:00:00' ? $("#nda_date").val(jData['nda_submitted_chrd_manager'].split(' ')[0]) : '';
				$("#crr_desc").val(jData['cb_rule_name']  + " \n \u00a0 \u00a0 \u00a0 \u00a0" + 'Rule No. ' + jData['cb_rule_number'] + ' - ' + jData['cb_rule_description'] + '\n' + dData.crrd.replace(/&#xA/g, '\n'));
			}
		});
	});

	$("#save").on('click', function () {
		action = 14;
		$("#nda_add_form").submit();
	});

	$("#send_m").on('click', function () {
		action = 15;
		$("#nda_add_form").submit();
	});

	$("#return_r").on('click', function () {
		action = 16;
		$("#nda_add_form").submit();
	});

	$("#send_chrdh").on('click', function () {
		action = 17;
		$("#nda_add_form").submit();
	});

	$("#return_r_to").on('click', function () {
		action = 18;
		$("#nda_add_form").submit();
	});

	$("#send_dh").on('click', function () {
		action = 19;
		$("#nda_add_form").submit();
	});

	$("#return_emp").on('click', function () {
		action = 20;
		$("#nda_add_form").submit();
	});

	$("#send_emp").on('click', function () {
		action = 21;
		$("#nda_add_form").submit();
	});

	$("#acknowledge").on('click', function () {
		action = 22;
		$("#nda_add_form").submit();
	});

	$("#send_cbrm").on('click', function () {
		action = 23;
		$("#nda_add_form").submit();
	});

	$("#cancel").on('click', function () 
  	{
    	action = 25;
	    $("#nda_add_form").submit();
  	});  

	$("#print_nda").on('click', function () {
		document.nda_add_form.action = urlNdaPrint;
		$("#nda_add_form").submit();
	});

	$( "#nda_add_form" ).submit(function( event ) 
	{
		if (stat === '16') 
	    {

	    }
	    else
	    {
	      	event.preventDefault();
	      	var sData = $( this ).serializeArray();
	      	var addArray = [
		        {name : 'action', value : action},
		        {name : '_token', value : token},
	      	];

	      	$.ajax({
	        	method: 'POST',
	        	url: url,
	        	data: pushArray(sData, addArray)
	      	}).done( function(data) {
	        	var message = $.parseJSON(data);
	          	var msg_cont = document.getElementById('container_message');
	          
	          	msg_cont.style.display = "block";
	          	$('.container_message').text("");
	          	if (message.scs) 
	          	{
	            	window.location = urlNdaListCHRD;

	            	$("#container_message").removeClass('alert-danger');
	            	$("#container_message").addClass('alert-success');
	            	if (action != 14)
	            	{
		            	disable_all(true);
		            	hide_all(true);
		            	
		            	var selectBox = document.getElementsByClassName('custom-combobox-input');
		            	selectBox.length > 0 ? selectBox[0].disabled = true : '';
	            	}
	          	}
	          	else 
	          	{
	            	$("#container_message").removeClass('alert-success');
	            	$("#container_message").addClass('alert-danger');
	          	}

	          	for (var i = 0; i < message.msg.length; i++) {            
		            $('.container_message').append(message.msg[i].toUpperCase() + '<br>');
	          	};
	      	});
      	}
	});

	function pushArray(origin, addArray)
  	{
	    for (var i = 0; i < addArray.length; i++) 
	    {
      		origin.push(addArray[i]);
	    };

	    return origin;
  	}

  	function disable_all(bool) 
  	{
    	var inputBox = document.getElementsByClassName('ib');
    	for (var i = 0; i < inputBox.length; i++) 
    	{
      		bool ? inputBox[i].disabled = true : inputBox[i].disabled = false;
    	};
  	}

  	function hide_all(bool) 
  	{
    	var btns = document.getElementsByClassName('btn');
    	for (var i = 0; i < btns.length-1; i++) 
    	{
      		bool ? btns[i].className += ' hidden' : btns[i].className = 'btn btn-default btndefault';
    	};
  	}
});