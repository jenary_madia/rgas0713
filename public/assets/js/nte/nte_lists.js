var nte_approval;
var nte_department;
var nte_employee;
var nte_request;
var nte_view;
var nte_returned;
var nda_department;
var nda_employee;
var nda_approval_head;
var nda_approval_manager;
var nda_approval;
var nda_request;
var nda_view;
var nda_returned;
var nte_delete;
var nda_cbrm;
var nda_acknowledged;

var nte_list_request;
var nda_list_request;

$(document).ready(function()
{
	"use strict";
	var base_url = $("base").attr("href");
	var selected = [];

	nte_view = function nte_view(id) 
    {
    	$("#ref_num_view").val(id);
		$("#nte_view").submit();
    }

    nte_approval = function nte_approval(id1, id2, id3, bool) 
    {
		$("#ref_num_approval").val(id1);
		$("#ref_num_approval2").val(id2 < 10 ? '0'+id2 : id2);
		$("#ref_num_approval3").val(id3);
		bool ? $("#bool_approval").val(1) : $("#bool_approval").val(0);
		$("#nte_approval").submit();
	}

	nte_department = function nte_department(id) 
	{
		$("#ref_num_department").val(id);
		$("#nte_department").submit();
	}

	nte_employee = function nte_employee(id) 
	{
		$("#ref_num_employee").val(id);
		$("#nte_employee").submit();
	}

	nte_request = function nte_request(id) 
	{
		$("#ref_num_request").val(id);
		$("#nte_request").submit();
	}

	nte_returned = function nte_returned(id) 
	{
		$("#ref_num_returned").val(id);
		$("#nte_returned").submit();
	}
	nda_approval_head = function nda_approval_head(id) 
    {
		$("#ref_num_approval_h").val(id);
		$("#nda_head").submit();
	}

	nda_approval_manager = function nda_approval_manager(id) 
	{
		$("#ref_num_approval_m").val(id);
		$("#nda_manager").submit();
	}

	nda_department = function nda_department(id) 
	{
		$("#nda_department #ref_num_department").val(id);
		$("#nda_department").submit();
	}

	nda_employee = function nda_employee(id) 
	{
		$("#nda_employee #ref_num_employee").val(id);
		$("#nda_employee").submit();
	}

	nda_request = function nda_request(id) {
		$("#ref_num_request").val(id);
		$("#nda_request").submit();
	}

	nda_view = function nda_view(id) {
		$("#nda_view #ref_num_view").val(id);
		$("#nda_view").submit();
	}

	nda_acknowledged = function nda_acknowledged(id) {
		$("#ref_num_acknowledged").val(id);
		$("#nda_acknowledged").submit();
	}

	nda_cbrm = function nda_cbrm(id, bool) 
	{
		$("#ref_num_cbrm").val(id);
		bool ? $("#bool_cbrm").val(1) : $("#bool_cbrm").val(0);
		$("#nda_cbrm").submit();
	}

	nte_delete = function nte_delete(ref_num, row) 
	{
	    var name = $('#nte_list_request tr:eq('+row+') td:eq(1)').html();
		
		confirm('ARE YOU SURE YOU WANT TO DELETE NTE OF '+name+'?') ? rdelete(ref_num) : "";
	}

	function rdelete(ref_num)
	{
		$.ajax({
			method: "POST",
			url: urlDes,
			data: {data : ref_num}
		}).done(function (data) {
			alert("NTE SUCCESSFULLY DELETED");
			location.reload();
		});
	}
    
    if ( $("#nte_list_approval").length != 0 ) 
    {
        var nte_list_approval = $('#nte_list_approval').DataTable( {

			  "sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/nte/nte/get_aa/" + module,
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true, "sWidth": "40%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%"  },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%"  },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "15%" },
			]
		});

		nte_list_approval.fnSort([[3, 'desc']]);
    }

    if ( $("#nte_list_department").length != 0 ) 
    {
        var nte_list_department = $('#nte_list_department').dataTable( {

			  "sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/nte/nte/get_ad/" + module,
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true, "sWidth": "12%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "12%" },
				{ "sClass": "td_center", "bSortable": true , "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "21%" },
			]
		});

		nte_list_department.fnSort([[0, 'desc']]);
    }

    if ( $("#nte_list_employee").length != 0 ) 
    {
        var nte_list_employee = $('#nte_list_employee').dataTable( {

			  "sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/nte/nte/get_ae/" + module,
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true, "sWidth": "19%"},
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "15%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "16%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "20%" },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "15%" },
			]
		});

		nte_list_employee.fnSort([[0, 'desc']]);
    }

    if ( $("#nte_list_request").length != 0 ) 
    {
        nte_list_request = $('#nte_list_request').dataTable( {
			  "sPaginationType": "full_numbers",
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "sAjaxSource": base_url + "/nte/nte/get/" + module,
			  "fnServerParams": function ( aoData ) 
			  {
				aoData.push( { "name": "sDepartment", "value": $('.s-nte-department').val() } );
				aoData.push( { "name": "sYear", "value": $('.s-nte-year').val() } );
				aoData.push( { "name": "sMonth", "value": $('.s-nte-month').val() } );
		      },
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				],
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "7%" },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "23%" },	
			]
		});

	    $('.s-nte-department, .s-nte-month, .s-nte-year').on('change', function() 
		{
			var value = $('.dataTables_filter input').val();
			nte_list_request.fnFilter(value);
		});

		nte_list_request.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_approval_head").length != 0 ) 
    {
        var nda_list_approval_head = $('#nda_list_approval_head').dataTable( {
			  "sPaginationType": "full_numbers",
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "sAjaxSource": base_url + "/nte/nda/get_ah/" + module,
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true,},
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "20%" },
			]
		});

		nda_list_approval_head.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_approval_manager").length != 0 ) 
    {   
        var nda_list_approval_manager = $('#nda_list_approval_manager').dataTable( {

			  "sPaginationType": "full_numbers",
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "sAjaxSource": base_url + "/nte/nda/get_am/" + module,
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true,},
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "20%" },
			]
		});

		nda_list_approval_manager.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_department").length != 0 ) 
    {
        var nda_list_department = $('#nda_list_department').dataTable( {

			  "sPaginationType": "full_numbers",
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "sAjaxSource": base_url + "/nte/nda/get_ad/" + module,
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true,},
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "20%" },
			]
		});

		nda_list_department.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_employee").length != 0 ) 
    {
        var nda_list_employee = $('#nda_list_employee').dataTable( {

			  "sPaginationType": "full_numbers",
			  "bFilter": (!(module == 'home')),
			  "bLengthChange": (!(module == 'home')),
			  "bPaginate": (!(module == 'home')),
			  "bInfo": (!(module == 'home')),
			  "sAjaxSource": base_url + "/nte/nda/get_ae/" + module,
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "20%" },
			]
		});

		nda_list_employee.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_request").length != 0 ) 
    {
        nda_list_request = $('#nda_list_request').dataTable( {

		  	"sPaginationType": "full_numbers",
		  	"bFilter": (!(module == 'home')),
			"bLengthChange": (!(module == 'home')),
			"bPaginate": (!(module == 'home')),
			"bInfo": (!(module == 'home')),
		  	"sAjaxSource": base_url + "/nte/nda/get/" + module,
		  	"fnServerParams": function ( aoData ) 
		  	{
				aoData.push( { "name": "sDepartment", "value": $('.s-nda-department').val() } );
				aoData.push( { "name": "sYear", "value": $('.s-nda-year').val() } );
				aoData.push( { "name": "sMonth", "value": $('.s-nda-month').val() } );
	      	},
		  	"oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
		  	},
		  	"aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
		  	"aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "10%" },
				{ "sClass": "td_center", "bSortable": true, "sWidth": "7%" },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "23%" },	
			]
		});

		$('.s-nda-department, .s-nda-month, .s-nda-year').on('change', function() 
		{
			var value = $('.dataTables_filter input').val();
			nda_list_request.fnFilter(value);
		});

		nda_list_request.fnSort([[0, 'desc']]);
    }

    if ( $("#nda_list_cbrm").length != 0 ) 
    {
        var nda_list_cbrm = $('#nda_list_cbrm').dataTable( {

			  "sPaginationType": "full_numbers",
			  "sAjaxSource": base_url + "/nte/nda/get_ac",
			  "oLanguage": {
				"sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>"
			  },
			  "fnDrawCallback": function( oSettings ) {
                  
			   },
			  "aLengthMenu": [
				[10, 20, 50, 100, 200, -1],
				[10, 20, 50, 100, 200, "All"]
				], 
			  "aoColumns": [
			  	{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": true },
				{ "sClass": "td_center", "bSortable": false, "sWidth": "20%" },
			]
		});

		nda_list_cbrm.fnSort([[0, 'desc']]);
    }
});