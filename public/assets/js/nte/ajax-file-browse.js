	var validType = [];
	var invalidTypeMessage = '';

	var file_size_limit = 20971520;
	
	var pdfType = ["application/pdf"];
	var mswordType = ["application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"];
	var msxlsType = ["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-excel"];
	var imgType = ["image/jpeg", "image/png", "image/exif", "image/bmp", "image/gif", "image/tiff"];

	var bs_file_counter = 0;
	var bs_file_size_counter = 0;

	var file_counter = 0;
	var file_size_counter = 0;

	$(".browse").on('click', function() {
        b_id = this.id;
        
        switch (b_id) {
        	case 'mraf':
        		validType = msxlsType;
        		invalidTypeMessage = 'You may only upload Excel Files.';
        		file_count_limit = 1;
        		file_counter = mraf_ctr;
        		break;
        	case 'pdq':
        		validType = $.merge($.merge(mswordType, pdfType), imgType);
        		invalidTypeMessage = 'You may only upload Word, PDF and Picture Files.';
        		file_count_limit = 1;
        		file_counter = pdq_ctr;
        		break;
        	case 'oc':
        		validType = $.merge($.merge(mswordType, pdfType), imgType);
        		invalidTypeMessage = 'You may only upload Word, PDF and Picture Files.';
        		file_count_limit = 1;
        		file_counter = oc_ctr;
        		break;
        	case 'files':
        		file_count_limit = 5;
        		file_counter = files_ctr;
        		break;
    		case 'csmd':
    			file_count_limit = 20;
    			file_counter = csmd_ctr;
    			break;
			case 'ssmd':
    			file_count_limit = 1;
    			file_counter = ssmd_ctr;
    			break;

        	default:
        		break;
        }

		bs_file_counter = file_counter;
        bs_file_size_counter = file_size_counter;

    });

	var browses = document.getElementsByClassName('browse');

	for (var i = 0; i < browses.length; i++) 
	{
		var b_id = browses[i].id;
		$("#"+b_id).fileupload({
			beforeSend: function(e, data) {
				if (validType.length > 0) 
				{
					var fileType = data.originalFiles[0].type;
					if (!validate_file_type(fileType, validType)) 
					{
						alert(invalidTypeMessage);
						return false;
					}

					validType = [];
				};
              	if((file_size_counter + data.total > file_size_limit) || (bs_file_size_counter + data.total > file_size_limit)) 
              	{
                	alert('You have already exceeded to the file size allowed.');
	                return false;
              	}
              	if(file_counter > (file_count_limit - 1) || bs_file_counter > (file_count_limit - 1))
              	{
	                alert('You have already exceeded to the number allowed of attachments.');
	                return false;
              	}

	            bs_file_counter++;
	            bs_file_size_counter += data.total;
            },
            url: urlUpload,
			dataType: 'json',
			done: function (e, data) {
				$.each(data.result.files, function (index, file) {
					row_attachment = "<p>";
					row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][filesize]' value='"+file.filesize+"'>";
					row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
					row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
					row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
					row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
					row_attachment += file.original_filename + ' | ' + convert(file.filesize);
					row_attachment += "&nbsp; <button type='button' id="+b_id+" class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>";
					row_attachment += "</p>";
					$("#"+b_id+"_attachments").append(row_attachment);
					file_counter++;
					file_size_counter += file.filesize;

					changeVariablesValues(b_id, file.filesize, true);

				});

				if ( file_counter == (file_count_limit) ) 
					document.getElementById(b_id).disabled = true;
			}
		});
	};

	function changeVariablesValues (c_id, filesize, isIncrement) 
	{
		switch (c_id)
		{
			case 'mraf':
				isIncrement ? mraf_ctr++ : mraf_ctr--;
				isIncrement ? mraf_size_ctr += filesize : mraf_size_ctr -= filesize;
				break;
			case 'pdq':
				isIncrement ? pdq_ctr++ : pdq_ctr--;
				isIncrement ? pdq_size_ctr += filesize : pdq_size_ctr -= filesize;
				break;
			case 'oc':
				isIncrement ? oc_ctr++ : oc_ctr--;
				isIncrement ? oc_size_ctr += filesize : oc_size_ctr -= filesize;
				break;
			case 'files':
				isIncrement ? files_ctr++ : files_ctr--;
				isIncrement ? files_size_ctr += filesize : files_size_ctr -= filesize;
				break;
			case 'csmd':
				isIncrement ? csmd_ctr++ : csmd_ctr--;
				isIncrement ? csmd_size_ctr += filesize : csmd_size_ctr -= filesize;
				break;
			case 'ssmd':
				isIncrement ? ssmd_ctr++ : ssmd_ctr--;
				isIncrement ? ssmd_size_ctr += filesize : ssmd_size_ctr -= filesize;
				break;

			default:
				break;

		}
	}
	
	function validate_file_type(fileType, validArray) 
	{
		valid = false;

		$.each(validArray, function( key, value ) 
		{
			valid = !valid ? fileType === value ? true : false : true;
		});

		return valid;
	}

	/* String.prototype.trunc = String.prototype.trunc ||
	function(n, se = true) {
	  return (this.length > n) ? se ? this.substr(0,n-1)+'&hellip;'+this.split('.').pop() : this.substr(0,n-1)+'&hellip;' : this;
	}; */


	function convert(size) 
	{
	    var n;

	    if (size >= 1073741824)
	    {
	      n = size / 1073741824;
	      n = n.toFixed(5) + ' GB' ;
	    }
	    else if (size >= 1048576)
	    {
	      n = size / 1048576;
	      n = n.toFixed(5) + ' MB' ;
	    }
	    else if (size >= 1024)
	    {
	      n = size / 1024;
	      n = n.toFixed(5) + ' kB' ;
	    }
	    else if (size >= 1)
	    {
	      n = size + ' bytes';
	    }
	    else if (size == 1)
	    {
	      n = size + ' byte';
	    }
	    else
	    {
	      n = '0 bytes';
	    }

	    return n;
  	};

  	/*$(document).on('click', 'button.confirm-delete', function () 
  	{
	    var this_file_size = parseInt(this.parentNode.childNodes[1]);
		var d_id = this.id;

		changeVariablesValues(d_id, this_file_size, false);
		document.getElementById(d_id).disabled = false;

	    $(this).closest('p').remove();
	    rename_attachments();
  	});*/

  	function rename_attachments() 
  	{
		var attachments = document.getElementsByClassName('atch');
		var a_id = '';

	    for (var v = 0; v < attachments.length; v++) 
	    {
	    	a_id = attachments[v].id.split('_')[0];
	      	for (var w = 0; w < attachments[v].childNodes.length; w++) 
	      	{
		        attachments[v].childNodes[w].childNodes[0].setAttribute('name', a_id+'['+w+'][filesize]');
		        attachments[v].childNodes[w].childNodes[1].setAttribute('name', a_id+'['+w+'][mime_type]');
		        attachments[v].childNodes[w].childNodes[2].setAttribute('name', a_id+'['+w+'][original_extension]');
		        attachments[v].childNodes[w].childNodes[3].setAttribute('name', a_id+'['+w+'][original_filename]');
		        attachments[v].childNodes[w].childNodes[4].setAttribute('name', a_id+'['+w+'][random_filename]');
	      	};
	    };
  	}
