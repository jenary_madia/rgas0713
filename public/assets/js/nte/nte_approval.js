var add_month_year;
var change_sev;

$(document).ready(function () 
{

  "use strict";

  function pushArray(origin, addArray)
  {
    for (var i = 0; i < addArray.length; i++) 
    {
      origin.push(addArray[i]);
    };

    return origin;
  }

  get_month_name = function get_month_name(month) 
  {
    var month_name;

    month == '01' ? month_name = 'January' : '';
    month == '02' ? month_name = 'February' : '';
    month == '03' ? month_name = 'March' : '';
    month == '04' ? month_name = 'April' : '';
    month == '05' ? month_name = 'May' : '';
    month == '06' ? month_name = 'June' : '';
    month == '07' ? month_name = 'July' : '';
    month == '08' ? month_name = 'August' : '';
    month == '09' ? month_name = 'September' : '';
    month == '10' ? month_name = 'October' : '';
    month == '11' ? month_name = 'November' : '';
    month == '12' ? month_name = 'December' : '';

    return month_name;
  }

  rename_row_uploads();

  /*var nte_tab = document.getElementById("nte_table_body");
  var last_row = nte_tab.rows.length;

  for (var d = 0; d < last_row; d++) 
  {
    initFileBrowse("#nte-browse"+d);
  };*/

  initFileBrowse(".flup");

  function selected_to_delete() 
  {
    var selected = $('.sel_chk:checkbox:checked');
    if (selected.length > 0) 
    {
      delete_nte_rows(selected);
    } 
    else 
    {
      alert('PLEASE SELECT A ROW.');
    }
  }

  function delete_nte_rows(selectedBox) 
  {
    for (var i=selectedBox.length - 1; i >= 0; i--) 
    {
      selectedBox[i].closest('tr').remove();
      i == 0 ? rename_row_uploads() : '';
    }
  }

  $('#return').on('click',function(event) 
  {
    action = 4
    $("#nte_add_form").submit();
  });

  $('#send_cpa').on('click',function(event) 
  {
    action = 5;
    $("#nte_add_form").submit();
  });

  $( "#nte_add_form" ).submit(function( event ) 
  {
    var sData = $( this ).serializeArray();
    var addArray = [
        {name : 'action', value : action},
        {name : '_token', value : token},
      ];
    event.preventDefault();
    var nte_tab = document.getElementById("nte_table_body");
    var selected = $('input:checkbox:checked');

    if (selected.length > 0) 
    {
      $.ajax({
        method: 'POST',
        url: url,
        data: pushArray(sData, addArray)
      }).done(function(data) 
      {
        var message = $.parseJSON(data);
        var msg_cont = document.getElementById('container_message');
        msg_cont.style.display = "block";
        $('.container_message').text("");

        if (message.scs) 
        {
          selected_to_delete(false, true);
          $("#container_message").removeClass('alert-danger');
          $("#container_message").addClass('alert-success');

          $("#chk_all").attr('checked', false);
          rename_row_uploads();

          nte_tab.rows.length > 0 ? '' : window.location = urlNteListCHRD;
        } 
        else 
        {
          $("#container_message").removeClass('alert-success');
          $("#container_message").addClass('alert-danger');
        }

        for (var i = 0; i < message.msg.length; i++) {            
          $('.container_message').append(message.msg[i].toUpperCase() + '<br>');
        };
      });
    } 
    else 
    {
      alert('PLEASE SELECT A ROW.');
    }
  });

  $("#chk_all").on('click', function () 
  {
    $("#chk_all").is(":checked") ? $(".sel_box").val(1) : $(".sel_box").val(0);
    $("#chk_all").is(":checked") ? $(".sel_chk").attr("checked", true) : $(".sel_chk").attr("checked", false);
  })
  
  $(document).on('click', '.sel_chk', function () 
  {
    var sel_box = $($(this.parentNode).children()[0]);
    this.checked ? sel_box.val(1) : sel_box.val(0);
  });
});
