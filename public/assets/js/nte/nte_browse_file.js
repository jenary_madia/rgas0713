var initFileBrowse;
var append_row_uploaded;
var rename_row_uploads;
var rename_attachments;
var convert;

var file_size_limit = 20971520;
var file_count_limit = 10;

var rowIndex = 0;
var attachment = 0;

var file_size_counter = 0;
var file_counter = 0;

var bs_file_counter = 0;
var bs_file_size_counter = 0;

$(document).ready( function () 
{
	"use strict";

	initFileBrowse = function initFileBrowse(element)
  {
 	$(element).on('click', function ()
  	{
      rowIndex = (this.parentNode.parentNode.parentNode.parentNode.rowIndex)-1;
      attachment = this.parentNode.parentNode.childNodes[0];
      file_size_counter = attachment.childNodes[0];
      file_counter = attachment.childNodes[1];

      bs_file_counter = parseInt(file_counter.value);
      bs_file_size_counter = parseInt(file_size_counter.value);
  	});

  	$(element).fileupload(
  	{
    	beforeSend: function(e, data) 
    	{
      	if((parseInt(file_size_counter.value) + data.total > file_size_limit) || (bs_file_size_counter + data.total > file_size_limit)) {
        	alert('You have already exceeded to the file size allowed.');
        	return false;
      	}

      	if(parseInt(file_counter.value) > file_count_limit-1 || (bs_file_counter > file_count_limit-1)){
        	alert('You have already exceeded to the number allowed of attachments.');
        	return false;
      	}

        bs_file_counter += 1;
        bs_file_size_counter += data.total;
    	},
    	url: urlUpload,
    	dataType: 'json',
    	done: function (e, data) 
    	{
      	$.each(data.result.files, function (index, file) 
      	{
          append_row_uploaded(rowIndex, attachment, file);
          rename_row_uploads();

          file_counter.value = parseInt(file_counter.value) + 1;
          file_size_counter.value = parseInt(file_size_counter.value) + file.filesize;
      	});
    	}
  	});
  }

  $(".browse").on('click', function() 
  {
    b_id = this.id;

    switch (b_id) {
      case 'files':
        file_count_limit = 10;
        file_counter = files_ctr;
        file_size_counter = files_size_ctr;
        break;
      case 'emp':
        file_count_limit = 5;
        file_counter = emp_ctr;
        file_size_counter = emp_size_ctr;
        break;
      case 'dept':
        file_count_limit = 5;
        file_counter = dept_ctr;
        file_size_counter = dept_size_ctr;
        break;
      case 'ndaf':
        file_count_limit = 5;
        file_counter = ndaf_ctr;
        file_size_counter = ndaf_size_ctr;
        break;
      case 'ndah':
        file_count_limit = 5;
        file_counter = ndah_ctr;
        file_size_counter = ndah_size_ctr;
        break;
      case 'multi':
        file_count_limit = 5;
        file_counter = multi_ctr;
        file_size_counter = multi_size_ctr;
        break;

      default:
        break;
    }

    bs_file_counter = file_counter;
    bs_file_size_counter = file_size_counter;
  });

  var browses = document.getElementsByClassName('browse');

  for (var i = 0; i < browses.length; i++) 
  {
    var b_id = browses[i].id;
    $("#"+b_id).fileupload({
      beforeSend: function(e, data) 
      {
        if((file_size_counter + data.total > file_size_limit) || (bs_file_size_counter + data.total > file_size_limit)) 
        {
          alert('You have already exceeded to the file size allowed.');
          return false;
        }
        if(file_counter > (file_count_limit - 1) || bs_file_counter > (file_count_limit - 1))
        {
          alert('You have already exceeded to the number allowed of attachments.');
          return false;
        }

        bs_file_counter++;
        bs_file_size_counter += data.total;
      },
      url: urlUpload,
      dataType: 'json',
      done: function (e, data) 
      {
        $.each(data.result.files, function (index, file) 
        {
          var row_attachment = "<p>";
          row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][filesize]' value='"+file.filesize+"'>";
          row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
          row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
          row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
          row_attachment += "<input type='hidden' name='"+b_id+"["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
          row_attachment += file.original_filename + ' | ' + convert(file.filesize);
          row_attachment += "&nbsp; <button type='button' id="+b_id+" class='btn btn-xs btn-danger remove-fn nte-indi-delete'>DELETE</button>";
          row_attachment += "</p>";
          $("#"+b_id+"_attachments").append(row_attachment);
          file_counter++;
          file_size_counter += file.filesize;

          changeVariablesValues(b_id, file.filesize, true);

        });

        if ( file_counter == (file_count_limit) ) 
          document.getElementById(b_id).disabled = true;
      }
    });
  };

  function changeVariablesValues (c_id, filesize, isIncrement) 
  {
    switch (c_id)
    {
      case 'files':
        isIncrement ? files_ctr++ : files_ctr--;
        isIncrement ? files_size_ctr += filesize : files_size_ctr -= filesize;
        break;
      case 'emp':
        isIncrement ? emp_ctr++ : emp_ctr--;
        isIncrement ? emp_size_ctr += filesize : emp_size_ctr -= filesize;
        break;
      case 'dept':
        isIncrement ? dept_ctr++ : dept_ctr--;
        isIncrement ? dept_size_ctr += filesize : dept_size_ctr -= filesize;
        break;
      case 'ndaf':
        isIncrement ? ndaf_ctr++ : ndaf_ctr--;
        isIncrement ? ndaf_size_ctr += filesize : ndaf_size_ctr -= filesize;
        break;
      case 'ndah':
        isIncrement ? ndah_ctr++ : ndah_ctr--;
        isIncrement ? ndah_size_ctr += filesize : ndah_size_ctr -= filesize;
        break;
      case 'multi':
        isIncrement ? multi_ctr++ : multi_ctr--;
        isIncrement ? multi_size_ctr += filesize : multi_size_ctr -= filesize;
        break;

      default:
        break;
    }
  }

  $(document).on('click', 'button.nte-indi-delete', function () 
  {
    var this_file_size = parseInt($(this.parentNode.childNodes[0]).val());
    var d_id = this.id;

    changeVariablesValues(d_id, this_file_size, false);
    document.getElementById(d_id).disabled = false;

    $(this).closest('p').remove();
    rename_attachments();
  });

  append_row_uploaded = function append_row_uploaded(rowIndex, attachment, file) 
  {
  	var p = document.createElement('p');

  	var pText = document.createTextNode(file.original_filename + ' | ' + convert(file.filesize) + '\u00a0');

  	var delButton = document.createElement('button');
  	var delText = document.createTextNode('-');
  	var delClass = document.createAttribute('class');
  	var delType = document.createAttribute('type');

  	delClass.value = 'btn btn-danger btndanger nte-row-upload nte-delete';
  	delType.value = 'button';

  	delButton.appendChild(delText);
  	delButton.setAttributeNode(delType);
  	delButton.setAttributeNode(delClass)

  	var inputFileSize = document.createElement('input');
  	var inputMimeType = document.createElement('input');
  	var inputOriginalExtension = document.createElement('input');
  	var inputOriginalFilename = document.createElement('input');
  	var inputRandomFilename = document.createElement('input');

  	var inputTypeFileSize = document.createAttribute('type');
  	inputTypeFileSize.value = 'hidden';
  	var inputTypeMimeType = document.createAttribute('type');
  	inputTypeMimeType.value = 'hidden';
  	var inputTypeOriginalExtension = document.createAttribute('type');
  	inputTypeOriginalExtension.value = 'hidden';
  	var inputTypeOriginalFilename = document.createAttribute('type');
  	inputTypeOriginalFilename.value = 'hidden';
  	var inputTypeRandomFilename = document.createAttribute('type');
  	inputTypeRandomFilename.value = 'hidden';

  	var inputValueFileSize = document.createAttribute('value');
  	inputValueFileSize.value = file.filesize;
  	var inputValueMimeType = document.createAttribute('value');
  	inputValueMimeType.value = file.mime_type;
  	var inputValueOriginalExtension = document.createAttribute('value');
  	inputValueOriginalExtension.value = file.original_extension
  	var inputValueOriginalFilename = document.createAttribute('value');
  	inputValueOriginalFilename.value = file.original_filename;
  	var inputValueRandomFilename = document.createAttribute('value');
  	inputValueRandomFilename.value = file.random_filename;

  	inputFileSize.setAttributeNode(inputTypeFileSize);
  	inputFileSize.setAttributeNode(inputValueFileSize);
  	inputMimeType.setAttributeNode(inputTypeMimeType);
  	inputMimeType.setAttributeNode(inputValueMimeType);
  	inputOriginalExtension.setAttributeNode(inputTypeOriginalExtension);
  	inputOriginalExtension.setAttributeNode(inputValueOriginalExtension);
  	inputOriginalFilename.setAttributeNode(inputTypeOriginalFilename);
  	inputOriginalFilename.setAttributeNode(inputValueOriginalFilename);
  	inputRandomFilename.setAttributeNode(inputTypeRandomFilename);
  	inputRandomFilename.setAttributeNode(inputValueRandomFilename);
  
  	p.appendChild(inputFileSize);
  	p.appendChild(inputMimeType);
  	p.appendChild(inputOriginalExtension);
  	p.appendChild(inputOriginalFilename);
  	p.appendChild(inputRandomFilename);
   	p.appendChild(pText);
  	p.appendChild(delButton);

  	attachment.appendChild(p);
  }

  rename_row_uploads = function rename_row_uploads() 
  {
  	var attachments = document.getElementsByClassName('atch');
      
  	for (var v = 0; v < attachments.length; v++) 
    {
    	for (var w = 2; w < attachments[v].childNodes.length; w++) 
      {
        attachments[v].childNodes[w].childNodes[0].setAttribute('name', 'files['+v+']['+(w-2)+'][filesize]');
        attachments[v].childNodes[w].childNodes[1].setAttribute('name', 'files['+v+']['+(w-2)+'][mime_type]');
        attachments[v].childNodes[w].childNodes[2].setAttribute('name', 'files['+v+']['+(w-2)+'][original_extension]');
        attachments[v].childNodes[w].childNodes[3].setAttribute('name', 'files['+v+']['+(w-2)+'][original_filename]');
        attachments[v].childNodes[w].childNodes[4].setAttribute('name', 'files['+v+']['+(w-2)+'][random_filename]');
    	};
  	};
  }

  rename_attachments = function rename_attachments() 
  {
    var attachments = document.getElementsByClassName('atch');
    var a_id = '';

    for (var v = 0; v < attachments.length; v++) 
    {
      a_id = attachments[v].id.split('_')[0];
        for (var w = 0; w < attachments[v].childNodes.length; w++) 
        {
          attachments[v].children[w].children[0].setAttribute('name', a_id+'['+w+'][filesize]');
          attachments[v].children[w].children[1].setAttribute('name', a_id+'['+w+'][mime_type]');
          attachments[v].children[w].children[2].setAttribute('name', a_id+'['+w+'][original_extension]');
          attachments[v].children[w].children[3].setAttribute('name', a_id+'['+w+'][original_filename]');
          attachments[v].children[w].children[4].setAttribute('name', a_id+'['+w+'][random_filename]');
        };
    };
  }

  $(document).on('click', 'button.nte-delete', function () 
  {
  	var this_file_size = this.parentNode.childNodes[0];
  	var file_size_counter = this.parentNode.parentNode.childNodes[0];
  	var file_counter = this.parentNode.parentNode.childNodes[1];

  	file_size_counter.value = parseInt(file_size_counter.value) - parseInt(this_file_size.value);
  	file_counter.value = parseInt(file_counter.value) - 1;

  	$(this).closest('p').remove();
  	rename_row_uploads();
  });

  convert = function convert(size) 
  {
    var n;

    if (size >= 1073741824)
    {
      n = size / 1073741824;
      n = n.toFixed(5) + ' GB' ;
    }
    else if (size >= 1048576)
    {
      n = size / 1048576;
      n = n.toFixed(5) + ' MB' ;
    }
    else if (size >= 1024)
    {
      n = size / 1024;
      n = n.toFixed(5) + ' kB' ;
    }
    else if (size >= 1)
    {
      n = size + ' bytes';
    }
    else if (size == 1)
    {
      n = size + ' byte';
    }
    else
    {
      n = '0 bytes';
    }

    return n;
  };
});