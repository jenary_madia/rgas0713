$(document).ready(function() 
{
  "use strict";

  rename_attachments();

  !drafted ? $("#departmentm").val(0) : '';
  !drafted ? $("#infractionm").val(0) : '';
  
  $("#departmentm").multiselect({
    open: function () 
    {
      $(".ui-multiselect-all").hide();
      $(".ui-multiselect-none").hide();
    },
    click: function(event, ui) {
      if (ui.checked)
        get_employees([ui.value]);    
      else
      {
        if ($('#box_'+ui.value+' input:checked').length > 0);
          if (confirm('there are selected persons uder this department do you like to remove them?'))
          {
            $($('#box_'+ui.value)[0].previousElementSibling).remove();
            $('#box_'+ui.value).remove();
          }
          else
            return false;
      }
    },
  }).multiselectfilter();

  get_employees($("#departmentm").val());

  $("#infractionm").multiselect({}).multiselectfilter();

  function get_employees(selectedDept) {
    $.ajax({
      method: 'POST',
      url: url,
      data: {data : selectedDept, _token : token},
      beforeSend: function () 
      {
        $("#departmentm").multiselect('disable');
      }
    }).done(function(data) 
    {
      console.log(selectedDept);
      var count1 = '';
      var count2 = '';

      var jData = $.parseJSON(data);
      var jDataEmployees = jData['employees'];
      var jDataDepartments = jData['departments'];

      var employees = jDataEmployees;
      var departments = jDataDepartments;

      for (var i = 0; i < departments.length; i++) 
      {
        var emps = [];
        $('#selections').append('<div class="clear_20"></div><div id="box_'+departments[i]['id']+'" class="col-sm-12" style="padding: 0px;"></div>');
        $('#box_'+departments[i]['id']).append('<label class="labels"><U>'+departments[i]['dept_name']+'</U></label><br><br>');
        $('#box_'+departments[i]['id']).append('<div id="selection1_'+departments[i]['id']+'" class="col-sm-6"></div><div id="selection2_'+departments[i]['id']+'" class="col-sm-6"></div>');

        for (var j = 0; j < employees.length; j++) 
          if (employees[j]['departmentid'] == departments[i]['id']) 
            emps.push([employees[j]['id'], employees[j]['name']]);

        count1 = emps.length / 2;
        count1 = Math.ceil(count1);
        count2 = emps.length - count1;

        $('#selection1_'+departments[i]['id']).text("");
        $('#selection2_'+departments[i]['id']).text("");

        for (var a = 0; a < count1 ; a++) 
        {
          var tagSelectedA = '';
          for (var z=0; z < draftsEmployees.length; z++)
            emps[a][0] === draftsEmployees[z]['nde_employee_id'] ? tagSelectedA = 'checked' : '';

          $('#selection1_'+departments[i]['id']).append('<div class="input-group"><label><span class="input-group-addon"><input type="checkbox" class="emp" value="'+emps[a][0]+'" name="employee[]"'+tagSelectedA+'></span><span class="form-control" style="width: 500px;"><b>'+emps[a][1]+'</b></span></label></div>');
        };

        for (var b = count1; b < count2+count1; b++) 
        {
          var tagSelectedB = '';
          for (var y=0; y < draftsEmployees.length; y++)
            emps[b][0] === draftsEmployees[y]['nde_employee_id'] ? tagSelectedB = 'checked' : '';
          
          $('#selection2_'+departments[i]['id']).append('<div class="input-group"><label><span class="input-group-addon"><input type="checkbox" class="emp" value="'+emps[b][0]+'" name="employee[]"'+tagSelectedB+'></span><span class="form-control" style="width: 500px;"><b>'+emps[b][1]+'</b></span></label></div>');
        };
      };

        $("#departmentm").multiselect('enable');

    });
  };

  function process_multiple(processor)
  {
    var link = processor ? red_add : multi_save;

    var message = [];
    var selectedBox = [];

    var selected = $('.emp:checked');

    for (var i=0; i < selected.length; i++) 
      selectedBox.push(parseInt(selected[i].getAttribute('value')));

    selectedBox.length == 0 ? message.push('Please select atleast 1 Employee.') : '';
    $("#infractionm").val() == null ? message.push('Please select atleast 1 Infraction.') : '';

    if (message.length == 0) 
    {
      document.nte_add_multi_form.action = link;
      $("#nte_add_multi_form").submit();
    } 
    else 
    {
      event.preventDefault();
      var error_cont = document.getElementById('container_message');
      error_cont.style.display = "block";
      $('.container_message').text("");

      for (var l = 0; l < message.length; l++) 
        $('.container_message').append('<p>' + message[l] + '</p> <br/>');
    }
  }

  $("#save").on('click', function() 
  {
    var selectedBox = [];
    var selected = $('input:checkbox:checked');

    for (var i=0; i < selected.length; i++) 
      selectedBox.push(parseInt(selected[i].getAttribute('value')));

  });

  $("#save").on('click', function() 
  {
      process_multiple(false);
  });  

  $("#proceed").on('click', function() 
  {
      process_multiple(true);
  });
});

//var selectedInfras = [];
    //var selectedInfra = document.getElementsByClassName('sel_infra');
    /*for (var j=0; j < selectedInfra.length; j++) 
    {
      selectedInfra[j].value != "" ? selectedInfras.push(parseInt(selectedInfra[j].value)) : '';
    }  */
    //selectedInfras.length == 0 ? message.push('Please select atleast 1 Infraction.') : '';

/*var selectedInfras = [];
      var selectedInfra = document.getElementsByClassName('sel_infra');
      for (var j=0; j < selectedInfra.length; j++) 
      {
        selectedInfras.push(parseInt(selectedInfra[j].value));
      }*/

/*  $("#add_infra").on('click', function() 
  {
    add_infractions();
  });*/

/*  function add_infractions () 
  {
    var infras = document.getElementById("infras");
    var last_row = infras.rows.length;
    var row = infras.insertRow(last_row);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);

    var crropts = '';

    for (var j=0; j<crrbuildup.length;j++) 
    {
      crropts += "<option value="+crrbuildup[j]['cb_crr_id']+">"+crrbuildup[j]['cb_rule_name']+"</option>";
    }

    cell1.innerHTML = "<td><label for='Infraction["+last_row+"]' class='lbl_infra labels'>Infraction "+(last_row+1)+":</label></td>";
    cell2.innerHTML = "<td><select class='form-control sel_infra' name='infraction["+last_row+"]'><option value=''>--Select--</option>"+crropts+"</select></td>";
    cell3.innerHTML = "<td><button type='button' class='removebutton btn btn-default btndefault'>DELETE</button></td>";
  }*/

/*  $(document).on('click', 'button.removebutton', function () 
  {
    var infras = document.getElementById("infras");
    var last_row = infras.rows.length;
    var lbl_infra = document.getElementsByClassName('lbl_infra');
    var sel_infra = document.getElementsByClassName('sel_infra');
    if (last_row <= 1) 
    {
      alert('YOU CANNOT DELETE THIS ROW. YOU ARE REQUIRED TO INPUT ATLEAST ONE INFRACTION.');
    } 
    else 
    {
      $(this).closest('tr').remove(); 
      var last_row = infras.rows.length;
      for (var i=0; i<last_row; i++) {
        lbl_infra[i].setAttribute('for', 'infraction['+i+']');
        lbl_infra[i].innerHTML = 'Infraction '+(i+1)+':';
        sel_infra[i].setAttribute('name', 'infraction['+i+']');
      }
    }

    return false;
  });*/

/*$( "#nte_add_multi_form" ).submit(function( event ) {
  document.nte_add_multi_form.action = red_add;
  $("#nte_add_multi_form").submit();
});*/