$(document).ready(function()
{
	"use strict";

    if($("#access").val() != "head")
    {
    	$("#section option").css("display" , "none");
    } 

    $("#department").change(function()
    {
        var id = $(this).val();
        $("#section option").css("display" , "none");
        $("#section option").each(function(key , val)
        {
            if($(this).attr("id") == id || $(this).val() == "" )  $(this).css("display" , "");
        });

        $("#section").val("");
    });

    $("#department , #year , #section , #month").change(function()
    {
        $(".btnsearch").click();
    });

    var base_url = $("base").attr("href");
    $(".btnsearch").click(function()
    {
       
        $(".tbl_list tr td").parent().remove();

        $.get(base_url + "/oam/get/list",{department : $("#department").val() , year : $("#year").val()  , section : $("#section").val()  , month : $("#month").val() }, function(data){
              $(".tbl_list").empty().append( data.list );
              $("#legend").empty().append(data.legend);


              if($(".tbl_list tr").length == 2)
              {
              	alert("No record found.");
              }
            }, "json");

    })

})