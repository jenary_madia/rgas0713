$(document).ready(function()
{
    "use strict";
    
    var base_url = $("base").attr("href");
    $(".btnsearch").click(function()
    {
      
        $(".tbl_logs tr td").parent().remove();
        $(".tbl_total tr td").parent().remove();
        $.get(base_url + "/oam/get/self", {year : $("#year").val()  ,  month : $("#month").val()}, function(data){

              $(".tbl_logs").append( data.table );
              $(".tbl_total").append( data.total );
              $(".restday").parent().addClass("label-default");
              $(".restday").parent().children(".nologs").removeClass("nologs");
              $(".nologs").parent().addClass("tr_nologs");                    
              $("#legend").empty().append(data.legend);
            }, "json");

    })

     $("#department , #year , #section , #month").change(function()
    {
        $(".btnsearch").click();
    });

    $(".tr_nologs").live("mouseover",function()
    {
        $(this).css({"cursor":"pointer"});
    })

    $(".tr_nologs").live("mouseout",function()
    {
        $(this).css({"cursor":"pointer","background-color":"inherit"});
    })

    $(".tr_nologs").live("click",function()
    {
      $('#RedirectModal').modal("show");
    })

});