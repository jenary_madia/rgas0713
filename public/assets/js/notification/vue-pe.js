var base_url = $("base").attr("href");
new Vue({
    el : "#peCreate",
    data : {
        departmentID : "",
        sectionID : "",
        sections  : [],
        employees : [],
        employeeIndex : "",
        toSend : [],
        toAdd : {
            employeeName : "",
            employeeNumber : "",
            notificationType : "",
            date : "",
            timeIn : "",
            timeOut : "",
            reason : "",
            employeeID : "",
            active : false,
        },
        toEdit : {},
        employeesToFile : [],
        employeesToFileOld : [],
        editable : false,
        deletable : false,
        processing : false,
    },
    methods : {
        saveEdit : function () {
            this.$set("processing",true);
            var that = this;
            Vue.http.post(base_url+'/ns/parse_employees',this.toEdit).then(function (response) {
                    var refId = response['data']['id'];
                    that.employeesToFile[refId]["date"] =  response['data']['date'];
                    that.employeesToFile[refId]["employeeID"] =  response['data']['employeeID'];
                    that.employeesToFile[refId]["employeeName"] =  response['data']['employeeName'];
                    that.employeesToFile[refId]["employeeNumber"] =  response['data']['employeeNumber'];
                    that.employeesToFile[refId]["notificationType"] =  response['data']['notificationType'];
                    that.employeesToFile[refId]["notifName"] =  response['data']['notifName'];
                    that.employeesToFile[refId]["reason"] =  response['data']['reason'];
                    that.employeesToFile[refId]["timeIn"] =  response['data']['timeIn'];
                    that.employeesToFile[refId]["timeOut"] =  response['data']['timeOut'];
                    that.$set("processing",false);
                    $('#editNotifDetails').modal('hide');
                },
                function () {
                    console.log("Something went wrong");
                });

            // $('#editNotifDetails').modal('hide');
        },

        deleteOffsetReference : function () {
            var r = confirm("Are you sure you want to delete this?");
            if (r == true) {
                var id = this.toEdit.id;
                this.employeesToFile.splice(id,1);
            }
        }, 
        
        chooseEmployee : function (reference,index) {
            for (var i = 0; i < this.employeesToFile.length; i++) {
                if(index != i) {
                    this.employeesToFile[i]['active'] = false;
                }else{
                    this.employeesToFile[i]['active'] = true;
                }
            }
            
            this.$set("toEdit", JSON.parse(JSON.stringify(reference)));
            this.$set("toEdit.id",index);
            this.$set("editable",true);
            this.$set("deletable",true);
            
        },
        parseToAddData : function () {
            var chosenEmployee = this.employees[this.employeeIndex];
            var fullname = chosenEmployee['firstname']+' '+chosenEmployee['middlename']+' '+chosenEmployee['lastname']
            this.$set('toAdd.employeeNumber',chosenEmployee['employeeid']);
            this.$set('toAdd.employeeName',fullname);
            this.$set('toAdd.employeeID',chosenEmployee['id']);
        },
        getToAddData : function () {
            this.$set("processing",true);
            var that = this;
            Vue.http.post(base_url+'/ns/parse_employees',this.toAdd).then(function (response) {
                    that.employeesToFile.push(response.data);
                    that.$set("processing",false);
                    $('#addNotifDetails').modal('hide');
                },
                function () {
                    console.log("Something went wrong");
                });

        },
        clear : function () {
            this.$set("toAdd.employeeName","");
            this.$set("toAdd.employeeNumber","");
            // this.$set("toAdd.notificationType","");
            this.$set("toAdd.date","");
            this.$set("toAdd.timeIn","");
            this.$set("toAdd.timeOut","");
            this.$set("toAdd.reason","");
            this.$set("toAdd.employeeID","");
            this.$set("employeeIndex","");
        }
    },
    watch : {
        "departmentID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/sections',{ "dept_id" : this.departmentID }).then(function (response) {
                    that.$set("sections",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });

            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
            this.employeesToFile = [];
        },
        "sectionID" : function () {
            var that = this;
            Vue.http.post(base_url+'/ns/employees',{"dept_id" : this.departmentID ,"sect_id" : this.sectionID }).then(function (response) {
                    that.$set("employees",response.data);
                },
                function () {
                    console.log("Something went wrong");
                });
            this.employeesToFile = [];
        },
        "employeesToFile" : function () {
            if (this.employeesToFile.length == 0) {
                this.$set("editable",false);
                this.$set("deletable",false);
            }
        }
    }
})