/**
 * Created by Jenary on 22/07/2016.
 */
$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#superiorNotification").length != 0 ) {
        var requests_my_table = $('#superiorNotification').dataTable( {

            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/superior_notifications",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {
                $('div#superiorNotification_info').append(' <button class="btn btn-default btndefault" id="approveSelected" name="action" value="batchApprove">APPROVE</button>');
                $("#approveSelected").on("click", function(){
                    var toApproved = [];
                    $('input:checked.chooseLeave').each(function () {
                        toApproved.push($(this).val());
                    });

                });

                $("#chooseAll_superiorNotification").on("click",function(){
                    $('input:checkbox.chosenSuperiorNotification').prop('checked', this.checked);
                });
            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%","bSortable" : true},
                { "sWidth": "15%","bSortable" : true},
                { "sWidth": "15%","bSortable" : false},
                { "bSortable" : false },
                { "bSortable" : false },
                { "bSortable" : false },
                { "sWidth": "20%","bSortable" : false},
                { "sWidth": "20%","bSortable" : false},

            ]

        });

    }

});


$("#formApproveNotif").on("submit", function(e){
    var r = confirm("Approve selected notifications?");
    if (r == true) {
        $("#formApproveNotif").submit;
    }else{
        e.preventDefault();
    }
});
