"use strict";
var base_url = $("base").attr("href");
new Vue({
    el: '#app',
    data: {
        formDetails : {
            employeeName : "",
            employeeNumber : "",
            dateFiled : "",
            company : "",
            status : "",
            department : "",
            contactNumber : "",
            section : ""
        },
        notificationType: '',
        offsetReference : {
            OTDate : "",
            type : "",
            timeInStartTime : "",
            timeOutEndTime : "",
            OTDuration : "",
            reason : "",
            OBDestination : "",
        },
        forEditReference : {},
        offsetDateReady : false,
        listOffsetReferenceInputOld : "",
        offset : {
            hoursFrom : 0,
            hoursTo : 0,
            dateFrom : "",
            dateTo : "",
            duration : "",
            scheduleType : "",
            periodTo : "",
            periodFrom : "",
            totalDays : 0,
            totalHours : 0,
            listOffsetReferences : [],
            totalListOffsetHours : 0,
            reason : ""
        },
        offsetPeriod : [
            "morning",
            "afternoon",
            "whole"
        ],
        offsetPeriodEditable : true,
        undertime : {
            scheduleType : "",
            day : "",
            scheduleTimeout : "",
            date : "",
            timeout : "",
            reason : ""
        },
        timeKeeping : {
            date : "",
            toCorrect : "",
            timeIn : "",
            timeOut: "",
            salaryAdjustment : 0,
            reason : ""
        },
        officialBusiness : {
            duration : "",
            dateFrom : "",
            fromStart : "",
            fromEnd : "",
            toStart : "",
            toEnd : "",
            dateTo : "",
            totalDays : 0,
            reason : "",
            destination : ""
        },
        cutTime : {
            dateFrom : "",
            dateTo : "",
            totalDays : "",
            totalHours : "",
            fromTimeInOut : "",
            toTimeInOut : "",
            reason : ""
        },

        submitStatus : {
            success : false,
            errors : [],
            message : "",
            submitted : false
        },

        editable : false,
        deletable : false,
        comment : "",
        attachments : [],
        addable : false,
        processing : false,
    },
    computed : {
        checkDateFormat : function () {
            var date_regex =  /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ ;
            if(!(date_regex.test(this.offsetReference.OTDate)))
            {
                return false;
            }
            return true;
        },
        checkforEditDateFormat : function () {
            var date_regex =  /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ ;
            if(!(date_regex.test(this.forEditReference.OTDate)))
            {
                return false;
            }
            return true;
        },
        getTotalListOffsetHours : function () {
            var listOffsetHours = 0;
            for (var i = 0; i < this.offset.listOffsetReferences.length; i++) {
                listOffsetHours += parseInt(this.offset.listOffsetReferences[i]['OTDuration']);
            }
            this.$set("offset.totalListOffsetHours",listOffsetHours);
        },

        getASD: function () {
            return JSON.stringify(this.offset.listOffsetReferences);
        },

        getOffsetTotalHours: function() {
            if(this.offset.duration != "whole" || this.offset.duration != "half") {
                var scheduleType = this.offset.scheduleType;
                var totalDays = this.offset.totalDays;
                var hoursFrom = (this.offset.hoursFrom == "" ? 0 : parseFloat(this.offset.hoursFrom));
                var hoursTo = (this.offset.hoursTo == "" ? 0 : parseFloat(this.offset.hoursTo));
                var total = hoursFrom + hoursTo;
                if( scheduleType != "special") {
                    var totalHours = 0;
                    if(scheduleType == "compressed") {
                        var hr_per_day = 9;
                    }else if (scheduleType == "regular") {
                        var hr_per_day = 8;
                    }
                    totalHours = totalDays * hr_per_day;
                    if(this.offset.periodFrom == "half") {
                        totalHours - hr_per_day / 2;
                    }

                    if(this.offset.periodTo == "half") {
                        totalHours - hr_per_day / 2;
                    }

                    this.$set("offset.totalHours", totalHours);
                }else{
                    this.$set("offset.totalHours", isNaN(total) ? 0 : total);
                }
            }
        },

        getTotalDays: function() {
            var duration = this.offset.duration;
            var startDate = new Date(this.offset.dateFrom);
            var endDate = new Date(this.offset.dateTo);
            var restDays = [22,22];
            if (duration == "multiple") {
                if(this.offset.scheduleType == "compressed") {
                    var wholeHr = 9;
                    var halfHr = 4.5
                    restDays = [6,0];
                }
                else if(this.offset.scheduleType == "regular") {
                    var wholeHr = 8;
                    var halfHr = 4;
                    restDays = [0];
                }

                if ((startDate >= endDate) || (startDate == endDate)) {
                    return 0;
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        diffDays = Math.round(Math.abs((a - b)/(c)));
                    var counter = 1;
                    for (var i = 0; i < diffDays; i++) {
                        startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
                        var dateFormated = startDate.toISOString().substr(0,10);
                        var finalDate = new Date(dateFormated);
                        var day = finalDate.getDay();
                        var checkIfrestDay = restDays.indexOf(day);
                        if (checkIfrestDay == -1)
                        {
                            counter += 1;
                        }
                        startDate = new Date(dateFormated);
                    }
                    if(this.offset.periodFrom == "afternoon" || this.offset.periodFrom == "morning") {
                        counter = counter - .5;
                        this.offset.hoursFrom = halfHr;
                    }else if (this.offset.periodFrom == "whole"){
                        this.offset.hoursFrom = wholeHr;
                    }

                    if(this.offset.periodTo == "afternoon" || this.offset.periodTo == "morning") {
                        counter = counter - .5;
                        this.offset.hoursTo = halfHr;
                    }else if (this.offset.periodTo == "whole"){
                        this.offset.hoursTo = wholeHr;
                    }
                    this.$set("offset.totalDays",counter);
                    this.$set("offset.totalHours",counter * wholeHr);
                }
                // this.$set("offset.hoursFrom","");
            }
            else if(duration == "one") {
                if(this.offset.scheduleType == "compressed") {
                    this.$set("offset.hoursFrom",9);
                }
                else if(this.offset.scheduleType == "regular") {
                    this.$set("offset.hoursFrom",8);
                }
                else if(this.offset.scheduleType == "special") {
                    this.$set("offset.hoursFrom","");
                }
                this.$set("offset.totalDays",1);
            }else if(duration == "half"){
                if(this.offset.scheduleType == "compressed") {
                    this.$set("offset.hoursFrom",4.5);
                }
                else if(this.offset.scheduleType == "regular") {
                    this.$set("offset.hoursFrom",4);
                }
                else if(this.offset.scheduleType == "special") {
                    this.$set("offset.hoursFrom","");
                }
                this.$set("offset.totalDays",.5);
            }
        },
        
        getOBTotalDays : function () {
            var duration = this.officialBusiness.duration;
            var startDate = new Date(this.officialBusiness.dateFrom);
            var endDate = new Date(this.officialBusiness.dateTo);
            if (duration == "multiple"){
                if(startDate == 'Invalid Date'|| endDate == 'Invalid Date') {
                    return 0;
                }
                var total = 0;
                if ((startDate >= endDate)) {
                    total = 0;
                }else{
                    var a = startDate.getTime(),
                        b = endDate.getTime(),
                        c = 24*60*60*1000,
                        total = Math.round(Math.abs((a - b)/(c)));
                }
                this.$set("officialBusiness.totalDays",total + 1);
            }
            else if(duration == "one") {
                this.$set("officialBusiness.totalDays",1);
            }else if(duration == "half"){
                this.$set("officialBusiness.totalDays",.5);
            }else{
                this.$set("officialBusiness.totalDays",0);
            }
        }
    },
    methods : {
        deleteOffsetReference : function () {
            if(this.deletable) {
                var r = confirm("Are you sure you want to delete?");
                if (r == true) {
                    var id = this.forEditReference.id;
                    this.offset.listOffsetReferences.splice(id,1);
                }
            }else{
                alert("Please select specific offset reference to delete.");
            }

        },
        saveEdit : function () {
            this.$set("processing",true);
            var refId = this.forEditReference.id;
            var that = this;
            Vue.http.post(base_url+'/ns/check-date-validity',{
                OTDate : this.forEditReference.OTDate,
                OTStart : this.forEditReference.timeInStartTime,
                OTEnd : this.forEditReference.timeOutEndTime
            }, {
                headers: {
                    'Cache-Control': 'no-cache'
                }
            }).then(function (response) {
                if(response.data == 0) {
                    alert("This offset reference is been filed as Overtime");
                    that.$set("forEditReference.OTDate","");
                    that.$set("forEditReference.timeInStartTime","");
                    that.$set("forEditReference.timeOutEndTime","");
                }else{
                    that.offset.listOffsetReferences[refId]["active"] =  false;
                    that.offset.listOffsetReferences[refId]["OTDate"] =  that.forEditReference.OTDate;
                    that.offset.listOffsetReferences[refId]["type"] =  that.forEditReference.type;
                    that.offset.listOffsetReferences[refId]["timeInStartTime"] =  that.forEditReference.timeInStartTime;
                    that.offset.listOffsetReferences[refId]["timeOutEndTime"] =  that.forEditReference.timeOutEndTime;
                    that.offset.listOffsetReferences[refId]["OTDuration"] =  that.forEditReference.OTDuration;
                    that.offset.listOffsetReferences[refId]["reason"] =  that.forEditReference.reason;
                    that.offset.listOffsetReferences[refId]["OBDestination"] =  that.forEditReference.OBDestination;
                    $("#offsetEditModal").modal("hide");
                    that.$set("processing",false);
                }
            });
        },
        chooseOffsetReference : function (reference,id) {
            for (var i = 0; i < this.offset.listOffsetReferences.length; i++) {
                this.offset.listOffsetReferences[i]['active'] = false;
                if(this.offset.listOffsetReferences[i] === reference) {
                    this.offset.listOffsetReferences[i]['active'] = true;
                }
            }
            this.$set("forEditReference", JSON.parse(JSON.stringify(reference)));
            this.$set("forEditReference.id",id)
            this.$set("editable",true);
            this.$set("deletable",true);
        },
        gatherData : function () {
            this.$set("processing",true);
            var that = this;
            Vue.http.post(base_url+'/ns/check-date-validity',{
                OTDate : this.offsetReference.OTDate,
                OTStart : this.offsetReference.timeInStartTime,
                OTEnd : this.offsetReference.timeOutEndTime
            }, {
                headers: {
                    'Cache-Control': 'no-cache'
                }
            }).then(function (response) {
                if(response.data == 0) {
                    alert("This offset reference is been filed as Overtime");
                    that.$set("offsetReference.OTDate","");
                    that.$set("offsetReference.timeInStartTime","");
                    that.$set("offsetReference.timeOutEndTime","");
                }else{
                    that.offset.listOffsetReferences.push({
                        "active" : false,
                        "OTDate": that.offsetReference.OTDate,
                        "type": that.offsetReference.type,
                        "timeInStartTime": that.offsetReference.timeInStartTime,
                        "timeOutEndTime": that.offsetReference.timeOutEndTime,
                        "OTDuration": that.offsetReference.OTDuration,
                        "reason": that.offsetReference.reason,
                        "OBDestination": that.offsetReference.OBDestination
                    });
                    $("#offsetAddModal").modal("hide");
                    that.$set("processing",false);
                }
            });
        },
        clearData : function () {
            this.$set("offsetReference.OTDate","");
            this.$set("offsetReference.type","");
            this.$set("offsetReference.timeInStartTime","");
            this.$set("offsetReference.timeOutEndTime","");
            this.$set("offsetReference.OTDuration","");
            this.$set("offsetReference.reason","");
            this.$set("offsetReference.OBDestination","");
        },

        checkdates : function() {
            var dateFrom = new Date(this.offset.dateFrom);
            var dateTo = new Date(this.offset.dateTo);
            if(this.offset.duration == "multiple") {
                if(dateFrom !== 'Invalid Date' && dateTo !== 'Invalid Date') {
                    if(dateFrom < dateTo) {
                        this.$set("addable",true);
                        this.dateValidation();
                    }else{
                        this.$set("addable",false);
                    }
                }else{
                    this.$set("addable",false);
                }
            }else{
                if(dateFrom != "Invalid Date") {
                    this.$set("addable",true);
                    this.dateValidation();
                }else{
                    this.$set("addable",false);
                }
            }
        },

        dateValidation : function () {
            var dateFrom = new Date(this.offset.dateFrom);
            var monthFrom = dateFrom.getMonth();
            var yearFrom = dateFrom.getFullYear();
            var dayFrom = dateFrom.getDate();
            var first = [0,1,2,3,4,5,6];
            var second = [3,4,5,6,7,8,9];
            var third = [6,7,8,9,10,11,0];
            var fourth = [9,10,11,0,1,2,3];
            var minDate = null;
            var maxDate = null;
            //Get year now
            var dateToday = new Date();
            var yearNow = dateToday.getFullYear();

            if(first.indexOf(monthFrom) != -1 && yearNow == yearFrom) {
                console.log("1st qtr");
                var filteredMonths = this.incrementEachIndex(this.getLowerMonths(first,monthFrom));
            }else if(second.indexOf(monthFrom) != -1 && yearNow == yearFrom){
                console.log("2nd qtr");
                var filteredMonths = this.incrementEachIndex(this.getLowerMonths(second,monthFrom));
            }else if(third.indexOf(monthFrom) != -1){
                console.log("3rd qtr");
                var filteredMonths = this.incrementEachIndex(this.getLowerMonths(third,monthFrom));
            }else if(fourth.indexOf(monthFrom) != -1){
                console.log("4th qtr");
                var filteredMonths = this.incrementEachIndex(this.getLowerMonths(fourth,monthFrom));
            }
            console.log("date from : "  + dateFrom);
            console.log("date today : "  + dateToday);
            if(dateFrom > dateToday) {
                dateToday.setDate(dateToday.getDate()-1);
                maxDate = dateToday;
            }else{
                dateFrom.setDate(dateFrom.getDate()-1);
                maxDate = dateFrom;
            }
            minDate = yearNow+'-'+filteredMonths[0]+'-01';
            // maxDate = yearFrom+'-'+filteredMonths[filteredMonths.length - 1]+'-'+dayFrom;

            $(".otDate").datepicker( "option", "minDate", minDate );
            $(".otDate").datepicker( "option", "maxDate", maxDate );
        },

        getLowerMonths : function (array,monthFrom) {
            var key = array.indexOf(monthFrom) + 1;
            return array.splice(0,key);
        },

        incrementEachIndex : function (array) {
            for (var i = 0; i < array.length; i++) {
                array[i] = array[i] + 1;
            }

            return array;
        },

        sendRequest : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : "send",
                formDetails : this.formDetails,
                attachments : this.attachments,
                comment : this.comment,
                notificationType : that.notificationType
            };

            if(that.notificationType == "cut_time") {
                createParams['requestDetails'] = that.cutTime;
            }else if(that.notificationType == "official") {
                createParams['requestDetails'] = that.officialBusiness;
            }else if(that.notificationType == "TKCorction") {
                createParams['requestDetails'] = that.timeKeeping;
            }else if(that.notificationType == "undertime") {
                createParams['requestDetails'] = that.undertime;
            }else if(that.notificationType == "offset") {
                createParams['requestDetails'] = that.offset;
            }
            this.fetch('/ns/action',createParams);
        },
        
        saveRequest : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : "save",
                formDetails : this.formDetails,
                attachments : this.attachments,
                comment : this.comment,
                notificationType : that.notificationType
            };

            if(that.notificationType == "cut_time") {
                createParams['requestDetails'] = that.cutTime;
            }else if(that.notificationType == "official") {
                createParams['requestDetails'] = that.officialBusiness;
            }else if(that.notificationType == "TKCorction") {
                createParams['requestDetails'] = that.timeKeeping;
            }else if(that.notificationType == "undertime") {
                createParams['requestDetails'] = that.undertime;
            }else if(that.notificationType == "offset") {
                createParams['requestDetails'] = that.offset;
            }
            this.fetch('/ns/action',createParams);
        },
        
        reSaveRequest : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : "resave",
                formDetails : this.formDetails,
                attachments : this.attachments,
                comment : this.comment,
                notificationType : that.notificationType
            };

            if(that.notificationType == "cut_time") {
                createParams['requestDetails'] = that.cutTime;
            }else if(that.notificationType == "official") {
                createParams['requestDetails'] = that.officialBusiness;
            }else if(that.notificationType == "TKCorction") {
                createParams['requestDetails'] = that.timeKeeping;
            }else if(that.notificationType == "undertime") {
                createParams['requestDetails'] = that.undertime;
            }else if(that.notificationType == "offset") {
                createParams['requestDetails'] = that.offset;
            }

            this.fetch('/ns/action/'+notif_id,createParams);
        },

        reSendRequest : function () {
            var that = this;
            $(".attachmentData").each(function() {
                that.attachments.push(JSON.parse($(this).val()));
            });
            var createParams = {
                action : "resend",
                formDetails : this.formDetails,
                attachments : this.attachments,
                comment : this.comment,
                notificationType : that.notificationType
            };

            if(that.notificationType == "cut_time") {
                createParams['requestDetails'] = that.cutTime;
            }else if(that.notificationType == "official") {
                createParams['requestDetails'] = that.officialBusiness;
            }else if(that.notificationType == "TKCorction") {
                createParams['requestDetails'] = that.timeKeeping;
            }else if(that.notificationType == "undertime") {
                createParams['requestDetails'] = that.undertime;
            }else if(that.notificationType == "offset") {
                createParams['requestDetails'] = that.offset;
            }
            
            this.fetch('/ns/action/'+notif_id,createParams);
        },

        fetch : function (actionUrl,params) {
            var that = this;
            Vue.http.post(base_url+actionUrl,params,{ headers: {
                "Cache-Control" : "no-cache, no-store, must-revalidate",
                "Pragma": "no-cache",
                "Expires": "0"
            }} ).then(function (response) {
                    that.$set("submitStatus.submitted",true);
                    that.$set("submitStatus.success",response.data['success']);
                    that.$set("submitStatus.message",response.data['message']);
                    that.$set("submitStatus.errors",response.data['errors']);
                    window.scrollTo(0,0);
                    if(response.data["success"] == true) {
                        setTimeout(function(){window.location = response.data["url"]}, 1000)
                    }else{
                        that.$set('attachments',[]);
                    }
                },
                function () {
                    alert("Something went wrong");
                });
        },
        am_pm_to_hours : function (time) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "pm" && hours < 12) hours = hours + 12;
            if (AMPM == "am" && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            return (sHours +':'+sMinutes);
        },

        editOffsetReference : function () {
            // this.dateValidation();
            if(this.editable) {
                $("#offsetEditModal").modal('show');
            }else{
                alert("Please select specific offset reference to edit.");
            }
        },
    },
    watch : {
        'officialBusiness.dateFrom' : function() {
            if(this.officialBusiness.dateTo != "") {
                if(this.officialBusiness.dateFrom >= this.officialBusiness.dateTo) {
                    alert("Please check your dates");
                    this.$set("officialBusiness.dateFrom","");
                }
            }
        },
        'officialBusiness.dateTo' : function() {
            if(this.officialBusiness.dateTo != "") {
                if(this.officialBusiness.dateTo <= this.officialBusiness.dateFrom) {
                    alert("Please check your dates");
                    this.$set("officialBusiness.dateTo","");
                }
            }
        },
        'cutTime.dateFrom' : function() {
            if(this.cutTime.dateTo != "") {
                if(this.cutTime.dateFrom > this.cutTime.dateTo) {
                    alert("Please check your dates");
                    this.$set("cutTime.dateFrom","");
                }
            }
        },
        'cutTime.dateTo' : function() {
            if(this.cutTime.dateTo != "") {
                if(this.cutTime.dateTo < this.cutTime.dateFrom) {
                    alert("Please check your dates");
                    this.$set("cutTime.dateTo","");
                }
            }
        },

        'offset.dateFrom' : function () {
            this.checkdates();
            if(this.offset.dateTo != "") {
                if(this.offset.dateTo <= this.offset.dateFrom) {
                    alert("Please check your dates");
                    this.$set("offset.dateFrom","");
                }
            }
        },
        'offset.dateTo' : function () {
            this.checkdates();
            if(this.offset.dateTo != "") {
                if(this.offset.dateTo <= this.offset.dateFrom) {
                    alert("Please check your dates");
                    this.$set("offset.dateTo","");
                }
            }

        },
        'notificationType' : function () {
            if (this.notificationType != "") {
                var that = this;
                $('.date_picker').datepicker({
                  dateFormat : 'yy-mm-dd'
                });

                $('.time_picker').timepicker({
                  defaultTime : false,
                  showMeridian : true
                });
            }
        },
        'offset.scheduleType': function () {
            var scheduleType = this.offset.scheduleType;
            if(scheduleType != "") {
                this.$set("offset.dateReady",true);
                if(scheduleType == "compressed") {
                    console.log(scheduleType);
                    this.$set("offsetPeriodEditable",false);
                }else if (scheduleType == "regular") {
                    console.log(scheduleType);
                    this.$set("offsetPeriodEditable",false);
                }else if (scheduleType == "special") {
                    console.log(scheduleType);
                    this.$set("offsetPeriodEditable",true);
                }
            }else{
                this.$set("offset.dateReady",false);
            }
        },
        'timeKeeping.toCorrect' : function () {
            if(this.timeKeeping.toCorrect == 'no_time_in' ){
                this.timeKeeping.timeOut = "";
            }else if(this.timeKeeping.toCorrect == 'no_time_out' ){
                this.timeKeeping.timeIn = "";
            }
        },
        'forEditReference.type' : function () {
            if (this.forEditReference.type == 'OT') {
                this.$set("forEditReference.OBDestination","")
            }
        },
        
        'undertime.day' : function () {
            console.log("hello");
        },

        'offset.duration' : function () {
            // if(this.offset.duration != "multiple" || this.offset.duration != "one") {
            //     this.$set("offset.hoursTo","");
            //     this.$set("offset.dateTo","");
            //     this.$set("offset.periodTo","");
            //     var wholeID = this.offsetPeriod.indexOf("whole");
            //     if (wholeID != -1) {
            //         this.offsetPeriod.splice(wholeID,1);
            //     }
            // }else 
            var wholeID = this.offsetPeriod.indexOf("whole");
            if(this.offset.duration == 'half'){
                this.$set("offset.hoursTo","");
                this.$set("offset.dateTo","");
                this.$set("offset.periodTo","");
                if (wholeID != -1) {
                    this.offsetPeriod.splice(wholeID,1);
                }
            }else if(this.offset.duration == "one") {
                this.$set("offset.hoursTo","");
                this.$set("offset.dateTo","");
                this.$set("offset.periodTo","");
                if (wholeID == -1) {
                    this.offsetPeriod.push("whole");
                }
                this.$set("offset.periodFrom","whole");
            }else if(this.offset.duration == "multiple") {
                if (wholeID == -1) {
                    this.offsetPeriod.push("whole");
                }
            }
            this.checkdates();
        },
        "offset.periodFrom" : function () {
            if(this.offset.scheduleType == "compressed") {
                if(this.offset.periodFrom == "whole") {
                    this.$set("offset.hoursFrom",9);
                }else{
                    this.$set("offset.hoursFrom",4.5);
                }
            }else if(this.offset.scheduleType == "regular") {
                if(this.offset.periodFrom == "whole") {
                    this.$set("offset.hoursFrom",8);
                }else{
                    this.$set("offset.hoursFrom",4);
                }
            }
        },

        "offset.periodTo" : function () {
            if(this.offset.scheduleType == "compressed") {
                if(this.offset.periodTo == "whole") {
                    this.$set("offset.hoursTo",9);
                }else{
                    this.$set("offset.hoursTo",4.5);
                }
            }else if(this.offset.scheduleType == "regular") {
                if(this.offset.periodTo == "whole") {
                    this.$set("offset.hoursTo",8);
                }else{
                    this.$set("offset.hoursTo",4);
                }
            }
        },
    },
    ready : function () {
        this.$watch('undertime.scheduleType',function () {
            this.$set("undertime.scheduleTimeout", "");
            // this.$set("undertime.date", "");
            // this.$set("undertime.timeout", "");
        });
        this.$watch('offset.scheduleType',function () {
            if (this.offsetScheduleType != "") {
                this.$set("offsetDateReady", true);
                //commented on july 17 2017
                // this.$set("offset.hoursFrom","");
                // this.$set("offset.hoursTo","");
                // this.$set("offset.dateFrom","");
                // this.$set("offset.dateTo","");
                // this.$set("offset.periodTo","");
                // this.$set("offset.periodFrom","");
                // this.$set("offset.duration","");
            } else {
                this.$set("offsetDateReady", false);
            }
        });

        var that = this;
        Vue.http.post(dataURL,{"notif_id" : notif_id}, {
            headers: {
                'Cache-Control': 'no-cache'
            }
        }).then(function (response) {
                if(that.listOffsetReferenceInputOld.length == 0) {
                    for (var i = 0; i < response.data.length; i++) {
                        that.offset.listOffsetReferences.push(response.data[i]);
                    }
                }
            },
            function () {
                console.log("Something went wrong");
            });
    }
})/**
 * Created by octal on 27/07/2016.
 */
