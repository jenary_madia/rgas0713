$(function()
{
    var file_counter = 0;
    $('#fileupload-revision').fileupload({
            dataType: 'json',
            done: function (e, data) {
                console.log(e);
                console.log(data);
                $.each(data.result.files, function (index, file) 
                {
                    console.log(index);
                    console.log(file);
                    row_attachment = "<p class='attachment_note'>";
                    row_attachment += file.original_filename;
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][filesize]' value='"+file.filesize+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][mime_type]' value='"+file.mime_type+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][original_extension]' value='"+file.original_extension+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][original_filename]' value='"+file.original_filename+"'>";
                    row_attachment += "<input type='hidden' name='files["+file_counter+"][random_filename]' value='"+file.random_filename+"'>";
                    row_attachment += " <button class='btn btn-sm btn-default remove-fn'>-</button>";
                    row_attachment += "</p>";
                    $("#attachments-revision").append(row_attachment);
                    file_counter += 1;
                });
            }
        });

      $(".remove-fn").live("click",function()
             {
                 if($(this).attr("attach-name"))
                {
                   var id = $(this).attr("attach-id"); 
                   $(this).parent().parent().append("<input type='hidden' value='" + $(this).attr("attach-name") + "' name='attachment_name[]' />");                 
                }
                
                 $(this).parent().remove();
             });
})