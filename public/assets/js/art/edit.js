$(function () 
{
        "use strict";
        $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

        $("button[name='action'][value='send']").click(function(e)
        {
            try
            {
                $(".date_picker").each(function()
                {
                    if($(this).val() )$.datepicker.parseDate( "yy-mm-dd",  $(this).val() );
                })                   
            }
            catch(e)
            {
               alert("Invalid inputted date");
               return false;
            }

            if(!$("input[name='project']").val() || !$("input[name='duedate']").val() ||
                !$("select[name='packaging']").val() || !$("select[name='servicerequested']").val() ||
                 !$("textarea[name='briefdesc']").val()  || !$("input[name='requestype']:checked").length )
            {
                alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                return false;
            }

            if($("select[name='packaging']").val() == "Others" && !$("input[name='othermaterial']").val())
            {
                alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                return false;
            }

            if($("select[name='servicerequested']").val() !="Event Materials" )
            {
                if(!$("input[name='productname']").val() )
                {
                    alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                    return false;
                }                
            }

            if($("select[name='servicerequested']").val() == "Event Materials" )
            {
                if(!$(".designspecification input[name='lenght']").val() || !$(".designspecification input[name='width']").val()  )
                {
                    alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                    return false;
                }                
            }


            if($("select[name='servicerequested']").val() == "New Product Packaging Design" || $("select[name='servicerequested']").val() == "Promo & Merchandising Materials" || $("select[name='servicerequested']").val() == "Existing Product Packaging Design Enhancement/Improvement")
            {
                if($("select[name='packaging']").val() == "Primary Wrapper" || $("select[name='packaging']").val() == "Secondary Wrapper" || $("select[name='packaging']").val() == "Carton/Case" )
                {
                    if(!$(".materialspecification input[name='weight']").val() )
                    {
                        alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                        return false;
                    }   
                }

                if($("select[name='packaging']").val() == "Primary Wrapper" || $("select[name='packaging']").val() == "Secondary Wrapper" || $("select[name='packaging']").val() == "Carton/Case" || $("select[name='packaging']").val() == "Banner" || $("select[name='packaging']").val() == "Gondola" )
                {
                    if(!$(".materialspecification input[name='lenght']").val() || !$(".materialspecification input[name='width']").val() )
                    {
                        alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                        return false;
                    }   
                }


                if($("select[name='packaging']").val() == "Primary Wrapper" || $("select[name='packaging']").val() == "Secondary Wrapper" || $("select[name='packaging']").val() == "Carton/Case" ||  $("select[name='packaging']").val() == "Gondola" )
                {
                    if( !$(".materialspecification input[name='height']").val() )
                    {
                        alert("ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.");
                        return false;
                    }   
                }
            }


            if($("input[name='duedate']").val())
            {
                var end = new Date( $("input[name='duedate']").val() ),
                now = new Date(),
                start  = new Date((now.getMonth() + 1) + "/" + (now.getDate()+1) + "/" + now.getFullYear() );
                var days = 0;

                for(var d = start; d < end; d.setDate(d.getDate() + 1)) 
                {
                    if(d.getDay() != 0 && d.getDay() != 6 )
                    {
                        days = parseInt(days) + 1;
                    }
                }

                if($("select[name='servicerequested']").val() == "New Product Packaging Design" && days < 5)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 5 MINIMUM WORKING DAYS");
                    return false;
                }
                else if($("select[name='servicerequested']").val() == "Event Materials" && days < 5)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 5 MINIMUM WORKING DAYS");
                    return false;
                }
                else if($("select[name='servicerequested']").val() == "Existing Product Packaging Design Enhancement/Improvement" && days < 4)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 4 MINIMUM WORKING DAYS");
                    return false;
                }
                else if($("select[name='servicerequested']").val() == "Promo & Merchandising Materials" && days < 3)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 3 MINIMUM WORKING DAYS");
                    return false;
                }
            }

            // if(!$(".remove-fn").length)
            // {
            //     alert("At least one file need to be attached.");
            //      return false;
            // }
            
    
            
        });

        $("input[name='requestype']").click(function()
        {
            $("select[name='packaging']").val("");
            $("input[name='othermaterial']").val("");
            $(".othermaterial").addClass("hide");

            $("select[name='packaging'] option").addClass("hide");
            if($(this).val() == "mockup")
            {
                $("select[name='packaging'] option[value='Primary Wrapper']").removeClass("hide");
                $("select[name='packaging'] option[value='Secondary Wrapper']").removeClass("hide");
                $("select[name='packaging'] option[value='Others']").removeClass("hide");
            }
            else
            {
                $("select[name='packaging'] option").removeClass("hide");
            }
        })

        $("select[name='packaging']").change(function()
        {
            $("input[name='othermaterial']").val("");
            if($(this).val() == "Others")
                 $(".othermaterial").removeClass("hide");
            else  $(".othermaterial").addClass("hide");


            $(".materialspecification #weight").removeClass("text-danger");
            $(".materialspecification #lenght").removeClass("text-danger");
            $(".materialspecification #height").removeClass("text-danger");
            $(".materialspecification #width").removeClass("text-danger");

                
            if($(this).val() == "Primary Wrapper" || $(this).val() == "Secondary Wrapper" || $(this).val() == "Carton/Case")
            {
                $(".materialspecification #weight").addClass("text-danger");
                $(".materialspecification #lenght").addClass("text-danger");
                $(".materialspecification #height").addClass("text-danger");
                $(".materialspecification #width").addClass("text-danger");
            }

            else if( $(this).val() == "Gondola")
            {
                $(".materialspecification #lenght").addClass("text-danger");
                $(".materialspecification #height").addClass("text-danger");
                $(".materialspecification #width").addClass("text-danger");
            }
            else if($(this).val() == "Banner" )
            {
                $(".materialspecification #lenght").addClass("text-danger");
                $(".materialspecification #width").addClass("text-danger");
            }

        });

        $("select[name='servicerequested']").change(function()
        {
            $(".materialspecification input , .materialspecification textarea").attr("disabled",true);
            $(".designspecification input , .designspecification textarea").attr("disabled",true);
            $(".materialspecification input , .materialspecification textarea").val("");
            $(".designspecification input , .designspecification textarea").val("");
            $(".materialspecification").addClass("hide");
            $(".designspecification").addClass("hide");
         

            if($(this).val() == "Event Materials")
            {
                $(".designspecification").removeClass("hide");
                $(".designspecification input , .designspecification textarea").attr("disabled",false); 

                $(".materialspecification input , .materialspecification textarea").val(""); 

            }
            else if($(this).val() == "New Product Packaging Design" || $(this).val() == "Promo & Merchandising Materials" || $(this).val() == "Existing Product Packaging Design Enhancement/Improvement")
            {
                $(".materialspecification").removeClass("hide");
                $(".materialspecification input , .materialspecification textarea").attr("disabled",false);  

                $(".designspecification input , .designspecification textarea").val("");
            }
            else
            {
                $(".materialspecification input , .materialspecification textarea").val("");
                $(".designspecification input , .designspecification textarea").val("");
            }

        });

    $("textarea[name='briefdesc']").trigger("keyup");
});

function AutoGrowTextArea(textField)
        {
          if (textField.clientHeight < textField.scrollHeight)
          {
            textField.style.height = textField.scrollHeight + "px";
            if (textField.clientHeight < textField.scrollHeight)
            {
              textField.style.height = 
                (textField.scrollHeight * 2 - textField.clientHeight) + "px";
            }
          }
        }