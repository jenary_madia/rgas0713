$(function()
{
    $(".datepicker").datepicker();

    $("a.tooltipLink").tooltip();

    $("button[name='action']").click(function(e)
        {
            if($(this).val() != "return") return true;

            try
            {
                $(".datepicker").each(function()
                {
                    if($(this).val() )$.datepicker.parseDate( "mm/dd/yy",  $(this).val() );
                })                   
            }
            catch(e)
            {
               alert("Invalid inputted date");
               return false;
            }

            
            if(!$("input[name='degree']:checked").length)
            {
                alert("TYPE OF REVISIONS is required.");
                e.preventDefault();
                return false;
            } 

             if(!$("textarea[name='revision']").val())
            {
                alert("DETAILS OF REVISIONS is required.");
                e.preventDefault();
                return false;
            } 
            else if(!$("input[name='completion_date']").val())
            {
                alert("TARGET COMPLETION DATE is required.");
                e.preventDefault();
                return false;
            } 


            if($("input[name='completion_date']").val())
              {
                var end = new Date( $("input[name='completion_date']").val() ),
                now = new Date(),
                 start  = new Date((now.getMonth() + 1) + "/" + (now.getDate() + 1) + "/" + now.getFullYear() );
                var days = 0;

                for(var d = start; d <= end; d.setDate(d.getDate() + 1)) 
                {
                    if(d.getDay() != 0 && d.getDay() != 6 )
                    { 
                        days = parseInt(days) + 1;
                    }
    
                }
 


                if($("input[name='degree']:checked").val() == "Major Revision" && days < 3)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 3 MINIMUM WORKING DAYS");
                    return false;
                }
                else if($("input[name='degree']:checked").val() == "Minor Revision" && days < 2)
                {
                    alert("PLEASE CHECK INPUTTED DUE DATE. ALLOW 2 MINIMUM WORKING DAYS");
                    return false;
                }
            }
            
            
        });
});