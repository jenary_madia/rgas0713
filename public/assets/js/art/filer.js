$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var requests_my_table = $('#myrecord ').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true ,
                 "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "td_center", "bSortable": false },      
                ]
              }); 

    requests_my_table.fnSort( [[0,"desc"]]); 

    if($(' #reviewer_record').length )
    {
     var my_table = $(' #reviewer_record').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true ,
                 "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "td_center", "bSortable": false },      
                ]
              });

       my_table.fnSort( [[0,"desc"]]);  
    }

    
    $(".confirm").live("click",function(e)
    {
    	var con = confirm("Are you sure you want to delete " + $(this).attr("ref") + " ?");
    	if(con) 
    	{
    		window.location = $(this).attr("link");
		}
    });

});