$(document).ready(function()
{
    "use strict";

    var base_url = $("base").attr("href");

    var requests_my_table = $('#myrecord').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true  ,
                 "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },   
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "td_center", "bSortable": false },      
                ]      }); 

    requests_my_table.fnSort( [[0,"desc"]]); 

      var myrecord_submitted = $('#myrecord_submitted').dataTable( {     
              "sPaginationType": "full_numbers" ,
               'bLengthChange':true,
               'iDisplayLength' : 10,
               'bInfo':true    ,
                 "aoColumns": [
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },     
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },
                  { "sClass": "td_center", "bSortable": true },   
                  { "sClass": "td_center", "bSortable": false },      
                ]    }); 

    myrecord_submitted.fnSort( [[0,"desc"]]); 

    var dep = "";
     $('.department').change( function() 
     {
        dep = $(this).val();
                    $.fn.dataTableExt.afnFiltering.push(
                        function( settings, data, dataIndex ) 
                        {
                            if(dep == "") return true;
                        
                            return (data[6] == dep)
                                ? true
                                : false
                        }     
                    );
                    requests_my_table.fnDraw();
    } );

});