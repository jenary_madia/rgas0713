var participating_brands_ctr = 1;
$(document).ready(function(){
   // $(".date_picker").datepicker({format: 'yyyy-mm-dd', autoclose: true});
       $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });


     $("input[name='initiatedby[others]'][value='Others']").click(function()
     {
     	if(!$(this).attr("checked"))
	     	 $("input[name='initiatedby[initiatedby_others]']").val("");
     })

    $("#req_activity_type").change(function(){
       $("#req_activity_details").val('');
       if($(this).val() == 'Others'){
           $(".activity_detail").show();
       }else{
           $(".activity_detail").hide();
       }
    });
    
    $("#req_activity_date").change(function(){
        
        $("#req_from,#req_to,#req_from,#req_noOfDays,#req_date").val('');
        $(".div_straight,.div_straight_no").hide();
        $(".div_staggered").hide();
        
        if($(this).val() == 'Multiple days-Staggered'){
             $(".div_staggered").show();
        }
        else if(!$(this).val())
        {

        }
        else{
             $(".div_straight,.div_straight_no").show();
        }
    });
    
	
    //add new brand 
    $("#brands_add").click(function(){
		$('.tr_pb td').css('background-color','#EBEBEB');
        var rowCount = $('#tbl_brands tr').length;
        // var participating_brands_ctr = $('#tbl_brands tr').length;
        if(rowCount < 10){ console.log(rowCount);
            // var cntID = rowCount++;
			participating_brands_ctr = rowCount;
			participating_brands_ctr = participating_brands_ctr + 1;
            var cntID = participating_brands_ctr;
            var newRow =   '<tr class="tr_pb" id="yc">'+
                                //'<td class="td_style hidden"><input type="checkbox" id="brand_'+cntID+'" class="chkbox_brand"/></td>'+
                                '<td class="td_style"><input type="textbox" name="qty_[]" id="qty_'+cntID+'" class="quantity_textbox" /></td>'+
                                '<td class="td_style"><input type="textbox" name="unit_[]" id="unit_'+cntID+'" class="quantity_textbox" /></td>'+
                                '<td class="td_style"><input type="textbox" name="product_[]" id="product_'+cntID+'" class="product_textbox" /></td>'+
                            '</tr>';
        
            $('#tbl_brands').append(newRow);
        }
    });

    
	// $("#nc").live('click', function() {
		// console.log("first row clicked");
		// if($("#brands_del").clicked){
			// console.log("huhuhu");
			// $("#qty_1").val('');
			// $("#unit_1").val('');
			// $("#product_1").val('');
		// }
	// });
	
	// $("#yc").live('click', function() {
		// console.log("other row clicked");
	// });
	
	// $(".tr_pb:not(#nc)").live('click', function() {
	$(".tr_pb").live('click', function() {
		selected_row =  this;
		console.log(selected_row);
		$('.row-selected').css('background-color','rgba(0, 0, 0, 0)');
		$(this).css('background-color','#CCC');
	});
	
    $("#brands_del").on('click',function () {
		console.log('del clicked!');
        if(confirm('Delete selected Participating Brands?')){
			var trId = $(selected_row).closest('tr').prop('id');
			console.log(trId);
			// if(trId == 'nc'){
				// $("#qty_1").val('');
				// $("#unit_1").val('');
				// $("#product_1").val('');
			// }
			// else if(trId == 'yc'){
				$(selected_row).remove();
				selected_row = "";
			// }
        }
    });
	
    $("#btnAddAttachment").click(function(){
		if($("#attachments input:file").length < 10){
			$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment' type='button'>DELETE</button></div>");
		}
	});
	
	$('.remove_file_attachment').live('click', function() {
			$(this).parent().remove();
	});
	

	// $('#req_to,#req_from').datepicker().on('changeDate', function (ev) {
 //       var days = validateDate($('#req_from').val(),$("#req_to").val());
 //       $("#req_noOfDays").val(days);
 //       $(this).datepicker('hide');
	// });

	$("#req_from").datepicker({ dateFormat: 'yy-mm-dd' , minDate: 0 , "onSelect":
		function(){
			
			if($("#req_activity_date").val() == "One Day")
				   $("#req_to").val( $("#req_from").val() );

			  var days = validateDate($('#req_from').val(),$("#req_to").val());
		       $("#req_noOfDays").val(days);
		       $(this).datepicker('hide');

		}});

	$(".date_picker_").datepicker({ dateFormat: 'yy-mm-dd' , minDate: 0 , "onSelect":
		function(){
			
			  var days = validateDate($('#req_from').val(),$("#req_to").val());
		       $("#req_noOfDays").val(days);
		       $(this).datepicker('hide');

		}});

	
});

function validateDate(fr,to){
    if(fr !== '' && to !== ''){
		if(fr === to){
			return 1;
		}
		else{
			return dayDiff(fr,to) == 0 ? dayDiff(fr,to) : (dayDiff(fr,to) + 1);
		}
	}
    else{
		return 0;
	}
}

function dayDiff(fr,to) {
    var a = new Date(fr),
        b = new Date(to),
        c = 24*60*60*1000,
        diffDays = Math.round(Math.abs((a - b)/(c)));
    
    diffDays = (a > b) ? (diffDays * -1) : diffDays;
    return diffDays;
}


