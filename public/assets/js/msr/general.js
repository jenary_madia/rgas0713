$(document).ready(function () {
    $('.date_picker').datepicker({
        dateFormat : 'yy-mm-dd'
    });

    $('.time_picker').timepicker({
        defaultTime : false,
        showMeridian : false
    });

    $('body').on('click','.toggleDatePicker',function () {
        $(this).closest( "div" ).find(".date_picker").datepicker("show");
    });
    $('body').on('change', '#schedule_typeOffset', function() {
        if ($(this).val() == "compressed"){
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 6 && day != 0)];
            });
        }else if ($(this).val() == "regular") {
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 6 && day != 20)];
            });
        }else{
            $('.date_picker').datepicker('option', 'beforeShowDay', function(date) {
                var day = date.getDay();
                return [(day != 22 && day != 20)];
            });
        }
    });

    $('body').on('input', '.durationType', function() {
        if ($(this).val() == "multiple") {
            $('.divTo').fadeIn()
        }else{
            $('.divTo').fadeOut()
        }
    });

    $(".numeric").live("keypress",function(event)
    {
        return isNumeric(event);
    });

    function isNumeric(e)
    {
        // Ensure that it is a number and stop the keypress
        if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) {
            e.preventDefault();
        }
    }

});

