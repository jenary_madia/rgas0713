$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#myMSR").length != 0 ) {
        var requests_my_table = $('#myMSR').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/get_my_msr/5",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { 'bSortable': false},
                { 'bSortable': false},
                { 'bSortable': false},
                { 'bSortable': false},
                { 'bSortable': false},
                { 'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#superiorMSR").length != 0 ) {
        var requests_my_table = $('#superiorMSR').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/get_superior_msr/5",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    $('body').on('submit','#formMyMSR',function (e) {
        var r = confirm("Delete this MSR?");
        if (r == true) {
            $("#formMyMSR").submit;
        }else{
            e.preventDefault();
        }
    });

    $('body').on('click','#btnGenerate',function (e) {
        e.preventDefault();
        $('#generateModal').modal('show');
    });

    $('body').on('change','#department',function () {
        $.ajax({
            url :  base_url + "/msr/get_filtered_submitted_msr",
            dataType : 'JSON',
            data : { department : $("#department").val() },
            method : 'POST'
        }).done(function(data) {
            submittedMSR.fnClearTable();
            for (var i = 0; i < data.length; i++) {
                submittedMSR.fnAddData([
                    data[i][0],
                    data[i][1],
                    data[i][2],
                    data[i][3],
                    data[i][4],
                    data[i][5]
                ]);
            }
        });
    });


    if ( $("#myMSR-SMIS").length != 0 ) {
        var requests_my_table = $('#myMSR-SMIS').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/smis/get_my_msr/5",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#superiorMSR-SMIS").length != 0 ) {
        var requests_my_table = $('#superiorMSR-SMIS').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/msr/smis/get_superior_msr/5",
            "aaSorting" : [[0, 'desc']],
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false},
                {'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

});

