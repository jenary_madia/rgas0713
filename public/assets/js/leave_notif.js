$(document).ready(function(){
    var base_url = $("base").attr("href");
    if ( $("#myPELeaves").length != 0 ) {
        var myPELeaves = $('#myPELeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_my_pe_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "22%",'bSortable': false},

            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#myLeaves").length != 0 ) {
        var myLeaves = $('#myLeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_my_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [5, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}
            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false
        });

    }

    if ( $("#forApproveLeaves").length != 0 ) {
        var forApproveLeaves = $('#forApproveLeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_for_approval_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search:"
            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "17%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false },
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "30%",'bSortable': false}

            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false
        });

    }

    if ( $("#superiorPELeaves").length != 0 ) {
        var superiorPELeaves = $('#superiorPELeaves').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/lrf/home_superior_pe_leaves",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "15%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},
                { "sWidth": "5%",'bSortable': false},

            ],
            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

//    Notifications

    if ( $("#myNotifications").length != 0 ) {
        var myNotifications = $('#myNotifications').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/home_my_notif",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "20%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "30%","bSortable": false},

            ],

            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#myPENotifications").length != 0 ) {
        var myPENotifications = $('#myPENotifications').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/home_my_pe_notif",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "20%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "5%","bSortable": false},
                { "sWidth": "30%","bSortable": false},

            ],

            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#superiorNotification").length != 0 ) {
        var superiorNotification = $('#superiorNotification').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/home_superior_notif",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {
            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "20%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "20%","bSortable" : false},

            ],

            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

    if ( $("#superiorPENotification").length != 0 ) {
        var superiorPENotification = $('#superiorPENotification').dataTable( {
            "bDestroy": true,
            "sPaginationType": "full_numbers",
            "sAjaxSource": base_url + "/ns/home_superior_pe_notif",
            "oLanguage": {
                "sProcessing":"<img src='"+base_url+"/assets/img/pre_loader.gif'>",
                "sSearch" : "Search : "
            },
            // fnFilter($(this).val())
            "fnDrawCallback": function( oSettings ) {

            },
            "aLengthMenu": [
                [10, 20, 50, 100, -1],
                [10, 20, 50, 100, "All"]
            ],
            "aoColumns": [
                { "sWidth": "20%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false},
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "5%","bSortable" : false },
                { "sWidth": "20%","bSortable" : false },

            ],

            "bPaginate": false,
            "bFilter": false,
            "bInfo" : false

        });

    }

});

$("#formMyNotifications").on("submit", function(e){
    var r = confirm("Delete this notification?");
    if (r == true) {
        $("#formMyNotifications").submit;
    }else{
        e.preventDefault();
    }
});

$("#formPEMyNotifications").on("submit", function(e){
    var r = confirm("Delete this notification?");
    if (r == true) {
        $("#formPEMyNotifications").submit;
    }else{
        e.preventDefault();
    }
});

$("#formApproveNotif").on("submit", function(e){
    var r = confirm("Approve selected notifications?");
    if (r == true) {
        $("#formApproveNotif").submit;
    }else{
        e.preventDefault();
    }
});

    

