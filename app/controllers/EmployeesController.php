<?php

class EmployeesController extends \BaseController {


	public function index() {
		//
	}
    

    public function employee_lists() {
        
        $data["base_url"] = URL::to("/");
        
        //Company List
        $company_list_html = "";
        $company_list = Companies::get_companies();
        
        ob_start();
        foreach($company_list as $company) {
           echo "<option value='{$company->comp_code}'>{$company->comp_name}</option>";
        }
        $company_list_html = ob_get_clean();
        $data["company_list_html"] = $company_list_html;
        
        
        //Department List
        $department_list_html = "";
        $department_list = Departments::get_departments();
        
        ob_start();
        foreach($department_list as $department) {
           echo "<option value='{$department->id}'>{$department->dept_name} ({$department->comp_code})</option>";
        }
        $department_list_html = ob_get_clean();
        $data["department_list_html"] = $department_list_html;
        
        
        $emp_list_html = "";
		ob_start();
		echo "<table id='employee_list' cellpadding='0' cellspacing='0' border='0' class='display' width='100%' >";
		echo "<thead>
					<tr>
                        <th align='center' width=''>Company</th>
						<th align='center'>Department</th>
						<th align='center' width=''>Employee Name</th>
						<th align='center' width=''>Designation</th>
						<th align='center'>Position</th>
						<th align='center'>Active</th>
					</tr>   
				</thead>";
                
		echo "</table>";
		$emp_list_html = ob_get_clean();
		$data["emp_list_html"] = $emp_list_html;
        
        return View::make("employee_list", $data);
        
    }
    
    //
    public function get_all_employees() {
        
        $base_url = URL::to('/');
		$result_data = []; //result data on my itr lists;
        
		$data_column_array = array(
             "employees.firstname"
            ,"employees.lastname"
            ,"comp.comp_name"
            ,"dept.dept_name"
            ,"employees.designation" 
        );
        
        $where_condition = " (employees.active in ('1','2','0')) ";
        
        $order_by_col = "comp.comp_code";
        $order_by_col2 = "dept.dept_code";
        $order_by_sort = "Asc";
        
        $iSortingCol = Input::get("iSortCol_0", null, true);
		$sSortDir = Input::get("sSortDir_0", null, true);
		$sSearch =  trim(strtolower(Input::get("sSearch", null, true)));
        
        //For Search (if has)
		if($sSearch != "") {
			$where_condition .= " AND";
			$where_condition .= "(";
			for($i = 0; $i < count($data_column_array); $i++)
			{
				$sep = ( $i == count($data_column_array) -1 )?"":" OR ";
				$where_condition .=  $data_column_array[$i] ." Like '%{$sSearch}%'". $sep;
			}
			$where_condition .= ")";
		}
        
        $employee_lists = Employees::get_all_employees($where_condition, $order_by_col, $order_by_col2, $order_by_sort);
        
        $result_data["iTotalDisplayRecords"] =  $employee_lists->count(); //total count filtered query
		$result_data["iTotalRecords"] = $employee_lists->count();
        
        //$result_data["aaData"][$ctr][] 
        if ( $employee_lists->count() > 0){
            $ctr = 0;
             foreach($employee_lists as $emp) {
                                
                $result_data["aaData"][$ctr][] = $emp->comp_code;
				$result_data["aaData"][$ctr][] = $emp->dept_name;
				$result_data["aaData"][$ctr][] = $emp->lastname.", ".$emp->firstname;
				$result_data["aaData"][$ctr][] = $emp->designation;
				$result_data["aaData"][$ctr][] = "";
				$result_data["aaData"][$ctr][] = $emp->active;
                 
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $employee_lists;
        }
       
       return json_encode($result_data);
        
    }
    
}