<?php

use RGAS\Modules\NTE;
use Carbon\Carbon;
use RGAS\Libraries;


define('MODULE_ID', Config::get('rgas.module_id_nte'));

define("USERID", Session::get('employee_id'));
define("TYPE", Session::get('is_chrd_aer_staff'));
define("DESIG_LEVEL", $desig_level = Session::get('desig_level'));
define("IS_CBRM", $is_cbrm = Session::get('is_cbrm'));

class NTEController extends \BaseController 
{
	const EMPLOYEE_NAME = "CONCAT(IFNULL(employees.firstname, ''), ' ', IFNULL(employees.middlename, ''), ' ', IFNULL(employees.lastname, '')) name";
	const EMP_NAME = "CONCAT(IFNULL(emp.firstname, ''), ' ', IFNULL(emp.middlename, ''), ' ', IFNULL(emp.lastname, '')) name";
	const COMMENTOR_NAME = "CONCAT(IFNULL(commentor.firstname, ''), ' ', IFNULL(commentor.middlename, ''), ' ', IFNULL(commentor.lastname, '')) commentor_name";
	const COMMENT_DATE = "DATE_FORMAT(date_time_posted, '%m/%d/%Y %h:%i %p') comment_date";

	const BTN_NTE_CREATE_SAVE = ['btn_txt' => 'SAVE', 'btn_lbl' => '<strong>SAVE</strong> TO EDIT LATER', 'btn_id' => 'save'];
	const BTN_NTE_CREATE_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> ' . (TYPE == 5 ? 'SELECTION TO CONCERNED PERSONNEL FOR ACCOMPLISHMENT' : 'SELECTION TO CHRD-AER SUPERVISOR FOR NOTATION'), 'btn_id' => 'send_chrd_aer'];

	const BTN_NTE_RETURNED_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD-AER SUPERVISOR FOR NOTATION', 'btn_id' => 'send_chrd_aer'];
	const BTN_NTE_RETURNED_SAVE = ['btn_txt' => 'SAVE', 'btn_lbl' => '<strong>SAVE</strong> TO EDIT LATER', 'btn_id' => 'save_returned'];

	const BTN_NTE_APPROVAL_RETURN = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> SELECTION TO FILER', 'btn_id' => 'return'];
	const BTN_NTE_APPROVAL_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> SELECTION TO CONCERNED PERSONNEL FOR ACCOMPLISHMENT', 'btn_id' => 'send_cpa'];

	const BTN_NTE_EMPLOYEE_SAVE = ['btn_txt' => 'SAVE', 'btn_lbl' => '<strong>SAVE</strong> TO EDIT LATER', 'btn_id' => 'save_dh'];
	const BTN_NTE_EMPLOYEE_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO DEPARTMENT HEAD FOR REVIEW', 'btn_id' => 'send_dh'];

	const BTN_NTE_DEPARTMENT_RETURN = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> TO EMPLOYEE FOR REVISION', 'btn_id' => 'return_er'];
	const BTN_NTE_DEPARTMENT_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD-AER FOR ACKNOWLEDGEMENT', 'btn_id' => 'send_sv'];

	const BTN_NTE_ACKNOWLEDGEMENT_RETURN = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> TO CONCERNED EMPLOYEES DEPARTMENT HEAD FOR REVISION', 'btn_id' => 'return_hr'];
	const BTN_NTE_ACKNOWLEDGEMENT_CLOSE = ['btn_txt' => 'CLOSE', 'btn_lbl' => '<strong>CLOSE</strong> NOTICE TO EXPLAIN', 'btn_id' => 'close'];

	const BTN_NTE_ACKNOWLEDGEMENT_PRINT = ['btn_txt' => 'PRINT', 'btn_lbl' => '<strong>PRINT</strong> NOTICE TO EXPLAIN', 'btn_id' => 'print_nte'];
	const BTN_NTE_ACKNOWLEDGEMENT_CREATE_NDA = ['btn_txt' => 'CREATE NDA', 'btn_lbl' => '<strong>CREATE</strong> NOTICE OF DISCIPLINARY ACTION', 'btn_id' => 'create_nda'];

	const BTN_NDA_CREATE_SAVE = ['btn_txt' => 'SAVE', 'btn_lbl' => '<strong>SAVE</strong> TO EDIT LATER', 'btn_id' => 'save'];
	const BTN_NDA_CREATE_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD-AER MANAGER FOR NOTATION', 'btn_id' => 'send_m'];

	const BTN_NDA_APPROVAL_RETURN = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> FOR REVISION', 'btn_id' => 'return_r'];
	const BTN_NDA_APPROVAL_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD HEAD FOR NOTATION', 'btn_id' => 'send_chrdh'];

	const BTN_NDA_RETURNED_SAVE = ['btn_txt' => 'SAVE', 'btn_lbl' => '<strong>SAVE</strong> TO EDIT LATER', 'btn_id' => 'save'];
	const BTN_NDA_RETURNED_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD-AER MANAGER FOR NOTATION', 'btn_id' => 'send_m'];

	const BTN_NDA_DEPARTMENT_RETURN = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> TO CHRD PERSONNEL FOR REVISION', 'btn_id' => 'return_emp'];
	const BTN_NDA_DEPARTMENT_SERVE = ['btn_txt' => 'SERVE', 'btn_lbl' => '<strong>SERVE</strong> TO CONCERNED EMPLOYEE FOR ACKNOWLEDGEMENT', 'btn_id' => 'send_emp'];

	const BTN_NDA_EMPLOYEE_ACKNOWLEDGE = ['btn_txt' => 'ACKNOWLEDGE', 'btn_lbl' => '<strong>ACKNOWLEDGE</strong> NOTICE OF DISCIPLINARY ACTION', 'btn_id' => 'acknowledge'];

	const BTN_NDA_ACKNOWLEDGEMENT_SEND = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO CHRD-CBRM FOR FILING', 'btn_id' => 'send_cbrm'];

	const BTN_NDA_CBRM_PRINT = ['btn_txt' => 'PRINT', 'btn_lbl' => '<strong>PRINT</strong> NOTICE OF DISCIPLINARY ACTION', 'btn_id' => 'print_nda'];

    const BTN_CANCEL = ['btn_txt' => 'CANCEL REQUEST', 'btn_lbl' => '<strong>CANCEL</strong> REQUEST', 'btn_id' => 'cancel'];

    // Helpers

    private $attachments_path = '/rgas/nte';
    protected $module_id;
    private $nte;

    public function getStoragePath($module)
    {
        return Config::get('rgas.rgas_storage_path').$module.'/';
    }

    public function _getMonthNameByMonthNumber($month) 
    {
        $monthName = '';

        $month == 01 || $month == 1 ? $monthName = 'January' : '';
        $month == 02 || $month == 2 ? $monthName = 'February' : '';
        $month == 03 || $month == 3 ? $monthName = 'March' : '';
        $month == 04 || $month == 4 ? $monthName = 'April' : '';
        $month == 05 || $month == 5 ? $monthName = 'May' : '';
        $month == 06 || $month == 6 ? $monthName = 'June' : '';
        $month == 07 || $month == 7 ? $monthName = 'July' : '';
        $month == 08 || $month == 8 ? $monthName = 'August' : '';
        $month == 09 || $month == 9 ? $monthName = 'September' : '';
        $month == 10 ? $monthName = 'October' : '';
        $month == 11 ? $monthName = 'November' : '';
        $month == 12 ? $monthName = 'December' : '';

        return $monthName;
    }

    public function _pullDateNow($day = '')
    {
        $dates = [];

    	$current_time_stamp = Carbon:: now();
		$current_time_stamp = explode(' ',$current_time_stamp);
		$current_date = explode('-', $current_time_stamp[0]);
		$current_time = explode(':', $current_time_stamp[1]);

		for ($i=$current_date[0]; $i > 1980; $i--)
		{
			$years[] = $i;
		}
		
		$dates[] = $current_date[0];
		$dates[] = $current_date[1];
		$dates[] = $current_date[2];
        $dates[] = $current_time[0];
        $dates[] = $current_time[1];
        $dates[] = $current_time[2];
		$dates[] = $this->_getMonthNameByMonthNumber($current_date[1]);
		$dates[] = date('t', strtotime($current_date[1].'/1/'.$current_date[0]));
        $dates[] = $years;
        $dates[] = $current_time_stamp;
        $dates[] = $this->_getMonthNameByMonthNumber($day);

		return $dates;
    }

    public function _removeElementWithValue($array, $key, $value)
    {
        for($i=0; $i<count($array); $i++)
        {
            if($array[$i][$key] == $value)
            {
                unset($array[$i]);
            }
        }
        
        return $array;
    }

	public function _validateNteUser($module, $toBeRedirect = FALSE) 
	{
    	$hasRight = false;

    	switch ($module) 
    	{
    		case 'crr.add': 
    		case 'crr.list':
    		case 'crr.edit':
    		case 'crr.multi':
    			$hasRight = (TYPE == 1 || TYPE == 2 || TYPE == 3);	
    			break;
    		
    		case 'nte.add':
    		case 'nte.multi':
    		case 'nte.request':
    		case 'nda.request':
    		case 'nda.return':
            case 'nda.acknowledged':
    			$hasRight = (TYPE == 1 || TYPE == 2 || TYPE == 3 || TYPE == 4 || TYPE == 5);	
    			break;

			case 'nte.approval':
    			$hasRight = (TYPE == 2 || TYPE == 3 || TYPE == 4 || TYPE == 5);	
    			break;	

    		case 'nte.department':
    		case 'nda.department':
    			$hasRight = (DESIG_LEVEL == 'head');
    			break;

			case 'nda.manager':
				$hasRight = (TYPE == 3);
				break;

			case 'nda.head':
				$hasRight = (TYPE == 5);
				break;

			case 'nte.employee':
			case 'nda.employee':
				$hasRight = (USERID != '');
				break;

			case 'nda.cbrm':
				$hasRight = (IS_CBRM);
				break;

    		default:
    			# code...
    			break;
    	}

        if ($toBeRedirect)
        {
            return $this->_returnError();
        }
        else
        {
        	return $hasRight;
        }
    }

    public function _messageThread($rData, $module, $splitMessage)
    {
        $data = [];
        
        $message = '';
        $my_comment = '';

        $returned = NteComments::select(DB::raw(self::COMMENTOR_NAME.','.self::COMMENT_DATE), 'nte_comments.module','nte_comments.commentor_id', 'nte_comments.comment', 'nte_comments.date_time_posted', 'nte_comments.module')
        ->where('nte_id', $rData['ref_num'])
        ->leftJoin("employees as commentor", function($join){
                
                $join->on("commentor.id", "=", "nte_comments.commentor_id");
                
            })->get();
        if ($splitMessage) 
        {
            $my_comment = $returned[count($returned) - 1]['commentor_id'] == Session::get('employee_id') ? $returned[count($returned) - 1]['comment'] : '';
            if ($returned[count($returned) - 1]['commentor_id'] == Session::get('employee_id'))
                unset($returned[count($returned) - 1]);
        }

        foreach ($returned as $return) 
        {
            if ($return->module == $module)
            {   
                $message .= $return->commentor_name . ' ' . $return->comment_date  . ': ' . $return->comment . '&#013';
            }
        }

        $data['message'] = $message;
        $data['my_comment'] = $my_comment;

        return $data;
    }

    // redirect if error

    public function _returnError() 
    {
    	return Redirect::to('/');
    }

    // Getting needs

    public function _pullEmployeeNameById($id)
    {
    	$employees = new Employees();
    	$returned = $employees->select(DB::raw(self::EMPLOYEE_NAME))->where("id", $id)->first();
    	return $returned['name'];
    }

    public function _pullDepartmentHead($dept_id)
    {
    	$employees = new Employees();
    	$returned = $employees->select('id')->where(["departmentid" => $dept_id, 'desig_level' => "head"])->first();
    	return $returned['id'];
    }

    public function _pullCHRDHead()
    {
        $employees = new Employees();
        $returned = $employees->select('id')->where(["designation" => "VP FOR CORP. HUMAN RESOURCE", 'desig_level' => "head", 'departmentid2' => "CHRD"])->first();
        return $returned['id'];
    }

    public function _pullCHRDHeirarchy($emp_id)
    {
        return Receivers::select('value')->where(['employeeid' => $emp_id, 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])->first()['value'];
    }

    public function _pullNdaCHRDId($value)
    {
        $receivers = new Receivers();
        return $receivers->select('employeeid')->where(['value' => $value, 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])->first()['employeeid'];
    } 

    public function _pullNteApproverId($id)
    {
    	$receivers = new Receivers();
    	return $receivers->select('employeeid')->where(['value' => 
	    			($receivers->select('value')
				    	->where(['employeeid' => $id, 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])
				    	->first()['value']) + 1
				    	, 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])->first()['employeeid'];
    } 

    public function _pullNteFilerId($id)
    {
        $receivers = new Receivers();
        return $receivers->select('employeeid')->where(['value' => 
                    ($receivers->select('value')
                        ->where(['employeeid' => $id, 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])
                        ->first()['value']) - 1
                        , 'module' => 'nte', 'code' => 'CHRD_AER_STAFF', 'title' => 'CHR AER Filer'])->first()['employeeid'];
    } 

	public function _pullFilerAll()
    {
    	$emp = new employees();
    	return $emp->select( DB::raw(Self::EMPLOYEE_NAME), 'employees.id', 'receivers.value')->where(['receivers.module' => 'nte', 'receivers.title' => 'CHR AER Filer'])->join('receivers', 'employees.id', '=', 'receivers.employeeid')->get();
    }

    public function _pullNteDisciplinaryActionsAll()
    {
        $rdas = new NteRdas();
        return $rdas->get();
    }

    public function _pullNteStatusAll()
    {
        $status = new NteStatus();
        return $status->get();
    }

    public function _pullNteCrrBuildUpAll()
    {
        $crrbuildup = new CrrBuildUp();
        return $crrbuildup->select('cb_crr_id', 'cb_rule_name')->where('cb_rule_status', 1)->get();
    }

    public function _pullDepartmentsAll()
    {
        $departments = new Departments();
        return $departments->select('id', 'dept_name')->where('comp_code', 'RBC-CORP')->orderBy('dept_name', 'asc')->get();  
    }

    public function _pullNteCompanyRulesAndRegulations()
    {
    	$html = '';

    	$crrbuildup = new CrrBuildUp();
		$crrbuildupdetail = new CrrBuildUpDetail();

        $crrbuildupheader = $crrbuildup->select('cb_crr_id', 'cb_rule_number', 'cb_rule_name', 'cb_rule_description')->where('cb_rule_status', 1)->orderBy('cb_rule_name', 'asc')->get();
		$crrbuildupdetails = $crrbuildupdetail->select('cbd_detail_id', 'cb_crr_id', 'cbd_section_number', 'cbd_section_description')->where('cbd_section_status', 1)->orderBy('cbd_section_number', 'asc')->get();

		for ($a=0; $a < count($crrbuildupheader); $a++) 
		{ 
			$sectionD = '';
			$section = ' - SECTION';

			if ($crrbuildupdetails->count() > 0) 
			{
				for ($b=0; $b < count($crrbuildupdetails); $b++) 
				{ 
					if ($crrbuildupdetails[$b]->cb_crr_id == $crrbuildupheader[$a]->cb_crr_id) 
					{
						if ($section === ' - Section') 
						{
							$section .= ' '.$crrbuildupdetails[$b]->cbd_section_number;
						} 
						else 
						{
							$section .= ', '.$crrbuildupdetails[$b]->cbd_section_number;
						}

				        $sectionD .= '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <i>Section '.$crrbuildupdetails[$b]->cbd_section_number.'</i> - '.$crrbuildupdetails[$b]->cbd_section_description.'</br>';
					}
				}
			
    			$html .= '<b><u>'.strtoupper($crrbuildupheader[$a]->cb_rule_name).'</u></b></br>
    			<b>RULE '.$crrbuildupheader[$a]->cb_rule_number . $section.'</b></br>&nbsp;&nbsp;
    			<b>Rule '.$crrbuildupheader[$a]->cb_rule_number.' - '.$crrbuildupheader[$a]->cb_rule_description.'</b></br>'.
    			$sectionD.'<br>';	
			}
		}

		return $html;
    }

    public function _pullNteAllWithJoins($rData = '', $whereArray = '', $statuses = '', $disciplinaryActions = '', $module = '')
    {
        $ntem = new Ntem();

        $rData != '' ? $whereArray = ['nte.nte_id' => $rData['ref_num']] : '';
        $whereArray = $whereArray != '' ? $whereArray : '';

        $whereArray['is_deleted'] = 0;
        
        $disciplinaryActions = $disciplinaryActions == '' ? [0,1,2,3,4,5] : $disciplinaryActions;

        $return = $ntem->select('nte.*', (DB::raw(self::EMP_NAME)), 'emp.sectionid','emp.employeeid', 'sect.sect_name', 
            'crr.cb_rule_name', 'crr.cb_rule_description' , 'crr.cb_rule_number' ,'emp.new_employeeid as emp_id' , 'dept.dept_name', 'rdas.name as rda', 
            'statuses.name as statuses', 'stats.name as stats', 'da.name as disact', 'nte.nte_disciplinary_action')

            ->where($whereArray)
            ->whereIn('nte_status', $statuses[0])
            ->whereIn('nda_status', $statuses[1])
            ->whereIn('nte.nte_disciplinary_action', $disciplinaryActions)

            ->leftJoin("employees as emp", function($join){
                
                $join->on("emp.id", "=", "nte.nte_employee_id");
                
            })->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "nte.nte_department_id");
                
            })->leftJoin("nte_rdas as rdas", function($join){
                
                $join->on("rdas.id", "=", "nte.nte_recommended_disciplinary_action");
                
            })->leftJoin("nte_status as statuses", function($join){
                
                $join->on("statuses.id", "=", "nte.nte_status");
                
            })->leftJoin("nte_status as stats", function($join){
                
                $join->on("stats.id", "=", "nte.nda_status");
                
            })->leftJoin("crrbuildup as crr", function($join){
                
                $join->on("crr.cb_crr_id", "=", "nte.cb_crr_id");
                
            })->leftJoin("sections as sect", function($join){
                
                $join->on("sect.id", "=", "emp.sectionid");
                
            })->leftJoin("nte_rdas as da", function($join){
                
                $join->on("da.id", "=", "nte.nte_disciplinary_action");
                
            })->first();

        return $return;
    }

	public function _needsForNte($buttons, $isEdit = false, $selectedFromMultipleEntries = '', $selectedFromApproval = '', $module = '') 
	{
    	$data["base_url"] = URL::to("/");
        $day = 0;

    	if ($selectedFromMultipleEntries != '' || $selectedFromMultipleEntries != null) 
    	{
	    	$data["emp_keys"] = array_keys($selectedFromMultipleEntries['employee']);
	    	$employees = new Employees();
			$data['employees'] = $employees->select('id', 'employeeid', 'new_employeeid' , 'departmentid', (DB::raw(self::EMPLOYEE_NAME)))->whereIn('departmentid', $selectedFromMultipleEntries['department'])->get();
            $day = $selectedFromMultipleEntries['month_dd'];

            if (isset($selectedFromMultipleEntries['multi'])) 
            {
                $count = count($selectedFromMultipleEntries['employee']) * count($selectedFromMultipleEntries['infraction']);

                $fm = new Libraries\FileManager;

                for ($i=0; $i < $count; $i++) 
                { 
                    $files[$i] = $fm->uploadFile(Config::get('rgas.rgas_temp_storage_path'), $selectedFromMultipleEntries['multi']);
                }
                
                $cie = new Libraries\CIEncrypt;

                $uploaded = [];
                $index = 0;

                foreach ($files as $file)
                {
                    $attch_html = '';
                    $lastCount = 0;
                    $lastSize = 0;

                    foreach ($file as $attachment) 
                    {
                        $attch_html .= "<p>";
                        $attch_html .= "<input type='hidden' name='files[][filesize]' value='".$attachment['filesize']."'>";
                        $attch_html .= "<input type='hidden' name='files[][mime_type]' value='".$attachment['mime_type']."'>";
                        $attch_html .= "<input type='hidden' name='files[][original_extension]' value='".$attachment['original_extension']."'>";
                        $attch_html .= "<input type='hidden' name='files[][original_filename]' value='".$attachment['original_filename']."'>";
                        $attch_html .= "<input type='hidden' name='files[][random_filename]' value='".$attachment['random_filename']."'>";
                        $attch_html .= $attachment['original_filename'];
                        $attch_html .= "&nbsp;<button type='button' class='btn btn-danger btndanger nte-row-upload nte-delete'>-</button>";
                        $attch_html .= "</p>";
                        
                        $lastCount++;
                        $lastSize += $attachment['filesize'];
                    }

                    $uploaded[$index]['last_count'] = $lastCount;
                    $uploaded[$index]['last_size'] = $lastSize;
                    $uploaded[$index]['attachment'] = $attch_html;

                    $index++;
                }

                $selectedFromMultipleEntries['files'] = $uploaded;
            }
    	} 
    	elseif ($selectedFromApproval != '' || $selectedFromApproval != null) 
    	{
    		$empids = [];
    		foreach ($selectedFromApproval['NTEs'] as $NTEs) 
			{
				$empids[] = $NTEs->nte_employee_id;
			}
    		$employees = new Employees();
			$data['employees'] = $employees->select('id', 'employeeid', 'new_employeeid', 'departmentid', (DB::raw(self::EMPLOYEE_NAME)))->whereIn('id', $empids)->get();
    	}

    	$data['buttons'] = $buttons;
    	$data['isEdit'] = $isEdit;
    	$data['module'] = $module;

		$data['html'] = $this->_pullNteCompanyRulesAndRegulations();
		$data['rdas'] = $this->_pullNteDisciplinaryActionsAll();
		$data['statuses'] = $this->_pullNteStatusAll();
		$data['crrbuildups'] = $this->_pullNteCrrBuildUpAll();
		$data["departments"] = $this->_pullDepartmentsAll();

		$data['dates'] = $this->_pullDateNow($day);

		$data['selectedFromMultipleEntries'] = $selectedFromMultipleEntries;
		$data['selectedFromApproval'] = $selectedFromApproval;

		return $data;
    }

    public function _needsForNteIndividual($sData, $module_id, $whereArray = '', $status = '', $disciplinaryActions = '', $filerAttach = false, $empAttach = false, $deptAttach = false, $ndaFilerAttach = false, $splitMessage = true, $ndaHeadAttach = false) 
    {
        $data["base_url"] = URL::to("/");
        //module_id => [1 = nte, 2 = nda, 0 = history]
        $module_name = '';
        switch ($module_id) 
        {
            case 0:
                $returned = $this->_pullNteAllWithJoins('', $whereArray, $status, $disciplinaryActions);
                $message = count($returned) > 0 ? $this->_messageThread($sData, 1, $splitMessage) : '';
                $module_id = 1;
                $module_name = 'nte';
                break;
            case 1:
                $returned = $this->_pullNteAllWithJoins($sData, $whereArray, $status);
                $message = count($returned) > 0 ? $this->_messageThread($sData, 1, $splitMessage) : '';
                $module_name = 'nte';
                break;
            case 2:
                $returned = $this->_pullNteAllWithJoins('', $whereArray, $status, $disciplinaryActions);
                $message = count($returned) > 0 ? $this->_messageThread($sData, 2, $splitMessage) : '';
                $module_name = 'nda';
                break;
            default:
                # code...
                break;
        }

        if (count($returned) > 0)
        {
            $data['html'] = $this->_pullNteCompanyRulesAndRegulations();
            $data['rdas'] = $this->_pullNteDisciplinaryActionsAll();
            $data['dates'] = $this->_pullDateNow();
            $data['crrbuildups'] = $this->_pullNteCrrBuildUpAll();

            $tvy['emp_id'] = $returned['nte_employee_id'];
            $tvy['year'] = $returned['nte_year'];
            $tvy['crr'] = $returned['cb_crr_id'];

            $data['returned'] = $returned;
            $data['message'] = $message['message'];
            $data['my_comment'] = $message['my_comment'];

            $data['crr'] = $returned['cb_rule_name'] . ' &#xA &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' . 'Rule No. ' . $returned['cb_rule_number'] . ' - ' .$returned['cb_rule_description'] . '  &#xA ';
            $crr = CrrBuildUpDetail::where('cb_crr_id', $returned['cb_crr_id'])->orderBy('cbd_section_number', 'asc')->get();

            foreach ($crr as $c)
                $data['crr'] .= ('&#xA Section ' . $c->cbd_section_number . ' - ' . $c->cbd_section_description);

            $data['tvy'] = $this->_pullNteTotalViolatedYear($tvy);

            $nte_submitted_chrd_aer = explode('-', $returned['nte_submitted_chrd_aer']);

            $data['month_name'] = $this->_getMonthNameByMonthNumber($returned['nte_month']);
            $data['nte_submitted_chrd_aer'] = $data['month_name'] . ' ' . explode(' ',$nte_submitted_chrd_aer[2])[0] . ', ' . $nte_submitted_chrd_aer[0];

            $data['nte_submitted_chrd_aer2'] = date_format(date_create($returned['nte_submitted_chrd_aer']),"F d, Y");

            $cie = new Libraries\CIEncrypt;
            $nteAttachments = new NteAttachments();
            $attachments = $nteAttachments->where(['nte_id' => $returned['nte_id'], 'module' => $module_id])->get();

            $attchNames = ['files', 'emp', 'dept', 'ndaf', 'ndah'];
            for ($i=1; $i < count($attchNames)+1; $i++) 
            { 
                $attch_html = '';
                $lastCount = 0;
                $lastSize = 0;

                $btn_upload_delete = "&nbsp;<button type='button' id='".$attchNames[($i-1)]."' class='btn btn-xs btn-danger remove-fn nte-indi-delete'>DELETE</button>";

                foreach ($attachments as $attachment) 
                {
                    if ($attachment->type == $i) 
                    {
                        $attch_html .= "<p>";
                        $attch_html .= "<input type='hidden' name='".$attchNames[($i-1)]."[][filesize]' value='".$attachment->filesize."'>";
                        $attch_html .= "<input type='hidden' name='".$attchNames[($i-1)]."[][mime_type]' value='".$attachment->mime_type."'>";
                        $attch_html .= "<input type='hidden' name='".$attchNames[($i-1)]."[][original_extension]' value='".$attachment->original_extension."'>";
                        $attch_html .= "<input type='hidden' name='".$attchNames[($i-1)]."[][original_filename]' value='".$attachment->original_filename."'>";
                        $attch_html .= "<input type='hidden' name='".$attchNames[($i-1)]."[][random_filename]' value='".$attachment->random_filename."'>";
                        $attch_html .= "<a href=".URL::to('nte/download')."/".$module_name."/".$returned[$module_name.'_reference_number']."/".$attachment->random_filename."/".$cie->encode($attachment->original_filename).">".$attachment->original_filename."</a>";
                        $attch_html .= (($filerAttach && $i == 1) || ($empAttach && $i == 2) || ($deptAttach && $i == 3) || ($ndaFilerAttach && $i == 4) || ($ndaHeadAttach && $i == 5)) ? $btn_upload_delete : '';
                        $attch_html .= "</p>";
                        $lastCount++;
                        $lastSize += $attachment->filesize;
                    }
                }
                $data[$attchNames[($i-1)].'_attch_html'] = $attch_html;
                $data[$attchNames[($i-1)].'_last_count'] = $lastCount;
                $data[$attchNames[($i-1)].'_last_size'] = $lastSize;
            }
        }

        return $data;
    }

    // Deleting

	public function _destroyRow() 
	{
		$inputs = Input::all();
		$nte = new Ntem();
		$nte->where('nte_reference_number', $inputs['data'])->update(['is_deleted' => 1]);

        $this->_pushAuditTrail(MODULE_ID, 'AU005', 'nte', ['nte.nte_reference_number' => $inputs['data']], '', '', '');
	}

	public function _desRow() 
	{
		$inputs = Input::all();
		$nte = new Ntem();
		$nte->where('nte_id', $inputs['data'])->update(['is_deleted' => 1]);

        $this->_pushAuditTrail(MODULE_ID, 'AU005', 'nte', ['nte.nte_id' => $inputs['data']], '', '', '');
	}

    // Pushings Saves

    public function _push()
    {
        $nte = new NTE\NTE;
        $return = $nte->_push();
        echo json_encode($return);
    }

    public function _pushNteDraft()
    {
        $nte = new NTE\NTE;
        $nte->_pushNteDraft();
        return Redirect::route('nte.list', 'ÇHRD');
    }

    // Show Views

	public function _revealCrrList()
	{
		if ($this->_validateNteUser('crr.list')) 
		{
			$data["base_url"] = URL::to("/");
	        return View::make("nte/crr_list", $data);
		}
	}

	public function _revealNteLists($module)
	{
		$data["base_url"] = URL::to("/");

        $data['module'] = $module;

        $data['date'] = $this->_pullDateNow();

		$data['users'] = [];

		$data['userrights'][] = FALSE;
		$data['userrights'][] = FALSE;

        if (Session::has('NTEMessages'))
        {
            if (Session::get('NTEMessages')['s'])
                $data['nte_messages'] = Session::get('NTEMessages')['m'];

            Session::forget('NTEMessages');
        }

        if ($module == 'Submitted')
        {
    		$data['userrights'][] = FALSE;
    		$data['userrights'][] = FALSE;
        }

        $data['form_action_view'] = route('nte.view');
        $data['form_id_view'] = 'nte_view';

        if (TYPE < 4 || (TYPE > 3 && !($module == 'Submitted')))
        {
            if ($this->_validateNteUser('nte.request')) 
            {
                $data['form_action_request'] = route('nte.request');
                $data['form_id_request'] = 'nte_request';
                $data['form_action_returned'] = route('nte.returned');
                $data['form_id_returned'] = 'nte_returned';
    		    $data['userrights'][0] = TRUE;
    	    }
        }

	    if ($this->_validateNteUser('nte.approval')) 
	    {
		    $data['form_action_approval'] = route('nte.approval');
		    $data['form_id_approval'] = 'nte_approval';
		    $data['userrights'][1] = TRUE;
		}

        if ($module == 'Submitted')
        {
    	    if ($this->_validateNteUser('nte.department')) 
    	    {
    		    $data['form_action_department'] = route('nte.department');
    		    $data['form_id_department'] = 'nte_department';
    		    $data['userrights'][2] = TRUE;
    	    }

    	    if ($this->_validateNteUser('nte.employee')) 
    	    {
    		    $data['form_action_employee'] = route('nte.employee');
    		    $data['form_id_employee'] = 'nte_employee';
    		    $data['userrights'][3] = TRUE;
    	    }
        }

        $data['departments'] = $this->_pullDepartmentsAll();

        return View::make("nte/nte_list", $data);
	}

	public function _revealNdaLists($module)
	{
		$data["base_url"] = URL::to("/");

        $data['module'] = $module;

        $data['date'] = $this->_pullDateNow();

		$data['userrights'][] = FALSE;
		$data['userrights'][] = FALSE;
		$data['userrights'][] = FALSE;
        
        if($module == 'Submitted')
        {
    		$data['userrights'][] = FALSE;
    		$data['userrights'][] = FALSE;
    		$data['userrights'][] = FALSE;
            $data['userrights'][] = FALSE;
        }

        $data['form_action_view'] = route('nda.view');
        $data['form_id_view'] = 'nda_view';

        if (TYPE < 4 || (TYPE > 3 && !($module == 'Submitted')))
        {
            if ($this->_validateNteUser('nda.request')) 
            {
                $data['form_action_request'] = route('nda.request');
                $data['form_id_request'] = 'nda_request';
    		    $data['userrights'][0] = TRUE;
    	    }
        }

	    if ($this->_validateNteUser('nda.manager')) 
	    {
		    $data['form_action_manager'] = route('nda.manager');
		    $data['form_id_manager'] = 'nda_manager';
		    $data['userrights'][1] = TRUE;
	    }

	    if ($this->_validateNteUser('nda.head')) 
	    {
		    $data['form_action_head'] = route('nda.head');
		    $data['form_id_head'] = 'nda_head';
		    $data['userrights'][2] = TRUE;
	    }

        if ($this->_validateNteUser('nda.acknowledged')) 
        {
            $data['form_action_acknowledged'] = route('nda.acknowledged');
            $data['form_id_acknowledged'] = 'nda_acknowledged';
            $data['userrights'][5] = TRUE;
        }

        if($module == 'Submitted')
        {
    	    if ($this->_validateNteUser('nda.department')) 
    	    {
    		    $data['form_action_department'] = route('nda.department');
    		    $data['form_id_department'] = 'nda_department';
    		    $data['userrights'][3] = TRUE;
    	    }

    	    if ($this->_validateNteUser('nda.employee')) 
    	    {
    		    $data['form_action_employee'] = route('nda.employee');
    		    $data['form_id_employee'] = 'nda_employee';
    		    $data['userrights'][4] = TRUE;
    	    }

            if ($this->_validateNteUser('nda.cbrm')) 
            {
                $data['form_action_cbrm'] = route('nda.cbrm');
                $data['form_id_cbrm'] = 'nda_cbrm';
                $data['userrights'][6] = TRUE;
            }
        }

        $data['departments'] = $this->_pullDepartmentsAll();

        return View::make("nte/nda_list", $data);
	}

	public function _revealCrr()
	{
		if ($this->_validateNteUser('crr.add')) 
		{
			$data["base_url"] = URL::to("/");
			return View::make("nte/crr_add", $data);
		} 
	}

	public function _revealCrrEdit()
    {
    	if ($this->_validateNteUser('crr.edit')) 
    	{
	    	$inputs = Input::all();
	    	$data["base_url"] = URL::to("/");
	    	$inputs['header_rule'];

	    	$crrbuildup = new CrrBuildUp();
	    	$crrbuildupdetail = new CrrBuildUpDetail();

	    	$crrbuildupH = $crrbuildup->where('cb_crr_id', $inputs['header_rule'])->first();
	    	$crrbuildupdetailD = $crrbuildupdetail->where('cb_crr_id', $crrbuildupH->cb_crr_id)->orderBy('cbd_section_number', 'asc')->get();

	    	$data['header'] = $crrbuildupH;
	    	$data['details'] = $crrbuildupdetailD;

	    	return View::make('nte/crr_add', $data);
		} 
    }

    public function _revealNte()
	{
		if ($this->_validateNteUser('nte.add')) 
		{
			$data['buttons'][] = self::BTN_NTE_CREATE_SAVE;
	    	$data['buttons'][] = self::BTN_NTE_CREATE_SEND;

			return View::make("nte/nte_add", $this->_needsForNte($data['buttons'], true));
		}
        return $this->_returnError();
	}

	public function _revealNteMultiple()
	{
		if ($this->_validateNteUser('nte.multi')) 
		{
			$data["base_url"] = URL::to("/");
            $data['drafted'] = 0;
            $data['drafts'] = [];
            $data['drafts']['departments'] = [];
            $data['drafts']['employees'] = [];
            $data['drafts']['infractions'] = [];
            $data['drafts']['head'] = [];

            $nteDrafts = new NteDrafts();
            $draft = $nteDrafts->where('nd_filer_id', Session::get('employee_id'))->get();

			$data["crrbuildups"] = $this->_pullNteCrrBuildUpAll();
			$data["departments"] = $this->_pullDepartmentsAll();
            $data['dates'] = $this->_pullDateNow();

            if (count($draft) > 0)
            {
                $depts = [];
                $nteDraftsDepartments = new NteDraftsDepartments();
                $nteDraftsEmployees = new NteDraftsEmployees();
                $nteDraftsInfractions = new NteDraftsInfractions();
                $nteDraftsAttachments = new NteDraftsAttachments();

                $data['drafts']['departments'] = $nteDraftsDepartments->where('nd_id', $draft[0]['nd_id'])->get();
                $data['drafts']['employees'] = $nteDraftsEmployees->where('nd_id', $draft[0]['nd_id'])->get();
                $data['drafts']['infractions'] = $nteDraftsInfractions->where('nd_id', $draft[0]['nd_id'])->get();
                $data['drafts']['attachments'] = $nteDraftsAttachments->where('nd_id', $draft[0]['nd_id'])->get();
                $data['drafts']['head'] = $draft;
                $data['drafted'] = TRUE;
            }
            $fm = new Libraries\FileManager;

            if (isset($data['drafts']['attachments'])) 
            {
                $attachments = '';
                $lastCount = 0;
                $lastSize = 0;
                foreach ($data['drafts']['attachments'] as $attachment) 
                {
                    $attachments .= "<p>";
                    $attachments .= "<input type='hidden' name='files[][filesize]' value='".$attachment->filesize."'>";
                    $attachments .= "<input type='hidden' name='files[][mime_type]' value='".$attachment->mime_type."'>";
                    $attachments .= "<input type='hidden' name='files[][original_extension]' value='".$attachment->original_extension."'>";
                    $attachments .= "<input type='hidden' name='files[][original_filename]' value='".$attachment->original_filename."'>";
                    $attachments .= "<input type='hidden' name='files[][random_filename]' value='".$attachment->random_filename."'>";
                    $attachments .= $attachment->original_filename;
                    $attachments .= "&nbsp;<button type='button' id='multi' class='btn btn-xs btn-danger nte-indi-delete'>DELETE</button>";
                    $attachments .= "</p>";
                    $lastCount++;
                    $lastSize += $attachment->filesize;

                    //$fm->moveFile($attachment->random_filename, Config::get('rgas.rgas_temp_storage_path'));
                }

                $data['attachments'] = $attachments;
                $data['last_count'] = $lastCount;
                $data['last_size'] = $lastSize;
            }

            $data['html'] = $this->_pullNteCompanyRulesAndRegulations();

            return View::make("nte/nte_add_multi", $data);
		} 
        return $this->_returnError();
	}

	public function _deflectNte() 
	{
		if ($this->_validateNteUser('crr.multi')) 
		{
			$data['selected'] = Input::all();
			$data['buttons'][] = self::BTN_NTE_CREATE_SAVE;
	    	$data['buttons'][] = self::BTN_NTE_CREATE_SEND;
            
			return View::make("nte/nte_add", $this->_needsForNte($data['buttons'], true, $data['selected']));
		} 
        return $this->_returnError();
	}

	public function _revealNdaFromNte() 
	{
		$sData = Input::all();

		$whereArray = ['nte.nte_id' => $sData['ref_num'], 'nte.nte_reference_number' => $sData['reference']];
		$status = [[8],[0]];
        $disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions, true, false, false, false, false);

		$data['buttons'][] = self::BTN_NDA_CREATE_SAVE;
    	$data['buttons'][] = self::BTN_NDA_CREATE_SEND;

        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;
        $data['filer_upload'] = TRUE;
        $data['manager_upload'] = FALSE;
        $data['emp_upload'] = FALSE;
        $data['dept_upload'] = FALSE;

		return isset($data['returned']) ? View::make("nte/nda_add", $data) : Redirect::route('nte.list');
	}

	public function _revealNda() 
	{
        if ($this->_validateNteUser('nte.add')) 
        {
    		$data["base_url"] = URL::to("/");

    		$employees = new Employees();
    		$data['employees'] = $employees->selectRaw("DISTINCT nte.nte_employee_id, employees.*")
    	        ->where(['nte_status' => 8, 'nda_status' => 0])
    			->whereIn('nte.nte_disciplinary_action', [3,4])
    			->join('nte', 'nte.nte_employee_id', '=', 'employees.id')->get();

    		$data['buttons'][] = self::BTN_NDA_CREATE_SAVE;
        	$data['buttons'][] = self::BTN_NDA_CREATE_SEND;

        	$data['returned'] = ['nte_status' => 8, 'nda_status' => 0];
            $data['editable'] = TRUE;
            $data['filer_upload'] = TRUE;
            $data['manager_upload'] = FALSE;
            $data['edit_returned'] = FALSE;
            $data['isNew'] = TRUE;

    		return View::make("nte/nda_add", $data);	
        }
        return $this->_returnError();	
	}

    // Getting data by primary keys

	public function _pullNteAllDrafted()
	{
		$data = Input::all();

		$nte_ids = [];
		$nte = new Ntem;

        $return = $this->_pullEmployeesByDepartment([$data['sD']]);

		$return['drafted'] = $nte->whereIn('nte_department_id', [$data['sD']])
			->where(array('nte_year'=> $data['sY'], 'nte_month' => $data['sM'], 'nte_status' => $data['s'], 'is_deleted' => 0, 'nte_filer_id' => USERID))->get();

		if (!empty($return['drafted'])) 
		{
			foreach ($return['drafted'] as $drafted) 
				$nte_ids[] = $drafted['nte_id'];

			$ntecomments = new NteComments();

			$return['comments'] = $ntecomments->select('nte_id', 'comment', (DB::raw(self::COMMENT_DATE.','.self::EMP_NAME)))
				->whereIn('nte_id', $nte_ids)->leftJoin("employees as emp", function($join){
                
                $join->on("emp.id", "=", "commentor_id");
                
            })->get();	

            $cie = new Libraries\CIEncrypt;
            $nteAttachments = new NteAttachments();
            $attachments = $nteAttachments->whereIn('nte_id', $nte_ids)->get();
            for ($i=0; $i < count($attachments); $i++) 
                $attachments[$i]['encrypted'] = $cie->encode($attachments[$i]['original_filename']);

            $return['attachments'] = $attachments;            
		}
		
		echo json_encode($return);
	}

	public function _pullNteApprovalByDepartmentIdMonthAndYear() 
	{
		if ($this->_validateNteUser('nte.approval')) 
		{
			$inputs = Input::all();
			$nte_ids = [];
            $nte_refs = [];
			$ntem = new Ntem();

            $filerId = $this->_pullNteFilerId(Session::get('employee_id'));

			$approval['NTEs'] = $ntem->where(['nte_department_id' => $inputs['ref_num'], 'nte_month' => $inputs['ref_num2'], 
					'nte_year' => $inputs['ref_num3'], 'nte_filer_id' => $filerId, 'nte_status' => 2, 'is_deleted' => 0])->get();
			if (count($approval['NTEs']) > 0) 
			{
				for ($i=0; $i < count($approval['NTEs']); $i++) 
				{
					$tvy['emp_id'] = $approval['NTEs'][$i]['nte_employee_id'];
					$tvy['year'] = $approval['NTEs'][$i]['nte_year'];
					$tvy['crr'] = $approval['NTEs'][$i]['cb_crr_id'];

                    $approval['NTEs'][$i]['month_name'] = $this->_getMonthNameByMonthNumber($approval['NTEs'][$i]['nte_month']);

					$approval['tvy'][] = $this->_pullNteTotalViolatedYear($tvy);

					$nte_ids[] = $approval['NTEs'][$i]['nte_id'];
				}

				$ntecomments = new NteComments();
				$approval['comments'] = $ntecomments->select('nte_id', 'comment', (DB::raw(self::COMMENT_DATE.','.self::EMP_NAME)))
					->whereIn('nte_id', $nte_ids)
					->leftJoin("employees as emp", function($join) {
	                
	                	$join->on("emp.id", "=", "commentor_id");
	                
	            	})->get();	

                $cie = new Libraries\CIEncrypt;
                $nteAttachments = new NteAttachments();
                $attachments = $nteAttachments->whereIn('nte_id', $nte_ids)->get();

                $uploaded = [];
                $index = 0;

                foreach ($nte_ids as $id)
                {
                    $attch_html = '';
                    $lastCount = 0;
                    $lastSize = 0;

                    foreach ($attachments as $attachment)
                    {
                        if ($attachment->nte_id == $id) 
                        {
                            $nte_reference_number = Ntem::select('nte_reference_number')->where(['nte_id' => $id, 'is_deleted' => 0])->first()['nte_reference_number'];

                            $attch_html .= "<p>";
                            $attch_html .= "<input type='hidden' name='files[][filesize]' value='".$attachment->filesize."'>";
                            $attch_html .= "<input type='hidden' name='files[][mime_type]' value='".$attachment->mime_type."'>";
                            $attch_html .= "<input type='hidden' name='files[][original_extension]' value='".$attachment->original_extension."'>";
                            $attch_html .= "<input type='hidden' name='files[][original_filename]' value='".$attachment->original_filename."'>";
                            $attch_html .= "<input type='hidden' name='files[][random_filename]' value='".$attachment->random_filename."'>";
                            $attch_html .= "<a href=".URL::to('nte/download')."/nte/".$nte_reference_number."/".$attachment->random_filename."/".$cie->encode($attachment->original_filename).">".$attachment->original_filename."</a>";
                            //$attch_html .= $inputs['bool'] ? "&nbsp;<button type='button' class='btn btn-danger btndanger nte-row-upload nte-delete'>-</button>" : "";
                            $attch_html .= "</p>";

                            $lastCount++;
                            $lastSize += $attachment->filesize;
                        }
                    }

                    $index++;

                    $uploaded[$index]['nte_id'] = $id;
                    $uploaded[$index]['last_count'] = $lastCount;
                    $uploaded[$index]['last_size'] = $lastSize;
                    $uploaded[$index]['attachment'] = $attch_html;
                }

	            $data['buttons'][] = self::BTN_NTE_APPROVAL_RETURN;
		    	$data['buttons'][] = self::BTN_NTE_APPROVAL_SEND;

		    	$bool = $inputs['bool'] == 1 ? true : false;

                $approval['attachments'] = $uploaded;
		    	
				return View::make("nte/nte_add", $this->_needsForNte($data['buttons'], '' ,'',$approval, $bool));
			} 
			return Redirect::route('nte.create');
		} 
	}

    public function _pullNteView()
    {
        $sData = Input::all();

        $statuses = [[2,3,4,5,6,7,8],[0,9,10,11,12,13,14,15,16,17]];

        $data = $this->_needsForNteIndividual($sData, 1, '', $statuses, '', false, false, false, false, false);
       
        if ($data['returned']['nte_status'] === 2 && $data['returned']['nte_filer_id'] === USERID)
        {
            $data['buttons'][] = self::BTN_CANCEL;
        }
        elseif ($data['returned']['nte_status'] === 8 && $this->_validateNteUser('nte.request'))
        {
            $data['buttons'][] = self::BTN_NTE_ACKNOWLEDGEMENT_PRINT;
        }
        elseif ($data['returned']['nte_status'] === 4 && $data['returned']['nte_employee_id'] === Session::get('employee_id')) 
        {
            Ntem::where(['nte.nte_id' => $sData['ref_num'], 'is_deleted' => 0])->update(['nte_status' => 5]);
        }

        if (($data['returned']['nte_disciplinary_action'] == 3 || $data['returned']['nte_disciplinary_action'] == 4) && $data['returned']['nte_head_decision'] == 2 && $data['returned']['nda_status'] == 0 && $data['returned']['nte_status'] == 8)
                $data['buttons'][] = self::BTN_NTE_ACKNOWLEDGEMENT_CREATE_NDA;

        $data['editable'] = FALSE;
        $data['edit_returned'] = FALSE;
        $data['filer_upload'] = FALSE;
        $data['emp_upload'] = FALSE;
        $data['dept_upload'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return isset($data['returned']) ? View::make("nte/nte", $data) : Redirect::to(URL::previous());
    }

    public function _pullNteReturnedByNteId()
    {
    	$sData = Input::all();

        $statuses = [[3],[0]];

        $data = $this->_needsForNteIndividual($sData, 1, '', $statuses, '', true, false, false, false);

        $data['buttons'][] = self::BTN_NTE_RETURNED_SAVE;
        $data['buttons'][] = self::BTN_NTE_RETURNED_SEND;
        $data['editable'] = TRUE;
        $data['edit_returned'] = TRUE;
        $data['filer_upload'] = TRUE;
        $data['emp_upload'] = FALSE;
        $data['dept_upload'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return $this->_validateNteUser('nte.request') ? isset($data['returned']) ? View::make("nte/nte", $data) : Redirect::route('nte.list') : Redirect::to('/');
    }

	public function _pullNteEmployeeByNteId() 
	{
		$sData = Input::all();

        $ntem = new Ntem();
        $currentStatus = $ntem->select('nte_status')->where(['nte.nte_id' => $sData['ref_num'], 'is_deleted' => 0])->first()['nte_status'];
        $currentStatus == 4 ? $ntem->where('nte.nte_id', $sData['ref_num'])->update(['nte_status' => 5]) : '';

        $statuses = [[4,5],[0]];
       
        $data = $this->_needsForNteIndividual($sData, 1, '', $statuses, '', false, true, false, false);

		$data['buttons'][] = self::BTN_NTE_EMPLOYEE_SAVE;
    	$data['buttons'][] = self::BTN_NTE_EMPLOYEE_SEND;
        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;
        $data['filer_upload'] = FALSE;
        $data['emp_upload'] = TRUE;
        $data['dept_upload'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nte", $data) : Redirect::route('nte.list');
	}

	public function _pullNteDepartmentByNteId() 
	{
		$sData = Input::all();

        $statuses = [[6],[0]];
       
        $data = $this->_needsForNteIndividual($sData, 1, '', $statuses, '', false, false, true, false);

        $data['buttons'][] = self::BTN_NTE_DEPARTMENT_RETURN;
    	$data['buttons'][] = self::BTN_NTE_DEPARTMENT_SEND;
        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;
        $data['filer_upload'] = FALSE;
        $data['emp_upload'] = FALSE;
        $data['dept_upload'] = TRUE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return isset($data['returned']) ? View::make("nte/nte", $data) : Redirect::route('nte.list');
	}

	public function _pullNteAcknowledgementByNteId() 
	{
		$sData = Input::all();

        $ntem = new Ntem();
        $current = $ntem->select('nte_status', 'nda_status','nte_disciplinary_action', 'nte_head_decision')->where(['nte.nte_id' => $sData['ref_num'], 'is_deleted' => 0])->first();

        $statuses = [[7],[0]];

        $deletable = false;

        $data = $this->_needsForNteIndividual($sData, 1, '', $statuses, '', $deletable, false, false, false);

        $data['buttons'] = [];

        /*if ($current['nte_status'] == 8) 
        {
            $data['buttons'][] = self::BTN_NTE_ACKNOWLEDGEMENT_PRINT;
            
                $data['editable'] = FALSE;
                $data['filer_upload'] = FALSE;
        } */
        /*elseif ($current['nte_status'] == 7) 
        {*/
            $data['buttons'][] = self::BTN_NTE_ACKNOWLEDGEMENT_RETURN;
            $data['buttons'][] = self::BTN_NTE_ACKNOWLEDGEMENT_CLOSE;
            $data['editable'] = TRUE;
            $data['filer_upload'] = TRUE;
        /*}*/ 

        $data['edit_returned'] = FALSE;
        $data['emp_upload'] = FALSE;
        $data['dept_upload'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return isset($data['returned']) ? View::make("nte/nte", $data) : Redirect::route('nte.list');
	}

	public function _pullNteHistoryByNteId()
	{
		$sData = Input::all();

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[0,9,10,11,12,13,14,15,16]];
		$disciplinaryActions = [0,1,2,3,4];

		$data = $this->_needsForNteIndividual($sData, 0, $whereArray, $status, $disciplinaryActions);

		echo json_encode($data);
	}

	public function _pullNdaByEmployeeId() 
	{
		$data = Input::all();
		$ntem = new Ntem();

		$result['references'] = $ntem->select('nte_reference_number')->where(['nte_employee_id' => $data['data'], 'is_deleted' => 0])
			->whereIn('nte.nte_status', [8])
            ->whereIn('nte.nda_status', [0])
			->whereIn('nte.nte_disciplinary_action', [3,4])->get();

        $result['employee'] = Employees::where('employees.id', $data['data'])
            ->leftJoin('departments', 'employees.departmentid', '=', 'departments.id')
            ->leftJoin('sections', 'employees.sectionid', '=', 'sections.id')
            ->first();

		echo json_encode($result);
	}

	public function _pullNdaByEmployeeIdReferenceNumber() 
	{
		$data = Input::all();

		$whereArray = ['nte.nte_employee_id' => $data['emp'], 'nte.nte_reference_number' => $data['ref']];
		$status = [[8],[9,0]];
		$disciplinaryActions = [3,4];

        $result['crrd'] = '';
		$result['data'] = $this->_pullNteAllWithJoins('', $whereArray, $status, $disciplinaryActions);
        $crr = CrrBuildUpDetail::where('cb_crr_id', $result['data']['cb_crr_id'])->orderBy('cbd_section_number', 'asc')->get();

        foreach ($crr as $c)
            $result['crrd'] .= (' &#xA Section ' . $c->cbd_section_number . ' - ' . $c->cbd_section_description);

		echo json_encode($result);
	}

    public function _pullNdaView()
    {
        $sData = Input::all();

        $whereArray = ['nte.nte_id' => $sData['ref_num']];
        $statuses = [[2,3,4,5,6,7,8],[0,9,10,11,12,13,14,15]];
        $disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $statuses, $disciplinaryActions, false, false, false, false, false);

        /*if (($data['returned']['nda_status'] === 10 || $data['returned']['nda_status'] === 12) && $data['returned']['nda_filer_id'] === Session::get('employee_id'))
            $data['buttons'][] = self::BTN_CANCEL;*/

        $data['editable'] = FALSE;
        $data['edit_returned'] = FALSE;
        $data['filer_upload'] = FALSE;
        $data['manager_upload'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return isset($data['returned']) ? View::make($data['returned']['nda_status'] > 10 ? "nte/nda" : "nte/nda_add", $data) : Redirect::route('nte.list');
    }

    public function _pullNdaApprovalByNteIdManager()
    {
		$sData = Input::all();

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[10]];
		$disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions, false, false, false, false, true, true);

        $data['buttons'][] = self::BTN_NDA_APPROVAL_RETURN;
        $data['buttons'][] = self::BTN_NDA_APPROVAL_SEND;

        $data['editable'] = TRUE;
        $data['filer_upload'] = FALSE;
        $data['manager_upload'] = TRUE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nda_add", $data) : Redirect::route('nda.list');
    }

    public function _pullNdaApprovalByNteIdHead()
    {
		$sData = Input::all();
        $sData['bool'] = TRUE;
		$options = '';

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[12]];
		$disciplinaryActions = [3,4];

		$returned = $this->_pullNteAllWithJoins('',$whereArray, $status, $disciplinaryActions, 'nda');

        if (count($returned) > 0)
        {
    		$filers = $this->_pullFilerAll();

    		for ($i=0; $i < count($filers) - 2; $i++)
                if (($i == 0 || $i == 1) && $filers[$i]['id'] == $returned['nte_filer_id'] || $i == 2)
    			    $options .= '<option value="'.$filers[$i]['id'].'|'.$filers[$i]['value'].'">'.$filers[$i]['name'].'</option>';

            $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

            $data['buttons'][] = ['btn_txt' => 'RETURN', 'btn_lbl' => '<strong>RETURN</strong> FOR REVISION TO <select class="form-control ib" name="return_to"><option value=""></option>'.$options.'</select>', 'btn_id' => 'return_r_to'];
            $data['buttons'][] = ['btn_txt' => 'SEND', 'btn_lbl' => '<strong>SEND</strong> TO '.$returned['dept_name'].' DEPARTMENT HEAD FOR NOTATION', 'btn_id' => 'send_dh'];

            $data['editable'] = TRUE;
            $data['edit_returned'] = FALSE;

            $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

            return isset($data['returned']) ? View::make("nte/nda", $data) : Redirect::route('nda.list');
        }
        return Redirect::route('nda.list');
    }

    public function _pullNdaApprovalByNteIdDepartment()
    {
		$sData = Input::all();

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[13]];
		$disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

        $data['buttons'][] = self::BTN_NDA_DEPARTMENT_RETURN;
    	$data['buttons'][] = self::BTN_NDA_DEPARTMENT_SERVE;

        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nda", $data) : Redirect::route('nda.list');
    }

    public function _pullNdaApprovalByNteIdEmployee()
    {
		$sData = Input::all();

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[14]];
		$disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

        $data['buttons'][] = self::BTN_NDA_EMPLOYEE_ACKNOWLEDGE;

        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nda", $data) : Redirect::route('nda.list');
    }

    public function _pullNdaRequestById()
    {
        $sData = Input::all();
        $sData['bool'] = TRUE;

        $whereArray = ['nte.nte_id' => $sData['ref_num']];
        $status = [[8],[9,11,15]];
        $disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions, false, false, false, true);

        $data['buttons'][] = self::BTN_NDA_CREATE_SAVE;
        $data['buttons'][] = self::BTN_NDA_CREATE_SEND;

        $data['editable'] = TRUE;
        $data['filer_upload'] = TRUE;
        $data['manager_upload'] = FALSE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

        return isset($data['returned']) ? View::make("nte/nda_add", $data) : Redirect::route('nda.list');
    }

    public function _pullNdaAcknowledgementById()
    {
		$sData = Input::all();
        $sData['bool'] = TRUE;

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[15]];
		$disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

    	$data['buttons'][] = self::BTN_NDA_ACKNOWLEDGEMENT_SEND;

        $data['editable'] = TRUE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nda", $data) : Redirect::route('nda.list');
    }

    public function _pullNdaCBRMById()
    {
		$sData = Input::all();
        $sData['bool'] = FALSE;

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[16]];
		$disciplinaryActions = [3,4];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

        $data['buttons'][] = self::BTN_NDA_CBRM_PRINT;
        $data['editable'] = FALSE;
        $data['edit_returned'] = FALSE;

        $this->_pushAuditTrail(MODULE_ID, 'AU003', 'nte', ['nte.nte_id' => $sData['ref_num']], '', '', '');

		return isset($data['returned']) ? View::make("nte/nda", $data) : Redirect::route('nda.list');
    }

    // Getting All for lists

    public function _pullCrrBuildUpAll()
    {
        $nte = new NTE\NTELists;
        $return = $nte->_pullCrrBuildUpAll();
        return json_encode($return);
    }

    public function _pullNteAll($module) 
    {
        $nte = new NTE\NTELists;

        $data_column_array = array(
           "nte.nte_reference_number"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            ,"dept.dept_name"
            ,"nte.nte_month"
            ,"nte.nte_day"
            ,"nte.nte_year"
            ,"infraction.cb_rule_name"
            ,"rdas.name"
            ,"status.name"
            ,"nte.nte_filer_id"
        );
        
        $where_condition = " (nte.nte_status in ('2','3','4','5','6','7','8') AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nte_status in ('2','3','4','5','6','7','8') AND is_deleted = 0 AND nte.nte_filer_id = " . USERID . ") ";
        elseif ($module == 'home')
            $where_condition = " (nte.nte_status in ('3', '7') AND is_deleted = 0 AND nte.nte_filer_id = " . USERID . ") ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nte.all', ($module == 'home'));

       return json_encode($return);
    }

    public function _pullNteAllApproval($module) 
    {
        $nte = new NTE\NTELists;

		$data_column_array = array(
			"nte.count"
	       	,"nte.nte_month"
	       	,"nte.nte_year"
	       	,"dept.dept_name"
		);
        
        $where_condition = " (nte.nte_status = 2 AND is_deleted = 0) ";
        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nte.approval', ($module == 'home'));
		return json_encode($return);
    }

    public function _pullNteAllDepartment($module) 
    {
        $nte = new NTE\NTELists;

		$data_column_array = array(
	       "nte.nte_reference_number"
	       	,"emp.firstname"
	       	,"emp.middlename"
	       	,"emp.lastname"
	        ,"nte.nte_month"
	        ,"nte.nte_day"
	        ,"nte.nte_year"
	        ,"infraction.cb_rule_name"
	        ,"rdas.name"
	        ,"status.name"
		);
        
        $where_condition = " (nte.nte_department_id = ".Session::get('dept_id')." AND nte.nte_status >= 6 AND is_deleted = 0) ";

        if ($module == 'home')
            $where_condition = " (nte.nte_department_id = ".Session::get('dept_id')." AND nte.nte_status = 6 AND is_deleted = 0) ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nte.department', ($module == 'home'));
       return json_encode($return);
    }

    public function _pullNteAllEmployee($module) 
    {
        $nte = new NTE\NTELists;
        
		$data_column_array = array(
	       "nte.nte_reference_number"
	        ,"nte.nte_month"
	        ,"nte.nte_day"
	        ,"nte.nte_year"
	        ,"infraction.cb_rule_name"
	        ,"rdas.name"
	        ,"status.name"
		);
        
        $where_condition = " (nte.nte_employee_id = ".Session::get('employee_id')." AND nte.nte_status >= 4 AND is_deleted = 0) ";

        if ($module == 'home')
            $where_condition = " (nte.nte_employee_id = ".Session::get('employee_id')." AND nte.nte_status in ('4','5') AND is_deleted = 0) ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nte.employee', ($module == 'home'));
        return json_encode($return);
    }

    public function _pullNdaAll($module) 
    {
        $nte = new NTE\NTELists;
        
		$data_column_array = array(
	       "nte.nte_reference_number"
	       	,"emp.firstname"
	       	,"emp.middlename"
	       	,"emp.lastname"
	        ,"dept.dept_name"
	        ,"nte.nte_month"
	        ,"nte.nte_day"
	        ,"nte.nte_year"
	        ,"infraction.cb_rule_name"
	        ,"rdas.name"
	        ,"status.name"
	        ,"nte.nte_filer_id"
		); 
        
        $where_condition = " (nte.nda_status in ('9','10','11','12','13','14','15') AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nda_status in ('9','10','11','12','13','14','15') AND is_deleted = 0 AND nte.nda_filer_id = " . USERID .") ";
        elseif ($module == 'home')
            $where_condition = " (nte.nda_status in ('11','15') AND is_deleted = 0 AND nte.nda_filer_id = " . USERID .") ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.all', ($module == 'home'));
       return json_encode($return);   
    }

    public function _pullNdaAllApprovalCHRDAERManager($module) 
    {
        $nte = new NTE\NTELists;

        $data_column_array = array(
           "nte.nte_reference_number"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            ,"dept.dept_name"
            ,"nte.nte_month"
            ,"nte.nte_day"
            ,"nte.nte_year"
            ,"infraction.cb_rule_name"
            ,"rdas.name"
            ,"status.name"
            ,"nte.nte_filer_id"
        );
        
        $where_condition = " (nte.nda_status >= 10 AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nda_status in ('10') AND is_deleted = 0 AND nte.nte_current_id = " . USERID . ") ";
        elseif ($module == 'home')
            $where_condition = " (nte.nda_status in ('10') AND is_deleted = 0 AND nte.nte_current_id = " . USERID . ") ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.chrd.manager', ($module == 'home'));
       return json_encode($return);
    }

   	public function _pullNdaAllApprovalCHRDHead($module) 
    {
        $nte = new NTE\NTELists;

		$data_column_array = array(
	       "nte.nte_reference_number"
	       	,"emp.firstname"
	       	,"emp.middlename"
	       	,"emp.lastname"
	        ,"dept.dept_name"
	        ,"nte.nte_month"
	        ,"nte.nte_day"
	        ,"nte.nte_year"
	        ,"infraction.cb_rule_name"
	        ,"rdas.name"
	        ,"status.name"
	        ,"nte.nte_filer_id"
		);
        
        $where_condition = " (nte.nda_status >= 12 AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nda_status in ('12') AND is_deleted = 0 AND nte.nte_current_id = " . USERID . ") ";
        elseif ($module == 'home')
            $where_condition = " (nte.nda_status in ('12') AND is_deleted = 0 AND nte.nte_current_id = " . USERID . ") ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.chrd.head', ($module == 'home'));
       return json_encode($return);
    }

    public function _pullNdaAllDepartment($module) 
    {
        $nte = new NTE\NTELists;

        $data_column_array = array(
           "nte.nte_reference_number"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            ,"nte.nte_month"
            ,"nte.nte_day"
            ,"nte.nte_year"
            ,"infraction.cb_rule_name"
            ,"rdas.name"
            ,"status.name"
            ,"nte.nte_filer_id"
        );
        
        $where_condition = " (nte.nda_status >= 13 AND nte.nte_department_id = ".Session::get('dept_id')." AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nda_status in ('13') AND nte.nte_department_id = ".Session::get('dept_id')." AND is_deleted = 0) ";
        elseif ($module == 'head')
            $where_condition = " (nte.nda_status in ('13') AND nte.nte_department_id = ".Session::get('dept_id')." AND is_deleted = 0) ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.department', ($module == 'home'));
        return json_encode($return);
    }

    public function _pullNdaAllEmployee($module) 
    {
        $nte = new NTE\NTELists;

        $data_column_array = array(
           "nte.nte_reference_number"
            ,"dept.dept_name"
            ,"nte.nte_month"
            ,"nte.nte_day"
            ,"nte.nte_year"
            ,"infraction.cb_rule_name"
            ,"rdas.name"
            ,"status.name"
            ,"nte.nte_filer_id"
        );
        
        $where_condition = " (nte.nda_status >= 14 AND nte.nte_employee_id = ".Session::get('employee_id')." AND is_deleted = 0) ";

        if ($module == 'CHRD')
            $where_condition = " (nte.nda_status in ('14') AND nte.nte_employee_id = ".Session::get('employee_id')." AND is_deleted = 0) ";
        elseif($module == 'head')
            $where_condition = " (nte.nda_status in ('14') AND nte.nte_employee_id = ".Session::get('employee_id')." AND is_deleted = 0) ";

        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.employee', ($module == 'home'));
        return json_encode($return);
    }

    public function _pullNdaAllCBRM() 
    {
        $nte = new NTE\NTELists;
        $data_column_array = array(
           "nte.nte_reference_number"
            ,"dept.dept_name"
            ,"nte.nte_month"
            ,"nte.nte_day"
            ,"nte.nte_year"
            ,"infraction.cb_rule_name"
            ,"rdas.name"
            ,"status.name"
            ,"nte.nte_filer_id"
        );
        
        $where_condition = " (nte.nda_status in ('16') AND is_deleted = 0) ";
        $return = $nte->_needsForNteList($data_column_array, $where_condition, 'nda.cbrm');
       return json_encode($return);
    }

    //For History

    public function _pullEmployeesByDepartmentViaJSON()
    {
        $data = Input::all();
        echo json_encode($this->_pullEmployeesByDepartment($data['data']));
    }

    public function _pullEmployeesByDepartment($department)
	{
		$departments = [];
        $excludeEmployee = [];

        $filers = $this->_pullFilerAll();

        foreach ($filers as $filer) 
            if ($filer->value > Session::get('is_chrd_aer_staff')) 
                $excludeEmployee[] = $filer->id;

		$employees = new Employees();
		$employeesSet = $employees
			->select((DB::raw("CONCAT(IFNULL(firstname, ''), ' ', IFNULL(middlename, ''), ' ', IFNULL(lastname,'')) name")),
				'employees.id', 'employees.employeeid', 'employees.new_employeeid', 'employees.departmentid')
            ->whereIn('departmentid', $department)
            ->whereNotIn('id', $excludeEmployee)
            ->whereNotIn('desig_level', ['president', 'top mngt', 'head'])
			->where(['active' => 1, 'company' => 'RBC-CORP'])
			->where('employees.id','!=',Session::get('employee_id'))->orderBy('name', 'asc')->get();

		foreach ($employeesSet as $dept) 
			$departments[] = $dept->departmentid;

        $departments = array_unique($departments);
        $departments = array_values($departments);

        $department = new Departments();
        $departmentsSet = $department->select('id','dept_name')->whereIn('id', $departments)->where(['active' => 1])->get();

        $return['departments'] = $departmentsSet;
		$return['employees'] = $employeesSet;
        return $return;
	}

    public function _pullNteHistyoryEmployeeDepartment() 
    {
        $returned = '';

    	$array = Input::all();
    	$emp_name = $array['emp_name'];

    	$returned = Employees::select((DB::raw(self::EMPLOYEE_NAME)),'dept.dept_name','departmentid', 'employees.id')
        ->where(['employees.id' => $emp_name])
        ->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "employees.departmentid");
                
            })
        ->first();

        echo json_encode($returned);
    }

    public function _pullNteHistyoryByEmployeeIdAndYear($array = '') 
    {
    	$array == '' ? $array = Input::all() : $array = $array;

    	$ntem = new Ntem();

    	$returned = $ntem->select('nte.nte_id', 'nte.nte_month', 'nte.cb_crr_id', 'nte.nte_total_incurred', 'nte.nte_disciplinary_action', 
    		'crr.cb_rule_name as infraction', 'rda.name as recdisact', 'da.name as disact')
    		->where(['nte.nte_employee_id' => $array['emp_name'], 'nte.nte_year' => $array['year'], 'nte.nte_status' => 8, 'nte.is_deleted' => 0])
    		->leftJoin("crrbuildup as crr", function($join){
                
                $join->on("crr.cb_crr_id", "=", "nte.cb_crr_id");
                
            })->leftJoin("nte_rdas as rda", function($join){
                
                $join->on("rda.id", "=", "nte.nte_recommended_disciplinary_action");
                
            })->leftJoin("nte_rdas as da", function($join){
                
                $join->on("da.id", "=", "nte.nte_disciplinary_action");
                
            })->orderBy('nte.nte_month')->get();
            
    	if ($array['request'] == 2)
		{
			echo json_encode($returned);
		}
		else
		{ 
			return $returned;
		}
    }

    public function _pullNteTotalViolatedYear($data = NULL)
    {
        if ($data == NULL)
        {
            $inputs = Input::all();
            $emp_id = explode('|', $inputs['ref_num'])[0];
        }
        else
        {
            $inputs = $data;
            $emp_id = $inputs['emp_id'];
        }

        $ntem = new Ntem();
        $returned = $ntem->select(DB::raw("count(*) as count"))
            ->where(['nte_employee_id' => $emp_id, 'nte_year' => $inputs['year'], 'cb_crr_id' => $inputs['crr'], 'nte_status' => 8, 'is_deleted' => 0])
            ->groupBy(array('nte_employee_id', 'nte_year', 'cb_crr_id'))->get();

        if ($data == NULL) 
        {
            echo json_encode($returned);
        }
        else
        {
            return $returned;
        }
    }

    //Reports

    public function _revealNteReport()
    {
    	$array = Input::all();

    	$ntem = new Ntem();

    	$returned = $ntem->select('nte.nte_id', 'nte.nte_employee_id',(DB::raw(self::EMP_NAME)),'emp.employeeid','nte.nte_department_id','dept.dept_name',
            'sec.sect_name','nte.nte_reference_number','nte.nte_submitted_chrd_aer','nte.nte_acknowledged','nte.nte_explanation', 
            'nte.nte_total_incurred', 'nte.nte_disciplinary_action','rda.name as disact','nte.cb_crr_id','crr.cb_rule_name','nte.nte_year','nte.nte_month','nte.nte_filer_id')
            ->where(['nte.nte_id' => $array['ref_num'], 'nte.is_deleted' => 0])
                ->leftJoin("employees as emp", function($join){
                
                $join->on("emp.id", "=", "nte.nte_employee_id");
                
            })->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "nte.nte_department_id");
                
            })->leftJoin("sections as sec", function($join){
                
                $join->on("sec.departmentid", "=", "nte.nte_department_id");
                
            })->leftJoin("crrbuildup as crr", function($join){
                
                $join->on("crr.cb_crr_id", "=", "nte.cb_crr_id");
                
            })->leftJoin("nte_rdas as rda", function($join){
                
                $join->on("rda.id", "=", "nte.nte_disciplinary_action");
                
            })->first();

        $month = $this->_getMonthNameByMonthNumber($returned['nte_month']);

        $receivers = new Receivers();

        $value = $receivers->select('value')->where(['employeeid' => $returned['nte_filer_id'], 'module' => 'nte', 'title' => 'CHR AER Filer', 
            'code' => 'CHRD_AER_STAFF'])->first()['value'];
       
        $supervisor = $receivers->select('receivers.employeeid', (DB::raw(self::EMP_NAME)))
            ->where(['receivers.value' => $value+1, 'receivers.module' => 'nte', 'receivers.title' => 'CHR AER Filer', 
                'code' => 'CHRD_AER_STAFF'])
            ->leftJoin("employees as emp", function($join){
                
                $join->on("emp.id", "=", "receivers.employeeid");
                
            })->first();

        $department_head = $this->_pullEmployeeNameById($this->_pullDepartmentHead($returned['nte_department_id']));

        $array['emp_name'] = $returned['nte_employee_id'];
        $array['year'] = $returned['nte_year'];
        $array['request'] = 1;

        $history = $this->_pullNteHistyoryByEmployeeIdAndYear($array);

        $data['data'] = $returned;
        $data['month_nte'] = $month;
        $data['supervisor'] = $supervisor;
        $data['department_head'] = $department_head;

        $newArr = [];
        $tableData = '';
        $rspan = 0;
		
        for ($i=0; $i < 12; $i++) 
        { 
        	$rspan = 0;
        	for ($j=0; $j < count($history); $j++) 
        	{
        		if ($history[$j]['nte_month'] == $i+1 && (!($history[$j]['nte_id'] == $returned['nte_id']))) 
        		{
        			$newArr[$i]['key'] = $i;
    				$newArr[$i][$rspan] = $history[$j];
    				$rspan = $rspan + 1;
        		}
        		else
    			{	
    				$newArr[$i][] = [];
    			}
        	}
            $newArr[$i] = array_filter($newArr[$i]);
            if ($i == 0)
                $newArr[$i]['key'] = $i;
        }

    	for ($l=0; $l < count($newArr); $l++) 
    	{
    		if (count($newArr[$l]) > 0) 
            {   
        		if ($newArr[$l]['key'] == $l) 
        		{
        			for ($m=0; $m < count($newArr[$l])-1; $m++) 
        			{ 
        				$tableData .= '<tr>';
	        				if ($m == 0) 
	        				{
	        					$tableData .= '<td rowspan="'.(count($newArr[$l])-1).'" class="bordered"><label>'.$this->_getMonthNameByMonthNumber($newArr[$l][$m]['nte_month']).'</label></td>';
		    					$tableData .= '<td class="bordered"><label>'.$newArr[$l][$m]['infraction'].'</label></td>';
		    					$tableData .= '<td class="bordered"><label>'.$newArr[$l][$m]['disact'].'</label></td>';
	        				}
	        				else
	    					{
		    					$tableData .= '<td class="bordered"><label>'.$newArr[$l][$m]['infraction'].'</label></td>';
		    					$tableData .= '<td class="bordered"><label>'.$newArr[$l][$m]['disact'].'</label></td>';
	    					}
        				$tableData .= '</tr>';
        			}
        		}
    		}
    	}
        $data["base_url"] = URL::to("/");
        $data['table'] = $tableData;
        $data['module'] = 'nte';

        $path = 'app/storage/rgas/nte_prints/';
        $ref = $returned['nte_reference_number'];

		$html = View::make('nte.nte_report', $data);
	   	ob_start();
	   	echo $html;
	   	$newHtml = ob_get_clean();
	   	file_put_contents('../'.$path.$ref.'.html', $newHtml);

	   	exec('wkhtmltopdf "../'.$path.$ref.'.html" "../'.$path.$ref.'.pdf"');

        $this->_pushAuditTrail(MODULE_ID, 'AU006', '', ['nte.nte_id' => $array['ref_num']], '', '', $returned['nte_reference_number']);

        $fm = new Libraries\FileManager;
        return $fm->download(Config::get('rgas.rgas_storage_path').'nte_prints/'.$ref.'.pdf', $ref.'.pdf');

        //return View::make('nte.nte_report', $data);
    }

    public function _revealNdaReport()
    {
		$sData = Input::all();
        $sData['bool'] = 0;

		$whereArray = ['nte.nte_id' => $sData['ref_num']];
		$status = [[8],[16]];
		$disciplinaryActions = [3,4];

        $data['buttons'] = [];

        $data = $this->_needsForNteIndividual($sData, 2, $whereArray, $status, $disciplinaryActions);

        $data['data'] = $data['returned'];
        $data['department_head'] = $this->_pullEmployeeNameById($this->_pullDepartmentHead($data['returned']['nte_department_id']));
        $data['chrd_head'] = $this->_pullEmployeeNameById($this->_pullCHRDHead());

		$data['module'] = 'nda';

        $path = 'app/storage/rgas/nda_prints/';
        $ref = $data['returned']['nda_reference_number'];

        $html = View::make('nte.nte_report', $data);
        ob_start();
        echo $html;
        $newHtml = ob_get_clean();
        file_put_contents('../'.$path.$ref.'.html', $newHtml);

        exec('wkhtmltopdf "../'.$path.$ref.'.html" "../'.$path.$ref.'.pdf"');

        $this->_pushAuditTrail(MODULE_ID, 'AU006', '', ['nte.nte_id' => $sData['ref_num']], '', '', $ref);

        $fm = new Libraries\FileManager;
        return $fm->download(Config::get('rgas.rgas_storage_path').'nda_prints/'.$ref.'.pdf', $ref.'.pdf');

		//return View::make("nte/nte_report", $data);
    }

    //For CRON

    public function _pushMailNteNotation()
    {
        $nteMailNotifications = new NTE\NTEMailNotifications;
        $nteMailNotifications->_composeNteNotationMail();
    }

    public function _pushMailNdaDepartmentNotation()
    {
        $nteMailNotifications = new NTE\NTEMailNotifications;
        $nteMailNotifications->_composeNdaDepartmentHeadNotationMail();
    }

    public function _pushMailNdaCHRDHeadNotation()
    {
        $nteMailNotifications = new NTE\NTEMailNotifications;
        $nteMailNotifications->_composeNdaCHRDNotationMail('head');
    }

    public function _pushMailNdaCHRDManagerNotation()
    {
        $nteMailNotifications = new NTE\NTEMailNotifications;
        $nteMailNotifications->_composeNdaCHRDNotationMail('manager');
    }

    public function download($module, $ref_num, $random_filename, $original_filename)
    {  
        $fm = new Libraries\FileManager;
        $cie = new Libraries\CIEncrypt;

        $filepath = $this->getStoragePath($module) . $ref_num ."/" . $random_filename;

        try {
            if(!$fm->download($filepath, $cie->decode($original_filename)))
            {
                $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
            }
            
            $this->_pushAuditTrail(MODULE_ID, 'AU008', '', '', '', ['original_filename' => $cie->decode($original_filename)], $ref_num);
            return $fm->download($filepath, $cie->decode($original_filename));
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function _pushAuditTrail($moduleId, $actionCode, $tableName, $params, $old, $new, $ref_num)
    {
        $audit = new AuditTrails();

        $audit->user_id = Session::get('employee_id');
        $audit->date = Carbon::now();
        $audit->time = Carbon::now();
        $audit->module_id = $moduleId;
        $audit->action_code = $actionCode;
        $audit->table_name = $tableName;
        $audit->params = json_encode($params);
        $audit->old = json_encode($old);
        $audit->new = json_encode($new);
        $audit->ref_num = $ref_num;

        $audit->save();
    }
}