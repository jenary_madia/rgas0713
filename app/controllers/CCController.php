<?php

use RGAS\Modules\CC;
use RGAS\Libraries;

class CCController extends \BaseController 
{

	public function __construct()
	{		
		$this->CC = new CC\CC;
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
	}

    public function download($ref_num, $random_filename, $original_filename)
    {       
        $fm = new Libraries\FileManager;
        $filepath = Config::get('rgas.rgas_storage_path') . "cc/" . "$ref_num/" . $random_filename;
        if(!$fm->download($filepath, CIEncrypt::decode($original_filename)))
        {            
            $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
        }
        return $fm->download($filepath, CIEncrypt::decode($original_filename));
    }

    

    public function chrd_list()
    {
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["department"] =  ClearanceCertification::department_list();
        $data["record"] =  $this->CC->get_chrd_personnel_record();
        return View::make("cc.report.chrd", $data);    
    }

    

    public function chrd_logs_get($id)
    {
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');
        return  $this->CC->get_chrd_logs($id);  
    }


    

    public function chrd_form($id)
    {
       
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');
        $data["id"] = $id;
        $data["base_url"] = URL::to("/");       
        $data["record"] =   ClearanceCertification::csdh_record($id , [1,5]);
        $data["other_dept"] =  ClearanceCertification::csdh_other_department($id);
        $data["comments"] =  ClearanceCertification::view_csdh_comment($id);
        $data["attachments"] =  ClearanceCertification::view_attachments($id);
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.chrd", $data);  
        }  
        
    }

     

    public function csdh_list()
    {
        $data["base_url"] = URL::to("/");       
        return View::make("cc.report.chsd", $data);    
    }

    

    public function csdh_list_get()
    {
        return $this->CC->get_csdh_record();
    }

    public function filer_list()
    {
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        return $this->CC->get_chrd_personnel_record(1);
    }

    public function resignee_list()
    {
        $data["base_url"] = URL::to("/");       
        return View::make("cc.report.resignee", $data);    
    }

    

    public function resignee_list_get()
    {
        return $this->CC->get_resignee_record();
    }

    public function filer_list_get()
    {
        return $this->CC->get_filer_record();
    }


     

    public function assesment_list()
    {
        //if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        return View::make("cc.report.assesment", $data);    
    }

    

    public function assesment_list_get()
    {
        return $this->CC->get_assestment_record();
    }


	

    public function create()
	{
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["employees"] =  ClearanceCertification::employees();
        $data["department"] =  ClearanceCertification::department();
        return View::make("cc.create", $data);    
    }

	/************************************************************************************/
        
	public function getEmployeerecord()
	{	
        $data =  $this->CC->getrecord();
        echo json_encode($data);
	}

    

    public function delete_record($id)
    {
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');
        return $this->CC->deleterecord($id);
    }

	

    public function acknowledge($id)
	{
        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::resignee_record($id);
        $data["comments"] =  ClearanceCertification::view_resignee_comment($id);
        $data["attachments"] =  ClearanceCertification::view_attachments($id);
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.resignee", $data);
        }        
    }

    

    public function edit($id)
	{
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::returned_record($id);
        $data["department"] =  ClearanceCertification::department();
        $data["other_department"] =  ClearanceCertification::clearance_department($id);
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;	
        $data["attachments"] =  ClearanceCertification::view_attachments($id);
        $data["comments"] =  ClearanceCertification::view_csdh_comment($id);
        
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.edit", $data);
        }        
    }

    

    public function csdh($id)
    {
        if(!Session::get("is_cc_csdh_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::csdh_record($id,[5]);
        $data["other_dept"] =  ClearanceCertification::csdh_other_department($id);
        $data["comments"] =  ClearanceCertification::view_csdh_comment($id);
        $data["attachments"] =  ClearanceCertification::view_attachments($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.csdh", $data);
        }        
    }

    

    public function chrd($id)
    {
        if(!Session::get("is_cc_chrd_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::chrd_record($id);
        $data["comments"] =  ClearanceCertification::view_chrd_comment($id);
        $data["attachments"] =  ClearanceCertification::view_chrd_attachments($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.chrd.head", $data);
        }        
    }

    

    public function department_head($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::department_head_record($id);
        $dept_id = Session::get("dept_id");
        $data["comments"] =  ClearanceCertification::view_department_comment($id , $dept_id);
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, $dept_id);
        $data["receiver"] =  ClearanceCertification::supervisor_list( $data["record"]['emp_no'] );
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.department.head", $data);
        }        
    }

    

    public function department_personnel($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::department_personnel_record($id);
        $dept_id = Session::get("dept_id");
        $data["comments"] =  ClearanceCertification::view_department_comment($id , $dept_id);
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, $dept_id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.department.assigned", $data);
        }        
    }

    

    public function department_review($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::department_review_record($id);
        $dept_id = Session::get("dept_id");
        $data["comments"] =  ClearanceCertification::view_department_comment($id , $dept_id);
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, $dept_id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("cc.department.approve", $data);
        }        
    }

    

    public function resignee_head($id)
	{
        if(!in_array(Session::get("desig_level") , ["head","top mngt","president","supervisor"])) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::resignee_head_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Resignee's Department");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Resignee's Department");
        $data["receiver"] =  ClearanceCertification::supervisor_list( $data["record"]['emp_no'] );
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.resignee.head", $data);
        }        
    }

    

    public function resignee_supervisor($id)
	{
        if(Session::get("desig_level") != "supervisor") return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::resignee_supervisor_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Resignee's Department");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Resignee's Department");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.resignee.supervisor", $data);
        }        
    }

    

    public function resignee_review($id)
	{
        if(!in_array(Session::get("desig_level") , ["head","top mngt","president","supervisor"])) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::resignee_review_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Resignee's Department");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Resignee's Department");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.resignee.approve", $data);
        }        
    }

    

    public function citd_technical($id)
	{
        if(!Session::get("is_cc_it_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::citd_technical_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "CITD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "CITD");
        $data["receiver"] =  ClearanceCertification::receivers_list(["CC_CITD_STAFF"] , $data["record"]['emp_no']);
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.citd.ithead", $data);
        }        
    }

    

    public function citd_staff($id)
	{
        if(!Session::get("is_cc_citd_staff")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::citd_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "CITD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "CITD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.citd.staff", $data);
        }        
    }

    

    public function citd_technical_review($id)
	{
        if(!Session::get("is_cc_it_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::citd_techreview_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "CITD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "CITD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.citd.ithead_approve", $data);
        }        
    }

    

    public function citd_head_review($id)
	{
        if(!Session::get("is_cc_citd_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::citd_headreview_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "CITD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "CITD");
        $data["receiver"] =  ClearanceCertification::receivers_list_head(["CC_IT_HEAD","CC_CITD_STAFF"] , $data["record"]['emp_no'],$id);
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.citd.approve", $data);
        }        
    }

    

    public function smdd_staff($id)
	{
        if(!Session::get("is_cc_smdd_staff")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::smdd_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "SMDD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "SMDD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.smd.staff", $data);
        }        
    }

    

    public function smdd_head_review($id)
	{
        if(!Session::get("is_cc_smdd_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::smdd_head_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "SMDD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "SMDD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.smd.head", $data);
        }        
    }

    

    public function erpd_staff($id)
	{
        if(!Session::get("is_cc_epr_staff")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::erpd_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "ERPD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "ERPD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.erp.staff", $data);
        }        
    }

    

    public function erpd_head_review($id)
	{
        if(!Session::get("is_cc_epr_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::erpd_head_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "ERPD");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "ERPD");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.erp.head", $data);
        }        
    }

    

    public function ssrv_staff($id)
	{
        if(!Session::get("is_cc_ssrv_staff")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::srvv_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "SSRV");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "SSRV");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.ssrv.staff", $data);
        }        
    }

    

    public function ssrv_head_review($id)
	{
        if(!Session::get("is_cc_ssrv_head")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::srvv_head_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "SSRV");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "SSRV");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.ssrv.head", $data);
        }        
    }

    

    public function chrd_training1($id)
	{
        if(!Session::get("is_cc_training1")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::training1_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Training1");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Training1");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.training1", $data);
        }        
    }

    

    public function chrd_cbr_personnel1($id)
	{
        if(!Session::get("is_cc_cbr_personnel1")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::cbrpersonnel1_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Personnel1");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Personnel1");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.compensation1", $data);
        }        
    }

    

    public function chrd_recruitment($id)
	{
        if(!Session::get("is_cc_recruitment")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::recruitment_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Recruitment");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Recruitment");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.recruitment", $data);
        }        
    }

    

    public function chrd_training2($id)
	{
        if(!Session::get("is_cc_training2")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::training2_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Training2");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Training2");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.training2", $data);
        }        
    }

    

    public function chrd_cbr_personnel2($id)
	{
        if(!Session::get("is_cc_cbr_personnel2")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::cbrpersonnel2_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Personnel2");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Personnel2");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.compensation2", $data);
        }        
    }

    

    public function chrd_cbr_manager($id)
	{
        if(!Session::get("is_cc_cbr_manager")) return Redirect::to('/');
        
        $data["base_url"] = URL::to("/");		
        $data["record"] =  ClearanceCertification::cbrmanager_staff_record($id);
        $data["comments"] =  ClearanceCertification::view_department_comment($id , "Manager");
        $data["attachments"] =  ClearanceCertification::view_department_attachments($id, "Manager");
        $data["id"] = $id;	
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("cc.chrd.compensationmanager", $data);
        }        
    }


    //ACTION MODULE FOR EACH FORM:
    

    public function create_action()
    {   
        $request = new CC\Requests\Store;
        $validate = $request->validate();
        if ($validate === true) 
        {
            if (Input::get('action') == "send") 
            {
                return $this->CC->resignee_sent();
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->resignee_save();
            }
        }
        else
        {
            return $validate;
        }       
    }

    

    public function acknowledge_action($id)
    {
        if (Input::get('action') == "acknowledge") 
        {
            return $this->CC->acknowledge_sent($id);
        }
        elseif(Input::get('action') == "return")
        {
            $request = new CC\Requests\Store;
            $validate = $request->validate_acknowledge_return($id);
            if ($validate === true) 
            {
                 return $this->CC->acknowledge_return($id);
            }
            else
            {
                return $validate;
            }            
        }
    }

    

    public function edit_action($id)
    {   
        $request = new CC\Requests\Store;
        $validate = $request->validate_edit($id);
        if ($validate === true) 
        {
            echo Input::get('action') . "jm";;
            if (Input::get('action') == "send") 
            {
                return $this->CC->edit_sent($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->edit_save($id);
            }   
        }
        else
        {
            return $validate;
        }     
    }

     

    public function csdh_action($id)
    {   
        if (Input::get('action') == "RETURN") 
        {
            $request = new CC\Requests\Store;
            $validate = $request->validate_csdh_return($id);
            if ($validate === true) 
            {
                return $this->CC->csdh_return($id);
            }
            else
            {
                return $validate;
            }                 
        }
        elseif (Input::get('action') == "send") 
        {
            return $this->CC->csdh_send($id);
        }
    }

     

    public function chrd_action($id)
    {   
        if (Input::get('action') == "RETURN") 
        {
            $request = new CC\Requests\Store;
            $validate = $request->validate_chrd_return($id);
            if ($validate === true) 
            {
                return $this->CC->chrd_return($id);
            }
            else
            {
                return $validate;
            }                 
        }
        elseif (Input::get('action') == "send") 
        {
            return $this->CC->chrd_send($id);
        }
    }

    

    public function resignee_head_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_resignee_head("cc/resignee/head/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->resignee_head_sent($id);
            }
            elseif(Input::get('action') == "assign")
            {
                return $this->CC->resignee_head_assign($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function department_head_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_department_head("cc/department/head/$id",1);
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->department_head_sent($id);
            }
            elseif(Input::get('action') == "assign")
            {
                return $this->CC->department_head_assign($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function department_personnel_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_department_head("cc/department/personnel/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->department_personnel_send($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->department_personnel_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

     

    public function department_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_department_head("cc/department/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "return")
            {
                return $this->CC->department_review_return($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->department_review_sent($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function resignee_supervisor_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_resignee_head("cc/supervisor/head/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->resignee_supervisor_sent($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->resignee_supervisor_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function resignee_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_resignee_head("cc/resignee/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "return")
            {
                return $this->CC->resignee_review_return($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->resignee_review_sent($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function citd_technical_action($id)
    {
        
        $request = new CC\Requests\Store;
        $validate = $request->validate_citd("cc/citd/technical/$id");
        if ($validate === true)
        {

            if (Input::get('action') == "assign")
            {
                return $this->CC->citd_technical_assign($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->citd_technical_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function citd_staff_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_citd("cc/citd/staff/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->citd_staff_send($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->citd_staff_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function citd_technical_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_citd("cc/citd/technical/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->citd_technical_review_send($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->CC->citd_technical_review_return($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function citd_head_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_citd("cc/citd/head/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->citd_head_review_send($id);
            }
            elseif(Input::get('action') == "assign")
            {
                return $this->CC->citd_head_review_assign($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function smdd_staff_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_smdd("cc/smdd/staff/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->smdd_staff_send($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->smdd_staff_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function smdd_head_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_smdd("cc/smdd/head/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->smdd_head_review_send($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->CC->smdd_head_review_return($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function erpd_staff_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_erpd("cc/erpd/staff/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->erpd_staff_send($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->erpd_staff_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function erpd_head_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_erpd("cc/erpd/head/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->erpd_head_review_send($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->CC->erpd_head_review_return($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function ssrv_staff_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_ssrv("cc/ssrv/staff/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->ssrv_staff_send($id);
            }
            elseif(Input::get('action') == "save")
            {
                return $this->CC->ssrv_staff_save($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function ssrv_head_review_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_ssrv("cc/ssrv/head/review/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->CC->ssrv_head_review_send($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->CC->ssrv_head_review_return($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_cbr_personnel1_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_compensation1("cc/chrd/cbr/personnel1/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_cbr_personnel1_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_cbr_personnel1_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_cbr_personnel2_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_compensation2("cc/chrd/cbr/personnel2/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_cbr_personnel2_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_cbr_personnel2_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_cbr_manager_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_manager("cc/chrd/cbr/manager/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_cbr_manager_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_cbr_manager_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_recruitment_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_recruitment("cc/chrd/recruitment/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_recruitment_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_recruitment_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }


    

    public function chrd_training1_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_training1("cc/chrd/training1/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_training1_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_training1_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_training2_action($id)
    {
        $request = new CC\Requests\Store;
        $validate = $request->validate_training2("cc/chrd/training2/$id");
        if ($validate === true)
        {
            if (Input::get('action') == "save")
            {
                return $this->CC->chrd_training2_save($id);
            }
            elseif(Input::get('action') == "send")
            {
                return $this->CC->chrd_training2_send($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    

    public function chrd_print($id)
    {
        if(!Session::get("is_cc_cbr_filer")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  ClearanceCertification::csdh_record($id , [1]);
        $data["other_dept"] =  ClearanceCertification::csdh_other_department($id);
        $data["comments"] =  ClearanceCertification::view_csdh_comment($id);
        $data["attachments"] =  ClearanceCertification::view_attachments($id);
        $data["id"] = $id;  
        $data["receiver"] = ClearanceCertification::receivers_clearance( $data["record"]['emp_no'] );
        $test =   View::make("cc.chrd_form", $data);     
       
        ob_start();  
        echo $test;
 
        $output = ob_get_clean();

        $path = 'assets/files/cc/';
        
        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get('name');
        $fname = "CC_$user" . "_$prnt_tstamp";
        $c = $path . $fname. '.html';
        $o = $path . $fname. '.pdf';

        file_put_contents($c ,$output);

        $fle = "wkhtmltopdf --dpi 125 --disable-external-links --disable-internal-links --orientation portrait --margin-top 12.7mm --margin-right 6.35mm --margin-bottom 12.7mm --margin-left 6.35mm --page-width 215.9mm --page-height 330.2mm \"$c\" \"$o\"";
        
         shell_exec($fle);

         //UPDATE PRINT STATUS
         ClearanceCertification::where("cc_id",$id)
         ->where("cc_status",1)
         ->update(["cc_print_status"=>1]);

         //UPDATE LOGS
         ClearanceCertification::update_logs($id);

         return Redirect::to("/$o");
    }

	
}