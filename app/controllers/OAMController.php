<?php

use RGAS\Modules\OAM;

class OAMController extends \BaseController 
{	
	private $OAM;
	public function __construct()
	{
		if( Session::get('company') != "RBC-CORP" ) return Redirect::to('/');
		$this->OAM = new OAM\OAM;	
	}

	public function index()
	{
		if( Session::get('company') != "RBC-CORP" ) return Redirect::to('/');
		$this->OAM->checkDivision();

		$data["subordinate"] = Cache::get("cache_byEmployee");
		$data["months"] = TMSTimeOutLog::month();
		$data["years"] = TMSTimeOutLog::year();
		$data["base_url"] = URL::to("/");
		$data["access"] = Session::get("desig_level");
		return View::make("oam/self" , $data);   
	}

	public function report()
	{
		if( Session::get('company') != "RBC-CORP" ) return Redirect::to('/');
		$check = $this->OAM->checkDivision();

		$data["department"] = $this->OAM->cache_Department;
		$data["section"] = $this->OAM->cache_Section;
		$data["months"] = TMSTimeOutLog::month();
		$data["years"] = TMSTimeOutLog::year();
		$data["base_url"] = URL::to("/");
		$data["access"] = Session::get("desig_level");
		$data["div_head"] = $check;
		return View::make("oam/index" , $data);   
	}

	public function oamlist()
	{
		set_time_limit(100000000);
		if( Session::get('company') != "RBC-CORP" ) return Redirect::to('/');
		$table =  $this->OAM->attendanceList();
		$legend =  $this->OAM->legend();
		echo json_encode(array("list"=>$table,"legend"=>$legend));
	}

	public function oamself()
	{
		if( Session::get('company') != "RBC-CORP" ) return Redirect::to('/');
		$legend =  array("legend"=>$this->OAM->legend());
		$record =  array_merge($legend , $this->OAM->attendanceself());
		echo json_encode($record);
	}

}