<?php

use RGAS\Modules\CBR;
use RGAS\Repositories\CBRRepository;
use RGAS\Libraries;

class CbrController extends \BaseController 
{    
	private $CBR;
	public function __construct()
	{
		$this->CBR = new CBR\CBR;
	}

    public function create()
	{
		if(Request::isMethod('post'))
		{
			$input = Input::all();
			if($input['action'] == 'save')
			{
				$is_save = $this->CBR->storeRequest($input);
				if($is_save === true){
					ReferenceNumbers::increment_reference_number('cbr');
					return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been saved!');
				}
			}
			
			if($input['action'] == 'send')
			{
				$request = new CBR\Requests\Store;
				$is_valid = $request->validate();
				if($is_valid === true){
					$is_sent = $this->CBR->storeRequest($input);
					if($is_sent === true){
						ReferenceNumbers::increment_reference_number('cbr');
						return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been sent!');
					}
				}
				else {
					return $is_valid;
				}
			}
		}
		
        $data["base_url"] = URL::to("/");
        return View::make("cbr/create", $data);    
    }
	
	public function SubmittedCBRRequests()
	{
		$data["base_url"] = URL::to("/");
		return View::make("cbr/submitted_cbr_requests", $data);
	}

	public function CBRRequests()
	{
		$data["base_url"] = URL::to("/");
		return View::make("cbr/cbr_requests", $data);
	}
	
	public function draftView($cb_ref_num)
	{
		
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbRefNum($cb_ref_num);
		
		if($request->cbrd)
		{
			$documents = array();
			foreach($request->cbrd as $a)
			{
				$b = json_decode($a->cbrd_req_docs, true);
				$documents[key($b)] = $b[key($b)];
			}
			$data["documents"] = $documents;
		}
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
		$this->CBR->draftviewLogCbr($cb_ref_num);
		return View::make("cbr/draft_view", $data); 
	}
	
	public function view($cbrd_id)
	{
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbrdRefNum($cbrd_id);
		$cbrd_ref_num = $request->cbrd_ref_num;

		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
		$this->CBR->viewLogs($cbrd_id, $cbrd_ref_num);
		return View::make("cbr/request_view",$data);
	}
	
	public function draftEdit($cb_ref_num)
	{
		if(Request::isMethod('post'))
		{
			$input = Input::all();
			if($input['action'] == 'save')
			{
				$is_save = $this->CBR->updateDraftRequest($cb_ref_num, $input);
				if($is_save === true){
					return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been saved!');
				}
			}
			
			if($input['action'] == 'send')
			{
				$request = new CBR\Requests\Update;
				$is_valid = $request->validate();
				if($is_valid === true){
					$is_sent = $this->CBR->updateDraftRequest($cb_ref_num, $input);
					if($is_sent === true){
						return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been sent!');
					}
				}
				else {
					return $is_valid;
				}
			}
		}
	
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbRefNum($cb_ref_num);
		
		if($request->cbrd)
		{
			$documents = array();
			foreach($request->cbrd as $a)
			{
				$b = json_decode($a->cbrd_req_docs, true);
				$documents[key($b)] = $b[key($b)];
			}
			$data["documents"] = $documents;
		}
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
	
		return View::make("cbr/draft_edit", $data); 
	}
	
	public function RequestsNew()
	{
		$cb = new CBRRepository;
		$requests = $cb->getRequestsNew();
		
		return json_encode($requests);
	}
	
	public function RequestsDashboardFiler()
	{
		$cbrd = new CBRRepository;
		$requests = $cbrd->getDashboardFiler();
		
		return json_encode($requests);
	}
	
	public function RequestsDashboardStaff()
	{
		$cbrd = new CBRRepository;
		$requests = $cbrd->getDashboardStaff();
		
		return json_encode($requests);
	}
	
	public function RequestsMy()
	{
		$cbrd = new CBRRepository;
		$requests = $cbrd->getRequestsMy();
		
		return json_encode($requests);
	}
	
	public function RequestsInProcess()
	{
		$cb = new CBRRepository;
		$requests = $cb->getRequestsInProcess();
		
		return json_encode($requests);
	}
	
	public function RequestsAcknowledged()
	{
		$base_url = URL::to('/');
		$cb = new CBRRepository;
		$requests = $cb->getRequestsAcknowledged();
		
		return json_encode($requests);
	}
	
	public function process($cbrd_ref_num)
	{
		$data["base_url"] = URL::to("/");
		$cb = new CBRRepository;
		$request = $cb->getRequestByCbrdRefNum($cbrd_ref_num);
		$data["request"] = $request;
		$chrd = Employees::get_all(16);
		$data["chrd"] = $chrd;
		return View::make("cbr/request_for_acknowledgement",$data);
	}
	
	public function reviewRequest($cb_ref_num, $view="")
	{

		if (Request::isMethod('post'))
		{	
			$input = Input::all();
			if($input['action'] == 'return')
			{
				$request = new CBR\Requests\SendBack;
				$is_valid = $request->validate();
				if($is_valid === true)
				{
					$this->CBR->returnRequest($input);
					return Redirect::to('cbr/review-request/'.$cb_ref_num)->with('message', 'The request has been returned.');
				}
				return $is_valid;
			}
			elseif($input['action'] == 'assign')
			{
				$request = new CBR\Requests\Assign;
				$is_valid = $request->validate();
				if($is_valid === true)
				{
					$this->CBR->assignRequest($input);
					return Redirect::to('cbr/review-request/'.$cb_ref_num)->with('message', 'The request has been assigned.');
				}
				return $is_valid;
			}
		}
		$data["base_url"] = URL::to("/");
		$cb = new CBRRepository;
		$request = $cb->getRequest($cb_ref_num);
		$data["request"] = $request[0];
		$data["view"] = false;
		if($view) $data["view"] = true;

		// $chrd = Employees::get_all(16); 
		$chrd = Receivers::get_cbr_staff(); //new dropdown list for cbr staff
		$data["chrd"] = $chrd;

		return View::make("cbr/request_review",$data);
	}

	public function forAcknowledgement($cbrd_ref_num)
	{
		$input = Input::all();
		$request = new CBR\Requests\ForAcknowledgement;
		$is_valid = $request->validate();
		if($is_valid === true)
		{
			$this->CBR->forAcknowledgement($input);
			return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Request successfully sent to Filer for Acknowledgement.');
		}
		return $is_valid;
	}
	
	public function acknowledged()
	{
		$input = Input::all();
		$this->CBR->acknowledgedRequest($input);
		return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'You have just been acknowledge your request.');
	}
	
	public function delete($status,$cbrd_ref_num)
	{
		$affectedRows = $this->CBR->deleteRequest($status,$cbrd_ref_num);
		return Redirect::to( 'cbr/submitted-cbr-requests' )
			->with('message', $affectedRows . ' Request has been deleted');
	}
	
	public function returnedEdit($cbrd_id)
	{
		$input = Input::all();
		
		if (Request::isMethod('post'))
		{	
			if($input['action'] == 'save'){
				$this->CBR->returnedEdit($input);
				return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Returned request has been saved.');
			}
			elseif($input['action'] == 'send'){
				$request = new CBR\Requests\ReturnedEdit;
				$is_valid = $request->validate();
				if($is_valid === true)
				{
					// $input = Input::all();
					$this->CBR->returnedEdit($input);
					return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Returned request has been sent.');
				}
				else
				{
					return $is_valid;
				}
			}
			
		}
	
		$cbrd = new CBRRepository();
		$request = $cbrd->getRequestByCbrdRefNum($cbrd_id);

		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$request->cbrd_req_docs = json_decode($request->cbrd_req_docs);
		return View::make("cbr/returned_edit", $data);    
	}
	
	public function returnedView($cbrd_id)
	{
		$cbrd = new CBRRepository();
		$request = $cbrd->getRequestByCbrdRefNum($cbrd_id);

		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$request->cbrd_req_docs = json_decode($request->cbrd_req_docs);
		return View::make("cbr/returned_view", $data);    
	}
	
	public function download($cb_ref_num, $random_filename, $original_filename)
	{
		$cbrd = new CBRRepository();
		$request = $cbrd->getRequestByCbrdRefNum($cb_ref_num);

		$fm = new Libraries\FileManager;
		$filepath = Config::get('rgas.rgas_storage_path') . $this->CBR->attachments_path . "$cb_ref_num/" . $random_filename;
		if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
			$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
		}
		
		$this->CBR->downloadLog($cb_ref_num);
		return $fm->download($filepath, CIEncrypt::decode($original_filename));		
	}
	
	/*
	public function resubmit()
	{
		$input = Input::all();
		echo "<pre>",print_r($input),"</pre>";
		$this->CBR->reSubmitRequest($input);
		return Redirect::to('cbr/submitted-cbr-requests')->with('message', 'Your request has been re-submitted');
	}
	*/
	
}