<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 08/12/2016
 * Time: 10:17 AM
 */
use RGAS\Modules\MOC;
use RGAS\Libraries;

class ajaxMOCController extends \BaseController
{
    protected $MOCvalidator;
    protected $MOC;

    public function __construct() {
        $this->MOCvalidator = new MOC\MOCValidator();
        $this->MOC = new MOC\MOC();
    }

    /*******************POPULATING DATA ON CREATION AND MY OWN MOC**************************/
    public function ajaxGetDepartment() {
        if(Request::ajax()) {
            return Departments::get();
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxFeedEditMOC() {
        if(Request::ajax()) {
            $mocDetails = MOCRequests::where("request_id",Input::get("id"))
                ->with('attachments')
                ->with('ccodes')
                ->with('comments')
                ->with('rcodes')
                ->with('signatories')
                ->first();
            for ($i = 0; $i < count($mocDetails['attachments']); $i++) {
                $imageDetails = json_decode($mocDetails['attachments'][$i]['fn']);
                $mocDetails['attachments'][$i]['urlDownload'] = URL::to('/moc/download'.'/'.$mocDetails['transaction_code'].'/'.$imageDetails->random_filename.'/'.CIEncrypt::encode($imageDetails->original_filename));
            }

            for ($i = 0; $i < count($mocDetails['comments']); $i++) {
                $newDate = date('m/d/Y h:i A', strtotime($mocDetails['comments'][$i]['ts_comment']));
                $mocDetails['comments'][$i]['ts_comment'] = $newDate;
            }

            return $mocDetails;
        }else{
            return Redirect::to("/");
        }
    }

    /***********For AJAX Create and My MOC action*************/

    public function ajaxSendMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->send(Input::all());
            if ($validate['success']) {
                return $this->MOC->createAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxSaveMOC() {
        if(Request::ajax()) {
            return $this->MOC->createAction(Input::all());
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxResendMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->send(Input::all());
            if ($validate['success']) {
                return $this->MOC->reCreateAction(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxResaveMOC() {
        if(Request::ajax()) {
            return $this->MOC->reCreateAction(Input::all());
        }else{
            return Redirect::to("/");
        }
    }

    public function ajaxCancelMOC() {
        if(Request::ajax()) {
            $validate = $this->MOCvalidator->cancelRequest(Input::all());
            if ($validate['success']) {
                return $this->MOC->cancelMOC(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }else{
            return Redirect::to("/");
        }
    }

    /*******************POPULATING DATA ON FOR APPROVAL MOC**************************/
    public function ajaxFeedEditForApproveMOC() {
        if(Request::ajax()) {
            $mocDetails = MOCRequests::where("request_id",Input::get("id"))
                ->with('attachments')
                ->with('ccodes')
                ->with('comments')
                ->with('rcodes')
                ->with('signatories')
                ->first();
            for ($i = 0; $i < count($mocDetails['attachments']); $i++) {
                $imageDetails = json_decode($mocDetails['attachments'][$i]['fn']);
                $mocDetails['attachments'][$i]['urlDownload'] = URL::to('/moc/download'.'/'.$mocDetails['transaction_code'].'/'.$imageDetails->random_filename.'/'.CIEncrypt::encode($imageDetails->original_filename));
            }

            for ($i = 0; $i < count($mocDetails['comments']); $i++) {
                $newDate = date('m/d/Y h:i A', strtotime($mocDetails['comments'][$i]['ts_comment']));
                $mocDetails['comments'][$i]['ts_comment'] = $newDate;
            }

            return $mocDetails;
        }else{
            return Redirect::to("/");
        }
    }

    /***********For AJAX for approval MOC action*************/
    
        /******RETURN TO FILER******/
        public function returnToFiler() {
            if(Request::ajax()) {
                $validate = $this->MOCvalidator->comment(Input::all());
                if ($validate['success']) {
                    return $this->MOC->returnToFiler(Input::all());
                }else{
                    return Response::json(array(
                        'success' => false,
                        'errors' => $validate['errors'],
                        'message' => 'Some fields are incomplete',
                        400
                    ));
                }
            }else{
                return Redirect::to("/");
            }
        }

        /******SEND TO FILER FOR ACKNOWLEDGEMENT******/
        public function sendToFiler() {
            if(Request::ajax()) {
                return $this->MOC->sendToFilerForAcknowledgement(Input::all());
            }else{
                return Redirect::to("/");
            }
        }

        /******ACKNOWLEDGE MOC******/
        public function acknowledgeMOC() {
            if(Request::ajax()) {
                return $this->MOC->acknowledgeMOC(Input::all());
            }else{
                return Redirect::to("/");
            }
        }


    /*******GENERAL ROUTING*******/
        public function ajaxDHtoMD() {
    
            if(Request::ajax()) {
    
                if(Input::get('action') == 'sendToMD') {
                    return $this->MOC->sendToMD(Input::all());
                }
    
            }else{
                return Redirect::to("/");
            }
            
        }
    
        public function ajaxDHtoADH() {
            $validate = $this->MOCvalidator->DHtoADH(Input::all());
            if ($validate['success']) {
                return $this->MOC->sendDHtoADH(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }

        public function ajaxADHtoDH() {
            return $this->MOC->sendADHtoDH(Input::all());
        }
    
        public function ajaxADHtoBSA() {
            $validate = $this->MOCvalidator->ADHtoBSA(Input::all());
            if ($validate['success']) {
                return $this->MOC->sendADHtoBSA(Input::all());
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => $validate['errors'],
                    'message' => 'Some fields are incomplete',
                    400
                ));
            }
        }

        public function ajaxSendBSAtoADH() {
            return $this->MOC->sendBSAtoADH(Input::all());
        }

        public function ajaxReturntoBSA() {
            return $this->MOC->returntoBSA(Input::all());
        }

        public function ajaxSendtoDivHead() {
            return $this->MOC->sendtoDivHead(Input::all());
        }
        /************* CORPORATE ROUTING  **********/
    
        /************* SATELLITE ROUTING  **********/
        public function ajaxToAOM() {
            if(Input::get('action') == 'sendToAOM'){
                $validate = $this->MOCvalidator->toAOM(Input::all());
                if ($validate['success']) {
                    return $this->MOC->sendToAOM(Input::all());
                }else{
                    return Response::json(array(
                        'success' => false,
                        'errors' => $validate['errors'],
                        'message' => 'Some fields are incomplete',
                        400
                    ));
                }
            }
        }

        //aom to csmd dh
        public function ajaxAOMtoDH() {
            return $this->MOC->sendToMD(Input::all(),2);
        }

        public function ajaxDivHeadtoSSMD() {
            return $this->MOC->sendDivHeadtoSSMD(Input::all());
        }

        /**** GET DEPARTMENTS ON COMPANY******/

        public function ajaxGetDepartments() {
//            return MOCRequests::where('comp_code',Input::get('company'))->select(['dept_name','id'])->get();
//            return DB::table('moc_requests')
//                ->join('departments',DB::raw( "CONCAT('%', moc_requests.dept_sec, '%')") ,'LIKE','departments.dept_name')->get();
//            $requests = MOCRequests::where('company',Input::get('company'))
//                ->get(['dept_sec']);
            $SSMDs = DB::table('employees')
                ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
                ->where("sections.sect_name","SATELLITE SYSTEMS")
                ->select('employees.id as empID')
                ->get();
            $SSMDids = [];
            foreach($SSMDs as $SSMD) {
                array_push($SSMDids,$SSMD->empID);
            }
            $requests = DB::table('moc_requests')
                ->leftjoin("moc_signatories",'moc_requests.request_id','=','moc_signatories.moc_request_id')
                ->leftjoin('employees','moc_requests.curr_emp','=','employees.id')
                ->where("moc_requests.company",Input::get('company'))
                ->select('moc_requests.*','moc_signatories.*');
            if (Session::get("company") != 'RBC-CORP') {
                $requests->where('moc_requests.company', Session::get("company"));
                $requests->whereIn('moc_requests.status', ["FOR ASSESSMENT", "FOR ACKNOWLEDGEMENT","ACKNOWLEDGED","FOR APPROVAL"]);
            }else{
                $requests->where(function ($query) use ($SSMDids){
                return  $query->whereNotIn('moc_requests.curr_emp',$SSMDids)
                    ->whereIn('moc_requests.status', ["FOR ASSESSMENT"])
                    ->orwhere(['moc_requests.status' => 'FOR APPROVAL','moc_signatories.signature_type' => 6])
                    ->orwhere(['moc_requests.status' => "FOR ACKNOWLEDGEMENT",'moc_signatories.signature_type' => 7])
                    ->orwhere(['moc_requests.status' => "ACKNOWLEDGED",'moc_signatories.signature_type' => 8]);
                });
               
            }
            $requests = $requests->get(['moc_requests.dept_sec']);
            $departments = [];
            foreach($requests as $request) {
                array_push($departments,json_decode($request->dept_sec)[0]);
            }

            return Departments::where('comp_code',Input::get('company'))
                ->whereIn("dept_name",$departments)
                ->selectRaw("DISTINCT(dept_name),id")->get();

        }

        /**** submitted MOC******/

        public function ajaxSearchMOC() {
            $submittedMOC = MOCRequests::search();
            $result_data = [];
            if ( count($submittedMOC) > 0){
                $ctr = 0;
                foreach($submittedMOC as $req) {
                    $result_data[$ctr][] = $req->transaction_code;
                    $result_data[$ctr][] = $req->title;
                    $result_data[$ctr][] = $req->status;
                    $result_data[$ctr][] = $req->firstname.' '.$req->lastname;
                    $result_data[$ctr][] = $req->curr_emp_firstname.' '.$req->curr_emp_lastname;
                    $result_data[$ctr][] = $req->assigned_date;
                    $result_data[$ctr][] = $req->acknowledge_date == "0000-00-00" ? "" : $req->acknowledge_date;
                    if($req->curr_emp == Session::get("employee_id")) {
                        $result_data[$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req->request_id).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req->request_id).">assess</a>";
                    }else{
                        $result_data[$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req->request_id).">View</a><a style='margin-left: 3px' disabled class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req->request_id).">assess</a>";
                    }
                    $ctr++;
                }
            }
            return $result_data;

        }
    
        public function ajaxSaveByApprover() {
            return $this->MOC->saveByApprover(Input::all());
        }

        public function ajaxReturn() { //returning for CSMD
            return $this->MOC->returnRouting(Input::all());
        }
        
        public function ajaxReturnSat() { //returning for CSMD
            return $this->MOC->returnSat(Input::all());
        }

}