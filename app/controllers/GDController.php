<?php

use RGAS\Modules\GD;
use RGAS\Libraries;


class GDController extends \BaseController 
{

    protected $module_id;
    

    
	public function __construct()
	{		
		$this->GD = new GD\GD;	
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
	}

    public function download($ref_num, $random_filename, $original_filename)
    {       
        $fm = new Libraries\FileManager;
        
        if(glob(Config::get('rgas.rgas_storage_path') . "gd/" . "$ref_num/" . $random_filename))
        {
            $filepath = Config::get('rgas.rgas_storage_path') . "gd/" . "$ref_num/" . $random_filename;        
        }
        else if(glob(Config::get('rgas.rgas_temp_storage_path') . $random_filename))
        {
             $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;       
        }      
        else
        {
            $filepath =  Config::get('rgas.rgas_storage_path') . "gd/" . CIEncrypt::decode($original_filename);
        }

        return $fm->download($filepath,  CIEncrypt::decode($original_filename));
    }

    

    public function reviewer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->GD->get_reviewer_record();
        return View::make("gd.reviewer", $data);    
    }

    

    public function filer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->GD->get_record();
        $data["reviewer_record"] =  $this->GD->get_reviewer_record();
        return View::make("gd.filer", $data);    
    }
        
    


	public function create()
	{
        $data["base_url"] = URL::to("/");		
        $data["employees"] =  GeneralDocuments::employees();
        $data["section"] =  GeneralDocuments::section();
        $data["doc_type"] =  GeneralDocuments::doc_type();
        return View::make("gd.create", $data);    
    }    

    

    public function edit($id)
	{
        $data["base_url"] = URL::to("/");		
        $data["record"] =  GeneralDocuments::filer_record($id);
        $data["signatory"] =  GeneralDocuments::filer_signatory($id);
        $data["employees"] =  GeneralDocuments::employees();
        $data["section"] =  GeneralDocuments::section();
        $data["doc_type"] =  GeneralDocuments::doc_type();
        $data["attachments"] =  GeneralDocuments::attachments($id);
        $data["id"] = $id;	
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("gd.edit", $data);
        }        
    }

    

    public function view($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  GeneralDocuments::view_record($id);
        $data["signatory"] =  GeneralDocuments::reviewer_signatory($id);
        $data["approve_list"] =  GeneralDocuments::approved_signatory($id);
        $data["receiver_list"] =  GeneralDocuments::gd_receivers();
        $data["attachments"] =  GeneralDocuments::attachments($id);
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("gd.view", $data);
        }        
    }

    

    public function delete_record($id)
    {
        return $this->GD->deleterecord($id);
    }

    

    public function review_form($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  GeneralDocuments::reviewer_record($id);
        $data["signatory"] =  GeneralDocuments::reviewer_signatory($id);
        $data["approve_list"] =  GeneralDocuments::approved_signatory($id);
        $data["receiver_list"] =  GeneralDocuments::gd_receivers();
        $data["attachments"] =  GeneralDocuments::attachments($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("gd.reviewform", $data);
        }        
    }

    


    public function create_action()
    {   
        if (Input::get('action') == "send") 
        {
            $request = new GD\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) 
            {
                return $this->GD->gd_sent();
            }
            else
            {
                return $validate;
            }               
        }
        elseif(Input::get('action') == "save")
        {
            return $this->GD->gd_save();
        }
            
    }

    

    public function edit_action($id)
    {   
        if (Input::get('action') == "send") 
        {
            $request = new GD\Requests\Store;
            $validate = $request->validate_edit($id);
            if ($validate === true) 
            {
                return $this->GD->edit_sent($id);
            }
            else
            {
                return $validate;
            } 
        }
        elseif(Input::get('action') == "save")
        {
            return $this->GD->edit_save($id);
        }
              
    }

    
    public function view_action($id)
    {   
        $request = new GD\Requests\Store;
        $validate = $request->cancel_validate($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "cancel") 
            {
                return $this->GD->view_cancel($id);
            }
        }
        else
        {
            return $validate;
        }       
    }

    

    public function review_action($id)
    {   
        $request = new GD\Requests\Store;
        $validate = $request->review_validate($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "approve") 
            {
                return $this->GD->review_approve($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->GD->review_return($id);
            }
            elseif(Input::get('action') == "return_to_ea")
            {
                return $this->GD->review_return_to_ea($id);
            }
        }
        else
        {
            return $validate;
        }       
    }

    public function check_ref($ref)
    {   
        $rec = GeneralDocuments::where("reference_no",$ref)->get()->toArray();
        $data["duplicate"] = empty($rec) ? 0 : 1;
        echo json_encode($data);
    }

     public function review_get()
    {
        return $this->GD->get_reviewer();
    }

      public function review_get_dashboard()
    {
        return $this->GD->get_reviewer_dashbard();
    }
        
}
