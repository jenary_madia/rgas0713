<?php

use RGAS\Modules\PMF;
use RGAS\Libraries;


class PmfController extends \BaseController 
{
    public function __construct()
    {
        
        $this->PMF = new PMF\PMF;   
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
    }

    public function download($ref_num, $random_filename, $original_filename)
    {       
        $fm = new Libraries\FileManager;

        if(glob(Config::get('rgas.rgas_storage_path') . "pmf/" . "$ref_num/" . $random_filename))
        {
            $filepath = Config::get('rgas.rgas_storage_path') . "pmf/" . "$ref_num/" . $random_filename;        
        }
        else if(glob(Config::get('rgas.rgas_temp_storage_path') . $random_filename))
        {
             $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;       
        }      
        else
        {
            $filepath =  (Config::get('rgas.rgas_storage_path') . "pmf/" . CIEncrypt::decode($original_filename));
        }

        return $fm->download(trim($filepath),  trim(CIEncrypt::decode($original_filename)));
    }

    public function create()
    {	
        $data["period"] =  Performance::period();
        $data["purpose"] =  Performance::purpose();
        $this->PMF->getSubordinate();
        $data["employee"] = Cache::get("cache_Employee");

	    $data["base_url"] = URL::to("/");        
        return View::make("pmf.creates", $data);     
    }

    public function create_sub_pmf($pmf,$purpose, $eid , $content=null, $copy=null, $sub=null)
    {   
        $data["kra_cluster"] =  Performance::cluster( $pmf );
        $data["behavior"] =  Performance::behavior( $pmf );
        $data["build"] =  Performance::build( $pmf );
        $data["e_record"] =  Performance::employee_record( $eid );
        $data["record"] =  Performance::copy_record($purpose, $content , $copy,$sub);
        $data["reccomendation"] =  Performance::reccomendation( $pmf );
        $data["pmf"] =  $pmf;
        $data["purpose"] = $purpose;
        $data["cover"] =  Performance::cover( $pmf );
        $data["base_url"] = URL::to("/");        

        return View::make("pmf.create_sub", $data);     
    }

    public function create_pmf($pmf,$purpose,$content=null,$copy=null)
    {   
        $data["kra_cluster"] =  Performance::cluster( $pmf );
        $data["behavior"] =  Performance::behavior( $pmf );
        $data["build"] =  Performance::build( $pmf );
        $data["record"] =  Performance::copy_record($purpose, $content , $copy);
        $data["pmf"] =  $pmf;
        $data["purpose"] = $purpose;
        $data["cover"] =  Performance::cover( $pmf );
        $data["base_url"] = URL::to("/");        

        return View::make("pmf.create", $data);     
    }

    public function filer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->PMF->get_record();
        $data["sub_record"] =  $this->PMF->sub_record();
        $data["approval_record"] =  $this->PMF->forapproval_record();
        $data["department"] =  Arts::department();
        return View::make("pmf.filer", $data);
    }

    public function export_report()
    {
        $data["base_url"] = URL::to("/");
        return View::make("pmf.export", $data);
    }

     public function create_action($sub=null)
    {           
        if (Input::get('action') == "send") 
        {
            $request = new PMF\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) 
            {
                return $this->PMF->create_sent(  $sub );
            }
            else
            {
                return $validate;
            }   
        }
        elseif(Input::get('action') == "save")
        {
            return $this->PMF->create_save( $sub );
        }  
        elseif(Input::get('action') == "return")
        {
            return $this->PMF->create_return(  );
        }             
    }


    public function export_report_action()
    {   
        if(Input::get('action') == "export")
        {
            $start = Input::get("date_from");
            $last = Input::get("date_to");
            $end  = date("Y-m-d 23:59:59",strtotime(Input::get("date_to")));
            $pur = is_array(Input::get("purpose")) ? Input::get("purpose") : array();
            $this->PMF->getSubordinate();
            $employees = is_array(Cache::get("cache_Employee")) ? Cache::get("cache_Employee")  : array() ;
            $eid[] = Session::get('employee_id');
            foreach($employees as $id=>$rs)
            {
                $eid[] = $id;
            }


            $result_data = array();
            $requests = Performance::join("employees" , "employees.id","=","pmf_employees.employee_id")
                ->whereBetween("ts_created",[$start,$end])
                ->whereIn("pmf_purposes.purpose" , $pur)
                ->whereIn("employees.id" , $eid)
                ->join("pmf_purposes","pmf_purposes.purpose_id","=","pmf_employees.purpose")
                ->get(["firstname","lastname","employees.designation","pmf_purposes.purpose","score","classification","employees.employeeid"]);


            include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
        
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 1;   
            
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'PERFORMANCE SUMMARY: CORPORATE SYSTEMS');
            $objPHPExcel->getActiveSheet()->getStyle("A$start_row:E$start_row")->getFont()->setBold(true);  
           

            $start_row++;
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'FROM:');
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $start);
            $objPHPExcel->getActiveSheet()->getStyle("A$start_row:E$start_row")->getFont()->setBold(true);
            $start_row++;
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'TO:');
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $last);
            $objPHPExcel->getActiveSheet()->getStyle("A$start_row:E$start_row")->getFont()->setBold(true);
            $start_row++;
            $start_row++;
  

            $purpose = array();
            foreach($requests as $rs)
            {
                $purpose[$rs->purpose] =  $rs->purpose;
            }

            foreach($purpose as $p)
            {
                $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", strtoupper($p));
                $objPHPExcel->getActiveSheet()->getStyle("A$start_row:E$start_row")->getFont()->setBold(true);
                $start_row++;
                $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'EMPLOYEE NAME');
                $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'EMP. NO');
                $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'POSITION');
                $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", '%RATING');
                $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'EQUIVALENT');
                $objPHPExcel->getActiveSheet()->getStyle("A$start_row:E$start_row")->getFont()->setBold(true);

               
                foreach($requests as $rs)
                {
                 if($p ==  $rs->purpose ):
                    $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", "$rs->firstname $rs->lastname");
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $rs->employeeid );
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $rs->designation);
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $rs->score);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $rs->classification);

                  endif;
                }
               

                $start_row++;
                $start_row++;
            }
            
            foreach(range('B','H') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);

             //WRITE AND DOWNLOAD EXEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();

            $fname = "rgas";

            $objWriter->save("../app/storage/rgas/pmf/$fname.xls");
            $objPHPExcel->disconnectWorksheets();

              $headers = array(
              'Content-Type: application/xls',
            );

            return Response::download("../app/storage/rgas/pmf/$fname.xls", "$fname.xls", $headers);
        }           
    }


    public function edit($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  Performance::filer_record($id);
        $data["id"] = $id;  
        
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            $data["kra_cluster"] =  Performance::cluster( $data["record"]["pmf"] );
            $data["behavior"] =  Performance::behavior( $data["record"]["pmf"] );
            $data["build"] =  Performance::build( $data["record"]["pmf"] );
            $data["reccomendation"] =  Performance::reccomendation( $data["record"]["pmf"]  );

            $data["readonly"] = $this->Readonly;
            return View::make("pmf.edit", $data);
        }        
    }

     public function edit_action($id)
    {   
        if (Input::get('action') == "send") 
        {
             return $this->PMF->edit_sent($id);
        }
        elseif(Input::get('action') == "save")
        {
            return $this->PMF->edit_save($id);
        }   
        elseif(Input::get('action') == "return")
        {
            return $this->PMF->edit_return( $id);
        }                 
    }



    public function approval($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  Performance::approval_record($id);
        $data["id"] = $id;  
        
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            $data["kra_cluster"] =  Performance::cluster( $data["record"]["pmf"] );
            $data["behavior"] =  Performance::behavior( $data["record"]["pmf"] );
            $data["build"] =  Performance::build( $data["record"]["pmf"] );
            $data["reccomendation"] =  Performance::reccomendation( $data["record"]["pmf"]  );

            $data["readonly"] = $this->Readonly;
            return View::make("pmf.approval", $data);
        }        
    }

     public function approval_action($id)
    {   
        if (in_array(Input::get('action') , ["head","send","hr" ])) 
        {
          return $this->PMF->approval_send($id); 
        }
        elseif(Input::get('action') == "save")
        {
            return $this->PMF->approval_save($id);
        }      
        elseif(Input::get('action') == "return")
        {
            $request = new PMF\Requests\Store;
            $validate = $request->validate_return($id);
            if ($validate === true)
            {
                return $this->PMF->approval_return($id);
            }
            else
            {
                return $validate;
            }
        }         
    }


     public function submitted_report()
    {
        if(Session::get("is_pmf_receiver") == "") return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->PMF->submitted_record();
        $data["department"] =  Arts::department();
        $data["period"] =  Performance::period();
        return View::make("pmf.submitted", $data);
    }


     public function receiver_record($id)
    {
        if(Session::get("is_pmf_receiver") == "") return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  Performance::submmited_record($id);
        $data["assign"] =  Performance::assign();
        $data["id"] = $id;  
        
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            $data["kra_cluster"] =  Performance::cluster( $data["record"]["pmf"] );
            $data["behavior"] =  Performance::behavior( $data["record"]["pmf"] );
            $data["build"] =  Performance::build( $data["record"]["pmf"] );
            $data["reccomendation"] =  Performance::reccomendation( $data["record"]["pmf"]  );

            $data["readonly"] = $this->Readonly;
            return View::make("pmf.receiver", $data);
        }        
    }

     public function receiver_record_action($id)
    {   
        if(Session::get("is_pmf_receiver") == "") return Redirect::to('/');

        if(Input::get('action') == "assign")
        {
             $request = new PMF\Requests\Store;
            $validate = $request->assign($id);
            if ($validate === true) 
            {
               return $this->PMF->submitted_assign($id);
            }
            else
            {
                return $validate;
            }   
            
        }
        elseif(Input::get('action') == "approve")
        {

            $request = new PMF\Requests\Store;
            $validate = $request->validate_return_approve($id);
            if ($validate === true)
            {
                return $this->PMF->submitted_approve($id);
            }
            else
            {
                return $validate;
            }
            
        }           
        elseif(Input::get('action') == "return")
        {
            $request = new PMF\Requests\Store;
            $validate = $request->validate_return_submitted($id);
            if ($validate === true)
            {
                return $this->PMF->submitted_return($id);
            }
            else
            {
                return $validate;
            }
            
        }             
    }


    public function check_sub_record($pmf,$purpose, $eid , $content=null, $copy=null, $sub=null)
    {   
        $record =Performance::join("pmf_purposes","pmf_purposes.pmf_id","=","pmf_employees.pmf_id")
            ->join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
            ->join("pmf","pmf.pmf_id","=","pmf_employees.pmf_id")
            ->whereIn("pmf_purposes.purpose",["Yearly Evaluation","For Regularization"])
            ->where("purpose_id",$purpose)
            ->where("pmf_employees.purpose",$purpose)
            ->where("e.id", $eid )
            ->where("pmf_employees.pmf_id",$pmf)
            ->get(["pmf_purposes.purpose","year","e.firstname","lastname"]);

        if(!$record->isEmpty())
        {
           $data["result"] = 0;
           $data["message"] = $record[0]->purpose . " PMF of " . $record[0]->firstname  . " " . $record[0]->lastname  ." for the year " . $record[0]->year . " already exists.";            
        }
        else
        {
            if($content)
            {
                $purpose = DB::table("pmf_purposes")
                ->where("purpose_id", $purpose)
                ->get(["purpose"]);

                $record =Performance::join("pmf_purposes","pmf_purposes.purpose_id","=","pmf_employees.purpose")
                ->join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
                ->whereIn("status_id" , [4,11])
                ->where("pmf_purposes.purpose",$purpose[0]->purpose)
                ->where("e.id", $sub )
                ->get(["pmf_purposes.purpose"]);

                if($record->isEmpty())
                {
                    $data["result"] = 0;
                    $data["message"] = "No available file.";
                }
                else
                {
                    $data["result"] = 1;
                }
            }
            else
            {
                $data["result"] = 1;
            }
        }
        

        echo json_encode($data);  
    }

    public function check_record($pmf,$purpose,$content=null,$copy=null)
    {   
        $record =Performance::join("pmf_purposes","pmf_purposes.pmf_id","=","pmf_employees.pmf_id")
            ->join("pmf","pmf.pmf_id","=","pmf_employees.pmf_id")
            ->whereIn("pmf_purposes.purpose",["Yearly Evaluation","For Regularization"])
            ->where("purpose_id",$purpose)
            ->where("pmf_employees.purpose",$purpose)
            ->where("employee_id",Session::get('employee_id'))
            ->where("pmf_employees.pmf_id",$pmf)
            ->get(["pmf_purposes.purpose","year"]);

        if(!$record->isEmpty())
        {
           $data["result"] = 0;
           $data["message"] = "Your " . $record[0]->purpose . " PMF for the year " . $record[0]->year . " already exists.";
        }
        else
        {
            if($content)
            {
                $purpose = DB::table("pmf_purposes")
                ->where("purpose_id", $purpose)
                ->get(["purpose"]);

                $record =Performance::join("pmf_purposes","pmf_purposes.purpose_id","=","pmf_employees.purpose")
                ->whereIn("status_id" , [4,11])
                ->where("pmf_purposes.purpose",$purpose[0]->purpose)
                ->where("employee_id",Session::get('employee_id'))
                ->get(["pmf_purposes.purpose"]);

                if($record->isEmpty())
                {
                    $data["result"] = 0;
                    $data["message"] = "No available file.";
                }
                else
                {
                    $data["result"] = 1;
                }
            }
            else
            {
                $data["result"] = 1;
            }
        }
        

        echo json_encode($data);  
    }


    public function pmf_print($id)
    {
        return $this->PMF->form_print($id);        
    }


     public function delete_record($id)
    {
        return $this->PMF->deleterecord($id);
    }

      public function review_get()
    {
        return $this->PMF->get_reviewer();
    }

}