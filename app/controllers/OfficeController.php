<?php

use RGAS\Modules\OS;
use RGAS\Libraries;


class OfficeController extends \BaseController 
{

    protected $module_id;
    
	public function __construct()
	{		
		$this->OS = new OS\OS;	
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
	}

    public function create()
    {
        $data["company"] =  OfficeSales::company();
        $data["food"] =  OfficeSales::food();
        $data["non_food"] =  OfficeSales::non_food();
        $data["base_url"] = URL::to("/");
        if(Session::get('is_affiliate'))
        {
            $data["so_code"] =  OfficeSales::so_code(2);
            return View::make("os.create_affiliate", $data);   
        }
        else
        {
            $data["so_code"] =  OfficeSales::so_code(1);
            return View::make("os.create", $data);    
        }
    }


    public function create_action()
    {   
        if (Input::get('action') == "send") 
        {
           $request = new OS\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) 
            {
                 return $this->OS->create_sent();
            }
            else
            {
                return $validate;
            }     
        }
        elseif(Input::get('action') == "save")
        {
            return $this->OS->create_save();
        }      
    }

    public function edit($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::filer_record($id);
        $data["food"] =  OfficeSales::food();
        $data["non_food"] =  OfficeSales::non_food();

        $data["company"] =  OfficeSales::company();
        $data["uom"] =  OfficeSales::uom();
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            if(Session::get('is_affiliate'))
            {
                $data["so_code"] =  OfficeSales::so_code(2);
                return View::make("os.edit_affiliate", $data);   
            }
            else
            {
                $data["so_code"] =  OfficeSales::so_code(1);
                return View::make("os.edit", $data);    
            }

        }        
    }

    public function view($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::filer_record($id);
        $data["food"] =  OfficeSales::food();
        $data["non_food"] =  OfficeSales::non_food();

        $data["company"] =  OfficeSales::company();
        $data["uom"] =  OfficeSales::uom();
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            if(Session::get('is_affiliate'))
            {
                $data["attachments"] =  OfficeSales::attachments($id);
                $data["so_code"] =  OfficeSales::so_code(2);
                return View::make("os.affiliate_view", $data);   
            }
            else
            {
                $data["so_code"] =  OfficeSales::so_code(1);
                return View::make("os.view", $data);    
            }

        }        
    }


    public function view_action($id)
    {   
        if(Input::get('action') == "cancel")
        {
            $request = new OS\Requests\Store;
            $validate = $request->validate_cancel($id);
            if ($validate === true) 
            {
                 return $this->OS->view_cancel($id);
            }
            else
            {
                return $validate;
            }
            
        }
       
    }
    

    public function edit_action($id)
    {   
        if (Input::get('action') == "send") 
        {
            $request = new OS\Requests\Store;
            $validate = $request->validate($id);
            if ($validate === true) 
            {
                 return $this->OS->edit_sent($id);
            }
            else
            {
                return $validate;
            }       
        }
        elseif(Input::get('action') == "save")
        {
            return $this->OS->edit_save($id);
        }
       
    }

     public function delete_record($id)
    {
        return $this->OS->deleterecord($id);
    }

    public function view_affiliate($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::employee_record($id);

        $data["uom"] =  OfficeSales::uom();
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;  
        $data["so_code"] =  OfficeSales::so_code(2);
        $data["attachments"] =  OfficeSales::attachments($id);
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("os.view_affiliate", $data);        
        }        
    }

    public function process($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::employee_record($id);

        $data["uom"] =  OfficeSales::uom();
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;  
        $data["so_code"] =  OfficeSales::so_code(2);
        $data["attachments"] =  OfficeSales::attachments($id);
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("os.process", $data);        
        }        
    }

    public function process_action($id)
    {   
        if (Input::get('action') == "close") 
        {
            $request = new OS\Requests\Store;
            $validate = $request->validate_process($id);
            if ($validate === true) 
            {
                 return $this->OS->process_close($id);
            }
            else
            {
                return $validate;
            }       
        }
        elseif(Input::get('action') == "save")
        {
            return $this->OS->process_save($id);
        }
       
    }

    public function view_employees($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::employee_record($id);

        $data["uom"] =  OfficeSales::uom();
        $data["readonly"] = $this->Readonly;
        $data["id"] = $id;  
        $data["so_code"] =  OfficeSales::so_code(1);
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("os.view_employee", $data);        
        }        
    }


	public function create_food()
	{
        if(!Session::get("is_office_receiver")) return Redirect::to('/');

        $data["company"] =  OfficeSales::company();
        $data["uom"] =  OfficeSales::uom();
        $data["base_url"] = URL::to("/");		
        return View::make("os.food.create", $data);    
    }    


    public function create_food_action()
    {   
        if(!Session::get("is_office_receiver")) return Redirect::to('/');

        if (Input::get('action') == "save") 
        {
           $request = new OS\Requests\Store;
            $validate = $request->validate_food();
            if ($validate === true) 
            {
                 return $this->OS->create_food_save();
            }
            else
            {
                return $validate;
            }     
        }  
    }

    public function create_non_food()
    {
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["uom"] =  OfficeSales::uom();
        $data["base_url"] = URL::to("/");       
        return View::make("os.non-food.create", $data);    
    }    


    public function create_non_food_action()
    {   
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        if (Input::get('action') == "save") 
        {
           $request = new OS\Requests\Store;
            $validate = $request->validate_non_food();
            if ($validate === true) 
            {
                 return $this->OS->create_non_food_save();
            }
            else
            {
                return $validate;
            }     
        }  
    }


    public function edit_food($id)
    {
        if(!Session::get("is_office_receiver")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::food_record($id);
        $data["company"] =  OfficeSales::company();
        $data["uom"] =  OfficeSales::uom();
        $data["id"] = $id;  

        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("os.food.edit", $data);
        }        
    }
    

    public function edit_food_action($id)
    {   
        if(!Session::get("is_office_receiver")) return Redirect::to('/');

        if(Input::get('action') == "save")
        {
            $request = new OS\Requests\Store;
            $validate = $request->validate_food($id);
            if ($validate === true) 
            {
              return $this->OS->edit_food_save($id);  
            }
            else
            {
                return $validate;
            }                
        }
        else if(Input::get('action') == "delete")
        {
            return $this->OS->edit_delete($id);
        }
           
    }

    public function edit_non_food($id)
    {
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  OfficeSales::non_food_record($id);
        $data["uom"] =  OfficeSales::uom();
        $data["id"] = $id;  

        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("os.non-food.edit", $data);
        }        
    }
    

    public function edit_non_food_action($id)
    {   
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        if(Input::get('action') == "save")
        {
            $request = new OS\Requests\Store;
            $validate = $request->validate_non_food($id);
            if ($validate === true) 
            {
              return $this->OS->edit_non_food_save($id);  
            }
            else
            {
                return $validate;
            }                
        }
        else if(Input::get('action') == "delete")
        {
            return $this->OS->edit_delete($id);
        }
           
    }

    public function report()
    {
        

        $data["base_url"] = URL::to("/");       
        
        if(Session::get("is_office_receiver") || Session::get("is_office_receiver_non_food"))
        {
            $data["department"] =  Arts::department();
            $comp = OfficeSales::company_list();
            foreach($comp as $key=>$val)
            {
                if($val->affiliate == 0) $arr[$val->comp_code] = $val->comp_code;
            }
            $data["company_arr"] = $arr;

            foreach($comp as $key=>$val)
            {
                if($val->affiliate == 1) $arr2[$val->comp_code] = $val->comp_code;
            }
            $data["company_arr_aff"] = $arr2;
            $data["record_employee"] =  $this->OS->get_employee_record("O");
            if(Session::get("is_office_receiver") != "BUK" && Session::get("is_office_receiver_non_food") != "BUK") $data["record_affiliate"] =  $this->OS->get_tobe_affiliate_record();
        }

        $data["record"] =  $this->OS->get_record();
        return View::make("os.report", $data);    
    }

    public function receiver_report()
    {
        if(!Session::get("is_office_receiver") && !Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["department"] =  Arts::department();
        $comp = OfficeSales::company_list();
        foreach($comp as $key=>$val)
            {
                if($val->affiliate == 0) $arr[$val->comp_code] = $val->comp_code;
            }
            $data["company_arr"] = $arr;

            foreach($comp as $key=>$val)
            {
                if($val->affiliate == 1) $arr2[$val->comp_code] = $val->comp_code;
            }

        $data["company_arr_aff"] = $arr2;
        $data["record_employee"] =  $this->OS->get_employee_record();
        if(Session::get("is_office_receiver") != "BUK"  && Session::get("is_office_receiver_non_food") != "BUK"  ) $data["record_affiliate"] =  $this->OS->get_affiliate_record();
        return View::make("os.receiver", $data);    
    }


     public function food_report()
    {
        if(!Session::get("is_office_receiver")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->OS->get_food_record();
        return View::make("os.food.report", $data);    
    }

     public function non_food_report()
    {
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->OS->get_non_food_record();
        return View::make("os.non-food.report", $data);    
    }

    public function process_print()
    {
        if(!Session::get("is_office_receiver") && !Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        if(Input::get('action') == "GENERATE")
        {
             return $this->OS->affiliate_print();
        }
    }  


    public function export_employees_food()
    {
        if(!Session::get("is_office_receiver") ) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        return View::make("os.export.employee_food", $data);    
    }  

    public function export_employees_non_food()
    {
        if( !Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["project"] =  OfficeSales::project();
        $data["base_url"] = URL::to("/");       
        return View::make("os.export.employee_non_food", $data);    
    }  

    public function export_affiliate()
    {
        if(!Session::get("is_office_receiver") && !Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        return View::make("os.export.affiliate", $data);    
    }  

    public function export_employees_food_action()
    {   
        if(!Session::get("is_office_receiver") ) return Redirect::to('/');

        if(Input::get('action') == "export")
        {
             return $this->OS->employee_food();
        }
    }

    public function export_employees_non_food_action()
    {   
        if(!Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        if(Input::get('action') == "export")
        {
             return $this->OS->employee_non_food();
        }
    }

    public function export_affiliate_action()
    {   
        if(!Session::get("is_office_receiver") && !Session::get("is_office_receiver_non_food")) return Redirect::to('/');

        if(Input::get('action') == "export")
        {
             return $this->OS->affiliate_food();
        }
    }

    public function get_cutoff_non()
    {   
        $product = Input::get("product");
        $amount = Input::get("amount");
        $pid = Input::get("pid");

           $rec = DB::table("officesale_project_items")->whereIn("officesale_project_items.code",$product)
            ->join("officesale_projects","officesale_projects.project_id","=","officesale_project_items.project_id")
            ->get(["officesale_project_items.project_id","officesale_project_items.code","limit","name"]);

            foreach($rec as $rs)
            {
                foreach($product as $index=>$prod)
                {
                    if($rs->code == $prod)
                    {
                        $proj[ $rs->project_id ]["limit"] = $rs->limit;
                        $proj[ $rs->project_id ]["amount"] = isset($proj[ $rs->project_id ]["amount"]) ? $proj[ $rs->project_id ]["amount"] + $amount[$index] :  $amount[$index];

                        if($proj[ $rs->project_id ]["limit"] < $proj[ $rs->project_id ]["amount"])   
                        {
                            $data["result"] = 0;
                            $data["msg"] = "Transaction Exceeds P " . $proj[ $rs->project_id ]["limit"] . " on project " . $rs->name . "! If you want to proceed with the ordering, please Pay in Cash" ;
                            echo json_encode($data);
                            exit();
                        }                  
                    }
                    
                }
                
            }


             $rec = OfficeSales::where("so_code","CH-O")
             ->where("sales_type","NON-FOOD")
             ->join("orderdetails","orderdetails.orderentryid","=","orderentries.id")
             ->join("officesale_project_items","officesale_project_items.code","=","orderdetails.prod_code")
             ->join("officesale_projects","officesale_project_items.project_id","=","officesale_projects.project_id")
            ->groupby("officesale_project_items.project_id")
            ->where("ownerid" , Session::get("employee_id"))
            ->where("orderentries.id" , "!=" , $pid )
            ->selectRaw("name , officesale_projects.limit , sum(order_qnty * orderdetails.price) as total , officesale_projects.project_id  ")
            ->CutoffPeriod()
            ->get();

            foreach($rec as $rs)
            {
                if($rs->limit < ($rs->total + $proj[ $rs->project_id ]["amount"] ))
                {
                    $data["result"] = 0;
                    $data["msg"] = "Transaction Exceeds P " . $proj[ $rs->project_id ]["limit"] . " on project " . $rs->name . "! If you want to proceed with the ordering, please Pay in Cash" ;
                    echo json_encode($data);
                    exit(); 
                }
            }
         


        $data["result"] = 1;
        echo json_encode($data);
    }


    public function get_cutoff($amount)
    {   
       // if(!Session::get("is_office_receiver")) return Redirect::to('/');

        $rec = OfficeSales::where("so_code","CH-O")
        ->where("ownerid" , Session::get("employee_id"))
        ->where("sales_type" , "FOOD")
        ->groupby("ownerid")
        ->selectRaw("sum(total_amount) as total_amount")
        ->CutoffPeriod()
        ->get()
        ->toArray();
         
        $data["amount"] = (isset($rec[0]["total_amount"]) ? $rec[0]["total_amount"] : 0) + $amount; 
        echo json_encode($data);
    }


    public function get_outstanding()
    {
        $rec = OfficeSales::where("so_code","CH-A")
        ->whereRaw("(officesale_payments.status is null or officesale_payments.status != 1)")
        ->whereRaw("orderentries.total_amount != paid_amount")
        ->whereRaw("delivery_date  < date_format(current_date - INTERVAL 30 DAY,'%Y-%m-%d')")
        ->leftJoin("officesale_payments","orderentryid","=","orderentries.id")
        ->selectRaw("sum(orderentries.total_amount -  paid_amount) as total")
        ->where("ownerid" , Session::get("employee_id"))
        ->groupby("ownerid")
        ->get()
        ->toArray();

        $data["amount"] = (isset($rec[0]["total"]) ? $rec[0]["total"] : 0) ; 
        echo json_encode($data);
    }

}