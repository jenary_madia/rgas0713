<?php

class HomeController extends BaseController {
    
    
	public function index()
	{
	
		if(!Session::has('logged_in')){
			return Redirect::to('login');
			die;
		}

		$data["homepage"] = "homepage";
		$data["base_url"] = URL::to("/");
        /* NTE */

        $data['form_action_nte_view'] = route('nte.view');
        $data['form_id_nte_view'] = 'nte_view';

        $data['form_action_nte_request'] = route('nte.request');
        $data['form_id_nte_request'] = 'nte_request';

        $data['form_action_returned'] = route('nte.returned');
        $data['form_id_returned'] = 'nte_returned';

        $data['form_action_approval'] = route('nte.approval');
        $data['form_id_approval'] = 'nte_approval';

        $data['form_action_nte_employee'] = route('nte.employee');
        $data['form_id_nte_employee'] = 'nte_employee';

        $data['form_action_nte_department'] = route('nte.department');
        $data['form_id_nte_department'] = 'nte_department';

        $data['form_action_nda_view'] = route('nda.view');
        $data['form_id_nda_view'] = 'nda_view';

        $data['form_action_nda_request'] = route('nda.request');
        $data['form_id_nda_request'] = 'nda_request';

        $data['form_action_acknowledged'] = route('nda.acknowledged');
        $data['form_id_acknowledged'] = 'nda_acknowledged';

        $data['form_action_manager'] = route('nda.manager');
        $data['form_id_manager'] = 'nda_manager';

        $data['form_action_head'] = route('nda.head');
        $data['form_id_head'] = 'nda_head';

        $data['form_action_nda_department'] = route('nda.department');
        $data['form_id_nda_department'] = 'nda_department';

        $data['form_action_nda_employee'] = route('nda.employee');
        $data['form_id_nda_employee'] = 'nda_employee';

        /* END NTE */
		return View::make("home", $data);
	}
    
    
    //For pop up only
    public function notice_page()
    {
        $data["base_url"] = URL::to("/");
        return View::make("notice", $data);    
    }
    
    // Update Hierarchy table
	public function update_hierarchy()
    {
        $department_lists = Departments::get_departments();
        
        if (!empty ($department_lists))
        {
            DB::table('hierarchies')->truncate();
        
            foreach ($department_lists as $depts)
            {
                $rs_emps = Employees::get_all($depts->id);
                
                if (!empty($rs_emps))
                {
                    foreach ($rs_emps as $emps)
                    {
                        $h = new Hierarchies;
                        $h->employee_id = $emps->id;
                        $h->superior_id = $emps->superiorid;
                        $h->department_id = $emps->departmentid;
                        $h->department_hrms_cd = $emps->departmentid2;
                        $h->name = $this->mb_convert($emps->name);
                        $h->superior = $this->mb_convert($emps->superior);
                        $h->designation = $this->mb_convert($emps->desig_level);
                        $h->save();
                    }
                }
                
                unset ($rs_emps);
            }
        }
    }
    
    function mb_convert($string, $mode = MB_CASE_TITLE)
    {
        $string = mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
        $string = mb_convert_case($string, $mode, 'UTF-8');

        return trim($string);
    }

}
