<?php

use RGAS\Modules\ART;
use RGAS\Libraries;


class ARTController extends \BaseController 
{

    protected $module_id;
    
	public function __construct()
	{		
		$this->ART = new ART\ART;	
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
	}


    public function download($ref_num, $random_filename, $original_filename)
    {       
        $fm = new Libraries\FileManager;

        if(glob(Config::get('rgas.rgas_storage_path') . "art/" . "$ref_num/" . $random_filename))
        {
            $filepath = Config::get('rgas.rgas_storage_path') . "art/" . "$ref_num/" . $random_filename;        
        }
        else if(glob(Config::get('rgas.rgas_temp_storage_path') . $random_filename))
        {
             $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;       
        }      
        else
        {
            $filepath =  (Config::get('rgas.rgas_storage_path') . "art/" . CIEncrypt::decode($original_filename));
        }

        return $fm->download(trim($filepath),  trim(CIEncrypt::decode($original_filename)));
    }

        
    


	public function create()
	{
        $data["base_url"] = URL::to("/");		
        return View::make("art.create", $data);    
    }    


     


    public function create_action()
    {   
        if (Input::get('action') == "send") 
        {
            $request = new ART\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) 
            {
                return $this->ART->create_sent();
            }
            else
            {
                return $validate;
            }   
        }
        elseif(Input::get('action') == "save")
        {
            return $this->ART->create_save();
        }            
    }

    

    public function edit($id)
	{
        $data["base_url"] = URL::to("/");		
        $data["record"] =  Arts::filer_record($id);
        $data["id"] = $id;	
        if(!is_array( $data["record"]))
        {
        	return  $data["record"];
        }
        else
        {
        	return View::make("art.edit", $data);
        }        
    }



    

    public function edit_action($id)
    {   
        if (Input::get('action') == "send") 
        {
            $request = new ART\Requests\Store;
            $validate = $request->validate($id);
            if ($validate === true) 
            {
                return $this->ART->edit_sent($id);
            }
            else
            {
                return $validate;
            }  
        }
        elseif(Input::get('action') == "save")
        {
            return $this->ART->edit_save($id);
        }             
    }


     

    public function view($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  Arts::filer_view_record($id);
        $data["revision"] =  Arts::revision($id);
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("art.view", $data);
        }        
    }

    

    public function view_action($id)
    {   
        $request = new ART\Requests\Store;
        $validate = $request->validate_view($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "cancel") 
            {
                return $this->ART->view_cancel($id);
            }
            else if (Input::get('action') == "return") 
            {
                return $this->ART->view_return($id);
            }
            else if (Input::get('action') == "conforme") 
            {
                return $this->ART->view_conforme($id);
            }
        }
        else
        {
            return $validate;
        }       
    }

    

    public function approval($id)
    {
        if(Session::get("desig_level") != "head") return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["record"] =  Arts::head_record($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("art.approval", $data);
        }        
    }

    

    public function approval_action($id)
    {         
        if(Session::get("desig_level") != "head") return Redirect::to('/');

        if (Input::get('action') == "send") 
        {
            return $this->ART->approval_send($id);
        }
        else if (Input::get('action') == "return") 
        {
            $request = new ART\Requests\Store;
            $validate = $request->validate_approval($id);
            if ($validate === true) 
            {
                 return $this->ART->approval_return($id);           
            }
            else
            {
                return $validate;
            }      
        } 
    }

     

    public function assign($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  Arts::art_head_record($id);
        $data["artist"] =  Arts::artist();
        $data["revision"] =  Arts::revision($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
             if(!$this->Readonly)
            {

                $arthead = Receivers::where("code","CRT_ART_RECEIVER")
                    ->where("value",1)
                    ->first(["receivers.employeeid"]);

                $headid = $arthead["employeeid"];

                if($data["record"]["curr_emp"] != $headid || $data["record"]["assignto"] )
                {
                    return Redirect::to("art/assign/$id?page=view");
                }
            }

            return View::make("art.reviewform", $data);
        }        
    }

    

    public function assign_action($id)
    {         
        $request = new ART\Requests\Store;
        $validate = $request->assign_validate($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "assign") 
            {
                return $this->ART->assig_assign($id);
            }
            elseif(Input::get('action') == "return")
            {
                return $this->ART->assig_return($id);
            }
        }
        else
        {
            return $validate;
        }  
    }

    

    public function artist($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  Arts::art_artist_record($id);
        $data["revision"] =  Arts::revision($id);
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("art.artist", $data);
        }        
    }

    

    public function artist_action($id)
    {         
        $request = new ART\Requests\Store;
        $validate = $request->conforme_validate($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "send") 
            {
                return $this->ART->artist_send($id);
            }
        }
        else
        {
            return $validate;
        }  
    }

     public function delete_record($id)
    {
        return $this->ART->deleterecord($id);
    }

     

    public function filer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->ART->get_record();
        $data["record_approval"] =  $this->ART->get_review_record();
        return View::make("art.filer", $data);    
    }

     public function art_department()
    {
        if(Session::get("dept_id") != 17) return Redirect::to('/');

        $data["base_url"] = URL::to("/");       
        $data["department"] =  Arts::department();
        $data["record"] =  $this->ART->get_art_record();
        $data["record_holder"] =  $this->ART->get_art_record_holder();
        return View::make("art.reviewer", $data);    
    }

     public function review_get()
    {
        return $this->ART->get_reviewer();
    }
        
      public function my_review()
    {
        return $this->ART->get_my_review();
    }
}