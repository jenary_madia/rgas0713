<?php 

use RGAS\Modules\TE;
use RGAS\Libraries;
use RGAS\Repositories\TERepository;

class TrainingEndorsementController extends \BaseController {

	private $TE;
	
	public function __construct()
	{
		$this->TE = new TE\TrainingEndorsementsForm;
	}

    public function create()
    {		
		$data['departments'] = Departments::get_departments();
		$data['employees'] = Employees::get_employees();
		$data["base_url"] = URL::to("/");
		
		return View::make("te/create", $data);
    }
	
	public function action()
	{			
		$data["base_url"] = URL::to("/");
		$data['departments'] = Departments::get_departments();
		$data['employees'] = Employees::get_employees();
		
		$input = Input::all();		
		$increment = false;
		
		$te = new TrainingEndorsements;
		$tep = new TrainingEndorsementsParticipants;
		if($input['action'] == 'save')
		{
			//$request = new TE\Requests\Store;
			//$request->validate();
			
			//$is_valid = $request->validate();
			//if($is_valid === true){
				$increment = true;
				ReferenceNumbers::increment_reference_number('te');
				$this->TE->storeEndorsement($input);
				return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully saved and can be edited later.');
		//	}
		//	else {
		//		return $is_valid;
		//	}
		} 
		if($input['action'] == 'send')
		{
			$request = new TE\Requests\Store;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$increment = true;
				ReferenceNumbers::increment_reference_number('te');
				$this->TE->storeEndorsement($input);
				return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully sent for assessment');
			}
			else {
				return $is_valid;
			}		
		}
		
		return true;
        return View::make("te/create", $data);   
	}
	
	public function view($te_id)
	{
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;
		
		$this->TE->storeViewLogsTE($te_id);
		return View::make("te/view_submitted_endorsement", $data);
	}

	public function edit($te_id)
	{	
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		$data['departments'] = Departments::get_departments();
		$data['employees'] = Employees::get_employees();
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;
		$data["base_url"] = URL::to("/");

		$input = Input::all();
		if(Request::isMethod('post')){
			if($input['action'] == 'save' )
			{
				//$request = new TE\Requests\Store;
				//$request->validate();
				
				//$is_valid = $request->validate();
				//if($is_valid === true){
					$this->TE->editEndorsement($te_id);
					return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully saved and can be edited later.');
				//}
				//else {
			//		return $is_valid;
			//	}		
			}
			
			if($input['action'] == 'send')
			{
				$request = new TE\Requests\Store;
				$request->validate();
				
				$is_valid = $request->validate();
				if($is_valid === true){
					$this->TE->editEndorsement($te_id);
					return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully sent for assessment');
				}
				else {
					return $is_valid;
				}	
			}
		}
		return View::make("te/edit", $data);
	}
	
	// public function viewChrd($te_id)
	// {
		// $te = new TERepository;
		// $request = $te->getRequest($te_id);
		// $vpforchr = $te->getVPforCHR();
		// $tepRecord = $te->getParticipants($te_id);
		// $data["request"] = $request;
		// $data["vpforchr"] = $vpforchr;
		// $data["tepRecord"] = $tepRecord;
		// $data["base_url"] = URL::to("/");
		
		// return View::make("te/view_assessment", $data);
	// }
	
	public function assessChrd($te_id)
	{		
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$vpforchr = $te->getVPforCHR();
		$tepRecord = $te->getParticipants($te_id);
		$data["request"] = $request;
		$data["vpforchr"] = $vpforchr;
		$data["tepRecord"] = $tepRecord;
		$data["base_url"] = URL::to("/");
	
		$input = Input::all();
		
		if(Request::isMethod('post')){
			$input = Input::all();
			if(Input::get('action') == 'send')
			{
				$request = new TE\Requests\Assess;
				$request->validate();
		
				$is_valid = $request->validate();
				if($is_valid === true){
					$this->TE->assess($te_id);
					return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully sent to VP for approval');
				}
				else {
					return $is_valid;
				}
			}
			if(Input::get('action') == 'save' ){			
				$this->TE->assess($te_id);
				return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully saved and can be edited later.');
			}
			if(Input::get('action') == 'disapprove' )
			{	
				$request = new TE\Requests\ValidateComments;
				$request->validate();
				
				$is_valid = $request->validate();
				if($is_valid === true){
					$this->TE->assess($te_id);
					return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully disapproved.');
				}
				else {
					return $is_valid;
				}
			}
		}
        return View::make("te/assess_te", $data);
	}
	
	public function viewForApproval($te_id)
	{
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;
		
		return View::make("te/view_for_approval", $data);
	}
	
	public function processForApproval($te_id)
	{
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		$pres = $te->getPres();
		$svpforcs = $te->getSVPforCS();
		$vpforchr = $te->getVPforCHR();
		
		$te = TrainingEndorsements::find($te_id);
		if(Request::isMethod('post')){
			if(Input::get('action') == 'approve')
			{
				$this->TE->workflow($te_id);
				return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully approved');
			}
		
			else if(Input::get('action') == 'disapprove')
			{	
				$validated_request = new TE\Requests\ValidateComments;
				$validated_request->validate();
				
				$is_valid = $validated_request->validate();
				if($is_valid === true){
					$this->TE->workflow($te_id);
					return Redirect::to('te/submitted-te-requests/')->with('message', 'Request successfully disapproved.');
				}
				else {
					return $is_valid;
				}
			}
		}
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;
		
		return View::make("te/process_for_approval", $data);
	}
	
	public function view_approved($te_id)
	{	
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;

		$this->TE->storeViewLogsTE($te_id);
		return View::make("te/view_approved", $data);
	}
	
	public function delete($te_id)
	{
		$affectedRows = $this->TE->deleteRequest($te_id);
		return Redirect::to('te/submitted-te-requests/')->with('message', ' Request has been deleted');

	}
	
	public function printRequest($te_id)
	{
	
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$tepRecord = $te->getParticipants($te_id);
		
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["tepRecord"] = $tepRecord;
		return View::make("te/print_request", $data);
	}
	
	public function download($te_ref_num, $random_filename, $original_filename)
	{		
		$fm = new Libraries\FileManager;
		$filepath = Config::get('rgas.rgas_storage_path') . $this->TE->attachments_path . "$te_ref_num/" . $random_filename;
		if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
			
			$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
		}
			
		$this->TE->downloadLog($te_ref_num);
		return $fm->download($filepath, CIEncrypt::decode($original_filename));
	}
	
	public function printFile($te_id){
		$this->TE->printLog($te_id);
		return Redirect::to('te/submitted-te-requests/');
	}
	
	public function SubmittedTERequests()
	{
		$data['departments'] = Departments::get_departments();
		$data["base_url"] = URL::to("/");
		return View::make("te/submitted_te_requests", $data);
	}
	
	public function EndorsementsNew()
	{
		$te = new TERepository;
		$requests = $te->getEndorsementsForAssessment();
		
		return json_encode($requests);
	}
	
	public function EndorsementsMy()
	{
		$te = new TERepository;
		$requests = $te->getEndorsementsMy();
		
		return json_encode($requests);
	}
	
	
	public function EndorsementsChrdVpApproval()
	{
		$te = new TERepository;
		$requests = $te->getEndorsementsChrdVpApproval();
		
		return json_encode($requests);
	}
	
	public function EndorsementsSvpCsApproval()
	{
		$te = new TERepository;		
		$requests = $te->getEndorsementsSvpCsApproval();
		
		return json_encode($requests);
	}	
	
	public function EndorsementsPresApproval()
	{
		$te = new TERepository;		
		$requests = $te->getEndorsementsPresApproval();
		
		return json_encode($requests);
	}


	public function FilerDashboard()
	{
		$te = new TERepository;
		$requests = $te->getFilerDashboard();
		
		return json_encode($requests);
	}

	public function ApproverDashboard()
	{
		$te = new TERepository;
		$requests = $te->getApproverDashboard();
		
		return json_encode($requests);
	}

}