<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/20/2016
 * Time: 11:08 AM
 */
use RGAS\Modules\MOC;
use RGAS\Libraries;

class MOCController extends \BaseController
{
    protected $MOCvalidator;
    protected $MOC;

    public function __construct() {
        $this->MOCvalidator = new MOC\MOCValidator();
        $this->MOC = new MOC\MOC();
    }

    public function index() {
//        $SSMDs = DB::table('employees')
//            ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
//            ->where("sections.sect_name","SATELLITE SYSTEMS")
//            ->select('employees.id as empID')
//            ->get();
//        $SSMDids = [];
//        foreach($SSMDs as $SSMD) {
//            array_push($SSMDids,$SSMD->empID);
//        }
//
//        return $SSMDids;
//        return $this->MOC->getDeptHead(Session::get("superiorid"));
        $asd = selectItems::where('module','moc')
            ->where('modulesfield','request_type')
            ->whereIn('item',["docu_control", "process_doc", "process_design", "review_change"])
            ->get();
        $data['jobTitle'] = "";
        foreach ($asd as $key) $data['jobTitle'][$key->item] = $key->text;


        $data["base_url"] = URL::to("/");
        return View::make("moc.index", $data);
    }

    public function create() {
        $data["base_url"] = URL::to("/");
        $data['requestTypes'] = (new SelectItems)->getList('moc','request_type');
        $data['changeIn'] = (new SelectItems)->getList('moc','change_in');
        return View::make("moc.create",$data);
    }

    public function myMOC($method,$id) {
        $data["base_url"] = URL::to("/");
        $data["mocID"] = $id;
        $data['changeIn'] = (new SelectItems)->getList('moc','change_in');
        $data['requestTypes'] = (new SelectItems)->getList('moc','request_type');
        $data['lastSignature'] = MOCSignatories::where('moc_request_id',$id)->max('signature_type');
        $data['mocDetails'] = MOCRequests::where('request_id',$id)->first();
        $mocStatus = $data['mocDetails']->status;
        if ($method == 'view') {
            return View::make("moc.my-moc-view", $data);
        }elseif($method == "edit") {
            if(in_array($mocStatus,json_decode(MOC_EDITABLE))) {
                return View::make("moc.my-moc-edit", $data);
            }else{
                return Redirect::to('/moc')
                    ->with('errorMessage', 'This MOC form is not editable');
            }
        }
    }

    public function myMOCProcess() {
        $data = explode('|',Input::get("action"));
        if(count($data) < 2) {
            return Redirect::to("moc")
                ->with("errorMessage","Invalid action");
        }else{
            $action = $data[0];
            $id = $data[1];
        }
        if($action == 'softDelete') {
            return $this->MOC->deleteMyMOC($id);
        }
    }

    /***********For endorsement*************/
    public function forEndorsement($method,$id) {
        $data["base_url"] = URL::to("/");
        $data["mocID"] = $id;
        $data["method"] = $method;
        $data['changeIn'] = (new SelectItems)->getList('moc','change_in');
        $data['requestTypes'] = (new SelectItems)->getList('moc','request_type');

        $data['mocDetails'] = MOCRequests::where('request_id',$id)->first();

        if(count($data['mocDetails']) >= 1) {
            return View::make("moc.for-endorsement", $data);
        }else{
            return Redirect::to('/moc')
                ->with('errorMessage', 'Invalid MOC');
        }
    }

    /***********For approval MOC*************/

    public function forApprove($method,$id) {
        $data["base_url"] = URL::to("/");
        $data["mocID"] = $id;
        $data["method"] = $method;
        $data['changeIn'] = (new SelectItems)->getList('moc','change_in');
        $data['requestTypes'] = (new SelectItems)->getList('moc','request_type');
        $data['mocDetails'] = MOCRequests::where('request_id',$id)
            ->with('owner')
            ->with('signatories');
        if(count($data['mocDetails']) >= 1) {
            if($method != "view") {
                $data['mocDetails'] = $data['mocDetails']->where('curr_emp',Session::get("employee_id"));
            }
            $data['mocDetails'] = $data['mocDetails']->first();
            $data['lastSignature'] = MOCSignatories::where('moc_request_id',$id)->max('signature_type');
            $mocSignatories =  MOCSignatories::where('moc_request_id',$id);
            if($data['lastSignature'] == 3) {
                if($data['mocDetails']['company'] != 'RBC-CORP') {
                    $data['toReturn'] = $mocSignatories->orderBy('id','desc')
                        ->with('employee')
                        ->limit(2)
                        ->get();
                    for ($x = 0; $x < count($data['toReturn']); $x++) {
                        if($data['toReturn'][$x]["employee"]["id"] == $data['mocDetails']['requested_by']) {
                            goto end;
                        }
                    } 
                    $data['toReturn'][2] = [
                        "employee" => 
                            [   
                                "id" => $data['mocDetails']['requested_by'],
                                "firstname" => $data['mocDetails']['firstname'],
                                "lastname" => $data['mocDetails']['lastname'],
                                "middlename" => $data['mocDetails']['middlename']
                            ]
                    ];
                    end:
                }   
            }
            elseif($data['lastSignature'] == 4 || (is_null($data['lastSignature']) && $data['mocDetails']['assessment_type'] == 2)) {
                $data['bsa'] = DB::table('receivers')
                    ->join('employees','employees.id','=','receivers.employeeid')
                    ->where('receivers.code' , CSMD_BSA)
                    ->select((DB::raw("receivers.employeeid,concat(employees.firstname,' ',employees.middlename,'. ',employees.lastname) as fullname")))
                    ->lists('fullname','employeeid');
            }elseif($data['lastSignature'] == 5) {
                $data['lastTwoSigs'] = DB::table('moc_signatories')
                    ->join('employees','employees.id','=','moc_signatories.employee_id')
                    ->where('moc_signatories.employee_id' , "!=", Session::get("employee_id"))
                    ->where([
                        'moc_signatories.moc_request_id' => $id,
                        'employees.departmentid' => CSMD_DEPT_ID,
                    ])
                    ->select((DB::raw("employees.id as employeeid,concat(employees.firstname,' ',employees.middlename,'. ',employees.lastname) as fullname")))
                    ->lists('fullname','employeeid');
                $data['signatureCount'] = $mocSignatories->where('signature_type',5)
                    ->count();
            }elseif($data['lastSignature'] == 6) {
                $data['lastTwoSigs'] = DB::table('moc_signatories')
                    ->join('employees','employees.id','=','moc_signatories.employee_id')
                    ->where('moc_signatories.employee_id' , "!=", Session::get("employee_id"))
                    ->where([
                        'moc_signatories.moc_request_id' => $id,
                        'employees.departmentid' => CSMD_DEPT_ID,
                    ])
                    ->select((DB::raw("employees.id as employeeid,concat(employees.firstname,' ',employees.middlename,'. ',employees.lastname) as fullname")))
                    ->limit(3)
                    ->orderBy('moc_signatories.id','desc')
                    ->lists('fullname','employeeid');
            }

        
            return View::make("moc.for-approval", $data);
        }else{
            return Redirect::to('/moc')
                ->with('errorMessage', 'Invalid MOC');
        }
    }

    /**********Submitted MOC**************/
    public function submittedMOC() {
        $data["base_url"] = URL::to("/");
        $companies[0] = 'ALL';
        $SSMDs = DB::table('employees')
            ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
            ->where("sections.sect_name","SATELLITE SYSTEMS")
            ->select('employees.id as empID')
            ->get();
        $SSMDids = [];
        foreach($SSMDs as $SSMD) {
            array_push($SSMDids,$SSMD->empID);
        }
//        $companies +=
            $result = DB::table('moc_requests')
            ->join('companylists', 'moc_requests.company', '=', 'companylists.comp_code')
            ->leftjoin("moc_signatories",'moc_requests.request_id','=','moc_signatories.moc_request_id')
            ->select(['companylists.comp_name as comp_name','moc_requests.company as comp_code']);

        if (Session::get("company") != 'RBC-CORP') {
            $result->where('moc_requests.company', Session::get("company"));
            $result->whereIn('moc_requests.status', ["FOR ASSESSMENT","FOR ACKNOWLEDGEMENT","ACKNOWLEDGED","FOR APPROVAL"]);
        }else{
            $result->whereNotIn('moc_requests.curr_emp',$SSMDids);
            $result->whereIn('moc_requests.status', ["FOR ASSESSMENT"])
                ->orwhere(['moc_requests.status' => 'FOR APPROVAL','moc_signatories.signature_type' => 6])
                ->orwhere(['moc_requests.status' => "FOR ACKNOWLEDGEMENT",'moc_signatories.signature_type' => 7])
                ->orwhere(['moc_requests.status' => "ACKNOWLEDGED",'moc_signatories.signature_type' => 8]);

        }
         $companies += $result->lists('comp_name','comp_code');
        $data['companies'] = $companies;
        return View::make("moc.submitted_moc", $data);
    }

    /***********For List*************/

    public function getMyMoc($limit = null) {
        if(Request::ajax()) {
            $result = MOCRequests::where('requested_by', Session::get('employee_id'))
                ->with('current_emp')
                ->where('is_deleted','!=',1)
                ->orderBy('request_id','DESC');
                if($limit) {
                    $result->whereIn("status",["FOR ENDORSEMENT","FOR ASSESSMENT","FOR APPROVAL","FOR ACKNOWLEDGEMENT","DISAPPROVED"]);
                    $result = $result->limit($limit);
                }else{
                    $result ->where('status','NOT LIKE',"%DELETED%");
                }
                $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $deletableNotifications = (in_array($req['status'], json_decode(MOC_DELETABLE,true)));
                    $editableNotifications = (in_array($req['status'], json_decode(MOC_EDITABLE,true)));
                    $btnDelete = "<button  ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' name='action' value='softDelete|{$req->request_id}'>Delete</button>";
                    $btnView = "<a class='btn btn-default btndefault' href=".url('/moc/view/my-moc/'.$req->request_id).">View</a>";
                    $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' href=".url('/moc/edit/my-moc/'.$req->request_id).">Edit</a>";
                    $result_data["aaData"][$ctr][] = $req['transaction_code'];
                    $result_data["aaData"][$ctr][] = $req['date'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['current_emp']['firstname'].' '.$req['current_emp']['lastname'];
                    $result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getWaitingForApproval() {
        if(Request::ajax()) {
            $result = MOCRequests::where('curr_emp', Session::get('employee_id'))
                ->whereIn('status',["FOR ENDORSEMENT","FOR ASSESSMENT","FOR APPROVAL"])
                ->orderBy('request_id','DESC')
                ->limit(5);
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['transaction_code'];
                    $result_data["aaData"][$ctr][] = $req['date'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['firstname'].' '.$req['lastname'];
                    if($req['status'] == "FOR ENDORSEMENT") {
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-endorsement/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/endorse/for-endorsement/'.$req['request_id']).">endorse</a>";
                    }elseif($req['status'] == "FOR ASSESSMENT"){
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">assess</a>";
                    }else{
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">approve</a>";
                    }
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getForEndorsementMoc($limit = null) {
        if(Request::ajax()) {
            $result = MOCRequests::where('curr_emp', Session::get('employee_id'))
                ->where('status',"FOR ENDORSEMENT")
                ->orderBy('request_id','DESC');
            if($limit) {
                $result->limit($limit);
            }
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['transaction_code'];
                    $result_data["aaData"][$ctr][] = $req['date'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['firstname'].' '.$req['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-endorsement/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/endorse/for-endorsement/'.$req['request_id']).">endorse</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getForAssessmentMoc() {
        if(Request::ajax()) {
            $SSMDs = DB::table('employees')
                ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
                ->where("sections.sect_name","SATELLITE SYSTEMS")
                ->select('employees.id as empID')
                ->get();
            $SSMDids = [];
            foreach($SSMDs as $SSMD) {
                array_push($SSMDids,$SSMD->empID);
            }
            $result = DB::table('moc_requests')
                ->leftjoin("moc_signatories",'moc_requests.request_id','=','moc_signatories.moc_request_id')
                ->leftjoin('employees','moc_requests.curr_emp','=','employees.id')
                ->selectRaw("DISTINCT(moc_requests.request_id), moc_requests.*,employees.id 
                AS curr_emp_id, employees.firstname 
                AS curr_emp_firstname, employees.middlename 
                AS curr_emp_middlename, employees.lastname 
                AS curr_emp_lastname");

            if (Session::get("company") != 'RBC-CORP') {
                $result->where('moc_requests.company', Session::get("company"));
                $result->whereIn('moc_requests.status', ["FOR ASSESSMENT","FOR ACKNOWLEDGEMENT","ACKNOWLEDGED","FOR APPROVAL"]);
            }else{
                $result->whereNotIn('moc_requests.curr_emp',$SSMDids);
                $result->whereIn('moc_requests.status', ["FOR ASSESSMENT"])
                    ->orwhere(['moc_requests.status' => 'FOR APPROVAL','moc_signatories.signature_type' => 6])
                    ->orwhere(['moc_requests.status' => "FOR ACKNOWLEDGEMENT",'moc_signatories.signature_type' => 7])
                    ->orwhere(['moc_requests.status' => "ACKNOWLEDGED",'moc_signatories.signature_type' => 8]);
            }

//
            $result = (array) $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req->transaction_code;
                    $result_data["aaData"][$ctr][] = $req->title;
                    $result_data["aaData"][$ctr][] = $req->status;
                    $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                    $result_data["aaData"][$ctr][] = $req->curr_emp_firstname.' '.$req->curr_emp_lastname;
                    $result_data["aaData"][$ctr][] = $req->assigned_date;
                    $result_data["aaData"][$ctr][] = $req->acknowledge_date == "0000-00-00" ? "" : $req->acknowledge_date;
                    if($req->curr_emp == Session::get("employee_id") && $req->status != 'ACKNOWLEDGED') {
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req->request_id).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req->request_id).">assess</a>";
                    }else{
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req->request_id).">View</a><a style='margin-left: 3px' disabled class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req->request_id).">assess</a>";
                    }
                    $ctr++;
                }
            }
            return $result_data;
//            $SSMDs = DB::table('employees')
//            ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
//            ->where("sections.sect_name","SATELLITE SYSTEMS")
//            ->select('employees.id as empID')
//            ->get();
//            $SSMDids = [];
//            foreach($SSMDs as $SSMD) {
//                array_push($SSMDids,$SSMD->empID);
//            }
//            $result = MOCRequests::whereIn('status', ["FOR ASSESSMENT", "FOR ACKNOWLEDGEMENT","ACKNOWLEDGED",'FOR APPROVAL'])
//                ->with('current_emp');

//            $result = $result->get();
//            $result_data["iTotalDisplayRecords"] = count($result);
//            $result_data["iTotalRecords"] = count($result);
//            if ( count($result) > 0){
//                $ctr = 0;
//                foreach($result as $req) {
//                    $result_data["aaData"][$ctr][] = $req['transaction_code'];
//                    $result_data["aaData"][$ctr][] = $req['title'];
//                    $result_data["aaData"][$ctr][] = $req['status'];
//                    $result_data["aaData"][$ctr][] = $req['firstname'].' '.$req['middlename'].' '.$req['lastname'];
//                    $result_data["aaData"][$ctr][] = $req['current_emp']['firstname'].' '.$req['current_emp']['middlename'].'. '.$req['current_emp']['lastname'];
//                    $result_data["aaData"][$ctr][] = $req['assigned_date'];
//                    $result_data["aaData"][$ctr][] = $req['acknowledge_date'] == "0000-00-00" ? "" : $req['acknowledge_date'];
//                    if($req['curr_emp'] == Session::get("employee_id")) {
//                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">assess</a>";
//                    }else{
//                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' disabled class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">assess</a>";
//                    }
//                    $ctr++;
//                }
//            }
//            else {
//                $result_data["aaData"] = $result;
//            }
//            return $result_data;
        }
    }

    public function getForApprovalMoc($limit = null) {
        if(Request::ajax()) {
            $result = MOCRequests::where('curr_emp', Session::get('employee_id'))
                ->where('status',"FOR APPROVAL");
            if($limit) {
                $result->limit($limit);
            }
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['transaction_code'];
                    $result_data["aaData"][$ctr][] = $req['date'];
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['firstname'].' '.$req['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/moc/view/for-approval-moc/'.$req['request_id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/moc/approve/for-approval-moc/'.$req['request_id']).">approve</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    /***************FOR EXPORTING*******************/
    public function downloadAttachments($refNum, $random_filename, $original_filename) {
        $dataID =  MOCRequests::where("transaction_code",$refNum)
            ->first(['request_id']);
        $fm = new Libraries\FileManager;
//        $this->logs->AU008($dataID['id'],'leaves','id',$refNum);
        $filepath = $this->MOC->getStoragePath() . "$refNum/" . $random_filename;
        if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
            $filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
        }
        return $fm->download($filepath, CIEncrypt::decode($original_filename));
    }

    /**********PRINT TO CSV*************/

    public function exportToCSV() {
        include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $start_row = 2;

        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'SUBMITTED PSR/MOC');
        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'EMPLOYEE NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DEPARTMENT/SECTION');
        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'DATE FILED');
        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'TYPE OF REQUEST');
        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'CHANGE IN');
        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'CHANGE TYPE');
        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'TARGET IMPLEMENTATION DATE');
        $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'DEPARTMENTS INVOLVED');
        $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'TITLE');
        $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", 'BACKGROUND INFORMATION');
        $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", 'SSMD ASSESSMENT/ACTION PLAN');
        $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", 'CSMD ASSESSMENT/ACTION PLAN');
        $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", 'COMMENT');
        $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", 'CURRENT');
        $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", 'DATE ASSIGNED');
        $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", 'STATUS');
        $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", 'DATE ACKNOWLEDGED');
        $objPHPExcel->getActiveSheet()->getStyle('A2:R2')->getFont()->setBold(true);

        $request = MOCRequests::search();
        foreach ($request as $req) {
            $ts_comments = MOCComments::where("request_id",$req->request_id)
            ->with("employee");
            $r_codes = MOCRequestCodes::where("request_id",$req->request_id)->get();
            $c_codes = MOCChangeInCodes::where("request_id",$req->request_id);
            $rcodes = [];
            $ccodes = [];
            $dept_involved = [];
            $comments = [];
            if((count($r_codes) > 0)) {
                foreach($r_codes as $rc) {
                    array_push($rcodes,$rc['request_desc']);
                }
            }
            if($c_codes) {
                foreach($c_codes as $cc) {
                    array_push($ccodes,$cc['changein_desc']);
                }
            }
            if($req->dept_involved) {
                foreach (json_decode($req->dept_involved) as $dept) {
                    array_push($dept_involved, $dept->dept_name);
                }
            }

            if($ts_comments) {
                foreach($ts_comments as $rc) {
                    array_push($comments,$rc->employee->firstname." says : $rc->comment;");
                }
            }
            $rcodes = implode(',',$rcodes);
            $ccodes = implode(',',$ccodes);
            $dept_involved = implode(',',$dept_involved);
            $comments = implode(',',$comments);

            $start_row++;
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req->transaction_code);
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req->firstname.' '.($req->middlename ? "$req->middlename ."  : '').$req->lastname);
            $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", json_decode($req->dept_sec)[0], "-", json_decode($req->dept_sec)[1]);
            $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req->date);
            $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $rcodes);
            $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $ccodes);
            $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req->change_type);
            $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req->implementation_date);
            $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $dept_involved);
            $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $req->title);
            $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", $req->reason);
            $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req->ssmd_action_plan);
            $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req->csmd_action_plan);
            $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $comments);
            $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req->curr_emp_firstname.' '.$req->curr_emp_lastname);
            $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req->assigned_date);
            $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req->status);
            $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->acknowledge_date);

        }
        //WRITE AND DOWNLOAD EXCEL FILE
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();

        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get("employee_id");
        $fname = (Input::get('action') == 'export' ? 'psr/moc' : (Input::get('filename') ?: 'unnamed'));
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fname.xls");
        return $objWriter->save('php://output');
        $objPHPExcel->disconnectWorksheets();
    }

    /*********PRINT TO PDF**********/

    public function printToPDF($id) {
        $data["base_url"] = URL::to("/");
        $data["mocID"] = $id;
        $data['changeIn'] = (new SelectItems)->getList('moc','change_in');
        $data['requestTypes'] = (new SelectItems)->getList('moc','request_type');
        $data['mocDetails'] = MOCRequests::where('request_id',$id)
            ->with('owner')
            ->with('signatories')
            ->with('rcodes')
            ->with('ccodes')
            ->with('attachments')
            ->with('comments')
            ->first();

        if(is_null($data['mocDetails'])) {
            return Redirect::to('/moc')
                ->with('errorMessage', 'invalid moc');
        }


        $data['dept_involved'] = json_decode($data['mocDetails']['dept_involved']);
        $data['lastSignature'] = MOCSignatories::where('moc_request_id',$id)->max('signature_type');
        $mocSignatories =  MOCSignatories::where('moc_request_id',$id);

        if($data['lastSignature'] == 3) {
            if($data['mocDetails']['company'] != 'RBC-CORP') {
                $data['toReturn'] = $mocSignatories->orderBy('id','desc')
                    ->with('employee')
                    ->limit(2)
                    ->get();
            }
        }
        elseif($data['lastSignature'] == 4 || (is_null($data['lastSignature']) && $data['mocDetails']['assessment_type'] == 2)) {
            $data['bsa'] = DB::table('receivers')
                ->join('employees','employees.id','=','receivers.employeeid')
                ->where('receivers.code' , CSMD_BSA)
                ->select((DB::raw("receivers.employeeid,concat(employees.firstname,' ',employees.middlename,'. ',employees.lastname) as fullname")))
                ->lists('fullname','employeeid');
        }elseif($data['lastSignature'] == 5) {
            $data['lastTwoSigs'] = $mocSignatories->orderBy('id','desc')
                ->with('employee')
                ->limit(2)
                ->get();
            $data['signatureCount'] = $mocSignatories->where('signature_type',5)
                ->count();
        }elseif($data['lastSignature'] == 6) {
            $data['lastTwoSigs'] = $mocSignatories->orderBy('id','desc')
                ->with('employee')
                ->limit(3)
                ->get();
        }
		
		
		
		
		

        $test =  View::make("moc.pdf.form", $data);


        ob_start();
        echo $test;
        $output = ob_get_clean();

        $path = 'assets/files/moc/';

        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get('name');
        $fname = "CC_$user" . "_$prnt_tstamp";
        $c = $path . $fname. '.html';
        $o = $path . $fname. '.pdf';

        file_put_contents($c ,$output);

        $fle = "wkhtmltopdf --dpi 125 --disable-external-links --disable-internal-links --orientation portrait --margin-top 12.7mm --margin-right 6.35mm --margin-bottom 12.7mm --margin-left 6.35mm --page-width 215.9mm --page-height 330.2mm \"$c\" \"$o\"";
        // 2>&1
         shell_exec($fle);

         return Redirect::to("/$o");
		
		
    }

}