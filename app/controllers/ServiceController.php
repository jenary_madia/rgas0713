<?php

use RGAS\Modules\SV;
use RGAS\Libraries;


class ServiceController extends \BaseController 
{

    protected $module_id;
    
	public function __construct()
	{		
		$this->SV = new SV\SV;	
        $this->Readonly = Input::get("page") == "view" ? " disabled " : "";
	}


	public function create()
	{
        $data["company"] =  ServiceVehicle::company();
        $data["location"] =  ServiceVehicle::location();
        $data["base_url"] = URL::to("/");		
        return View::make("svr.create", $data);    
    }    


    public function create_action()
    {   
        if (Input::get('action') == "send") 
        {
           $request = new SV\Requests\Store;
            $validate = $request->validate();
            if ($validate === true) 
            {
                 return $this->SV->create_sent();
            }
            else
            {
                return $validate;
            }     
        }
        elseif(Input::get('action') == "save")
        {
            return $this->SV->create_save();
        }      
    }

    public function edit($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::filer_record($id);
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        
        $data["location"] =  ServiceVehicle::location();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }

        $data["readonly"] = $this->Readonly;
        $data["company"] = $comp;
        $data["company_arr"] = $arr;
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.edit", $data);
        }        
    }



    

    public function edit_action($id)
    {   
        if (Input::get('action') == "send") 
        {
            $request = new SV\Requests\Store;
            $validate = $request->validate($id);
            if ($validate === true) 
            {
                 return $this->SV->edit_sent($id);
            }
            else
            {
                return $validate;
            }       
        }
        elseif(Input::get('action') == "save")
        {
            return $this->SV->edit_save($id);
        }
       
    }

    public function view($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::view_record($id);
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        $data["readonly"] = $this->Readonly;
        $data["location"] =  ServiceVehicle::location();
        $data["vehicle"] =  ServiceVehicle::vehicle();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }


        $data["company"] = $comp;
        $data["company_arr"] = $arr;
        $data["id"] = $id;  
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.view", $data);
        }        
    }



    public function view_action($id)
    {   
        $request = new SV\Requests\Store;
        $validate = $request->validate_view($id);
        if ($validate === true) 
        {
            if (Input::get('action') == "cancel") 
            {
                return $this->SV->view_cancel($id);
            }
        }
        else
        {
            return $validate;
        }       
    }

    public function approval($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::head_record($id,"FOR APPROVAL");
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        $data["location"] =  ServiceVehicle::location();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }
        $data["company_arr"] = $arr;
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.approval", $data);
        }        
    }

    

    public function approval_action($id)
    {
        if (Input::get('action') == "send") 
        {
            return $this->SV->approval_send($id);
        }
        else if (Input::get('action') == "return") 
        {
            $request = new SV\Requests\Store;
            $validate = $request->validate_approval($id);
            if ($validate === true) 
            {
                 return $this->SV->approval_return($id);           
            }
            else
            {
                return $validate;
            }      
        } 
    }

    
    public function filer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->SV->get_record();
        $data["record_approval"] =  $this->SV->get_review_record();
        return View::make("svr.filer", $data);    
    }

     public function reviewer_report()
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  $this->SV->get_receiver_record();

        $data["department"] =  ServiceVehicle::department();
        $comp = ServiceVehicle::company_list();
        foreach($comp as $key=>$val)
        {
            $arr[$val->comp_code] = $val->comp_code;
        }
        $data["company_arr"] = $arr;

        return View::make("svr.reviewer", $data);    
    }



    public function accept($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::head_record($id,"FOR ACCEPTANCE");
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        $data["location"] =  ServiceVehicle::location();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }

        $data["company"] = $comp;
        $data["company_arr"] = $arr;

        $data["plateno"] =  ServiceVehicle::plateno();
        $data["vehicle"] =  ServiceVehicle::vehicle();
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.reviewform", $data);
        }        
    }

    

    public function accept_action($id)
    {
        $request = new SV\Requests\Store;
        $validate = $request->validate_accept($id);
        if ($validate === true)
        {
            if (Input::get('action') == "send")
            {
                return $this->SV->accept_send($id);
            }
            else if (Input::get('action') == "return")
            {
                return $this->SV->accept_return($id);
            }
            else if (Input::get('action') == "served")
            {
                return $this->SV->accept_served($id);
            }
            else if (Input::get('action') == "cancelled")
            {
                return $this->SV->accept_cancelled($id);
            }
        }
        else
        {
            return $validate;
        }
    }

    public function process($id)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::head_record($id,"FOR PROCESSING");
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        $data["location"] =  ServiceVehicle::location();
        $data["vehicle"] =  ServiceVehicle::vehicle();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }
        $data["company_arr"] = $arr;
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.process", $data);
        }        
    }

    public function review_view($id,$status)
    {
        $data["base_url"] = URL::to("/");       
        $data["record"] =  ServiceVehicle::head_record($id,$status);
        $data["detail"] =  ServiceVehicle::vehicle_detail($id);
        $data["location"] =  ServiceVehicle::location();
        $data["vehicle"] =  ServiceVehicle::vehicle();

        $comp = ServiceVehicle::company();
        $arr[0] = "";
        foreach($comp as $key=>$val)
        {
            $arr[$val->company_id] = $val->company;
        }
        $data["company_arr"] = $arr;
        $data["id"] = $id;  
        $data["readonly"] = $this->Readonly;
        if(!is_array( $data["record"]))
        {
            return  $data["record"];
        }
        else
        {
            return View::make("svr.viewreview", $data);
        }        
    }

    public function delete_review($id)
    {   
        return $this->SV->delete_review($id);
    }

    public function delete_review_row()
    {   
        return $this->SV->delete_review_row();
    }

    public function deleterecord($id)
    {   
        return $this->SV->deleterecord($id);
    }


     public function review_get()
    {
        return $this->SV->get_reviewer();
    }

     public function review_get_dashboard()
    {
        return $this->SV->get_reviewer_dashboard();
    }


    public function curl()
    {

        $requests["result"] = 1;
        $requests["message"] = array("error1","error2");
        
        echo json_encode($requests);
        exit();
    }
}