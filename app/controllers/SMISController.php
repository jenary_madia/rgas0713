<?php
use RGAS\Modules\MSR;
use RGAS\Libraries;
class SMISController extends \BaseController
{
    protected $SMISvalidator;
    protected $SMIS;
    protected $MSR;

    public function __construct()
    {

        $this->SMISvalidator = new MSR\SMISValidator();
        $this->SMIS = new MSR\SMIS();
        $this->MSRvalidator = new MSR\MSRValidator();
        $this->MSR = new MSR\MSR();

    }

    public function index()
    {
        return Session::all();
    }

    /*************  For creation of MSR SMIS  ***********************/

    public function createAction()
    {
        if (Input::get("action") == 'send') {
            $validate = $this->SMISvalidator->send();
            if ($validate === true) {
                return $this->SMIS->send();
            } else {
                return $validate;
            }

        } elseif (Input::get("action") == 'save') {
            return $this->SMIS->save();
        }
    }

    public function reprocessAction($id) {
        if (Input::get("action") == 'send') {
            $validate = $this->SMISvalidator->send();
            if ($validate === true) {
                return $this->SMIS->resend($id);
            } else {
                return $validate;
            }

        } elseif (Input::get("action") == 'save') {
            return $this->SMIS->resave($id);
        }
    }
    /*************  For Approval Action  ***********************/

    public function forApprovalAction($id) {
        if (Input::get("action") == "toIquest") {
            return $this->SMIS->toIQuest($id);
        }elseif(Input::get("action") == "toFiler"){
            return $this->MSR->returnToFiler($id);
        }
    }

    /***************** LIST SUBMITTED MSR *****************/
    public function submittedMSR() {
        $data["base_url"] = URL::to("/");
        $SMISDepts = json_decode(MSR_SMIS_DEPARTMENTS,true);
        $iquestIndex = array_search('IQUEST',$SMISDepts);
        unset($SMISDepts[$iquestIndex]);
        $data["departments"] = Departments::whereIn('dept_code',$SMISDepts)->get();
        return View::make("msr.smis.submitted_msr", $data);
    }

    // ***************************************VIEWING SUBMITTED MSR
    public function viewSubmittedMSR($id,$method) {
        $data['method'] = $method;
        $data['MSRDetails'] = ManpowerService::where('id', $id)
            ->where('curr_emp', Session::get("employee_id"))
            ->with('attachments')
            ->with('section')
            ->with('agencies')
            ->with('signatories')
            ->first();
        $data['SMISRequirements'] = (new ProjectRequirements())->fetch_req($id);
        $data['endorsed'] = [];
        if ($data['MSRDetails']['signatories']) {
            foreach ($data['MSRDetails']['signatories'] as $signatory){
                if($signatory['signature_type'] == 1) {
                    array_push($data['endorsed'],$signatory);
                }
            }
        }
        if (!$data['MSRDetails']) {
            return Redirect::to('msr')
                ->with("errorMessage", "Invalid MSR");
        }
        foreach ($data['MSRDetails']['requirements'] as $key) {
            $key['active'] = false;
        }

        foreach ($data['MSRDetails']['agencies'] as $key) {
            $key['active'] = false;
        }
        if (in_array($method,['view','process'])) {
            $data["base_url"] = URL::to("/");
            return View::make("msr.smis.submitted_msr_view", $data);
        }else{
            return Redirect::to("msr")
                ->with("errorMessage","Invalid action");
        }
    }


    /*************  FOR AJAX - POPULATING OWN MSR ***********************/
    public function getMyMSR($limit = null)
    {
        if (Request::ajax()) {
            $result = ManpowerService::where('employeeid', Session::get('employee_id'))
                ->with('current_emp')
                ->where('status', '!=', "DELETED");
            if($limit) {
                $result->whereIn("status",["FOR APPROVAL","DISAPPROVED","FOR PROCESSING"]);
                $result = $result->limit($limit);
            }else{
                $result ->where('status','NOT LIKE',"%DELETED%");
            }
			$result->orderBy("id","desc");
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if (count($result) > 0) {
                $ctr = 0;
                foreach ($result as $req) {
                    $deletableNotifications = (in_array($req['status'], json_decode(MSR_DELETABLE, true)));
                    $editableNotifications = (in_array($req['status'], json_decode(MSR_EDITABLE, true)));
                    $btnDelete = "<button  " . ($deletableNotifications ? '' : 'disabled') . " class='btn btn-default btndefault' name='lrfAction' value='softDelete|{$req->id}'>Delete</button>";
                    $btnView = "<a class='btn btn-default btndefault' href=" . url('/msr/my_msr/' . $req->id . '/view') . ">View</a>";
                    $btnEdit = "<a " . ($editableNotifications ? '' : 'disabled') . " class='btn btn-default btndefault' href=" . url('/msr/my_msr/' . $req->id . '/edit') . ">Edit</a>";
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = ($req['projectname'] ? substr($req['projectname'], 0, 25) : "");
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['current_emp']['firstname'] . ' ' . $req['current_emp']['lastname'];
                    $result_data["aaData"][$ctr][] = $btnDelete . ' ' . $btnView . ' ' . $btnEdit;
                    $ctr++;
                }
            } else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function submittedMSRAction($id) {
        return $this->SMIS->saveToArchive($id);
    }

    /*************  FOR AJAX - POPULATING FOR APPROVAL MSR ***********************/
    public function getSuperiorMSR($limit = null) {
        if(Request::ajax()) {
            $result = ManpowerService::where('curr_emp', Session::get('employee_id'))
                ->with('owner')
                ->whereIn('departmentid', $this->getDeptID(json_decode(MSR_SMIS_DEPARTMENTS,true)))
                ->whereIn('status',['FOR APPROVAL','FOR PROCESSING']);
            if($limit) {
                $result = $result->limit($limit);
            }
			$result->orderBy("id","desc");
            $result = $result->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = ($req['projectname'] ? substr($req['projectname'], 0, 30) : "");
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                    if($req['status'] == "FOR APPROVAL") {
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/msr/for_approval/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/msr/for_approval/'.$req['id'].'/approve').">approve</a>";
                    }elseif($req['status'] == "FOR PROCESSING") {
                        $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/msr/smis/submitted/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/msr/smis/submitted/'.$req['id'].'/process').">process</a>";
                    }
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    /*************  FOR AJAX - POPULATING SUBMITTED MSR ***********************/
    public function getSubmittedMSR() {
        if(Request::ajax()) {
            $SMISDepts = json_decode(MSR_SMIS_DEPARTMENTS,true);
            $iquestIndex = array_search('IQUEST',$SMISDepts);
            unset($SMISDepts[$iquestIndex]);
            $result = ManpowerService::where('curr_emp', Session::get('employee_id'))
                ->whereIn('departmentid', $this->getDeptID($SMISDepts))
                ->whereIn('status',['FOR PROCESSING','PROCESSED'])
                ->with('owner')
                ->get();
            $result_data["iTotalDisplayRecords"] = count($result);
            $result_data["iTotalRecords"] = count($result);
            if ( count($result) > 0){
                $ctr = 0;
                foreach($result as $req) {
                    $result_data["aaData"][$ctr][] = $req['reference_no'];
                    $result_data["aaData"][$ctr][] = $req['datefiled'];
                    $result_data["aaData"][$ctr][] = ($req['projectname'] ? substr($req['projectname'], 0, 30) : "");
                    $result_data["aaData"][$ctr][] = $req['status'];
                    $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                    $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/msr/smis/submitted/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' ".($req['status']=='PROCESSED' ? 'disabled' : '')." class='btn btn-default btndefault' href=".url('/msr/smis/submitted/'.$req['id'].'/process').">process</a>";
                    $ctr++;
                }
            }
            else {
                $result_data["aaData"] = $result;
            }
            return $result_data;
        }
    }

    public function getFilteredSubmittedMSR(){
        $SMISDepts = json_decode(MSR_SMIS_DEPARTMENTS,true);
        $iquestIndex = array_search('IQUEST',$SMISDepts);
        unset($SMISDepts[$iquestIndex]);
        return $result = (new ManpowerService())->filterMSRForReceiver(2,$this->getDeptID($SMISDepts));
    }

    /*********FOR EXPORTING TO CSV*******/
    public function exportToCSV() {
        include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $start_row = 2;

        $objPHPExcel->getActiveSheet()->setCellValue("A1", 'SUBMITTED MSR');
        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'EMPLOYEE NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'EMPLOYEE NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'COMPANY');
        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'DEPARTMENT');
        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'SECTION');
        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'REFERENCE NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'DATE FILED');
        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'STATUS');
        $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'CONTACT NUMBER');
        $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'REQUEST');
        $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", 'URGENCY');
        $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", 'PROJEC TITLE/NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", 'JOB TITLE/JOB POSITION');
        $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", 'RECRUITMENT DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", 'RECRUITMENT DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", 'RECRUITMENT NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", 'PREPARATION DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", 'PREPARATION DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", 'PREPARATION NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", 'BRIEFING DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", 'BRIEFING DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", 'BRIEFING NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", 'FIELD WORK DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", 'FIELD WORK DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", 'FIELD WORK NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", 'FIELD WORK AREA');
        $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", 'EDITING DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", 'EDITING DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", 'EDITING NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", 'CODING DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", 'CODING DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", 'CODING NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", 'ENCODING DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", 'ENCODING DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("AI{$start_row}", 'ENCODING NUMBER OF PAX');
        $objPHPExcel->getActiveSheet()->setCellValue("AJ{$start_row}", 'DATA CLEANING DATE NEEDED');
        $objPHPExcel->getActiveSheet()->setCellValue("AK{$start_row}", 'DATA CLEANING DATE NEEDED TO');
        $objPHPExcel->getActiveSheet()->setCellValue("AL{$start_row}", 'DATA CLEANING');
        $objPHPExcel->getActiveSheet()->setCellValue("AM{$start_row}", 'AGENCY NAME');
        $objPHPExcel->getActiveSheet()->setCellValue("AN{$start_row}", 'AGENCY INFO');
        $objPHPExcel->getActiveSheet()->setCellValue("AO{$start_row}", 'SIGNATORIES SIGNATURE TYPE');
        $objPHPExcel->getActiveSheet()->setCellValue("AP{$start_row}", 'SIGNATORIES EMPLOYEE SIGNED');
        $objPHPExcel->getActiveSheet()->setCellValue("AQ{$start_row}", 'SIGNATORIES DATE');

        $objPHPExcel->getActiveSheet()->getStyle('A2:U2')->getFont()->setBold(true);
        $SMISDepts = json_decode(MSR_SMIS_DEPARTMENTS,true);
        $iquestIndex = array_search('IQUEST',$SMISDepts);
        unset($SMISDepts[$iquestIndex]);
        $request = (new ManpowerService())->printMSRForReceiver(2,$this->getDeptID($SMISDepts));
        foreach ($request as $req) {
            $countList = [
                count($req['agencies']),
                count($req['projectRequirements']),
                count($req['signatories'])
            ];
            $rounds = max($countList);

            for ($x = 0; $x <= $rounds - 1; $x++)
            {
                $start_row++;
                $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req['owner']['firstname'].' '.$req['owner']['middlename'].' '.$req['owner']['lastname']);
                $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req['owner']['employeeid']);
                $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req['owner']['company']);
                $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req['department']['dept_name']);
                $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req['section']['sect_name']);
                $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req['reference_no']);
                $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req['datefiled']);
                $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req['status']);
                $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req['contactno']);
                $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", ($req['pooling'] ? "POOLING" : "SCREENING"));
                $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", ($req['rush'] ? "RUSH" : "NORMAL PRIORITY"));
                $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req['projectname']);

                if(array_key_exists($x,json_decode($req['projectRequirements'],true))) {
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req['project_requirements'][$x]['position_jobtitle']);
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $req['project_requirements'][$x]['rec_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req['project_requirements'][$x]['rec_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req['project_requirements'][$x]['rec_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req['project_requirements'][$x]['prep_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req['project_requirements'][$x]['prep_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", $req['project_requirements'][$x]['prep_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", $req['project_requirements'][$x]['brif_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", $req['project_requirements'][$x]['brif_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", $req['project_requirements'][$x]['brif_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", $req['project_requirements'][$x]['fw_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", $req['project_requirements'][$x]['fw_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", $req['project_requirements'][$x]['fw_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", $req['project_requirements'][$x]['fw_area']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", $req['project_requirements'][$x]['edit_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", $req['project_requirements'][$x]['edit_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", $req['project_requirements'][$x]['edit_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", $req['project_requirements'][$x]['code_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", $req['project_requirements'][$x]['code_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", $req['project_requirements'][$x]['code_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", $req['project_requirements'][$x]['enc_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", $req['project_requirements'][$x]['enc_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AI{$start_row}", $req['project_requirements'][$x]['enc_pax']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AJ{$start_row}", $req['project_requirements'][$x]['dc_dateneeded']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AK{$start_row}", $req['project_requirements'][$x]['dc_dateneeded_to']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AL{$start_row}", $req['project_requirements'][$x]['dc_pax']);

                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AI{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AJ{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AK{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AL{$start_row}", '*');

                }

                if(array_key_exists($x,json_decode($req['agencies'],true))) {
                    $objPHPExcel->getActiveSheet()->setCellValue("AM{$start_row}", $req['agencies'][$x]['agency']);
                    $objPHPExcel->getActiveSheet()->setCellValue("AN{$start_row}", $req['agencies'][$x]['agency_info']);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("AM{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AN{$start_row}", '*');
                }

                if(array_key_exists($x,json_decode($req['signatories'],true))) {
                    switch ($req['signatories'][$x]['signature_type']) {
                        case 1:
                            $signatureType = 'endorsed';
                            break;
                        case 2:
                            $signatureType = 'received';
                            break;
                        case 3:
                            $signatureType = 'processed';
                            break;
                    }
                    $objPHPExcel->getActiveSheet()->setCellValue("AO{$start_row}", $signatureType);
                    $objPHPExcel->getActiveSheet()->setCellValue("AP{$start_row}", $this->fetchFullName($req['signatories'][$x]['employee']));
                    $objPHPExcel->getActiveSheet()->setCellValue("AQ{$start_row}", $req['signatories'][$x]['approval_date']);
                }else{
                    $objPHPExcel->getActiveSheet()->setCellValue("AO{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AP{$start_row}", '*');
                    $objPHPExcel->getActiveSheet()->setCellValue("AQ{$start_row}", '*');
                }

            }
        }
        //WRITE AND DOWNLOAD EXCEL FILE
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();

        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get("employee_id");
        $fname = (Input::get('action') == "generate" ? Input::get('filename') : (Input::get('filename') ? Input::get('filename') : date('Y-m-d')."-MSR"));
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fname.xls");
        return $objWriter->save('php://output');
        $objPHPExcel->disconnectWorksheets();
    }

    private function getDeptID($dept_code) {
        $data = Departments::whereIn('dept_code',$dept_code)->get(['id']);
        $ids  = [];
        if ($data) {
            foreach($data as $key) {
                array_push($ids,$key['id']);
            }
        }

        return $ids;
    }
    
    private function fetchFullName($data) {
        return $data['firstname']." ".$data['middlename']." ".$data['lastname'];
    }

}