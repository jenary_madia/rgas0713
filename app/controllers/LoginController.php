
<?php

class LoginController extends \BaseController {
    
    
	//
	public function index()
	{
        //
		if(Session::has("logged_in"))
        {
			return Redirect::to("");
			die;
		}
        
		$data["base_url"] = URL::to("/");
		return View::make("login", $data);
	}
	
	
	//For login page original
	public function sign_in()
	{
	
		$result_arr = array();
		$username = Input::get("username", null, true);
		$password = md5(Input::get("password", null, true));
		
		$emp = Employees::leftJoin("departments as dept", function($join){
		 
			$join->on("employees.departmentid", "=", "dept.id");
			
		 })->leftJoin("sections as sect", function($join){
		 
			$join->on("employees.sectionid", "=", "sect.id");
			
		 })->where("employees.username", "=", $username)->where("employees.password","=", $password)->where("employees.active", "<>", "0")->first([
			 "employees.id as employee_id"
			,"employees.employeeid"
			,"employees.new_employeeid"
			,"employees.firstname"
			,"employees.middlename"
			,"employees.lastname"
			,"employees.desig_level"
			,"employees.company"
			,"employees.designation"
			,"employees.superiorid"
			,"employees.username"
			,"employees.email"
			,"employees.departmentid2"
			,"dept.dept_name"
			,"dept.dept_type"
			,"dept.id as dept_id"
			,"sect.sect_name"
			,"sect.id as sect_id"

			/*MSR JENARY*/
				,"dept.dept_code as dept_code"
			/*END MSR JENARY*/
			//FOR MOC
				,"dept.dept_type as dept_type"
			//END FOR MOC
		 ]);
		 
        
        
		if(!empty($emp))
		{
			$employee = Employees::find($emp->employee_id);
            Auth::login($employee);
			Session::put("logged_in", TRUE);
			Session::put("employee_id", $emp->employee_id);
			Session::put("employeeid", $emp->employeeid);
			Session::put("new_employeeid", $emp->new_employeeid);
            $employee_name = $emp->firstname . " " . ((!empty($emp->middlename)) ? $emp->middlename . ". " : "") . $emp->lastname;
            Session::put("employee_name", $employee_name);
            Session::put("full_name", $emp->firstname." ".$emp->lastname);
			Session::put("firstname", $emp->firstname);
			Session::put("lastname", $emp->lastname);
			Session::put("desig_level", $emp->desig_level);
			Session::put("company", $emp->company);
			Session::put("designation", $emp->designation);
			Session::put("superiorid", $emp->superiorid);
			Session::put("dept_name", $emp->dept_name);
			Session::put("dept_type", $emp->dept_type);
			Session::put("dept_id", $emp->dept_id);
			Session::put("sect_name", $emp->sect_name);

//			may 12 2017 added by jenary
			Session::put("middlename", $emp->middlename);
//			end jenary

			//added by jma 12272016
			Session::put("departmentid2", $emp->departmentid2);
			
			// added by jenary
			Session::put("username", $emp->username);
			Session::put("sect_id", $emp->sect_id);
			Session::put("email", $emp->email);

			// for MSR
			Session::put("dept_code", $emp->dept_code);
			
			
			// end for MSR

			//FOR MOC
			Session::put("dept_type", $emp->dept_type);
			//END FOR MOC

			// end by jenary

			$is_taf_receiver = Receivers::is_taf_receiver($emp->employee_id);
			Session::put("is_taf_receiver", $is_taf_receiver);
			
            $is_itr_bsa = Receivers::is_itr_bsa($emp->employee_id);
            Session::put('is_itr_bsa', $is_itr_bsa);
            
            $is_citd_employee = ItrRequests::check_citd_employee($emp->employee_id);
            Session::put('is_citd_employee', $is_citd_employee);
            
            $is_system_satellite = ItrRequests::check_satellite_systems($emp->employee_id);
            Session::put('is_system_satellite', $is_system_satellite);
			
			// CBR
			// Check if the user logged-in is CBR Receiver
			$is_cbr_receiver = Receivers::is_cbr_receiver($emp->employee_id);
            Session::put('is_cbr_receiver', $is_cbr_receiver);
			
			// Check if the user logged-in is CBR Staff
			$is_cbr_staff = Receivers::is_cbr_staff($emp->employee_id);
            Session::put('is_cbr_staff', $is_cbr_staff);
			//-- End CBR
						
			//assessment
			// Check if the user logged-in has privilege in TE
			$is_te_chrd = Receivers::is_te_chrd($emp->employee_id);
			Session::put('is_te_chrd', $is_te_chrd);
            
			//vp
			// Check if the user logged-in has privilege in TE Vice President
			// $is_chrd_vp = Receivers::is_chrd_vp($emp->employee_id);
			// Session::put('is_chrd_vp', $is_chrd_vp);
			
			//svp
			// Check if the user logged-in has privilege in TE SVP
			// $is_svp_for_cs = Receivers::is_svp_for_cs($emp->employee_id);
			// Session::put('is_svp_for_cs',$is_svp_for_cs);
			
            //-- End TE
			
			//PMARF
			$is_pmarf_npmd_personnel = Receivers::is_pmarf_npmd_personnel($emp->employee_id);
            Session::put('is_pmarf_npmd_personnel', $is_pmarf_npmd_personnel);	
			
			$is_pmarf_npmd_head = Receivers::is_pmarf_npmd_head($emp->employee_id);
            Session::put('is_pmarf_npmd_head', $is_pmarf_npmd_head);			
			//-- End PMARF

			/*  NTE  */
			$is_chrd_head = Receivers::isChrdHead($emp->employeeid);
			Session::put('is_chrd_head',$is_chrd_head);

			$is_chrd_aer_staff = Receivers::is_chrd_aer_staff($emp->employee_id);
			Session::put("is_chrd_aer_staff", $is_chrd_aer_staff);

			$is_cbrm = $emp->designation == "BENEFITS ADMINSTRATION ASST" ? TRUE : FALSE;
			Session::put('is_cbrm', $is_cbrm);

            $is_corporate_employee = $emp->company == "RBC-CORP" ? TRUE : FALSE;
            Session::put('is_corporate_employee', $is_corporate_employee);

            $is_recruitment_specialist = $emp->designation == "SR. RECRUITMENT SPECIALIST" ? TRUE : FALSE;
            Session::put('is_recruitment_specialist', $is_recruitment_specialist);

            //$is_mrf_filer = MrfSectionHeadAccesses::isMrfFiler($emp->employee_id);
            //Session::put('is_mrf_filer', $is_mrf_filer);

            $is_csmd_head = $emp->departmentid2 == "CSMD" && $emp->desig_level == "head"? TRUE : FALSE;
            Session::put('is_csmd_head', $is_csmd_head);

            $is_csmd_assistant = $emp->departmentid2 == "CSMD" && $emp->desig_level == "supervisor"? TRUE : FALSE;
            Session::put('is_csmd_assistant', $is_csmd_assistant);

            $is_csmd_bsa = $emp->departmentid2 == "CSMD" && $emp->designation == "SR. BUSINESS SYSTEM SPECIALIST"? TRUE : FALSE;
            Session::put('is_csmd_bsa', $is_csmd_bsa);

            $is_system_staff = $emp->designation == "SYSTEMS STAFF" ? TRUE : FALSE;
            Session::put('is_system_staff', $is_system_staff);	

            $is_vpo = $emp->desig_level == 'vpo' ? TRUE :FALSE;
            Session::put('is_vpo', $is_vpo);

            /*  END NTE  */

            // Added by Jenary
 
            $is_lrf_receiver = Receivers::is_lrf_receiver($emp->employee_id);
            Session::put('is_lrf_receiver',$is_lrf_receiver);
 
            $is_clinic_receiver = Receivers::is_clinic_receiver($emp->employee_id);
            Session::put('is_clinic_receiver',$is_clinic_receiver);
 
            $is_notif_receiver = Receivers::is_notif_receiver($emp->employee_id);
            Session::put('is_lrf_receiver',$is_lrf_receiver);
 
        	//MSR
        	$is_msr_receiver = Receivers::is_msr_receiver($emp->employee_id);
			Session::put('is_msr_receiver',$is_msr_receiver);
			//END MSR

			// for moc
				$is_div_head = Receivers::is_div_head($emp->employee_id);
				Session::put('is_div_head',$is_div_head);

				$is_bsa = Receivers::is_bsa_receiver($emp->employee_id);
				Session::put('is_bsa',$is_bsa);
			// end for moc

            // End Add by Jenary
            
			//070417 added by jenary
				$is_ssmd = Receivers::is_ssmd($emp->employee_id);
				Session::put('is_ssmd',$is_ssmd);
			//end 070417

			//110517 added by jenary
				$is_smis_adh = Receivers::is_msr_adh(MSR_SMIS_ADH,$emp->employee_id);
				Session::put('is_smis_adh',$is_smis_adh);
	
				$is_npmd_adh = Receivers::is_msr_adh(MSR_NPMD_ADH,$emp->employee_id);
				Session::put('is_npmd_adh',$is_npmd_adh);
			//end 110517

			//220617 added by jenary
				$mrf_is_chrd_head = Receivers::mrf_is_chrd_head($emp->employee_id);
				Session::put('is_smis_adh',$mrf_is_chrd_head);
			//end 220617

			//--
			$result_arr["logged_in"] = TRUE;
			
			
			// START: ALL ADDED BY JM

			//get access for clearance JMA
			Receivers::get_access($emp->employee_id);
            
			//check if employee is division head JMA
			$is_division_head = DB::table("divisions")
			->where("employeeid",$emp->employee_id)
			->get(["div_code"]);

			if(isset($is_division_head[0]->div_code))
			{
				Session::put('is_division_head', true);
				Session::put('div_code', $is_division_head[0]->div_code);				
			}

			//check if employee is affiliate
			$is_affiliate = DB::table("companylists")
			->where("comp_code", $emp->company )
			->get(["affiliate"]);

			if(isset($is_affiliate[0]->affiliate) && $is_affiliate[0]->affiliate == 1)
			{
				Session::put('is_affiliate', true);			
			}
			
			//01162017
			$ispmf = Receivers::is_pmF_receiver($emp->employee_id);
			Session::put('is_pmf_receiver', $ispmf);

			// END: ALL ADDED BY JM
			
			AuditTrail::store(
				$emp->employee_id, // user_id
				'AU009', // action_code
				'', // module_id
				'', // table_name
				json_encode(array('username','password')), // params
				"", // old
				"", // new
				"" // ref_num
			);
            
		}
		else
		{
			Session::put("logged_in", FALSE);
			$result_arr["logged_in"] = FALSE;
			$result_arr["message"] = "Invalid Username or Password";
			
			AuditTrail::store(
				'', // user_id
				'AU010', // action_code
				'', // module_id
				'', // table_name
				json_encode(array('username','password')), // params
				"", // old
				"", // new
				"" // ref_num
			);
		}

		echo json_encode($result_arr);
		
	}
	
    
    //for CURL
    public function sign_in2()
    {
        
        $result = false;
        
		$username = Input::get("username", null, true);
		$password = md5(Input::get("password", null, true));
        $employee_id = intval(Input::get("employee_id", null, true));
        
		$token = Input::get("token", null, true);
        
		$emp = Employees::leftJoin("departments as dept", function($join){
		 
			$join->on("employees.departmentid", "=", "dept.id");
			
		 })->leftJoin("sections as sect", function($join){
		 
			$join->on("employees.sectionid", "=", "sect.id");
			
		 })->where("employees.username", "=", $username)->where("employees.password","=", $password)->where("employees.active", "<>", "0")->first([
			 "employees.id as employee_id"
			,"employees.employeeid"
			,"employees.new_employeeid"
			,"employees.firstname"
			,"employees.middlename"
			,"employees.lastname"
			,"employees.desig_level"
			,"employees.company"
			,"employees.designation"
			,"employees.superiorid"
			,"dept.dept_name"
			,"dept.dept_type"
			,"dept.id as dept_id"
			,"sect.sect_name"
		 ]);
		
        
		if(!empty($emp))
		{
            
            $employee_id = $emp->employee_id;    
            SessionToken::where("emp_id", "=", "{$employee_id}")->delete();
            
            $session_token = new SessionToken;
            $session_token->session_token = $token;
            $session_token->emp_id = $employee_id;
            $session_token->save();
            
            $result = TRUE;
		}
        
        
		return $result;
    }
    
	
	//
	public function sign_out()
	{
		Session::flush();
		Auth::logout();
		
		AuditTrail::store(
				'', // user_id
				'AU011', // action_code
				'', // module_id
				'', // table_name
				// json_encode(array('username','password')), // params,
				"", // params
				"", // old
				"", // new
				"" // ref_num
			);
		
		return Redirect::to("login");
	}
    
    
    public function sign_out2()
	{
        $result = false;
		$tk = Input::get("token", null, true);
        
        try {
            
            SessionToken::where("session_token", "=", "{$tk}")->delete();
        
        if($affected_rows > 0)
        {
            Session::flush(); //Clear session for laravel;
            $result = true;
            return $result;
        }
        else
        {
           return $result;
        }
        }
        catch (\Exception $e) {
            
            return FALSE;
            throw $e;
            
        }
        
        Auth::logout();
        
        
	}
	
}
//End of File