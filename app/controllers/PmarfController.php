
<?php
use RGAS\Modules\PMARF;
use RGAS\Libraries;
use RGAS\Repositories\PMARFRepository;

class PmarfController extends \BaseController {

	private $PMARF;
	
	public function __construct()
	{
		$this->PMARF = new PMARF\Pmarf;
	}
	public function index() {
		//
	}
	
	public function view_my_request($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$request = $pmarf->getRequest($pmarf_id);
		
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
	}
	
	public function submittedPmarfRequests()
	{
		$data["base_url"] = URL::to("/");
		return View::make("pmarf/submitted_pmarf_requests", $data);
	}

	public function submittedReport()
	{
		$data["base_url"] = URL::to("/");
		return View::make("pmarf/pmarf_requests", $data);
	}
		
    public function create()
    {	
		$data["base_url"] = URL::to("/");
		//$data["reference_number"] = ReferenceNumbers::get_current_reference_number('pmarf');
		$data["activity_type"] = PmarfLibrary::defined_lib('activity_type');
        $data["activity_date"] = PmarfLibrary::defined_lib('activity_date');
        $data["implementer"] = PmarfLibrary::defined_lib('implementer');
        $data["initiated_by"] = PmarfLibrary::defined_lib('initiated_by');
		
		return View::make("pmarf/create_pmarf", $data);
    }
    
	//Requestor's Creation
    public function action()
    {	
		$data["base_url"] = URL::to("/");
       // $data["reference_number"] = ReferenceNumbers::get_current_reference_number('pmarf');
        $data["activity_type"] = PmarfLibrary::defined_lib('activity_type');
        $data["activity_date"] = PmarfLibrary::defined_lib('activity_date');
        $data["implementer"] = PmarfLibrary::defined_lib('implementer');
        $data["initiated_by"] = PmarfLibrary::defined_lib('initiated_by');
		
		$input = Input::all();
		$increment = false;
		
		if($input['action'] == 'save')
		{
			$increment = true;
			//ReferenceNumbers::increment_reference_number('pmarf');
			$this->PMARF->storeRequest($input);
			return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SAVED');
		}
		if($input['action'] == 'send')
		{	
			
			$request = new PMARF\Requests\Store;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$increment = true;
				//ReferenceNumbers::increment_reference_number('pmarf');
				$this->PMARF->storeRequest($input);
				
				if(Session::get('desig_level') == 'head'){
					return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO NPMD DEPARTMENT HEAD FOR PROCESSING');
				}
				else{
					return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO IMMEDIATE SUPERIOR FOR APPROVAL');
				}
			}
			else {
				return $is_valid;
			}

		}
        return View::make("pmarf/create_pmarf", $data);
    }
	
	//USE THIS FOR EDIT
    public function edit_pmarf($pmarf_id)
	{
        $pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);

		$pmamrf                         = new PromoAndMerchandising;
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;
				 // die();
				
			}
			$data["init"] = $init;
		}
				
        $data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
		$input = Input::all();
		if(Request::isMethod('post')){
			if($input['action'] == 'update' )
			{
				$this->PMARF->editRequest($pmarf_id);
				return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SAVED');	
			}
			
			if($input['action'] == 'send')
			{	
				$request = new PMARF\Requests\Store;
				$request->validate();
				
				$is_valid = $request->validate();
				if($is_valid === true){
					$increment = true;
					ReferenceNumbers::increment_reference_number('pmarf');
					$this->PMARF->editRequest($pmarf_id);
					if(Session::get('desig_level') == 'head'){
						return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO NPMD DEPARTMENT HEAD FOR PROCESSING');
					}
					else{
						return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO IMMEDIATE SUPERIOR FOR APPROVAL');
					}
				}
				else {
					return $is_valid;
				}

			}
		}

		return View::make('pmarf/edit_pmarf', $data);
    }
	
	public function download($pmarf_ref_num, $random_filename, $original_filename)
	{	
		$fm = new Libraries\FileManager;
		$filepath = Config::get('rgas.rgas_storage_path') . $this->PMARF->attachments_path . "$pmarf_ref_num/" . $random_filename;
		if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
			$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
		}
			
		$this->PMARF->downloadLog($pmarf_ref_num);
		return $fm->download($filepath, CIEncrypt::decode($original_filename));
	}
	
	public function download_npmd($pmarf_ref_num, $random_filename, $original_filename)
	{	
		$fm = new Libraries\FileManager;
		$filepath = Config::get('rgas.rgas_storage_path') . $this->PMARF->attachments_path . "$pmarf_ref_num/" . $random_filename;
		if(!$fm->download($filepath, CIEncrypt::decode($original_filename))){
			$filepath = Config::get('rgas.rgas_temp_storage_path') . $random_filename;
		}
			
		$this->PMARF->downloadLog_npmd($pmarf_ref_num);
		return $fm->download($filepath, CIEncrypt::decode($original_filename));
	}
	
   //Requestor
   // public function get_my_save_pmarf()
   // {
        // $pmamrf = new PromoAndMerchandising;
        // $oEmp   = Employees::get(Session::get('employee_id'));
        // switch($oEmp->desig_level){
	        // case "employee"://IS
	            // $aData = $pmamrf->get_save();
	        // break;
	        // case "supervisor"://IS
	            // $aData = $pmamrf->ISview();
	        // break;
	        // case "head"://IS
	            // $aData = $pmamrf->DHview();
	        // break;
        // }
        // return json_encode($aData);
    // }
    
    
	//IMMEDIATE SUPERVISOR's action
    public function immSupApprove($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);

		if(Input::get('action') == 'send'){
			$this->PMARF->immSupAction($pmarf_id);
			return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR APPROVAL');
		}
        
        if(Input::get('action') == 'return'){
			$request = new PMARF\Requests\ValidateComments;
			$request->validate();
			
			$is_valid = $request->validate();
			
			if($is_valid === true){
				$this->PMARF->immSupAction($pmarf_id);
				return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED TO FILER');
			}
			else {
				return $is_valid;
			}
        }
		else{
				$pmamrf                         = new PromoAndMerchandising;
				$data["base_url"]               = URL::to("/");
				$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
				$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
				$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
				$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
				$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');

				$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
				$init = array();

				$b = json_decode($request->pmarf_initiated_by, true);
				if(is_array($b)){
					foreach($b as $a => $c){
						 $init[$a] = $c;						
					}
					$data["init"] = $init;
				}
				$data['request'] = $request;
        }
		return View::make("pmarf/imm_sup_approve_pmarf", $data); 
    }
	
	//DEPT HEAD's action
    public function dhApprove($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);

		if(Input::get('action') == 'send'){
			$this->PMARF->deptHeadAction($pmarf_id);
			return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO NPMD DEPARTMENT HEAD FOR PROCESSING');
		}
		
		if(Input::get('action') == 'return'){
			$request = new PMARF\Requests\ValidateComments;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->deptHeadAction($pmarf_id);
				return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED TO FILER');
			}
			else {
				return $is_valid;
			}
		}
		
		else{
				$pmamrf                         = new PromoAndMerchandising;
				$data["base_url"]               = URL::to("/");
				$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
				$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
				$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
				$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
				$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
				$data["supInfo"]                = json_decode($request->pmarf_supervisor_info);
				$data["depInfo"]                = json_decode($request->pmarf_depthead_info);
				$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
				$init = array();

				$b = json_decode($request->pmarf_initiated_by, true);
				if(is_array($b)){
					foreach($b as $a => $c){
						 $init[$a] = $c;						
					}
					$data["init"] = $init;
				}
				$data["request"] = $request;
        }
		return View::make("pmarf/dept_head_approve_pmarf", $data);

    }
	
	public function npmdHeadAssigning($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
		$npmdpersonnel = $pmarfRep->getNpmdPersonnel();

		$pmamrf                         = new PromoAndMerchandising;
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["supInfo"]                = json_decode($request->pmarf_supervisor_info);
		$data["depInfo"]                = json_decode($request->pmarf_depthead_info);
		
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;
			}
			$data["init"] = $init;
		}
		
		if(Input::get('action') == 'assign')
		{
			$request = new PMARF\Requests\Assign;
			$request->validate();
	
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdHeadAction($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO ASSIGNED PERSONNEL');
			}
			else {
				return $is_valid;
			}
			
		}
		if(Input::get('action') == 'return')
		{
			$request = new PMARF\Requests\Returning;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdHeadAction($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED');
			}
			else{
				return $is_valid;
			}
		}
		
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		$data["npmdpersonnel"] = $npmdpersonnel;
		
		return View::make("pmarf/assignment_of_request", $data);
	}
	
	public function npmdPersonnelProcess($pmarf_id)
	{		
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
        
		$pmamrf                         = new PromoAndMerchandising;
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;
			}
			$data["init"] = $init;
		}
		
        if(Input::get('action') == 'send'){
			$this->PMARF->npmdPerAction($pmarf_id);
			return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR ACCEPTANCE');
        }
		
		if(Input::get('action') == 'return'){
			$request = new PMARF\Requests\ValidateComments;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdPerAction($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED TO FILER');
			}
			else {
				return $is_valid;
			}
		}
		
        $data["base_url"] = URL::to("/");
        $data["request"] = $request;
        return View::make("pmarf/for_acceptance_pmarf", $data);    
        
	}

	public function npmdHeadAcceptance($pmarf_id)
	{	
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
        
		$pmamrf                         = new PromoAndMerchandising;
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["supInfo"]                = json_decode($request->pmarf_supervisor_info);
		$data["depInfo"]                = json_decode($request->pmarf_depthead_info);
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;				
			}
			$data["init"] = $init;
		}
		
        if(Input::get('action') == 'accept'){
			$this->PMARF->npmdHeadAcceptance($pmarf_id);
			return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY ACCEPTED FOR EXECUTION');
        }
		
		if(Input::get('action') == 'return'){
			$request = new PMARF\Requests\Returning;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdHeadAcceptance($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED');
			}
			else {
				return $is_valid;
			}
		}
		
        $data["base_url"] = URL::to("/");
        $data["request"] = $request;
        return View::make("pmarf/npmd_acceptance_pmarf", $data);     
	}
	
	public function npmdPersonnelExecution($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
		
		$pmamrf                         = new PromoAndMerchandising;
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;
			}
			$data["init"] = $init;
		}
		
		if(Input::get('action') == 'send'){
			$request = new PMARF\Requests\CompletionDate;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdPerExecute($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR NOTATION');
			}
			else {
				return $is_valid;
			}
		}
		
		$data["base_url"] = URL::to("/");
        $data["request"] = $request;
        return View::make("pmarf/npmd_personnel_execution", $data);
	}

	public function npmdHeadNotation($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
		
		$pmamrf                         = new PromoAndMerchandising;
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;
			}
			$data["init"] = $init;
		}
		
		if(Input::get('action') == 'send'){
			$this->PMARF->npmdHeadNotate($pmarf_id);
			return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY SENT TO REQUESTOR FOR CONFORME');
        }
		
		if(Input::get('action') == 'return'){
			$request = new PMARF\Requests\ValidateComments;
			$request->validate();
			
			$is_valid = $request->validate();
			if($is_valid === true){
				$this->PMARF->npmdHeadNotate($pmarf_id);
				return Redirect::to('pmarf/submitted-pmarf-requests')->with('message', 'PMARF SUCCESSFULLY RETURNED TO CONCERNED PERSONNEL');
			}
			else {
				return $is_valid;
			}
			
        }
	
		$data["base_url"] = URL::to("/");
        $data["request"] = $request;
        return View::make("pmarf/npmd_head_notation", $data);
	}

	public function requestorConfirmation($pmarf_id)
	{
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
		
		$pmamrf                         = new PromoAndMerchandising;
		$rec                            = $pmamrf->edit_pmarf($pmarf_id);
		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["rec"]                    = $rec;
		$data["empInfo"]                = json_decode($rec->req_emp_info);
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');    
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;				
			}
			$data["init"] = $init;
		}
		
		if(Input::get('action') == 'acknowledge'){
			$this->PMARF->requestorConfirm($pmarf_id);
			return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY ACKNOWLEDGED');
        }
		
		$data["base_url"] = URL::to("/");
        $data["request"] = $request;
        return View::make("pmarf/requestor_confirm", $data);
	}

    
    public function delete_pmarf($pmarf_id)
	{
		$affectedRows = $this->PMARF->deleteRequest($pmarf_id);
        
		$data["base_url"] = URL::to("/");
		return Redirect::to('pmarf/pmarf-requests')->with('message', $affectedRows. 'PMARF SUCCESSFULLY DELETED');

    }
    
    public function view($pmarf_id)
	{	
		$pmarfRep = new PMARFRepository;
		$request = $pmarfRep->getRequest($pmarf_id);
		
			if(Input::get('action') == 'cancel'){
				$request = new PMARF\Requests\Cancel;
				$request->validate();
				
				$is_valid = $request->validate();
				if($is_valid === true){
					$this->PMARF->cancelRequest($pmarf_id);
					return Redirect::to('pmarf/pmarf-requests')->with('message', 'PMARF SUCCESSFULLY CANCELLED');
				}
				else {
					return $is_valid;
				}
			}
		
			if(Input::get('action') == 'back'){
			   return Redirect::action("PmarfController@submittedPmarfRequests");
			}
			else{
				$pmamrf                         = new PromoAndMerchandising;
				$rec                            = $pmamrf->edit_pmarf($pmarf_id);
				$data["base_url"]               = URL::to("/");
				$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
				$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
				$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
				$data["rec"]                    = $rec;
				$data["empInfo"]                = json_decode($rec->req_emp_info);
				$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
				
				$init = array();

				$b = json_decode($request->pmarf_initiated_by, true);
				if(is_array($b)){
					foreach($b as $a => $c){
						 $init[$a] = $c;
					}
					$data["init"] = $init;
				}
				$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
				$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');    
			}
			$data["base_url"] = URL::to("/");
			$data["request"] = $request;
				
		$this->PMARF->storeViewLogsPMARF($pmarf_id);
		return View::make("pmarf/view_pmarf", $data);
    }
	
	public function printPmarfRequest($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$request = $pmarf->getRequest($pmarf_id);
		
		$pmamrf                         = new PromoAndMerchandising;
		$rec                            = $pmamrf->edit_pmarf($pmarf_id);

		$data["base_url"]               = URL::to("/");
		$data["activity_type"]          = PmarfLibrary::defined_lib('activity_type');
		$data["activity_date"]          = PmarfLibrary::defined_lib('activity_date');
		$data["implementer"]            = PmarfLibrary::defined_lib('implementer');
		$data["rec"]                    = $rec;
		$data["empInfo"]                = json_decode($rec->req_emp_info);
		$data["initiated_by"]           = json_decode($request->pmarf_initiated_by);
		
		$data["supInfo"]                = json_decode($request->pmarf_supervisor_info);
		$data["depInfo"]                = json_decode($request->pmarf_depthead_info);

		
		$init = array();

		$b = json_decode($request->pmarf_initiated_by, true);
		if(is_array($b)){
			foreach($b as $a => $c){
				 $init[$a] = $c;				
			}
			$data["init"] = $init;
		}
		$data["brands"]                 = $pmamrf->rebuild_brands($request->pmarf_participating_brands);
		$data["attachments"]            = $attachments=json_decode($request->pmarf_attachments,'array');
				
		$data["base_url"] = URL::to("/");
		$data["request"] = $request;
		
		return View::make("pmarf/view_closed_pmarf", $data);
	}
	
    // public function send_pmarf($pmarf_id){
        // $pmamrf = new PromoAndMerchandising;
        // $pmamrf->send_request($pmarf_id,'approval');

        // $data["base_url"] = URL::to("/");
        // return View::make("pmarf/save_pmarf", $data);
    // }
    
	/* AJAX */
	public function MyPmarf()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getMyRequests();
		
		return json_encode($requests);
	}
	
	public function ImmSupPmarf()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getIsRequests();
		
		return json_encode($requests);
	}	
	
	public function ApproverClosedReq()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getClosedRequests();
		
		return json_encode($requests);
	}	
	
	public function ApproverClosedReqDh()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getClosedRequestsDh();
		
		return json_encode($requests);
	}	
	
	public function HeadofReq()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getDhRequests();
		
		return json_encode($requests);
	}
	
	public function NmpdHeadReq()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getNpmdHeadRequests();
		
		return json_encode($requests);
	}

	public function NmpdHeadReq_limit()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getNpmdHeadRequests_limit();
		
		return json_encode($requests);
	}
	
	//all pmarf requests
	public function AllPmarfReq()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getAllPmarfRequests();
		
		return json_encode($requests);
	}
	
	public function NmpdPersonnelReq()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getNpmdPersonnelRequests();
		
		return json_encode($requests);
	}

	public function NmpdPersonnelReq_limit()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getNpmdPersonnelRequests_limit();
		
		return json_encode($requests);
	}
	
	public function PmarfFilerDashboard()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getPmarfFilerDashboard();
		
		return json_encode($requests);
	}
	
	public function PmarfApproverDashboard()
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getPmarfApproverDashboard();
		
		return json_encode($requests);
	}
	/* End of AJAX */
	

}