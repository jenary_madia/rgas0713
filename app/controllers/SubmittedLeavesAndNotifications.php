<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 22/08/2016
 * Time: 9:43 AM
 */
use RGAS\Libraries;
use RGAS\Modules\LRF;
use RGAS\Modules\NS;


class SubmittedLeavesAndNotifications extends \BaseController
{
    private $leaves;
    private $notifications;
    private $allNotifs;
    private $allNotifBatch;
    private $allLeaveBatch;
    private $notificationBatch;
    private $leaveBatch;
    private $undertime = array();
    private $offset = array();
    private $timekeep = array();
    private $official = array();
    private $cuttime = array();
    private $LRF;

    public function __construct()
    {
        $this->notifications = new Notifications();
        $this->notificationBatch = new NotificationBatch();
        $this->leaves = new Leaves;
        $this->LRF = new LRF\LRF;
        $this->NS = new NS\NS;
        $this->leaveBatch = new LeaveBatch();
        $this->allNotifs = $this->notifications->getNotif();
        $this->allLeaveBatch = $this->leaveBatch->getLeaves();
        $this->allNotifBatch = $this->notificationBatch->getNotif();
        foreach ($this->allNotifs as $key)
        {
            if($key->noti_type == 'undertime'){
                array_push($this->undertime,$key);
            }elseif($key->noti_type == 'offset'){
                array_push($this->offset,$key);
            }elseif($key->noti_type == 'TKCorction'){
                array_push($this->timekeep,$key);
            }elseif($key->noti_type == 'official'){
                array_push($this->official,$key);
            }elseif($key->noti_type == 'cut_time'){
                array_push($this->cuttime,$key);
            }

        }
        
    }

    public function index() 
    {
        $data['departments'] = DB::table('departments')
        ->leftjoin('leaves', 'leaves.department', '=', 'departments.dept_name')
        ->leftjoin('notifications', 'notifications.department', '=', 'departments.dept_name')
        ->select("departments.dept_name as dept_name","departments.id as dept_id")
        ->where("leaves.status","APPROVED")
        ->lists('dept_name','dept_name');
        $data['departments']['all'] = 'ALL';
        $data["base_url"] = URL::to("/");
        $data["leaveTypes"] = SelectItems::where("module","lrf")
            ->where("modulesfield","leaveType")->get();
        $data["notificationTypes"] = SelectItems::where("module","notif")
            ->where("modulesfield","notifType")->get();

        return View::make("submitted_leaves_notifications.list",$data);
    }

    public function getLeaves()
    {
        return $this->leaves->submittedToReceiver();
    }

    public function getNotifUndertime()
    {
        $notif = $this->undertime;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {
                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $result_data["aaData"][$ctr][] = $req->datecreated;
                $result_data["aaData"][$ctr][] = $req->dateapproved;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = $req->from_date;
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;
    }

    public function getNotifOffset()
    {
        $notif = $this->offset;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {
                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $result_data["aaData"][$ctr][] = $req->datecreated;
                $result_data["aaData"][$ctr][] = $req->dateapproved;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = $req->duration_type;
                $result_data["aaData"][$ctr][] = ($req->duration_type == 'multiple' ? $req->from_date.' TO '.$req->to_date : $req->from_date);
                $result_data["aaData"][$ctr][] = $req->totaldays;
                $result_data["aaData"][$ctr][] = $req->totalhours;
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;
    }

    public function getNotifTimekeep()
    {
        $notif = $this->timekeep;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {

                if ($req->to_correct == 'both'){
                    $inOut =  $req->from_inout.'/'.$req->to_inout;
                }elseif ($req->to_correct == 'no_time_in'){
                    $inOut =  $req->from_inout;
                }elseif ($req->to_correct == 'no_time_out'){
                    $inOut =  $req->to_inout;
                }else{
                    $inOut = '------';
                }

                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $result_data["aaData"][$ctr][] = $req->datecreated;
                $result_data["aaData"][$ctr][] = $req->dateapproved;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = $req->from_date;
                $result_data["aaData"][$ctr][] = $inOut;
                $result_data["aaData"][$ctr][] = ($req->salary_adjustment ? "SALARY ADJUSTMENT" : "");
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;
    }

    public function getNotifOfficial()
    {
        $notif = $this->official;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {
                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $result_data["aaData"][$ctr][] = $req->datecreated;
                $result_data["aaData"][$ctr][] = $req->dateapproved;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = $req->duration_type;
                $result_data["aaData"][$ctr][] = ($req->duration_type == 'multiple' ? $req->from_date.' TO '.$req->to_date : $req->from_date);
                $result_data["aaData"][$ctr][] = $req->totaldays;
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;
    }

    public function getNotifCuttime()
    {
        $notif = $this->cuttime;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {
                $result_data["aaData"][$ctr][] = $req->documentcode.'-'.$req->codenumber;
                $result_data["aaData"][$ctr][] = $req->datecreated;
                $result_data["aaData"][$ctr][] = $req->dateapproved;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = $req->totaldays;
                $result_data["aaData"][$ctr][] = $req->totalhours;
                $result_data["aaData"][$ctr][] = $req->from_date.' TO '.$req->to_date;
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;
    }
    
    public function search() {
        $result = [];
        if(Input::has("module")) {
            if(in_array('leave',Input::get("module"))) {
                $result['leave'] = $this->leaves->filterLeaveForReceiver();
            }else{
                $result['leave'] = [];
            }

            if(in_array('notification',Input::get("module"))) {
                $result['notification'] = $this->notifications->filterNotifForReceiver();
            }else{
                $result['notification'] = [];
            }
        }
        return $result;
    }

    public function printExcel() {

        include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');

        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $start_row = 1;
        if(Input::has("module")) {
            if(in_array('leave',Input::get("module"))) {

                $objPHPExcel->getActiveSheet()->setCellValue("A1", 'LEAVES');
                $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'DATE OF LEAVE');
                $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'TOTAL LEAVE DAYS');
                $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'REASON');

                $objPHPExcel->getActiveSheet()->getStyle("A$start_row:G$start_row")->getFont()->setBold(true);

                $requests = $this->leaves->filterLeaveForReceiver();
                foreach ($requests as $req) {
                    $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req[6]);
                }

            }

            if(in_array('notification',Input::get("module"))) {
                $notifications = $this->notifications->filterNotifForReceiver();

                if(count($notifications["TKCorction"]) > 0) {
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'NOTIFICATION (Time Keeping Correction)');
                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row")->getFont()->setBold(true);
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'DATE OF TIME KEEPING CORRECTION');
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'TIME IN/TIME OUT');
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'SALARY ADJUSTMENT');
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'REASON');

                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row:H$start_row")->getFont()->setBold(true);

                    foreach ($notifications["TKCorction"] as $req) {
                        $start_row++;
                        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req[6]);
                        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req[7]);
                    }
                }

                if(count($notifications["cut_time"]) > 0) {
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'NOTIFICATION (Cut Time)');
                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row")->getFont()->setBold(true);
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'TOTAL NUMBER OF DAYS');
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'TOTAL NUMBER OF HOURS');
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'DATE OF CUT TIME');
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'REASON');

                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row:H$start_row")->getFont()->setBold(true);

                    foreach ($notifications["cut_time"] as $req) {
                        $start_row++;
                        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req[6]);
                        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req[7]);
                    }
                }

                if(count($notifications["official"]) > 0 ) {
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'NOTIFICATION (Official Business)');
                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row")->getFont()->setBold(true);
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'OB DURATION');
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'OB DATE');
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'TOTAL OB DAYS');
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'REASON');

                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row:H$start_row")->getFont()->setBold(true);

                    foreach ($notifications["official"] as $req) {
                        $start_row++;
                        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req[6]);
                        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req[7]);
                    }
                }



                if(count($notifications["undertime"]) > 0) {
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'NOTIFICATION (Undertime)');
                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row")->getFont()->setBold(true);
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'DATE OF UNDERTIME');
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'REASON');

                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row:F$start_row")->getFont()->setBold(true);

                    foreach ($notifications["undertime"] as $req) {
                        $start_row++;
                        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                    }
                }

                if(count($notifications["offset"]) > 0) {
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'NOTIFICATION (Offset)');
                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row")->getFont()->setBold(true);
                    $start_row ++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'REFERENCE NUMBER');
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE FILED');
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DATE APPROVED');
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'EMPLOYEE NAME');
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'OFFSET DURATION');
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'OFFSET DATE');
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'TOTAL OFFSET DAYS');
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'TOTAL OFFSET HOURS');
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'REASON');

                    $objPHPExcel->getActiveSheet()->getStyle("A$start_row:I$start_row")->getFont()->setBold(true);

                    foreach ($notifications["offset"] as $req) {
                        $start_row++;
                        $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req[0]);
                        $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req[1]);
                        $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req[2]);
                        $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req[3]);
                        $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req[4]);
                        $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req[5]);
                        $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req[6]);
                        $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req[7]);
                        $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req[8]);
                    }
                }
            }

            //WRITE AND DOWNLOAD EXCEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();

            $prnt_tstamp = date('YmdHis',time());
            $user = Session::get("employee_id");
            $fname = "submitted_leaves_notifications_$user" . "_$prnt_tstamp";
            header('Content-type: application/vnd.ms-excel');
            header("Content-Disposition: attachment; filename=$fname.xls");
            return $objWriter->save('php://output');
            $objPHPExcel->disconnectWorksheets();
        }

        return Redirect::back()
            ->with("errorMessage","Choose atleast one module");
    }

    public function leaveView($id,$method) {

        $data["base_url"] = URL::to("/");
        $data["leaveDetails"] = Leaves::getLeave($id,2);
        if (! $data["leaveDetails"]) {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }
        $data["method"] = $method;
        $data["leaveSignatories"] = Leaves::find($id)->signatories()->with('employee')->get();
        return View::make("submitted_leaves_notifications.leave",$data);
    }

    public function notifView($id,$method) {
        $data["base_url"] = URL::to("/");
        $data["notifDetails"] = Notifications::getNotifications($id,2);
        $data["notifAdditionalDetails"] = NotificationDetails::where("notificationid",$id)->get();
        $details_total = [];
        foreach ($data["notifAdditionalDetails"] as $key) {
            array_push($details_total,$key['total_time']);
        }

        $data['total'] = array_sum($details_total);
        if (! $data["notifDetails"])
        {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }
        $data["method"] = $method;
        $data["notifSignatories"] = Notifications::find($id)->signatories()->with('employee')->get();
        return View::make("submitted_leaves_notifications.notif",$data);
    }

    public function leaveAction($id)
    {
        if(Input::get("action") == "return") {
            $message = "Please indicate reason for disapproval.";
        }else{
            $message = "Please indicate reason for cancellation.";
        }
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => $message,
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }
        $leave = Leaves::where("curr_emp", Session::get("employee_id"))
            ->with('signatories')
            ->where("id",$id)->first();
        if (! $leave)
        {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }

        $leave->signatories()->delete();
        $files = Input::get("files");
        $fm = new Libraries\FileManager;
        if($files) {
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->LRF->getStoragePath() . $leave->documentcode . '-' . $leave->codenumber);
                }
            }
            $leave->receiver_attachments = json_encode($files);
        }

        $leave->curr_emp = 0;
        $leave->isdeleted = 0;
        $leave->status = (Input::get("action") == "return" ?  "RETURNED" : "CANCELLED" );
        $leave->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
            ."')");
        $save = $leave->save();
        if (! $save)
        {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'Something went wrong');
        }else{
            return Redirect::to('submitted/leaves_notifications')
                ->with('successMessage', (Input::get("action") == "return" ?  'Leave successfully returned to the filer' : 'Leave successfully cancelled' ));

        }
    }

    public function notifAction($id)
    {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $notif = Notifications::getNotifications($id,2);
        if (! $notif)
        {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }
        $files = Input::get("files");
        $fm = new Libraries\FileManager;
        if($files) {
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->NS->getStoragePath() . $notif->documentcode . '-' . $notif->codenumber);
                }
            }
            $notif->receiver_attachments = json_encode($files);
        }
        $notif->curr_emp = 0;
        $notif->status = (Input::get("action") == "return" ?  "RETURNED" : "CANCELLED" );
        $notif->isdeleted = 0;
        $notif->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
            ."')");
        $save = $notif->save();
        if (! $save)
        {
            return Redirect::to('submitted/leaves_notifications')
                ->with('errorMessage', 'Something went wrong');
        }else{
            return Redirect::to('submitted/leaves_notifications')
                ->with('successMessage', (Input::get("action") == "return" ?  'Notification successfully returned to the Filer.' : 'Notification successfully cancelled.' ));

        }
    }

//    Production Encoders

    public function PEIndex() 
    {
        $data["forPrint"] = null;
        if (Input::old()) {
            $data["forPrint"] = (new CIEncrypt)->encode(json_encode(array(Input::old())));
        }
        $data['departments'] = DB::table('departments')
        ->leftjoin('leave_batch', 'leave_batch.departmentid', '=', 'departments.id')
        ->leftjoin('notification_batch', 'notification_batch.departmentid', '=', 'departments.id')
        ->selectRaw("distinct(departments.dept_name) as dept_name,departments.id as dept_id")
        ->where("leave_batch.status","APPROVED")
        ->lists('dept_name','dept_id');
        $data['departments'][0] = 'ALL';

        $data['notificationType'] = [];
        $selectItems = (new SelectItems)->getItems('notif','notifType');
        foreach ($selectItems as $key) $data['notificationType'][$key->item] = $key->text;

        $data["base_url"] = URL::to("/");

        return View::make("submitted_leaves_notifications.pe_list",$data);
    }

    public function PEGetLeaves() {

        $leaves = $this->allLeaveBatch;

        $result_data["iTotalDisplayRecords"] = count($leaves);
        $result_data["iTotalRecords"] = count($leaves);
        if ( count($leaves) > 0){
            $ctr = 0;
            foreach($leaves as $req) {
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['dateapproved'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['middlename'].' '.$req['owner']['lastname'] ;
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/pe_leave/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/pe_leave/'.$req['id'].'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $leaves;
        }
        return $result_data;

    }

    public function PEGetNotifications() {

        $notif = $this->allNotifBatch;
        $result_data["iTotalDisplayRecords"] = count($notif);
        $result_data["iTotalRecords"] = count($notif);
        if ( count($notif) > 0){
            $ctr = 0;
            foreach($notif as $req) {
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['dateapproved'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data["aaData"][$ctr][] = $req['owner']['firstname'].' '.$req['owner']['middlename'].' '.$req['owner']['lastname'];
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/pe_notification/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/pe_notification/'.$req['id'].'/process').">Process</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $notif;
        }
        return $result_data;

    }
    
    public function PESearch() {

        if (Request::ajax()) {
            $result = [];
            if(Input::has("application")) {
                if (in_array('leaves', Input::get("application"))) {
                    $result['leaves'] = $this->leaveBatch->searchPEReceiver();
                }

                if (in_array('notifications', Input::get("application"))) {
                    $result['notifications'] = $this->notificationBatch->searchPEReceiver();
                }
            }
            return $result;
        }

    }

    public function PEPrintExcel() {
        $result = [];
        if(Input::has("application")) {
            if (in_array('leaves', Input::get("application"))) {
                $result['leaves'] = $this->leaveBatch->printPEReceiver();
            }

            if (in_array('notifications', Input::get("application"))) {
                $result['notifications'] = $this->notificationBatch->printPEReceiver();
            }
        }

        if(empty($result)) {
            return Redirect::back()
                ->with('errorMessage', 'Choose atleast one module');
        }
        include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
        $objPHPExcel = new PHPExcel();
        if(array_key_exists('leaves',$result)) {
            $objPHPExcel->getActiveSheet()->setTitle("Leaves");
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 1;
            foreach(range('A','F') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->setCellValue("A1", 'LEAVES (PRODUCTION ENCODERS)');
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'DATE FILED');
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE APPROVED');
            $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DEPARTMENT');
            $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'SECTION');
            $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'EMPLOYEE NAME');
            $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'EMPLOYEE NUMBER');
            $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'LEAVE TYPE');
            $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'DURATION OF LEAVE');
            $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'TOTAL LEAVE DAYS');
            $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'REASON');

            $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);

        

            foreach ($result['leaves'] as $req) {
                foreach ($req['batch_details'] as $key) {
                    $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req['datecreated']);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req['dateapproved']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req['department']['dept_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req['section']['sect_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $key['employee_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $key['employee_number']);
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $key['leave_item']['text']);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", ($key['duration'] == "multiple" ? $key['from_date'].' to '.$key['to_date'] : $key['from_date']));
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $key['noofdays']);
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $key['reason']);
                }
            }

        }
        if(array_key_exists('notifications',$result)) {
            $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Notifications');

            // Attach the "My Data" worksheet as the first worksheet in the PHPExcel object
            $objPHPExcel->addSheet($myWorkSheet, 1);
            $objPHPExcel->setActiveSheetIndex(1);
            $start_row = 1;
            foreach(range('A','K') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            $objPHPExcel->getActiveSheet()->setCellValue("A1", 'NOTIFICATIONS (PRODUCTION ENCODERS)');
            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'DATE FILED');
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'DATE APPROVED');
            $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'DEPARTMENT');
            $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'SECTION');
            $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'EMPLOYEE NAME');
            $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'EMPLOYEE NUMBER');
            $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'NOTIFICATION TYPE');
            $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'NOTIFICATION DATE');
            $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'TIME IN');
            $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'TIME OUT');
            $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", 'REASON');

            $objPHPExcel->getActiveSheet()->getStyle('A1:F1')->getFont()->setBold(true);

        

            foreach ($result['notifications'] as $req) {
                foreach ($req['batch_details'] as $key) {
                    $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req['datecreated']);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req['dateapproved']);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req['department']['dept_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req['section']['sect_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $key['employee_name']);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $key['employee_number']);
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $key['notif']['text']);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $key['from_date']);
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $key['from_in']);
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $key['from_out']);
                    $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", $key['reason']);
                }
            }
        }

        $objPHPExcel->setActiveSheetIndex(0);
        //WRITE AND DOWNLOAD EXCEL FILE
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        ob_end_clean();

        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get("employee_id");
        $fname = "submitted_leaves_notifications_$user" . "_$prnt_tstamp";
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=$fname.xls");
        return $objWriter->save('php://output');
    }

    public function PELeaveView($id,$method){
        $data['method'] = $method;
        $data['leaveBatch'] = LeaveBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('curr_emp',Session::get("employee_id"))
            ->whereIn('status',["APPROVED","APPROVED/DELETED"])
            ->first();
        if (! $data["leaveBatch"]) {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid leave');
        }
        if (Request::ajax())
        {
            $parsedData= array();
            for ($i=0; $i < count($data['leaveBatch']['batchDetails']); $i++) {
                $parsedData[$i]["employeeID"] =  $data['leaveBatch']['batchDetails'][$i]['employee_id'];
                $parsedData[$i]["employeeName"] =  $data['leaveBatch']['batchDetails'][$i]['employee_name'];
                $parsedData[$i]["employeeNumber"] =  $data['leaveBatch']['batchDetails'][$i]['employee_number'];
                $parsedData[$i]["leaveType"] =  $data['leaveBatch']['batchDetails'][$i]['appliedfor'];
                $parsedData[$i]["leaveName"] =  $data['leaveBatch']['batchDetails'][$i]['leave_item']['text'];
                $parsedData[$i]["reason"] =  $data['leaveBatch']['batchDetails'][$i]['reason'];
                $parsedData[$i]["fromDate"] =  $data['leaveBatch']['batchDetails'][$i]['from_date'];
                $parsedData[$i]["fromPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['from_duration'];
                $parsedData[$i]["toDate"] =  $data['leaveBatch']['batchDetails'][$i]['to_date'];
                $parsedData[$i]["toPeriod"] =  $data['leaveBatch']['batchDetails'][$i]['to_duration'];
                $parsedData[$i]["totalDays"] =  $data['leaveBatch']['batchDetails'][$i]['noofdays'];
                $parsedData[$i]["duration"] =  $data['leaveBatch']['batchDetails'][$i]['duration'];
                $parsedData[$i]['active'] = false;
                $parsedData[$i]['noted'] = false;
            }
            return $parsedData;
        }
        $data["base_url"] = URL::to("/");
        return View::make("submitted_leaves_notifications.pe_leave", $data);
    }

    public function PENotifView($id,$method){
        $data['method'] = $method;
        $data['notification'] = NotificationBatch::where("id", $id)
            ->with('batchDetails')
            ->with('department')
            ->with('section')
            ->where('curr_emp',Session::get("employee_id"))
            ->whereIn('status',["APPROVED","APPROVED/DELETE"])
            ->first();
        if (! $data["notification"]) {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid request');
        }
        if (Request::ajax())
        {
            return $data['notification']['batchDetails'];
        }

        $data["base_url"] = URL::to("/");
        return View::make("submitted_leaves_notifications.pe_notif", $data);
    }

    public function PELeaveAction($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => (Input::get("action") == "return" ?  "Please indicate reason for disapproval." : "Please indicate reason for cancellation." ),
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $leave = LeaveBatch::where("id", $id)
            ->where('curr_emp',Session::get("employee_id"))
            ->whereIn('status',["APPROVED","APPROVED/DELETED"])
            ->first();

        if (! $leave)
        {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid request');
        }
        $leave->curr_emp = 0;
        $leave->status = (Input::get("action") == "return" ?  "RETURNED" : "CANCELLED" );
        $leave->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
            ."')");
        $save = $leave->save();
        if (! $save)
        {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'Something went wrong');
        }else{
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('successMessage', (Input::get("action") == "return" ?  'Leave request successfully returned to Filer.' : 'Leave request successfully cancelled.' ));

        }
    }
    public function PENotifAction($id) {

        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }
        $notif = NotificationBatch::where("id", $id)
            ->where('curr_emp',Session::get("employee_id"))
            ->whereIn('status',["APPROVED","APPROVED/DELETED"])
            ->first();

        if (! $notif)
        {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'You\'ve looking for invalid notification');
        }
        $notif->curr_emp = 0;
        $notif->status = (Input::get("action") == "return" ?  "RETURNED" : "CANCELLED" );
        $notif->comment = DB::raw("concat(comment,'|','".
            json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ))
            ."')");
        $save = $notif->save();
        if (! $save)
        {
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('errorMessage', 'Something went wrong');
        }else{
            return Redirect::to('submitted/pe_leaves_notifications')
                ->with('successMessage', (Input::get("action") == "return" ?  'Notification successfully returned to the filer' : 'Notification successfully cancelled' ));

        }
    }
}












