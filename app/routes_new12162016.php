<?php
use RGAS\Libraries;

/*
|--------------------------------------------------------------------------
| Application Routes
|------------------------	--------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(array('before' => 'auth'), function(){

	Route::resource('file-uploader', 'FileUploaderController');

	//--
	Route::group(array('prefix' => 'cbr'), function()
	{
		Route::get('create', 'CbrController@create');
		Route::post('create', 'CbrController@create');
		Route::get('submitted-cbr-requests', 'CbrController@SubmittedCBRRequests');
		// Called via Ajax
		Route::get('requests-dashboard-filer', 'CbrController@RequestsDashboardFiler');
		Route::get('requests-dashboard-staff', 'CbrController@RequestsDashboardStaff');
		
		Route::get('requests-new', 'CbrController@RequestsNew');
		Route::post('requests-new', 'CbrController@RequestsNew');
		
		Route::get('requests-my', 'CbrController@RequestsMy');
		Route::post('requests-my', 'CbrController@RequestsMy');
		
		Route::get('requests-in-process', 'CbrController@RequestsInProcess');
		Route::post('requests-in-process', 'CbrController@RequestsInProcess');
		
		Route::get('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		Route::post('requests-acknowledged', 'CbrController@RequestsAcknowledged');
		//--
		// Request has been saved as draft
		Route::get('draft/view/{cb_ref_num}', 'CbrController@draftView');
		Route::get('draft/edit/{cb_ref_num}', 'CbrController@draftEdit');
		Route::post('draft/edit/{cb_ref_num}', 'CbrController@draftEdit');
		//--
		
		// Route::get('requests-new', 'CbrController@getRequestsNew');
		// Route::post('requests-new', 'CbrController@postRequestsNew');
		Route::get('view/{cb_id}', 'CbrController@view');
		
		Route::get('review-request/{cb_ref_num}/{view?}', 'CbrController@reviewRequest');
		Route::post('review-request/{cb_ref_num}', 'CbrController@reviewRequest');	
		Route::get('process/{cb_ref_num}', 'CbrController@process');
		Route::post('process/{cb_ref_num}', 'CbrController@forAcknowledgement');
		// Route::post('for-acknowledgement/{cbrd_ref_num}', 'CbrController@forAcknowledgement');
		
		Route::get('returned/view/{cb_id}', 'CbrController@returnedView');
		Route::get('returned/edit/{cb_id}', 'CbrController@returnedEdit');
		Route::post('returned/edit/{cb_id}', 'CbrController@returnedEdit');
		Route::get('received-transaction', 'CbrController@received_cbr_transaction');
		Route::post('acknowledged', 'CbrController@acknowledged');
		Route::get('delete/{cbrd_ref_num}', 'CbrController@delete');
		
		Route::get('download/{cb_ref_num}/{random_filename}/{original_filename}', 'CbrController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	});
	
	/* Training Endorsement*/
	Route::group(array('prefix' => 'te'), function()
	{
		Route::get('create', 'TrainingEndorsementController@create');
		Route::post('create', 'TrainingEndorsementController@action');
		Route::get('submitted-te-requests', 'TrainingEndorsementController@SubmittedTERequests');
		//Called via AJAX
		Route::get('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		Route::post('endorsements-new', 'TrainingEndorsementController@EndorsementsNew');
		
		Route::get('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		Route::post('endorsements-my', 'TrainingEndorsementController@EndorsementsMy');
		
		Route::get('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		Route::post('endorsements_chrd_vp_approval', 'TrainingEndorsementController@EndorsementsChrdVpApproval');
		
		Route::get('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		Route::post('endorsements_svp_for_cs_approval', 'TrainingEndorsementController@EndorsementsSvpCsApproval');
		
		Route::get('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		Route::post('endorsements_pres_approval', 'TrainingEndorsementController@EndorsementsPresApproval');
		
		Route::get('filer_dashboard', 'TrainingEndorsementController@FilerDashboard');
		Route::get('approver_dashboard', 'TrainingEndorsementController@ApproverDashboard');
		//Requestor
		Route::get('view/{te_id}', 'TrainingEndorsementController@view');
		Route::get('view-approved/{te_id}', 'TrainingEndorsementController@view_approved');
		Route::get('edit/{te_id}', 'TrainingEndorsementController@edit');
		Route::post('edit/{te_id}', 'TrainingEndorsementController@edit');
		Route::get('delete/{te_id}', 'TrainingEndorsementController@delete');
		
		//CHRD
		Route::get('assess/{te_id}', 'TrainingEndorsementController@assessChrd');
		Route::post('assess/{te_id}', 'TrainingEndorsementController@assessChrd');
		
		//vphr,svp,pres approval		
		Route::get('view-for-approval/{te_id}', 'TrainingEndorsementController@viewForApproval');
		Route::get('process-for-approval/{te_id}', 'TrainingEndorsementController@processForApproval');
		Route::post('process-for-approval/{te_id}', 'TrainingEndorsementController@processForApproval');
		
		Route::get('download/{te_ref_num}/{random_filename}/{original_filename}', 'TrainingEndorsementController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
		Route::get('print/{te_id}', 'TrainingEndorsementController@printFile');
		Route::get('download/{te_id}', 'TrainingEndorsementController@downloadFile');
		
		Route::get('print-request/{te_id}', 'TrainingEndorsementController@printRequest');
		
		Route::get('departments/for_chrd', function(){
			// 1
			// $departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			// 2
			$departments = TrainingEndorsements::where('te_level','=','CHRD-TOD')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_vp', function(){
			// 1
			// $departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			// 2
			$departments = TrainingEndorsements::where('te_level','=','VPforCHR')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_svp', function(){
			// 1
			// $departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			// 2
			$departments = TrainingEndorsements::where('te_level','=','SVPforCS')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});

		Route::get('departments/for_pres', function(){
			// 1
			// $departments = Departments::get_departments()->sortBy('dept_name')->groupBy('dept_name')->toJson();
			
			// 2
			$departments = TrainingEndorsements::where('te_level','=','PRES')
					->where("te_isdeleted","=",0)
					->groupBy('te_department')
					->orderBy('te_department','asc')
					->get();	
			
			echo $departments;
		});


	});
	
	
	

	// /* PMARF */
	Route::group(array('prefix' => 'pmarf'), function()
	{
		Route::get('npmd_execution_pmarf', 'PmarfController@npmd_execution_pmarf');
		/*Newly Added by Lucille*/
		
		Route::get('create', 'PmarfController@create');
		Route::get('view/my-requests/{pmarf_id}', 'PmarfController@view_my_request');
		Route::get('submitted-pmarf-requests', 'PmarfController@submittedPmarfRequests');

		Route::get('approve/{pmarf_id}', 'PmarfController@immSupApprove');
		Route::post('approve/{pmarf_id}', 'PmarfController@immSupApprove');

		Route::get('dept-head-approve/{pmarf_id}', 'PmarfController@dhApprove');
		Route::post('dept-head-approve/{pmarf_id}', 'PmarfController@dhApprove');
		
		Route::get('npmd-head-approve/{pmarf_id}', 'PmarfController@npmdHeadAssigning');
		Route::post('npmd-head-approve/{pmarf_id}', 'PmarfController@npmdHeadAssigning');
		
		Route::get('npmd-personnel-process/{pmarf_id}', 'PmarfController@npmdPersonnelProcess');
		Route::post('npmd-personnel-process/{pmarf_id}', 'PmarfController@npmdPersonnelProcess');
		
		Route::get('npmd-head-acceptance/{pmarf_id}', 'PmarfController@npmdHeadAcceptance');
		Route::post('npmd-head-acceptance/{pmarf_id}', 'PmarfController@npmdHeadAcceptance');
		
		Route::get('npmd-personnel-execution/{pmarf_id}', 'PmarfController@npmdPersonnelExecution');
		Route::post('npmd-personnel-execution/{pmarf_id}', 'PmarfController@npmdPersonnelExecution');
		
		Route::get('npmd-head-notation/{pmarf_id}', 'PmarfController@npmdHeadNotation');
		Route::post('npmd-head-notation/{pmarf_id}', 'PmarfController@npmdHeadNotation');
		
		Route::get('confirmation/{pmarf_id}', 'PmarfController@requestorConfirmation');
		Route::post('confirmation/{pmarf_id}', 'PmarfController@requestorConfirmation');
		
		Route::get('pmarf-filer-requests', 'PmarfController@PmarfFilerDashboard');
		Route::get('pmarf-approver-requests', 'PmarfController@PmarfApproverDashboard');
		// Route::get('approver_dashboard', 'PmarfController@PmarfApproverDashboard');
		
		
		/*AJAX*/
		Route::get('my-pmarf-requests', 'PmarfController@MyPmarf');
		Route::post('my-pmarf-requests', 'PmarfController@MyPmarf');
		
		Route::get('for-approval-requests', 'PmarfController@ImmSupPmarf');
		Route::post('for-approval-requests', 'PmarfController@ImmSupPmarf');
		
		Route::get('for-head-approval-requests', 'PmarfController@HeadofReq');
		Route::post('for-head-approval-requests', 'PmarfController@HeadofReq');
		
		Route::get('npmd-head-requests', 'PmarfController@NmpdHeadReq');
		Route::post('npmd-head-requests', 'PmarfController@NmpdHeadReq');
		
		Route::get('npmd-personnel-requests', 'PmarfController@NmpdPersonnelReq');
		Route::post('npmd-personnel-requests', 'PmarfController@NmpdPersonnelReq');
		/*End of AJAX*/
		
		/*End of Newly Added*/
		
		
		//Route::get('create', 'PmarfController@create_pmarf');
		Route::post('create', 'PmarfController@action');
		Route::get('view_save', 'PmarfController@view_pmarf');
		Route::post('view_save', 'PmarfController@view_pmarf');
		
		/*Called via ajax*/
		Route::get('save_pmarf', 'PmarfController@get_my_save_pmarf');
		Route::post('save_pmarf', 'PmarfController@get_my_save_pmarf');
		/*End of Called via ajax*/
	
		Route::get('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		Route::post('edit/{pmarf_id}', 'PmarfController@edit_pmarf');
		
		Route::get('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		Route::post('delete/{pmarf_id}', 'PmarfController@delete_pmarf');
		
		Route::get('send/{pmarf_id}', 'PmarfController@send_pmarf');
		Route::post('send/{pmarf_id}', 'PmarfController@send_pmarf');
		
		Route::get('view/{pmarf_id}', 'PmarfController@view');
		Route::post('view/{pmarf_id}', 'PmarfController@view');
		
		Route::get('download/{pmarf_ref_num}/{random_filename}/{original_filename}', 'PmarfController@download')->where('random_filename', '[A-Za-z0-9\-\_\.]+');
	});
	
	Route::get('employees/list', 'EmployeesController@employee_lists');
	Route::post('employees/get', 'EmployeesController@get_all_employees');
	// Route::get('notice', 'HomeController@notice_page');
	Route::get('hierarchyupdate', 'HomeController@update_hierarchy');

	
	/* Performance Management */
	Route::group(array('prefix' => 'pmf'), function()
	{
		Route::get('create', 'PmfController@create');
		Route::post('create', 'PmfController@action');
	});
	
	/* Clearance Certification */
	Route::group(array('prefix' => 'cc'), function()
	{
		Route::get('create', 'ClearanceCertificationController@create');
		Route::get('review_clearance', 'ClearanceCertificationController@review_clearance');
	});
		Route::get('index', 'CCController@index');
	//--
	
	/* Notice To Explain */
	Route::group(array('prefix' => 'nte'), function()
	{
		Route::get('index', 'NTEController@index');
	});
	//--
	
	/* Online Attendance Monitoring */
	Route::group(array('prefix' => 'oam'), function()
	{
		Route::get('index', 'OAMController@index');
	});
	//--
	
	/* Leave Request Form */
	Route::group(array('prefix' => 'lrf'), function()
	{
		Route::get('index', 'LRFController@index');
	});
	//--
	
	Route::get('sign-out', 'LoginController@sign_out');
	Route::post('sign-out2', 'LoginController@sign_out2');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|------------------------	--------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('employees_by_department/{dep_id}', function($dep_id){
	$employees = Employees::get_all($dep_id)->toJson();
	echo $employees;
});

Route::resource('/', 'HomeController');
/* Login */
Route::group(array('before' => 'guest'), function() {
	Route::get('login', 'LoginController@index');
	Route::post('sign-in', 'LoginController@sign_in');
	Route::post('sign-in2', 'LoginController@sign_in2');	
});
	







