@extends('template/header')

@section('content')

<div id="wrap">
 
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">
               
    <div class="">   
           
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company1 form-control" name="" >
                    <option value="">All</option>
                    @foreach($company_arr as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    

        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department1 form-control" name="" >
                    <option value="">All</option>
                    @foreach($department as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <table id="myrecord1" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="8">SUBMITTED OFFICE SALES (EMPLOYEE)</th></tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record_employee as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["trans_date"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["department"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

    <div class="clear_20"></div>   

    <div class="">   
           @if(Session::get("is_office_receiver") != "BUK" && Session::get("is_office_receiver") ) 
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company2 form-control" name="" >
                    <option value="">All</option>
                    @foreach($company_arr_aff as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        

         <div class="clear_10"></div>    

        <div class="text-right">
            <a href="#" class="btn btn-default" data-toggle="modal" data-target="#office-modal"  >PRINT SUMMARY</a>
        </div>
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder" style="overflow-y:hidden;overflow-x:auto;" >
            <table id="myrecord2" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="8">SUBMITTED OFFICE SALES (AFFILIATE)</th></tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ORDER TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">SALES TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >OFFICE SALES AMOUNT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >PAYMENT TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >PAYMENT AMOUNT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >PAYMENT DATE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >PAYMENT REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >DELIVERY DATE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >BALANCE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >AGE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >REMARKS</th>
                    </tr>                           
                </thead>

                <tbody>
                    <?php $balance = array();?>
                     @foreach($record_affiliate as $rs)
                    <tr>
                    <td>{{ $rs["action"] }}</td> 
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["trans_date"] }}</td>  
                    <td>{{ $rs["order_type"] }}</td>  
                    <td>{{ $rs["sales_type"] }}</td>  
                    <td>{{ $rs["requestedby"] }}</td>
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["department"] }}</td>  
                    <td>{{ $rs["status"] }}</td> 
                    <td>{{ $rs["office_sales_amount"] ? number_format($rs["office_sales_amount"],2) : "" }}</td> 
                    <td>{{ $rs["payment_type"] }}</td>  
                    <td>{{ $rs["payment_amount"] ? number_format($rs["payment_amount"],2) : "" }}</td>  
                    <td>{{ $rs["payment_date"] }}</td>  
                    <td>{{ $rs["payment_ref_no"] }}</td>
                    <td>{{ $rs["delivery_date"] }}</td>  
                    <td>{{ $balance[] = $rs["office_sales_amount"] - $rs["payment_amount"] < 1 || $rs["pstats"] ? "" : number_format($rs["office_sales_amount"] - $rs["payment_amount"],2) }}</td> 
                    <td>{{  $rs["office_sales_amount"] - $rs["payment_amount"] < 1 || $rs["pstats"] ? "" : (strtotime(date("Y-m-d")) - strtotime($rs["delivery_date"]))  / 86400 }}</td>
                    <td>{{ $rs["remarks"] }}</td>                      
                    </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr role="row"><th class="" role="columnheader" rowspan="1" colspan="8">TOTAL OUTSTANDING AMOUNT: <input type="text" readonly="" value="{{ number_format(array_sum($balance),2)}}" /></th></tr>
                    
                </tfoot>
            </table>  
        </div>
        @endif

    </div>

    <div class="clear_20"></div>   
</div>
</div>


<div class="modal fade" id="office-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">OFFICE SALES ORDER AND PAYMENT SUMMARY</h4>
      </div>
      {{ Form::open(array('url' => "os/process/print", 'method' => 'post', 'files' => true)) }}
      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-3"><label class="labels text-danger">FROM: </label></div>
                <div class="col-sm-7">
                    <div class=" input-group">
                        <input  required type="text" class="date_picker form-control"  name="start_date" />
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
              </div>

              <div class="clear_10"></div>

            <div class="row">
                <div class="col-sm-3"><label class="labels text-danger">TO: </label></div>
                <div class="col-sm-7">
                    <div class=" input-group">
                        <input required type="text" class="date_picker form-control"  name="end_date" />
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>

          </div>
      </div>

      <div class="text-center  ">
        <button type="submit" class="btn btn-secondary text-center" name="action" value="GENERATE" >GENERATE</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">BACK</button>        
      </div>

      </form>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>

@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/receiver.js') }}
@stop