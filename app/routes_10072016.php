<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::resource('/', 'HomeController');
Route::get('employees/list', 'EmployeesController@employee_lists');
Route::post('employees/get', 'EmployeesController@get_all_employees');
Route::get('notice', 'HomeController@notice_page');
Route::get('hierarchyupdate', 'HomeController@update_hierarchy');


/* Login */
Route::get('login', 'LoginController@index');
Route::post('sign-in', 'LoginController@sign_in');
Route::post('sign-in2', 'LoginController@sign_in2');
Route::get('sign-out', 'LoginController@sign_out');
Route::post('sign-out2', 'LoginController@sign_out2');