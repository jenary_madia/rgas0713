<?php
namespace app\RGAS;

use Employees;

class RGASHelpers implements IRGASHelpers
{
	public function getEmployeeNameById($id)
	{
		$employee = Employees::find($id);
		return $employee['firstname']." ".$employee['lastname'];
	}
}
?>