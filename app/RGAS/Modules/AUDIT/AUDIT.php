<?php namespace RGAS\Modules\AUDIT;

use Session;
use RGAS\Libraries;
use Input;
use AuditTrails;
use Validator;

use PHPExcel;
use PHPExcel_CachedObjectStorageFactory;
use PHPExcel_Settings;
use PHPExcel_Style_Border;
use PHPExcel_Style_Alignment;
use PHPExcel_IOFactory;
use Response;
class AUDIT implements IAUDIT
{

	public function auditlist()
	{

            $input = Input::all();
            $val_data = ['from' => 'required|date',
                         'to' => 'required|date'];
            
            $validator = Validator::make($input , $val_data);
            //RUN LARAVEL VALIDATION    
            if ($validator->fails()) return ;

            include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
		
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 1;

            $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'DATE');
            $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'TIME');
            $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'EMPLOYEE NAME');
            $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", 'MODULE');
            $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", 'REFERENCE NUMBER');
            $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", 'ACTION');
            $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", 'TABLE NAME');
            $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", 'PARAMETERS');
            $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", 'OLD VALUE');
            $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", 'NEW VALUE');
            $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);        
                    
            foreach(range('A','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            
            
            $start = Input::get("from");
            $end = Input::get("to");
            $module = Input::get("module");
            $employee = Input::get("employee");
            
             $requests = AuditTrails::join('modules','modules.id','=','module_id')
                        ->join('employees','employees.id','=','user_id' )
                         ->module($module)
                        ->employee($employee)
                        ->whereBetween('date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end))] )                        
                        ->orderBy("date" , "Desc")
                        ->orderBy("time" , "desc")
                        ->get(["firstname","lastname","new","old","params","table_name","action_code","ref_num","date","time","user_id","desc"]); 
             
              foreach($requests as $req) 
                 {	
                  $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req->date);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req->time);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req->firstname . " " . $req->lastname);
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req->desc);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req->ref_num);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", AuditTrails::actions($req->action_code));
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req->table_name);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", AuditTrails::extract($req->params));
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", AuditTrails::extract($req->old));
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", AuditTrails::extract($req->new));
                    $objPHPExcel->getActiveSheet()->getStyle("H{$start_row}")->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle("I{$start_row}")->getAlignment()->setWrapText(true);
                    $objPHPExcel->getActiveSheet()->getStyle("J{$start_row}")->getAlignment()->setWrapText(true);
                 }


            //WRITE AND DOWNLOAD EXEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            ob_end_clean();

            $prnt_tstamp = date('YmdHis',time());
	        $user = Session::get("employee_id");
            $fname = "AUDIT_$user" . "_$prnt_tstamp";

            $objWriter->save("../app/storage/audit/$fname.xls");
            $objPHPExcel->disconnectWorksheets();

           // echo json_encode(array('id' => "/../app/storage/audit/$fname.xls") );

            return Response::download("../app/storage/audit/$fname.xls", "$fname.xls");
	}
        
}
?>