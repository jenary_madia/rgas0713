<?php

namespace RGAS\Modules\MOC;
use Validator;
use Input;
use Redirect;
use Response;
use URL;
//use Session;

class MOCValidator
{
    public function send($data) {
        $inputs = [
            'requestTypes' => $data['mocDetails']['requestTypes'],
            'urgency' => $data['mocDetails']['urgency'],
            'changeIn' => $data['mocDetails']['changeIn'],
            'changeInOthersInput' => $data['mocDetails']['changeInOthersInput'],
            'changeType' => $data['mocDetails']['changeType'],
            'targetDate' => $data['mocDetails']['targetDate'],
            'departmentsInvolved' => $data['mocDetails']['departmentsInvolved'],
            'title' => $data['mocDetails']['title'],
            'backgroundInfo' => $data['mocDetails']['backgroundInfo'],
//            'ssmdActionPlan' => $data['mocDetails']['ssmdActionPlan'],
        ];
        
        $rules = [
            'requestTypes' => 'required',
            'urgency' => 'required',
            'title' => 'required',
            'backgroundInfo' => 'required',
        ];

        $message = [
            'requestTypes.required' => "Type of request is required.",
            'urgency.required' => "Urgency is required.",
            'changeIn.required' => "Change in is required.",
            'changeType.required' => "Change type is required.",
            'targetDate.required' => "Target implementation date is required.",
            'departmentsInvolved.required' => "Departments involved is required.",
            'title.required' => "Title/subject is required.",
            'backgroundInfo.required' => "Background information is required.",
            'targetDate.date_format' => "The target date format is invalid ex. 2016-12-12"
        ];

//        if(Session::get("is_ssmd") && Session::get('desig_level') == 'head' ) {
//            $rules["ssmdActionPlan"] = "required";
//            $message["ssmdActionPlan.required"] = "SSMD action plan is required";
//        }

        if (in_array('process_design',$data['mocDetails']['requestTypes']) || in_array('review_change',$data['mocDetails']['requestTypes'])) {
            $rules['changeIn'] = 'required';
            $rules['changeType'] = 'required';
            $rules['targetDate'] = 'required|date_format:"Y-m-d"';
            $rules['departmentsInvolved'] = 'required';
            if(in_array('others',$data['mocDetails']['changeIn'])) {
                $rules['changeInOthersInput'] = 'required';
            }
        }

        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function comment($data) {
        $inputs = [
            'comment' => $data['comment']
        ];

        $rules = [
            'comment' => 'required',
        ];

        $message = [
            'comment.required' => 'Please indicate reason for disapproval.'
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];

    }

    public function cancelRequest($data) {
        $inputs = [
            'comment' => $data['comment']
        ];

        $rules = [
            'comment' => 'required',
        ];

        $message = [
            'comment.required' => 'Please indicate reason for cancellation.'
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function DHtoADH($data) {
        $inputs = [
            'assessmentType' => $data['assessmentType']
        ];

        $rules = [
            'assessmentType' => 'required',
        ];

        $validate = Validator::make(
            $inputs,
            $rules
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
    
    public function ADHtoBSA($data) {
        $inputs = [
            'assigned_bsa' => $data['assignedBSA']
        ];

        $rules = [
            'assigned_bsa' => 'required',
        ];

        $messages = [
            'assigned_bsa:required' => 'Please select BSA',
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $messages
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }

    public function toAOM($data) {
        $inputs = [
            'ssmdActionPlan' => $data['SSMDActionPlan']
        ];

        $rules = [
            'ssmdActionPlan' => 'required',
        ];

        $message = [
            'ssmdActionPlan.required' => 'SSMD assessment/action plan is required.'
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return [
                'success' => false,
                'errors' => $validate->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
    }
}