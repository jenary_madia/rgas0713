<?php

namespace RGAS\Modules\MRF;
use RGAS\Libraries;
use Employees;
use Receivers;
use Sections;
use ManpowerRequest;
use Response;
use Session;
use URL;
use Config;
use Mail;
use Input;
use DB;
use Carbon\Carbon;

use MrfAttachments;
use MrfComments;
use MrfSignatories;
use MRFRecruitmentDetails;

class MRF
{
    protected $superiorID;
    protected $employeeid;
    protected $newEmployeeid;
    protected $company;
    protected $desig_level;
    protected $dept_id;

    protected $signatories;

    protected $selectItems;
    protected $logs;
    protected $mrfValidator;
    protected $mrfRecDetails;

    public function __construct(){
        $this->logs = new Logs;
        $this->employeeid = Session::get('employee_id');
        $this->newEmployeeid = Session::get('employee_id');
        $this->company = Session::get('company');
        $this->superiorID = Session::get("superiorid");
        $this->desig_level = Session::get("desig_level");
        $this->dept_id = Session::get("dept_id");
        $this->mrfValidator = new MRFValidator();
        $this->signatories = new MrfSignatories();
        $this->mrfRecDetails = new MRFRecruitmentDetails();
    }

    public function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'mrf/';
    }

    /***********creation and own moc actions*************/

    public function createAction($data) {
        /**** SETTING UP PARAMETERS FOR manpowerrequests*****/
        $requestParams = [
            'contact_no' => $data['formDetails']['contactNumber'],
            'position_jobtitle' => $data['requestDetails']['positionTitle'],
            'dateneeded' => $data['requestDetails']['dateNeeded'],
            'request_nature' => $data['requestDetails']['nature'],
            'periodcovered' => $data['requestDetails']['periodCovered'],
            'internal' => $data['requestDetails']['internal'],
            'external' => $data['requestDetails']['external'],
            'reason' => $data['requestDetails']['reason'],
            'comment' => json_encode([
                [
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=>$data['comment']
                ]
            ])
        ];

        if(Session::get("company") == "RBC-CORP") {
            $requestParams['request_for'] = $data['requestDetails']['requestFor'];
            $requestParams['company_name'] = $data['requestDetails']['company'];
            $requestParams['department_name'] = $data['requestDetails']['department'];
            $requestParams['manpower_count'] = ($data['requestDetails']['count'] == 11 ? $data['requestDetails']['otherCount'] : $data['requestDetails']['count']);
        }

        $section = Sections::where('sect_name',$data['formDetails']['section'])->count();
        if($section < 1) {
            $requestParams['other_section'] = $data['formDetails']['section'];
        }else{
            $requestParams['sectionid'] = $data['formDetails']['section'];
        }
        if(! $data['mrfID']) {
            $requestParams['reference_no'] = (new ManpowerRequest)->generateRefNo();
            $requestParams['requestedby'] = $this->employeeid;
            $requestParams['employeeid'] = $this->newEmployeeid;
            $requestParams['datefiled'] = Carbon::now()->toDateString();
            $requestParams['company'] = $this->company;
            $requestParams['departmentid'] = $this->dept_id;
        }else{
            $manpowerRequest = ManpowerRequest::where([
                'id' => $data['mrfID'],
                'employeeid' => $this->employeeid
            ])->first();
        }

        if($data['action'] == 'send') {
            if($this->company == 'RBC-CORP') {
                /******CORPORATE******/
                if(in_array($this->desig_level,['employee','supervisor'])) {
                    /******assigned staff******/
                    $requestParams['curr_emp'] = $this->getDeptHead($this->superiorID)->id;
                    $requestParams['status'] = 'FOR ENDORSEMENT';
                    $successMessage = "MRF successfully sent to department head";
                }else{
                    /******DH and up******/
                    if(IS_CHRD_HEAD) {
                        switch ($requestParams['request_nature']) {
                            case 1:/******New position******/
                                $requestParams['curr_emp'] = $this->getCHRDRecipient();
                                $requestParams['status'] = 'FOR PROCESSING';
                                $successMessage = "MRF successfully sent to CHRD Recipient";
                                break;
                            case 2:/****Additional Staff****/
                                $requestParams['curr_emp'] = $this->getCHRDRecipient();
                                $requestParams['status'] = 'FOR PROCESSING';
                                $successMessage = "MRF successfully sent to CHRD Recipient";
                                break;
                            case 3:/******Replacement******/
                                $requestParams['curr_emp'] = $this->getRecruitmentStaff();
                                $requestParams['status'] = 'NOTED';
                                $successMessage = "MRF successfully sent to Recruitment Staff";
                                break;
                            case 4:/******Temporary******/
                                $requestParams['curr_emp'] = $this->getRecruitmentStaff();
                                $requestParams['status'] = 'NOTED';
                                $successMessage = "MRF successfully sent to Recruitment Staff";
                                break;
                        }
                    }else{
                        switch ($requestParams['request_nature']) {
                            case 1:
                                $requestParams['curr_emp'] = $this->getCHRDRecipient();
                                $requestParams['status'] = 'FOR PROCESSING';
                                $successMessage = "MRF successfully sent to CHRD Recipient";
                                break;
                            case 2:
                                $requestParams['curr_emp'] = $this->getCHRDRecipient();
                                $requestParams['status'] = 'FOR PROCESSING';
                                $successMessage = "MRF successfully sent to CHRD Recipient";
                                break;
                            case 3:/******Replacement******/
                                $requestParams['curr_emp'] = $this->getCHRDHead();
                                $requestParams['status'] = 'APPROVED';
                                $successMessage = "MRF successfully sent to HR";
                                break;
                            case 4:
                                $requestParams['curr_emp'] = $this->getCHRDHead();
                                $requestParams['status'] = 'APPROVED';
                                $successMessage = "MRF successfully sent to HR";
                                break;
                        }
                    }

                }
            }else{
                /******SATELLITE******/
                if($this->desig_level == "head") {
                    switch ($requestParams['request_nature']) {
                        case 5:/******Replacement******/
                            $requestParams['curr_emp'] = $this->getSHRDHead();
                            $requestParams['status'] = 'APPROVED';
                            $successMessage = "MRF successfully sent to Satellite HR Head";
                            break;
                        case 6:/****Temporary****/
                            $requestParams['curr_emp'] = $this->getSHRDHead();
                            $requestParams['status'] = 'APPROVED';
                            $successMessage = "MRF successfully sent to Satellite HR Head";
                            break;
                        case 7:/******New Job Position and Additional Staff******/
                            $requestParams['curr_emp'] = $this->getSSMD(Session::get("company"));
                            $requestParams['status'] = 'FOR ASSESSMENT';
                            $successMessage = "MRF successfully sent to Satellite Systems";
                            break;

                    }
                }else{
                    return Response::json([
                        'success' => false,
                        'errors' => ['User not allowed to do this process'],
                        'message' => "Something went wrong",
                        'url' => URL::to("/mrf/list-view/my-mrf"),
                        400
                    ]);
                }
            }
        }else{
            $requestParams['status'] = 'NEW';
            if(! $data['mrfID']) {
                $successMessage = 'MRF successfully saved and can be edited later.';
            }else{
                $successMessage = 'MRF successfully updated.';
            }

        }

        /**** SETTING UP PARAMETERS FOR MrfAttachemnts*****/
        $fm = new Libraries\FileManager;
        $mraf = [];
        $pdq = [];
        $orgChart = [];
        foreach ($data['MRAFattachments'] as $key) {
            if ($data['action'] == "send") {
                $fm->move($key['random_filename'], $this->getStoragePath() . ($data['mrfID'] ? $manpowerRequest['reference_no'] : $requestParams['reference_no']));
            }
            array_push($mraf,[
                'filesize' => $key['filesize'],
                'mime_type' => $key['mime_type'],
                'original_extension' => $key['original_extension'],
                'original_filename' => $key['original_filename'],
                'random_filename' => $key['random_filename'],
            ]);
        }

        foreach ($data['PDQattachments'] as $key) {
            if ($data['action'] == "send") {
                $fm->move($key['random_filename'], $this->getStoragePath() . ($data['mrfID'] ? $manpowerRequest['reference_no'] : $requestParams['reference_no']));
            }
            array_push($pdq, [
                'filesize' => $key['filesize'],
                'mime_type' => $key['mime_type'],
                'original_extension' => $key['original_extension'],
                'original_filename' => $key['original_filename'],
                'random_filename' => $key['random_filename'],
            ]);
        }

        foreach ($data['OCattachments'] as $key) {
            if ($data['action'] == "send") {
                $fm->move($key['random_filename'], $this->getStoragePath() . ($data['mrfID'] ? $manpowerRequest['reference_no'] : $requestParams['reference_no']));
            }
            array_push($orgChart, [
                'filesize' => $key['filesize'],
                'mime_type' => $key['mime_type'],
                'original_extension' => $key['original_extension'],
                'original_filename' => $key['original_filename'],
                'random_filename' => $key['random_filename'],
            ]);
        }

        $requestParams['mraffile'] = json_encode($mraf);
        $requestParams['pdq'] = json_encode($pdq);
        $requestParams['org_chart'] = json_encode($orgChart);

        /*************INSERT INTO MRFrequests*******************/
        if(! $data['mrfID']) {
            $manpowerRequest = ManpowerRequest::create($requestParams);
        }else{
            if(! $manpowerRequest) {
                return Response::json([
                    'success' => false,
                    'errors' => ['User not allowed to do this process'],
                    'message' => "Something went wrong",
                    'url' => URL::to("/mrf"),
                    400
                ]);
            }
            $manpowerRequest->update($requestParams);
            MrfSignatories::where("manpowerrequestid",$data['mrfID'])->delete();
        }
        /********LOGS**********/
        $this->logs->AU001($manpowerRequest['id'],'manpowerrequests','id',json_encode($manpowerRequest),$manpowerRequest['reference_no']);

        if($data['MRAFattachments'] || $data['PDQattachments'] || $data['OCattachments']) {
            $this->logs->AU002($manpowerRequest['id'], 'manpowerrequests', 'id', json_encode($manpowerRequest), $manpowerRequest['reference_no']);
        }


        /*********EMAIL************/
        if($data['action'] == 'send') {
            $this->sendMail(
                'checking',
                $manpowerRequest['status'],
                $manpowerRequest['reference_no'],
                Session::get("employee_name"),
                Session::get("dept_name"),
                $this->getEmail($requestParams['curr_emp'])
            );
        }

        /***************DISPLAY RESPONSE********************/
        return Response::json([
            'success' => true,
            'message' => $successMessage,
            'url' => URL::to("/mrf/list-view/my-mrf"),
            200
        ]);
    }

    public function processAction($data) {
        $manpowerRequest = ManpowerRequest::where([
            'id' => $data['id'],
            'curr_emp' => $this->employeeid,
        ])
            ->with("owner")
            ->with("department")
            ->first();

        if(! $manpowerRequest) {
            return Response::json([
                'success' => false,
                'message' => "Invalid MRF",
                'url' => URL::to("/mrf"),
                200
            ]);
        }

        if($manpowerRequest->status == "FOR ENDORSEMENT") {
            $url = URL::to("/mrf/list-view/for-endorsement");
        }elseif($manpowerRequest->status == "FOR PROCESSING"){
            $url = URL::to("/mrf/list-view/for-processing");
        }else{
            $url = URL::to("/mrf/list-view/submitted");
        }
        /**** SETTING UP PARAMETERS FOR manpowerrequests*****/
        $routingResponse = $this->nextOnRouting(
            $data['id'],
            $manpowerRequest->company,
            $manpowerRequest->request_for,
            $manpowerRequest->request_nature,
            $manpowerRequest->employeeid,
            $manpowerRequest->status,
            $manpowerRequest->job_level,
            $manpowerRequest->reference_no
        );

        if ($routingResponse['success'] == true) {
            $comment = [[
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>$data['comment']
            ]];
            $successMessage = $routingResponse['message'];
            $comments = array_merge(json_decode($manpowerRequest['comment']),$comment);
            $routingResponse['comment'] = json_encode($comments);
            unset($routingResponse['message']);
            unset($routingResponse['success']);
            /*************UPDATE INTO MRFrequests*******************/
            $manpowerRequest->update($routingResponse);

            /**** SETTING UP PARAMETERS FOR MRFcomment*****/
            $commentParam = [
                'manpowerrequestid' => $manpowerRequest->id,
                'commentor_id' => $this->employeeid,
                'comment' => $data['comment'],
            ];
            if($data['action'] != "save") {
                /*************INSERT INTO MRFcomment*******************/
                // MrfComments::create($commentParam);
                /*********EMAIL************/
                $this->sendMail(
                    'checking',
                    $manpowerRequest['status'],
                    $manpowerRequest['reference_no'],
                    $manpowerRequest->owner->firstname.' '.$manpowerRequest->owner->middlename.' '.$manpowerRequest->owner->lastname,
                    $manpowerRequest->department->dept_name,
                    $this->getEmail($manpowerRequest['curr_emp'])
                );
            }
            /***************DISPLAY RESPONSE********************/
            return Response::json([
                'success' => true,
                'message' => $successMessage,
                'url' => $url,
                200
            ]);

        }

        return Response::json($routingResponse);
    }

    /**
     * @param $data
     */
    public function saveRecruitment($data) {
        $manpowerRequest = ManpowerRequest::where([
            'id' => $data['id'],
            'curr_emp' => $this->employeeid,
        ])->first();

        if(! $manpowerRequest) {
            return Response::json([
                'success' => false,
                'message' => "Invalid MRF",
                'url' => URL::to("/mrf/list-view/submitted"),
                200
            ]);
        }
//        return $data;
        $this->mrfRecDetails->insertRecruitmentDetails($data['id'],$data['recruitmentDetails']);

//        $commentParam = [
//            'manpowerrequestid' => $data['id'],
//            'commentor_id' => $this->employeeid,
//            'comment' => $data['comment'],
//        ];
//        /*************INSERT INTO MRFcomment*******************/
//        MrfComments::create($commentParam);

        return Response::json([
            'success' => true,
            'message' => "MRF Sucessfully save",
            'url' => URL::to("/mrf/list-view/submitted"),
            200
        ]);

    }

    public function returnAction($data) {
        $manpowerRequest = ManpowerRequest::where([
            'id' => $data['id'],
            'curr_emp' => $this->employeeid,
        ])->first();

        $comment = [[
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=>$data['comment']
        ]];
        $comments = json_encode(array_merge(json_decode($manpowerRequest['comment']),$comment));

        if(! $manpowerRequest) {
            return Response::json([
                'success' => false,
                'message' => "Invalid MRF",
                'url' => URL::to("/mrf"),
                200
            ]);
        }

        if($manpowerRequest->status == "FOR ENDORSEMENT") {
            $url = URL::to("/mrf/list-view/for-endorsement");
        }elseif($manpowerRequest->status == "FOR PROCESSING"){
            $url = URL::to("/mrf/list-view/for-processing");
        }else{
            $url = URL::to("/mrf/list-view/submitted");
        }

        if($manpowerRequest->status == "FOR ASSESSMENT") {
            for ($i=0; $i < count($data['CSMDattachments']); $i++) {
                $data['CSMDattachments'][$i]['manpowerrequestid'] = $data['id'];
            }


            /**** SETTING UP PARAMETERS FOR moc_attachments*****/
            $fm = new Libraries\FileManager;
            $attachmentsToInsert = [];
            if($data['CSMDattachments']) {
                foreach ($data['CSMDattachments'] as $key) {
                    $fm->move($key['random_filename'], $this->getStoragePath() . $manpowerRequest['reference_no']);
                    array_push($attachmentsToInsert, [
                        "filesize" => $key['filesize'],
                        "mime_type" => $key['mime_type'],
                        "original_extension" => $key['original_extension'],
                        "original_filename" =>  $key['original_filename'],
                        "random_filename" =>  $key['random_filename'],
                        'manpowerrequestid' => $data['id']
                    ]);
                }
            }
            /*************INSERT INTO moc_attachments*******************/
            if(count($attachmentsToInsert) > 0) {
                MrfAttachments::where('manpowerrequestid',$data['id'])->delete();
                MrfAttachments::insert($attachmentsToInsert);
            }

        }
        if($data['process'] == "toFiler") {
            $manpowerRequest->update([
                "curr_emp" => $manpowerRequest->employeeid,
                "status" => "RETURNED",
                "comment" => $comments
            ]);

            $successMessage = "Successfully returned to filer";
        }

        if($data['process'] == "toCHRDRecipient") {
            $manpowerRequest->update([
                "curr_emp" => $this->getCHRDRecipient(),
                "status" => "FOR PROCESSING",
                "comment" => $comments
            ]);

            $manpowerRequest->csmdadhassessedby = null;
            $manpowerRequest->bsaassessedby = null;
            $manpowerRequest->save();
            $successMessage = "Successfully returned to CHRD Recipient";
            $this->signatories->rollbackSignatories($data['id'],2);
        }

        if($data['process'] == "toCsmdADH") {
            $manpowerRequest->update([
                "curr_emp" => $this->getCSMD('supervisor')
            ]);

            $manpowerRequest->csmdadhassessedby = null;
            $manpowerRequest->bsaassessedby = null;
            $manpowerRequest->save();
            $successMessage = "Successfully returned to CSMD ADH";
            $this->signatories->rollbackSignatories($data['id'],3);
        }

        if($data['process'] == "toBSA") {
            $manpowerRequest->update([
                "curr_emp" => $manpowerRequest->bsaassessedby
            ]);
            $manpowerRequest->bsaassessedby = null;
            $manpowerRequest->save();
            $successMessage = "Successfully returned to assigned CSMD BSA";
            $this->signatories->rollbackSignatories($data['id'],3);
        }

        if($data['process'] == "toCHRD") {
            $manpowerRequest->update([
                "curr_emp" => $this->getCHRDHead()
            ]);
            $manpowerRequest->svpapprovedby = null;
            $manpowerRequest->save();
            $successMessage = "Successfully returned to CHRD";
            $this->signatories->rollbackSignatories($data['id'],4);
        }

        if($data['process'] == "toDivHead") {
            $manpowerRequest->update([
                "curr_emp" => $this->getDvHead(CSMD_CORP_DIVHEAD),
            ]);
            $manpowerRequest->svpapprovedby = null;
            $manpowerRequest->save();
            $successMessage = "Successfully returned to Corporate Services Division Head ";
            $this->signatories->rollbackSignatories($data['id'],4);
        }

        return Response::json([
            'success' => true,
            'message' => $successMessage,
            'url' => $url,
            200
        ]);
    }

    public function cancelAction($data) {
        $manpowerRequest = ManpowerRequest::where([
            'id' => $data['id'],
            'employeeid' => $this->employeeid,
            'status' => "FOR ENDORSEMENT"
        ])->first();

        if(! $manpowerRequest) {
            return Response::json([
                'success' => false,
                'message' => "Invalid MRF",
                'url' => URL::to("/mrf/list-view/my-mrf"),
                200
            ]);
        }

        $manpowerRequest->update([
            "curr_emp" => $this->employeeid,
            "status" => "CANCELLED"
        ]);
        $manpowerRequest->save();
        $successMessage = "MRF Successfully cancelled";

        return Response::json([
            'success' => true,
            'message' => $successMessage,
            'url' => URL::to("/mrf/list-view/my-mrf"),
            200
        ]);
    }


    /***************private functions*********************/
    private function getDeptHead($superiorId) {
        $personnel = Employees::where('id',$superiorId)->first(['desig_level','id']);
        if($personnel['desig_level'] != 'head') {
            while ($personnel['desig_level'] != 'head') {
                $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                $superiorId = $personnel['superiorid'];
            }
        }elseif ($personnel['desig_level'] == 'head'){
            $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
        }

        return ($personnel ? $personnel : null);
    }

    private function getCSMD($type) {
        switch ($type) {
            case "head":
                return Receivers::where([
                    'description' => 'CSMD Department Head',
                    'title' => 'Corporate MRF receiver',
                    'code' => 'CORP_MRF_RECEIVER'
                ])->first()['employeeid'];
                break;
            case "supervisor":
                return Receivers::where([
                    "code" => MRF_CSMD_ADH,
                ])->first()->employeeid;
                break;
        }

    }

    private function getSSMD($comp = null) {
        $company = $comp ? $comp : Session::get("company");
        return Receivers::where([
            "code" => MRF_SSMD_RECEIVER,
            "company" => $company
        ])->first()->employeeid;
    }

    private function getCHRDHead()
    {
        return Receivers::where([
            'description' => 'HRMD Department Head',
            'title' => 'Corporate MRF receiver',
            'code' => 'CORP_MRF_RECEIVER'
        ])->first()['employeeid'];
    }

    private function getSHRDHead()
    {
        return Receivers::where([
            'code' => SAT_CHRD_RECEIVER
        ])->first()['employeeid'];
    }

    private function getRecruitmentStaff()
    {
        return Sections::where('sect_code', Session::get("company") == "RBC-CORP" ? "HRR" : "HRR-SAT")->join('employees', 'employees.departmentid', '=', 'sections.departmentid')->first()['id'];
    }

    private function getCHRDRecipient()
    {
        return Receivers::where(['description' => 'CHRD Recipient', 'title' => 'Corporate MRF receiver', 'code' => 'CORP_MRF_RECEIVER'])->first()['employeeid'];
    }

    private function getVPO($company = null)
    {
        return Receivers::where(['code' => VPO,'company' => ($company ? $company : Session::get("company"))])->first()['employeeid'];
    }

    private function getDvHead($company_type) {

        $result = DB::table('receivers')
            ->where('receivers.code' , $company_type)
            ->first()->employeeid;
        return $result;
    }

    public function getPresident()
    {
        return Employees::where('desig_level', 'president')->first()['id'];
    }

    private function sendMail($action,$status,$refno,$filer,$department,$email) {
        if($email) {
            if ($action == "DISAPPROVED") {
                Mail::send('mrf.email.disapprove', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer){
                        $message->to($email,$filer)->subject('RGAS Notification Alert: MOC for processing.');
                    }
                );
            }elseif ($action == "CANCEL") {
                Mail::send('mrf.email.cancel', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer){
                        $message->to($email,$filer)->subject('RGAS Notification Alert: MOC for processing.');
                    }
                );
            }else{
                Mail::send('mrf.email.mrf', array(
                    'action' => $action,
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer,$action){
                        $message->to($email,$filer)->subject("RGAS Notification Alert: MOC for processing.");
                    }
                );
            }
        }

    }

    private function getEmail($id) {
        return Employees::where('id',$id)->first()['email'];
    }

    private function nextOnRouting($id,$company,$org,$nature,$employee_id,$status,$jobLevel = null,$reference_no) {

        $level = $this->checkOwnerLevel($employee_id);
        $action = Input::get("action");
        $lastSig = MrfSignatories::where('manpowerrequestid',$id)->max('signature_type');
        $recruitmentStaff = Input::get("recruitmentStaff");
        $departmentHead = Input::get("deptHead");
        $recruitmentDetails = Input::get("recruitmentDetails");
        $csmdAttachments = Input::get("CSMDattachments");
        $bsa = Input::get("csmdBSA");
        switch ($jobLevel) {
            case 3:
                if($company == "RBC-CORP") {
                    $nextEmp = $this->getCHRDHead();
                    $nextMessage = "MRF approved and successfully sent to CHRD Head";
                    $nextStatus = 'APPROVED';
                }else{
                    $nextEmp = $this->getSHRDHead();
                    $nextStatus = 'APPROVED';
                    $nextMessage = "MRF successfully sent to Satellite HR Head";
                }
                break;

            case 2:
                $nextEmp = $this->getPresident();
                $nextMessage = "MRF approved and successfully sent to President";
                $nextStatus = 'FOR APPROVAL';
                break;

        }
        if($company == "RBC-CORP") {
            if($status == "FOR ENDORSEMENT") {
                if(in_array($nature,[1,2])) {
                    $response = [
                        'curr_emp' => $this->getCHRDRecipient(),
                        'status' => 'FOR PROCESSING',
                        'message' => "MRF successfully sent to CHRD Recipient",
                        'success' => true,
                    ];

                    $this->signatories->addSignatories($id,1);

                }elseif(in_array($nature,[3,4])){
                    $response = [
                        'curr_emp' => $this->getCHRDHead(),
                        'status' => 'APPROVED',
                        'message' => "MRF successfully sent to HR",
                        'success' => true,
                    ];

                    $this->signatories->addSignatories($id,4);
                }

            }elseif($status == "FOR PROCESSING") {
                $jobLevel = Input::get("jobLevel");
                if($action == 'assign') {
                    $validate = $this->mrfValidator->validate([
                        'recruitmentStaff' => $recruitmentStaff,
                        'jobLevel' => $jobLevel,
                    ]);
                    if($validate['success'] == "true") {
                        $response = [
                            'curr_emp' => $recruitmentStaff,
                            'job_level' => $jobLevel,
                            'status' => 'NOTED',
                            'message' => "MRF successfully sent to Recruitment Staff",
                            'success' => true,
                        ];
//                                    $this->signatories->addSignatories($id,2);FOR NOTED
                    }else{
                        $response = $validate;
                    }
                }elseif ($action == 'forProcess') {
                    $validate = $this->mrfValidator->validate([
                        'jobLevel' => $jobLevel,
                    ]);
                    if($validate['success'] == "true") {
                        $response = [
                            'curr_emp' => $this->getCSMD('supervisor'),
                            'job_level' => $jobLevel,
                            'status' => 'FOR ASSESSMENT',
                            'message' => "MRF successfully sent to CSMD Assistant Department Head for assessment",
                            'success' => true,
                        ];
                        $this->signatories->addSignatories($id,2);
                    }else{
                        $response = $validate;
                    }
                }
            }elseif($status == "FOR ASSESSMENT") {
                if($action == 'assign') {
                    $validate = $this->mrfValidator->validate([
                        'bsa' => $bsa,
                    ]);
                    if($validate['success'] == "true") {
                        $response = [
                            'curr_emp' => $bsa,
                            'csmdadhassessedby' => $this->employeeid,
                            'message' => "MRF successfully sent to assigned CSMD BSA",
                            'success' => true,
                        ];
                    }else{
                        $response = $validate;
                    }
                }elseif ($action == 'forProcess') {

                    if($lastSig == 3) { //CSMD DH TO SVP CORP
                        $response = [
                            'curr_emp' => $this->getDvHead(CSMD_CORP_DIVHEAD),
                            'csmddhassessedby' => $this->employeeid,
                            'status' => 'FOR APPROVAL',
                            'message' => "MRF successfully sent to Corporate Services Division Head for approval",
                            'success' => true,
                        ];
                        $this->signatories->addSignatories($id,3);
                    }else{ //BSA TO CSMD DH
                        $response = [
                            'curr_emp' => $this->getCSMD('head'),
                            'bsaassessedby' => $this->employeeid,
                            'message' => "MRF successfully sent to CSMD Department Head for review",
                            'success' => true,
                        ];
                        $this->signatories->addSignatories($id,3);
                    }

                }elseif ($action == 'toCsmdDH') {
                    $response = [
                        'curr_emp' => $this->getCSMD('head'),
                        'csmdadhassessedby' => $this->employeeid,
                        'message' => "MRF successfully sent to CSMD Department Head for review",
                        'success' => true,
                    ];
                    $this->signatories->addSignatories($id,3);
                }elseif($action == "save") {
                    $response = [
                        'message' => "MRF successfully updated",
                        'success' => true,
                    ];
                }

                for ($i=0; $i < count($csmdAttachments); $i++) {
                    $csmdAttachments[$i]['manpowerrequestid'] = $id;
                }

                /**** SETTING UP PARAMETERS FOR moc_attachments*****/
                $fm = new Libraries\FileManager;
                $attachmentsToInsert = [];
                if($csmdAttachments) {
                    foreach ($csmdAttachments as $key) {
                        $fm->move($key['random_filename'], $this->getStoragePath() . $reference_no);
                        array_push($attachmentsToInsert, [
                            "filesize" => $key['filesize'],
                            "mime_type" => $key['mime_type'],
                            "original_extension" => $key['original_extension'],
                            "original_filename" =>  $key['original_filename'],
                            "random_filename" =>  $key['random_filename'],
                            'manpowerrequestid' => $id
                        ]);
                    }
                }

                /*************INSERT INTO moc_attachments*******************/
                if(count($attachmentsToInsert) > 0) {
                    MrfAttachments::where('manpowerrequestid',$id)->delete();
                    MrfAttachments::insert($attachmentsToInsert);
                }

            }elseif($status == "FOR APPROVAL") {
                if($action == "assign") {
                    $validate = $this->mrfValidator->validate([
                        'departmentHead' => $departmentHead,
                    ]);
                    if($validate['success'] == "true") {
                        $response = [
                            'curr_emp' => $departmentHead,
                            'status' => 'FOR FURTHER ASSESSMENT',
                            'message' => 'MRF successfully sent for further assessment',
                            'success' => true,
                        ];

                        $this->signatories->rollbackSignatories($id,4);
                    }else{
                        $response = $validate;
                    }
                }elseif($action == "forProcess") {
                    if($lastSig == 4) {
                        $response = [
                            'curr_emp' => $this->getCHRDHead(),
                            'status' => 'APPROVED',
                            'message' => "MRF approved and successfully sent to CHRD Head",
                            'svpapprovedby' => $this->employeeid,
                            'success' => true,
                        ];
                    }else{
                        $response = [
                            'curr_emp' => $nextEmp,
                            'status' => $nextStatus,
                            'message' => $nextMessage,
                            'success' => true,
                        ];
                        if($nextStatus == "FOR APPROVAL") {
                            $response['svpapprovedby'] = $this->employeeid;
                        }else{
                            $response['presapprovedby'] = $this->employeeid;

                        }

                        $this->signatories->addSignatories($id,4);
                    }

                }elseif($action == "disapprove") {
                    $response = [
                        'curr_emp' => $this->getCHRDHead(),
                        'status' => 'DISAPPROVED',
                        'presapprovedby' => $this->employeeid,
                        'message' => "MRF disapproved and successfully sent to CHRD Head",
                        'success' => true,
                    ];

                    $this->signatories->addSignatories($id,5);
                }
            }elseif($status=="FOR FURTHER ASSESSMENT"){
                if($action == "forProcess") {
                    $response = [
                        'curr_emp' => $this->getDvHead(CSMD_CORP_DIVHEAD),
                        'status' => 'FOR APPROVAL',
                        'message' => "MRF successfully sent to Corporate Services Division Head for approval",
                        'success' => true,
                    ];
                    // $this->signatories->addSignatories($id,3); FOR CLARIFICATION
                }elseif($action == "save") {
                    $response = [
                        'message' => "MRF successfully updated",
                        'success' => true,
                    ];
                }

            }elseif ($status=="APPROVED") {
                $validate = $this->mrfValidator->recruitmentStaff(Input::get("recruitmentStaff"));
                if($validate['success'] == "true") {
                    $response = [
                        'curr_emp' => Input::get("recruitmentStaff"),
                        'status' => 'NOTED',
                        'message' => "MRF successfully sent to Recruitment Staff",
                        'success' => true,
                    ];

                    $this->signatories->addSignatories($id,7);
                }else{
                    $response = $validate;
                }

            }elseif ($status=="NOTED") {
                $this->mrfRecDetails->insertRecruitmentDetails($id,$recruitmentDetails);
                $response = [
                    'curr_emp' => $this->getSHRDHead(),
                    'status' => 'NOTED', // OLD data is approved.. 28/07/2016
                    'message' => "MRF successfully sent to Satellite HR Head",
                    'success' => true,
                    'receivedby' => $this->getSHRDHead()
                ];
            }
        }else{
            if($status == "FOR ASSESSMENT") {
                $response = [
                    'curr_emp' => $this->getVPO($company),
                    'status' => 'FOR APPROVAL',
                    'message' => "MRF successfully sent to VP-operations",
                    'success' => true,
                ];

                $this->signatories->addSignatories($id,1);
            }elseif($status == "FOR APPROVAL") {
                if($lastSig == 4) {
                    $response = [
                        'curr_emp' => $this->getSHRDHead(),
                        'status' => 'APPROVED',
                        'message' => "MRF successfully sent to Satellite HR Head",
                        'presapprovedby' => $this->employeeid,
                        'success' => true,
                    ];
                }else {
                    $response = [
                        'curr_emp' => $this->getPresident(),
                        'status' => 'FOR APPROVAL',
                        'message' => 'MRF approved and successfully sent to President',
                        'svpapprovedby' => $this->employeeid,
                        'success' => true,
                    ];
                    $this->signatories->addSignatories($id, 4);
                }
                
            }elseif($status == "APPROVED") {
                $validate = $this->mrfValidator->recruitmentStaff(Input::get("recruitmentStaff"));

                if($validate['success'] == "true") {
                    $response = [
                        'curr_emp' => Input::get("recruitmentStaff"),
                        'status' => 'NOTED',
                        'message' => "MRF successfully sent to Recruitment Staff",
                        'success' => true,
                    ];
                    $this->signatories->addSignatories($id,7);
                }else{
                    $response = $validate;
                }
            }
        }

        return $response;
    }

    private function checkOwnerLevel($employee_id) {
        $employee = Employees::find($employee_id);
        if(in_array($employee->desig_level,['employee','supervisor'])) {
            return 1;
        }else{
            if(Receivers::mrf_is_chrd_head($employee_id)) {
                return 2;
            }else{
                return 3;
            }
        }
    }

}