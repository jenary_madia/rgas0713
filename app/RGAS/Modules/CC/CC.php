<?php namespace RGAS\Modules\CC;

use Session;
use RGAS\Libraries;
use ClearanceCertification;
use DB;
use Input;
use Redirect;
use Employees;
use Receivers;
use Mail;
use RGAS\Libraries\AuditTrail;
use Config;

class CC extends CCLogs implements ICC
{
	private $module = 15;
	private $table = "clearancecertification";
	

	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_cc');
		return $this->module_id;
	}

	public function getrecord()
	{	
		$empid = Input::get("id");

		$emp = DB::table("employees")
				->leftjoin("departments" , "departments.id" , "=" ,"employees.departmentid")
				->leftjoin("sections" , "sectionid" , "=" ,"sections.id")
				->where("employees.id",$empid)
				->get(["sect_name","firstname" , "lastname","dept_name","employeeid" , "div_code","employees.departmentid" , "sectionid","designation" , "date_hired","superiorid"]);
		
		foreach($emp as $rs)
		{
			$data["employee_no"] = $rs->employeeid;//employee no.
			$data["div_code"] = $rs->div_code;//division

			$data["dept_id"] = $rs->departmentid;//department
			$data["dept_name"] = $rs->dept_name;//department
			$data["section"] = $rs->sectionid;//section
			$data["section_name"] = $rs->sect_name;//section
			$data["position"] = $rs->designation;//position

			$data["date_hired"] = $rs->date_hired;//date hired
			$data["superior_id"] = $rs->superiorid;//immediate head
		}

		$div_head = DB::table("divisions")->where("div_code", $rs->div_code)->get(["employeeid"]);
		$data["div_head_id"] = isset($div_head[0]->employeeid) ? $div_head[0]->employeeid : 0;

		$dept_head = DB::table("employees")
			->where("desig_level","head")
			->where("departmentid", $rs->departmentid)
			->get(["id","firstname" , "lastname"]);
		$data["dept_head_id"] = isset($dept_head[0]->id) && $dept_head[0]->id && $dept_head[0]->id != $empid ? $dept_head[0]->id : $rs->superiorid;
		$data["dept_head_name"] = isset($dept_head[0]->id) && $dept_head[0]->id != $empid ? $dept_head[0]->firstname . " " . $dept_head[0]->lastname : "";

		$name = DB::table("employees")
			->wherein("id",[$data["div_head_id"] , $data["superior_id"] , $data["dept_head_id"] ])
			->get(["firstname" , "lastname","id"]);
		foreach($name as $rs)
		{
			if($rs->id == $data["div_head_id"])
			{
				$data["div_head_name"] = $rs->firstname . " " . $rs->lastname;//name
			}

			if($rs->id == $data["superior_id"])
			{
				$data["superior_name"] = $rs->firstname . " " . $rs->lastname;//name
			}
			if(!$data["dept_head_name"] && $rs->id == $data["dept_head_id"])
			{
				$data["dept_head_name"] = $rs->firstname . " " . $rs->lastname;//name
			}
		}

		return $data;
	}

	
	public function deleterecord($id)
	{	
		DB::beginTransaction();

		DB::table("clearancecertificationdepartments")->where("ccd_cc_id",$id)->delete();
		DB::table("clearancecertificationcomments")->where("ccc_cc_id",$id)->delete();
		DB::table("clearancecertificationlogs")->where("ccl_cc_id",$id)->delete();

		$store =ClearanceCertification::where("cc_id",$id)
			->Where(function ($query) {
                $query->whereIn('cc_status' , [0,3])
                      ->orWhere('cc_print_status' , 1);
            })
			->delete();
		if($store)
		{		
			DB::Commit();
			return Redirect::to("cc/report")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY DELETED.');
		}
		else
		{
			DB::rollback();
			return Redirect::to("cc/report")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
		}
		
	}

	
	public function resignee_sent()
	{	
		$reference = ClearanceCertification::get_reference_no();
		$parameters = array(
		 		'cc_emp_id' => Input::get('employee'),
		 		'cc_div_code' => Input::get('division'),
		 		'cc_emp_departmentid' => Input::get('department_id'),
		 		'cc_emp_sectionid' => Input::get('section'),
		 		'cc_position' => Input::get('position'),
		 		'cc_division_head' => Input::get('div_head_id'),
		 		'cc_dept_head' => Input::get('dept_head_id'),
		 		'cc_immediate_head' => Input::get('supervisor_id'),
		 		'cc_date_hired' => Input::get('date_hired'),
		 		'cc_resignation_date' => date("Y-m-d",strtotime(Input::get('effective_date'))),
		 		'cc_ref_num' => Input::get('cc_ref_num'),
		 		'cc_status' => 2,
		 		'cc_others_status' => (isset($_POST['other_dept']) ? 0 :  1),
		 		'cc_create_date' => date("Y-m-d"),
		 		'cc_create_id' => Session::get("employee_id"),
		 		'cc_ref_num' => $reference
		 		);

		DB::beginTransaction();
		$store =ClearanceCertification::create($parameters);

		//INSERT AUDIT
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		if(isset($_POST['other_dept']))
		{
			$other_dep_emp = Employees::wherein("departmentid" , Input::get("other_dept") )
				->where("desig_level","head")
				->get(["departmentid","id"]);
			foreach ($other_dep_emp as $key => $value) {
				$emp_id[$value->departmentid] = $value->id;
			}

			foreach (Input::get("other_dept") as $key => $value) 
			{			
				$requirements = array();
				$employee_requirements = Input::get("employee_" . $value);
				if(Input::get("remarks_" . $value)):
				foreach (Input::get("remarks_" . $value) as  $index=>$remarks) 
				{
					if($employee_requirements[$index])
					{
						$requirements[$index]["employee"] = $employee_requirements[$index];
			            $requirements[$index]["applicable"] = "";
			            $requirements[$index]["date"] = "";
			            $requirements[$index]["amount"] = "";
			            $requirements[$index]["remarks"] = "";
			            $requirements[$index]["chrd_remarks"] = $remarks;	
			        }				
				}
				endif;

					if(!empty($requirements) &&  $value )
					{	
						if(!isset($emp_id[ $value ]))
						{
							DB::rollBack();
							return Redirect::to('cc/create')
			           			->with('errorMessage', 'ONE OF THE DEPARTMENT HAS NO DEPARTMENT HEAD.');
						}

						$other_dept["ccd_cc_id"] = $store->id;
						$other_dept["ccd_dep_id"] = $value;
						$other_dept["ccd_requirements"] = json_encode($requirements);
						$other_dept["ccd_assigned_id"] = $emp_id[ $value ];

						DB::table("clearancecertificationdepartments")->insert( $other_dept );
					}
			}
		}

        $emp = Employees::where("id",Input::get('employee'))
				->get(["firstname","email"])->toArray();

		//SEND EMAIL NOTIFICATION
		Mail::send('cc.email.create', array(
                        'reference' => $reference),
                        function($message ) use ($emp)
                        {
                            $message->to( $emp[0]["email"] , $emp[0]["firstname"])
                            	->subject("RGAS Notification Alert: Clearance Certification Acknowledgement");
                        }
                    );

		//INSERT LOGS
		ClearanceCertification::insert_logs($store->id,0,Input::get('employee'));

		//INSERT COMMENT
		ClearanceCertification::insert_comment($store->id , "Filer","All" , 1, Input::get("commentid"));

		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment($store->id , "Filer","All",$reference);

		DB::commit();

		if ($store) 
			return Redirect::to('cc/report')
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO RESIGNEE FOR ACKNOWLEDGEMENT.');
		else
			return Redirect::to('cc/create')
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_save()
	{	
		$reference = ClearanceCertification::get_reference_no();
		$parameters = array(
		 		'cc_emp_id' => Input::get('employee'),
		 		'cc_div_code' => Input::get('division'),
		 		'cc_emp_departmentid' => Input::get('department_id'),
		 		'cc_emp_sectionid' => Input::get('section'),
		 		'cc_position' => Input::get('position'),
		 		'cc_division_head' => Input::get('div_head_id'),
		 		'cc_dept_head' => Input::get('dept_head_id'),
		 		'cc_immediate_head' => Input::get('supervisor_id'),
		 		'cc_date_hired' => Input::get('date_hired'),
		 		'cc_resignation_date' => !Input::get('effective_date') ? null : date("Y-m-d",strtotime(Input::get('effective_date'))),
		 		'cc_ref_num' => Input::get('cc_ref_num'),
		 		'cc_status' => 0,
		 		'cc_ref_num' => ClearanceCertification::get_reference_no(),
		 		'cc_create_id' => Session::get("employee_id")
		 		);

		$store =ClearanceCertification::create($parameters);

		//INSERT AUDIT
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		if(isset($_POST['other_dept']))
		{
			foreach (Input::get("other_dept") as $key => $value) 
			{			
				if($value):
				$requirements = array();
				$employee_requirements = Input::get("employee_" . $value);
				foreach (Input::get("remarks_" . $value) as  $index=>$remarks) 
				{
				//	if($employee_requirements[$index])
				//	{
						$requirements[$index]["employee"] = $employee_requirements[$index];
			            $requirements[$index]["applicable"] = "";
			            $requirements[$index]["date"] = "";
			            $requirements[$index]["amount"] = "";
			            $requirements[$index]["remarks"] = "";
			            $requirements[$index]["chrd_remarks"] = $remarks;
				//	}		
				}

				//	if(!empty($requirements) &&  $value )
				//	{			
						$other_dept["ccd_cc_id"] = $store->id;
						$other_dept["ccd_dep_id"] = $value;
						$other_dept["ccd_requirements"] = json_encode($requirements);

						DB::table("clearancecertificationdepartments")->insert( $other_dept );
				//	}
				endif;
			}
		}

		//INSERT COMMENT
		ClearanceCertification::insert_comment($store->id , "Filer","All" , 0, Input::get("commentid"));

		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment($store->id , "Filer","All",$reference);

		if ($store)
			return Redirect::to('cc/report')
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
		else
			return Redirect::to('cc/create')
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
		
	}
	

	
	public function edit_sent($id)
	{	
		$parameters = array(
		 		'cc_status' => 2,
		 		'cc_others_status' => (isset($_POST['other_dept']) ? 0 :  1),
		 		'cc_create_date' => date("Y-m-d"),
		 		'cc_create_id' => Session::get("employee_id"),
		 		'cc_resignation_date' => date("Y-m-d",strtotime(Input::get('effective_date')))
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[3,0])
        		->update($parameters);

        //INSERT AUDIT
		$table = $this->table;
		$module = $this->module;
		$action = "AU004";
		$params = json_encode(["cc_id"=>$id]);
		$old["cc_status"] = 0;
		$new["cc_status"] = 1;
		$ref_num = Input::get("Reference_no");

		if(Input::get("old_effective_date") != Input::get("effective_date"))
		{
			$old["old_resignation_date"] = Input::get("old_effective_date");
			$new["new_resignation_date"] = Input::get("effective_date");
		}		

		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		if($old != $new) $this->AU004($id , json_encode($old) , json_encode($new) ,Input::get("reference") );



        DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)->delete();	

		if(isset($_POST['other_dept']))
		{
			$other_dep_emp = Employees::wherein("departmentid" , Input::get("other_dept") )
				->where("desig_level","head")
				->get(["departmentid","id"]);
			foreach ($other_dep_emp as $key => $value) {
				$emp_id[$value->departmentid] = $value->id;
			}

			foreach (Input::get("other_dept") as $key => $value) 
			{			
				$requirements = array();
				$employee_requirements = Input::get("employee_" . $value);
				if( Input::get("remarks_" . $value) ):
				foreach (Input::get("remarks_" . $value) as  $index=>$remarks) 
				{
					if($employee_requirements[$index])
					{
						$requirements[$index]["employee"] = $employee_requirements[$index];
			            $requirements[$index]["applicable"] = "";
			            $requirements[$index]["date"] = "";
			            $requirements[$index]["amount"] = "";
			            $requirements[$index]["remarks"] = "";
			            $requirements[$index]["chrd_remarks"] = $remarks;			
			        }		
				}
				endif;
					if(!empty($requirements) &&  $value )
					{
						if(!isset($emp_id[ $value ]))
						{
							return Redirect::to("cc/edit/$id")
			           			->with('errorMessage', 'One of the department has no department head.');
						}

						$other_dept["ccd_cc_id"] = $id;
						$other_dept["ccd_dep_id"] = $value;
						$other_dept["ccd_requirements"] = json_encode($requirements);
						$other_dept["ccd_assigned_id"] = $emp_id[ $value ];

						DB::table("clearancecertificationdepartments")->insert( $other_dept );
					}
			}
		}
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=","cc_emp_id")
				->get(["firstname","email","cc_emp_id"])->toArray();

		//SEND EMAIL NOTIFICATION
		Mail::send('cc.email.create', array(
                        'reference' => Input::get("reference")),
                        function($message ) use ($emp)
                        {
                            $message->to( $emp[0]["email"] , $emp[0]["firstname"])
                            	->subject("RGAS Notification Alert: Clearance Certification Acknowledgement");
                        }
                    );

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,0,$emp[0]["cc_emp_id"]);

		//INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Filer","All" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Filer","All");

		if ($store) 
			return Redirect::to("cc/report")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO RESIGNEE FOR ACKNOWLEDGEMENT.');
		else
			return Redirect::to("cc/edit/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function edit_save($id)
	{	
		$parameters = array('cc_resignation_date' => !Input::get('effective_date') ? null : date("Y-m-d",strtotime(Input::get('effective_date'))));

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[3,0])
        		->update($parameters);

        //INSERT AUDIT
        $old = json_encode(["cc_resignation_date"=>Input::get("old_effective_date")]);
		$new = json_encode(["cc_resignation_date"=>Input::get("effective_date")]);
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		if($old != $new) $this->AU004($id , $old , $new ,Input::get("reference") );


        DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)->delete();		

		if(isset($_POST['other_dept']))
		{
			$other_dep_emp = Employees::wherein("departmentid" , Input::get("other_dept") )
				->where("desig_level","head")
				->get(["departmentid","id"]);
			foreach ($other_dep_emp as $key => $value) {
				$emp_id[$value->departmentid] = $value->id;
			}

			foreach (Input::get("other_dept") as $key => $value) 
			{			
				if($value):
				$requirements = array();
				$employee_requirements = Input::get("employee_" . $value);
				foreach (Input::get("remarks_" . $value) as  $index=>$remarks) 
				{
			//		if($employee_requirements[$index])
				//	{
						$requirements[$index]["employee"] = $employee_requirements[$index];
			            $requirements[$index]["applicable"] = "";
			            $requirements[$index]["date"] = "";
			            $requirements[$index]["amount"] = "";
			            $requirements[$index]["remarks"] = "";
			            $requirements[$index]["chrd_remarks"] = $remarks;	
			 //       }				
				}

			//		if(!empty($requirements) &&  $value )
			//		{
						$other_dept["ccd_cc_id"] = $id;
						$other_dept["ccd_dep_id"] = $value;
						$other_dept["ccd_requirements"] = json_encode($requirements);
					//	$other_dept["ccd_assigned_id"] = $emp_id[ $value ];

						DB::table("clearancecertificationdepartments")->insert( $other_dept );
			//		}
				endif;
			}
		}

		//INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Filer","All" , 0 , Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Filer","All");

			return Redirect::to("cc/report")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
		
	}

	
	public function acknowledge_return($id)
	{	
		$parameters = array('cc_status' => 3);

		$loguser = Session::get("employee_id");
		$store =ClearanceCertification::where("cc_id" , $id)->where("cc_status",2)
        ->where("cc_emp_id" , $loguser)->update($parameters);

        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=","cc_create_id")
				->get(["firstname","email","cc_create_id"])->toArray();

		//SEND EMAIL NOTIFICATION
		Mail::send('cc.email.revision', array(
                        'reference' => Input::get("reference")),
                        function($message ) use ($emp)
                        {
                            $message->to( $emp[0]["email"] , $emp[0]["firstname"])
                            	->subject("RGAS Notification Alert: Clearance Certification Revision");
                        }
                    );

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2,$emp[0]["cc_create_id"]);

		//INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee","All" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee","All");

		//INSERT AUDIT
		$old = ["status"=>"For Acknowledgement"];
		$new = ["status"=> "new"];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store)
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO FILER.');
		else
			return Redirect::to("cc/acknowledge/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');	
	}

	
	public function acknowledge_sent($id)
	{	
		$time = date("Y-m-d H:i:s");
		$parameters = array('cc_status' => 4,"cc_acknowledgement_date"=>date("Y-m-d"));

		$loguser = Session::get("employee_id");
		
        //EMAIL to department head
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=","cc_dept_head")
				->get(["firstname","email","cc_dept_head"])->toArray();

		if(!isset($emp[0]["firstname"]))
		{
			return Redirect::to("cc/acknowledge/$id")
		           ->with('errorMessage', 'Resignee has no superior head.');
		}

		$store =ClearanceCertification::where("cc_id" , $id)->where("cc_status",2)
        ->where("cc_emp_id" , $loguser)->update($parameters);

		//SEND EMAIL NOTIFICATION
		ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment");

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3,$emp[0]["cc_dept_head"],$time);

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);

		//INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee","All" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee","All");

		//INSERT AUDIT
		$old = ["status"=>"For Acknowledgement"];
		$new = ["status"=> "For Assessment"];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		//EMAIL TO ALL RECEIVERS
		$receiver = Receivers::wherein("code",["CC_ERP_STAFF","CC_SSRV_STAFF","CC_SMDD_STAFF","CC_TOD1","CC_TOD2",
			"CC_CBR_PERSONNEL1","CC_CBR_PERSONNEL2","CC_CBR_MANAGER","CC_RECRUITMENT","CC_IT_HEAD"])
				->join("employees","employees.id","=","receivers.employeeid")
				->get(["firstname","email","code","receivers.employeeid"]);
		foreach ($receiver as $key => $rs) 
		{
			//SEND EMAIL NOTIFICATION
			ClearanceCertification::send_email($rs->firstname,$rs->email,"Assessment");

			//INSERT LOGS
			ClearanceCertification::insert_logs($id,3,$rs->employeeid,$time);
		}

		//EMAIL TO ALL OTHER DEPARTMENT HEAD
		$o_dept = DB::table("clearancecertificationdepartments")
				->where("ccd_cc_id",$id)
				->join("employees","employees.id","=","ccd_assigned_id")
				->get(["firstname","email","ccd_assigned_id"]);
		foreach ($o_dept as $key => $rs) 
		{
			//SEND EMAIL NOTIFICATION
			ClearanceCertification::send_email($rs->firstname,$rs->email,"Assessment");

			//INSERT LOGS
			ClearanceCertification::insert_logs($id,3,$rs->ccd_assigned_id,$time);
		}

		if ($store)
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO ALL CONCERNED DEPARTMENTS FOR PROCESSING.');
		else
			return Redirect::to("cc/acknowledge/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');	
	}

	
	public function chrd_return($id)
	{	
		switch (Input::get("personnel")) 
		{
			case 'training1':
					$field = "cc_training1_status";
					$emp_field = "cc_training1_assigned";
					$code = "CC_TOD1";
				break;
			case 'training2':
					$field = "cc_training2_status";
					$emp_field = "cc_training2_assigned";
					$code = "CC_TOD2";

				break;
			case 'personnel1':
					$field = "cc_cb_personnel1_status";
					$emp_field = "cc_cb_personnel1_assigned";
					$code = "CC_CBR_PERSONNEL1";
				break;
			case 'personnel2':
					$field = "cc_cb_personnel2_status";
					$emp_field = "cc_cb_personnel2_assigned";
					$code = "CC_CBR_PERSONNEL2";
				break;
			case 'recruitment':
					$field = "cc_recruitment_status";
					$emp_field = "cc_recruiment_assigned";
					$code = "CC_RECRUITMENT";
				break;
			case 'manager':
					$field = "cc_cb_manager_status";
					$emp_field = "cc_cb_manager_assigned";
					$code = "CC_CBR_MANAGER";
				break;
		}


		//SEND RETURN EMAIL
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", $emp_field)
				->get(["firstname","email",$emp_field])->toArray();

		//SEND EMAIL NOTIFICATION
		ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2,$emp[0][$emp_field]);

		//INSERT AUDIT
		$old = [ $emp_field=>Session::get("employee_id")];
		$new = [ $emp_field=>$emp[0][$emp_field]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		$parameters = array($field => 0);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein($field ,[2,1])
        		->update($parameters);


        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , ucwords( Input::get("personnel") ) ,"Staff" , 1, Input::get("commentid"),"remarks");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("cc/chrd/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function csdh_return($id)
	{	
		//UPDATE LOGS
		ClearanceCertification::update_logs($id);		
		switch (Input::get("personnel"))
		{
			case 'resigneesdepartment':
					$parameters = array("cc_resignee_status" => 3);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_resignee_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , "Resignee's Department" ,"CSDH" , 1, null,"remarks");

			        //EMAIL to department head
       				 $emp = Employees::where("cc_id",$id)
			        		->join("clearancecertification","employees.id","=","cc_dept_head")
							->get(["firstname","email","cc_dept_head"])->toArray();

					//INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["cc_dept_head"]);

					//INSERT AUDIT
					$old = [ "cc_resignee_assigned" =>Session::get("employee_id")];
					$new = [ "cc_resignee_assigned" =>$emp[0]["cc_dept_head"]];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

					//SEND EMAIL NOTIFICATION
					ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");
				break;
			case 'citd':
					$parameters = array("cc_technical_status" => 4);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_technical_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , "CITD" ,"CSDH" , 1, null,"remarks");

			        $emp = Employees::where("code","CC_CITD_HEAD")
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->get(["firstname","email","receivers.employeeid"])->toArray();

		            //INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["employeeid"]);

					//INSERT AUDIT
					$old = [ "cc_technical_assigned" =>Session::get("employee_id")];
					$new = [ "cc_technical_assigned" =>$emp[0]["employeeid"]];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		            //SEND EMAIL NOTIFICATION
		            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");
				break;
			case 'smdd':
					$parameters = array("cc_smd_status" => 2);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_smd_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , "SMDD" ,"CSDH" , 1, null,"remarks");

			        $emp = Employees::where("code","CC_SMDD_HEAD")
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->get(["firstname","email", "receivers.employeeid"])->toArray();

		            //INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["employeeid"]);

					//INSERT AUDIT
					$old = [ "cc_smd_assigned" =>Session::get("employee_id")];
					$new = [ "cc_smd_assigned" =>$emp[0]["employeeid"]];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		            //SEND EMAIL NOTIFICATION
		            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");
				break;
			case 'erpd':
					$parameters = array("cc_erp_status" => 2);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_erp_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT 
			        ClearanceCertification::insert_comment($id , "ERPD" ,"CSDH" , 1, null,"remarks");

			        $emp = Employees::where("code","CC_ERP_HEAD")
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->get(["firstname","email", "receivers.employeeid"])->toArray();

		            //INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["employeeid"]);

					//INSERT AUDIT
					$old = [ "cc_erp_assigned" =>Session::get("employee_id")];
					$new = [ "cc_erp_assigned" =>$emp[0]["employeeid"]];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		            //SEND EMAIL NOTIFICATION
		            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");
				break;
			case 'ssrv':
					$parameters = array("cc_ssrv_status" => 2);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_ssrv_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , "SSRV" ,"CSDH" , 1, null,"remarks");

			        $emp = Employees::where("code","CC_SSRV_HEAD")
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->get(["firstname","email", "receivers.employeeid"])->toArray();

		            //INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["employeeid"]);

		            //SEND EMAIL NOTIFICATION
		            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

		            //INSERT AUDIT
					$old = [ "cc_ssrv_assigned" =>Session::get("employee_id")];
					$new = [ "cc_ssrv_assigned" =>$emp[0]["employeeid"]];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );
				break;
			case 'chrd':
					$parameters = array("cc_chrd_status" => 2);
					$store =ClearanceCertification::where("cc_id" , $id)->where("cc_chrd_status" ,1)
        				->update($parameters);

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , "CHRD" ,"CSDH" , 1, null,"remarks");

			        $emp = Employees::where("code","CC_CHRD_HEAD")
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->get(["firstname","email", "receivers.employeeid"])->toArray();

		            //INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]["employeeid"]);

		            //SEND EMAIL NOTIFICATION
		            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");


				break;
			case 'others':
					$parameters = array("cc_others_status" => 0);
					ClearanceCertification::where("cc_id" , $id)->where("cc_others_status" ,1)
        				->update($parameters);

        			$parameters = array("ccd_status" => 3);	
        			$store = DB::table("clearancecertificationdepartments")
        				->where("ccd_dep_id",Input::get("primary"))
        				->where("ccd_cc_id", $id)
        				->update($parameters);;

        			//INSERT COMMENT
			        ClearanceCertification::insert_comment($id , Input::get("primary") ,"CSDH" , 1, null,"remarks");

			        $emp = DB::table("clearancecertificationdepartments")
						->where("ccd_cc_id",$id)
						->where("ccd_dep_id",Input::get("primary"))
						->join("employees","employees.id","=","ccd_assigned_id")
						->get(["firstname","email","ccd_assigned_id"]);


					//INSERT LOGS
					ClearanceCertification::insert_logs($id,2,$emp[0]->ccd_assigned_id);

					//INSERT AUDIT
					$old = [ "ccd_assigned_id" =>Session::get("employee_id")];
					$new = [ "ccd_assigned_id" =>$emp[0]->ccd_assigned_id];
					$this->setTable('clearancecertification');
					$this->setPrimaryKey('cc_id');
					$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

					//SEND EMAIL NOTIFICATION
					ClearanceCertification::send_email($emp[0]->firstname,$emp[0]->email,"Revision");
				break;
		}


	//	if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO DEPARTMENT HEAD.');
		// else
		// 	return Redirect::to("cc/csdh/$id")
		//            ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function chrd_send($id)
	{
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 1;
		$parameters["cc_chrd_status"] = 1;
		$parameters["cc_training1_status"] = 1;
		$parameters["cc_recruitment_status"] = 1;
		$parameters["cc_cb_personnel1_status"] = 1;
		$parameters["cc_training2_status"] = 1;
		$parameters["cc_cb_personnel2_status"] = 1;
		$parameters["cc_cb_manager_status"] = 1;

		$store =ClearanceCertification::where("cc_id" , $id)
				->whereIn("cc_chrd_status" ,[1,2])
				->whereIn("cc_training1_status",[1,2])
                ->whereIn("cc_cb_personnel1_status",[1,2])
                ->whereIn("cc_recruitment_status",[1,2])
                ->whereIn("cc_training2_status",[1,2])
                ->whereIn("cc_cb_personnel2_status",[1,2])
                ->whereIn("cc_cb_manager_status",[1,2])
        		->update($parameters);

        $for_csdh = ClearanceCertification::update_clearance_status($id);

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		

         //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CHRD","Head" , 1, Input::get("commentid"));

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("cc/chrd/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function csdh_send($id)
	{
		$parameters = array("cc_status" => 1);

		$store =ClearanceCertification::where("cc_id" , $id)
				->where("cc_status" ,5)
        		->update($parameters);


        $emp = Employees::where("code","CC_CBR_FILER")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email","receivers.employeeid"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,1,$emp[0]["employeeid"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Approved"); 

         //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CHRD","Head" , 1, Input::get("commentid"));

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CHRD FOR FINAL PROCESS.');
		else
			return Redirect::to("cc/chrd/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function department_head_sent($id)
	{	
		$data = ClearanceCertification::update_department();
		extract($data);

		$parameters = array(
		 		'ccd_status' => 1,
		 		'ccd_requirements' => json_encode($requirements) , 
		 		'ccd_others' => json_encode($others),
		 		'ccd_date' => date("Y-m-d")
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 0 )->update($parameters);

        $check = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
        		->wherein("ccd_status",[0,2,3])->get();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        if(empty($check))
        {
        	ClearanceCertification::where("cc_id" , $id)->update(["cc_others_status"=>1]);
        	$for_csdh = ClearanceCertification::update_clearance_status($id,"ccd_assigned_id");
        }

         //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid ,"Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Head");



		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW');
		else
			return Redirect::to("cc/department/head/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function department_head_assign($id)
	{	
		$data = ClearanceCertification::update_department();
		extract($data);
		
		$parameters = array(
		 		'ccd_status' => 2,
		 		'ccd_requirements' => json_encode($requirements) , 
		 		'ccd_others' => json_encode($others),
		 		'ccd_assigned_id' => Input::get("assign")
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 0 )->update($parameters);  

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3,Input::get("assign"));

		$emp = Employees::where("id",Input::get("assign"))
                ->get(["firstname","email"])->toArray();

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment"); 

		 //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid , "Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Head");

		//INSERT AUDIT
		$old = ["ccd_assigned_id"=>Session::get("employee_id")];
		$new = ["ccd_assigned_id"=> Input::get("assign")];
		$this->setTable('clearancecertificationdepartments');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO ASSIGNED PERSONNEL FOR ASSESSMENT.');
		else
			return Redirect::to("cc/department/head/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function department_personnel_save($id)
	{	
		$data = ClearanceCertification::update_department();
		extract($data);
		
		$parameters = array(
		 		'ccd_requirements' => json_encode($requirements) , 
		 		'ccd_others' => json_encode($others)
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 2 )->update($parameters);  

		 //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid ,"Staff" , 0, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Staff");

		//cc/department/personnel/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	
	}

	
	public function department_personnel_send($id)
	{	
		$data = ClearanceCertification::update_department();
		extract($data);
		
		$parameters = array(
				'ccd_status' => 3,
		 		'ccd_requirements' => json_encode($requirements) , 
		 		'ccd_others' => json_encode($others),
		 		'ccd_date' => date("Y-m-d")
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 2 )->update($parameters);  

		$emp = Employees::where("departmentid", $deptid )
				->where("desig_level" , "head")
                ->get(["firstname","email","id"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["id"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment"); 

		 //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid ,"Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Staff");

		//INSERT AUDIT
		$old = ["ccd_assigned_id"=>Session::get("employee_id")];
		$new = ["ccd_assigned_id"=> $emp[0]["id"]];
		$this->setTable('clearancecertificationdepartments');
		$this->setPrimaryKey('ccd_cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function department_review_sent($id)
	{	
		$parameters = array(
				'ccd_status' => 1
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 3 )->update($parameters);  

		$check = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
        		->wherein("ccd_status",[0,2,3])->get();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        if(empty($check))
        {
        	ClearanceCertification::where("cc_id" , $id)->update(["cc_others_status"=>1]);
        	$for_csdh = ClearanceCertification::update_clearance_status($id,"ccd_assigned_id");
        }	

         //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid ,"Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function department_review_return($id)
	{	
		$parameters = array(
				'ccd_status' => 2
		 		);

		$deptid = Session::get("dept_id");
		$store = DB::table("clearancecertificationdepartments")->where("ccd_cc_id" , $id)
			->where("ccd_dep_id", $deptid )->where("ccd_status", 3 )->update($parameters);  

		 //INSERT LOGS
		//ClearanceCertification::insert_logs($id , 4);

		$emp = Employees::where("ccd_cc_id",$id)
                ->join("clearancecertificationdepartments","employees.id","=", "ccd_assigned_id")
                ->get(["firstname","email","ccd_assigned_id"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2, $emp[0]["ccd_assigned_id"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision"); 

		 //INSERT COMMENT
		ClearanceCertification::insert_comment($id , $deptid ,"Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , $deptid ,"Head");

		//INSERT AUDIT
		$old = ["ccd_assigned_id"=>Session::get("employee_id")];
		$new = ["ccd_assigned_id"=>$emp[0]["ccd_assigned_id"]];
		$this->setTable('clearancecertificationdepartments');
		$this->setPrimaryKey('ccd_cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_head_sent($id)
	{	
		$data = ClearanceCertification::update_resignee();
		extract($data);

		$parameters = array(
		 		'cc_resignee_status' => 1,
		 		'cc_manual_surrendered' => json_encode($manual) , 
		 		'cc_workfiles' => json_encode($workfiles) , 
		 		'cc_drawer' => json_encode($drawer) , 
		 		'cc_fixed_assets' => json_encode($assets) , 
		 		'cc_office_supplies' => json_encode($office) , 
		 		'cc_accountableforms' => json_encode($accountable) , 
		 		'cc_revolvingfund' => json_encode($revolving) , 
		 		'cc_companycar' => json_encode($car) , 
		 		'cc_other_resignee_deliverables' => json_encode($others),
		 		'cc_resignee_assigned' =>  Session::get("employee_id"),
		 		'cc_resignee_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);
       
        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_resignee_assigned");

		//INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("cc/resignee/head/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_head_assign($id)
	{	
		$data = ClearanceCertification::update_resignee();
		extract($data);
		
		$parameters = array(
		 		'cc_resignee_status' => 2,
		 		'cc_manual_surrendered' => json_encode($manual) , 
		 		'cc_workfiles' => json_encode($workfiles) , 
		 		'cc_drawer' => json_encode($drawer) , 
		 		'cc_fixed_assets' => json_encode($assets) , 
		 		'cc_office_supplies' => json_encode($office) , 
		 		'cc_accountableforms' => json_encode($accountable) , 
		 		'cc_revolvingfund' => json_encode($revolving) , 
		 		'cc_companycar' => json_encode($car) , 
		 		'cc_other_resignee_deliverables' => json_encode($others),
		 		'cc_resignee_assigned' => Input::get("assign")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);    

        $emp = Employees::where("id",Input::get("assign"))
                ->get(["firstname","email","id"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3, $emp[0]["id"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment"); 

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Head" , 1, Input::get("commentid"));  

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Head");

		//INSERT AUDIT
		$old = ["cc_resignee_assigned"=>Session::get("employee_id")];
		$new = ["cc_resignee_assigned"=>Input::get("assign")];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO ASSIGNED PERSONNEL FOR ASSESSMENT.');
		else
			return Redirect::to("cc/resignee/head/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_supervisor_save($id)
	{	
		$data = ClearanceCertification::update_resignee();
		extract($data);
		
		$parameters = array(				
		 		'cc_manual_surrendered' => json_encode($manual) , 
		 		'cc_workfiles' => json_encode($workfiles) , 
		 		'cc_drawer' => json_encode($drawer) , 
		 		'cc_fixed_assets' => json_encode($assets) , 
		 		'cc_office_supplies' => json_encode($office) , 
		 		'cc_accountableforms' => json_encode($accountable) , 
		 		'cc_revolvingfund' => json_encode($revolving) , 
		 		'cc_companycar' => json_encode($car) , 
		 		'cc_other_resignee_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Staff" , 0, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Staff");

		//cc/resignee/supervisor/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
		}

	
	public function resignee_supervisor_sent($id)
	{	
		$data = ClearanceCertification::update_resignee();
		extract($data);
		
		$parameters = array(
				'cc_resignee_status' => 3,
		 		'cc_manual_surrendered' => json_encode($manual) , 
		 		'cc_workfiles' => json_encode($workfiles) , 
		 		'cc_drawer' => json_encode($drawer) , 
		 		'cc_fixed_assets' => json_encode($assets) , 
		 		'cc_office_supplies' => json_encode($office) , 
		 		'cc_accountableforms' => json_encode($accountable) , 
		 		'cc_revolvingfund' => json_encode($revolving) , 
		 		'cc_companycar' => json_encode($car) , 
		 		'cc_other_resignee_deliverables' => json_encode($others),
		 		'cc_resignee_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       


        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=","cc_dept_head")
				->get(["firstname","email","cc_dept_head"])->toArray();

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3, $emp[0]["cc_dept_head"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment"); 

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_resignee_assigned"=>Session::get("employee_id")];
		$new = ["cc_resignee_assigned"=>$emp[0]["cc_dept_head"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("cc/resignee/supervisor/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_review_return($id)
	{	
	
		$parameters = array(
				'cc_resignee_status' => 2
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       


        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", "cc_resignee_assigned")
				->get(["firstname","email","cc_resignee_assigned"])->toArray();

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2, $emp[0]["cc_resignee_assigned"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Head");

		//INSERT AUDIT
		$old = ["cc_resignee_assigned"=>Session::get("employee_id")];
		$new = ["cc_resignee_assigned"=>$emp[0]["cc_resignee_assigned"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function resignee_review_sent($id)
	{		
		$parameters = array(
				'cc_resignee_status' => 1,
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_resignee_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "Resignee's Department","Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Resignee's Department" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_technical_assign($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
				'cc_technical_status' => 2,
		 		'cc_revoke_internet' => json_encode($internet) , 
		 		'cc_revoke_intranet' => json_encode($intranet) , 
		 		'cc_revoke_internal' => json_encode($internal) , 
		 		'cc_revoke_external' => json_encode($external) , 
		 		'cc_remove_sms' => json_encode($sms) , 
		 		'cc_pc_laptop' => json_encode($laptop) ,
		 		'cc_other_it_deliverables' => json_encode($others),
		 		'cc_technical_assigned' => Input::get("assign")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       


        $emp = Employees::where("id",Input::get("assign"))
                ->get(["firstname","email","id"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3, $emp[0]["id"]);

         //SEND EMAIL NOTIFICATION
         ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment"); 

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>Input::get("assign")];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO ASSIGNED PERSONNEL FOR ASSESSMENT');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_technical_send($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
				'cc_technical_status' => 4,
		 		'cc_revoke_internet' => json_encode($internet) , 
		 		'cc_revoke_intranet' => json_encode($intranet) , 
		 		'cc_revoke_internal' => json_encode($internal) , 
		 		'cc_revoke_external' => json_encode($external) , 
		 		'cc_remove_sms' => json_encode($sms) , 
		 		'cc_pc_laptop' => json_encode($laptop) ,
		 		'cc_other_it_deliverables' => json_encode($others),
		 		'cc_technical_assigned' =>  Session::get("employee_id"),
		 		'cc_technical_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);   


        //SEND  EMAIL
        $emp = Employees::where("code","CC_CITD_HEAD")
            ->join("receivers","employees.id","=", "receivers.employeeid")
            ->get(["firstname","email","receivers.employeeid"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));    

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_staff_save($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
		 		'cc_revoke_internet' => json_encode($internet) , 
		 		'cc_revoke_intranet' => json_encode($intranet) , 
		 		'cc_revoke_internal' => json_encode($internal) , 
		 		'cc_revoke_external' => json_encode($external) , 
		 		'cc_remove_sms' => json_encode($sms) , 
		 		'cc_pc_laptop' => json_encode($laptop) ,
		 		'cc_other_it_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);  

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 0, Input::get("commentid"));     

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//cc/citd/staff/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	}

	
	public function citd_staff_send($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
				'cc_technical_status' => 3,
		 		'cc_revoke_internet' => json_encode($internet) , 
		 		'cc_revoke_intranet' => json_encode($intranet) , 
		 		'cc_revoke_internal' => json_encode($internal) , 
		 		'cc_revoke_external' => json_encode($external) , 
		 		'cc_remove_sms' => json_encode($sms) , 
		 		'cc_pc_laptop' => json_encode($laptop) ,
		 		'cc_other_it_deliverables' => json_encode($others),
		 		'cc_technical_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //SEND  EMAIL
        $emp = Employees::where("code","CC_IT_HEAD")
            ->join("receivers","employees.id","=", "receivers.employeeid")
            ->get(["firstname","email","receivers.employeeid"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3, $emp[0]["employeeid"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO IT TECHNICAL HEAD FOR REVIEW');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_technical_review_return($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
				'cc_technical_status' => 2,
		 		// 'cc_revoke_internet' => json_encode($internet) , 
		 		// 'cc_revoke_intranet' => json_encode($intranet) , 
		 		// 'cc_revoke_internal' => json_encode($internal) , 
		 		// 'cc_revoke_external' => json_encode($external) , 
		 		// 'cc_remove_sms' => json_encode($sms) , 
		 		// 'cc_pc_laptop' => json_encode($laptop) ,
		 		// 'cc_other_it_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);     

        //SEND  EMAIL
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", "cc_technical_assigned")
				->get(["firstname","email","cc_technical_assigned"])->toArray();

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2, $emp[0]["cc_technical_assigned"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));  

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>$emp[0]["cc_technical_assigned"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_technical_review_send($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		$parameters = array(
				'cc_technical_status' => 4,
		 		// 'cc_revoke_internet' => json_encode($internet) , 
		 		// 'cc_revoke_intranet' => json_encode($intranet) , 
		 		// 'cc_revoke_internal' => json_encode($internal) , 
		 		// 'cc_revoke_external' => json_encode($external) , 
		 		// 'cc_remove_sms' => json_encode($sms) , 
		 		// 'cc_pc_laptop' => json_encode($laptop) ,
		 		// 'cc_other_it_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //SEND  EMAIL
        $emp = Employees::where("code","CC_CITD_HEAD")
            ->join("receivers","employees.id","=", "receivers.employeeid")
            ->get(["firstname","email","receivers.employeeid"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assessment");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_head_review_assign($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 2;
		$parameters["cc_technical_status"] = (Input::get("code") == "CC_CITD_STAFF" ? 2 : 0);
		$parameters["cc_technical_assigned"] = Input::get("assign");

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);    


        //SEND  EMAIL
        $emp = Employees::where("id",Input::get("assign"))
            ->get(["firstname","email","id"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,3, $emp[0]["id"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Staff" , 1, Input::get("commentid"));   

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Head");

		//INSERT AUDIT
		$old = ["cc_technical_assigned"=>Session::get("employee_id")];
		$new = ["cc_technical_assigned"=>Input::get("assign")];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO ASSIGNED PERSONNEL FOR ASSESSMENT');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function citd_head_review_send($id)
	{	
		$data = ClearanceCertification::update_citd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 1;
		$parameters["cc_technical_status"] = 1;
		
		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_technical_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "CITD","Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "CITD" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function smdd_staff_send($id)
	{	
		$data = ClearanceCertification::update_smdd();
		extract($data);
		
		$parameters = array(
				'cc_smd_status' => 2,
		 		'cc_revoke_rgas' => json_encode($rgas) , 
		 		'cc_other_smd_deliverables' => json_encode($others),
		 		'cc_smd_assigned' =>  Session::get("employee_id"),
		 		'cc_smd_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

		//SEND  EMAIL
            $emp = Employees::where("code","CC_SMDD_HEAD")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email","receivers.employeeid"])->toArray();

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Approval");        

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "SMDD","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SMDD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_smd_assigned"=>Session::get("employee_id")];
		$new = ["cc_smd_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function smdd_staff_save($id)
	{	
		$data = ClearanceCertification::update_smdd();
		extract($data);
		
		$parameters = array(
		 		'cc_revoke_rgas' => json_encode($rgas) , 
		 		'cc_other_smd_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);      

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "SMDD","Staff" , 0, Input::get("commentid")); 

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SMDD" ,"Staff");

	//cc/smdd/staff/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	}

	
	public function smdd_head_review_return($id)
	{	
		$data = ClearanceCertification::update_smdd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 2;
		$parameters["cc_smd_status"] = 0;

		$parameters = array(
				'cc_smd_status' => 0
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       


        //SEND  EMAIL
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", "cc_smd_assigned")
				->get(["firstname","email","cc_smd_assigned"])->toArray();

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,2, $emp[0]["cc_smd_assigned"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "SMDD","Staff" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SMDD" ,"Head");

		//INSERT AUDIT
		$old = ["cc_smd_assigned"=>Session::get("employee_id")];
		$new = ["cc_smd_assigned"=>$emp[0]["cc_smd_assigned"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function smdd_head_review_send($id)
	{	
		$data = ClearanceCertification::update_smdd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 1;
		$parameters["cc_smd_status"] = 1;

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);      

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_smd_assigned"); 

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "SMDD","Head" , 1, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SMDD" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function erpd_staff_save($id)
	{	
		$data = ClearanceCertification::update_erpd();
		extract($data);
		
		$parameters = array(
		 		'cc_revoke_sap' => json_encode($sap) , 
		 		'cc_other_erp_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
		ClearanceCertification::insert_comment($id , "ERPD","Staff" , 0, Input::get("commentid"));

		//DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "ERPD" ,"Staff");

		//cc/erpd/staff/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	
	}

	
	public function erpd_staff_send($id)
	{	
		$data = ClearanceCertification::update_erpd();
		extract($data);
		
		$parameters = array(
				'cc_erp_status' => 2,
		 		'cc_revoke_sap' => json_encode($sap) , 
		 		'cc_other_erp_deliverables' => json_encode($others),
		 		'cc_erp_assigned' =>  Session::get("employee_id"),
		 		'cc_erp_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

		//SEND  EMAIL
            $emp = Employees::where("code","CC_ERP_HEAD")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email", "receivers.employeeid"])->toArray();

         //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);

            //SEND EMAIL NOTIFICATION
            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Approval");        		

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "ERPD","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "ERPD" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_erp_assigned"=>Session::get("employee_id")];
		$new = ["cc_erp_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	

	
	public function erpd_head_review_return($id)
	{	
		$data = ClearanceCertification::update_erpd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 2;
		$parameters["cc_erp_status"] = 0;

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //SEND  EMAIL
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", "cc_erp_assigned")
				->get(["firstname","email","cc_erp_assigned"])->toArray();

		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["cc_erp_assigned"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "ERPD","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "ERPD" ,"Head");

		//INSERT AUDIT
		$old = ["cc_erp_assigned"=>Session::get("employee_id")];
		$new = ["cc_erp_assigned"=>$emp[0]["cc_erp_assigned"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function erpd_head_review_send($id)
	{	
		$data = ClearanceCertification::update_erpd();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 1;
		$parameters["cc_erp_status"] = 1;

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_erp_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "ERPD","Head" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "ERPD" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}


	
	public function ssrv_staff_send($id)
	{	
		$data = ClearanceCertification::update_ssrv();
		extract($data);
		
		$parameters = array(
				'cc_ssrv_status' => 2,
		 		'cc_unpaid_vale' => json_encode($vale) , 
		 		'cc_other_ssrv_deliverables' => json_encode($others),
		 		'cc_ssrv_assigned' =>  Session::get("employee_id"),
		 		'cc_ssrv_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //SEND  EMAIL
            $emp = Employees::where("code","CC_SSRV_HEAD")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email" , "receivers.employeeid"])->toArray();

         //UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Approval");

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "SSRV","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SSRV" ,"Staff");

		//INSERT AUDIT
		$old = ["cc_ssrv_assigned"=>Session::get("employee_id")];
		$new = ["cc_ssrv_assigned"=>$emp[0]["employeeid"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function ssrv_staff_save($id)
	{	
		$data = ClearanceCertification::update_ssrv();
		extract($data);
		
		$parameters = array(
		 		'cc_unpaid_vale' => json_encode($vale) , 
		 		'cc_other_ssrv_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);      

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "SSRV","Staff" , 0, Input::get("commentid")); 

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SSRV" ,"Staff");

	//cc/ssrv/staff/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	
	}

	
	public function ssrv_head_review_send($id)
	{	
		$data = ClearanceCertification::update_ssrv();
		extract($data);
		
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 1;
		$parameters["cc_ssrv_status"] = 1;

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        $for_csdh = ClearanceCertification::update_clearance_status($id,"cc_ssrv_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "SSRV","Head" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SSRV" ,"Head");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO CORPORATE SERVICES DIVISION HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function ssrv_head_review_return($id)
	{	
		$data = ClearanceCertification::update_ssrv();
		extract($data);
		
		if(Input::get("resignee_head")) $parameters["cc_resignee_status"] = 2;
		$parameters["cc_ssrv_status"] = 0;

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);      

        //SEND  EMAIL
        $emp = Employees::where("cc_id",$id)
        		->join("clearancecertification","employees.id","=", "cc_ssrv_assigned")
				->get(["firstname","email","cc_ssrv_assigned"])->toArray();
	
		//UPDATE LOGS
		ClearanceCertification::update_logs($id);
		//INSERT LOGS
		ClearanceCertification::insert_logs($id,4, $emp[0]["cc_ssrv_assigned"]);		

        //SEND EMAIL NOTIFICATION
        ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Revision");

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "SSRV","Staff" , 1, Input::get("commentid")); 

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "SSRV" ,"Head");

		//INSERT AUDIT
		$old = ["cc_ssrv_assigned"=>Session::get("employee_id")];
		$new = ["cc_ssrv_assigned"=>$emp[0]["cc_ssrv_assigned"]];
		$this->setTable('clearancecertification');
		$this->setPrimaryKey('cc_id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY RETURNED TO PERSONNEL.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function chrd_cbr_personnel1_save($id)
	{	
		$data = ClearanceCertification::update_cb1();
		extract($data);
		
		$parameters = array(
		 		'cc_hmo_card' => json_encode($hmo_surrender) , 
		 		'cc_hmo_cancellation' => json_encode($hmo_cancel) , 
		 		'cc_id_card' => json_encode($card) , 
		 		'cc_building_access' => json_encode($building) , 
		 		'cc_other_cb_personnel1_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Personnel1","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Personnel1" ,"Staff");

		//cc/chrd/cbr/personnel1/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	}

	
	public function chrd_cbr_personnel1_send($id)
	{	
		$data = ClearanceCertification::update_cb1();
		extract($data);
		
		$parameters = array(
				'cc_cb_personnel1_status' => 2,
		 		'cc_hmo_card' => json_encode($hmo_surrender) , 
		 		'cc_hmo_cancellation' => json_encode($hmo_cancel) , 
		 		'cc_id_card' => json_encode($card) , 
		 		'cc_building_access' => json_encode($building) , 
		 		'cc_other_cb_personnel1_deliverables' => json_encode($others),
		 		'cc_cb_personnel1_assigned' =>  Session::get("employee_id"),
		 		'cc_cb_personnel1_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        		ClearanceCertification::update_chrd_status($id,"cc_cb_personnel1_assigned");

       //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Personnel1","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
		//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Personnel1" ,"Staff");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function chrd_cbr_personnel2_save($id)
	{	
		$data = ClearanceCertification::update_cb2();
		extract($data);
		
		$parameters = array(
		 		'cc_outstanding_sss' => json_encode($sss) , 
		 		'cc_outstanding_aub' => json_encode($aub) , 
		 		'cc_outstanding_hmo' => json_encode($hmo) , 
		 		'cc_other_cb_personnel2_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Personnel2","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Personnel2" ,"Staff");

		//cc/chrd/cbr/personnel2/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');

	}

	
	public function chrd_cbr_personnel2_send($id)
	{	
		$data = ClearanceCertification::update_cb2();
		extract($data);
		
		$parameters = array(
				'cc_cb_personnel2_status' => 2,
		 		'cc_outstanding_sss' => json_encode($sss) , 
		 		'cc_outstanding_aub' => json_encode($aub) , 
		 		'cc_outstanding_hmo' => json_encode($hmo) , 
		 		'cc_other_cb_personnel2_deliverables' => json_encode($others),
		 		'cc_cb_personnel2_assigned' =>  Session::get("employee_id"),
		 		'cc_cb_personnel2_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);      

        		ClearanceCertification::update_chrd_status($id,"cc_cb_personnel2_assigned"); 

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Personnel2","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Personnel2" ,"Staff");

			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');

	}


	
	public function chrd_cbr_manager_save($id)
	{	
		$data = ClearanceCertification::update_cbmanager();
		extract($data);
		
		$parameters = array(
		 		'cc_staff_employee' => json_encode($staff) , 
		 		'cc_other_cb_manager_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Manager","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Manager" ,"Staff");

		//cc/chrd/cbr/manager/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');

	}

	
	public function chrd_cbr_manager_send($id)
	{	
		$data = ClearanceCertification::update_cbmanager();
		extract($data);
		
		$parameters = array(
				'cc_cb_manager_status' => 2,
		 		'cc_staff_employee' => json_encode($staff) , 
		 		'cc_other_cb_manager_deliverables' => json_encode($others),
		 		'cc_cb_manager_assigned' =>  Session::get("employee_id"),
		 		'cc_cb_manager_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        		ClearanceCertification::update_chrd_status($id,"cc_cb_manager_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

         //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Manager","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Manager" ,"Staff");


			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');

	}

	
	public function chrd_recruitment_save($id)
	{	
		$data = ClearanceCertification::update_recruitment();
		extract($data);
		
		$parameters = array(
		 		'cc_biometrics' => json_encode($biometrics) , 
		 		'cc_other_recruitment' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

         //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Recruitment","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Recruitment" ,"Staff");

		//cc/chrd/recruitment/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');

	}

	
	public function chrd_recruitment_send($id)
	{	
		$data = ClearanceCertification::update_recruitment();
		extract($data);
		
		$parameters = array(
				'cc_recruitment_status' => 2,
		 		'cc_biometrics' => json_encode($biometrics) , 
		 		'cc_other_recruitment' => json_encode($others),
		 		'cc_recruiment_assigned' =>  Session::get("employee_id"),
		 		'cc_recruitment_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        		ClearanceCertification::update_chrd_status($id,"cc_recruiment_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Recruitment","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Recruitment" ,"Staff");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function chrd_training1_save($id)
	{	
		$data = ClearanceCertification::update_training1();
		extract($data);
		
		$parameters = array(
		 		'cc_conducted_exit' => json_encode($exit) , 
		 		'cc_other_training1_deliverables' => json_encode($others)
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Training1","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Training1" ,"Staff");

		//cc/chrd/training1/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');

	}

	
	public function chrd_training1_send($id)
	{	
		$data = ClearanceCertification::update_training1();
		extract($data);
		
		$parameters = array(
				'cc_conducted_exit' => json_encode($exit) , 
		 		'cc_other_training1_deliverables' => json_encode($others),
		 		'cc_training1_status' => 2,
		 		'cc_training1_assigned' =>  Session::get("employee_id"),
		 		'cc_training1_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        		ClearanceCertification::update_chrd_status($id,"cc_training1_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Training1","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Training1" ,"Staff");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	
	public function chrd_training2_save($id)
	{	
		$data = ClearanceCertification::update_training2();
		extract($data);
		
		$parameters = array(
		 		'cc_service_aggreement' => json_encode($service) , 
		 		'cc_training_materials' => json_encode($material) , 
		 		'cc_training_equipment' => json_encode($equipment) , 
		 		'cc_other_training2_deliverables' => json_encode($others),
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Training2","Staff" , 0, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Training2" ,"Staff");

		//cc/chrd/training2/$id
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SAVED.');
	
	}

	
	public function chrd_training2_send($id)
	{	
		$data = ClearanceCertification::update_training2();
		extract($data);
		
		$parameters = array(
				'cc_service_aggreement' => json_encode($service) , 
		 		'cc_training_materials' => json_encode($material) , 
		 		'cc_training_equipment' => json_encode($equipment) , 
		 		'cc_other_training2_deliverables' => json_encode($others),
		 		'cc_training2_status' => 2,
		 		'cc_training2_assigned' =>  Session::get("employee_id"),
		 		'cc_training2_date' => date("Y-m-d")
		 		);

		$store =ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[5,4])
        		->update($parameters);       

        		ClearanceCertification::update_chrd_status($id,"cc_training2_assigned");

        //UPDATE LOGS
		ClearanceCertification::update_logs($id);

        //INSERT COMMENT
        ClearanceCertification::insert_comment($id , "Training2","Staff" , 1, Input::get("commentid"));

        //DELETE ATTACHMENT
		ClearanceCertification::delete_attachment( $id );
		
//INSERT ATTACHMENT
		ClearanceCertification::insert_attachment( $id , "Training2" ,"Staff");

		if ($store) 
			return Redirect::to("")
		           ->with('successMessage', 'CLEARANCE CERTIFICATION SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW.');
		else
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
	}

	

	public function get_resignee_record()
	{	
		$requests = ClearanceCertification::where("cc_emp_id" , Session::get("employee_id"))
			->join("employees","employees.id","=","cc_create_id")
			->where("cc_status",2)
			//->limit(5)
			->get();

		$result_data["aaData"] = array();
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
    	$result_data["iTotalRecords"] = count($requests);
    	if ( count($requests) > 0)
        {
                $ctr = 0;
                foreach($requests as $req) 
                 {		

                    $result_data["aaData"][$ctr][] = $req->cc_ref_num;
    				$result_data["aaData"][$ctr][] = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) ;
    				$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
					$result_data["aaData"][$ctr][] = $req->cc_create_date;
    				$result_data["aaData"][$ctr][] = "For Acknowledgement";		
    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
    				$result_data["aaData"][$ctr][] = "
    					<a ' class='btn btn-default btndefault' href='" . url("cc/acknowledge/$req->cc_id") . "' >VIEW</a>
    					";
                    $ctr++;
                 }
        }
        else
        {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}

	public function get_filer_record()
	{	
		$result_data["aaData"] = array();
		//CHECK FIRST, IF USER IS CC CSDH
		if(Session::get("is_cc_cbr_filer"))
		{
			$requests = ClearanceCertification::whereIn("cc_status",[3])
				->where("cc_create_id" , Session::get("employee_id"))
				->join("employees","employees.id","=","cc_emp_id")
				->join("departments","departments.id","=","cc_emp_departmentid")
			//	->limit(5)
				->get(["cc_ref_num","dept_name","cc_create_date","cc_id","cc_resignation_date", "lastname","firstname","cc_status"]);

			
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    	if ( count($requests) > 0)
	        {
	        		$status[5] = "FOR APPROVAL";
	        		$status[3] = "RETURNED";
	        		
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {		

	                    $result_data["aaData"][$ctr][] = $req->cc_ref_num;
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
	    				$result_data["aaData"][$ctr][] = $req->dept_name;
	    				$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
	    				$result_data["aaData"][$ctr][] = $status[$req->cc_status];		
	    				
	    				if($req->cc_status == 3)
	    				{
	    					$result_data["aaData"][$ctr][]= "
	    						<button  type='button' class='btn btn-default btndefault confirm' link='" . url("cc/delete/$req->cc_id") . "' >DELETE</button>
		    					<a ' class='btn btn-default btndefault' href='" . url("cc/edit/$req->cc_id?page=view") . "' >VIEW</a>
		    					<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("cc/edit/$req->cc_id") . "'\" >EDIT</button>
		    					";
	    				}
	    				else
	    				{
	    					$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id?page=view") . "' >VIEW</a>
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id") . "' >APPROVE</a>
	    					";
	    				}
	    				
	                    $ctr++;
	                 }
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		}
		return $result_data;
	}

	public function get_csdh_record()
	{	
		$result_data["aaData"] = array();
		//CHECK FIRST, IF USER IS CC CSDH
		if(Session::get("is_cc_csdh_head"))
		{
			$requests = ClearanceCertification::where("cc_status",5)
				->join("employees","employees.id","=","cc_emp_id")
				->join("departments","departments.id","=","cc_emp_departmentid")
				//->limit(5)
				->get(["cc_ref_num","dept_name","cc_create_date","cc_id","cc_resignation_date", "lastname","firstname"]);

			
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {		

	                    $result_data["aaData"][$ctr][] = $req->cc_ref_num;
	                    $result_data["aaData"][$ctr][] = $req->cc_create_date;
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
	    				$result_data["aaData"][$ctr][] = $req->dept_name;
	    				$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
	    				$result_data["aaData"][$ctr][] = "For Approval";		
	    				$result_data["aaData"][$ctr][] = (strtotime(date("m/d/Y")) - strtotime($req->cc_create_date)) / 86400 + 1;
	    				
	    				$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id?page=view") . "' >VIEW</a>
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id") . "' >APPROVE</a>
	    					";
	                    $ctr++;
	                 }
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		}
		return $result_data;
	}

	

	public function get_chrd_personnel_record($ajax = null)
	{	
		//CHECK FIRST, IF USER IS CC CSDH
		//if(Session::get("is_cc_cbr_filer"))
		{
			$result_data = array();
			$requests = ClearanceCertification::join("employees","employees.id","=","cc_emp_id")
				->where("cc_create_id" , Session::get("employee_id"))
				->join("departments","departments.id","=","departmentid")
				->get(["cc_print_status","cc_ref_num","firstname" , "lastname","cc_id","cc_resignation_date","dept_name","cc_status"]);

			if($ajax == null)
			{
				if ( count($requests) > 0)
		        {
		        	$status[0] = "New";
		        	$status[1] = "Approved";
		        	$status[2] = "For Acknowledgement";
		        	$status[3] = "Returned";
		        	$status[4] = "For Assessment";
		        	$status[5] = "For Approval";

		                $ctr = 0;
		                foreach($requests as $req) 
		                 {				
		                 	$view_link = in_array($req->cc_status, [0,3,2,4]) ? "cc/edit/$req->cc_id?page=view" : "cc/form/$req->cc_id";
		                 	$disable  = in_array($req->cc_status, [0,3]) ? "" : "disabled";
		                 	$delete_disable  = in_array($req->cc_status, [0,3]) || $req->cc_print_status ? "" : "disabled";
		                 	$icon = '<i class="fa fa-list fa-xs  text-primary view" id="' .  $req->cc_id . '" ></i>';
		                    $result_data[$ctr][] = $icon . " " . $req->cc_ref_num;
		    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname ;
		    				$result_data[$ctr][] = $req->dept_name;
		    				$result_data[$ctr][] = $req->cc_resignation_date;
		    				$result_data[$ctr][] = $status[$req->cc_status];
		    				$result_data[$ctr][] = "
		    					<button $delete_disable type='button' class='btn btn-default btndefault confirm' link='" . url("cc/delete/$req->cc_id") . "' >DELETE</button>
		    					<a ' class='btn btn-default btndefault' href='" . url($view_link) . "' >VIEW</a>
		    					<button $disable type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("cc/edit/$req->cc_id") . "'\" >EDIT</button>
		    					";
		                    $ctr++;
		                 }
		        }
			}	    	
	        else
	        {
	        	if ( count($requests) > 0)
		        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {		
	                 	if($req->cc_status == 3)
	                 	{
	                 		$result_data["aaData"][$ctr][] = $req->cc_ref_num;
		    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
		    				$result_data["aaData"][$ctr][] = $req->dept_name;
		    				$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
		    				$result_data["aaData"][$ctr][] = "Returned";			    				
		    				$result_data["aaData"][$ctr][] = "<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("cc/edit/$req->cc_id") . "'\" >VIEW</button>";
		                    $ctr++;
	                 	}
	                    
	                 }
		        }
		        else
		        {
		            $result_data["aaData"] = $requests;
		        }
	        }

			return $result_data;
		}
	}

	

	public function get_chrd_logs($id)
	{	
		//CHECK FIRST, IF USER IS CC CSDH
		if(Session::get("is_cc_cbr_filer"))
		{
			$result_data = array();
			$requests = DB::table("clearancecertificationlogs")
				->join("employees","employees.id","=","ccl_assigned")
				->join("departments","departments.id","=","employees.departmentid")
				->where("ccl_cc_id",$id)
				->get(["ccl_date_received","firstname" , "lastname","ccl_status","ccl_date_complete","dept_name","ccl_remarks"]);

	    	if ( count($requests) > 0)
	        {
	        	$status[0] = "For Acknowledgement";
	        	$status[1] = "For Final Pay Processing";
	        	$status[2] = "Returned";
	        	$status[3] = "For Assessment";
	        	$status[4] = "For Review";
	        	$status[5] = "For Approval";

                $ctr = 0;
                foreach($requests as $req) 
                 {				
                 	$result_data[$ctr][] = date("Y-m-d",strtotime($req->ccl_date_received));
                 	$result_data[$ctr][] = (strtotime(($req->ccl_date_complete ? $req->ccl_date_complete : date("Y-m-d"))) - strtotime(  date("Y-m-d",strtotime($req->ccl_date_received))  )) / 86400 + 1;
                 	$result_data[$ctr][] = $status[$req->ccl_status];
    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname ;
    				$result_data[$ctr][] = $req->dept_name;
    				$result_data[$ctr][] = $req->ccl_remarks;	    				
    				$result_data[$ctr][] = $req->ccl_date_complete;
    				$result_data[$ctr][] = date("H:i:s",strtotime($req->ccl_date_received));
                    $ctr++;
                    $previous = $req->ccl_date_complete;
                 }
       	 	}

			return $result_data;
		}
	}

	

	public function get_assestment_record()
	{
		$requests4 = ClearanceCertification::where("cc_emp_id" , Session::get("employee_id"))
			->join("employees","employees.id","=","cc_create_id")
			->where("cc_status",2)
			//->limit(5)
			->get();

		$result_data["aaData"] = array();
		 $ctr = 0;
    	if ( count($requests4) > 0)
        {             
                foreach($requests4 as $rs) 
                 {		
                 	$result_data["aaData"][$ctr][] = $rs->cc_ref_num;
	                $result_data["aaData"][$ctr][] = $rs->cc_create_date;
					$result_data["aaData"][$ctr][] =  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) ;
					$result_data["aaData"][$ctr][] = Session::get("dept_name");
					$result_data["aaData"][$ctr][] = $rs->cc_resignation_date;
					$result_data["aaData"][$ctr][] = "FOR ACKNOWLEDGEMENT";		
					$result_data["aaData"][$ctr][] = (strtotime(date("m/d/Y")) - strtotime($rs->cc_create_date)) / 86400 + 1;
					
					$result_data["aaData"][$ctr][] = "
    					<a ' class='btn btn-default btndefault' href='" . url("cc/acknowledge/$rs->cc_id") . "' >VIEW</a>
    					";

					// //END
     //                $result_data["aaData"][$ctr][] = $req->cc_ref_num;
    	// 			$result_data["aaData"][$ctr][] = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) ;
    	// 			$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
					// $result_data["aaData"][$ctr][] = $req->cc_create_date;
    	// 			$result_data["aaData"][$ctr][] = "For Acknowledgement";		
    	// 			$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
    	// 			$result_data["aaData"][$ctr][] = "
    	// 				<a ' class='btn btn-default btndefault' href='" . url("cc/acknowledge/$req->cc_id") . "' >VIEW</a>
    	// 				";
                    $ctr++;
                 }
        }

		
		$requests = ClearanceCertification::condition()
			->join("employees","employees.id","=","cc_emp_id")
			->join("departments","departments.id","=","cc_emp_departmentid")
			->wherein("cc_status",[5,4])
		//	->limit(5)
			->get(["cc_technical_status","cc_ref_num","cc_resignee_status","cc_id","firstname","lastname","dept_name",
				"cc_create_date","cc_resignation_date"]);

		
		$arr_id = array();
		if ( count($requests) > 0)
	     {
			foreach ($requests as $key => $rs) 
			{

				if( Session::get("is_cc_epr_head") )         {
		            $link =  "cc/erpd/head/review/$rs->cc_id";
		            $label = "APPROVE";
				}
		        else if( Session::get("is_cc_ssrv_head") )     {
		            $link =  "cc/ssrv/head/review/$rs->cc_id"; 
		            $label = "APPROVE";
		        }
		        else if( Session::get("is_cc_smdd_head") ) {
		        	$link =  "cc/smdd/head/review/$rs->cc_id";
		        	$label = "APPROVE";
		        }   
		        else if( Session::get("is_cc_epr_staff") )       
		            {
		            	$link =  "cc/erpd/staff/$rs->cc_id";
		            	$label = "PROCESS";
		            }
		        else if( Session::get("is_cc_ssrv_staff") )  {    
		            $link =  "cc/ssrv/staff/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_smdd_staff") )    {  
		            $link =  "cc/smdd/staff/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_training1") )    {  
		            $link =  "cc/chrd/training1/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_training2") )      {
		            $link =  "cc/chrd/training2/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_cbr_personnel1") )   {   
		            $link =  "cc/chrd/cbr/personnel1/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_cbr_personnel2") )   {   
		            $link =  "cc/chrd/cbr/personnel2/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_cbr_manager") )    {  
		            $link =  "cc/chrd/cbr/manager/$rs->cc_id";  
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_recruitment") )     { 
		            $link =  "cc/chrd/recruitment/$rs->cc_id";	
		            $label = "PROCESS";	
		            }        
		        else if( Session::get("is_cc_chrd_head") )  {    
		            $link =  "cc/chrd/$rs->cc_id"; 		
		            $label = "APPROVE";       
		        }
		        else if( Session::get("is_cc_it_head") && $rs->cc_technical_status == 0)    {  
		            $link =  "cc/citd/technical/$rs->cc_id"; 
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_it_head") && $rs->cc_technical_status == 3)   {   
		            $link =  "cc/citd/technical/review/$rs->cc_id"; 
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_citd_staff") )     { 
		            $link =  "cc/citd/staff/$rs->cc_id"; 
		            $label = "PROCESS";
		        }
		        else if( Session::get("is_cc_citd_head") )   {   
		        	$label = "APPROVE";
		            $link =  "cc/citd/head/review/$rs->cc_id";
		        }
		        else if(Session::get("desig_level") == "head" && $rs->ccd_status == 0) {
		            $link =  "cc/department/head/$rs->cc_id";
		            $label = "PROCESS";
		        }
		        else if(Session::get("desig_level") == "head" && $rs->ccd_status == 3) {
		            $link =  "cc/department/review/$rs->cc_id";
		            $label = "APPROVE";
		        }
		        else if(Session::get("desig_level") == "supervisor") {
		            $link =  "cc/department/personnel/$rs->cc_id";
		            $label = "PROCESS";
		        }

		         $arr_id[] = $rs->cc_id;	

		        $result_data["aaData"][$ctr][] = $rs->cc_ref_num;
                $result_data["aaData"][$ctr][] = $rs->cc_create_date;
				$result_data["aaData"][$ctr][] = $rs->firstname . " " . $rs->lastname ;
				$result_data["aaData"][$ctr][] = $rs->dept_name;
				$result_data["aaData"][$ctr][] = $rs->cc_resignation_date;
				$result_data["aaData"][$ctr][] = "FOR ASSESSMENT";		
				$result_data["aaData"][$ctr][] = (strtotime(date("m/d/Y")) - strtotime($rs->cc_create_date)) / 86400 + 1;
				
				$result_data["aaData"][$ctr][] = "
					<a ' class='btn btn-default btndefault' href='" .  url($link) . "?page=view' >VIEW</a>
					<a ' class='btn btn-default btndefault' href='" .  url($link) . "' >$label</a>
					";

                $ctr++;
			}
		}
		
		//ADD THIS TO CHECK IF USER IS THE ASSIGNED PERSONNEL FOR RESIGNEE'S IMMEDIATE HEAD/SUPERVISOR
		$requests2 = ClearanceCertification::resignee_condition()
			->join("employees","employees.id","=","cc_emp_id")
			->join("departments","departments.id","=","cc_emp_departmentid")
			->wherein("cc_status",[5,4])
			->get(["cc_ref_num","cc_resignee_status","cc_id","firstname","lastname","dept_name",
				"cc_create_date","cc_resignation_date",
				"cc_chrd_status","cc_smd_status","cc_technical_status","cc_ssrv_status","cc_erp_status","cc_dept_head"]);

		$count2 = 0;

		if ( count($requests2) > 0)
	    {
			foreach ($requests2 as $key => $rs) 
			{
				//condition to avoid duplicate of review form
				if( ((Session::get("is_cc_epr_head") && $rs->cc_erp_status==2) || 
					(Session::get("is_cc_ssrv_head") && $rs->cc_ssrv_status==2) ||
					(Session::get("is_cc_citd_head") && $rs->cc_technical_status==4) ||
					(Session::get("is_cc_smdd_head") && $rs->cc_smd_status==2) ||
					(Session::get("is_cc_chrd_head") && $rs->cc_chrd_status==2)) && $rs->cc_resignee_status != 0)
				{}
				else
				{
					if(in_array(Session::get("desig_level") ,["head","top mngt","president"]) && $rs->cc_resignee_status == 0)
			            $link =  "cc/resignee/head/$rs->cc_id";
			        else if(in_array(Session::get("desig_level") ,["supervisor"]) && $rs->cc_resignee_status == 0 && $rs->cc_dept_head == Session::get("employee_id"))
			            $link =  "cc/resignee/head/$rs->cc_id";
			        else if(in_array(Session::get("desig_level") ,["head","top mngt","president"]) && $rs->cc_resignee_status == 3)
			            $link =  "cc/resignee/review/$rs->cc_id";
			        else if(in_array(Session::get("desig_level") ,["supervisor"]) && $rs->cc_resignee_status == 2 && $rs->cc_dept_head == Session::get("employee_id") )
			            $link =  "cc/resignee/review/$rs->cc_id";
			        else if(Session::get("desig_level") == "supervisor")
			            $link =  "cc/resignee/supervisor/$rs->cc_id";

			        $result_data["aaData"][$ctr][] = $rs->cc_ref_num;
	                $result_data["aaData"][$ctr][] = $rs->cc_create_date;
					$result_data["aaData"][$ctr][] = $rs->firstname . " " . $rs->lastname ;
					$result_data["aaData"][$ctr][] = $rs->dept_name;
					$result_data["aaData"][$ctr][] = $rs->cc_resignation_date;
					$result_data["aaData"][$ctr][] = "FOR ASSESSMENT";		
					$result_data["aaData"][$ctr][] = (strtotime(date("m/d/Y")) - strtotime($rs->cc_create_date)) / 86400 + 1;
					
					$label =  $rs->cc_resignee_status == 0 ? "PROCESS" : "APPROVE";

					$result_data["aaData"][$ctr][] = "
						<a ' class='btn btn-default btndefault' href='" . url($link) . "?page=view' >VIEW</a>
						<a ' class='btn btn-default btndefault' href='" . url($link) . "' >$label</a>
						";
	                $ctr++;
	                $count2 = $count2 + 1;
	            }
			}	
		}	


		//CHECK FIRST, IF USER IS CC CSDH
		if(Session::get("is_cc_csdh_head"))
		{
			$requests3 = ClearanceCertification::where("cc_status",5)
				->join("employees","employees.id","=","cc_emp_id")
				->join("departments","departments.id","=","cc_emp_departmentid")
				//->limit(5)
				->get(["cc_ref_num","dept_name","cc_create_date","cc_id","cc_resignation_date", "lastname","firstname"]);

			

	    	if ( count($requests3) > 0)
	        {
	                $ctr = 0;
	                foreach($requests3 as $req) 
	                 {		

	                    $result_data["aaData"][$ctr][] = $req->cc_ref_num;
	                    $result_data["aaData"][$ctr][] = $req->cc_create_date;
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname ;
	    				$result_data["aaData"][$ctr][] = $req->dept_name;
	    				$result_data["aaData"][$ctr][] = $req->cc_resignation_date;
	    				$result_data["aaData"][$ctr][] = "For Approval";		
	    				$result_data["aaData"][$ctr][] = (strtotime(date("m/d/Y")) - strtotime($req->cc_create_date)) / 86400 + 1;
	    				
	    				$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id?page=view") . "' >VIEW</a>
	    					<a ' class='btn btn-default btndefault' href='" . url("cc/csdh/$req->cc_id") . "' >APPROVE</a>
	    					";
	                    $ctr++;
	                 }
	        }
			
		}


		$result_data["iTotalDisplayRecords"] = count($requests) + count($requests2) + count($requests4)  +  (isset($requests3) ?count(  $requests3) : 0)   ; //total count filtered query
	    $result_data["iTotalRecords"] = count($requests) + count($requests2) + count($requests4) +  (isset($requests3) ? count( $requests3 ) : 0)  ;

		return $result_data;
	}


	
}
?>