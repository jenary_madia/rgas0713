<?php namespace RGAS\Modules\CC\Requests;

use Input;
use Validator;
use Session;
use Redirect;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();
		$val_data = [
            'employee' => 'required',
             'emp_no' => 'required',     
             'department' => 'required',
             'date_hired' => 'required',
             'position' => 'required',
//             'other_dept.0' => "required"
        ];

		if(Input::get('action') == "send")
		{
			$val_data = array_merge($val_data, [ 'effective_date' => 'required','files' => 'required']);			
		}
		
		$validator = Validator::make($input , $val_data);

		// if(Input::get('action') == "send")
		// {
		// 	if(isset($_POST['other_dept'])) $validator->each('other_dept', ['required', 'numeric']);			
		// }


		if ($validator->fails())
		{
			return Redirect::to('cc/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_edit($id)
	{
		$input = Input::all();
		$val_data = [];

		if(Input::get('action') == "send")
		{
			$val_data = array_merge($val_data, [ 'effective_date' => 'required']);			
		}
		
		$validator = Validator::make($input , $val_data);

		if(Input::get('action') == "send")
		{
			if(isset($_POST['other_dept'])) $validator->each('other_dept', ['required', 'numeric']);			
		}

		if ($validator->fails())
		{
			return Redirect::to("cc/edit/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_chrd_return($id)
	{
		$input = Input::all();
		$val_data = [
             'personnel' => 'required',
             'remarks' => 'required'
        ];
		
		$validator = Validator::make($input , $val_data);
		
		
		if ($validator->fails())
		{
			return Redirect::to( "cc/chrd/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_csdh_return($id)
	{
		$input = Input::all();
		$val_data = [
             'personnel' => 'required',
             'remarks' => 'required'
        ];
		
		$validator = Validator::make($input , $val_data);
		
		
		if ($validator->fails())
		{
			return Redirect::to( "cc/csdh/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_acknowledge_return($id)
	{
		$input = Input::all();
		$val_data = [
             'comment' => 'required'
        ];
		
		$validator = Validator::make($input , $val_data);
		
		
		if ($validator->fails())
		{
			return Redirect::to( "cc/acknowledge/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_resignee_head($link)
	{
		$input = Input::all();
		$val_data = [
            'manual_surrender_date' => '|date',
             'work_files_date' => '|date',
             'drawer_date' => '|date',
             'assets_date' => '|date',
             'supplies_date' => '|date',
             'accountable_date' => '|date',
             'revolving_date' => '|date',
             'car_date' => '|date'
        ];

		if(Input::get('action') == "assign")
		{
			$val_data = array_merge($val_data, ["assign"=>"required"]);
		}
		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_department_head($link , $head=null)
	{
		$input = Input::all();
		$val_data = [];
		
		if(Input::get('action') == "assign" )
		{
			$val_data = array_merge($val_data, ["assign"=>"required"]);
		}
		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		$validator = Validator::make($input , $val_data);
		if(!$head)
		{
			$validator->each('employeeRequirement', ['required']);
			//$validator->each('availability', ['required']);
			$validator->each('date', ['date']);
			$validator->each('amount', ['numeric']);
			$validator->each('requirement_date', ['date']);
			$validator->each('requirement_amount', ['numeric']);
		}

		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_citd($link)
	{
		$input = Input::all();
		$val_data = [
             'internet_date' => '|date',
             'intranet_date' => '|date',
             'internal_date' => '|date',
             'external_date' => '|date',
             'sms_date' => '|date',
             'laptop_date' => '|date',
             'internet_amount' => '|numeric',
             'intranet_amount' => '|numeric',
             'internal_amount' => '|numeric',
             'external_amount' => '|numeric',
             'sms_amount' => '|numeric',
             'laptop_amount' => '|numeric'
        ];

		if(Input::get('action') == "assign")
		{
			$val_data = array_merge($val_data, ["assign"=>"required" ]);			
		}
		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);			
		}
		
		$validator = Validator::make($input , $val_data);

		if(Input::get("requirement_date")) $validator->each('requirement_date', ['date']);
		if(Input::get("requirement_amount")) $validator->each('requirement_amount', ['numeric']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_smdd($link)
	{
		$input = Input::all();
		$val_data = [
            'rgas_date' => '|date'
        ];

		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_ssrv($link)
	{
		$input = Input::all();
		$val_data = [
            'vale_date' => '|date',
            'vale_amount' => '|numeric'
        ];

		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		$validator->each('requirement_amount', ['numeric']);
		
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_erpd($link)
	{
		$input = Input::all();
		$val_data = [
            'sap_date' => '|date'
        ];

		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		
		$validator = Validator::make($input , $val_data);
		
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_compensation1($link)
	{
		$input = Input::all();
		$val_data = [
            'hmo_surrender_date' => '|date',
             'hmo_cancel_date' => '|date',
             'card_date' => '|date',
             'building_date' => '|date'
            ];

		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_compensation2($link)
	{
		$input = Input::all();
		$val_data = [
            'sss_date' => '|date',
             'aub_date' => '|date',
             'hmo_date' => '|date',
             'sss_amount' => '|numeric',
             'aub_amount' => '|numeric',
             'hmo_amount' => '|numeric'
        ];

		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		$validator->each('requirement_amount', ['numeric']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	/*************************************************************************************/

	public function validate_training1($link)
	{
		$input = Input::all();
		$val_data = [
            'exit_date' => '|date'
        ];

	
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_training2($link)
	{
		$input = Input::all();
		$val_data = [
            'service_date' => '|date',
             'material_date' => '|date',
             'equipment_date' => '|date'
        ];

	
		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_recruitment($link)
	{
		$input = Input::all();
		$val_data = [
            'biometrics_date' => '|date'
        ];

	
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/

	public function validate_manager($link)
	{
		$input = Input::all();
		$val_data = [
            'staff_date' => '|date'
        ];

		
		$validator = Validator::make($input , $val_data);
		$validator->each('requirement_date', ['date']);
		
		
		if ($validator->fails())
		{
			return Redirect::to( $link )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	/*************************************************************************************/
}
?>