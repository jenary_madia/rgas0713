<?php namespace RGAS\Modules\ART;

use Session;
use RGAS\Libraries;
use Input;
use Arts;
use Redirect;
use Receivers;
use Employees;
use DB;
use Config;
use Mail;

class ART extends ARTLogs implements IART
{
	
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_art');
		return $this->module_id;
	}

	public function create_save()
	{	
		$reference = Arts::get_reference_no();
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

		
		$parameters = array(
				'employeeid' => Session::get('employeeid'),
		 		'department' => Session::get('dept_name'),
		 		'requestedby' =>  Session::get('employee_name'),
		 		'lastname' =>Session::get('lastname'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename') ,
		 		'daterequested' => date("Y-m-d"),
		 		'company' => Session::get('company'),
		 		'duedate' => Input::get('duedate') ? date("Y-m-d",strtotime(Input::get('duedate'))) : null,
		 		'contactno' => Input::get('contact'),
		 		'requestfor' => Input::get('requestype'),
		 		'service_requested' => Input::get('servicerequested'),
		 		'packaging_materials' => Input::get('packaging'),
		 		'othermaterials' => Input::get('othermaterial'),
		 		'productname' => Input::get('productname'),
		 		'netweight' => Input::get('weight'),
		 		'length' => Input::get('lenght'),
		 		'width' => Input::get('width'),
		 		'height' => Input::get('height'),
		 		'otherspecifications' => Input::get('otherspecification'),
		 		'briefdescription' => Input::get('briefdesc'),
		 		'ownerid' => Session::get('employee_id'),
		 		'curr_emp' => Session::get('employee_id'),
		 		'datecreated' => date("Y-m-d"),
		 		'status' => "New",
		 		'section' => Input::get('section'),
		 		'reference_no' => $reference,
		 		'project_name' => Input::get('project'),
		 		"attachments" => $attachments,
		 		'comment_save' => Input::get('comment'),
		 		'comment' => "" 
		 		);

		$store = Arts::create($parameters);

		//INSERT AUDIT
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		if(!empty($files)):
		foreach($files as $file)
        {
            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . $reference );
            //INSERT AUDIT
			$this->setTable('arts');
			$this->setPrimaryKey('id');
			$this->AU002($store->id  ,  $file["original_filename"] , Input::get("reference") );
      	}
      	endif;

		if ($store) 
			return Redirect::to('art/report')
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY SAVED.');
		else
			return Redirect::to('art/report')
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function create_sent()
	{	
		if(Session::get('desig_level') == "head"  || Session::get('desig_level') == "president")
		{
			$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->join("employees" , "employees.id" , "=","receivers.employeeid")
				->get(["firstname","lastname","email","receivers.employeeid","value"]);

			foreach ($arthead as $rs) 
			{
				if($rs->value == 1)
				{
					$name = $rs->firstname . " " . $rs->lastname;
					$email = $rs->email ;
					$assign = $rs->employeeid;
				}
				else
				{
					$supname = $rs->firstname . " " . $rs->lastname;
					$supemail = $rs->email ;
					$supassign = $rs->employeeid;
				}				
			}
			
			$endorsedby =  Session::get('employee_name');
        	$dateendorsed = date("Y-m-d");
			$status = "For Processing";
			$msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO MKTG-ART FOR PROCESSING";
		}
		else
		{
			$head = Employees::where("departmentid",Session::get('dept_id'))
				->where("desig_level" , "head")
				->first(["firstname","lastname","email","id"]);

			$name = $head["firstname"] . " " . $head["lastname"];
			$email = $head["email"];
			$assign = $head["id"];
			$status = "For Approval";
			$msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR APPROVAL";
			$endorsedby =  "";
        	$dateendorsed = null;

			//if filer is executive
			if(!$head["firstname"])
			{
				$arthead = Arts::superior( Session::get("superiorid") );

				$name = $arthead["firstname"] . " " . $arthead["lastname"];
				$email = $arthead["email"];
				$assign = $arthead["id"];

				// $arthead = Receivers::where("code","CRT_ART_RECEIVER")
				// ->join("employees" , "employees.id" , "=","receivers.employeeid")
				// ->get(["firstname","lastname","email","receivers.employeeid","value"]);

				// foreach ($arthead as $rs) 
				// {
				// 	if($rs->value == 1)
				// 	{
				// 		$name = $rs->firstname . " " . $rs->lastname;
				// 		$email = $rs->email ;
				// 		$assign = $rs->employeeid;
				// 	}
				// 	else
				// 	{
				// 		$supname = $rs->firstname . " " . $rs->lastname;
				// 		$supemail = $rs->email ;
				// 		$supassign = $rs->employeeid;
				// 	}				
				// }
				
				// $endorsedby =  Session::get('employee_name');
    //     		$dateendorsed = date("Y-m-d");
				// $status = "For Processing";
				// $msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO MKTG-ART FOR PROCESSING";
			}
		}

		$reference = Arts::get_reference_no();		
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;        
		$parameters = array(
				'employeeid' => Session::get('employeeid'),
		 		'department' => Session::get('dept_name'),
		 		'requestedby' =>  Session::get('employee_name'),
		 		'lastname' =>Session::get('lastname'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename') ,
		 		'daterequested' => date("Y-m-d"),
		 		'company' => Session::get('company'),
		 		'duedate' => date("Y-m-d",strtotime(Input::get('duedate'))),
		 		'contactno' => Input::get('contact'),
		 		'requestfor' => Input::get('requestype'),
		 		'service_requested' => Input::get('servicerequested'),
		 		'packaging_materials' => Input::get('packaging'),
		 		'othermaterials' => Input::get('othermaterial'),
		 		'productname' => Input::get('productname'),
		 		'netweight' => Input::get('weight'),
		 		'length' => Input::get('lenght'),
		 		'width' => Input::get('width'),
		 		'height' => Input::get('height'),
		 		'otherspecifications' => Input::get('otherspecification'),
		 		'briefdescription' => Input::get('briefdesc'),
		 		'ownerid' => Session::get('employee_id'),
		 		'comment' => $comment,
		 		'curr_emp' => $assign,
		 		'datecreated' => date("Y-m-d"),
		 		'status' => $status,
		 		'section' => Input::get('section'),
		 		'reference_no' => $reference,
		 		'project_name' => Input::get('project'),
		 		"attachments" => $attachments,
		 		'endorsedby' =>  $endorsedby,
        		'dateendorsed' => $dateendorsed 
		 		);

		$store = Arts::create($parameters);

		//INSERT AUDIT
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		if(!empty($files)):
		foreach($files as $file)
        {
            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . $reference );
            //INSERT AUDIT
			$this->setTable('arts');
			$this->setPrimaryKey('id');
			$this->AU002($store->id  ,  $file["original_filename"] , Input::get("reference") );
      	}
      	endif;
      	
		//SEND EMAIL NOTIFICATION
		if($status == "For Approval")
		{
			Mail::send('art.email.approve', array(
	                        'reference' => $reference,
	                        'filer' => Session::get('employee_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Art Design Request for Approval");
	                        }
	                    );
		}
		else
		{
			Mail::send('art.email.process', array(
	                        'reference' => $reference,
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
	                        }
	                    );

			Mail::send('art.email.process', array(
	                        'reference' => $reference,
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($supemail ,  $supname )
	                        {
	                            $message->to( $supemail, $supname )
	                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
	                        }
	                    );
		}

		if ($store) 
			return Redirect::to('art/report')
		           ->with('successMessage', $msg );
		else
			return Redirect::to('art/report')
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}


	

	public function edit_save($id)
	{	
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);

		$parameters = array(
				'employeeid' => Session::get('employeeid'),
		 		'department' => Session::get('dept_name'),
		 		'requestedby' =>  Session::get('employee_name'),
		 		'lastname' =>Session::get('lastname'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename') ,
		 		'daterequested' => date("Y-m-d"),
		 		'company' => Session::get('company'),
		 		'duedate' => Input::get('duedate') ? date("Y-m-d",strtotime(Input::get('duedate'))) : null,
		 		'contactno' => Input::get('contact'),
		 		'requestfor' => Input::get('requestype'),
		 		'service_requested' => Input::get('servicerequested'),
		 		'packaging_materials' => Input::get('packaging'),
		 		'othermaterials' => Input::get('othermaterial'),
		 		'productname' => Input::get('productname'),
		 		'netweight' => Input::get('weight'),
		 		'length' => Input::get('lenght'),
		 		'width' => Input::get('width'),
		 		'height' => Input::get('height'),
		 		'otherspecifications' => Input::get('otherspecification'),
		 		'briefdescription' => Input::get('briefdesc'),
		 		'datecreated' => date("Y-m-d"),
		 		'section' => Input::get('section'),
		 		'project_name' => Input::get('project'),
		 		"attachments" => $attachments,
		 		'comment_save' => Input::get('comment')
		 		);

			Arts::where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["New","Disapproved"])
        	->update($parameters);

			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY SAVED.');
	}

	

	public function edit_sent($id)
	{	
		if(Session::get('desig_level') == "head" || Session::get('desig_level') == "president")
		{
			$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->join("employees" , "employees.id" , "=","receivers.employeeid")
				->get(["firstname","lastname","email","receivers.employeeid","value"]);

			foreach ($arthead as $rs) 
			{
				if($rs->value == 1)
				{
					$name = $rs->firstname . " " . $rs->lastname;
					$email = $rs->email ;
					$assign = $rs->employeeid;
				}
				else
				{
					$supname = $rs->firstname . " " . $rs->lastname;
					$supemail = $rs->email ;
					$supassign = $rs->employeeid;
				}				
			}

			$endorsedby =  Session::get('employee_name');
        	$dateendorsed = date("Y-m-d");
			$status = "For Processing";
			$msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO MKTG-ART FOR PROCESSING";
		}
		else
		{
			$head = Employees::where("departmentid",Session::get('dept_id'))
				->where("desig_level" , "head")
				->first(["firstname","lastname","email","id"]);

			$name = $head["firstname"] . " " . $head["lastname"];
			$email = $head["email"];
			$assign = $head["id"];
			$status = "For Approval";
			$msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR APPROVAL";
			$endorsedby =  "";
        	$dateendorsed = null;

			//if filer is executive
			if(!$head["firstname"])
			{
				$arthead = Arts::superior( Session::get("superiorid") );

				$name = $arthead["firstname"] . " " . $arthead["lastname"];
				$email = $arthead["email"];
				$assign = $arthead["id"];

				// $arthead = Receivers::where("code","CRT_ART_RECEIVER")
				// ->join("employees" , "employees.id" , "=","receivers.employeeid")
				// ->get(["firstname","lastname","email","receivers.employeeid","value"]);

				// foreach ($arthead as $rs) 
				// {
				// 	if($rs->value == 1)
				// 	{
				// 		$name = $rs->firstname . " " . $rs->lastname;
				// 		$email = $rs->email ;
				// 		$assign = $rs->employeeid;
				// 	}
				// 	else
				// 	{
				// 		$supname = $rs->firstname . " " . $rs->lastname;
				// 		$supemail = $rs->email ;
				// 		$supassign = $rs->employeeid;
				// 	}				
				// }
				
				// $endorsedby =  Session::get('employee_name');
    //     		$dateendorsed = date("Y-m-d");
				// $status = "For Processing";
				// $msg = "ART DESIGN REQUEST SUCCESSFULLY SENT TO MKTG-ART FOR PROCESSING";
			}
		}

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);

		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;        
		$parameters = array(
				'employeeid' => Session::get('employeeid'),
		 		'department' => Session::get('dept_name'),
		 		'requestedby' =>  Session::get('employee_name'),
		 		'lastname' =>Session::get('lastname'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename') ,
		 		'daterequested' => date("Y-m-d"),
		 		'company' => Session::get('company'),
		 		'duedate' => date("Y-m-d",strtotime(Input::get('duedate'))),
		 		'contactno' => Input::get('contact'),
		 		'requestfor' => Input::get('requestype'),
		 		'service_requested' => Input::get('servicerequested'),
		 		'packaging_materials' => Input::get('packaging'),
		 		'othermaterials' => Input::get('othermaterial'),
		 		'productname' => Input::get('productname'),
		 		'netweight' => Input::get('weight'),
		 		'length' => Input::get('lenght'),
		 		'width' => Input::get('width'),
		 		'height' => Input::get('height'),
		 		'otherspecifications' => Input::get('otherspecification'),
		 		'briefdescription' => Input::get('briefdesc'),		 		
		 		'curr_emp' => $assign,
		 		'datecreated' => date("Y-m-d"),
		 		'status' => $status,
		 		'section' => Input::get('section'),
		 		'project_name' => Input::get('project'),
		 		"attachments" => $attachments,
		 		'comment_save' => "",
		 		'endorsedby' =>  $endorsedby,
        		'dateendorsed' => $dateendorsed,
        		'comment' =>  DB::raw("concat(comment , ?)")
		 		);

		$store = Arts::addBinding($comment)->where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["New","Disapproved"])
        	->update($parameters);



        //SEND EMAIL NOTIFICATION
		if($status == "For Approval")
		{
			Mail::send('art.email.approve', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Art Design Request for Approval");
	                        }
	                    );
		}
		else
		{
			Mail::send('art.email.process', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
	                        }
	                    );

			Mail::send('art.email.process', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name'),
	                        'type' => strtoupper( Input::get('requestype')) ),
	                        function($message ) use ($supemail ,  $supname )
	                        {
	                            $message->to( $supemail, $supname )
	                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
	                        }
	                    );
		}

		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', $msg );
		else
			return Redirect::to("art/edit/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function view_cancel($id)
	{	
		$head = Employees::where("departmentid",Session::get('dept_id'))
				->where("desig_level" , "head")
				->first(["firstname","lastname","email","id"]);

		$name = $head["firstname"] . " " . $head["lastname"];
		$email = $head["email"];
		$assign = $head["id"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.cancel', array(
                        'reference' => Input::get("reference"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Cancelled Art Design");
                        }
                    );


        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "Cancelled",
		 		'curr_emp' => Session::get("employee_id"),
		 		'comment' =>  DB::raw("concat(comment , ?)")	 		
		 		);

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->where("status" , "For Approval")
        	->update($parameters);


		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY CANCELLED.');
		else
			return Redirect::to("art/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function view_conforme($id)
	{	

		 //insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);


        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "Closed",		 		
		 		'attachments2' => $attachments,
		 		'comment' =>  DB::raw("concat(comment , ?)")
		 		);

       
		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->where("status" , "For Conforme")
        	->update($parameters);

        //INSERT AUDIT
		$old = ["status"=> "For Processing"];
		$new = ["status"=> "Closed"];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );


		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY ACKNOWLEDGED');
		else
			return Redirect::to("art/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function view_return($id)
	{
		$emp = Arts::where("arts.id", $id)
				->join("employees as e" , "e.id" , "=","arts.assignto")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "For Conforme")
				->first(["e.firstname","e.lastname","email","assignto"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["assignto"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.revision', array(
                        'reference' => Input::get("reference"),
                        'filer' => Session::get('employee_name'),
	                    'department' => Session::get('dept_name'),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Art Design Request For Revision");
                        }
                    );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "For Processing",
		 		'curr_emp' => $assign,
		 		'attachments2' => $attachments	,
		 		'comment' =>  DB::raw("concat(comment , ?)"),
		 			 		
		 		);

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "For Conforme")
        	->update($parameters);

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id")];
		$new = ["curr_emp"=>  $assign];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );



        $revision["ar_art_id"] = $id;
        $revision["ar_received_date"] = date("Y-m-d");
        $revision["ar_completion_date"] = date("Y-m-d",strtotime(Input::get('completion_date')));
        $revision["ar_detail"] = Input::get('revision');
        $revision["ar_revision_degree"] = Input::get('degree');

        DB::table("art_revisions")->insert($revision);

		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY RETURNED TO ART PERSONNEL FOR PROCESSING');
		else
			return Redirect::to("art/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function approval_send($id)
	{	

		$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->join("employees" , "employees.id" , "=","receivers.employeeid")
				->get(["firstname","lastname","email","receivers.employeeid","value"]);

		foreach ($arthead as $rs) 
			{
				if($rs->value == 1)
				{
					$name = $rs->firstname . " " . $rs->lastname;
					$email = $rs->email ;
					$assign = $rs->employeeid;
				}
				else
				{
					$supname = $rs->firstname . " " . $rs->lastname;
					$supemail = $rs->email ;
					$supassign = $rs->employeeid;
				}				
			}

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.process', array(
                        'reference' => Input::get("reference"),
                        'filer' => Input::get("empname"),
                        'department' => Input::get("department"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
                        }
                    );

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.process', array(
                        'reference' => Input::get("reference"),
                        'filer' => Input::get("empname"),
                        'department' => Input::get("department"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($supemail ,  $supname )
                        {
                            $message->to( $supemail, $supname )
                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
                        }
                    );


		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);
        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
        		'endorsedby' => Session::get('employee_name'),
        		'dateendorsed' => date("Y-m-d"),
		 		'status' => "For Processing",
		 		'curr_emp' => $assign,
		 		'attachments' => $attachments	,
		 		'comment' =>  DB::raw("concat(comment , ?)"),
		 			
		 		);

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "For Approval")
        	->update($parameters);

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id")];
		$new = ["curr_emp"=>  $assign];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY SENT TO MKTG-ART FOR PROCESSING');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function approval_return($id)
	{	
		$emp = Arts::where("arts.id", $id)
				->join("employees as e" , "e.id" , "=","arts.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "For Approval")
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.disapprove', array(
                        'reference' => Input::get("reference"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Returned Art Design");
                        }
                    );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
	            //INSERT AUDIT
				$this->setTable('arts');
				$this->setPrimaryKey('id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Arts::delete_attachment($id);

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "Disapproved",
		 		'curr_emp' => $assign,
		 		'attachments' => $attachments	,
		 		'comment' =>  DB::raw("concat(comment , ?)"),
		 				 		
		 		);

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "For Approval")
        	->update($parameters);

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id")];
		$new = ["curr_emp"=>  $assign];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY RETURNED TO FILER');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function assig_return($id)
	{	
		$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->where("value",1)
				->first(["receivers.employeeid"]);

		$headid = $arthead["employeeid"];


		$emp = Arts::where("arts.id", $id)
				->join("employees as e" , "e.id" , "=","arts.ownerid")
				->where("curr_emp" ,$headid )
        		->where("status" , "For Processing")
        		->whereNull("assignto")
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.disapprove', array(
                        'reference' => Input::get("reference"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Returned Art Design");
                        }
                    );


        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "Disapproved",
		 		'curr_emp' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

        

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , $headid  )
        	->where("status" , "For Processing")
        		->whereNull("assignto")
        	->update($parameters);

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id")];
		$new = ["curr_emp"=>  $assign];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("art/reviewer/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY RETURNED TO FILER');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function assig_assign($id)
	{	
		$emp = Employees::where("id" , Input::get('assign'))
				->first(["firstname","lastname","email","id"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["id"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.process', array(
                        'reference' => Input::get("reference"),
                        'filer' => Input::get("empname"),
                        'department' => Input::get("department"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Art Design Request for Processing");
                        }
                    );


        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'curr_emp' => $assign,
		 		'assignto' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)")	 		
		 		);


        $arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->where("value",1)
				->first(["receivers.employeeid"]);

		$headid = $arthead["employeeid"];


		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , $headid  )
        	->where("status" , "For Processing")
        	->whereNull("assignto")
        	->update($parameters);

        

		if ($store) 
		{
			//INSERT AUDIT
			$old = ["curr_emp"=> Session::get("employee_id")];
			$new = ["curr_emp"=>  $assign];
			$this->setTable('arts');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

	        $revision["ar_art_id"] = $id;
	        $revision["ar_received_date"] = date("Y-m-d");
	        $revision["ar_completion_date"] = Input::get('duedate');

	        DB::table("art_revisions")->insert($revision);
        
			return Redirect::to("art/reviewer/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY ASSIGNED TO CONCERNED PERSONNEL');
		}
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function artist_send($id)
	{	
		$emp = Arts::where("arts.id", $id)
				->join("employees as e" , "e.id" , "=","arts.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "For Processing")
        		->where("assignto", Session::get("employee_id"))
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

		//SEND EMAIL NOTIFICATION
		Mail::send('art.email.conforme', array(
                        'reference' => Input::get("reference"),
                        'type' => strtoupper( Input::get('requestype')) ),
                        function($message ) use ($email ,  $name )
                        {
                            $message->to( $email, $name )
                            	->subject("RGAS Notification Alert: Art Design Request for Conforme");
                        }
                    );


        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "For Conforme",
		 		'curr_emp' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)")	 		
		 		);

		$store =Arts::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "For Processing")
        	->where("curr_emp" , Session::get("employee_id"))
    		->where("status" , "For Processing")
    		->where("assignto", Session::get("employee_id"))
        	->update($parameters);

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id")];
		$new = ["curr_emp"=>  $assign];
		$this->setTable('arts');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

        	//insert attach
        	$files = Input::get("files");
	        $attach = array();
	        $fm = new Libraries\FileManager;

        	foreach($files as $file)
            {
                $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'art/' . Input::get('reference') );
          	}

          	if(strtotime(date("Y-m-d")) > strtotime(Input::get("completion_date")))
          		$stats = "Delayed";
          	else if(strtotime(date("Y-m-d")) < strtotime(Input::get("completion_date")))
          		$stats = "Advance";
          	else 
          		$stats = "On Time";

          	$revision["ar_submmited_date"] = date("Y-m-d");
	        $revision["ar_status"] = $stats;
	        $revision["ar_attachments"] = json_encode($files);

	        DB::table("art_revisions")
	        	->where("ar_art_id" , $id)
	        	->whereNull("ar_attachments")
	        	->update($revision);

		if ($store) 
			return Redirect::to("art/reviewer/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY SENT TO FILER FOR CONFORME');
		else
			return Redirect::to("art/artist/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	

	public function get_record()
	{	
			$result_data = array();
			$requests = Arts::where("ownerid" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","curr_emp","left")
				->where("isdeleted" , 0)
				->get(["e.firstname","e.lastname","reference_no","daterequested","requestfor" , "status","arts.id"]);

	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disabled_edit = in_array($req->status,["New","Disapproved"]) ? "" : "disabled";
	                 	$disable_delete = in_array($req->status,["For Approval","For Processing","For Conforme"]) ? "disabled" : "";
	                 	$result_data[$ctr]["current"] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr]["status"] = strtoupper($req->status );
	    				$result_data[$ctr]["type"] = strtoupper($req->requestfor);
	    				$result_data[$ctr]["date"] = $req->daterequested;
	    				$result_data[$ctr]["reference"] = $req->reference_no;
	    				$result_data[$ctr]["action"] = "
	    					<button ref='$req->reference_no' $disable_delete type='button' class='btn btn-default btndefault confirm' link='" . url("art/delete/$req->id") . "' >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("art/view/$req->id") . "' >VIEW</a> " . 
	    					"<button $disabled_edit  type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("art/edit/$req->id") . "'\" >EDIT</button>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	

	public function get_review_record()
	{	
			$result_data = array();
			$requests = Arts::where("curr_emp" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","ownerid","left")
				->where("isdeleted" , 0)
				->where("status","For Approval")
				->get(["e.firstname","e.lastname","reference_no","daterequested","requestfor" , "status","arts.id"]);

	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disable_delete = in_array($req->status,["For Approval","For Processing","For Conforme"]) ? "disabled" : "";
	                 	$result_data[$ctr]["from"] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr]["status"] = strtoupper($req->status) ;
	    				$result_data[$ctr]["type"] = strtoupper($req->requestfor);
	    				$result_data[$ctr]["date"] = $req->daterequested;
	    				$result_data[$ctr]["reference"] = $req->reference_no;
	    				$result_data[$ctr]["action"] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("art/approval/$req->id?page=view") . "' >VIEW</a>
	    					<a ' class='btn btn-default btndefault' href='" . url("art/approval/$req->id") . "' >APPROVE</a>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	

	public function get_art_record()
	{	
			$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->where("value",1)
				->first(["receivers.employeeid"]);

			$assign = $arthead["employeeid"];


			$result_data = array();
			//whereRaw("(curr_emp = ? or (curr_emp = ?)" , [Session::get("employee_id") , $assign] )
			$requests = Arts::whereIn("status",["For Processing","Closed","For Conforme"])
				->join("employees","employees.id","=","curr_emp")
				->get(["employees.firstname","employees.lastname","curr_emp","assignto","dateendorsed","requestedby","reference_no","duedate","requestfor" , "status","arts.id","department"]);

	    	if ( count($requests) > 0)
	        {
	        		
	                	$ctr = 0;
		                foreach($requests as $req) 
		                 {	
					             if(Session::get("is_art_receiver") || $req->curr_emp ==Session::get("employee_id") || $req->assignto ==Session::get("employee_id"))
				                {			
			                 	$result_data[$ctr]["requestedby"] = $req->requestedby;
			    				$result_data[$ctr]["status"] = strtoupper($req->status) ;
			    				$result_data[$ctr]["type"] = strtoupper($req->requestfor);
			    				$result_data[$ctr]["duedate"] = $req->duedate;
			    				$result_data[$ctr]["reference"] = $req->reference_no;
			    				$result_data[$ctr]["approveddate"] = $req->dateendorsed;
			    				$result_data[$ctr]["department"] = $req->department;
			    				$result_data[$ctr]["current"] = $req->firstname . " " . $req->lastname;
			    				$result_data[$ctr]["action"] = Session::get("is_art_receiver") && $req->curr_emp == $assign && $req->status == "For Conforme" ? 
				    				"<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id?page=view") . "' >VIEW</a>" 
				    				. "<button   type='button'  class=' btn btn-default btndefault'  disabled >PROCESS</button>"
				    					: 
					    					($req->status !="Closed" && $req->assignto == Session::get("employee_id") && $req->curr_emp == Session::get("employee_id") ? "
						    					<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id?page=view") . "' >VIEW</a>
						    					<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id") . "' >PROCESS</a>"
				    						:

				    						($req->assignto == Session::get("employee_id") && $req->status == "For Conforme" ? "<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id?page=view") . "' >VIEW</a>" 
				    						:
						    					"
						    					<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id?page=view") . "' >VIEW</a>" . 
						    					
						    					(Session::get("is_art_receiver") && $req->curr_emp == $assign && $req->status == "For Processing" && $req->assignto != $assign ?  
						    						"<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id") . "' >PROCESS</a>" 
							    					: 
							    					"<button   type='button'  class=' btn btn-default btndefault'  disabled >PROCESS</button>")
						    				)
					    				);
			                    $ctr++;
			                }
		                 }
	                
	                if(!isset($result_data))  $result_data["aaData"] = $requests;    
	                
	        }

			return $result_data;
	}

	public function get_art_record_holder()
	{	
			$arthead = Receivers::where("code","CRT_ART_RECEIVER")
				->where("value",1)
				->first(["receivers.employeeid"]);

			$assign = $arthead["employeeid"];


			$result_data = array();
			
			$requests = Arts::whereRaw("( ((curr_emp = ? or curr_emp = ?) and status = 'For Processing') || (assignto = ? and status in ('For Processing' , 'For Conforme','Closed')))" , [Session::get("employee_id") , $assign,Session::get("employee_id")] )
				->join("employees","employees.id","=","curr_emp")
				->get(["employees.firstname","employees.lastname","curr_emp","assignto","dateendorsed","requestedby","reference_no","duedate","requestfor" , "status","arts.id","department"]);

	    	if ( count($requests) > 0)
	        {
	        		
	                	$ctr = 0;
		                foreach($requests as $req) 
		                 {	
					             if(Session::get("is_art_receiver") || $req->curr_emp ==Session::get("employee_id") || $req->assignto ==Session::get("employee_id"))
				                {			

				                if(Session::get("is_art_receiver") && $req->curr_emp == $assign && $req->status == "For Processing" && $req->assignto == $assign && Session::get("employee_id") != $assign )
				                {

				                }
				                else
				                {
				                	$result_data[$ctr]["requestedby"] = $req->requestedby;
				    				$result_data[$ctr]["status"] = strtoupper($req->status) ;
				    				$result_data[$ctr]["type"] = strtoupper($req->requestfor);
				    				$result_data[$ctr]["duedate"] = $req->duedate;
				    				$result_data[$ctr]["reference"] = $req->reference_no;
				    				$result_data[$ctr]["approveddate"] = $req->dateendorsed;
				    				$result_data[$ctr]["department"] = $req->department;
				    				$result_data[$ctr]["current"] = $req->firstname . " " . $req->lastname;
				    				$result_data[$ctr]["action"] = Session::get("is_art_receiver") && $req->curr_emp == $assign && $req->status == "For Conforme" ? 
					    				"<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id?page=view") . "' >VIEW</a>" 
					    				. "<button   type='button'  class=' btn btn-default btndefault'  disabled >PROCESS</button>"
					    					: 
						    					($req->status !="Closed" && $req->assignto == Session::get("employee_id") && $req->curr_emp == Session::get("employee_id") ? "
							    					<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id?page=view") . "' >VIEW</a>
							    					<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id") . "' >PROCESS</a>"
					    						:

					    						($req->assignto == Session::get("employee_id") && $req->status == "For Conforme" ? "<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id?page=view") . "' >VIEW</a>" 
					    						:
							    					"
							    					<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id?page=view") . "' >VIEW</a>" . 
							    					
							    					(Session::get("is_art_receiver") && $req->curr_emp == $assign && $req->status == "For Processing" ?  
							    						"<a ' class='btn btn-default btndefault' href='" . url("art/assign/$req->id") . "' >PROCESS</a>" 
								    					: 
								    					"<button   type='button'  class=' btn btn-default btndefault'  disabled >PROCESS</button>")
							    				)
						    				);
				                    $ctr++;
				                }
			                 	
			                }
		                 }
	                
	                if(!isset($result_data))  $result_data["aaData"] = $requests;    
	                
	        }

			return $result_data;
	}

	public function deleterecord($id)
	{	
		$store =Arts::where("id",$id)
			->where("ownerid" , Session::get("employee_id"))
			->wherein("status" , ["New","Disapproved","Closed","Cancelled"])
			->update(["isdeleted"=>1]);
	                 
		if($store)
		{		
			$rec =Arts::where("id",$id)
			->where("ownerid" , Session::get("employee_id"))
			->wherein("status" , ["New","Disapproved","Closed","Cancelled"])
			->get(["reference_no"])->first();

			//INSERT AUDIT
			$old = ["isdeleted"=> 0];
			$new = ["isdeleted"=> 1];
			$this->setTable('arts');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , $rec["reference_no"] );

			return Redirect::to("art/report")
		           ->with('successMessage', 'ART DESIGN REQUEST SUCCESSFULLY DELETED.');
		}
		else
			return Redirect::to("art/report")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
		
	}


	public function get_reviewer()
	{	
			


			$result_data["aaData"] = array();
			$requests = Arts::where("curr_emp" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","ownerid","left")
				->where("isdeleted" , 0)
				->orderBy("reference_no","desc")
				->whereRaw("(status = 'For Approval' || (status = 'For Processing' and assignto is not null))")
				->limit(5)
			//	->whereIn("status",["For Approval","For Processing"])
				->get(["e.firstname","e.lastname","reference_no","daterequested","requestfor" , "status","arts.id"]);
		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {

	        		$ctr = 0;
	                foreach($requests as $req) 
	                {		  			
	                    $result_data["aaData"][$ctr][] = $req->reference_no;
	    				$result_data["aaData"][$ctr][] = $req->daterequested;
	    				$result_data["aaData"][$ctr][] = strtoupper( $req->requestfor );
	    				$result_data["aaData"][$ctr][] = strtoupper( $req->status ); 
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
	    				if($req->status=="For Approval") $result_data["aaData"][$ctr][] = "<a ' class='btn btn-default btndefault' href='" . url("art/approval/$req->id?page=view") . "' >VIEW</a> <a ' class='btn btn-default btndefault' href='" . url("art/approval/$req->id") . "' >APPROVE</a>";
	    				else if($req->status=="For Processing")  $result_data["aaData"][$ctr][] = "<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id?page=view") . "' >VIEW</a>
			    					<a ' class='btn btn-default btndefault' href='" . url("art/artist/$req->id") . "' >PROCESS</a>";
	                    $ctr++;
			                
	             	}	   	             	   
	        }
	        else
	        {
	           // $result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}


	public function get_my_review()
	{	
			$result_data["aaData"] = array();

			$requests = Arts::where("ownerid" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","curr_emp","left")
				->where("isdeleted" , 0)
				->orderBy("reference_no","desc")
				->limit(5)
				->whereIn("status",["For Approval","Disapproved","For Conforme","For Processing"])
				->get(["e.firstname","e.lastname","reference_no","daterequested","requestfor" , "status","arts.id"]);

		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {

	        		$ctr = 0;
	                foreach($requests as $req) 
	                {		  		
	                	$disabled_edit = in_array($req->status,["New","Disapproved"]) ? "" : "disabled";
	                 	$disable_delete = in_array($req->status,["For Approval","For Processing","For Conforme"]) ? "disabled" : "";	
	                    $result_data["aaData"][$ctr][] = $req->reference_no;
	    				$result_data["aaData"][$ctr][] = $req->daterequested;
	    				$result_data["aaData"][$ctr][] = strtoupper( $req->requestfor );
	    				$result_data["aaData"][$ctr][] = strtoupper( $req->status ); 
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
	    				 $result_data["aaData"][$ctr][] = "
	    					<button ref='$req->reference_no'  type='button' class='$disable_delete btn btn-default btndefault confirm' link='" . url("art/delete/$req->id") . "' >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("art/view/$req->id") . "' >VIEW</a> " . 
	    					"<button   type='button'  class='$disabled_edit btn btn-default btndefault' onClick=\"window.location='" . url("art/edit/$req->id") . "'\" >EDIT</button>";

	                    $ctr++;
			                
	             	}	   	             	   
	        }
	        else
	        {
	            //$result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}

}
?>