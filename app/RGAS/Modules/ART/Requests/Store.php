<?php namespace RGAS\Modules\ART\Requests;

use Input;
use Validator;
use Redirect;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
             'project' => 'required',
             'duedate' => 'required|date',
             'requestype' => 'required',
             'servicerequested' => 'required',
             'packaging' => 'required',
             'briefdesc' => 'required'
        ]);
	
		if ($validator->fails())
		{
			return Redirect::to('art/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_view($id)
	{
		$input = Input::all();
		
		
		$val_data = array();
		if(Input::get('action') == "return")
		{
			$messages = [
				'revision.required' => 'DETAILS OF REVISIONS is required',
				'completion_date.required' => 'TARGET COMPLETION DATE is required',
			];

			$val_data = array_merge($val_data, 
				[
				"revision"=>"required",
				"completion_date"=>"required|date"]);

			$validator = Validator::make($input , $val_data,$messages);
		}
		else if(Input::get('action') == "cancel")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);

			$validator = Validator::make($input , $val_data);
		}
		else
		{
			$validator = Validator::make($input , $val_data);
		}
		

		if ($validator->fails())
		{
			return Redirect::to("art/view/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_approval($id)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
             'comment' => 'required'
        ]);
	
		if ($validator->fails())
		{
			return Redirect::to("art/approval/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function assign_validate($id)
	{
		$input = Input::all();
		
		
		$val_data = array();
		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}
		else if(Input::get('action') == "assign")
		{
			$val_data = array_merge($val_data, ["assign"=>"required" ]);
		}

		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to("art/assign/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	public function conforme_validate($id)
	{
		$input = Input::all();
		
		$val_data = array();
		if(!isset($_FILES["files"]))
		{
			$val_data = array_merge($val_data, ["files"=>"required"]);
		}

		$messages = [
		    'required' => 'SUBMIT DESIGN IS REQUIRED',
		];
		$validator = Validator::make($input , $val_data,$messages);

		if ($validator->fails())
		{
			return Redirect::to("art/artist/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}
}
?>