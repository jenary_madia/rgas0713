<?php namespace RGAS\Modules\SV\Requests;

use Input;
use Validator;
use Redirect;

class Store extends RResult
{
	public function validate($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'purpose' => 'required',
             'requestype' => 'required'
        ];

		if(!isset($_POST['pickup-date'])) 
		{
			$val_data = array_merge($val_data, ["Service_Vehicle_Detail"=>"required"]);
		}

		$validator = Validator::make($input , $val_data);

		if(isset($_POST['pickup-date'])) $validator->each('pickup-date', ['required|date']);
		if(isset($_POST['pickup-time'])) $validator->each('pickup-time', ['required|']);
		if(isset($_POST['passenger'])) $validator->each('passenger', ['required|numeric']);

		if ($validator->fails())
		{
			return Redirect::to($id ? "svr/edit/$id" : 'svr/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_view($id)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
             'comment' => 'required'
        ],['comment.required' => 'Please indicate reason for cancellation.']);
	
		if ($validator->fails())
		{
			return Redirect::to("svr/view/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_approval($id)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
             'comment' => 'required'
        ],['comment.required' => 'Please indicate reason for disapproval.']);
	
		if ($validator->fails())
		{
			return Redirect::to("svr/approval/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_accept($id)
	{
		$input = Input::all();
		
		
		$val_data =  [ ];

		if(Input::get('action') == "return" || Input::get('action') == "cancelled")
		{
			$val_data = array_merge($val_data, ["comment"=>"required"]);
		}

		$validator = Validator::make($input , $val_data,['comment.required' => 'Please indicate reason for disapproval.']);

		if(Input::get('action') == "send")
		{
			$validator->each('vtype', ['required']);
			$validator->each('plateno', ['required']);
			$validator->each('plateid', ['required']);
		}

		if ($validator->fails())
		{
			return Redirect::to(Input::get('action') == "cancelled" ? "svr/process/$id" : "svr/accept/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}
}
?>