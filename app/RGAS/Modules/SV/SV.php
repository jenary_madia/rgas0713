<?php namespace RGAS\Modules\SV;

use AUDITTRAILING;
use Session;
use RGAS\Libraries;
use Input;
use ServiceVehicle;
use DB;
use Redirect;
use Employees;
use Receivers;
use Mail;
use Config;
use Validator;

class SV extends SVLogs implements ISV
{
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_sv');
		return $this->module_id;
	}
/*************************************************************************************/
	public function create_save()
	{	
		$reference = ServiceVehicle::get_reference_no();
		$parameters = array(
		 		'date' => date("Y-m-d"),
		 		'requestfor' => Input::get('requestype'),
		 		'controlno' => $reference ,
		 		'requestor' => Session::get('employee_name'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename'),
		 		'lastname' => Session::get('lastname'),
		 		'department' => Session::get('dept_name'),
		 		'company' => Session::get('company'),
		 		'purpose' => Input::get('purpose'),
		 		'ownerid' => Session::get('employee_id'),
		 		'status' => "NEW",
		 		'comment_save' =>  trim(Input::get('comment')) ,
		 		'comment' => "" ,
		 		'datecreated' => date("Y-m-d"), 
		 		'curr_emp' => Session::get('employee_id'),

		 		'section' => Input::get('section'),
		 		'contact_no' => Input::get('contact')
		 		);

		$store =ServiceVehicle::create($parameters);
		//INSERT AUDIT
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		$vehicle = Input::get("pickup-date");
		$pickuptime = Input::get("pickup-time");
		$pickupplace = Input::get("pickup-company-other");
		$pickupplace_id = Input::get("pickup-company");
		$pickupplace_location = Input::get("pickup-location-other");
		$pickupplace_location_id = Input::get("pickup-location");
		$destination = Input::get("destin-company-other");
		$destination_id = Input::get("destin-company");
		$destination_location = Input::get("destin-location-other");
		$destination_location_id = Input::get("destin-location");
		$noofpassengers = Input::get("passenger");
		$estimated_trip_time = Input::get("triptime");

		if(!empty($vehicle)):
		foreach($vehicle as $key=>$date)
        {
            $detail[] = array(
                    "servicevehicleid" => $store->id,
                    "date" => $date,
                    "pickuptime" => $pickuptime[$key],
                    "pickupplace" => $pickupplace[$key],
                    "pickupplace_id" => $pickupplace_id[$key],
                    "pickupplace_location" => $pickupplace_location[$key],
                    "pickupplace_location_id" => $pickupplace_location_id[$key],
                    "destination" => $destination[$key],
                    "destination_id" => $destination_id[$key],
                    "destination_location" => $destination_location[$key],
                    "destination_location_id" => $destination_location_id[$key],
                    "noofpassengers" => $noofpassengers[$key],
                    "estimated_trip_time" => $estimated_trip_time[$key],
                    "vehicle_id" => "",//san to?
                    "status_id" => 0
                );

            //INSERT AUDIT
			$this->setTable('servicevehicledetails');
			$this->setPrimaryKey('servicevehicles.id');
			$this->AU001($store->id , json_encode(end($detail)) , $reference );
        }


        DB::table("servicevehicledetails")->insert( $detail ); 
        endif;

		if ($store) 
			return Redirect::to('svr/report')
		           ->with('successMessage', 'Service vehicle request successfully saved.');
		else
			return Redirect::to('svr/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}

/*************************************************************************************/
	public function create_sent()
	{	
		$submitted = false;
		if(in_array(Session::get("desig_level"), ["vpo","om","aom","plant-aom","admin-aom","top mngt","president" , "head"])
			|| ( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"]) && !in_array(Session::get("desig_level") , ["employee",'supervisor'])  ))
		{
			if( in_array(Session::get("company") ,["RBC-CORP","RBC-SAT"]) && Input::get("requestype") == "TRUCK")
			{

				//send to receiver
				$assign = Receivers::where("code","WH_SERVICEVEHICLE_RECEIVER")
					->join("employees" , "employees.id" , "=","receivers.employeeid")
					->first(["firstname","lastname","email","receivers.employeeid as id"]);
			}
			else
			{
				$assign = Receivers::where("code","SPRO_SERVICEVEHICLE_RECEIVER")
					->where("receivers.company",Session::get("company"))
					->join("employees" , "employees.id" , "=","receivers.employeeid")
					->first(["firstname","lastname","email","receivers.employeeid as id"]);
			}

			$status = "FOR ACCEPTANCE";
		}
		else if( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"] ))
		{
			//immediate superior
			$assign = ServiceVehicle::superior( Session::get("superiorid")  );
			$status = "FOR APPROVAL";
		}
		else
		{
			//to dept head
			$submitted = true;
			$assign = Employees::where("desig_level" , "head")
				->where("departmentid",Session::get("dept_id"))
				->first(["firstname","lastname","email", "id"]);

			$status = "FOR APPROVAL";
		}


		//$submitted = false;
		// if(Session::get("dept_name") == "OFFICE OF THE EXECUTIVES" && Session::get("company") == "RBC-CORP")
		// {
		// 	$assign = ServiceVehicle::superior_rbc( Session::get("superiorid")  );
		// }
		// else if(Session::get("dept_name") == "OPERATION MANAGEMENT")
		// {
		// 	$assign = ServiceVehicle::superior( Session::get("superiorid")  );
		// }
		// else if(Session::get("desig_level") == "head" && Session::get("company") == "RBC-CORP")
		// {
		// 	$assign = ServiceVehicle::superior_rbc( Session::get("superiorid")  );
		// }
		// else
		// {
		// 	$submitted = true;
		// 	$assign = Employees::where("desig_level" , "head")
		// 		->where("departmentid",Session::get("dept_id"))
		// 		->first(["firstname","lastname","email", "id"]);
		// }

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];
		

		$reference = ServiceVehicle::get_reference_no();
		$parameters = array(
		 		'date' => date("Y-m-d"),
		 		'requestfor' => Input::get('requestype'),
		 		'controlno' =>  $reference ,
		 		'requestor' => Session::get('employee_name'),
		 		'firstname' => Session::get('firstname'),
		 		'middlename' => Session::get('middlename'),
		 		'lastname' => Session::get('lastname'),
		 		'department' => Session::get('dept_name'),
		 		'company' => Session::get('company'),
		 		'purpose' => Input::get('purpose'),
		 		'ownerid' => Session::get('employee_id'),
		 		'status' => $status,
		 		'submittedto' => $submitted,
		 		'comment' => ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
		 		'datecreated' => date("Y-m-d"), 
		 		'curr_emp' => $assign["id"],
		 		'comment_save' => "",
		 		'section' => Input::get('section'),
		 		'contact_no' => Input::get('contact')
		 		);

		$store =ServiceVehicle::create($parameters);
		//INSERT AUDIT
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		$vehicle = Input::get("pickup-date");
		$pickuptime = Input::get("pickup-time");
		$pickupplace = Input::get("pickup-company-other");
		$pickupplace_id = Input::get("pickup-company");
		$pickupplace_location = Input::get("pickup-location-other");
		$pickupplace_location_id = Input::get("pickup-location");
		$destination = Input::get("destin-company-other");
		$destination_id = Input::get("destin-company");
		$destination_location = Input::get("destin-location-other");
		$destination_location_id = Input::get("destin-location");
		$noofpassengers = Input::get("passenger");
		$estimated_trip_time = Input::get("triptime");

		foreach($vehicle as $key=>$date)
        {
            $detail[] = array(
                    "servicevehicleid" => $store->id,
                    "date" => $date,
                    "pickuptime" => $pickuptime[$key],
                    "pickupplace" => $pickupplace[$key],
                    "pickupplace_id" => $pickupplace_id[$key],
                    "pickupplace_location" => $pickupplace_location[$key],
                    "pickupplace_location_id" => $pickupplace_location_id[$key],
                    "destination" => $destination[$key],
                    "destination_id" => $destination_id[$key],
                    "destination_location" => $destination_location[$key],
                    "destination_location_id" => $destination_location_id[$key],
                    "noofpassengers" => $noofpassengers[$key],
                    "estimated_trip_time" => $estimated_trip_time[$key],
                    "vehicle_id" => "",
                    "status_id" => 0
                );

            //INSERT AUDIT
			$this->setTable('servicevehicledetails');
			$this->setPrimaryKey('servicevehicles.id');
			$this->AU001($store->id , json_encode(end($detail)) , $reference );
        }

        DB::table("servicevehicledetails")->insert( $detail ); 

        if($status == "FOR APPROVAL")
        {
         Mail::send('svr.email.approve', array(
        					'status' => "FOR APPROVAL",
	                        'reference' => $reference,
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Service Vehicle for Approval");
	                        }
	                    );	
        }
        else
        {
        	Mail::send('svr.email.approve', array(
        					'status' => "FOR PROCESSING",
	                        'reference' => $reference,
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Service Vehicle for Processing");
	                        }
	                    );
        }

        


		if ($store) 
			return Redirect::to('svr/report')
		           ->with('successMessage', $status == "FOR ACCEPTANCE" ? 'Service vehicle request successfully sent for processing.' : 'Service vehicle request successfully sent for approval.');
		else
			return Redirect::to('svr/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}	


	/*************************************************************************************/
	public function edit_save($id)
	{	
		$parameters = array(
		 		'date' => date("Y-m-d"),
		 		'requestfor' => Input::get('requestype'),	
		 		'purpose' => Input::get('purpose'),
		 		'comment_save' =>  trim(Input::get('comment')) ,
		 		'section' => Input::get('section'),
		 		'contact_no' => Input::get('contact')
		 		);

		ServiceVehicle::where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["NEW","DISAPPROVED"])
        	->update($parameters);

		$vehicle = Input::get("pickup-date");
		$pickuptime = Input::get("pickup-time");
		$pickupplace = Input::get("pickup-company-other");
		$pickupplace_id = Input::get("pickup-company");
		$pickupplace_location = Input::get("pickup-location-other");
		$pickupplace_location_id = Input::get("pickup-location");
		$destination = Input::get("destin-company-other");
		$destination_id = Input::get("destin-company");
		$destination_location = Input::get("destin-location-other");
		$destination_location_id = Input::get("destin-location");
		$noofpassengers = Input::get("passenger");
		$estimated_trip_time = Input::get("triptime");

		DB::table("servicevehicledetails")
			->where("servicevehicleid",$id)
			->delete(); 

		if(!empty($vehicle)):
		foreach($vehicle as $key=>$date)
		{
            $detail[] = array(
                    "servicevehicleid" => $id,
                    "date" => $date,
                    "pickuptime" => $pickuptime[$key],
                    "pickupplace" => $pickupplace[$key],
                    "pickupplace_id" => $pickupplace_id[$key],
                    "pickupplace_location" => $pickupplace_location[$key],
                    "pickupplace_location_id" => $pickupplace_location_id[$key],
                    "destination" => $destination[$key],
                    "destination_id" => $destination_id[$key],
                    "destination_location" => $destination_location[$key],
                    "destination_location_id" => $destination_location_id[$key],
                    "noofpassengers" => $noofpassengers[$key],
                    "estimated_trip_time" => $estimated_trip_time[$key],
                    "status_id" => 0
                );
		}

        DB::table("servicevehicledetails")->insert( $detail ); 
        endif;

		return Redirect::to("svr/report")
		           ->with('successMessage', 'Service vehicle request successfully saved.');

	}

/*************************************************************************************/
	public function edit_sent($id)
	{	
		$submitted = false;
		if(in_array(Session::get("desig_level"), ["vpo","om","aom","plant-aom","admin-aom","top mngt","president" , "head"])
			|| ( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"]) && !in_array(Session::get("desig_level") , ["employee",'supervisor']) ))
		{
			if( in_array(Session::get("company") ,["RBC-CORP","RBC-SAT"]) && Input::get("requestype") == "TRUCK")
			{
				//send to receiver
				$assign = Receivers::where("code","WH_SERVICEVEHICLE_RECEIVER")
					->join("employees" , "employees.id" , "=","receivers.employeeid")
					->first(["firstname","lastname","email","receivers.employeeid as id"]);
			}
			else
			{
				$assign = Receivers::where("code","SPRO_SERVICEVEHICLE_RECEIVER")
					->where("receivers.company",Session::get("company"))
					->join("employees" , "employees.id" , "=","receivers.employeeid")
					->first(["firstname","lastname","email","receivers.employeeid as id"]);
			}

			$status = "FOR ACCEPTANCE";
		}
		else if( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"] ))
		{
			//immediate superior
			$assign = ServiceVehicle::superior( Session::get("superiorid")  );
			$status = "FOR APPROVAL";
		}
		else
		{
			//to dept head
			$submitted = true;
			$assign = Employees::where("desig_level" , "head")
				->where("departmentid",Session::get("dept_id"))
				->first(["firstname","lastname","email", "id"]);

			$status = "FOR APPROVAL";
		}

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];

		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
		$parameters = array(
		 		'date' => date("Y-m-d"),
		 		'requestfor' => Input::get('requestype'),	
		 		'purpose' => Input::get('purpose'),
		 		'submittedto' => $submitted,		 		
		 		'status' => $status,
		 		'curr_emp' => $assign["id"],
		 		'section' => Input::get('section'),
		 		'contact_no' => Input::get('contact'),
		 		'comment_save' =>  "",
		 		'comment' =>  DB::raw("concat(comment , ?)"),
		 		);

		ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["NEW","DISAPPROVED"])        	
        	->update($parameters);

		$vehicle = Input::get("pickup-date");
		$pickuptime = Input::get("pickup-time");
		$pickupplace = Input::get("pickup-company-other");
		$pickupplace_id = Input::get("pickup-company");
		$pickupplace_location = Input::get("pickup-location-other");
		$pickupplace_location_id = Input::get("pickup-location");
		$destination = Input::get("destin-company-other");
		$destination_id = Input::get("destin-company");
		$destination_location = Input::get("destin-location-other");
		$destination_location_id = Input::get("destin-location");
		$noofpassengers = Input::get("passenger");
		$estimated_trip_time = Input::get("triptime");

		DB::table("servicevehicledetails")
			->where("servicevehicleid",$id)
			->delete(); 

		foreach($vehicle as $key=>$date)
        {
            $detail[] = array(
                    "servicevehicleid" => $id,
                    "date" => $date,
                    "pickuptime" => $pickuptime[$key],
                    "pickupplace" => $pickupplace[$key],
                    "pickupplace_id" => $pickupplace_id[$key],
                    "pickupplace_location" => $pickupplace_location[$key],
                    "pickupplace_location_id" => $pickupplace_location_id[$key],
                    "destination" => $destination[$key],
                    "destination_id" => $destination_id[$key],
                    "destination_location" => $destination_location[$key],
                    "destination_location_id" => $destination_location_id[$key],
                    "noofpassengers" => $noofpassengers[$key],
                    "estimated_trip_time" => $estimated_trip_time[$key],
                    "status_id" => 0
                );
        }

        DB::table("servicevehicledetails")->insert( $detail ); 

        if($status == "FOR APPROVAL")
        {
         Mail::send('svr.email.approve', array(
        					'status' => "FOR APPROVAL",
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Service Vehicle for Approval");
	                        }
	                    );	
        }
        else
        {
        	Mail::send('svr.email.approve', array(
        					'status' => "FOR PROCESSING",
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Service Vehicle for Processing");
	                        }
	                    );
        }

		return Redirect::to("svr/report")
		           ->with('successMessage', $status == "FOR ACCEPTANCE" ?  'Service vehicle request successfully sent for processing.' : 'Service vehicle request successfully sent for approval.');
	}	


	public function approval_send($id)
	{	
		if( in_array(Input::get("company") ,["RBC-CORP","RBC-SAT"]) && Input::get("requestype") == "TRUCK")
		{
			$head = Receivers::where("code","WH_SERVICEVEHICLE_RECEIVER")
				->join("employees" , "employees.id" , "=","receivers.employeeid")
				->first(["firstname","lastname","email","receivers.employeeid"]);
		}
		else
		{
			$head = Receivers::where("code","SPRO_SERVICEVEHICLE_RECEIVER")
				->where("receivers.company",Input::get("company"))
				->join("employees" , "employees.id" , "=","receivers.employeeid")
				->first(["firstname","lastname","email","receivers.employeeid"]);
		}

		$name = $head["firstname"] . " " . $head["lastname"];
		$email = $head["email"];
		$assign = $head["employeeid"];

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
        		'sprm' => Session::get('employee_name'),
        		(Session::get("desig_level") == "head" ? 'dept_mngr' : "plant_mngr") =>  Session::get('employee_name'),
        		"datereceived" => date("Y-m-d"),
		 		'status' => "FOR ACCEPTANCE",
		 		'curr_emp' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)"),
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR APPROVAL")
        	->update($parameters);

        	Mail::send('svr.email.approve', array(
        					'status' => "FOR PROCESSING",
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get('empname'),
	                        'department' => Input::get('department')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Service Vehicle for Processing");
	                        }
	                    );

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id"),"status"=>"For Approval"];
		$new = ["curr_emp"=>  $assign,"status"=>"For processing"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("svr/report")
		           ->with('successMessage', 'Service vehicle request successfully sent for processing.');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}

	

	public function approval_return($id)
	{	
		$emp = ServiceVehicle::where("servicevehicles.id", $id)
				->join("employees as e" , "e.id" , "=","servicevehicles.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "FOR APPROVAL")
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "DISAPPROVED",
		 		'curr_emp' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR APPROVAL")
        	->update($parameters);

        Mail::send('svr.email.approve', array(
        					'status' => "DISAPPROVED",
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get('empname'),
	                        'department' => Input::get('department')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Disapproved Service Vehicle");
	                        }
	                    );

        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id"),"status"=>"For Approval"];
		$new = ["curr_emp"=>  $assign,"status"=>"Disapproved"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("svr/report")
		           ->with('successMessage', 'Service vehicle request successfully returned to Filer.');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}

	public function accept_return($id)
	{	
		$emp = ServiceVehicle::where("servicevehicles.id", $id)
				->join("employees as e" , "e.id" , "=","servicevehicles.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "FOR ACCEPTANCE")
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "DISAPPROVED",
		 		'curr_emp' => $assign,
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR ACCEPTANCE")
        	->update($parameters);

        Mail::send('svr.email.approve', array(
        					'status' => "DISAPPROVED",
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get('empname'),
	                        'department' => Input::get('department')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Disapproved Service Vehicle");
	                        }
	                    );


        //INSERT AUDIT
		$old = ["curr_emp"=> Session::get("employee_id"),"status"=>"For Acceptance"];
		$new = ["curr_emp"=>  $assign,"status"=>"Disapproved"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("svr/reviewer")
		           ->with('successMessage', 'Service vehicle request successfully returned to Filer.');
		else
			return Redirect::to("art/approval/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}


	public function accept_send($id)
	{
        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
        		'acceptanceof' => Session::get('employee_name'),
		 		'status' => "FOR PROCESSING",
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);


        //ADD CURL HERE
        if( in_array(Input::get("company") ,["RBC-CORP","RBC-SAT"]))
        {
        	$emp = ServiceVehicle::where("servicevehicles.id", $id)
				->join("employees as e" , "e.id" , "=","servicevehicles.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "FOR ACCEPTANCE")
				->first(["departmentid3"]);

			$departmentid3 = $emp["departmentid3"];
			$departmentid3 = $emp["departmentid3"];

        	$vehicle = Input::get("pickup-date");
			$pickuptime = Input::get("pickup-time");
			$pickupplace = Input::get("pickup-company-other");
			$pickupplace_id = Input::get("pickup-company");

			$pickupplace_location = Input::get("pickup-location-other");
			$pickupplace_location_id = Input::get("pickup-location");

			$destination = Input::get("destin-company-other");
			$destination_id = Input::get("destin-company");
			$destination_location = Input::get("destin-location-other");
			$destination_location_id = Input::get("destin-location");
			$noofpassengers = Input::get("passenger");
			$estimated_trip_time = Input::get("triptime");

			$vtype = Input::get("vtype");
			$pid = Input::get("plateid");
			$detailid = Input::get("vehicleid");

			foreach($vehicle as $key=>$date)
	        {
	        	$time = strtotime($pickuptime[$key]);

	            $data = array(
	                        "itinerary_date" => date('m/d/Y', strtotime($date)),
	                        "control_no" => Input::get("reference"), 
	                        "department_id" => $departmentid3,
	                        "vehicle_class_id" => (Input::get("requestype") == 'SERVICE') ? 1 : ((Input::get("requestype") == 'TRUCK') ? 2 : 0),
	                        "vehicle_type_id" => $vtype[$key],	                        
	                        "pickup_hour" => date("h",$time),
	                        "pickup_minutes" => date("m",$time),
	                        "pickup_meridian" => date("A",$time),
	                        "pickup_loc_id" => ($pickupplace_id[$key] == 0) ? 13 : $pickupplace_id[$key],
	                        "pickup_loc" => $pickupplace[$key],//san to
	                        "other_pickup_loc" => (($pickupplace_id[$key] == 0) ? 13 : $pickupplace_id[$key] != 13) ? '' : $pickupplace[$key] . ' - ' . $pickupplace_location[$key],
	                        "dest_loc_id" => ($destination_id[$key] == 0) ? 13 : $destination_id[$key],
	                        "dest_loc" => $destination[$key],
	                        "other_dest_loc" => (($destination_id[$key] == 0) ? 13 : $destination_id[$key] != 13) ? '' : $destination[$key] . ' - ' . $destination_location[$key],
	                        "driver_id" => (Input::get("requestype") == 'SERVICE') ? 1 : ((Input::get("requestype") == 'TRUCK') ? 52 : 0),
	                        "vehicle_id" => $pid[$key],
	                        "comment" => Input::get("purpose"),
	                        "passengers" => $noofpassengers[$key]
	                    );

				$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'rgas';

	            $ch = curl_init();
	             //$url = "http://localhost/rgasbl_/public/sent_curl";
				$url = 'http://rgtstest.rebisco.com/rgts/?c=vehicle_post&m=post';

	            curl_setopt($ch, CURLOPT_URL, $url);
	            curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

	            curl_setopt($ch, CURLOPT_POST, 1);
	            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	            $response = curl_exec($ch);
	            $response = str_replace('RGAS_Callback({', '{', $response);
	            $response = str_replace('})', '}', $response);
	  
	            $response = json_decode($response, true);
	            
	            if ($response['result'])
	            {
	            	//save status to 1 to submit into rgts
	                DB::table("servicevehicledetails")->where("servicevehicleid" , $id)
		        	->where("id" , $detailid [$key])
		        	->update(["status_id"=>1]);
	            }
	            else
	            {
	                $responses[] = array(
	                    "date" =>  $date ,
	                    "pickuptime" => $pickuptime[$key] ,
	                    "message" => $response['message']
	                );
	            }
	            
	            curl_close($ch);
	        }


	        if (!empty($responses))
	        {
	            $messages = array();
	            foreach ($responses as $r)
	            {
	                $messages[] = $r['date'] . ' - ' . $r['pickuptime'] . ': ' . implode(' ', $r['message']);
	            }

	            return Redirect::to("svr/accept/$id")
			           ->with('errorMessage', 'Error saving in RGTS! <br />' . implode(' <br /> ', $messages) );
	        }
        }
        //END CURL


		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR ACCEPTANCE")
        	->update($parameters);

        $vtype = Input::get("vtype");
        $plateid = Input::get("plateid");
        $plateno = Input::get("plateno");

        foreach(Input::get("vehicleid") as $key=>$rs)
        {
        	$detail = array(
        			"vehicletype" => $vtype[$key],
        			"vehicle_id" => $plateid[$key],
        			"plateno" => $plateno[$key]
        		);

        	DB::table("servicevehicledetails")->where("servicevehicleid" , $id)
        	->where("id" , $rs)
        	->update($detail);
        }


        //INSERT AUDIT
		$old = ["status"=>"For Acceptance"];
		$new = ["status"=>"For Processing"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store)
			return Redirect::to("svr/reviewer")
		           ->with('successMessage', 'Service vehicle request successfully sent for processing.');
		else
			return Redirect::to("svr/accept/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}

	public function accept_served($id)
	{
        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "SERVED",
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR PROCESSING")
        	->update($parameters);     

        //INSERT AUDIT
		$old = ["status"=>"For Processing"];
		$new = ["status"=>"Served"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store)
			return Redirect::to("svr/reviewer")
		           ->with('successMessage', 'Service vehicle request successfully updated as served.');
		else
			return Redirect::to("svr/accept/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}

	public function accept_cancelled($id)
	{
		$emp = ServiceVehicle::where("servicevehicles.id", $id)
				->join("employees as e" , "e.id" , "=","servicevehicles.ownerid")
				->where("curr_emp" , Session::get("employee_id"))
        		->where("status" , "FOR PROCESSING")
				->first(["e.firstname","e.lastname","email","ownerid"]);

		$name = $emp["firstname"] . " " . $emp["lastname"];
		$email = $emp["email"];
		$assign = $emp["ownerid"];

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "CANCELLED",
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->where("status" , "FOR PROCESSING")
        	->update($parameters);   

        Mail::send('svr.email.approve', array(
        					'status' => "CANCELLED",
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get('empname'),
	                        'department' => Input::get('department')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Cancelled Service Vehicle");
	                        }
	                    );

        //INSERT AUDIT
		$old = ["status"=>"For Processing"];
		$new = ["status"=>"Cancelled"];
		$this->setTable('servicevehicles');
		$this->setPrimaryKey('id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );  

		if ($store)
			return Redirect::to("svr/reviewer")
		           ->with('successMessage', 'Service vehicle request successfully updated as cancelled.');
		else
			return Redirect::to("svr/accept/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}


	public function view_cancel($id)
	{	
		$head = ServiceVehicle::where("servicevehicles.id", $id)
				->join("employees as e" , "e.id" , "=","servicevehicles.curr_emp")
				->where("ownerid" , Session::get("employee_id"))
        		->where("status" , "FOR APPROVAL")
				->first(["e.firstname","e.lastname","email","curr_emp"]);

		$name = $head["firstname"] . " " . $head["lastname"];
		$email = $head["email"];
		$assign = $head["id"];

		//INSERT EMAIL HERE
		Mail::send('svr.email.approve', array(
        					'status' => "CANCELLED",
	                        'reference' => Input::get("reference"),
	                        'filer' => Session::get('employee_name'),
	                        'department' => Session::get('dept_name')),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: Cancelled Service Vehicle");
	                        }
	                    );



        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "CANCELLED",
		 		'curr_emp' => Session::get("employee_id"),
		 		'comment' =>  DB::raw("concat(comment , ?)"),	 		
		 		);

		$store =ServiceVehicle::addBinding($comment)->where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->where("status" , "FOR APPROVAL")
        	->update($parameters);


		if ($store) 
			return Redirect::to("svr/report")
		           ->with('successMessage', 'Service Vehicle Request successfully cancelled.');
		else
			return Redirect::to("svr/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting request.');
	}

	public function get_receiver_record()
	{
			$result_data = array();
			$requests = ServiceVehicle::where("curr_emp" , Session::get("employee_id"))
				->where("isdeleted" , 0)
				->get(["datereceived","requestfor","controlno","requestor" , "company",
					"status","servicevehicles.id","department"]);

	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req)
	                 {				
	                 	$result_data[$ctr]["requestedby"] =   $req->requestor;
	    				$result_data[$ctr]["status"] = $req->status ;
	    				$result_data[$ctr]["type"] = $req->requestfor;
	    				$result_data[$ctr]["reference"] = ( in_array($req->status ,["CANCELLED","SERVED","SERVED/DELETED","CANCELLED/DELETED"]) ? "<input class='stats-$req->status' type='checkbox' value='$req->id' name='service[]' />" . " " : "") . $req->controlno;
	    				$result_data[$ctr]["approveddate"] = $req->datereceived;
	    				$result_data[$ctr]["department"] = $req->department;
	    				$result_data[$ctr]["company"] = $req->company;
	    				$status = $req->status == "SERVED/DELETED" ? "SERVED_" : ( $req->status == "CANCELLED/DELETED" ? "CANCELLED_" :  $req->status );
	    				$result_data[$ctr]["action"] = "<a ' class='btn btn-default btndefault' href='" . url("svr/review/view/$req->id/$status") . "' >VIEW</a> " . 
	    					($req->status == "FOR ACCEPTANCE" ? "<a ' class='btn btn-default btndefault' href='" . url("svr/accept/$req->id") . "' >ACCEPT</a> " : 
	    						( $req->status == "FOR PROCESSING" ? "<a ' class='btn btn-default btndefault' href='" . url("svr/process/$req->id") . "' >UPDATE</a>" : "<a ' class='btn btn-default btndefault' href='" . url("svr/review/delete/$req->id") . "' >DELETE</a>" ));
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}


	public function get_record()
	{	
			$result_data = array();
			$requests = ServiceVehicle::where("ownerid" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","curr_emp","left")
				->whereIn("status" ,["NEW","FOR APPROVAL","DISAPPROVED","FOR ACCEPTANCE","FOR PROCESSING","SERVED","CANCELLED"])
				->get(["e.firstname","e.lastname","controlno","date","requestfor" , "status","servicevehicles.id"]);

	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disabled_edit = in_array($req->status,["NEW","DISAPPROVED"]) ? "" : "disabled";
	                 	$disable_delete = in_array($req->status,["FOR APPROVAL","FOR PROCESSING","FOR ACCEPTANCE"]) ? "disabled" : "";
	                 	$result_data[$ctr]["current"] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr]["status"] = $req->status ;
	    				$result_data[$ctr]["type"] = $req->requestfor;
	    				$result_data[$ctr]["date"] = $req->date;
	    				$result_data[$ctr]["reference"] = $req->controlno;
	    				$result_data[$ctr]["action"] = "
	    					<button $disable_delete type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("svr/delete/$req->id") . "'\" >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("svr/view/$req->id") . "' >VIEW</a> " . 
	    					"<button $disabled_edit type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("svr/edit/$req->id") . "'\" >EDIT</button>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}
	
	public function get_review_record()
	{	
			$result_data = array();
			$requests = ServiceVehicle::where("curr_emp" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","ownerid","left")
				->where("isdeleted" , 0)
				->where("status","FOR APPROVAL")
				->get(["e.firstname","e.lastname","controlno","date","requestfor" , "status","servicevehicles.id"]);

	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$result_data[$ctr]["from"] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr]["status"] = $req->status ;
	    				$result_data[$ctr]["type"] = $req->requestfor;
	    				$result_data[$ctr]["date"] = $req->date;
	    				$result_data[$ctr]["reference"] = $req->controlno;
	    				$result_data[$ctr]["action"] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("svr/approval/$req->id?page=view") . "' >VIEW</a>
	    					<a ' class='btn btn-default btndefault' href='" . url("svr/approval/$req->id") . "' >APPROVE</a>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function delete_review($id)
	{
		$rec =ServiceVehicle::where("id",$id)
            ->where("curr_emp" , Session::get("employee_id"))
            ->wherein("status" , ["SERVED","CANCELLED","SERVED/DELETED" ,"CANCELLED/DELETED"])
            ->get(["controlno"])->first();

        $store =ServiceVehicle::where("id",$id)
            ->where("curr_emp" , Session::get("employee_id"))
            ->wherein("status" , ["SERVED","CANCELLED","SERVED/DELETED" ,"CANCELLED/DELETED"])
            ->update(["isdeleted"=>1]);        
                     
        if($store)
        {      
            //INSERT AUDIT
            $old = ["isdeleted"=> 0];
            $new = ["isdeleted"=> 1];
            $this->setTable('servicevehicledetails');
            $this->setPrimaryKey('id');
            $this->AU004($id , json_encode($old),  json_encode($new) , $rec["controlno"] );

            return Redirect::to("svr/reviewer")
                   ->with('successMessage', 'Service Vehicle request successfully deleted.');
        }
        else
            return Redirect::to("svr/reviewer")
                   ->with('errorMessage', 'Something went wrong upon service vehicle.');
	}

	public function delete_review_row()
	{
		$input = Input::all();
		
		
		$val_data =  [
        ];

        $messages = [
			    'service.required' => 'Service vehicle record/s is required'
			];

		if(!isset($_POST['pickup-date'])) 
		{
			$val_data = array_merge($val_data, ["service"=>"required"]);
		}

		$validator = Validator::make($input , $val_data , $messages);

		if(isset($_POST['pickup-date'])) $validator->each('service', ['required|numeric']);
		


		if ($validator->fails())
		{
			return Redirect::to( "svr/reviewer")
		           ->withInput()
		           ->withErrors($validator);
		}


		$aid = Input::get("service");                     
        foreach($aid as $id)
        {
             $rec =ServiceVehicle::where("id",$id)
                ->where("curr_emp" , Session::get("employee_id"))
                ->wherein("status" , ["SERVED","CANCELLED","SERVED/DELETED" ,"CANCELLED/DELETED"])
                ->get(["controlno"])->first();

            $store =ServiceVehicle::where("id",$id)
                ->where("curr_emp" , Session::get("employee_id"))
                ->wherein("status" , ["SERVED","CANCELLED","SERVED/DELETED" ,"CANCELLED/DELETED"])
                ->update(["isdeleted"=>1]);   

            if($store)
            {
                //INSERT AUDIT
                $old = ["isdeleted"=> 0];
                $new = ["isdeleted"=> 1];
                $this->setTable('servicevehicles');
                $this->setPrimaryKey('id');
                $this->AU004($id , json_encode($old),  json_encode($new) , $rec["controlno"] );
            }
        }


        if($store)
        {       
            return Redirect::to("svr/reviewer")
                   ->with('successMessage', 'Service Vehicle request successfully deleted.');
        }
        else
            return Redirect::to("svr/reviewer");
	}

	public function deleterecord($id)
	{
		 $rec =ServiceVehicle::where("id",$id)
                ->where("curr_emp" , Session::get("employee_id"))
                ->wherein("status" , ["SERVED","CANCELLED"])
                ->get(["controlno","status"])->first();

        $store =ServiceVehicle::where("id",$id)
            ->where("ownerid" , Session::get("employee_id"))
            ->wherein("status" , ["SERVED","CANCELLED","NEW","CANCELLED","DISAPPROVED"])
            ->update(["status"=> DB::raw("concat(status , '/DELETED')") ]);
                     
        if($store)
        {       
            //INSERT AUDIT
            $old = ["status"=> $rec["status"] . '/DELETED'];
            $new = ["status"=> $rec["status"] .  '/DELETED'];
            $this->setTable('servicevehicledetails');
            $this->setPrimaryKey('id');
            $this->AU004($id , json_encode($old),  json_encode($new) , $rec["controlno"] );

            return Redirect::to("svr/report")
                   ->with('successMessage', 'Service Vehicle request successfully deleted.');
        }
        else
            return Redirect::to("svr/report")
                   ->with('errorMessage', 'Something went wrong upon service vehicle.');
        
	}



	public function get_reviewer()
	{	
			$result_data["aaData"] = array();
			$requests = ServiceVehicle::where("curr_emp" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","ownerid","left")
				->where("isdeleted" , 0)
				->where("status","FOR APPROVAL")
				->orderBy("controlno","desc")
				->limit(5)
				->get(["e.firstname","e.lastname","controlno","date","requestfor" , "status","servicevehicles.id"]);
		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                {		  			
	                    $result_data["aaData"][$ctr][] = $req->controlno;
	    				$result_data["aaData"][$ctr][] = $req->date;
	    				$result_data["aaData"][$ctr][] = $req->requestfor;
	    				$result_data["aaData"][$ctr][] = $req->status ;
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
	    				$result_data["aaData"][$ctr][] = "<a ' class='btn btn-default btndefault' href='" . url("svr/approval/$req->id?page=view") . "' >VIEW</a> <a ' class='btn btn-default btndefault' href='" . url("svr/approval/$req->id") . "' >APPROVE</a>";
	                    $ctr++;
			                
	             	}	            
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}


	public function get_reviewer_dashboard()
	{	
			$result_data["aaData"] = array();
			$requests = ServiceVehicle::where("ownerid" , Session::get("employee_id"))
				->whereIn("status" ,["FOR APPROVAL","DISAPPROVED","FOR ACCEPTANCE","FOR PROCESSING"])
				->where("isdeleted" , 0)
				->join("employees as e" , "e.id","=","curr_emp","left")
				->orderBy("controlno","desc")
				->limit(5)
				->get(["e.firstname","e.lastname","controlno","date","requestfor" , "status","servicevehicles.id"]);
		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                {		  			
	                    $result_data["aaData"][$ctr][] = $req->controlno;
	    				$result_data["aaData"][$ctr][] = $req->date;
	    				$result_data["aaData"][$ctr][] = $req->requestfor;
	    				$result_data["aaData"][$ctr][] = $req->status ;
	    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
	    				if($req->status == "DISAPPROVED")
	    				{
	    					$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("svr/view/$req->id") . "' >VIEW</a> " . 
	    					"<button  type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("svr/edit/$req->id") . "'\" >EDIT</button>";
	                    
	    				}
	    				else
	    				{
	    					$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("svr/view/$req->id") . "' >VIEW</a> ";	                    	
	    				}
	    				
	                    $ctr++;
			                
	             	}	            
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}



}
?>