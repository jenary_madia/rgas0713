<?php namespace RGAS\Modules\OS\Requests;

use Input;
use Validator;
use Redirect;
use Session;

class Store extends RResult
{
	public function validate($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'sales_type' => 'required',
            'type' => 'required'
        ];

		if(Session::get('is_affiliate')) 
		{
			$val_data = array_merge($val_data, ["order_type"=>"required"]);
		}

		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to($id ? "os/edit/$id" : 'os/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_food($id = null)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
             'code' => 'required',
             'name' => 'required',
             'company' => 'required',
             'measure' => 'required',
             'status' => 'required',
        ]);

		$validator->each('uom', ['required']);
        $validator->each('factory', ['required|numeric']);
        $validator->each('selling', ['required|numeric']);
	
		if ($validator->fails())
		{
			return Redirect::to($id  ? "os/food/edit/$id" : "os/food/create")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_non_food($id = null)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
			 'owner' => 'required',
             'incharge' => 'required',
             'selling_from' => 'required',
             'selling_to' => 'required',
             'maximum' => 'required'
        ]);
	
		if ($validator->fails())
		{
			return Redirect::to($id  ? "os/non-food/edit/$id" : "os/non-food/create")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_process($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'payment_date' => 'required|date',
            'payment_type' => 'required',
            'payment_amount' => 'required|min:0'
        ];

        if(Input::get('payment_type') != "Cash") 
		{
			$val_data = array_merge($val_data, ["payment_reference_num"=>"required"]);
		}


		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to("os/process/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_cancel($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'comment' => 'required'
        ];

		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to("os/view/$id?page=view" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

}
?>