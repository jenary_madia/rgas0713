<?php namespace RGAS\Modules\OS;

use AUDITTRAILING;
use Session;
use RGAS\Libraries;
use Input;
use OfficeSales;
use DB;
use Redirect;
use Employees;
use Receivers;
use Mail;
use Config;
use Validator;
use PHPExcel;
use PHPExcel_IOFactory;
use AuditTrails;

use Response;

class OS extends OSLogs implements IOS
{

	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_os');
		return $this->module_id;
	}

	public function create_save()
	{	
		if(Session::get('is_affiliate'))
			$reference = OfficeSales::get_affiliate_reference_no();
		else 
			$reference = OfficeSales::get_employee_reference_no();
		$parameters = array(
		 		'deliver_to' => Input::get('delivery_to'),
		 		'delivery_add' => Input::get('delivery_address'),
		 		'delivery_tel' => Input::get('delivery_number'),
		 		'delivery_remarks' => Input::get('delivery_remarks'),
		 		'so_code' => Input::get('type'),
		 		'order_type' => Input::get('order_type'),
		 		'total_amount' => is_array(Input::get("amount")) ? array_sum(Input::get("amount")) : 0,
		 		'paid_amount' => "",
		 		'contact_no' => Input::get('contact'),
		 		'sales_type' => Input::get('sales_type'),
		 		'delivery_date' => Input::get('delivery_date') ? Input::get('delivery_date') : null,
		 		'reference_no' => $reference ,

		 		'section' => Input::get('section'),
		 		'cust_code' => Session::get('employeeid'),
		 		'cust_name' => Session::get('employee_name'),
		 		'department' => Session::get('dept_name'),
		 		'company' => Session::get('company'),
		 		
		 		'ownerid' => Session::get('employee_id'),
		 		'status' => "N",
		 		'trans_date' => date("Y-m-d"), 
		 		'create_date' => date("Y-m-d"), 
		 		'modified_by' => Session::get('employee_name'),
		 		'modified_date' => date("Y-m-d"), 
		 		'create_by' => Session::get('employee_name'),
		 		'curr_emp' => Session::get('employee_id'),	
		 		'comment_save' => Input::get('comment'),	 		

		 		"owner_code" => "",
		 		"transmittal" => "",
		 		"owner_code" => ""
		 		);

		$store =OfficeSales::create($parameters);

		//INSERT AUDIT
		$this->setTable('orderentries');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		$product = Input::get("product");
		$quantity = Input::get("qty");
		$unit = Input::get("uom");
		$price = Input::get("price");
	
		if(!empty($product)):
		foreach($product as $key=>$prod_code)
        {
            $detail[] = array(
                    "orderentryid" => $store->id,
                    "prod_code" => $prod_code,
                    "uom_code" => $unit[$key],
                    "order_qnty" => $quantity[$key],
                    "price" => $price[$key],
                    'modified_by' => Session::get('employee_name'),
		 			'modified_date' => date("Y-m-d"), 
                    "owner_code" => "",//san to?
                    "served_qnty" => "",//san to?
                    "allocated_qnty" => ""//san to?
                );

            //INSERT AUDIT
			$this->setTable('orderdetails');
			$this->setPrimaryKey('orderentries.id');
			$this->AU001($store->id , json_encode(end($detail)) , $reference );
        }


        DB::table("orderdetails")->insert( $detail ); 
        endif;

		if ($store) 
			return Redirect::to('os/report')
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY SAVED AND CAN BE EDITED LATER.');
		else
			return Redirect::to('os/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}

	public function create_sent()
	{	
		$reciver_comp = Session::get('company') == "BUK" ? "BUK" : "RBC-CORP";
		$assign = Employees::where("code",Input::get('sales_type') == "FOOD" ? "OFFICESALE_RECEIVER" : "OFFICESALE_RECEIVER_NON" )
						->where("receivers.company", $reciver_comp)
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->first(["firstname","lastname","email", "receivers.employeeid"]);

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];

		if(Session::get('is_affiliate'))
			$reference = OfficeSales::get_affiliate_reference_no();
		else 
			$reference = OfficeSales::get_employee_reference_no();

		$parameters = array(
		 		'deliver_to' => Input::get('delivery_to'),
		 		'delivery_add' => Input::get('delivery_address'),
		 		'delivery_tel' => Input::get('delivery_number'),
		 		'delivery_remarks' => Input::get('delivery_remarks'),
		 		'so_code' => Input::get('type'),
		 		'order_type' => Input::get('order_type'),
		 		'total_amount' => is_array(Input::get("amount")) ? array_sum(Input::get("amount")) : 0,
		 		'paid_amount' => "",
		 		'contact_no' => Input::get('contact'),
		 		'sales_type' => Input::get('sales_type'),
		 		'delivery_date' => Input::get('delivery_date') ? Input::get('delivery_date') : null,
		 		'reference_no' => $reference ,

		 		'section' => Input::get('section'),
		 		'cust_code' => Session::get('employeeid'),
		 		'cust_name' => Session::get('employee_name'),
		 		'department' => Session::get('dept_name'),
		 		'company' => Session::get('company'),
		 		
		 		'ownerid' => Session::get('employee_id'),
		 		'status' => "O",
		 		'remarks' => ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
		 		'trans_date' => date("Y-m-d"), 
		 		'create_date' => date("Y-m-d"), 
		 		'modified_by' => Session::get('employee_name'),
		 		'modified_date' => date("Y-m-d"), 
		 		'submit_date' => date("Y-m-d"),
		 		'create_by' => Session::get('employee_name'),
		 		'curr_emp' => $assign["employeeid"],	
		 		'comment_save' => "",	 		

		 		"owner_code" => "",
		 		"transmittal" => "",
		 		"owner_code" => ""
		 		);

		$store =OfficeSales::create($parameters);

		//INSERT AUDIT
		$this->setTable('orderentries');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		$product = Input::get("product");
		$quantity = Input::get("qty");
		$unit = Input::get("uom");
		$price = Input::get("price");
	
		if(!empty($product)):
		foreach($product as $key=>$prod_code)
        {
            $detail[] = array(
                    "orderentryid" => $store->id,
                    "prod_code" => $prod_code,
                    "uom_code" => $unit[$key],
                    "order_qnty" => $quantity[$key],
                    "price" => $price[$key],
                    'modified_by' => Session::get('employee_name'),
		 			'modified_date' => date("Y-m-d")
                );

            //INSERT AUDIT
			$this->setTable('orderdetails');
			$this->setPrimaryKey('orderentries.id');
			$this->AU001($store->id , json_encode(end($detail)) , $reference );
        }


        DB::table("orderdetails")->insert( $detail ); 
        endif;

        //SEND EMAIL NOTIFICATION
        Mail::send(Session::get('is_affiliate') ? "os.email.affiliate" : 'os.email.employee', array(
            'reference' => $reference,
            'filer' =>  Session::get('employee_name'),
            'company' =>  Session::get('company'),
            'department' => Session::get('dept_name')),
            function($message ) use ($name , $email)
            {
                $message->to($email , $name )->subject('RGAS Notification Alert: Office Sales Order for Processing');
            }
        );

		if ($store) 
			return Redirect::to('os/report')
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY SENT TO THE OFFICE SALES RECEIVER.');
		else
			return Redirect::to('os/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}



	public function edit_save($id)
	{	
		$parameters = array(
		 		'deliver_to' => Input::get('delivery_to'),
		 		'delivery_add' => Input::get('delivery_address'),
		 		'delivery_tel' => Input::get('delivery_number'),
		 		'delivery_remarks' => Input::get('delivery_remarks'),
		 		'so_code' => Input::get('type'),
		 		'order_type' => Input::get('order_type'),
		 		'total_amount' => is_array(Input::get("amount")) ? array_sum(Input::get("amount")) : 0,
		 		'contact_no' => Input::get('contact'),
		 		'sales_type' => Input::get('sales_type'),
		 		'delivery_date' => Input::get('delivery_date') ? Input::get('delivery_date') : null,	
		 		'section' => Input::get('section'),
		 		'trans_date' => date("Y-m-d"), 
		 		'modified_by' => Session::get('employee_name'),
		 		'modified_date' => date("Y-m-d"),
		 		'comment_save' => Input::get('comment') 
		 		);

		OfficeSales::where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["N"])
        	->update($parameters);


		$product = Input::get("product");
		$quantity = Input::get("qty");
		$unit = Input::get("uom");
		$price = Input::get("price");
			
		DB::table("orderdetails")
			->where("orderentryid",$id)
			->delete(); 

		if(!empty($product)):
		foreach($product as $key=>$prod_code)
        {
            $detail[] = array(
                    "orderentryid" => $id,
                    "prod_code" => $prod_code,
                    "uom_code" => $unit[$key],
                    "order_qnty" => $quantity[$key],
                    "price" => $price[$key],
                    'modified_by' => Session::get('employee_name'),
		 			'modified_date' => date("Y-m-d")
		 			 );
        }


        DB::table("orderdetails")->insert( $detail ); 
        endif;

		return Redirect::to("os/report")
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY SAVED AND CAN BE EDITED LATER.');
		
	}


	public function edit_sent($id)
	{	
		$reciver_comp = Session::get('company') == "BUK" ? "BUK" : "RBC-CORP";
		$assign = Employees::where("code", Input::get('sales_type') == "FOOD" ? "OFFICESALE_RECEIVER" : "OFFICESALE_RECEIVER_NON"  )
						->where("receivers.company", $reciver_comp)
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->first(["firstname","lastname","email", "receivers.employeeid"]);

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];

		$parameters = array(
		 		'deliver_to' => Input::get('delivery_to'),
		 		'delivery_add' => Input::get('delivery_address'),
		 		'delivery_tel' => Input::get('delivery_number'),
		 		'delivery_remarks' => Input::get('delivery_remarks'),
		 		'so_code' => Input::get('type'),
		 		'order_type' => Input::get('order_type'),
		 		'total_amount' => is_array(Input::get("amount")) ? array_sum(Input::get("amount")) : 0,
		 		'contact_no' => Input::get('contact'),
		 		'sales_type' => Input::get('sales_type'),
		 		'delivery_date' => Input::get('delivery_date') ? Input::get('delivery_date') : null,	
		 		'section' => Input::get('section'),
		 		'remarks' => ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
		 		'trans_date' => date("Y-m-d"), 
		 		'modified_by' => Session::get('employee_name'),
		 		'modified_date' => date("Y-m-d"), 
		 		"status" =>"O",
		 		'curr_emp' => $assign["employeeid"],
		 		'comment_save' => ""
		 		);

		OfficeSales::where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->wherein("status",["N"])
        	->update($parameters);

		$product = Input::get("product");
		$quantity = Input::get("qty");
		$unit = Input::get("uom");
		$price = Input::get("price");
	
		DB::table("orderdetails")
			->where("orderentryid",$id)
			->delete(); 
		if(!empty($product)):
		foreach($product as $key=>$prod_code)
        {
            $detail[] = array(
                    "orderentryid" => $id,
                    "prod_code" => $prod_code,
                    "uom_code" => $unit[$key],
                    "order_qnty" => $quantity[$key],
                    "price" => $price[$key],
                    'modified_by' => Session::get('employee_name'),
		 			'modified_date' => date("Y-m-d")
                );
        }


        DB::table("orderdetails")->insert( $detail ); 
        endif;

        //SEND EMAIL NOTIFICATION
        Mail::send(Session::get('is_affiliate') ? "os.email.affiliate" : 'os.email.employee', array(
            'reference' => Input::get('reference'),
            'filer' =>  Session::get('employee_name'),
            'company' =>  Session::get('company'),
            'department' => Session::get('dept_name')),
            function($message ) use ($name , $email)
            {
                $message->to($email , $name )->subject('RGAS Notification Alert: Office Sales Order for Processing');
            }
        );

		return Redirect::to('os/report')
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY SENT TO THE OFFICE SALES RECEIVER.');

	}

	public function process_save($id)
	{	
		OfficeSales::where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->wherein("status",["R"])
        	->update(["delivery_date"=>Input::get('delivery_date') ? Input::get('delivery_date') : null ,"paid_amount"=>Input::get('payment_amount')]);

		$parameters = array(
		 		'payment_type' => Input::get('payment_type'),
		 		'payment_amount' => Input::get('payment_amount'),
		 		'payment_ref_no' => Input::get('payment_reference_num'),
		 		'payment_date' => Input::get('payment_date') ? Input::get('payment_date') : "",
		 		'remarks' => Input::get('payment_remarks')		 		
		 		);
		if(Input::get('payment'))
		{
			DB::table("officesale_payments")->where("orderentryid" , $id)
        	->update($parameters);
		}
        else
        {
        	$parameters["orderentryid"] = $id;
        	DB::table("officesale_payments")
        	->insert($parameters);
        }

        //DELETE ATTACHMENT
        $attachment = Input::get("attachment_id");
        if(!empty($attachment))
        {
            DB::table("officesale_attachments")
                ->where("orderentryid" , $id)
                ->wherein("id",$attachment)
                ->delete(); 
        }

        //ADD ATTACHMENT
        $files = Input::get("files");
        $attach = array();
        $fm = new Libraries\FileManager;
        
        if(!empty($files)):
            foreach($files as $file)
            {
                $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'os/' .  Input::get("reference") );
                $attach[] = array(
                        "orderentryid" => $id,
                        "attachment_type" => 2,
                        "fn" => $file['original_filename'],
                        "employee_id" => Session::get("employee_id"),
                        "ts_uploaded" => date("Y-m-d H:i:s"),
                        "code" => json_encode($file)
                    );

            }
        endif;

        if(!empty($attach))  DB::table("officesale_attachments")->insert( $attach ); 

		return Redirect::to("os/receiver/report")
		           ->with('successMessage', 'PAYMENT DETAIL SUCCESSFULLY SAVED.');		
	}


	public function process_close($id)
	{	
		OfficeSales::where("id" , $id)
        	->where("curr_emp" , Session::get("employee_id"))
        	->wherein("status",["R"])
        	->update(["delivery_date"=>Input::get('delivery_date'),"paid_amount"=>Input::get('payment_amount') ]);

		$parameters = array(
		 		'payment_type' => Input::get('payment_type'),
		 		'payment_amount' => Input::get('payment_amount'),
		 		'payment_ref_no' => Input::get('payment_reference_num'),
		 		'payment_date' => Input::get('payment_date') ? Input::get('payment_date') : "",
		 		'remarks' => Input::get('payment_remarks')	,
		 		'status' => 1	 		
		 		);
		if(Input::get('payment'))
		{
			DB::table("officesale_payments")->where("orderentryid" , $id)
        	->update($parameters);
		}
        else
        {
        	$parameters["orderentryid"] = $id;
        	DB::table("officesale_payments")
        	->insert($parameters);
        }

        //DELETE ATTACHMENT
        $attachment = Input::get("attachment_id");
        if(!empty($attachment))
        {
            DB::table("officesale_attachments")
                ->where("orderentryid" , $id)
                ->wherein("id",$attachment)
                ->delete(); 
        }

        //ADD ATTACHMENT
        $files = Input::get("files");
        $attach = array();
        $fm = new Libraries\FileManager;
        
        if(!empty($files)):
            foreach($files as $file)
            {
                $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'os/' .  Input::get("reference") );
                $attach[] = array(
                        "orderentryid" => $id,
                        "attachment_type" => 2,
                        "fn" => $file['original_filename'],
                        "employee_id" => Session::get("employee_id"),
                        "ts_uploaded" => date("Y-m-d H:i:s"),
                        "code" => json_encode($file)
                    );

            }
        endif;

        if(!empty($attach))  DB::table("officesale_attachments")->insert( $attach ); 

         //INSERT AUDIT
		$old = ["status"=> "For Editing"];
		$new = ["status"=> "Closed"];
		$this->setTable('officesale_payments');
		$this->setPrimaryKey('orderentries.id');
		$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		return Redirect::to("os/receiver/report")
		           ->with('successMessage', 'PAYMENT DETAIL SUCCESSFULLY CLOSED.');		
	}


/*************************************************************************************/
	public function create_food_save()
	{
		$parameters = array(
		 		'prod_code' => Input::get('code'),
		 		'comp_code' => Input::get('company'),
		 		'prod_name' => Input::get('name'),
		 		'def_uom_code' => Input::get('measure'),
		 		'status' => Input::get('status'),
		 		'create_date' => date("Y-m-d H:i:s"),
		 		'create_by' => Session::get('employee_name'),
		 		'modified_date' => date("Y-m-d H:i:s"),
		 		'modified_by' => Session::get('employee_name'),
		 		'comp_group' => Session::get('is_office_receiver') 		
		 		);

		$store = DB::table("products")->insert( $parameters ); 

		$uom = Input::get("uom");
		$factory = Input::get("factory");
		$selling = Input::get("selling");

		if(!empty($uom)):
		foreach($uom as $key=>$val)
        {
            $detail[] = array(
                    "comp_code" => Input::get('company'),
                    "prod_code" => Input::get('code'),
                    "uom_code" => $val,
                    "factory_price" => $factory[$key],
                    "selling_price" => $selling[$key],
                    "modified_date" => date("Y-m-d H:i:s"),
                    "modified_by" =>  Session::get('employee_name'),
                    "comp_group" => Session::get('is_office_receiver')	               
                );
        }


        DB::table("productprices")->insert( $detail ); 
        endif;

		if ($store) 
			return Redirect::to('os/food/report')
		           ->with('successMessage', 'PRODUCT SUCCESSFULLY ADDED TO THE LIST.');
		else
			return Redirect::to('os/food/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}

	public function edit_food_save($id)
	{
		$record = DB::table("products")
        ->where("id" , $id)
        ->get(["prod_code","comp_code","prod_name","def_uom_code","status","comp_group"]);

		$parameters = array(
		 		'prod_code' => Input::get('code'),
		 		'comp_code' => Input::get('company'),
		 		'prod_name' => Input::get('name'),
		 		'def_uom_code' => Input::get('measure'),
		 		'status' => Input::get('status'),
		 		'modified_date' => date("Y-m-d H:i:s"),
		 		'modified_by' => Session::get('employee_name')		
		 		);

		$store = DB::table("products")
		->where("id" , $id)
		->update( $parameters ); 

		$uom = Input::get("uom");
		$factory = Input::get("factory");
		$selling = Input::get("selling");

		DB::table("productprices")
			->where("comp_code", $record[0]->comp_code)
	        ->where("prod_code", $record[0]->prod_code)
	        ->where("comp_group", $record[0]->comp_group)
			->delete(); 

		if(!empty($uom)):
		foreach($uom as $key=>$val)
        {
            $detail[] = array(
                    "comp_code" => Input::get('company'),
                    "prod_code" => Input::get('code'),
                    "uom_code" => $val,
                    "factory_price" => $factory[$key],
                    "selling_price" => $selling[$key],
                    "modified_date" => date("Y-m-d H:i:s"),
                    "modified_by" =>  Session::get('employee_name'),
                    "comp_group" => Session::get('is_office_receiver')	                  
                );
        }


        DB::table("productprices")->insert( $detail ); 
        endif;

		return Redirect::to("os/food/report")
		           ->with('successMessage', 'PRODUCT SUCCESSFULLY UPDATED TO THE LIST.');

	}

	public function edit_delete($id)
	{
		$record = DB::table("products")
        ->where("id" , $id)
        ->get(["prod_code","comp_code","prod_name","def_uom_code","status","comp_group"]);

		$store = DB::table("products")
		->where("id" , $id)
		->delete(); 

		DB::table("productprices")
			->where("comp_code", $record[0]->comp_code)
	        ->where("prod_code", $record[0]->prod_code)
	        ->where("comp_group", $record[0]->comp_group)
			->delete(); 

		return Redirect::to("os/food/report")
		           ->with('successMessage', 'PRODUCT SUCCESSFULLY DELETED TO THE LIST.');

	}

	public function create_non_food_save()
	{
		$last_id =DB::table("officesale_projects")
				->where("comp_group" , Session::get("company"))
				->orderby("project_id","desc")
                ->get(["project_id"]);

		$code = "OSNF." . date("y") . "." . sprintf("%'.03d", (isset($last_id[0]->project_id) ? $last_id[0]->project_id : 0) + 1);;

		$allowable = is_array(Input::get('allowable')) ? Input::get('allowable') : array();
		$parameters = array(
		 		'owner' => Input::get('owner'),
		 		'code' => $code,
		 		'incharge' => Input::get('incharge'),
		 		'name' => Input::get('desc'),
		 		'selling_from' => Input::get('selling_from'),
		 		'selling_to' => Input::get('selling_to'),
		 		'allowable' => count($allowable) == 2 ? 0 : ($allowable[0] == "cash" ? 1 : 2), 
		 		'status' => Input::get('status'),
		 		'limit' => Input::get('maximum'),
		 		'comp_group' => Session::get('is_office_receiver_non_food') 		
		 		);

		$store = DB::table("officesale_projects")->insertGetId( $parameters ,"ID"); 

		$uom = Input::get("uom-code");
		$code = Input::get("item-code");
		$desc = Input::get("item-desc");
		$price = Input::get("selling-price");
		$status = Input::get("item-status");

		if(!empty($uom)):
		foreach($uom as $key=>$val)
        {
            $detail[] = array(
                    "comp_group" => Session::get('is_office_receiver_non_food'),
                    "code" => $code[$key],
                    "desc" => $desc[$key],
                    "uom" => $val,
                    "price" => $price[$key],
                    "status" =>  $status[$key],
                    "project_id" =>$store                
                );
        }


        DB::table("officesale_project_items")->insert( $detail ); 
        endif;

		if ($store) 
			return Redirect::to('os/non-food/report')
		           ->with('successMessage', 'PRODUCT SUCCESSFULLY ADDED TO THE LIST.');
		else
			return Redirect::to('os/non-food/create')
		           ->with('errorMessage', 'Something went wrong upon submitting service vehicle');
	}

	public function edit_non_food_save($id)
	{
		$allowable = is_array(Input::get('allowable')) ? Input::get('allowable') : array();
		$parameters = array(
		 		'owner' => Input::get('owner'),
		 		'name' => Input::get('desc'),
		 		'selling_from' => Input::get('selling_from'),
		 		'selling_to' => Input::get('selling_to'),
		 		'allowable' => count($allowable) == 2 ? 0 : ($allowable[0] == "cash" ? 1 : 2), 
		 		'status' => Input::get('status'),
		 		'limit' => Input::get('maximum'),	 		
		 		);

		$store = DB::table("officesale_projects")
		->where("project_id" , $id)
		->update( $parameters ); 

		DB::table("officesale_project_items")
			->where("project_id" , $id)
			->delete(); 

		$uom = Input::get("uom-code");
		$code = Input::get("item-code");
		$desc = Input::get("item-desc");
		$price = Input::get("selling-price");
		$status = Input::get("item-status");

		if(!empty($uom)):
		foreach($uom as $key=>$val)
        {
            $detail[] = array(
                    "comp_group" => Session::get('is_office_receiver_non_food') ,
                    "code" => $code[$key],
                    "desc" => $desc[$key],
                    "uom" => $val,
                    "price" => $price[$key],
                    "status" =>  $status[$key],
                    "project_id" =>$id                
                );
        }

        DB::table("officesale_project_items")->insert( $detail ); 
        endif;


		return Redirect::to("os/non-food/report")
		           ->with('successMessage', 'PRODUCT SUCCESSFULLY UPDATED TO THE LIST.');
	}

	public function get_affiliate_record()
	{	
			$result_data = array();
			$requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
				->join("employees" , "ownerid" , "=" , "employees.id")
				->Leftjoin("officesale_payments" , "orderentryid" , "=" , "orderentries.id")	
				->join("companylists" , "comp_code" , "=" , "employees.company")		
				->Leftjoin("sotypes" , "sotypes.so_code" , "=" , "orderentries.so_code")			
				->where("affiliate",1)
				->where("orderentries.status", "R")
				->where("sotypes.group", 2)
				//->where("isdeleted",0)
				->get(["reference_no","trans_date","orderentries.status","cust_name","orderentries.id",
					"order_type","so_name","employees.company","orderentries.total_amount",
					"payment_type","payment_amount","payment_date",
					"payment_ref_no","delivery_date","officesale_payments.remarks","department","officesale_payments.status as pstats"]);

			$status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$result_data[$ctr]["department"] = $req->department;
	                 	$result_data[$ctr]["requestedby"] = $req->cust_name;
	    				$result_data[$ctr]["status"] = $status[$req->status];
	    				$result_data[$ctr]["trans_date"] = $req->trans_date;
	    				$result_data[$ctr]["reference"] = $req->reference_no;

	    				$result_data[$ctr]["order_type"] = $req->order_type;
	    				$result_data[$ctr]["sales_type"] = $req->so_name;
	    				$result_data[$ctr]["company"] = $req->company;
	    				$result_data[$ctr]["office_sales_amount"] = $req->total_amount;
	    				$result_data[$ctr]["payment_type"] = $req->payment_type;
	    				$result_data[$ctr]["payment_amount"] = $req->payment_amount;
	    				$result_data[$ctr]["payment_date"] = $req->payment_date == "0000-00-00" ? "" : $req->payment_date ;
	    				$result_data[$ctr]["payment_ref_no"] = $req->payment_ref_no;
	    				$result_data[$ctr]["delivery_date"] = $req->delivery_date;
	    				$result_data[$ctr]["remarks"] = $req->remarks;
	    				$result_data[$ctr]["pstats"] = $req->pstats;
	    				$result_data[$ctr]["action"] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("os/affiliate/view/$req->id") . "' >VIEW</a>" . 
	    					($req->pstats  ? "<button disabled class='btn btn-default btndefault' >UPDATE</button>" :  "<a ' class='btn btn-default btndefault' href='" . url("os/process/$req->id") . "' >UPDATE</a>");
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function get_tobe_affiliate_record()
	{	
			$result_data = array();
			$requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
				->join("employees" , "ownerid" , "=" , "employees.id")
				->join("companylists" , "comp_code" , "=" , "employees.company")				
				->where("affiliate",1)
				->where("isdeleted",0)
				->where("orderentries.status", "O")
				->get(["reference_no","trans_date","status","cust_name","orderentries.id","department","employees.company"]);

			$status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$result_data[$ctr]["company"] = $req->company;
	    				$result_data[$ctr]["department"] =  $req->department;
	                 	$result_data[$ctr]["requestedby"] = $req->cust_name;
	    				$result_data[$ctr]["status"] = $status[$req->status];
	    				$result_data[$ctr]["trans_date"] = $req->trans_date;
	    				$result_data[$ctr]["reference"] = $req->reference_no;
	    				$result_data[$ctr]["action"] = "<a ' class='btn btn-default btndefault' href='" . url("os/affiliate/view/$req->id") . "' >VIEW</a>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}	

	public function get_employee_record($status = "R")
	{	
			$result_data = array();
			
			$requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
				->join("employees" , "ownerid" , "=" , "employees.id")
				->join("companylists" , "comp_code" , "=" , "employees.company")				
				->where("affiliate",0)
				//->where("isdeleted",0)
				->where("status", $status)
				->bukCompany()
				->get(["reference_no","trans_date","status","cust_name","orderentries.id","employees.company","department"]);

			$status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {	
	                 	$result_data[$ctr]["company"] = $req->company;
	                 	$result_data[$ctr]["department"] = $req->department;			
	                 	$result_data[$ctr]["requestedby"] = $req->cust_name;
	    				$result_data[$ctr]["status"] = $status[$req->status];
	    				$result_data[$ctr]["trans_date"] = $req->trans_date;
	    				$result_data[$ctr]["reference"] = $req->reference_no;
	    				$result_data[$ctr]["action"] = "<a ' class='btn btn-default btndefault' href='" . url("os/form/view/$req->id") . "' >VIEW</a>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}


	public function get_record()
	{	
			$result_data = array();
			$requests = OfficeSales::where("ownerid" , Session::get("employee_id"))
				->where("isdeleted",0)
				->join("employees as e" , "e.id","=","curr_emp","left")
				->get(["e.firstname","e.lastname","reference_no","trans_date" , "status","orderentries.id"]);

			$status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {	
	                 	$disable_edit = in_array($req->status,["N"]) ? "" : "disabled";			
	                 	$disable_delete = in_array($req->status,["O","C"]) ? "disabled" : "";
	                 	$result_data[$ctr]["requestedby"] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr]["status"] = $status[$req->status] ;
	    				$result_data[$ctr]["trans_date"] = $req->trans_date;
	    				$result_data[$ctr]["reference"] = $req->reference_no;
	    				$result_data[$ctr]["action"] = "
	    					<button $disable_delete type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("os/delete/$req->id") . "'\" >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("os/view/$req->id?page=view") . "' >VIEW</a> " . 
	    					 "<button $disable_edit  type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("os/edit/$req->id") . "'\" >EDIT</button>";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function view_cancel($id)
	{	
		$reciver_comp = Session::get('company') == "BUK" ? "BUK" : "RBC-CORP";
		$assign = Employees::where("code","OFFICESALE_RECEIVER")
						->where("receivers.company", $reciver_comp)
					//	->where("isdeleted",0)
		                ->join("receivers","employees.id","=", "receivers.employeeid")
		                ->first(["firstname","lastname","email", "receivers.employeeid"]);

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];



        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ;
        $parameters = array(
		 		'status' => "C",
		 		'ownerid' => Session::get("employee_id"),
		 		'remarks' =>  DB::raw("concat(remarks , '$comment')")		 		
		 		);

		$store =OfficeSales::where("id" , $id)
        	->where("ownerid" , Session::get("employee_id"))
        	->where("status" , "O")
        	->update($parameters);

        //SEND EMAIL NOTIFICATION
        Mail::send(Session::get('is_affiliate') ? "os.email.affiliate_cancel" : 'os.email.employee_cancel', array(
            'reference' => Input::get('reference'),
            'filer' =>  Session::get('employee_name'),
            'company' =>  Session::get('company'),
            'department' => Session::get('dept_name')),
            function($message ) use ($name , $email)
            {
                $message->to($email , $name )->subject('RGAS Notification Alert: Cancelled Office Sales Order');
            }
        );


		if ($store) 
			return Redirect::to("os/report")
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY CANCELLED.');
		else
			return Redirect::to("os/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting art design.');
	}

	public function deleterecord($id)
	{	
		$rec =OfficeSales::where("id",$id)
                ->where("status", "!=" , "O" )
				->where("ownerid" , Session::get("employee_id"))
                ->get(["reference_no"])->first();

		$store = OfficeSales::where("id",$id)
            ->where("status", "!=" , "O" )
			->where("ownerid" , Session::get("employee_id"))
            ->update(["isdeleted"=>1]);   
			

		if($store)
		{		

			//INSERT AUDIT
            $old = ["isdeleted"=> 0];
            $new = ["isdeleted"=> 1];
            $this->setTable('orderentries');
            $this->setPrimaryKey('id');
            $this->AU004($id , json_encode($old),  json_encode($new) , $rec["reference_no"] );

			return Redirect::to("os/report")
		           ->with('successMessage', 'OFFICE SALES ORDER SUCCESSFULLY DELETED.');
		}
		else
			return Redirect::to("os/edit/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting clearance.');
		
	}

	public function get_food_record()
	{	
			$result_data = array();
			$requests = DB::table("products")				
				->join("systemconfigs","def_uom_code" , "=" , "syscon_code")	
				->where("comp_group" , Session::get("company"))
				->get(["prod_code","prod_name","comp_code","syscon_desc","status","products.id"]);

			$status["Y"] = "Active";
			$status["N"] = "Inactive";
			
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$result_data[$ctr]["code"] = $req->prod_code;
	    				$result_data[$ctr]["status"] = $status[$req->status] ;
	    				$result_data[$ctr]["uom"] = $req->syscon_desc;
	    				$result_data[$ctr]["name"] = $req->prod_name;
	    				$result_data[$ctr]["company"] = $req->comp_code;
	    				$result_data[$ctr]["id"] = $req->id;
	    				 $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function get_non_food_record()
	{	
			$result_data = array();
			$requests = DB::table("officesale_projects")				
				->where("comp_group" , Session::get("company"))
				->get(["code","name","status","project_id"]);

			$status[1] = "Active";
			$status[0] = "Inactive";
			
	    	if ( count($requests) > 0)
	        {
	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$result_data[$ctr]["code"] = $req->code;
	    				$result_data[$ctr]["status"] = $status[$req->status] ;
	    				$result_data[$ctr]["name"] = $req->name;
	    				$result_data[$ctr]["id"] = $req->project_id;
	    				 $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function employee_food()
	{
            $input = Input::all();
            $val_data = ['date_from' => 'required|date',
                         'date_to' => 'required|date'];
            
            $validator = Validator::make($input , $val_data);
            //RUN LARAVEL VALIDATION    
            if ($validator->fails()) 
            	return Redirect::to( "os/export/employees/food")
		           ->withInput()
		           ->withErrors($validator);

            include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
		
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 0;

         //   $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", 'DATE');
         //   $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", 'TIME');
         //   $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", 'EMPLOYEE NAME');
                    
            foreach(range('A','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            
            
            $start = Input::get("date_from");
            $end = Input::get("date_to");
            
             $requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
						->join("employees" , "ownerid" , "=" , "employees.id")
						->join("companylists" , "comp_code" , "=" , "employees.company")	
						->join("orderdetails" , "orderentryid" , "=" , "orderentries.id")				
						->where("affiliate",0)
						->where("orderentries.status", "O")
						->where("sales_type" , "FOOD")
						->where("isdeleted",0)
						->whereBetween("trans_date",[$start,$end])
						->bukCompany()
						->get(["orderentries.id","orderentries.owner_code","orderentries.deliver_to","orderentries.delivery_add","orderentries.delivery_tel",
							"orderentries.delivery_remarks","orderentries.cust_code","orderentries.cust_name","orderentries.trans_date","orderentries.so_code",
							"orderentries.status","orderentries.remarks","orderentries.create_date","orderentries.create_by","orderentries.modified_date",
							"orderentries.modified_by","orderentries.total_amount","orderentries.paid_amount","orderentries.transmittal","orderentries.ordertypeno",
							"orderentries.submit_date","orderdetails.id as did","orderdetails.prod_code","orderdetails.uom_code","orderdetails.order_qnty","orderdetails.allocated_qnty",
							"orderdetails.served_qnty","orderdetails.price","orderdetails.dtl_seq","orderdetails.modified_date as mod_date","orderdetails.modified_by as mod_by",
							"orderentries.company","orderentries.order_type","orderentries.reference_no"]);
             
              $UPT_ARR= array();
              foreach($requests as $req)
                 {	
                 	$UPT_ARR[] = $req->id;
                    $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req->id);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req->owner_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req->deliver_to);  
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req->delivery_add);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req->delivery_tel);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req->delivery_remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req->cust_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req->cust_name);
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req->trans_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $req->so_code == "CH-A" ? "CH" : $req->so_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", $req->status);
                    $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req->remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req->create_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $req->create_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req->modified_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req->modified_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req->total_amount);
              //      $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->total_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->paid_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", $req->transmittal);     
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", $req->ordertypeno);     
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", $req->submit_date);     
                    $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", $req->did);     
                    $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", $req->prod_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", $req->uom_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", $req->order_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", $req->allocated_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", $req->served_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", $req->price);
                    $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", $req->dtl_seq);
                    $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", $req->mod_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", $req->mod_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", in_array($req->company, ['RBC-CORP', 'RBC-SAT'] ) ? "RBC"  : $req->company );
                    $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", $req->order_type);
                    $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", $req->reference_no);               
                 }

                 if(!empty($UPT_ARR))
                 {
                 	OfficeSales::whereIn("id" ,$UPT_ARR)->where("Status","O")->update(["Status"=>"R"]);
                 }


            //WRITE AND DOWNLOAD EXEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            ob_end_clean();

            $prnt_tstamp = date('YmdHis',time());
	        $user = Session::get("employee_id");
            $fname = "rgas";

            $objWriter->save("../app/storage/os/$fname.csv");
            $objPHPExcel->disconnectWorksheets();

           
             //INSERT AUDIT
	        $parameters["Start_date"] = $start ;
	        $parameters["End_date"] = $end ;
			$this->AU006(json_encode($parameters) , $fname . ".csv" );


            return Response::download("../app/storage/os/$fname.csv", "rgas.csv");
	}

	public function employee_non_food()
	{
            $input = Input::all();
            $val_data = ['project' => 'required|',
            			 'date_from' => 'required|date',
                         'date_to' => 'required|date'];
            
            $validator = Validator::make($input , $val_data);
            //RUN LARAVEL VALIDATION    
            if ($validator->fails()) 
            	return Redirect::to( "os/export/employees/non_food")
		           ->withInput()
		           ->withErrors($validator);

            include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
		
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 0;

           
                    
            foreach(range('A','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            
            
            $start = Input::get("date_from");
            $end = Input::get("date_to");
            $project = Input::get("project");
            
             $requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
						->join("employees" , "ownerid" , "=" , "employees.id")
						->join("companylists" , "comp_code" , "=" , "employees.company")

						->join("orderdetails" , "orderentryid" , "=" , "orderentries.id")
						->join('officesale_project_items as d', function ($join) 
	                    {
	                        $join->on("d.code","=","orderdetails.prod_code")
	                        ->on("d.uom","=","orderdetails.uom_code");	
	                    })
						->where("comp_group", Session::get("is_office_receiver_non_food"))
						->where("affiliate",0)
						->where("orderentries.status", "O")
						->where("sales_type" , "NON-FOOD")
						->where("isdeleted",0)
					//	->where("project_id" , $project)
						->ProjectCondition($project)
						->whereBetween("trans_date",[$start,$end])
						->bukCompany()
						->get(["orderentries.id","orderentries.owner_code","orderentries.deliver_to","orderentries.delivery_add","orderentries.delivery_tel",
							"orderentries.delivery_remarks","orderentries.cust_code","orderentries.cust_name","orderentries.trans_date","orderentries.so_code",
							"orderentries.status","orderentries.remarks","orderentries.create_date","orderentries.create_by","orderentries.modified_date",
							"orderentries.modified_by","orderentries.total_amount","orderentries.paid_amount","orderentries.transmittal","orderentries.ordertypeno",
							"orderentries.submit_date","orderdetails.id as did","orderdetails.prod_code","orderdetails.uom_code","orderdetails.order_qnty","orderdetails.allocated_qnty",
							"orderdetails.served_qnty","orderdetails.price","orderdetails.dtl_seq","orderdetails.modified_date as mod_date","orderdetails.modified_by as mod_by",
							"orderentries.company","orderentries.order_type","orderentries.reference_no"]);
             
              $UPT_ARR= array();
              foreach($requests as $req)
                 {	
                 	$UPT_ARR[] = $req->id;
	                $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req->id);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req->owner_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req->deliver_to);  
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req->delivery_add);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req->delivery_tel);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req->delivery_remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req->cust_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req->cust_name);
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req->trans_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $req->so_code == "CH-A" ? "CH" : $req->so_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", $req->status);
                    $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req->remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req->create_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $req->create_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req->modified_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req->modified_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req->total_amount);
                 //   $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->total_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->paid_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", $req->transmittal);     
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", $req->ordertypeno);     
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", $req->submit_date);     
                    $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", $req->did);     
                    $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", $req->prod_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", $req->uom_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", $req->order_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", $req->allocated_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", $req->served_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", $req->price);
                    $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", $req->dtl_seq);
                    $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", $req->mod_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", $req->mod_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", in_array($req->company, ['RBC-CORP', 'RBC-SAT'] ) ? "RBC"  : $req->company );
                    $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", $req->order_type);
                    $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", $req->reference_no);                    
                 }

                 if(!empty($UPT_ARR))
                 {
                 	OfficeSales::whereIn("id" ,$UPT_ARR)->where("Status","O")->update(["Status"=>"R"]);
                 }


            //WRITE AND DOWNLOAD EXEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            ob_end_clean();

            $prnt_tstamp = date('YmdHis',time());
	        $user = Session::get("employee_id");
            $fname = "rgas";

            $objWriter->save("../app/storage/os/$fname.csv");
            $objPHPExcel->disconnectWorksheets();

             //INSERT AUDIT
	        $parameters["Start_date"] = $start ;
	        $parameters["End_date"] = $end ;
			$this->AU006(json_encode($parameters) , $fname . ".csv" );


            return Response::download("../app/storage/os/$fname.csv", "rgas.csv");
	}

	public function affiliate_food()
	{
            $input = Input::all();
            $val_data = ['date_from' => 'required|date',
                         'date_to' => 'required|date'];
            
            $validator = Validator::make($input , $val_data);
            //RUN LARAVEL VALIDATION    
            if ($validator->fails()) 
            	return Redirect::to( "os/export/affiliate")
		           ->withInput()
		           ->withErrors($validator);

            include('../vendor/phpoffice/phpexcel/Classes/PHPExcel.php');
		
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $start_row = 0;

   
                    
            foreach(range('A','J') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            
            
            $start = date("Y-m-d",strtotime( Input::get("date_from") ));
            $end = date("Y-m-d",strtotime( Input::get("date_to") ));
            
             $requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
						->join("employees" , "ownerid" , "=" , "employees.id")
						->join("companylists" , "comp_code" , "=" , "employees.company")			
						->join("orderdetails" , "orderentryid" , "=" , "orderentries.id")	
						->where("affiliate",1)
						->where("isdeleted",0)
						->where("orderentries.status", "O")
						->where("sales_type" , "FOOD")
						->whereBetween("trans_date",[$start,$end])
						->bukCompany()
						->get(["orderentries.id","orderentries.owner_code","orderentries.deliver_to","orderentries.delivery_add","orderentries.delivery_tel",
							"orderentries.delivery_remarks","orderentries.cust_code","orderentries.cust_name","orderentries.trans_date","orderentries.so_code",
							"orderentries.status","orderentries.remarks","orderentries.create_date","orderentries.create_by","orderentries.modified_date",
							"orderentries.modified_by","orderentries.total_amount","orderentries.paid_amount","orderentries.transmittal","orderentries.ordertypeno",
							"orderentries.submit_date","orderdetails.id as did","orderdetails.prod_code","orderdetails.uom_code","orderdetails.order_qnty","orderdetails.allocated_qnty",
							"orderdetails.served_qnty","orderdetails.price","orderdetails.dtl_seq","orderdetails.modified_date as mod_date","orderdetails.modified_by as mod_by",
							"orderentries.company","orderentries.order_type","orderentries.reference_no"]);
             
              $UPT_ARR= array();
              foreach($requests as $req)
                 {	
                 	$UPT_ARR[] = $req->id;
	                $start_row++;
                    $objPHPExcel->getActiveSheet()->setCellValue("A{$start_row}", $req->id);
                    $objPHPExcel->getActiveSheet()->setCellValue("B{$start_row}", $req->owner_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("C{$start_row}", $req->deliver_to);  
                    $objPHPExcel->getActiveSheet()->setCellValue("D{$start_row}", $req->delivery_add);
                    $objPHPExcel->getActiveSheet()->setCellValue("E{$start_row}", $req->delivery_tel);
                    $objPHPExcel->getActiveSheet()->setCellValue("F{$start_row}", $req->delivery_remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("G{$start_row}", $req->cust_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("H{$start_row}", $req->cust_name);
                    $objPHPExcel->getActiveSheet()->setCellValue("I{$start_row}", $req->trans_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("J{$start_row}", $req->so_code == "CH-A" ? "CH" : $req->so_code);
                    $objPHPExcel->getActiveSheet()->setCellValue("K{$start_row}", $req->status);
                    $objPHPExcel->getActiveSheet()->setCellValue("L{$start_row}", $req->remarks);  
                    $objPHPExcel->getActiveSheet()->setCellValue("M{$start_row}", $req->create_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("N{$start_row}", $req->create_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("O{$start_row}", $req->modified_date);  
                    $objPHPExcel->getActiveSheet()->setCellValue("P{$start_row}", $req->modified_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("Q{$start_row}", $req->total_amount);
                  //  $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->total_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("R{$start_row}", $req->paid_amount);     
                    $objPHPExcel->getActiveSheet()->setCellValue("S{$start_row}", $req->transmittal);     
                    $objPHPExcel->getActiveSheet()->setCellValue("T{$start_row}", $req->ordertypeno);     
                    $objPHPExcel->getActiveSheet()->setCellValue("U{$start_row}", $req->submit_date);     
                    $objPHPExcel->getActiveSheet()->setCellValue("V{$start_row}", $req->did);     
                    $objPHPExcel->getActiveSheet()->setCellValue("W{$start_row}", $req->prod_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("X{$start_row}", $req->uom_code);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Y{$start_row}", $req->order_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("Z{$start_row}", $req->allocated_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AA{$start_row}", $req->served_qnty);     
                    $objPHPExcel->getActiveSheet()->setCellValue("AB{$start_row}", $req->price);
                    $objPHPExcel->getActiveSheet()->setCellValue("AC{$start_row}", $req->dtl_seq);
                    $objPHPExcel->getActiveSheet()->setCellValue("AD{$start_row}", $req->mod_date);
                    $objPHPExcel->getActiveSheet()->setCellValue("AE{$start_row}", $req->mod_by);
                    $objPHPExcel->getActiveSheet()->setCellValue("AF{$start_row}", in_array($req->company, ['RBC-CORP', 'RBC-SAT'] ) ? "RBC"  : $req->company );
                    $objPHPExcel->getActiveSheet()->setCellValue("AG{$start_row}", $req->order_type);
                    $objPHPExcel->getActiveSheet()->setCellValue("AH{$start_row}", $req->reference_no);                    
                 }

                 if(!empty($UPT_ARR))
                 {
                 	OfficeSales::whereIn("id" ,$UPT_ARR)->where("Status","O")->update(["Status"=>"R","delivery_date"=> date("Y-m-d" , strtotime(date("Y-m-d") . "+2 day") ) ]);
                 }

            //WRITE AND DOWNLOAD EXEL FILE
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
            ob_end_clean();

            $prnt_tstamp = date('YmdHis',time());
	        $user = Session::get("employee_id");
            $fname = "rgas";

            $objWriter->save("../app/storage/os/$fname.csv");
            $objPHPExcel->disconnectWorksheets();


             //INSERT AUDIT
	        $parameters["Start_date"] = $start ;
	        $parameters["End_date"] = $end ;
			$this->AU006(json_encode($parameters) , $fname . ".csv" );

            return Response::download("../app/storage/os/$fname.csv", "rgas.csv");
	}


	public function affiliate_print()
	{
            $input = Input::all();
            $val_data = ['start_date' => 'required|date',
            			 'end_date' => 'required|date'];
            
            $validator = Validator::make($input , $val_data);
            //RUN LARAVEL VALIDATION    
            if ($validator->fails()) 
            	return Redirect::to( "os/receiver/report")
		           ->withInput()
		           ->withErrors($validator);

		    $start = Input::get("start_date");
            $end = Input::get("end_date");
            

		    $html = "<table border=1 cellpadding=0 cellspacing=0 style='font-size:10px' width='100%'>
		    			<tr><th colspan='11' align='center'>AFFILIATES' OFFICE SALES ORDER AND PAYMENT SUMMARY FOR THE CUT-OFF $start - $end</th></tr>
		    			<tr>
		    			<th align='center' >REFERENCE NO.</th> 
		    			<th align='center' >DATE FILED</th>
		    			<th align='center' >ORDER TYPE</th> 
		    			<th align='center' >SALES TYPE</th>
		    			<th align='center' >STATUS</th> 
		    			<th align='center' >OFFICE<BR>SALES<BR>AMOUNT</th>
		    			<th align='center' >PAYMENT<BR>TYPE</th> 
		    			<th align='center' >PAID<BR>AMOUNT</th>
		    			<th align='center' >PAYMENT<BR>DATE</th>
		    			<th align='center' >PAYMENT<BR>REF<BR>NO.</th>
		    			<th align='center' >REMARKS</th>
		    			</tr>";

		    $requests = OfficeSales::where("curr_emp" , Session::get("employee_id"))
				->join("employees" , "ownerid" , "=" , "employees.id")
				->Leftjoin("officesale_payments" , "orderentryid" , "=" , "orderentries.id")	
				->join("companylists" , "comp_code" , "=" , "employees.company")	
				->Leftjoin("sotypes" , "sotypes.so_code" , "=" , "orderentries.so_code")				
				->where("affiliate",1)
				->where("isdeleted",0)
				->whereBetween("trans_date", [$start,$end])
				->where("orderentries.status", "R")
				->where("group", 2)
				->orderby("comp_name")
				->get(["reference_no","trans_date","orderentries.status","cust_name","orderentries.id",
					"order_type","so_name","employees.company","orderentries.total_amount",
					"payment_type","payment_amount","payment_date",
					"payment_ref_no","delivery_date","officesale_payments.remarks","comp_name"]);

			$status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
            $company = "";
             foreach($requests as $req) 
             {			
             	if($company != $req->comp_name)
             	{
             		 $html .= "<tr>
				    			<tr><th colspan='11' align='left'>$req->comp_name</th>
			    			</tr>";
			    	$company = $req->comp_name;
             	}

             	 $html .= "<tr>
				    			<td>$req->reference_no</td> 
				    			<td>$req->trans_date</td>
				    			<td>$req->order_type</td> 
				    			<td>$req->so_name</td>
				    			<td>" . $status[$req->status] . "</td> 
				    			<td>$req->total_amount</td>
				    			<td>$req->payment_type</td> 
				    			<td>$req->payment_amount</td>
				    			<td>$req->payment_date</td>
				    			<td>$req->payment_ref_no</td>
				    			<td>$req->remarks</td>
			    			</tr>";
             }

            $html .= "</table>";


		    ob_start();  
	        echo $html;
	        $output = ob_get_clean();

	        $path = 'assets/files/os/';
	        
	        $prnt_tstamp = date('YmdHis',time());
	        $user = Session::get('name');
	        $fname = "rgas";
	        $c = $path . $fname. '.html';
	        $o = $path . $fname. '.pdf';

	        file_put_contents($c ,$output);

	        $fle = "wkhtmltopdf --dpi 125 --disable-external-links --disable-internal-links --orientation portrait --margin-top 12.7mm --margin-right 6.35mm --margin-bottom 12.7mm --margin-left 6.35mm --page-width 215.9mm --page-height 330.2mm \"$c\" \"$o\"";
	        
	         shell_exec($fle);

	         //INSERT AUDIT
	        $parameters["Start_date"] = $start ;
	        $parameters["End_date"] = $end ;
			$this->AU006(json_encode($parameters) , $fname . ".pdf" );

	         return Redirect::to("/$o");
	}


}