<?php
namespace RGAS\Modules\PMARF;

interface IPMARF
{
	public function storeRequest($input);
	public function get_supervisor($nEmpId);
	public function get_department_head($nEmpId);
	public function obj_group($aInputs,$sGroup);
	public function immSupAction($pmarfId);
	public function deptHeadAction($pmarfId);
	public function editRequest($pmarf_id);
	public function npmdHeadAction($pmarf_id);
	public function npmdPerAction($pmarf_id);
	public function npmdHeadAcceptance($pmarf_id);
	public function npmdPerExecute($pmarf_id);
	public function npmdHeadNotate($pmarf_id);
	public function requestorConfirm($pmarf_id);
	
}
?>