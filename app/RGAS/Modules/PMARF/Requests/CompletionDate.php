<?php namespace RGAS\Modules\PMARF\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class CompletionDate extends RResult
{
	public function validate()
	{
		$input = Input::all();
	
		$inputs['completion_date'] = $input['completion_date'];
		$rules['completion_date'] = 'required|date';
		$messages['completion_date.required'] = 'ERROR! COMPLETION DATE FIELD IS REQUIRED!';
		$messages['completion_date.date'] = 'COMPLETION DATE FIELD MUST BE A VALID DATE.';
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url()  )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
	}
}
?>