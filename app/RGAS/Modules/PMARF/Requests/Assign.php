<?php namespace RGAS\Modules\PMARF\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class Assign extends RResult
{
	public function validate()
	{
		// Put all your validation rules here
		// Sample validation code
		$input = Input::all();
		$inputs['assign_to_emp_id'] = $input['assign_to_emp_id'];
		$rules['assign_to_emp_id'] = 'required';
		$messages['assign_to_emp_id.required'] = 'PLEASE SELECT PERSONNEL FOR ASSIGNING';
		
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url()  )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
	}
}
?>