<?php namespace RGAS\Modules\PMARF\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class Cancel extends RResult
{
	public function validate()
	{
		$input = Input::all();
	
		$inputs['comments'] = $input['comments'];
		$rules['comments'] = 'max:5000|required';
		$messages['comments.max'] = 'COMMENT LENGTH MUST NOT BE GREATER 5000 CHARACTERS';
		$messages['comments.required'] = 'ERROR! COMMENT IS REQUIRED!';

		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url()  )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
	}
}
?>