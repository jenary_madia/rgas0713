<?php namespace RGAS\Modules\PMARF\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class Store extends RResult
{
	public function validate()
	{
		// Put all your validation rules here
		// Sample validation code
		$input = Input::all();
		
		$inputs['emp_name'] = $input['emp_name'];
		$rules['emp_name'] = 'required';
		$messages['emp_name.required'] = 'EMPLOYEE NAME IS REQUIRED';
		
		$inputs['emp_id'] = $input['emp_id'];
		$rules['emp_id'] = 'required';
		$messages['emp_id.required'] = 'EMPLOYEE ID IS REQUIRED';
		
		$inputs['pmarf_date_filed'] = $input['pmarf_date_filed'];
		$rules['pmarf_date_filed'] = 'required|date';
		$messages['pmarf_date_filed.required'] = 'DATE FILED IS REQUIRED';
		$messages['pmarf_date_filed.date'] = 'DATE FILED MUST BE A VALID DATE';

		$inputs['emp_company'] = $input['emp_company'];
		$rules['emp_company'] = 'required';
		$messages['emp_company.required'] = 'COMPANY NAME IS REQUIRED';
		
		$inputs['pmarf_status'] = $input['pmarf_status'];
		$rules['pmarf_status'] = 'required';
		$messages['pmarf_status.required'] = 'STATUS IS REQUIRED';
		
		$inputs['emp_department'] = $input['emp_department'];
		$rules['emp_department'] = 'required';
		$messages['emp_department.required'] = 'DEPARTMENT NAME IS REQUIRED';
		
		$inputs['emp_contactno'] = $input['emp_contactno'];
		$rules['emp_contactno'] = 'max:50';
		$messages['emp_contactno.max'] = 'CONTACT NUMBER LENGTH MUST NOT BE GREATER 50 CHARACTERS';

		$inputs['req_activity_type'] = $input['req_activity_type'];
		$rules['req_activity_type'] = 'required';
		$messages['req_activity_type.required'] = 'ACTIVITY TYPE IS REQUIRED';
		
		if($inputs['req_activity_type'] == 'Others'){
			$inputs['req_activity_details'] = $input['req_activity_details'];
			$rules['req_activity_details'] = 'max:1000|required';
			$messages['req_activity_details.max'] = 'ACTIVITY DETAILS LENGTH MUST NOT BE GREATER 1000 CHARACTERS';
			$messages['req_activity_details.required'] = 'ACTIVITY DETAILS IS REQUIIRED';
		}
	
		$inputs['req_activity_date'] = $input['req_activity_date'];
		$rules['req_activity_date'] = 'required';
		$messages['req_activity_date.required'] = 'DATE OF ACTIVITY IS REQUIRED';
		
		if($inputs['req_activity_date'] == 'Multiple days-Staggered'){
			$inputs['multiple_dates'] = $input['multiple_dates'];
			$rules['multiple_dates'] = 'max:1000|required';
			$messages['multiple_dates.max'] = 'DATES MUST NOT BE GREATER THAN 1000 CHARACTERS';
			$messages['multiple_dates.required'] = 'DATES IS REQUIRED';
		}
	
		if($inputs['req_activity_date'] == 'One Day' || $inputs['req_activity_date'] == 'Multiple days-Straight')
		{
				$inputs['req_from'] = $input['req_from'];
				$rules['req_from'] = 'date|required';
				$messages['req_from.date'] = 'FROM FIELD MUST BE A VALID DATE';
				$messages['req_from.required'] = 'FROM FIELD IS REQUIRED';
				
				$inputs['req_to'] = $input['req_to'];
				$rules['req_to'] = 'date|required';
				$messages['req_to.date'] = 'TO FIELD MUST BE A VALID DATE';
				$messages['req_to.required'] = 'TO FIELD IS REQUIRED';

				if($input['req_to']  && $input['req_from'] )
				{
					$inputs['req_from'] = $input['req_from'];
					$rules['req_from'] = 'date|before:'. (date("Y-m-d",strtotime($input['req_to']) +  86400)) ;
					$messages['req_from.before'] = 'THE DATE FROM ACTIVITY MUST BE DATE BEFORE ' . (date("m/d/Y",strtotime($input['req_to']) +  86400));


					$inputs['req_noOfDays'] = $input['req_noOfDays'];
					$rules['req_noOfDays'] = 'integer|min:0|max:' . (((strtotime($input['req_to']) - strtotime($input['req_from'])) / 86400) + 1);
					$messages['req_noOfDays.max'] = 'NO. OF DAYS MUST NOT BE GREATER THAN ' .(((strtotime($input['req_to']) - strtotime($input['req_from'])) / 86400) + 1) . ".";

					$messages['req_noOfDays.min'] = 'NO. OF DAYS MUST NOT BE LESS THAN 0.';
					$messages['req_noOfDays.integer'] = 'NO. OF DAYS MUST BE AN INTEGER.';
				}
		}
	
		
		$inputs['req_area_coverage'] = $input['req_area_coverage'];
		$rules['req_area_coverage'] = 'required|max:100';
		$messages['req_area_coverage.required'] = 'AREA COVERAGE IS REQUIRED';
		$messages['req_area_coverage.max'] = 'AREA COVERAGE LENGTH MUST NOT BE GREATER 100 CHARACTERS';
		
		$inputs['req_implementer'] = $input['req_implementer'];
		$rules['req_implementer'] = 'required';
		$messages['req_implementer.required'] = 'IMPLEMENTER IS REQUIRED';
		
		if($inputs['req_implementer'] == 'External'){
			$inputs['req_agency_name'] = $input['req_agency_name'];
			$rules['req_agency_name'] = 'max:100|required';
			$messages['req_agency_name.max'] = 'AGENCY NAME LENGTH MUST NOT BE GREATER 100 CHARACTERS';
			$messages['req_agency_name.required'] = 'AGENCY NAME IS REQUIRED';
		}

		$initiatedby_others = $input['initiatedby']['initiatedby_others'];
		unset($input['initiatedby']['initiatedby_others']);
		
		if(count($input['initiatedby']) == 0){
			$inputs['initiatedby'] = "";
			$rules['initiatedby'] = 'required';
			$messages['initiatedby.required'] = 'PLEASE SELECT ATLEAST ONE ITEM IN INITIATED BY';
		}
		else{
			if(in_array('Others',$input['initiatedby'])){
				$input['initiatedby_others'] = $initiatedby_others;
				$inputs['initiatedby_others'] = $input['initiatedby_others'];
				$rules['initiatedby_others'] = 'required|max:100';
				$messages['initiatedby_others.required'] = 'OTHERS FIELD IS REQUIRED';
				$messages['initiatedby_others.max'] = 'OTHERS MUST NOT BE GREATER THAN 100 CHARACTERS';
			}
		}
		
		$inputs['specs_proposal'] = $input['specs_proposal'];
		$rules['specs_proposal'] = 'required|max:1000';
		$messages['specs_proposal.required'] = 'PROPOSAL OBJECTIVES IS REQUIRED';
		$messages['specs_proposal.max'] = 'PROPOSAL OBJECTIVES LENGTH MUST NOT BE GREATER 1000 CHARACTERS';
		
		$inputs['specs_proposal_desc'] = $input['specs_proposal_desc'];
		$rules['specs_proposal_desc'] = 'required|max:5000';
		$messages['specs_proposal_desc.required'] = 'BRIEF DESCRIPTION OF PROPOSAL IS REQUIRED';
		$messages['specs_proposal_desc.max'] = 'BRIEF DESCRIPTION OF PROPOSAL LENGTH MUST NOT BE GREATER 5000 CHARACTERS';

		$inputs['specs_additional_support'] = $input['specs_additional_support'];
		$rules['specs_additional_support'] = 'required|max:5000';
		$messages['specs_additional_support.required'] = 'ADDITIONAL SUPPORT ACTIVITIES IS REQUIRED';
		$messages['specs_additional_support.max'] = 'ADDITIONAL SUPPORT ACTIVITIES LENGTH MUST NOT BE GREATER 5000 CHARACTERS';
		
		$rows = count($input['qty_']);
		
		for($row = 0; $row < $rows; $row++)
		{
			if($row == 0)
			{
				$inputs['qty_'.$row] = $input['qty_'][$row];
				$rules['qty_'.$row] = 'required|max:10';
				$messages['qty_'.$row.'.required'] = 'QUANTITY IN ROW '.($row+1).' IS REQUIRED';
				$messages['qty_'.$row.'.max'] = 'QUANTITY '.($row+1).' MUST NOT BE GREATER THAN 10 CHARACTERS';
				
				$inputs['unit_'.$row] = $input['unit_'][$row];
				$rules['unit_'.$row] = 'required|max:10';
				$messages['unit_'.$row.'.required'] = 'UNIT IN ROW '.($row+1).' IS REQUIRED';
				$messages['unit_'.$row.'.max'] = 'UNIT IN ROW '.($row+1).' MUST NOT BE GREATER THAN 10 CHARACTERS';
				
				$inputs['product_'.$row] = $input['product_'][$row];
				$rules['product_'.$row] = 'required|max:1000';
				$messages['product_'.$row.'.required'] = 'PRODUCT NAME IN ROW '.($row+1).' IS REQUIRED';
				$messages['product_'.$row.'.max'] = 'PRODUCT NAME IN ROW '.($row+1).' MUST NOT BE GREATER THAN 1000 CHARACTERS';
			}
			else if($input['qty_'][$row] || $input['unit_'][$row] || $input['product_'][$row])
			{
				$inputs['qty_'.$row] = $input['qty_'][$row];
				$rules['qty_'.$row] = 'required|max:10';
				$messages['qty_'.$row.'.required'] = 'QUANTITY IN ROW '.($row+1).' IS REQUIRED';
				$messages['qty_'.$row.'.max'] = 'QUANTITY '.($row+1).' MUST NOT BE GREATER THAN 10 CHARACTERS';
				
				$inputs['unit_'.$row] = $input['unit_'][$row];
				$rules['unit_'.$row] = 'required|max:10';
				$messages['unit_'.$row.'.required'] = 'UNIT IN ROW '.($row+1).' IS REQUIRED';
				$messages['unit_'.$row.'.max'] = 'UNIT IN ROW '.($row+1).' MUST NOT BE GREATER THAN 10 CHARACTERS';
				
				$inputs['product_'.$row] = $input['product_'][$row];
				$rules['product_'.$row] = 'required|max:1000';
				$messages['product_'.$row.'.required'] = 'PRODUCT NAME IN ROW '.($row+1).' IS REQUIRED';
				$messages['product_'.$row.'.max'] = 'PRODUCT NAME IN ROW '.($row+1).' MUST NOT BE GREATER THAN 1000 CHARACTERS';
			}	
		}
		
		$inputs['comments'] = $input['comments'];
		$rules['comments'] = 'max:5000';
		$messages['comments.max'] = 'COMMENT LENGTH MUST NOT BE GREATER 5000 CHARACTERS';
		
		$file_counter = 0;
		$fs = 0;
		if(@count($input['files']) > 1){
			// $inputs['files_count'] = count($input['files']);
			$file_counter = count($input['files']);
			$fs = 0;
			foreach($input['files'] as $a => $b){
				$fs += $b['filesize'];
			}
		}
		$inputs['files_count'] = $file_counter;
		$rules['files_count'] = 'numeric|max:10';
		$messages['files_count.numeric'] = 'ATTACHMENT COUNT MUST BE NUMERIC';
		$messages['files_count.max'] = 'ATTACHMENT MUST NOT SUCCEED MORE THAN 10';
		
		$inputs['files_size'] = $fs;
		$rules['files_size'] = 'numeric|max:20971520';
		$messages['files_size.numeric'] = 'FILESIZE MUST BE NUMERIC';
		$messages['files_size.max'] = 'ATTACHMENT FILESIZE MUST NOT SUCCEED MORE THAN 20MB IN TOTAL';
		
		
		$totalFiles = @count('current_files[]') + @count($input['files']);
		
		if($totalFiles > 10){
			$inputs['totalFiles'] = $totalFiles;
			$rules['totalFiles'] = 'numeric|max:10';
			$messages['totalFiles.numeric'] = 'ATTACHMENT MUST NOT SUCCEED MORE THAN 10';
			$messages['totalFiles.max'] = 'ATTACHMENT MUST NOT SUCCEED MORE THAN 10';
		}

		Validator::extend('greater_than', function($attribute, $value, $parameters) { 
 			$other = $parameters[0];
			return   ($value) <= ($other);
		});

		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);

		
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url()  )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS');
			exit;
			//$this->setResult(false);
		}
		
		return true;
	}
}
?>