<?php namespace RGAS\Modules\PMARF;

use Session;
use Input;
use Redirect;
use RGAS\Repositories\PMARFRepository;
use RGAS\Libraries;
use ReferenceNumbers;
use Request;
use PromoAndMerchandising;
use Employees;
use RGAS\Libraries\AuditTrail;
use Config;
use Mail;
use RGAS\Helpers\EmployeesHelper;

class Pmarf extends EmailReceivers implements IPmarf
{	
	public $attachments_path = 'pmarf/';
	protected $module_id;
	private $pmarf;
	
	
	private function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'pmarf/';
	}	
	
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_pmarf');
		return $this->module_id;
	}
	
	public function storeViewLogsPMARF($pmarf_id)
	{	
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$pmarf = PromoAndMerchandising::where('pmarf_id','=',$pmarf_id)->first();
		$this->AU003($pmarf->pmarf_id, $pmarf->pmarf_ref_num);
	}
	
	public function downloadLog($pmarf_ref_num)
	{
		$pmarf = new PMARFRepository;
		$request = $pmarf->getRequestByRefNum($pmarf_ref_num);
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$pmarf_id = $request->pmarf_id;
		$pmarf_ref_num = $request->pmarf_ref_num;
		
		foreach(json_decode($request['pmarf_attachments']) as $attachment)
		$new = $attachment->original_filename;
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU008($pmarf_id, $new, $pmarf_ref_num);
	}
	
	public function downloadLog_npmd($pmarf_ref_num)
	{
		$pmarf = new PMARFRepository;
		$request = $pmarf->getRequestByRefNum($pmarf_ref_num);
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$pmarf_id = $request->pmarf_id;
		$pmarf_ref_num = $request->pmarf_ref_num;
		
		foreach(json_decode($request['pmarf_npmd_attachments']) as $attachment)
		$new = $attachment->original_filename;
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU008($pmarf_id, $new, $pmarf_ref_num);
	}
	
	public function storeRequest($input)
	{
		$pmarf = new PromoAndMerchandising;
		$input = Input::all();
		
		if($input['action'] == 'save'){
			$status = "New";
			$assigned_to = "";
			$stage = "REQUESTOR";
			$issent = 0;
		}
		
		if($input['action'] == 'send'){
			$reqSup = Employees::where('id','=',$input['emp_id'])->first();
			$pmarfRep = new PMARFRepository;
			$npmdHead = $pmarfRep->getNpmdHead();
			$issent = 1;
			
			if(Session::get('desig_level') == 'head'){
				$assigned_to = $npmdHead->id;
				$stage = 'NPMD-HEAD';
				$status = 'For Assessment';
			}
			else{
				$assigned_to = $reqSup->superiorid;
				$stage = "IMM-SUP";
				$status = "For Approval";				
			}
		}
		
		$aParticipatingBranch['qty'] = $this->obj_group($input,'qty_');
		$aParticipatingBranch['unit'] = $this->obj_group($input,'unit_');
		$aParticipatingBranch['product'] = $this->obj_group($input,'product_');
	    // $aExistngpmarf_attachments = json_decode($this->obj_group($input,'existing_attachment'),'array');
        $oSupervisor = json_encode($this->get_supervisor(Input::get('emp_id')));
	    $oDeptDead = json_encode($this->get_department_head(Input::get('emp_id')));
				
		//$pmarf->pmarf_ref_num  = ReferenceNumbers::get_current_reference_number('pmarf');
		$pmarf->pmarf_ref_num  = ReferenceNumbers::get_current_reference_number_pmarf();
		$pmarf->pmarf_date_filed = date('Y-m-d');
		$pmarf->pmarf_status  = $status;
		$pmarf->pmarf_issent  = $issent;
		$pmarf->pmarf_emp_id = Input::get('emp_id');
		$pmarf->pmarf_employees_id = Input::get('employees_id');
		$pmarf->pmarf_emp_name = Input::get('emp_name');
		$pmarf->pmarf_company = Input::get('emp_company');
		$pmarf->pmarf_department = Input::get('emp_department');
		$pmarf->pmarf_section = Input::get('emp_section');
		$pmarf->pmarf_current = $assigned_to;
		$pmarf->pmarf_level = $stage;
		$pmarf->pmarf_contact_no = Input::get('emp_contactno');
		$pmarf->pmarf_activity_type = Input::get('req_activity_type');
		$pmarf->pmarf_activity_details = Input::get('req_activity_details');
		$pmarf->pmarf_activity_dates = Input::get('req_activity_date');
		$pmarf->pmarf_activity_from = Input::get('req_from');
		$pmarf->pmarf_activity_to = Input::get('req_to');
		$pmarf->pmarf_activity_no_of_days = Input::get('req_noOfDays');
		$pmarf->pmarf_multiple_dates = Input::get('multiple_dates');
		$pmarf->pmarf_activity_area = Input::get('req_area_coverage');
		$pmarf->pmarf_implementer = Input::get('req_implementer');
		$pmarf->pmarf_agency_name = Input::get('req_agency_name');
		$pmarf->pmarf_initiated_by = json_encode(Input::get('initiatedby'));
		$pmarf->pmarf_proposal_objective = Input::get('specs_proposal');	
		$pmarf->pmarf_proposal_description = Input::get('specs_proposal_desc');
		$pmarf->pmarf_add_support_act = Input::get('specs_additional_support');
		$pmarf->pmarf_participating_brands = json_encode($aParticipatingBranch);
		$pmarf->pmarf_attachments = @json_encode($input['files']);
		$pmarf->pmarf_comments = json_encode(
			array(
				array(
					'name'=>Session::get('employee_name'),
					'datetime'=>date('m/d/Y g:i A'),
					'message'=>trim($input['comments'])
				)
			)
		);
		$pmarf->pmarf_created_date = date('Y-m-d');
		$pmarf->pmarf_created_by = Input::get('emp_id');
		$pmarf->pmarf_supervisor_info = $oSupervisor;
		$pmarf->pmarf_depthead_info = $oDeptDead;
		
		try{
			$pmarf->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('pmarf');
			$pmarf->pmarf_ref_num = ReferenceNumbers::get_current_reference_number('pmarf');
			$pmarf->save();
		}
		unset($aParticipatingBranch);
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU001($pmarf->pmarf_id, json_encode($pmarf['attributes']), $pmarf->pmarf_ref_num);
		
		if(@count($input['files']) > 0){
			foreach($input['files'] as $file){
				$this->setTable('promoandmerchandising');
				$this->setPrimaryKey('pmarf_id');
				$this->AU002($pmarf->pmarf_id, json_encode($file), $pmarf->pmarf_ref_num);
			}	
		}
		
		if($issent == 1){
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarf->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarf->pmarf_ref_num);
					}
				}
			}
			
			if(Session::get('desig_level') == 'head'){
				Mail::send('pmarf.email_templates.pmarf_for_assessment', array(
					'ref_num' => $pmarf->pmarf_ref_num,
					'requestor' => $pmarf->pmarf_emp_name,
					'department' => $pmarf->pmarf_department,
					'activity_type' => $pmarf->pmarf_activity_type),
					function($message){
						$message->to($this->getNpmdDeptHead()->email, $this->getNpmdDeptHead()->firstname." ".$this->getNpmdDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Assessment');
					}
				);
			}
			else{
				Mail::send('pmarf.email_templates.pmarf_for_approval', array(
					'ref_num' => $pmarf->pmarf_ref_num,
					'requestor' => $pmarf->pmarf_emp_name,
					'department' => $pmarf->pmarf_department,
					'activity_type' => $pmarf->pmarf_activity_type),
					function($message){
						$message->to($this->getImmSup()->email, $this->getImmSup()->firstname." ".$this->getImmSup()->lastname)->subject('RGAS Notification: PMARF Request for Approval');
					}
				);
				
			}
		}		
		return true;
	}
	
	public function editRequest($pmarf_id)
	{
		$input = Input::all();		
		$pmarf = PromoAndMerchandising::find($pmarf_id);
		$pmarfRep = new PMARFRepository;
		$npmdHead = $pmarfRep->getNpmdHead();
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		if($input['action'] == 'update'){
			if($pmarf['pmarf_status'] == 'New'){
				$status = "New";	
				$assigned_to = "";
				$stage = "REQUESTOR";
				$issent = 0;
			}
			
			elseif($pmarf['pmarf_status'] == 'Returned' || $pmarf['pmarf_status'] == 'Disapproved'){
				$status = $pmarf->pmarf_status;	
				$assigned_to = $pmarf->pmarf_current;
				$stage = $pmarf->pmarf_level;
				$issent = 0;
			}
		}
		
		elseif($input['action'] == 'send'){
		
			$reqSup = Employees::where('id','=',$input['emp_id'])->first();
			$issent = 1;

			if(Session::get('desig_level') == 'head'){
				$assigned_to = $npmdHead->id;
				$stage = 'NPMD-HEAD';
				$status = 'For Assessment';
				
				Mail::send('pmarf.email_templates.pmarf_for_assessment', array(
					'ref_num' => $pmarf->pmarf_ref_num,
					'requestor' => $pmarf->pmarf_emp_name,
					'department' => $pmarf->pmarf_department,
					'activity_type' => $pmarf->pmarf_activity_type),
					function($message){
						$message->to($this->getNpmdDeptHead()->email, $this->getNpmdDeptHead()->firstname." ".$this->getNpmdDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Assessment');
					}
				);
			
			}
			
			else{
				$assigned_to = $reqSup->superiorid;
				$stage = "IMM-SUP";
				$status = "For Approval";
				
				Mail::send('pmarf.email_templates.pmarf_for_approval', array(
					'ref_num' => $pmarf->pmarf_ref_num,
					'requestor' => $pmarf->pmarf_emp_name,
					'department' => $pmarf->pmarf_department,
					'activity_type' => $pmarf->pmarf_activity_type),
					function($message){
						$message->to($this->getImmSup()->email, $this->getImmSup()->firstname." ".$this->getImmSup()->lastname)->subject('RGAS Notification: PMARF Request for Approval');
					}
				);
			
			}	
		
		}
		
		if($pmarf['pmarf_status'] == 'New'){
			$pmarf->pmarf_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>trim($input['comments'])
					)
				)
			);
		}
		
		elseif($pmarf['pmarf_status'] == 'Returned' || $pmarf['pmarf_status'] == 'Disapproved'){
			$comments = (array) json_decode($pmarf->pmarf_comments);
			if($input['action'] == 'update'){
				$comments[] = array(
					'name'=>Session::get('employee_name'),
					'datetime'=>date('m/d/Y g:i A'),
					'message'=>trim(Input::get('comments')),
					'status'=>'draft'
				);
			}
			else{
				$comments[] = array(
					'name'=>Session::get('employee_name'),
					'datetime'=>date('m/d/Y g:i A'),
					'message'=>trim(Input::get('comments'))
				);
			}
				
			$pmarf->pmarf_comments = json_encode($comments);
		}
			
		$aParticipatingBranch['qty'] = $this->obj_group($input,'qty_');
		$aParticipatingBranch['unit'] = $this->obj_group($input,'unit_');
		$aParticipatingBranch['product'] = $this->obj_group($input,'product_');
	    // $aExistngpmarf_attachments = json_decode($this->obj_group($input,'existing_attachment'),'array');
        $oSupervisor = json_encode($this->get_supervisor(Input::get('emp_id')));
	    $oDeptDead = json_encode($this->get_department_head(Input::get('emp_id')));
		
		$pmarf->pmarf_ref_num  = Input::get('pmarf_refno');
		$pmarf->pmarf_date_filed = date('Y-m-d');
		$pmarf->pmarf_status  = $status;
		$pmarf->pmarf_issent  = $issent;
		$pmarf->pmarf_emp_id = Input::get('emp_id');
		$pmarf->pmarf_employees_id = Input::get('employees_id');
		$pmarf->pmarf_emp_name = Input::get('emp_name');
		$pmarf->pmarf_company = Input::get('emp_company');
		$pmarf->pmarf_department = Input::get('emp_department');
		$pmarf->pmarf_section = Input::get('emp_section');
		$pmarf->pmarf_current = $assigned_to;
		$pmarf->pmarf_level = $stage;
		$pmarf->pmarf_contact_no = Input::get('emp_contactno');
		$pmarf->pmarf_activity_type = Input::get('req_activity_type');
		$pmarf->pmarf_activity_details = Input::get('req_activity_details');
		$pmarf->pmarf_activity_dates = Input::get('req_activity_date');
		$pmarf->pmarf_activity_from = Input::get('req_from');
		$pmarf->pmarf_activity_to = Input::get('req_to');
		$pmarf->pmarf_activity_no_of_days = Input::get('req_noOfDays');
		$pmarf->pmarf_multiple_dates = Input::get('multiple_dates');
		$pmarf->pmarf_activity_area = Input::get('req_area_coverage');
		$pmarf->pmarf_implementer = Input::get('req_implementer');
		$pmarf->pmarf_agency_name = Input::get('req_agency_name');
		$pmarf->pmarf_initiated_by = json_encode(Input::get('initiatedby'));
		$pmarf->pmarf_proposal_objective = Input::get('specs_proposal');	
		$pmarf->pmarf_proposal_description = Input::get('specs_proposal_desc');
		$pmarf->pmarf_add_support_act = Input::get('specs_additional_support');
		$pmarf->pmarf_participating_brands = json_encode($aParticipatingBranch);
		$pmarf->pmarf_attachments = @json_encode($input['files']);
		
		$pmarf->pmarf_created_date = date('Y-m-d');
		$pmarf->pmarf_created_by = Input::get('emp_id');
		$pmarf->pmarf_modified_date =  date('Y-m-d');
		$pmarf->pmarf_modified_by = Session::get('employee_id');
		
		try{
			$pmarf->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('pmarf');
			$pmarf->pmarf_ref_num = ReferenceNumbers::get_current_reference_number('pmarf');
			$pmarf->save();
		}
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU001($pmarf->pmarf_id, json_encode($pmarf['attributes']), $pmarf->pmarf_ref_num);
		
		if($issent == 1){
			if(isset($input['files']) && @count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarf->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarf->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarf->pmarf_id, json_encode($file), $pmarf->pmarf_ref_num);
				}
			}
		}
	}
		
	public function immSupAction($pmarf_id)
	{
	
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getRequest($pmarf_id);
		$input = Input::all();
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
        $supInfo  = trim($requests->pmarf_supervisor_info) != '' ? json_decode($requests->pmarf_supervisor_info) : false;
        $supId    = $supInfo != false ? $supInfo->id : 0;
        $deptInfo = trim($requests->pmarf_depthead_info) != '' ? json_decode($requests->pmarf_depthead_info) : false;
        $deptId    = $deptInfo != false ? $deptInfo->id : 0;
		
		$aParticipatingBranch['qty'] = $this->obj_group($input,'qty_');
		$aParticipatingBranch['unit'] = $this->obj_group($input,'unit_');
		$aParticipatingBranch['product'] = $this->obj_group($input,'product_');
	    // $aExistngpmarf_attachments = json_decode($this->obj_group($input,'existing_attachment'),'array');
        $oSupervisor = json_encode($this->get_supervisor(Input::get('emp_id')));
	    $oDeptDead = json_encode($this->get_department_head(Input::get('emp_id')));
		
		$old = '"'.$requests->pmarf_level.'","'.$requests->pmarf_status.'","'.$requests->pmarf_current.'","'.$requests->pmarf_issent.'"';

		if(Input::get('action') == 'send'){
			$requests->pmarf_level        	= 'DH';
			$requests->pmarf_status           = 'For Approval'; 
			$requests->pmarf_current  = $deptId;
			$requests->pmarf_issent = 1;
			
			Mail::send('pmarf.email_templates.pmarf_for_approval', array(
				'ref_num' => $requests->pmarf_ref_num,
				'requestor' => $requests->pmarf_emp_name,
				'department' => $requests->pmarf_department,
				'activity_type' => $requests->pmarf_activity_type),
				function($message){
					$message->to($this->getDeptHead()->email, $this->getDeptHead()->firstname." ".$this->getDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Approval');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$requests->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$requests->pmarf_ref_num);
						
					}
				}
			
			}
			$requests->pmarf_attachments = @json_encode($input['files']);
		}
			
		if(Input::get('action') == 'return')
		{

			//JMA ADDITIONAL FILES ON RETURN
			// if(isset($input['current_files']) && is_array($input['current_files']))
			// {
			// 	foreach($input['current_files'] as $old_files)
			// 	{
			// 		$input['files'][] = json_decode($old_files, true);
			// 	}
			// }

			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$requests->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$requests->pmarf_ref_num);
						
					}
				}
			
			}
			$requests->pmarf_attachments = @json_encode($input['files']);

			$requests->pmarf_level        = 'REQUESTOR-DISAPPROVED';
			$requests->pmarf_status           = 'Disapproved'; 
			$requests->pmarf_current  = $requests->pmarf_emp_id;
			$requests->pmarf_issent = 0;
			
			Mail::send('pmarf.email_templates.pmarf_disapproved', array(
				
				'ref_num' => $requests->pmarf_ref_num,
				'date_filed' => $requests->pmarf_date_filed),
				
				function($message){
			
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();					
					
					$emp = Employees::where("id","=",$ass->pmarf_emp_id)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Disapproved Request');
				}
			);

		}
		
		$comments = (array) json_decode($requests->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$requests->pmarf_comments = json_encode($comments);

		
		try{
			$requests->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('pmarf');
			$requests->pmarf_ref_num = ReferenceNumbers::get_current_reference_number('pmarf');
			$requests->save();
		}
		
		$requests->save();
		
		$new = '"'.$requests->pmarf_level.'","'.$requests->pmarf_status.'","'.$requests->pmarf_current.'","'.$requests->pmarf_issent.'"';
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU004($requests->pmarf_id, $old, $new, $requests->pmarf_ref_num);

	}
	
	public function deptHeadAction($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$requests = $pmarf->getRequest($pmarf_id);
		$npmdHead = $pmarf->getNpmdHead();
		
		$input = Input::all();
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$aParticipatingBranch['qty'] = $this->obj_group($input,'qty_');
		$aParticipatingBranch['unit'] = $this->obj_group($input,'unit_');
		$aParticipatingBranch['product'] = $this->obj_group($input,'product_');
		
		$old = '"'.$requests->pmarf_level.'","'.$requests->pmarf_status.'","'.$requests->pmarf_current.'","'.$requests->pmarf_issent.'"';
		
		if(Input::get('action') == 'send'){

			$requests->pmarf_issent = 1;
			$requests->pmarf_level        = 'NPMD-HEAD';
			$requests->pmarf_status           = 'For Assessment'; 
			$requests->pmarf_current = $npmdHead->id;
			$requests->pmarf_modified_date   = date('Y-m-d');
			$requests->pmarf_modified_by     = Session::get('employee_id');
			
			Mail::send('pmarf.email_templates.pmarf_for_assessment', array(
				'ref_num' => $requests->pmarf_ref_num,
				'requestor' => $requests->pmarf_emp_name,
				'department' => $requests->pmarf_department,
				'activity_type' => $requests->pmarf_activity_type),
				function($message){
					$message->to($this->getNpmdDeptHead()->email, $this->getNpmdDeptHead()->firstname." ".$this->getNpmdDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Assessment');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$requests->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$requests->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($requests->pmarf_id, json_encode($file), $requests->pmarf_ref_num);
				}
			}
			$requests->pmarf_attachments = @json_encode($input['files']);
		}
		
		if(Input::get('action') == 'return'){
			$requests->pmarf_level        = 'REQUESTOR-DISAPPROVED';
			$requests->pmarf_status           = 'Disapproved'; 
			$requests->pmarf_current  = $requests->pmarf_emp_id;
			$requests->pmarf_modified_date   = date('Y-m-d');
			$requests->pmarf_modified_by     = Session::get('employee_id');
			$requests->pmarf_issent = 0;
			
			Mail::send('pmarf.email_templates.pmarf_disapproved', array(
				'ref_num' => $requests->pmarf_ref_num,
				'date_filed' => $requests->pmarf_date_filed),
				
				function($message){
			
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();					
					
					$emp = Employees::where("id","=",$ass->pmarf_emp_id)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Disapproved Request');
				}
			);
			
		}
		$comments = (array) json_decode($requests->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>Input::get('comments')
		);
		$requests->pmarf_comments = json_encode($comments);
		$requests->pmarf_modified_date   = date('Y-m-d');
		$requests->pmarf_modified_by     = Session::get('employee_id');
		
		try{
			$requests->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('pmarf');
			$requests->pmarf_ref_num = ReferenceNumbers::get_current_reference_number('pmarf');
			$requests->save();
		}
		
		
		$requests->save();
		
		$new = '"'.$requests->pmarf_level.'","'.$requests->pmarf_status.'","'.$requests->pmarf_current.'","'.$requests->pmarf_issent.'"';
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU004($requests->pmarf_id, $old, $new, $requests->pmarf_ref_num);
		
	}
	
	public function npmdHeadAction($pmarf_id)
	{
		$input = Input::all();
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		
		$old = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';
		
		if(Input::get('action') == 'assign')
		{
			$pmarfRes->pmarf_status = "For Assessment";
			$pmarfRes->pmarf_issent = 1;
			$pmarfRes->pmarf_current = Input::get('assign_to_emp_id');
			$pmarfRes->pmarf_npmd_personnel = Input::get('assign_to_emp_id');
			$pmarfRes->pmarf_level = "NPMD-PERSONNEL-ASSIGN";
			
			Mail::send('pmarf.email_templates.pmarf_for_processing', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'requestor' => $pmarfRes->pmarf_emp_name,
				'department' => $pmarfRes->pmarf_department,
				'activity_type' => $pmarfRes->pmarf_activity_type),
				function($message){
					$emp = Employees::where('id','=',Input::get('assign_to_emp_id'))->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Request for Processing');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
				
				foreach($input['files'] as $file_key => $file_values){
					$file_values['uploader'] = Session::get('employee_id');
					$new_files[] = $file_values;
				}
			}
	
			$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);

			
		}
		
		if(Input::get('action') == 'return')
		{
			$pmarfRes->pmarf_status = "Returned";
			$pmarfRes->pmarf_issent = 0;
			$pmarfRes->pmarf_current = Input::get('return_to_emp_id');
			$pmarfRes->pmarf_level = "NPMD-RETURNED";
			
			Mail::send('pmarf.email_templates.pmarf_returned', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed),
				
				function($message){
								
					$emp = Employees::where("id","=",Input::get('return_to_emp_id'))->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Returned Request');
				}
			);

			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
				foreach($input['files'] as $file_key => $file_values){
					if(!isset($file_values['uploader']) && Session::get('employee_id') != @$file_values['uploader']){
						$file_values['uploader'] = Session::get('employee_id');
					}
					$new_files[] = $file_values;
				}
			}

			$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);
		}
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		
		$pmarfRes->save();
		
		$new = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';
	
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU004($pmarfRes->pmarf_id, $old, $new, $pmarfRes->pmarf_ref_num);
		
		
	}
	
	public function npmdPerAction($pmarf_id)
	{
		$input = Input::all();
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		$npmdHead = $pmarf->getNpmdHead();
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		
		$old = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';

		
		if(Input::get('action') == 'send')
		{
			$pmarfRes->pmarf_current = $npmdHead->id;
			$pmarfRes->pmarf_level = "NPMD-HEAD-ACCEPT";
			$pmarfRes->pmarf_status = "For Assessment";
			$pmarfRes->pmarf_issent = 1;
			
			Mail::send('pmarf.email_templates.pmarf_for_processing', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'requestor' => $pmarfRes->pmarf_emp_name,
				'department' => $pmarfRes->pmarf_department,
				'activity_type' => $pmarfRes->pmarf_activity_type),
				function($message){
					$message->to($this->getNpmdDeptHead()->email, $this->getNpmdDeptHead()->firstname." ".$this->getNpmdDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Processing');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
				foreach($input['files'] as $file_key => $file_values){
					if(!isset($file_values['uploader']) && Session::get('employee_id') != @$file_values['uploader']){
						$file_values['uploader'] = Session::get('employee_id');
					}
					$new_files[] = $file_values;
				}
			}

			$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);
		}
		
		if(Input::get('action') == 'return')
		{
			$pmarfRes->pmarf_status = "Returned";		
			$pmarfRes->pmarf_level = 'NPMD-RETURNED';
			$pmarfRes->pmarf_current  = $pmarfRes->pmarf_emp_id;
			$pmarfRes->pmarf_issent = 0;
			
			Mail::send('pmarf.email_templates.pmarf_returned', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed),
				
				function($message){
			
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();					
					
					$emp = Employees::where("id","=",$ass->pmarf_emp_id)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Returned Request');
				}
			);


			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
				foreach($input['files'] as $file_key => $file_values){
					if(!isset($file_values['uploader']) && Session::get('employee_id') != @$file_values['uploader']){
						$file_values['uploader'] = Session::get('employee_id');
					}
					$new_files[] = $file_values;
				}
			}

			$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);

			
		}
		$pmarfRes->save();
		
		$new = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';
	
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU004($pmarfRes->pmarf_id, $old, $new, $pmarfRes->pmarf_ref_num);
	}
	
	public function npmdHeadAcceptance($pmarf_id) // null attachments
	{	
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		
		
		$input = Input::all();
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		
		$old = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';
		
		if(Input::get('action') == 'accept')
		{
			$pmarfRes->pmarf_current = $pmarfRes->pmarf_npmd_personnel;
			$pmarfRes->pmarf_level = "NPMD-PERSONNEL-EXECUTE";
			$pmarfRes->pmarf_status = "For Execution";

			
			Mail::send('pmarf.email_templates.pmarf_accepted', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed,
				'activity_type' => $pmarfRes->pmarf_activity_type),
				
				function($message){
			
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();
					
					$emp = Employees::where("id","=",$ass->pmarf_npmd_personnel)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: Accepted PMARF');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
				foreach($input['files'] as $file_key => $file_values){
					if(!isset($file_values['uploader']) && Session::get('employee_id') != @$file_values['uploader']){
						$file_values['uploader'] = Session::get('employee_id');
					}
					$new_files[] = $file_values;
				}
			}

			$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);
		}
		
		if(Input::get('action') == 'return')
		{
			$pmarfRes->pmarf_current = Input::get('return_to_emp_id');
			$pmarfRes->pmarf_level = "NPMD-RETURNED";
			$pmarfRes->pmarf_status = "Returned";
			
			Mail::send('pmarf.email_templates.pmarf_returned', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed),
				
				function($message){
								
					$emp = Employees::where("id","=",Input::get('return_to_emp_id'))->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Request Returned');
				}
			);
			
		}
		$pmarfRes->save();
		
		$new = '"'.$pmarfRes->pmarf_level.'","'.$pmarfRes->pmarf_status.'","'.$pmarfRes->pmarf_current.'","'.$pmarfRes->pmarf_issent.'"';
	
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU004($pmarfRes->pmarf_id, $old, $new, $pmarfRes->pmarf_ref_num);
	}
	
	public function npmdPerExecute($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		$npmdHead = $pmarf->getNpmdHead($pmarf_id);
		
		$input = Input::all();
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		$pmarfRes->pmarf_current = $npmdHead->id;
		$pmarfRes->pmarf_level = "NPMD-HEAD-NOTATION";
		$pmarfRes->pmarf_status = "For Execution";	
		$pmarfRes->pmarf_completion_date = Input::get('completion_date');
		
		if(isset($input['files']) && count($input['files'] > 0)){
			$fm = new Libraries\FileManager;	
			foreach($input['files'] as $file){
				if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
					$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
				}
				$this->setTable('promoandmerchandising');
				$this->setPrimaryKey('pmarf_id');
				$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
			}
			foreach($input['files'] as $file_key => $file_values){
				if(!isset($file_values['uploader']) && Session::get('employee_id') != @$file_values['uploader']){
					$file_values['uploader'] = Session::get('employee_id');
				}
				$new_files[] = $file_values;
			}
		}

		$pmarfRes->pmarf_npmd_attachments = @json_encode((object)$new_files);

		$pmarfRes->save();
		
		Mail::send('pmarf.email_templates.pmarf_for_processing', array(
		'ref_num' => $pmarfRes->pmarf_ref_num,
		'requestor' => $pmarfRes->pmarf_emp_name,
		'department' => $pmarfRes->pmarf_department,
		'activity_type' => $pmarfRes->pmarf_activity_type),
			function($message){
				$message->to($this->getNpmdDeptHead()->email, $this->getNpmdDeptHead()->firstname." ".$this->getNpmdDeptHead()->lastname)->subject('RGAS Notification: PMARF Request for Notation');
			}
		);

	}
	
	public function npmdHeadNotate($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		
		$input = Input::all();
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		
		if(Input::get('action') == 'send'){
			$pmarfRes->pmarf_current = $pmarfRes->pmarf_emp_id;
			$pmarfRes->pmarf_level = "REQUESTOR-CONFORME";
			$pmarfRes->pmarf_status = "For Conforme";
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$pmarfRes->pmarf_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$pmarfRes->pmarf_ref_num);
					}
					$this->setTable('promoandmerchandising');
					$this->setPrimaryKey('pmarf_id');
					$this->AU002($pmarfRes->pmarf_id, json_encode($file), $pmarfRes->pmarf_ref_num);
				}
			}
			$pmarfRes->pmarf_npmd_attachments = @json_encode($input['files']);
			
			Mail::send('pmarf.email_templates.pmarf_for_acknowledgement', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed),
				
				function($message){
			
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();					
					
					$emp = Employees::where("id","=",$ass->pmarf_emp_id)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Request for Acknowledgement');
				}
			);			
		}
		
		if(Input::get('action') == 'return'){
			$pmarfRes->pmarf_current = $pmarfRes->pmarf_npmd_personnel;
			$pmarfRes->pmarf_level = "NPMD-PERSONNEL-RETURNED";
			$pmarfRes->pmarf_status = "Returned";
			
			Mail::send('pmarf.email_templates.pmarf_returned', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'date_filed' => $pmarfRes->pmarf_date_filed),
				
				function($message){
					$ass = PromoAndMerchandising::where("pmarf_id","=",Input::get('pmarf_id'))->first();					
					$emp = Employees::where("id","=",$ass->pmarf_npmd_personnel)->first();
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGAS Notification: PMARF Request for Acknowledgement');
				}
			);
		}
		$pmarfRes->save();
	}
	
	public function requestorConfirm($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		$pmarfRes->pmarf_current = $pmarfRes->pmarf_emp_id;
		$pmarfRes->pmarf_level = "REQUESTOR-ACKNOWLEDGED";
		$pmarfRes->pmarf_status = "Closed";	
		$pmarfRes->save();
	}
	
	public function cancelRequest($pmarf_id)
	{
		$pmarf = new PMARFRepository;
		$pmarfRes = $pmarf->getRequest($pmarf_id);	
		
		$comments = (array) json_decode($pmarfRes->pmarf_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>trim(Input::get('comments'))
		);
		$pmarfRes->pmarf_comments = json_encode($comments);
		
		//jma
		$current = $pmarfRes->pmarf_current;

		$pmarfRes->pmarf_status = 'Cancelled';
		$pmarfRes->pmarf_level = 'REQUESTOR-CANCELLED';
		$pmarfRes->pmarf_current = $pmarfRes['pmarf_emp_id'];
		$pmarfRes->pmarf_modified_by = Session::get('employee_id');
		$pmarfRes->pmarf_modified_date = date('Y-m-d');
		$pmarfRes->save();
		

		Mail::send('pmarf.email_templates.pmarf_cancelled', array(
			'ref_num' => $pmarfRes->pmarf_ref_num,
			'requestor' => $pmarfRes->pmarf_emp_name,
			'date_filed' => $pmarfRes->pmarf_date_filed,
			'activity_type' => $pmarfRes->pmarf_activity_type),
			function($message){
				$message->to($this->getImmSup()->email, $this->getImmSup()->firstname." ".$this->getImmSup()->lastname)->subject('RGAS Notification: PMARF Request Cancelled');
			}
		);
		


		if($current == $this->getDeptHead()->id)
		{
			Mail::send('pmarf.email_templates.pmarf_cancelled', array(
				'ref_num' => $pmarfRes->pmarf_ref_num,
				'requestor' => $pmarfRes->pmarf_emp_name,
				'date_filed' => $pmarfRes->pmarf_date_filed,
				'activity_type' => $pmarfRes->pmarf_activity_type),
				function($message){
					$message->to($this->getDeptHead()->email, $this->getDeptHead()->firstname." ".$this->getDeptHead()->lastname)->subject('RGAS Notification: PMARF Request Cancelled');
				}
			);
		}
		
			
	}
	
	
	public function get_supervisor($nEmpId){
        $oEmp  = Employees::get($nEmpId);
        $oSup  = Employees::get($oEmp->superiorid);
        
        if(count($oSup)>0)
        {
           return array("id"=>$oSup->id,
                        "firstname"=>$oSup->firstname,
                        "middlename"=>$oSup->middlename,
                        "lastname"=>$oSup->lastname,
                        "designation"=>$oSup->designation);   
        }
		else{
           return false; 
        }
    }
	
	public function get_department_head($nEmpId)
	{
		$oEmp       = Employees::get($nEmpId);
		$oDepthead  = Employees::get_department_head($oEmp->departmentid);

		if(count($oDepthead)>0)
		{
		   return array("id"=>$oDepthead->id,
						"firstname"=>$oDepthead->firstname,
						"middlename"=>$oDepthead->middlename,
						"lastname"=>$oDepthead->lastname,
						"designation"=>$oDepthead->designation);   
		}else{
		   return false; 
		}
    }
	
	public function obj_group($aInputs,$sGroup){ 

		if(is_array($aInputs) && $sGroup !== ''){
			if(isset($aInputs[$sGroup]))
			{
				$group = array();
				foreach($aInputs[$sGroup] as $grpKey => $grp)
				{
					// if($grp == '' || !$grp) break;
					if($grp)
					{
						$grpKey = $grpKey + 1;
						$group[$sGroup.$grpKey] = $grp;
					}
					
				}
				return json_encode($group);
			}			
		}

    }
	
	public function deleteRequest($pmarf_id)
	{	
		$pmarf = PromoAndMerchandising::where('pmarf_id','=',$pmarf_id)->first();
		$old_status = $pmarf->pmarf_status;
		
		$pmarf->pmarf_status = $pmarf->pmarf_status."/Deleted";
		$pmarf->pmarf_isdeleted = 1;
		$pmarf->pmarf_deleted_by = Session::get('employee_id');
		$pmarf->pmarf_date_deleted = date('Y-m-d');
		$pmarf->save();
		
		$new_status = $pmarf->pmarf_status;
		
		$this->setTable('promoandmerchandising');
		$this->setPrimaryKey('pmarf_id');
		$this->AU005($pmarf->pmarf_id, $old_status, $new_status, $pmarf->pmarf_ref_num);
	}


}
?>