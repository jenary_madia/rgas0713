<?php namespace RGAS\Modules\PMARF;

use AuditTrail;
use Session;

class PMARFLogs {
	private $table;
	private $primaryKey;

	public function setTable($table)
	{
		$this->table = $table;
	}
	
	public function setPrimaryKey($primaryKey)
	{
		$this->primaryKey = $primaryKey;
	}
	
	//Create
	protected function AU001($pmarf_id, $new, $pmarf_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU001",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			"",
			$new,
			$pmarf_ref_num
		);
	}
	
	//Upload File
	protected function AU002($pmarf_id, $new, $pmarf_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU002",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			"",
			$new,
			$pmarf_ref_num
		);
	}
	
	
	//Retrieve
	protected function AU003($pmarf_id, $pmarf_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU003",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			"",
			"",
			$pmarf_ref_num
		);
	}
	
	//Edit
	protected function AU004($pmarf_id, $old, $new, $pmarf_ref_num)
	{
		AuditTrail::store(		
			Session::get('employee_id'),
			"AU004",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			$old,
			$new,
			$pmarf_ref_num
		);
	}
	
	//Delete 
	protected function AU005($pmarf_id, $old_status, $new_status, $pmarf_ref_num)
	{		
		AuditTrail::store(
			Session::get('employee_id'),
			"AU005",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			$old_status,
			$new_status,
			$pmarf_ref_num
		);
	}
	
	//Print
	protected function AU006($pmarf_id, $pmarf_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU006",
			$this->getModuleId(),
			"",
			json_encode(array("{$this->primaryKey}"=>$pmarf_id)),
			"",
			"",
			$pmarf_ref_num
		);
	}

	//Download 
	protected function AU008($pmarf_id, $new, $pmarf_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU008",
			$this->getModuleId(),
			"",
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$pmarf_id)),
			"",
			$new,
			$pmarf_ref_num
		);
	}

}
?>



