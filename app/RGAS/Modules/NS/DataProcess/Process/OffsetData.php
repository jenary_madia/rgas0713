<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 11:34 AM
 */

namespace RGAS\Modules\NS\DataProcess\Process;


use RGAS\Modules\NS\DataProcess\Contracts\DataParseRepository;
use Notifications;
use Session;
use Redirect;
use NotificationDetails;
use RGAS\Modules\NS\NS;
use Input;
use RGAS\Modules\NS\Logs;
use Receivers;
use Employees;
use NotifSignatory;
use Response;
use URL;
use DB;


class OffsetData implements DataParseRepository
{
    private $sessionParse;
    private $logs;
    private $NS;
    protected $superiorEmail;
    public function __construct()
    {

        $this->NS = new NS;
        $this->logs = new Logs;
        $this->sessionParse = array(
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'employeeid' => Session::get('employeeid'),
            'company' => Session::get('company'),
            'department' => Session::get('dept_name'),
            'ownerid' => Session::get('employee_id')
        );
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
    }

    public function forSend($inputs)
    {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];

        /*

        ADDED VALIDATION
        */
        $dateValidation = $this->checkDates($requestDetails['dateFrom'],$requestDetails['periodFrom'],$requestDetails['listOffsetReferences'],$requestDetails['scheduleType'],$requestDetails['duration'],$requestDetails['dateTo']);
        if (! $dateValidation) {
            return Response::json(array(
                'success' => false,
                'errors' => ["Check Time Offset Reference."],
                'message' => 'Some fields are incomplete',
                400
            ));
        }

        return "success";

        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],

            'from_absenthours' => $requestDetails["hoursFrom"],
            'to_absenthours' => $requestDetails["hoursTo"],
            'from_date' => $requestDetails["dateFrom"] ? $requestDetails["dateFrom"] : null,
            'to_date' => $requestDetails["dateTo"] ? $requestDetails["dateTo"] : null,
            'duration_type' => $requestDetails["duration"],
            'to_absentperiod' => $requestDetails["periodTo"],
            'from_absentperiod' => $requestDetails["periodFrom"],
            'totaldays' => $requestDetails["totalDays"],
            'totalhours' => $requestDetails["totalHours"],
            'reason' => $requestDetails["reason"],
            'schedule_type' => $requestDetails['scheduleType'],
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
            'curr_emp' => Session::get('superiorid'),
        );


        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $listReference = [];
        foreach($requestDetails["listOffsetReferences"] as $reference) {
            array_push($listReference,array(
                'date' => $reference['OTDate'],
                'type' => $reference['type'],
                'time_start' => $reference['timeInStartTime'],
                'time_end' => $reference['timeOutEndTime'],
                'total_time' => $reference['OTDuration'],
                'reason' => $reference['reason'],
                'destination' => $reference['OBDestination']
            ));
        }
        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::fetch("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Response::json(array(
                        'success' => false,
                        'errors' => ["No notification receiver declared"],
                        'message' => 'Something went wrong',
                        400
                    ));
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => ["No Immediate superior declared"],
                    'message' => 'Something went wrong',
                    400
                ));

            }

        }
        
        $creation = Notifications::create($dataGather);
        for ($i=0; $i < count($listReference); $i++) {
            $listReference[$i]['notificationid'] = $creation->id ;
        }
        if(count($listReference) > 0) {
            NotificationDetails::insert($listReference);
        }
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        $files = $inputs["attachments"];
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $dataGather['documentcode'].'-'.$dataGather['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('desig_level') == "president" ? $hrReciever['employee']['email'] : $this->superiorEmail,
            $this->NS->getEmpData(Session::get('desig_level') == "president" ? $hrReciever['employee']['id'] : Session::get("superiorid"))
        );
        
        if(!$creation){
            return Response::json(array(
                'success' => false,
                'message' => 'Something went wrong',
                400
            ));
        }else{
            if(Session::get('desig_level') == "president") {
                return Response::json([
                    'success' => true,
                    'message' => "Successfully approved and sent to HR Receiver",
                    'url' => URL::to("ns/notifications"),
                    200
                ]);
            }else{
                return Response::json([
                    'success' => true,
                    'message' => "Notification request successfully sent to Immediate Superior for approval.",
                    'url' => URL::to("ns/notifications"),
                    200
                ]);
            }
        }
    }

    
    public function forSave($inputs)
    {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_absenthours' => $requestDetails["hoursFrom"],
            'to_absenthours' => $requestDetails["hoursTo"],
            'from_date' => $requestDetails["dateFrom"] ? $requestDetails["dateFrom"] : null,
            'to_date' => $requestDetails["dateTo"] ? $requestDetails["dateTo"] : null,
            'duration_type' => $requestDetails["duration"],
            'to_absentperiod' => $requestDetails["periodTo"],
            'from_absentperiod' => $requestDetails["periodFrom"],
            'totaldays' => $requestDetails["totalDays"],
            'totalhours' => $requestDetails["totalHours"],
            'reason' => $requestDetails["reason"],
            'curr_emp' => Session::get('employee_id'),
            'schedule_type' => $requestDetails['scheduleType'],
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
        );

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);

        $listReference = [];
        foreach($requestDetails["listOffsetReferences"] as $reference) {
            array_push($listReference,array(
                'date' => $reference['OTDate'],
                'type' => $reference['type'],
                'time_start' => $reference['timeInStartTime'],
                'time_end' => $reference['timeOutEndTime'],
                'total_time' => $reference['OTDuration'],
                'reason' => $reference['reason'],
                'destination' => $reference['OBDestination']
            ));
        }
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $creation = Notifications::create($dataGather);
        for ($i=0; $i < count($listReference); $i++) {
            $listReference[$i]['notificationid'] = $creation->id ;
        }
        if(count($listReference) > 0) {
            NotificationDetails::insert($listReference);
        }
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);

        if(!$creation){
            return Response::json(array(
                'success' => false,
                'message' => 'Something went wrong',
                400
            ));
        }else{
            return Response::json([
                'success' => true,
                'message' => "Notification request successfully saved and can be edited later.",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }
    }

    public function reSave($inputs,$id) {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));


        $notifDetails = $notification->first();

        if ($notifDetails['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notification->first(["id"]));

            if ($signatories->count() > 0) {

                $signatories->delete();

            }
        }

        $dataParse = array(
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_absenthours' => $requestDetails["hoursFrom"],
            'to_absenthours' => $requestDetails["hoursTo"],
            'from_date' => $requestDetails["dateFrom"] ? $requestDetails["dateFrom"] : null,
            'to_date' => $requestDetails["dateTo"] ? $requestDetails["dateTo"] : null,
            'duration_type' => $requestDetails["duration"],
            'to_absentperiod' => $requestDetails["periodTo"],
            'from_absentperiod' => $requestDetails["periodFrom"],
            'totaldays' => $requestDetails["totalDays"],
            'totalhours' => $requestDetails["totalHours"],
            'reason' => $requestDetails["reason"],
            'schedule_type' => $requestDetails['scheduleType'],
            'curr_emp' => Session::get('employee_id'),
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
        );

        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $listReference = [];
        foreach($requestDetails["listOffsetReferences"] as $reference) {
            array_push($listReference,array(
                'date' => $reference['OTDate'],
                'type' => $reference['type'],
                'time_start' => $reference['timeInStartTime'],
                'time_end' => $reference['timeOutEndTime'],
                'total_time' => $reference['OTDuration'],
                'reason' => $reference['reason'],
                'destination' => $reference['OBDestination'],
                'notificationid' => $id
            ));
        }
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $notification->update($dataGather);
        NotificationDetails::where('notificationid',$id)->delete();
        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        for ($i=0; $i < count($listReference); $i++) {
            $listReference[$i]['notificationid'] = $id ;
        }
        NotificationDetails::insert($listReference);
        return Response::json([
            'success' => true,
            'message' => "Notification request successfully saved and can be edited later.",
            'url' => URL::to("ns/notifications"),
            200
        ]);
    }
    public function reSend($inputs,$id) {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];


        /*

        ADDED VALIDATION
        */

        $dateValidation = $this->checkDates($requestDetails['dateFrom'],$requestDetails['listOffsetReferences']);

        if (! $dateValidation) {
            return Response::json(array(
                'success' => false,
                'errors' => ["Check Time Offset Reference."],
                'message' => 'Some fields are incomplete',
                400
            ));
        }

        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $notifDetails = $notification->first();
        if ($notifDetails['status'] != 'NEW') {

            NotifSignatory::where('notification_id',$id)->delete();

            $comment = DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment']
                ))
                ."')");
        }else{
            $comment =  json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            );
        }

        $dataParse = [
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_absenthours' => $requestDetails["hoursFrom"],
            'to_absenthours' => $requestDetails["hoursTo"],
            'from_date' => $requestDetails["dateFrom"] ? $requestDetails["dateFrom"] : null,
            'to_date' => $requestDetails["dateTo"] ? $requestDetails["dateTo"] : null,
            'duration_type' => $requestDetails["duration"],
            'to_absentperiod' => $requestDetails["periodTo"],
            'from_absentperiod' => $requestDetails["periodFrom"],
            'totaldays' => $requestDetails["totalDays"],
            'totalhours' => $requestDetails["totalHours"],
            'reason' => $requestDetails["reason"],
            'schedule_type' => $requestDetails['scheduleType'],
            'comment' => $comment,
            'curr_emp' => Session::get("superiorid")
            ];

        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $listReference = [];
        foreach($requestDetails["listOffsetReferences"] as $reference) {
            array_push($listReference,array(
                'date' => $reference['OTDate'],
                'type' => $reference['type'],
                'time_start' => $reference['timeInStartTime'],
                'time_end' => $reference['timeOutEndTime'],
                'total_time' => $reference['OTDuration'],
                'reason' => $reference['reason'],
                'destination' => $reference['OBDestination'],
                'notificationid' => $id
            ));
        }
        $dataGather = array_merge($this->sessionParse,$dataParse);
        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::fetch("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Response::json(array(
                        'success' => false,
                        'errors' => ["No notification receiver declared"],
                        'message' => 'Something went wrong',
                        400
                    ));
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => ["No Immediate superior declared"],
                    'message' => 'Something went wrong',
                    400
                ));

            }

        }

        $notification->update($dataGather);
        NotificationDetails::where('notificationid',$id)->delete();
        for ($i=0; $i < count($listReference); $i++) {
            $listReference[$i]['notificationid'] = $notifDetails['id'];
        }
        NotificationDetails::insert($listReference);
        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('desig_level') == "president" ? $hrReciever['employee']['email'] : $this->superiorEmail,
            $this->NS->getEmpData(Session::get('desig_level') == "president" ? $hrReciever['employee']['id'] : Session::get("superiorid"))
        );

        if(Session::get('desig_level') == "president") {
            return Response::json([
                'success' => true,
                'message' => "Successfully approved and sent to HR Receiver",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }else{
            return Response::json([
                'success' => true,
                'message' => "Notification request successfully sent to Immediate Superior for approval.",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }

    }

    public function checkDates($dateFrom,$periodFrom,$references,$scheduleType,$duration,$dateTo = null) {

        $monthFrom = date("m",strtotime($dateFrom));
        $yearFrom = date("Y",strtotime($dateFrom));
        $dayFrom = date("d",strtotime($dateFrom));
        $first = [1,2,3,4,5,6,7];
        $second = [4,5,6,7,8,9,10];
        $third = [7,8,9,10,11,12,1];
        $fourth = [10,11,12,1,2,3,4];
        $minDate = null;
        $maxDate = null;
        $yearNow = date("Y");
        $dateValidation = [];
        $dateToValidation = [];
        if(in_array($monthFrom, $first) && $yearNow == $yearFrom) {
            $key = array_search($monthFrom, $first) + 1;
            $dateValidation = array_slice($first,0,$key);
        }else if(in_array($monthFrom, $second) && $yearNow == $yearFrom){
             $key = array_search($monthFrom, $second) + 1;
             $dateValidation = array_slice($second,0,$key);
        }else if(in_array($monthFrom, $third)){
             $key = array_search($monthFrom, $third) + 1;
             $dateValidation = array_slice($third,0,$key);
        }else if(in_array($monthFrom, $fourth)){
             $key = array_search($monthFrom, $fourth) + 1;
             $dateValidation = array_slice($fourth,0,$key);
        }

        if($dateTo) {
            $monthTo = date("m",strtotime($dateTo));
            $yearTo = date("Y",strtotime($dateTo));
            $dayTo = date("d",strtotime($dateTo));
            $first = [1,2,3,4,5,6,7];
            $second = [4,5,6,7,8,9,10];
            $third = [7,8,9,10,11,12,1];
            $fourth = [10,11,12,1,2,3,4];
            $minDate = null;
            $maxDate = null;
            $yearNow = date("Y");
            if(in_array($monthTo, $first) && $yearNow == $yearTo) {
                $key = array_search($monthTo, $first) + 1;
                $dateToValidation = array_slice($first,0,$key);
            }else if(in_array($monthTo, $second) && $yearNow == $yearTo){
                 $key = array_search($monthTo, $second) + 1;
                 $dateToValidation = array_slice($second,0,$key);
            }else if(in_array($monthTo, $third)){
                 $key = array_search($monthTo, $third) + 1;
                 $dateToValidation = array_slice($third,0,$key);
            }else if(in_array($monthTo, $fourth)){
                 $key = array_search($monthTo, $fourth) + 1;
                 $dateToValidation = array_slice($fourth,0,$key);
            }
        }
        if($duration == "multiple" && $scheduleType != "special") {
            $days_in_month = cal_days_in_month(CAL_GREGORIAN,$monthFrom,$yearFrom);
            $hoursFromDateValidation = $this->countDaysInValidation($dateFrom,$dateValidation,$days_in_month,$yearFrom,$scheduleType,$periodFrom);
            
            $filtered_months = $this->removeDuplicates($dateValidation,$dateToValidation);
            $hoursFromReference = 0;

            $minDate = date('Y-m-d', strtotime($yearNow.'-'.$dateValidation[0].'-01'));
            $maxDate = date('Y-m-d',strtotime($yearFrom.'-'.$dateValidation[count($dateValidation) - 1].'-'.$dayFrom));
            foreach ($references as $key) {
                if (! (($key['OTDate'] >= $minDate) && ($key['OTDate'] <= $maxDate))) {
                    return false;
                }
                $OTDate_month = date('n', strtotime($key['OTDate']));
                if(in_array($OTDate_month,$filtered_months)) {
                    $hoursFromReference += $key['OTDuration'];
                }
            };
            if($dateValidation !== $dateToValidation) {
                if($hoursFromReference > $hoursFromDateValidation) {
                    return false;
                }
            }
        }else{
            $minDate = date('Y-m-d', strtotime($yearNow.'-'.$dateValidation[0].'-01'));
            $maxDate = date('Y-m-d',strtotime($yearFrom.'-'.$dateValidation[count($dateValidation) - 1].'-'.$dayFrom));
            foreach ($references as $key) {
                if (! (($key['OTDate'] >= $minDate) && ($key['OTDate'] <= $maxDate))) {
                    return false;
                }
            };
        }
        

        return true;
    }

    // public function construasdctDate($dateFrom,$monthArray,$days,$year,$restDays) {
    //     $month = $monthArray[count($monthArray)-1];
    //     $dateFrom = date_create($dateFrom); 
    //     $date = date_create($year.'-'.$month.'-'.$days);
    //     $diffDays = date_diff($dateFrom,$date)->format("%a") + 1;
    //     return date_format($date,'Y-m-d');
    //     $day = date('w',strtotime($year.'-'.$month.'-'.$days)) + 1;
    //     $days = 0;

    //     for ($i = 0; $i < $diffDays; $i++) {
    //         // startDate.setDate(startDate.getDate() + 1); //number  of days to add, e.x. 15 days
    //         // var dateFormated = startDate.toISOString().substr(0,10);
    //         // var finalDate = new Date(dateFormated);
    //         // var day = finalDate.getDay();
            
    //         // var checkIfrestDay = restDays.indexOf(day);
    //         if (! in_array($day, $restDays))
    //         {
    //             $days += 1;
    //         }
    //     }

    //     return $days;

    // }

    public function countDaysInValidation($first,$monthArray,$days_in_month,$year,$scheduleType,$periodFrom,$step = '+1 day', $output_format = 'Y-m-d' ) {
         if($scheduleType == "compressed") {
            $restDays = [0,6];
            $whole = 9;
        }elseif($scheduleType == "regular") {
            $restDays = [0];
            $whole = 8;
        }

        $half = $whole / 2;


        $month = $monthArray[count($monthArray)-1];
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($year.'-'.$month.'-'.$days_in_month);
        while( $current <= $last ) {
            $day = date('w',$current);
            if(! in_array($day, $restDays)) {
                $dates[] = date($output_format, $current);
            }
            $current = strtotime($step, $current);
        }

        return $periodFrom != "whole" ? count($dates) * $whole - $half : count($dates) * $whole;
    }

    public function removeDuplicates($array1,$array2) {
        $filtered = [];
        foreach ($array1 as $key) {
            if(! in_array($key,$array2)) {
                array_push($filtered,$key);
            }
        }
        return $filtered;
    }
    
}