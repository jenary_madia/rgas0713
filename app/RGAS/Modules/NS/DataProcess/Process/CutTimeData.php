<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:25 PM
 */

namespace RGAS\Modules\NS\DataProcess\Process;


use RGAS\Modules\NS\DataProcess\Contracts\DataParseRepository;
use Notifications;
use RGAS\Modules\NS\Logs;
use Session;
use Redirect;
use RGAS\Modules\NS\NS;
use Input;
use Receivers;
use NotifSignatory;
use Employees;
use Response;
use URL;
use DB;

class CutTimeData implements DataParseRepository
{
    private $sessionParse;
    private $NS;
    private $logs;
    protected $superiorEmail;
    public function __construct()
    {
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
        $this->NS = new NS;
        $this->logs = new Logs;
        $this->sessionParse = array(
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'employeeid' => Session::get('employeeid'),
            'company' => Session::get('company'),
            'department' => Session::get('dept_name'),
            'ownerid' => Session::get('employee_id')
        );
    }

    public function forSend($inputs)
    {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_inout' => $requestDetails['fromTimeInOut'],
            'to_inout' => $requestDetails['toTimeInOut'],
            'reason' => $requestDetails['reason'],
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
            'curr_emp' => Session::get('superiorid'),
        );
        if($requestDetails['dateFrom']) {
            $dataParse['from_date'] = $requestDetails['dateFrom'];
        }

        if($requestDetails['dateTo']) {
            $dataParse['to_date'] = $requestDetails['dateTo'];
        }

        if($requestDetails['totalDays']) {
            $dataParse['totaldays'] = $requestDetails['totalDays'];
        }

        if($requestDetails['totalHours']) {
            $dataParse['totalhours'] = $requestDetails['totalHours'];
        }

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::fetch("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Response::json(array(
                        'success' => false,
                        'errors' => ["No notification receiver declared"],
                        'message' => 'Something went wrong',
                        400
                    ));
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => ["No Immediate superior declared"],
                    'message' => 'Something went wrong',
                    400
                ));

            }

        }

        $creation = Notifications::create($dataGather);

        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $dataGather['documentcode'].'-'.$dataGather['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('desig_level') == "president" ? $hrReciever['employee']['email'] : $this->superiorEmail,
            $this->NS->getEmpData(Session::get('desig_level') == "president" ? $hrReciever['employee']['id'] : Session::get("superiorid"))
        );
        $files = $inputs["attachments"];
        if($files) {
            $this->logs->AU002($creation->id, 'notifications', 'id', json_encode($files), $dataGather['documentcode'].'-'.$dataGather['codenumber']);
        }
        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        /***************DISPLAY RESPONSE********************/
        if(!$creation){
            return Response::json(array(
                'success' => false,
                'message' => 'Something went wrong',
                400
            ));
        }else{
            if(Session::get('desig_level') == "president") {
                return Response::json([
                    'success' => true,
                    'message' => "Successfully approved and sent to HR Receiver.",
                    'url' => URL::to("ns/notifications"),
                    200
                ]);
            }else{
                return Response::json([
                    'success' => true,
                    'message' => "Notification request successfully sent to Immediate Superior for approval.",
                    'url' => URL::to("ns/notifications"),
                    200
                ]);
            }
        }
    }

    public function forSave($inputs)
    {
        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $dataParse = array(
            'documentcode' => date('Y-m'),
            'codenumber' => (new Notifications)->generateCodeNumber(),
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_inout' => $requestDetails['fromTimeInOut'],
            'to_inout' => $requestDetails['toTimeInOut'],
            'reason' => $requestDetails['reason'],
            'curr_emp' => Session::get('employee_id'),
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
        );

        if($requestDetails['dateFrom']) {
            $dataParse['from_date'] = $requestDetails['dateFrom'];
        }

        if($requestDetails['dateTo']) {
            $dataParse['to_date'] = $requestDetails['dateTo'];
        }

        if($requestDetails['totalDays']) {
            $dataParse['totaldays'] = $requestDetails['totalDays'];
        }

        if($requestDetails['totalHours']) {
            $dataParse['totalhours'] = $requestDetails['totalHours'];
        }

        $files = $this->NS->attachments($dataParse['documentcode'].'-'.$dataParse['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $creation = Notifications::create($dataGather);

        $this->logs->AU001($creation->id,'notifications','id',json_encode($dataGather),$dataGather['documentcode'].'-'.$dataGather['codenumber']);
        if(!$creation){
            return Response::json(array(
                'success' => false,
                'message' => 'Something went wrong',
                400
            ));
        }else{
            return Response::json([
                'success' => true,
                'message' => "Notification request successfully saved and can be edited later.",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }
    }

    public function reSave($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $notifDetails = $notification->first();

        if ($notifDetails['status'] != 'NEW') {

            $signatories = NotifSignatory::where('notification_id', $notifDetails["id"]);

            if ($signatories->count() > 0) {

                $signatories->delete();

            }

        }

        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $dataParse = array(
            'datecreated' => date('Y-m-d'),
            'status' => 'NEW',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_inout' => $requestDetails['fromTimeInOut'],
            'to_inout' => $requestDetails['toTimeInOut'],
            'reason' => $requestDetails['reason'],
            'curr_emp' => Session::get('employee_id'),
            'comment' => json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            ),
        );

        if($requestDetails['dateFrom']) {
            $dataParse['from_date'] = $requestDetails['dateFrom'];
        }

        if($requestDetails['dateTo']) {
            $dataParse['to_date'] = $requestDetails['dateTo'];
        }

        if($requestDetails['totalDays']) {
            $dataParse['totaldays'] = $requestDetails['totalDays'];
        }

        if($requestDetails['totalHours']) {
            $dataParse['totalhours'] = $requestDetails['totalHours'];
        }

        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);
        $notification->update($dataGather);
        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        return Response::json([
            'success' => true,
            'message' => "Notification request successfully saved and can be edited later.",
            'url' => URL::to("ns/notifications"),
            200
        ]);
    }
    public function reSend($inputs,$id) {
        $notification = Notifications::where('id',$id)
            ->where('ownerid',Session::get("employee_id"));

        $notifDetails = $notification->first();

        if ($notifDetails['status'] != 'NEW') {

            NotifSignatory::where('notification_id', $id)->delete();

            $comment = DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment']
                ))
                ."')");
        }else{
            $comment =  json_encode(array(
                    'name'=>Session::get('firstname').' '.Session::get('lastname'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> $inputs['comment'])
            );
        }

        $formDetails = $inputs["formDetails"];
        $requestDetails = $inputs["requestDetails"];
        $dataParse = array(
            'datecreated' => date('Y-m-d'),
            'status' => 'FOR APPROVAL',
            'contact_no' => $formDetails['contactNumber'],
            'section' => $formDetails['section'],
            'noti_type' => $inputs['notificationType'],
            'from_inout' => $requestDetails['fromTimeInOut'],
            'to_inout' => $requestDetails['toTimeInOut'],
            'reason' => $requestDetails['reason'],
            'comment' => $comment,
            'curr_emp' => Session::get('superiorid')
        );

        if($requestDetails['dateFrom']) {
            $dataParse['from_date'] = $requestDetails['dateFrom'];
        }

        if($requestDetails['dateTo']) {
            $dataParse['to_date'] = $requestDetails['dateTo'];
        }

        if($requestDetails['totalDays']) {
            $dataParse['totaldays'] = $requestDetails['totalDays'];
        }

        if($requestDetails['totalHours']) {
            $dataParse['totalhours'] = $requestDetails['totalHours'];
        }

        $files = $this->NS->attachments($notification->first()['documentcode'].'-'.$notification->first()['codenumber']);
        $dataParse = array_merge($dataParse,$files);
        $dataGather = array_merge($this->sessionParse,$dataParse);

        if( ! Session::get('superiorid')) {
            if(Session::get('desig_level') == "president") {
                $hrReciever = Receivers::fetch("notifications","HR_NOTIFICATION_RECEIVER",Session::get("company"));
                if(! $hrReciever) {
                    return Response::json(array(
                        'success' => false,
                        'errors' => ["No notification receiver declared"],
                        'message' => 'Something went wrong',
                        400
                    ));
                }
                $dataGather['curr_emp'] = $hrReciever['employee']['id'];
                $dataGather['status'] = 'APPROVED';
            }else{
                return Response::json(array(
                    'success' => false,
                    'errors' => ["No Immediate superior declared"],
                    'message' => 'Something went wrong',
                    400
                ));

            }

        }
        
        $notification->update($dataGather);
        
        $this->logs->AU004($notifDetails['id'],'notifications','id',json_encode($dataGather),$notifDetails['documentcode'].'-'.$notifDetails['codenumber'],json_encode($notifDetails));
        $this->NS->sendMail(
            'checking',
            'FOR APPROVAL',
            $notifDetails['documentcode'].'-'.$notifDetails['codenumber'],
            Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
            Session::get('dept_name'),
            Session::get('desig_level') == "president" ? $hrReciever['employee']['email'] : $this->superiorEmail,
            $this->NS->getEmpData(Session::get('desig_level') == "president" ? $hrReciever['employee']['id'] : Session::get("superiorid"))
        );

        if(Session::get('desig_level') == "president") {
            return Response::json([
                'success' => true,
                'message' => "Successfully approved and sent to HR Receiver.",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }else{
            return Response::json([
                'success' => true,
                'message' => "Notification request successfully sent to Immediate Superior for approval.",
                'url' => URL::to("ns/notifications"),
                200
            ]);
        }
    }

}