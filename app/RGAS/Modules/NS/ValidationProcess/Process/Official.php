<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:29 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;
use Response;

class Official implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $requestDetails = $inputs['requestDetails'];
        $data = array(
            'duration_type'=> $requestDetails['duration'],
            'from_date'=> $requestDetails['dateFrom'],
            'from_inout'=> $requestDetails['fromStart'],
            'from_out'=> $requestDetails['fromEnd'],
            'total_days'=> $requestDetails['totalDays'],
            'reason'=> $requestDetails['reason'],
            'destination'=> $requestDetails['destination']
        );
        
        $rules = array(
            'duration_type'=> 'required',
            'from_date'=> 'required|date_format:"Y-m-d"',
            'from_inout'=> 'required|date_format:"g:i A"',
            'from_out'=> 'required|date_format:"g:i A"',
            'total_days'=> 'required|numeric',
            'reason'=> 'required',
            'destination'=> 'required'
        );

        if($requestDetails["duration"] == "multiple")
        {
            $data['to_date'] = $requestDetails['dateTo'];
            $data['to_inout'] = $requestDetails['toStart'];
            $data['to_out'] = $requestDetails['toEnd'];

            $rules['to_date'] = 'required|date_format:"Y-m-d"';
            $rules['to_inout'] = 'required|date_format:"g:i A"';
            $rules['to_out'] = 'required|date_format:"g:i A"';
        }

        $validator = Validator::make($data,$rules,[
            'from_date.date_format' => "Date from format is invalid ex. 2016-12-12.",
            'from_inout.date_format' => "From in from format is invalid ex. 2:00 AM.",
            'from_out.date_format' => "From out format is invalid ex. 2:00 AM.",
            'to_date.date_format' => "Date to format is invalid ex. 2016-12-12.",
            'to_inout.date_format' => "To in format is invalid ex. 2:00 AM.",
            'to_out.date_format' => "To out format is invalid ex. 2:00 AM.",
            'duration_type.required' => 'OB duration is required.',
            'from_date.required' => 'From date is required.',
            'from_inout.required' => 'From start time is required.',
            'from_out.required' => 'From end time is required.',
            'total_days.required' => 'Total days is required.',
            'reason.required' => 'Reason is required.',
            'destination.required' => 'Destination is required.',
            'to_date.required' => 'To date is required.',
            'to_inout.required' => 'To start time is required.',
            'to_out.required' => 'To end time is required.'
        ]);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                400
            ));
        }
        return 1;
    }
}