<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:25 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;
use Response;

class CutTime implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $validator = Validator::make(
            array(
                'date_from' => $inputs['requestDetails']['dateFrom'],
                'date_to' => $inputs['requestDetails']['dateTo'],
                'total_days' => $inputs['requestDetails']['totalDays'],
                'total_hours' => $inputs['requestDetails']['totalHours'],
                'time_in/out_from' => $inputs['requestDetails']['fromTimeInOut'],
                'time_in/out_to' => $inputs['requestDetails']['toTimeInOut'],
                'reason' => $inputs['requestDetails']['reason'],
            ),
            array(
                'total_days' => 'required|numeric|between: 1,99',
                'total_hours' => 'required|numeric|between: 1,99',
                'date_from' => 'required|date_format:"Y-m-d"',
                'time_in/out_from' => 'required|date_format:"g:i A"',
                'date_to' => 'required|date_format:"Y-m-d"',
                'time_in/out_to' => 'required|date_format:"g:i A"',
                'reason' => 'required',
            ),
            [
                'date_from.date_format' => "Date from format is invalid ex. 2016-12-12.",
                'date_to.date_format' => "Date to format is invalid ex. 2016-12-12.",
                'time_in/out_from.date_format' => "Time in/out from format is invalid ex. 2:00 AM.",
                'time_in/out_to.date_format' => "Time in/out to format is invalid ex. 2:00 AM.",
                'total_days.required' => 'Total number of days is required.',
                'total_hours.required' => 'Total hours is required.',
                'date_from.required' => 'From date is required.',
                'time_in/out_from.required' => 'From time in/out is required.',
                'date_to.required' => 'To date is required.',
                'time_in/out_to.required' => 'To time in/out is required.',
                'reason.required' => 'Reason is required.',
            ]
        );

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                400
            ));
        }
        return 1;
    }
}