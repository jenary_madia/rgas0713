<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 5:28 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;
use Response;

class TimeKeep implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $requestDetails = $inputs['requestDetails'];
        $fields = array(
            'date_from' => $requestDetails["date"],
            'to_correct' => $requestDetails["toCorrect"],
            'reason' => $requestDetails["reason"]
        );
        $rules = array(
            'date_from' => 'required|date_format:"Y-m-d"',
            'to_correct' => 'required',
            'reason' => 'required'
        );

        if($requestDetails["toCorrect"]) {
            if($requestDetails["toCorrect"] == 'no_time_in') {
                $fields['from_inout'] = $requestDetails["timeIn"];
                $rules['from_inout'] = 'required|date_format:"g:i A"';
            }elseif($requestDetails["toCorrect"] == 'no_time_out'){
                $fields['to_inout'] = $requestDetails["timeOut"];
                $rules['to_inout'] ='required|date_format:"g:i A"';
            }else{
                $fields['from_inout'] = $requestDetails["timeIn"];
                $rules['from_inout'] = 'required|date_format:"g:i A"';
                $fields['to_inout'] = $requestDetails["timeOut"];
                $rules['to_inout'] = 'required|date_format:"g:i A"';
            }
        }

        $message = [
            'date_from.required' => 'Date is required.',
            'date_from.date_format' => 'Date format is invalid ex. 2016-12-12.',
            'from_inout.date_format' => 'Time in from format is invalid ex. 2:00 AM.',
            'to_inout.date_format' => 'Time in to format is invalid ex. 2:00 AM.',
            'from_inout.required' => 'Time in is required.',
            'to_inout.required' => 'Time out is required.',
            'to_correct.required' => "To correct is required.",
            'reason.required' => "Reason is required.",
        ];
        
        $validator = Validator::make(
            $fields,$rules,$message
        );

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                400
            ));
        }
        return 1;
    }
}