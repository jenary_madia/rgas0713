<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 18/07/2016
 * Time: 3:29 PM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;
use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;
use Response;


class Undertime implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $requestDetails = $inputs['requestDetails'];
        $params = [
            'schedule_type' => $requestDetails['scheduleType'],
            'date' => $requestDetails['date'],
            'time_out' => $requestDetails['timeout'],
            'reason' => $requestDetails['reason']
        ];
        $rules = [
            'schedule_type' => 'required',
            'date' => 'required|date_format:"Y-m-d"',
            'time_out' =>'required|date_format:"g:i A"',
            'reason' => 'required'
        ];

        if ($requestDetails['scheduleType'] == "special") {
            $params['schedule_time_out'] = $requestDetails['scheduleTimeout'];
            $rules['schedule_time_out'] = 'required|date_format:"g:i A"';
        }

        $validator = Validator::make(
            $params,
            $rules,
            [
                'date.date_format' => "Date format is invalid ex. 2016-12-12.",
                'schedule_time_out.date_format' => "Schedule time out format is invalid ex. 2:00 AM.",
                'time_out.date_format' => "Timeout format is invalid ex. 2:00 AM.",
                'schedule_type.required' => 'Schedule type is required.',
                'schedule_time_out.required' => 'Schedule time out is required.',
                'date.required' => 'Date is required.',
                'time_out.required' =>'Time out is required.',
                'reason.required' => 'Reason is required.'
            ]
        );

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                400
            ));
        }
        return 1;

    }
}