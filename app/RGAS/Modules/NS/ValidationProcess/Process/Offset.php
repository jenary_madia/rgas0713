<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 20/07/2016
 * Time: 9:30 AM
 */

namespace RGAS\Modules\NS\ValidationProcess\Process;


use RGAS\Modules\NS\ValidationProcess\Contracts\ValidationRepository;
use Validator;
use Redirect;
use Input;
use Response;


class Offset implements ValidationRepository
{
    public function validateInputs(array $inputs)
    {
        $requestDetails = $inputs['requestDetails'];
        $data = array(
            'hours_from' => $requestDetails['hoursFrom'],
            'date_from' => $requestDetails['dateFrom'],
            'duration_offset' => $requestDetails['duration'],
            'schedule_type' => $requestDetails['scheduleType'],
            'period_from' => $requestDetails['periodFrom'],
            'total_days' => $requestDetails['totalDays'],
            'total_hours' => $requestDetails['totalHours'],
            'list_reference' => json_encode($requestDetails['listOffsetReferences']),
            'reason' => $requestDetails['reason'],
        );

        $rules = array(
            'schedule_type' => 'required',
            'duration_offset' => 'required',
            'reason' => 'required',
            'date_from' => 'required|date_format:"Y-m-d"',
            'period_from' => 'required',
            'hours_from' => 'required|numeric|min:1|max:12',
            'total_days' => 'required',
            'total_hours' => 'required',
            'list_reference' => 'required|not_in:[]',
        );

        if($requestDetails['duration'] == "multiple")
        {
            $data['date_to'] = $requestDetails['dateTo'];
            $data['period_to'] = $requestDetails['periodTo'];
            $data['hours_to'] = $requestDetails['hoursTo'];

            $rules['date_to'] = 'required|date_format:"Y-m-d"';
            $rules['period_to'] = 'required';
            $rules['hours_to'] = 'required|numeric|min:1|max:12';
        }

        $message = array(
            'schedule_type.required' => 'Schedule type is required.',
            'list_reference.not_in' => 'Please provide time offset reference.',
            'date_from.date_format' => "Date from format is invalid ex. 2016-12-12.",
            'date_to.date_format' => "Date to format is invalid ex. 2016-12-12.",
            'date_from.required' => "From offset date is required.",
            'date_to.required' => "To offset date is required.",
            'duration_offset.required' => 'Duration of offset is required.',
            'reason.required' => 'Reason is required.',
            'period_from.required' => 'From offset period is required.',
            'period_to.required' => 'To offset period is required.',
            'hours_from.min' => 'Hours from is required.',
            'total_days.required' => 'Total days is required.',
            'total_hours.required' => 'Total Hours is required.',
        );
        
        $validator = Validator::make($data,$rules,$message);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
                'message' => 'Some fields are incomplete',
                400
            ));
        }

        if($requestDetails['totalListOffsetHours'] < $requestDetails['totalHours']) {
            return Response::json(array(
                'success' => false,
                'errors' => ["Check Time Offset Reference."],
                'message' => 'Some fields are incomplete',
                400
            ));
        }

        return 1;
    }
}