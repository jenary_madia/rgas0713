<?php namespace RGAS\Modules\TE;

use TrainingEndorsements;
use TrainingEndorsementsParticipants;
use Session;
use Input;
use Redirect;
use RGAS\Repositories\TERepository;
use RGAS\Libraries;
use RGAS\Modules\TE;


class TEParticipants implements ITEParticipants
{

	public $_tep_id;
	public $_tep_ref_num;
	public $parent_ref_num;	
	
	public function __construct()
	{
		$this->TE = new TrainingEndorsements;
	}
	
	public function setTep_te_id($tep_id)
	{
		$this->_tep_id = $tep_id;
	}
	
	public function setParent_ref_num($ref_num)
	{
		$this->parent_ref_num = $ref_num;
	}
	
	public function getRequest($tep_id)
	{
		// take note of this
		$request = $this->find($tep_id);
				
		return $request;
	}
	
	public function storeparticipants()
	{	
		echo "storeparticipants";
		//die();
				
		$b = array();		
		$input = Input::all();
		
		foreach($input['p_name'] as $key => $p_name){
			if($p_name != ""){
				$a['tep_te_id'] = $this->_tep_id;
				$a['tep_emp_name'] = $p_name;
				$a['tep_dep_name'] = $input['p_department'][$key];
				$a['tep_designation'] = $input['p_designation'][$key];
				$a['tep_ref_num'] = $this->parent_ref_num;
				$b[] = $a;
				unset($a);
			}
		}
		if(count($b) > 0) $this->insert($b);	
	}

}

?>