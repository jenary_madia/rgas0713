<?php namespace RGAS\Modules\TE;

use Receivers;	
use Employees;
use TrainingEndorsements;
use Input;

class EmailReceivers extends TELogs{

	function getrequestor($id)
	{
		return TrainingEndorsements::find($id);
	}
	
	function getchrd(){
		$receiver = Receivers::where('code','=','TE_CHRD')->where('module','=','te')->first();
		$employee = Employees::find($receiver->employeeid);
		
		return $employee;
	}
	function getvp(){
		$vp = Employees::where('designation','=','VP FOR CORP. HUMAN RESOURCE')->first();

		return $vp;
	}
	function getsvp(){
		$svp = Employees::where('designation','=','SVP FOR CORP. SERVICES')->first();
		
		return $svp;
	}
	function getpres(){
		$pres = Employees::where('designation','=','PRESIDENT')->first();

		return $pres;
	}
	function getfiler()
	{
		$te = TrainingEndorsements::find(Input::get('te_id'));
		$empl = Employees::where('id','=',$te->te_emp_id)->first();
				
		return $empl;
	}
	
}

?>