<?php namespace RGAS\Modules\TE\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class ValidateComments extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		$inputs['comment'] = $input['comment'];
		$rules['comment'] = 'max:5000|required';
		$messages['comment.max'] = 'Comment length must not be greater 5000 characters';
		$messages['comment.required'] = 'Error! Comment is Required!';
	
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			//return Redirect::to('te/process-for-approval/'.$input['te_id'])
			return Redirect::to( Request::url() )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error! Comment is Required!');
			exit;
			//$this->setResult(false);
		}
		
		return true;
	}
}
?>