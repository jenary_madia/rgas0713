<?php namespace RGAS\Modules\TE\Requests;

use Input;
use Validator;
use Redirect;

class EditRequest extends RResult
{
	public function validate()
	{
		$input = Input::all();
		// edit starts here
		$inputs['employee_name'] = $input['employee_name'];
		$rules['employee_name'] = 'required';
		$messages['employee_name.required'] = 'Employee Name is required';
		
		$inputs['te_ref_num'] = $input['te_ref_num'];
		$rules['te_ref_num'] = 'required';
		$messages['te_ref_num.required'] = 'Reference number is required';
		
		$inputs['employeeid'] = $input['employeeid'];
		$rules['employeeid'] = 'required';
		$messages['employeeid.required'] = 'Employee ID is required';
		
		$inputs['te_date_filed'] = $input['te_date_filed'];
		$rules['te_date_filed'] = 'required|date';
		$messages['te_date_filed.required'] = 'Date filed is required';
		$messages['te_date_filed.date'] = 'Date filed must be a valid date';

		$inputs['comp_name'] = $input['comp_name'];
		$rules['comp_name'] = 'required';
		$messages['comp_name.required'] = 'Company name is required';
		
		$inputs['te_status'] = $input['te_status'];
		$rules['te_status'] = 'required';
		$messages['te_status.required'] = 'Status is required';
		
		$inputs['dept_name'] = $input['dept_name'];
		$rules['dept_name'] = 'required';
		$messages['dept_name.required'] = 'Department name is required';
	
		$inputs['te_contact_number'] = $input['te_contact_number'];
		$rules['te_contact_number'] = 'required|numeric';
		$messages['te_contact_number.required'] = 'Contact number is required';
		$messages['te_contact_number.numeric'] = 'Contact number must be numeric';
		
		// $inputs['section'] = $input['section'];
		// $rules['section'] = 'required';
		// $messages['section.required'] = 'Section field is required';
		
 		$inputs['te_training_title'] = $input['te_training_title'];
		$rules['te_training_title'] = 'required|min:3|max:200';
		$messages['te_training_title.required'] = 'Training Title field is required';
		$messages['te_training_title.min'] = 'Training Title field must be atleast 3 characters';
		$messages['te_training_title.max'] = 'Training Title field length must not be greater 200 characters';
		
 		$inputs['te_training_date'] = $input['te_training_date'];
		$rules['te_training_date'] = 'required|date';
		$messages['te_training_date.required'] = 'Training Date field is required';
		$messages['te_training_date.date'] = 'Training Date must be a valid date';
		
		$inputs['te_amount_range'] = @$input['te_amount_range'];
		$rules['te_amount_range'] = "required";
		$messages['te_amount_range.required'] = 'Total Amount Range is required';
			
		if(isset($input["te_amount_range"])){
			list($min,$max) =  explode("-",$input["te_amount_range"]);
			
			$inputs['te_total_amount'] = $input['te_total_amount'];
			$rules['te_total_amount'] = "required|numeric|min:$min|max:$max";
			$messages['te_total_amount.required'] = 'Total Amount field is required';
			$messages['te_total_amount.numeric'] = 'Total Amount field must be numeric';
			$messages['te_total_amount.min'] = 'Total Amount should match to amount range (min)';
			$messages['te_total_amount.max'] = 'Total Amount should match to amount range (max)';
		}
		
 		
		
 		$inputs['te_vendor'] = $input['te_vendor'];
		$rules['te_vendor'] = 'required|min:3|max:200';
		$messages['te_vendor.required'] = 'Training Vendor field is required';
		$messages['te_training_title.min'] = 'Training Vendor field must be atleast 3 characters';
		$messages['te_training_title.max'] = 'Training Vendor field length must not be greater 200 characters';
		
 		$inputs['te_training_venue'] = $input['te_training_venue'];
		$rules['te_training_venue'] = 'required|min:3|max:200';
		$messages['te_training_venue.required'] = 'Training Venue field is required';
		$messages['te_training_venue.min'] = 'Training Venue field must be atleast 3 characters';
		$messages['te_training_venue.max'] = 'Training Venue field length must not be greater 200 characters';
		
 		$inputs['te_echo_training_date'] = $input['te_echo_training_date'];
		$rules['te_echo_training_date'] = 'required|date';
		$messages['te_echo_training_date.required'] = 'Echo Training Date field is required';
		$messages['te_echo_training_date.date'] = 'Echo Training Date must be a valid date';
		
		$inputs['comment'] = $input['comment'];
		$rules['comment'] = 'max:5000';
		$messages['comment.max'] = 'Purpose of request length must not be greater 5000 characters';
		
		// echo "<pre>";
			// print_r($input['p_name']);
		// echo "</pre>";
		
		if(!isset($input['p_name'])){
			$input['p_name'] = "";
			$inputs['participants'] = $input['p_name'];
			$rules['participants'] = 'required';
			$messages['participants.required'] = 'Please select atleast one participant ';
		}
		
		

	
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to('te/create')
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Some fields are incomplete.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
		
		//$messages = $validator->messages();
		
		//$this->setMessages($messages->all());
		
		//return $this->getMessages();
	}
}
?>