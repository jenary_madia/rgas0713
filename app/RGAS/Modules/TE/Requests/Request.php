<?php
namespace RGAS\Modules\TE\Requests;

use Input;
use Validator;

class Request implements IRequest
{
	public $rResult = false;
	public $rMessages;
	
	public function __construct()
	{
		
		$input = Input::all();
		$validator = Validator::make(
			array(
				'name' => '',
				'password' => 'lamepassword',
				
			),
			array(
				'name' => 'required',
				'password' => 'required|min:8',
				
			)
		);
		
		$this->bResult = true;
		if ($validator->fails())
		{
			$this->bResult = false;
		}
		
		$messages = $validator->messages();
		
		$this->rMessages = $messages->all();
	}
	
	public function create()
	{
		$input = Input::all();
		$validator = Validator::make(
			array(
				'name' => 'sdfgfdgdf',
				'password' => 'lamepassword',
			),
			array(
				'name' => 'required',
				'password' => 'required|min:8',
			)
		);
		
		$this->rResult = true;
		if ($validator->fails())
		{
			$this->rResult = false;
		}
		
		$messages = $validator->messages();
		
		$this->rMessages = $messages->all();
	}
	
	public function getResult()
	{
		return $this->rResult;
	}
	
	public function getMessages()
	{
		return $this->rMessages; 
	}
}
?>