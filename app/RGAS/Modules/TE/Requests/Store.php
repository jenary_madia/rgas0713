<?php namespace RGAS\Modules\TE\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		// edit starts here
		$inputs['employee_name'] = $input['employee_name'];
		$rules['employee_name'] = 'required';
		$messages['employee_name.required'] = 'Employee Name is required';
		
		// $inputs['te_ref_num'] = $input['te_ref_num'];
		// $rules['te_ref_num'] = 'required';
		// $messages['te_ref_num.required'] = 'Reference number is required';
		
		$inputs['employeeid'] = $input['employeeid'];
		$rules['employeeid'] = 'required';
		$messages['employeeid.required'] = 'Employee ID is required';
		
		$inputs['te_date_filed'] = $input['te_date_filed'];
		$rules['te_date_filed'] = 'required|date';
		$messages['te_date_filed.required'] = 'Date filed is required';
		$messages['te_date_filed.date'] = 'Date filed must be a valid date';

		$inputs['comp_name'] = $input['comp_name'];
		$rules['comp_name'] = 'required';
		$messages['comp_name.required'] = 'Company name is required';
		
		$inputs['te_status'] = $input['te_status'];
		$rules['te_status'] = 'required';
		$messages['te_status.required'] = 'Status is required';
		
		$inputs['dept_name'] = $input['dept_name'];
		$rules['dept_name'] = 'required';
		$messages['dept_name.required'] = 'Department name is required';
		
		$inputs['te_contact_number'] = $input['te_contact_number'];
		$rules['te_contact_number'] = 'max:50';
		$messages['te_contact_number.max'] = 'Contact Number field length must not be greater 50 characters';
		
		// $inputs['section'] = $input['section'];
		// $rules['section'] = 'required';
		// $messages['section.required'] = 'Section field is required';
		
 		$inputs['te_training_title'] = $input['te_training_title'];
		$rules['te_training_title'] = 'required|min:3|max:200';
		$messages['te_training_title.required'] = 'Training Title field is required';
		$messages['te_training_title.min'] = 'Training Title field must be at least 3 characters';
		$messages['te_training_title.max'] = 'Training Title field length must not be greater 200 characters';
		
 		$inputs['te_training_date'] = $input['te_training_date'];
		$rules['te_training_date'] = 'required|date';
		$messages['te_training_date.required'] = 'Training Date field is required';
		$messages['te_training_date.date'] = 'Training Date must be a valid date';
		
		$inputs['te_total_amount'] = $input['te_total_amount'];
		$rules['te_total_amount'] = "required|numeric";
		$messages['te_total_amount.required'] = 'Total Amount field is required';
		$messages['te_total_amount.numeric'] = 'Total Amount field must be numeric';
		
		// $inputs['te_amount_range'] = @$input['te_amount_range'];
		// $rules['te_amount_range'] = "required";
		// $messages['te_amount_range.required'] = 'Total Amount Range is required';
			
		// if(isset($input["te_amount_range"])){
			// list($min,$max) =  explode("-",$input["te_amount_range"]);
			
			// $inputs['te_total_amount'] = $input['te_total_amount'];
			// $rules['te_total_amount'] = "required|numeric|min:$min|max:$max";
			// $messages['te_total_amount.required'] = 'Total Amount field is required';
			// $messages['te_total_amount.numeric'] = 'Total Amount field must be numeric';
			// $messages['te_total_amount.min'] = 'Total Amount should match to amount range (min)';
			// $messages['te_total_amount.max'] = 'Total Amount should match to amount range (max)';
		// }
		
 		$inputs['te_vendor'] = $input['te_vendor'];
		$rules['te_vendor'] = 'required|min:3|max:200';
		$messages['te_vendor.required'] = 'Vendor field is required';
		$messages['te_vendor.min'] = 'Vendor field must be at least 3 characters.';
		$messages['te_vendor.max'] = 'Vendor field must not be greater than 200 characters.';
		
 		$inputs['te_training_venue'] = $input['te_training_venue'];
		$rules['te_training_venue'] = 'required|min:3|max:200';
		$messages['te_training_venue.required'] = 'Training Venue field is required';
		$messages['te_training_venue.min'] = 'Training Venue field must be atleast 3 characters';
		$messages['te_training_venue.max'] = 'Training Venue field length must not be greater 200 characters';
		
 		$inputs['te_echo_training_date'] = $input['te_echo_training_date'];
		$rules['te_echo_training_date'] = 'required|date';
		$messages['te_echo_training_date.required'] = 'Echo Training Date field is required';
		$messages['te_echo_training_date.date'] = 'Echo Training Date must be a valid date';
		
		$inputs['comment'] = $input['comment'];
		$rules['comment'] = 'max:5000';
		$messages['comment.max'] = 'Comment length must not be greater 5000 characters';
		
		if(!isset($input['p_name'])){
			$input['p_name'] = "";
			$inputs['participants'] = $input['p_name'];
			$rules['participants'] = 'required';
			$messages['participants.required'] = 'Error! Please add Employee ';
		}

		if(( $input['te_echo_training_date'] ) && ( $input['te_training_date'] ) && $input['te_training_date'] != $input['te_echo_training_date'] )
		{
			$inputs['te_training_date'] = $input['te_training_date'];
			$rules['te_training_date'] = 'before:'. $input['te_echo_training_date']  ;
			$messages['te_training_date.before'] = 'The training date must be date before ' . $input['te_training_date'];
		}
		
		$file_counter = 0;
		$fs = 0;
		if(@count($input['files']) > 1){
			// $inputs['files_count'] = count($input['files']);
			$file_counter = count($input['files']);
			$fs = 0;
			foreach($input['files'] as $a => $b){
				$fs += $b['filesize'];
			}
		}
		$inputs['files_count'] = $file_counter;
		$rules['files_count'] = 'numeric|max:10';
		$messages['files_count.numeric'] = 'Attachment count must be numeric';
		$messages['files_count.max'] = 'Attachment must not succeed more than 10';
		
		$inputs['files_size'] = $fs;
		$rules['files_size'] = 'numeric|max:20971520';
		$messages['files_size.numeric'] = 'Filesize must be numeric';
		$messages['files_size.max'] = 'Attachment filesize must not succeed more than 20MB in total';
		//edit until here

		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url()  )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error! Please accomplish required fields.');
			exit;
			//$this->setResult(false);
		}
		
		return true;

	}
}
?>