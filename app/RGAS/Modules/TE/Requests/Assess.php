<?php namespace RGAS\Modules\TE\Requests;

use Input;
use Validator;
use Redirect;

class Assess extends RResult
{
	public function validate()
	{
			$input = Input::all();

	
		// edit starts here
		$inputs['teq_target_performance'] = $input['teq_target_performance'];
		$rules['teq_target_performance'] = 'required|min:3|max:500';
		$messages['teq_target_performance.required'] = 'Target Performance is required';
		$messages['teq_target_performance.min'] = 'Target Performance field must be atleast 3 characters';
		$messages['teq_target_performance.max'] = 'Target Competency Item 1 field must not be greater than 500 characters.';
		
		$inputs['target_competency'] = $input['target_competency'];
		$rules['target_competency'] = 'required|min:3|max:500';
		$messages['target_competency.required'] = 'Target Competency is required';
		$messages['target_competency.min'] = 'Target Competency must be atleast 3 characters';
		$messages['target_competency.max'] = 'Target Competency Item 2 field must not be greater than 500 characters.';
		
		$inputs['newfunction_description'] = $input['newfunction_description'];
		$rules['newfunction_description'] = 'required|min:3|max:500';
		$messages['newfunction_description.required'] = 'Description is required';
		$messages['newfunction_description.min'] = 'Description field must be atleast 3 characters';
		$messages['newfunction_description.max'] = 'Description field must not be greater than 500 characters';
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to('te/assess/'.$input['te_id'])
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error! Please accomplish required fields.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
		
		//$messages = $validator->messages();
		
		//$this->setMessages($messages->all());
		
		//return $this->getMessages();
	}
}
?>