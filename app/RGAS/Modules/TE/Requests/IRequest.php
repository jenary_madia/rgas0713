<?php
namespace RGAS\Modules\TE\Requests;

interface IRequest
{
	// public function validate();
	public function getResult();
	public function getMessages();
}
?>