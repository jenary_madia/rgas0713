<?php
namespace RGAS\Modules\TE;

interface ITrainingEndorsementsForm
{
	// private function getStoragePath();
	public function getModuleId();
	public function storeViewLogsTE($te_id);
	public function printLog($te_id);
	public function downloadLog($te_ref_num);
	public function storeEndorsement($input);
	public function editEndorsement($te_id);
	public function workflow($te_id);
	public function assess($te_id);
	public function deleteRequest($te_id);
}
?>