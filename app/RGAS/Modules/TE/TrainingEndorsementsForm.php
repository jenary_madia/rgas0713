<?php 
namespace RGAS\Modules\TE;

use Employees;
use TrainingEndorsements;
use TrainingEndorsementsParticipants;
use Session;
use Input;
use Redirect;
use RGAS\Repositories\TERepository;
use RGAS\Libraries;
use RGAS\Modules\TE;
use ReferenceNumbers;
use Mail;
use Request;
use RGAS\Libraries\AuditTrail;
use Config;
class TrainingEndorsementsForm extends EmailReceivers implements ITrainingEndorsementsForm
{
	public $attachments_path = 'te/';
	protected $module_id;
	private $te;

	//storage path for TE attachments
	private function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'te/';
	}

	//getting of module ID
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_te');
		return $this->module_id;
	}
	
	//Audit Trail Log for Viewing
	public function storeViewLogsTE($te_id)
	{	
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$te = TrainingEndorsements::where('te_id','=',$te_id)->first();
		$this->AU003($te->te_id, $te->te_ref_num);
	}
	
	//Audit Trail Log for Printing
	public function printLog($te_id)
	{
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$te = TrainingEndorsements::where('te_id','=',$te_id)->first();
		$this->AU006($te->te_id, $te->te_ref_num);
	}
	
	//Audit Trail Log for Download
	public function downloadLog($te_ref_num)
	{
		$te = new TERepository;
		$request = $te->getRequestByRefNum($te_ref_num);
		
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$te_id = $request->te_id;
		$te_ref_num = $request->te_ref_num;
		
		foreach(json_decode($request['te_attachments']) as $attachment)
			$new = $attachment->original_filename;
		$this->AU008($te_id, $new, $te_ref_num);
	}
	
	//Creation (save and send)
	public function storeEndorsement($input)
	{	
		$te = new TrainingEndorsements;
		$ter = new TERepository;
		$chrd = $ter->getChrd();
		
		if($input['action'] == 'save')
		{
			$issent = 0;
			$status = "NEW";
			$level = "REQUESTOR";
			$current = Session::get('employee_id');			
		}
		
		elseif($input['action'] == 'send')
		{
			$issent = 1;
			$status = "FOR ASSESSMENT";
			$level = "CHRD-TOD";
			$current = $chrd->id;
			$te->te_date_assigned = date('Y-m-d');
		}
		
        $te->te_employees_id = Session::get('employee_id');
        $te->te_emp_name = Session::get('employee_name');
        $te->te_emp_id = Session::get('employee_id');
		$te->te_company = Session::get('company');
		$te->te_department = Session::get('dept_name');
		$te->te_emp_section = Input::get('section');
		$te->te_ref_num = ReferenceNumbers::get_current_reference_number('te');
        $te->te_date_filed = Input::get('te_date_filed');
        $te->te_contact_no = Input::get('te_contact_number');
        $te->te_training_title = Input::get('te_training_title');
        $te->te_training_date = Input::get('te_training_date');
        $te->te_echo_training_date = Input::get('te_echo_training_date');
        $te->te_total_amount = Input::get('te_total_amount');
        $te->te_current = $current;
        $te->te_vendor = Input::get('te_vendor');
        $te->te_training_venue = Input::get('te_training_venue');
        $te->te_attachments = @json_encode($input['files']);
		// if($input['comment'] != ""){
				$te->te_comments = json_encode(
					array(
						array(
							'name'=>Session::get('employee_name'),
							'datetime'=>date('m/d/Y g:i A'),
							'message'=>$input['comment']
						)
					)
				);
		// }
	
		$te->te_issent = $issent;
		$te->te_status = $status;
		$te->te_level = $level;
		try{
			$te->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('te');
			$te->te_ref_num = ReferenceNumbers::get_current_reference_number('te');
			$te->save();
		}
		
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU001($te->te_id, json_encode($te['attributes']), $te->te_ref_num);
		
		if(@count($input['files']) > 0){
			foreach($input['files'] as $file){
				$this->AU002($te->te_id, json_encode($file), $te->te_ref_num);
			}	
		}
		
		$tep = new TrainingEndorsementsParticipants;
		
		$b = array();	
		//JMA ADD IF CONDITION 02072017
		if(isset($input['p_name'])):			
		foreach($input['p_name'] as $key => $p_name){
			if($p_name != ""){
				$a['tep_te_id'] = $te->te_id;
				$a['tep_emp_name'] = $p_name;
				$a['tep_dep_name'] = $input['p_department'][$key];
				$a['tep_dep_id'] = $input['p_dep_id'][$key];
				$a['tep_designation'] = $input['p_designation'][$key];
			    $a['tep_emp_id'] = $input['p_id'][$key];
				$a['tep_ref_num'] = $te->te_ref_num;
				$b[] = $a;
				
				$tep_id = $tep->insertGetId($a);
				$this->setTable('trainingendorsementsparticipants');
				$this->setPrimaryKey('tep_id');
				$this->AU001($tep_id, json_encode($a), $a['tep_ref_num']);
				
				unset($a);
			}
		}
		endif;
		
		if($issent == 1){
			Mail::send('te.email_templates.te_for_assessment', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$message->to($this->getchrd()->email, $this->getchrd()->firstname." ".$this->getchrd()->lastname)->subject('RGTS Notification: TE Request for Assessment');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					$fm->move($file['random_filename'], $this->getStoragePath().$te->te_ref_num);
				}
			}
			
		}
		
		return true;
	}
	
	//Edit Request
	public function editEndorsement($te_id)
	{
		$input = Input::all();
		$te = TrainingEndorsements::find($te_id);
		$ter = new TERepository;
		$chrd = $ter->getChrd();
		$old = json_encode($te['attributes']);
		
		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}
		
		if($input['action'] == 'save')
		{
			$issent = 0;
			$level = "REQUESTOR";
			$current = Session::get('employee_id');
			
			if($input['te_status'] == 'DISAPPROVED'){
				$status = "DISAPPROVED";
			}
			else{
				$status = "NEW";	
			}
		}
		
		elseif($input['action'] == 'send')
		{
			$issent = 1;
			$status ="FOR ASSESSMENT";
			$level = "CHRD-TOD";
			$current = $chrd->id;
			
			if($te['te_status'] == 'NEW'){
				$te->te_date_assigned = date('Y-m-d');
			}
		}
		
		$te->te_emp_section = Input::get('section');
		$te->te_contact_no = Input::get('te_contact_number');
		$te->te_training_title = Input::get('te_training_title');
		$te->te_training_date = Input::get('te_training_date');
		$te->te_echo_training_date = Input::get('te_echo_training_date');
		$te->te_date_filed = date('Y-m-d');
		$te->te_total_amount = Input::get('te_total_amount');
		$te->te_current = $current;
		$te->te_vendor = Input::get('te_vendor');
		$te->te_training_venue = Input::get('te_training_venue');
		$te->te_attachments = @json_encode($input['files']);
		$te->te_issent = $issent;
		$te->te_level = $level;
		$te->te_status = $status;
		$te->te_comments = json_encode(
			array(
				array(
					'name'=>Session::get('employee_name'),
					'datetime'=>date('m/d/Y g:i A'),
					'message'=>$input['comment']
				)
			)
		);
		try{
			$te->save();
		}		
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('te');
			$te->te_ref_num = ReferenceNumbers::get_current_reference_number('te');
			$te->save();
		}
		$new = json_encode($te['attributes']);
			
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU004($te->te_id, $old, $new, $te->te_ref_num);

		$oldTep = TrainingEndorsementsParticipants::where("tep_te_id","=",$te_id)->first();
		$oldVal = json_encode($oldTep['attributes']);

		$tep = TrainingEndorsementsParticipants::where("tep_te_id","=",$te_id)->delete();
		$tep = new TrainingEndorsementsParticipants;

		
		$b = array();
		//JMA ADD IF CONDITION 02072017
		if(isset($input['p_name'])):	
		foreach($input['p_name'] as $key => $p_name){
			if($p_name != ""){
				$a['tep_te_id'] = $te->te_id;
				$a['tep_emp_name'] = $p_name;
				$a['tep_dep_name'] = $input['p_department'][$key];
				$a['tep_dep_id'] = $input['p_dep_id'][$key];
				$a['tep_designation'] = $input['p_designation'][$key];
				$a['tep_emp_id'] = $input['p_id'][$key];
				$a['tep_ref_num'] = $te->te_ref_num;
				$b[] = $a;
				
				$tep_id = $tep->insertGetId($a);
				$this->setTable('trainingendorsementsparticipants');
				$this->setPrimaryKey('tep_id');
				$this->AU004($tep_id, $oldVal, json_encode($a), $a['tep_ref_num']);
				
				unset($a);
			}
		}
		endif;
		
		if($issent == 1){
			Mail::send('te.email_templates.te_for_assessment', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$message->to($this->getchrd()->email, $this->getchrd()->firstname." ".$this->getchrd()->lastname)->subject('RGTS Notification: TE Request for Assessment');
				}
			);

			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;				
				foreach($input['files'] as $file){
					if(!file_exists($this->getStoragePath().$te->te_ref_num.'/'.$file['random_filename'])){
						$fm->move($file['random_filename'], $this->getStoragePath().$te->te_ref_num); //glenn
					}
				}
			}
			
		}
		return true;
	}

	//Approval of VP of CHR, SVP and President
	public function workflow($te_id)
	{

		$ter = new TERepository;
		$request = $ter->getRequest($te_id);
		$svpforcs = $ter->getSVPforCS();
		$vpforchr = $ter->getVPforCHR();
		$pres = $ter->getPres();

		$input=Input::all();

		$te = TrainingEndorsements::find($input['te_id']);
		$old = '"'.$te->te_level.'","'.$te->te_current.'","'.$te->te_flag.'","'.$te->te_status.'","'.$te->te_issent.'"';

		if($input['action'] == 'approve')
		{			
			
			if($te->te_level == "VPforCHR" && $te->te_current == Session::get('employee_id'))
			{
				if($te->te_total_amount >= 5001)
				{
					if($input['te_status'] == "FOR APPROVAL"){
						$level = "SVPforCS";
						$current = $svpforcs->id;
						$flag = 1;
						$status = "FOR APPROVAL";
						$issent = 1;
						$vp_app = Session::get('employee_id');
						$svp_app = "";
						$pres_app = "";
						$req_completed = "";


						Mail::send('te.email_templates.te_for_approval', array(
							'status' => $status,
							'ref_num' => $te->te_ref_num,
							'requestor' => $te->te_emp_name,
							'department' => $te->te_department),
							function($message){
								$message->to($this->getsvp()->email, $this->getsvp()->firstname." ".$this->getsvp()->lastname)->subject('RGTS Notification: TE Request for Approval');
							}
						);
					}
				}
				else{
					$level = "REQUESTOR";
					$current = $request['te_employees_id'];
					$flag = 2;
					$status = "APPROVED";
					$issent = 1;
					$vp_app = Session::get('employee_id');
					$req_completed = date('Y-m-d');
					$svp_app = "";
					$pres_app = "";

												
					Mail::send('te.email_templates.te_approved', array(
						'status' => $status,
						'ref_num' => $te->te_ref_num,
						'requestor' => $te->te_emp_name,
						'department' => $te->te_department),
						
						function($message){
								$message->to($this->getfiler()->email, $this->getfiler()->firstname." ".$this->getfiler()->lastname)->subject('RGTS Notification: TE Request Approved');
						}
					);
					
				}
			}
			elseif($te->te_level == "SVPforCS" && $te->te_current == Session::get('employee_id'))
			{
				if($input['te_status'] == "FOR APPROVAL" && $te->te_total_amount >= 50000)
				{
					
					$level = "PRES";
					$current = $pres->id;
					$flag = 1;
					$status = "FOR APPROVAL";
					$issent = 1;
					$vp_app = $te->te_vp_approver;
					$svp_app = Session::get('employee_id');
					$pres_app = "";
					$req_completed = "";
					
					Mail::send('te.email_templates.te_for_approval', array(
						'status' => $status,
						'ref_num' => $te->te_ref_num,
						'requestor' => $te->te_emp_name,
						'department' => $te->te_department),
						function($message){
							$message->to($this->getpres()->email, $this->getpres()->firstname." ".$this->getpres()->lastname)->subject('RGTS Notification: TE Request for Approval');
						}
					);
				}
				else{
					$level = "REQUESTOR";
					$status = "APPROVED";
					$current = $request['te_employees_id'];
					$flag = 2;
					$issent = 1;
					$vp_app = $te->te_vp_approver;
					$svp_app = Session::get('employee_id');
					$req_completed = date('Y-m-d');
					$pres_app = "";

					Mail::send('te.email_templates.te_approved', array(
						'status' => $status,
						'ref_num' => $te->te_ref_num,
						'requestor' => $te->te_emp_name,
						'department' => $te->te_department),
						
						function($message){
								$message->to($this->getfiler()->email, $this->getfiler()->firstname." ".$this->getfiler()->lastname)->subject('RGTS Notification: TE Request Approved');
						}
					);
					
				}
			}
			
			elseif($te->te_level == "PRES" && $te->te_current == Session::get('employee_id'))
			{
				$this->te = TrainingEndorsements::find($input['te_id']);
				
				$level = "REQUESTOR";
				$status = "APPROVED";
				$current = $request['te_employees_id'];
				$flag = 2;
				$issent = 1;
				$vp_app = $te->te_vp_approver;
				$svp_app = $te->te_svp_approver;
				$pres_app = Session::get('employee_id');
				$req_completed = date('Y-m-d');
				
				Mail::send('te.email_templates.te_approved', array(
					'status' => $status,
					'ref_num' => $te->te_ref_num,
					'requestor' => $te->te_emp_name,
						'department' => $te->te_department),
					
					function($message){
							$message->to($this->getfiler()->email, $this->getfiler()->firstname." ".$this->getfiler()->lastname)->subject('RGTS Notification: TE Request Approved');
					}
				);
				
			}
		}
		
		elseif($input['action'] == 'disapprove')
		{	
			$te = TrainingEndorsements::find($input['te_id']);
			
			$status = "DISAPPROVED";
			$issent = 0;
			$current = $request['te_assessed_by'];
			$level = "CHRD-TOD";
			$flag = 0;
			
			$vp_app = "";
			$svp_app = "";
			$pres_app = "";
			$req_completed = "";
						
			$this->te = TrainingEndorsements::find($te_id);
			$assesstor = $this->te['te_assessed_by'];
			$emp = Employees::find($assesstor);
			
			Mail::send('te.email_templates.te_disapproved', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$assesstor = $this->te['te_assessed_by'];
					$emp = Employees::find($assesstor);
					$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGTS Notification: TE Request Disapproved');
				}
			);
		}
	
		$te = TrainingEndorsements::find($input['te_id']);
		$te->te_level = $level;
		$te->te_current = $current;
		$te->te_flag = $flag;
		$te->te_status = $status;
		$te->te_issent = $issent;
		$comments = (array) json_decode($te->te_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>Input::get('comment')
		);
		$te->te_comments = json_encode($comments);
		$te->te_reviewed_by = $vpforchr->id;
		$te->te_vp_approver = $vp_app;
		$te->te_svp_approver = $svp_app;
		$te->te_pres_approver = $pres_app;
		$te->te_date_request_completed = $req_completed;
		$te->save();
		
		$new = '"'.$te->te_level.'","'.$te->te_current.'","'.$te->te_flag.'","'.$te->te_status.'","'.$te->te_issent.'"';
		
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU004($te->te_id, $old, $new, $te->te_ref_num);
	}
	
	//CHRD Assessment
	public function assess($te_id)
	{
		$input = Input::all();
		$te = new TERepository;
		$request = $te->getRequest($te_id);
		$vpforchr = $te->getVPforCHR();
		
		$te = TrainingEndorsements::find($te_id);
		$old = '"'.$te->te_status.'","'.$te->te_issent.'","'.$te->te_current.'","'.$te->te_level.'","'.$te->te_assessed_by.'","'.$te->te_flag.'"';

		if($input['action'] == 'save')
		{
			$current = Session::get('employee_id');
			$status = "FOR ASSESSMENT";
			$level = "CHRD-TOD";
			$issent = 1;
			$flag = ''; 
			$dcompleted = '';
		}
		
		elseif($input['action'] == 'send')
		{
			$status = "FOR APPROVAL";
			$issent = 1;
			$current = $vpforchr->id;
			$level = "VPforCHR";
			$flag = 0;
			$dcompleted = date('Y-m-d');
		}
		
		elseif($input['action'] == 'disapprove')
		{
			
			$status = "DISAPPROVED";
			$issent = 0;
			$current = $request['te_employees_id'];
			$level = "REQUESTOR";
			$flag = 0;
			$dcompleted = '';
		}

		$te->teq_target_performance = Input::get('teq_target_performance');
		$te->teq_target_competency = Input::get('target_competency');
		$te->teq_newfunction_description = Input::get('newfunction_description');
		$te->teq_reg_emp = Input::get('regular_employee');
		$te->teq_submitted_req = Input::get('submit_req');
		$te->teq_new_seminar = Input::get('new_seminar');
		$te->teq_idp = Input::get('idp');
		$te->teq_tga = Input::get('tga');
		$te->teq_target_2_higher = Input::get('proficiency_level');
		$te->teq_result_1_higher = Input::get('gap_result');
		$te->teq_required = Input::get('required');
		$te->teq_overall_recom = Input::get('overall_recom');
		$te->teq_eta = Input::get('teq_eta');
		$te->te_date_completed = $dcompleted;
		$comments = (array) json_decode($te->te_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>Input::get('comment')
		);
		$te->te_comments = json_encode($comments);
		$te->te_current = $current;
		$te->te_issent = $issent;
		$te->te_level = $level;
		$te->te_status = $status;
		$te->te_assessed_by = Session::get('employee_id');
		$te->te_flag = $flag;
		$te->save();
		
		$new = '"'.$te->te_status.'","'.$te->te_issent.'","'.$te->te_current.'","'.$te->te_level.'","'.$te->te_assessed_by.'","'.$te->te_flag.'"';
		
		if($issent == 1){
			Mail::send('te.email_templates.te_for_approval', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$message->to($this->getvp()->email, $this->getvp()->firstname." ".$this->getvp()->lastname)->subject('RGTS Notification: TE Request for Approval');
				}
			);
		}
		
		else{
			$this->te = TrainingEndorsements::find($input['te_id']);

			Mail::send('te.email_templates.te_disapproved', array(
				'status' => $status,
				'ref_num' => $te->te_ref_num,
				'requestor' => $te->te_emp_name,
				'department' => $te->te_department),
				function($message){
					$emp = Employees::find($this->te['te_employees_id']);
					$message->to($emp->email, $this->te->te_emp_name)->subject('RGTS Notification: TE Request Disapproved');
				}
			);
		}
		
		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU004($te->te_id, $old, $new, $te->te_ref_num);
	}
	
	public function deleteRequest($te_id)
	{
		$te = TrainingEndorsements::where('te_id','=',$te_id)->first();
		$old_status = $te->te_status;
		
		$te->te_status = $te->te_status."/DELETED";	
		$te->te_isdeleted = 1;
		$te->save();
		
		$new_status = $te->te_status;

		$this->setTable('trainingendorsements');
		$this->setPrimaryKey('te_id');
		$this->AU005($te->te_id, $old_status, $new_status, $te->te_ref_num);
		

	}
}
?>