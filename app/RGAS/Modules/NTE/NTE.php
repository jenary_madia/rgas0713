<?php namespace RGAS\Modules\NTE;

use NoticeToExplain;
use CrrBuildUp;
use CrrBuildUpDetail;
use NteRefs;
use Ntem;
use NteAttachments;
use NteComments;
use NteDrafts;
use NteDraftsDepartments;
use NteDraftsEmployees;
use NteDraftsInfractions;
use NteDraftsAttachments;

use AuditTrails;
use Session;
use RGAS\Libraries;

use Carbon\Carbon;
use Validator;
use Input;
use Redirect;
use Request;
use DB;
use Mail;
use NTEController;
use Config;

class NTE extends NTEController
{
    const FIELD_NAMES = [
    	'rule_name' => 'RULE NAME',
    	'rule_number' => 'RULE NUMBER',
    	'rule_description' => 'RULE DESCRIPTION',
    	'year' => 'YEAR',
    	'month' => 'MONTH',
    	'explanation' => 'EXPLANATION',
    	'head_dec' => 'DEPARTMENT HEAD\'S DECISION',
    	'head_act' => 'DISCIPLINARY ACTION',
    	'sus_from' => 'SUSPENSION FROM',
    	'sus_to' => 'SUSPENSION TO',
    	'sus_no_days' => 'SUSPENSION NO OF DAYS',
    	'remarks' => 'COMMENT',
    	'return_to' => 'RETURN TO'
    ];

    const VALIDATION_MESSAGES = [
    	'required' => 'ERROR! PLEASE ACCOMPLISH :attribute.'
    ];

	public function _generateObjectError($objectErrors)
	{
		$return = [];

		foreach ($objectErrors as $oError) {
			foreach ($oError as $message) {
				$return[] = $message;
			}
		}

		return $return;
	}

	public function _generateArrayError($arrayErrors)
	{
		$return = [];

		foreach ($arrayErrors as $aError) {
			foreach ($aError as $message) {
				$return[] = $message;
			}
		}

		return $return;
	}

	public function _getMessageAndStatus($array)
	{
		$data = [];
		switch ($array[0]) {
			case 2: 
				$data['status'] = 1;
				$data['msg'] = NTEMessages::_messages('nte.save');
				break;
			case 3:
				$data['status'] = TYPE == 5 ? 4 : 2;
				break;
			case 4:
				$data['status'] = 3;
				$data['msg'] = NTEMessages::_messages('nte.return.filer');
				break;
			case 5: 
				$data['status'] = 4;
				$data['msg'] = NTEMessages::_messages('nte.send.personnel.accomplishment');
				break;
			case 6:
				$data['status'] = 3;
				$data['msg'] = NTEMessages::_messages('nte.save');
				break;
			case 7:
				$data['status'] = 2;
				$data['msg'] = NTEMessages::_messages('nte.send');
				break;
			case 8:
				$data['status'] = 5;
				$data['msg'] = NTEMessages::_messages('nte.save');
				break;
			case 9:
				$data['status'] = 6;
				$data['msg'] = NTEMessages::_messages('nte.send.head.review');
				break;
			case 10:
				$data['status'] = 5;
				$data['msg'] = NTEMessages::_messages('nte.return.employee.revision');
				break;
			case 11:
				$data['status'] = 7;
				$data['msg'] = NTEMessages::_messages('nte.send.acknowledgement');
				break;
			case 12:
				$data['status'] = 6;
				$data['msg'] = NTEMessages::_messages('nte.return.department.head');
				break;
			case 13:
				$data['status'] = 8;
				$data['msg'] = NTEMessages::_messages('nte.acknowledged');
				break;
			case 14:
				$data['status'] = 9;
				$data['msg'] = NTEMessages::_messages('nda.save');
				break;
			case 15:
				$data['status'] = 10;
				$data['msg'] = NTEMessages::_messages('nda.send.manager');
				break;
			case 16:
				$data['status'] = 11;
				$data['msg'] = NTEMessages::_messages('nda.return.filer');
				break;
			case 17:
				$data['status'] = 12;
				$data['msg'] = NTEMessages::_messages('nda.send.chrdhead');
				break;
			case 18:
				$data['status'] = 10;
				$data['msg'] = NTEMessages::_messages('nda.return.manager');
				break;
			case 19:
				$data['status'] = 13;
				$data['msg'] = NTEMessages::_messages('nda.send.department.head');
				break;
			case 20:
				$data['status'] = 12;
				$data['msg'] = NTEMessages::_messages('nda.return.chrd.head');
				break;
			case 21:
				$data['status'] = 14;
				$data['msg'] = NTEMessages::_messages('nda.send.employee');
				break;
			case 22:
				$data['status'] = 15;
				$data['msg'] = NTEMessages::_messages('nda.acknowledged');
				break;
			case 23:
				$data['status'] = 16;
				$data['msg'] = NTEMessages::_messages('nda.cbrm');
				break;
			case 24:
			case 25:
				$data['status'] = 17;
				break;

			default:
				# code...
				break;
		}

		return $data;
	}

	public function _generateRef($module)
	{
		$y = date("Y");
		$gr = new NteRefs();
		$ln = $gr->where(['c' => $module])->get()->first();

		if (empty($ln)) 
		{
			$gr->c = $module;
			$gr->y = $y;
			$gr->n = 1;
			$gr->save();
		} 
		else 
		{
			if ($ln->y != $y) 
			{
				$gr->where('id', $ln->id)->update(array('y' => $y , 'n' => 1));
			} 
			else 
			{
				$gr->where('id', $ln->id)->update(array('n' => $ln->n+1));	
			}
		}

		$ln = $gr->where(['c' => $module])->get()->first();
		$count = strlen($ln->n);

		switch ($count) 
		{
			case '1':
				$n = '0000'.$ln->n;
				break;
			case '2':
				$n = '000'.$ln->n;
				break;
			case '3':
				$n = '00'.$ln->n;
				break;
			case '4':
				$n = '0'.$ln->n;
				break;
			case '5':
				$n = $ln->n;
				break;			
			default:
				break;
		}
		return $y.'-'.$n;
	}

	public function _multipleRowsValidation($array, $rules, $message) 
	{
		$validates = [];
		for ($j=0; $j < count($array); $j++) 
		{ 
			$validator = Validator::make($array, [
				$j => $rules
			]);
			$validator->fails() ? $validates[] = TRUE : $validates[] = FALSE;
		}
		
		$validates = array_filter($validates);
		return count($validates) > 0 ? [$message] : [];
	}

	public function _multipleRowsValidationSameValuesMultipleArray($section_no, $section_status)
	{
		$return = [];
		$intersect = [];
		$duplicate = '';

		for ($i=0; $i < count($section_no); $i++) 
		{ 
			$toAdd = FALSE;
			for ($j=0; $j < count($section_no); $j++) 
			{ 
				if ($section_no[$j] === $section_no[$i] && $section_status[$j] === $section_status[$i] && $section_no[$j] != '') 
				{
					if ($toAdd) 
					{
						$intersect[] = $section_no[$i];
						unset($section_no[$i]);
						$section_no = array_values($section_no);
						unset($section_status[$i]);
						$section_status = array_values($section_status);
					}
					$toAdd = TRUE;
				}
			}
		}

		if (count($intersect) > 0) 
		{
			for ($k=0; $k < count($intersect); $k++) 
			{ 
				if ($k == 0) 
					$duplicate .= $intersect[$k];
				else
					$duplicate .= ', '.$intersect[$k];
			}

			$return = [NTEMessages::_messages('crr.row.error', $duplicate)];
		}

		return $return;
	}

	public function _multipleRowsValidationSameValues($array) 
	{
		$return = [];
		$arr_unique = array_unique($array);
		$arr_duplicates = array_diff_assoc($array, $arr_unique);
		$arr_duplicates = array_values($arr_duplicates);

		foreach ($arr_duplicates as $duplicate) {
			$return = [NTEMessages::_messages('crr.row.error', $duplicate)];
		}

		return $return;
	}

	public function _pushComments($nte_id, $comment, $module, $override = false)
	{
		$ncomments = NteComments::where(['nte_id' => $nte_id, 'module' => $module])->orderBy('date_time_posted', 'desc')->get();
		$ntecomments = count($ncomments) > 0 ? $ncomments[0]['commentor_id'] == Session::get('employee_id') && !$override ? $ncomments[0] : new NteComments() : new NteComments();
		$ntecomments->nte_id = $nte_id;
		$ntecomments->module = $module;
		$ntecomments->commentor_id = Session::get('employee_id');
		$ntecomments->comment = $comment;
		$ntecomments->save();
	}

	public function _pushAttachments($nte_id, $file, $module, $type, $ref_num)
	{
		$nteAttachment = new NteAttachments();
		$nteAttachment->nte_id = $nte_id;
		$nteAttachment->module = $module;
		$nteAttachment->type = $type;
		$nteAttachment->filesize = $file['filesize'];
		$nteAttachment->mime_type = $file['mime_type'];
		$nteAttachment->original_extension = $file['original_extension'];
		$nteAttachment->original_filename = $file['original_filename'];
		$nteAttachment->random_filename = $file['random_filename'];
		$nteAttachment->save();

		$new = [
			'filesize' => $file['filesize'],
			'mime_type' => $file['mime_type'],
			'original_extension' => $file['original_extension'],
			'original_filename' => $file['original_filename'],
			'random_filename' => $file['random_filename']
		];

		$this->_pushAuditTrail(MODULE_ID, 'AU002', 'nte', ['nte.nte_id' => $nte_id], '', $new, $ref_num);
	}

	public function _push()
	{
		$sData = Input::all();

		$data = '';

		/*echo "<pre>";
		print_r($sData);*/

		switch ($sData['action']) 
		{
			case 1: // CRR -> SAVE
				$data = $this->_pushCrr($sData);
				break;
			case 2: // NTE CREATE -> SAVE
			case 3: // NTE CREATE -> SEND
				$data = $this->_pushNte($sData);
				break;
			case 4: // NTE APPROVAL -> RETURN
			case 5: // NTE APPROVAL -> SEND
				$data = $this->_pushNteReturnAndApprove($sData);
				break;
			case 6: // NTE RETURNED -> SAVE
			case 7: // NTE REUTRNED -> SEND
				$data = $this->_pushNteReturnedToSend($sData);
				break;
			case 8: // NTE EMPLOYEE -> SAVE
			case 9: // NTE EMPLOYEE -> SEND
				$data = $this->_pushNteEmployeeToSend($sData);
				break;
			case 10: // NTE DEPARTMENT -> RETURN
			case 11: // NTE DEPARTMENT -> SEND
				$data = $this->_pushNteDepartmentToSend($sData);
				break;
			case 12: // NTE ACKNOWLEDGEMENT -> RETURN
			case 13: // NTE ACKNOWLEDGEMENT -> SEND
				$data = $this->_pushNteAcknowledgementToSend($sData);
				break;
			case 14: // NDA ACCOMPLISHMENT -> SAVE
			case 15: // NDA ACCOMPLISHMENT -> SEND
				$data = $this->_pushNda('manager', $sData);
				break;
			case 16: // NDA NOTATION MANAGER -> RETURN
			case 17: // NDA NOTATION MANAGER -> SEND
				$data = $this->_pushNda('head', $sData);
				break;
			case 18: // NDA NOTATION HEAD -> RETURN
			case 19: // NDA NOTATION HEAD -> SEND
				$data = $this->_pushNda('department', $sData);
				break;
			case 20: // NDA NOTATION DHEAD -> RETURN
			case 21: // NDA NOTATION DHEAD -> SEND
				$data = $this->_pushNda('employee', $sData);
				break;
			case 22: // NDA ACKNOWLEDGEMENT -> ACKNOWLDGE
				$data = $this->_pushNda('acknowledge', $sData);
				break;
			case 23: // NDA ACKNOWLEDGED -> SEND
				$data = $this->_pushNda('cbrm', $sData);
				break;
			case 24: // NTE CANCEL
				$data = $this->_pushCancel('nte', $sData);
				break;
			case 25: // NDA CANCEL
				$data = $this->_pushCancel('nda', $sData);

			default:
				# code...
				break;
		}

		return $data;
	}

	public function _pushCrr($array)
	{
		$return = [];

		$crrHeaderId = '';

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$headerValidate = validator::make($array, [
			'rule_name' => 'required|max:20',
    	    'rule_number' => 'required|numeric|max:99999',
        	'rule_description' => 'required|max:1000'
		], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

		$headerValidate->fails() ? $objectErrors[] = $headerValidate->messages()->all() : '';

		$ruleDuplicate = validator::make($array, [
			//'rule_name' => 'unique:crrbuildup,cb_rule_name,NULL,cb_crr_id,cb_rule_status,1,cb_rule_number,'.$array['rule_number']
			'rule_name' => 'unique:crrbuildup,cb_rule_name,NULL,cb_crr_id,cb_rule_status,1'
		], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

		$ruleDuplicate->fails() && $array['hId'] == "" ? $arrayErrors[] = [NTEMessages::_messages('crr.row.error', $array['rule_name'])] : '';

		//$arrayErrors[] = $this->_multipleRowsValidation($array['section_no'], 'required', 'Section No. is required');
		$arrayErrors[] = $this->_multipleRowsValidation($array['section_no'], 'numeric', 'Section No. must be numeric');
		$arrayErrors[] = $this->_multipleRowsValidation($array['section_no'], 'between:0,99999.99', 'Section No. may only contain value between 0-9999.99');
		//$arrayErrors[] = $this->_multipleRowsValidation($array['section_desc'], 'required', 'Section Description is required');
		$arrayErrors[] = $this->_multipleRowsValidation($array['section_desc'], 'max:1000', 'Section Description may only contain 1000 characters');

		$arrayErrors[] = $this->_multipleRowsValidationSameValuesMultipleArray($array['section_no'], $array['section_status']);
		//$arrayErrors[] = $this->_multipleRowsValidationSameValues($array['section_no']);

		$arrayErrors = array_filter($arrayErrors);

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));
		
		DB::beginTransaction();

		if (count($return['msg']) == 0) 
		{
			try 
			{
				if ($array['hId'] != "") 
				{
					$CrrBuildUpHeader = CrrBuildUp::where('cb_crr_id', $array['hId'])->first();

					$pk = ['crrbuildup.cb_crr_id' => $array['hId']];
					$old = ['cb_rule_status' => $CrrBuildUpHeader->cb_rule_status];
					$new = ['cb_rule_status' => $array['rule_status']];

					$this->_pushAuditTrail(MODULE_ID, 'AU004', 'crrbuildup', $pk, $old, $new, '');
					$return['msg'] = [NTEMessages::_messages('crr.edit')];
				} 
				else 
				{
					$CrrBuildUpHeader = new CrrBuildUp();
					$CrrBuildUpHeader->cb_rule_name = $array['rule_name'];
					$CrrBuildUpHeader->cb_rule_number = $array['rule_number'];
					$CrrBuildUpHeader->cb_rule_description = $array['rule_description'];

					$CrrBuildUpHeader->rule_number = 'RULE ' . $array['rule_number'];

					$pk = ['crrbuildup.cb_crr_id' => $crrHeaderId];
					$new = ['cb_rule_name' => $array['rule_name'], 
							'cb_rule_number' => $array['rule_number'], 
							'cb_rule_description' => $array['rule_description'], 
							'cb_rule_status' => $array['rule_status']];

					$this->_pushAuditTrail(MODULE_ID, 'AU001', 'crrbuildup', $pk, '', $new, '');

					$return['msg'] = [NTEMessages::_messages('crr.add')];
				}

				$CrrBuildUpHeader->cb_rule_status = $array['rule_status'];
				$CrrBuildUpHeader->save();

				$crrHeaderId = $CrrBuildUpHeader->cb_crr_id;
				
				for ( $i=0; $i < count($array['crr_id']); $i++ ) 
				{
					if ($array['crr_id'][$i] != "") 
					{
						$CrrBuildUpDetails = CrrBuildUpDetail::where('cbd_detail_id', $array['crr_id'][$i])->first();

						$pk = ['crrbuildupdetail.cbd_detail_id' => $array['crr_id'][$i]];
						$old = ['cbd_section_status' => $CrrBuildUpDetails->cbd_section_status];
						$new = ['cbd_section_status' => $array['section_status'][$i]];

						$this->_pushAuditTrail(MODULE_ID, 'AU004', 'crrbuildupdetails', $pk, $old, $new, '');
					} 
					else 
					{
						$CrrBuildUpDetails = new CrrBuildUpDetail();

						$CrrBuildUpDetails->cb_crr_id = $crrHeaderId;

						if (!($array['section_no'][$i] == '' && $array['section_desc'][$i] == ''))
						{
							$CrrBuildUpDetails->cbd_section_number = $array['section_no'][$i];
							$CrrBuildUpDetails->cbd_section_description = $array['section_desc'][$i];

							$CrrBuildUpDetails->section_number = 'SECTION ' . $array['section_no'][$i];

							$pk = ['crrbuildupdetail.cbd_detail_id' => $CrrBuildUpDetails->cbd_detail_id];
							$new = ['cbd_section_number' => $array['section_no'][$i], 
									'cbd_section_description' => $array['section_desc'][$i], 
									'cbd_section_status' => $array['section_status'][$i]];

							$this->_pushAuditTrail(MODULE_ID, 'AU001', 'crrbuildupdetails', $pk, '', $new, '');
						}
					}
					
					if (!($array['section_no'][$i] == '' && $array['section_desc'][$i] == ''))
					{
						$CrrBuildUpDetails->cbd_section_status = $array['section_status'][$i];
						$CrrBuildUpDetails->save();
					}
				}

				$return['scs'] = TRUE;
					
			} catch (Exception $e) {
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}
		}

		DB::Commit();
		return $return;
	}

	public function _pushNte($array)
	{
		$return = [];
		$arrayUnsetted = [];

		$sentItems = 0;
		$now = Carbon::now();

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		if ($array['action'] == 3) 
		{	
			for ($i = 0; $i < count($array['sel_emp']); $i++) 
			{ 	
				if ($array['sel_emp'][$i] == 1) 
				{
					$arrayUnsetted['sel_emp'][] = $array['sel_emp'][$i];
					$arrayUnsetted['emp_name'][] = $array['emp_name'][$i];
					$arrayUnsetted['date_comm'][] = $array['date_comm'][$i];
					$arrayUnsetted['infraction'][] = $array['infraction'][$i];
					$arrayUnsetted['tim'][] = $array['tim'][$i];
					$arrayUnsetted['rda'][] = $array['rda'][$i];
					$arrayUnsetted['ref_num'][] = $array['ref_num'][$i];
					$arrayUnsetted['remarks'][] = $array['remarks'][$i];
					
					if (isset($array['files'][$i]))
						$arrayUnsetted['files'][] = $array['files'][$i];
					else
						$arrayUnsetted['files'][] = [];
				}
			}

			$hValidate = validator::make($array, [
				'year' => 'required',
				'month' => 'required'
			], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$hValidate->fails() ? $objectErrors[] = $hValidate->messages()->all() : '';

			$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['emp_name'], 'required', 'Employee Name is required');
			$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['infraction'], 'required', 'Infraction is required');
			$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['tim'], 'required', 'Total Incurred for the month is required');
			/*$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['tim'], 'alpha_num', 'Total Inccured for the month may only contain letters and numbers');*/
			$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['rda'], 'required', 'Recommended Disciplinary Action is required');
		} 
		elseif ($array['action'] == 2) 
		{
			for ($i = 0; $i < count($array['sel_emp']); $i++) 
			{ 	
				if ($array['sel_emp'][$i] == 0) 
				{
					$arrayUnsetted['sel_emp'][] = $array['sel_emp'][$i];
					$arrayUnsetted['emp_name'][] = $array['emp_name'][$i];
					$arrayUnsetted['date_comm'][] = $array['date_comm'][$i];
					$arrayUnsetted['infraction'][] = $array['infraction'][$i];
					$arrayUnsetted['tim'][] = $array['tim'][$i];
					$arrayUnsetted['rda'][] = $array['rda'][$i];
					$arrayUnsetted['ref_num'][] = $array['ref_num'][$i];
					$arrayUnsetted['remarks'][] = $array['remarks'][$i];

					if (isset($array['files'][$i]))
						$arrayUnsetted['files'][] = $array['files'][$i];
					else
						$arrayUnsetted['files'][] = [];
				}
			}

			//$arrayErrors[] = $this->_multipleRowsValidation($arrayUnsetted['emp_name'], 'required', 'Employee Name column is required');
		}

		$arrayUnsetted['year'] = $array['year'];
		$arrayUnsetted['month'] = $array['month'];
		$arrayUnsetted['action'] = $array['action'];
		$arrayUnsetted['isFromMultiple'] = $array['isFromMultiple'];
		$arrayUnsetted['_token'] = $array['_token'];

		$arrayErrors = array_filter($arrayErrors);
		$arrayErrors = array_values($arrayErrors);

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));
		
		$array = $arrayUnsetted;

		DB::beginTransaction();

		if (count($return['msg']) == 0)
		{
			$fm = new Libraries\FileManager;

			try {

				!$array['isFromMultiple'] ? $this->_destroyExistingNteDrafts() : '';

				for ($i=0; $i < count($array['sel_emp']); $i++) 
				{
					if (!($array['emp_name'][$i] == null || $array['emp_name'][$i] == ''))
					{
						$auditOld = NULL;

						$employee = $array['emp_name'][$i] == null || $array['emp_name'][$i] == '' ? '0|0' : $array['emp_name'][$i];
						$emp_and_dep = explode('|', $employee);

						$nte_reference_number = $array['ref_num'][$i] == null ? $this->_generateRef('nte') : $array['ref_num'][$i];

						$auditNew = ['nte_department_id' => $emp_and_dep[1], 'nte_year' => $array['year'], 'nte_month' => $array['month'],
										'nte_employee_id' => $emp_and_dep[0], 'nte_filer_id' => Session::get('employee_id'), 'nte_day' => $array['date_comm'][$i],
										'cb_crr_id' => $array['infraction'][$i], 'nte_total_incurred' => $array['tim'][$i], 
										'nte_recommended_disciplinary_action' => $array['rda'][$i], 'nte_reference_number' => $nte_reference_number, 'nte_status' => $data['status']];

						if ($data['status'] == 1 && $array['sel_emp'][$i] == 0) 
						{
							$nteCurrentId = Session::get('employee_id');

							if ($array['ref_num'][$i] == null || $array['ref_num'][$i] == '')
							{
								$auditCode = 'AU001';
								$ntem = new Ntem();	
							}
							else
							{
								$auditCode = 'AU004';
								$ntem = Ntem::where(['nte_reference_number' => $array['ref_num'][$i], 'is_deleted' => 0])->first();
								$auditOld = ['nte_department_id' => $ntem->nte_department_id, 'nte_year' => $ntem->nte_year, 'nte_month' => $ntem->nte_month,
										'nte_employee_id' => $ntem->nte_employee_id, 'nte_filer_id' => $ntem->nte_filer_id, 'nte_day' => $ntem->nte_day,
										'cb_crr_id' => $ntem->cb_crr_id, 'nte_total_incurred' => $ntem->nte_total_incurred, 
										'nte_recommended_disciplinary_action' => $ntem->nte_recommended_disciplinary_action, 
										'nte_reference_number' => $ntem->nte_reference_number, 'nte_status' => $ntem->nte_status, 'nte_current_id' => $ntem->nte_current_id];
							}

							$auditNew['nte_current_id'] = Session::get('employee_id');
							$return['msg'] = [$data['msg']];
						}
						elseif (($data['status'] == 2 || $data['status'] == 4) && $array['sel_emp'][$i] == 1) 
						{
							$sentItems = $sentItems + 1;

							$nteCurrentId = TYPE == 5 ? $emp_and_dep[0] : $this->_pullNteApproverId(Session::get('employee_id'));

							if ($array['ref_num'][$i] == null || $array['ref_num'][$i] == '')
							{
								$auditCode = 'AU001';
								$ntem = new Ntem();
							} 
							else 
							{
								$auditCode = 'AU004';
								$auditNew['nte_submitted_chrd_aer'] = $now;
								$ntem = Ntem::where(['nte_reference_number' => $array['ref_num'][$i], 'is_deleted' => 0])->first();
								$auditOld = ['nte_department_id' => $ntem->nte_department_id, 'nte_year' => $ntem->nte_year, 'nte_month' => $ntem->nte_month,
										'nte_employee_id' => $ntem->nte_employee_id, 'nte_filer_id' => $ntem->nte_filer_id, 'nte_day' => $ntem->nte_day,
										'cb_crr_id' => $ntem->cb_crr_id, 'nte_total_incurred' => $ntem->nte_total_incurred, 
										'nte_recommended_disciplinary_action' => $ntem->nte_recommended_disciplinary_action, 
										'nte_reference_number' => $ntem->nte_reference_number, 'nte_status' => $ntem->nte_status, 'nte_current_id' => $ntem->nte_current_id];
							}

							$ntem->nte_submitted_chrd_aer = $now;
							$auditNew['nte_current_id'] = $nteCurrentId;

							$return['msg'] = TYPE == 5 ? [NTEMessages::_messages('nte.send.personnel.accomplishment')] : [NTEMessages::_messages('nte.send.notation', $sentItems)];
						}
						elseif ($data['status'] == 2 && $array['sel_emp'][$i] == 0) 
						{
							$return['scs'] = FALSE;
							$return['msg'] = [NTEMessages::_messages('i.error')];

							return $return;
						}

						$ntem->nte_department_id = $emp_and_dep[1];
						$ntem->nte_year = $array['year'];
						$ntem->nte_month = $array['month'];
						$ntem->nte_employee_id = $emp_and_dep[0];
						$ntem->nte_filer_id = Session::get('employee_id');
						$ntem->nte_day = $array['date_comm'][$i];
						$ntem->cb_crr_id = $array['infraction'][$i];
						$ntem->nte_total_incurred = $array['tim'][$i];
						$ntem->nte_recommended_disciplinary_action = $array['rda'][$i];
						$ntem->nte_reference_number = $nte_reference_number;
						$ntem->nte_status = $data['status'];
						$ntem->nte_current_id = $nteCurrentId;
						$ntem->save();	

						$pk = ['nte.nte_id' => $ntem->nte_id];

						$this->_pushComments($ntem->nte_id, $array['remarks'][$i], 1);

						$this->_pushAuditTrail(MODULE_ID, $auditCode, 'nte', $pk, $auditOld, $auditNew, $ntem->nte_reference_number);

						NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1])->delete();

						if (isset($array['files'])) 
						{
							if(isset($array['files'][$i]))
							{
								foreach($array['files'][$i] as $file)
								{
									$this->_pushAttachments($ntem->nte_id, $file, 1, 1, $ntem->nte_reference_number);
									
									$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
									$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
									$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : $fm->moveFile($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number, Config::get('rgas.rgas_storage_path').'/nte_drafts/'.USERID.'/');
								}
							}
						}
					}
				}

				$return['scs'] = TRUE;
			} 
			catch (Exception $e) 
			{
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = NTEMessages::_messages('i.error');
			}	
		} 
		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		DB::Commit();
		return $return;
	}

	public function _destroyExistingNteDrafts()
	{
		$nteDrafts = new NteDrafts();
		$nteDraftsDepartments = new NteDraftsDepartments();
		$nteDraftsEmployees = new NteDraftsEmployees();
		$nteDraftsInfractions = new NteDraftsInfractions();
		$nteDraftsAttachments = new nteDraftsAttachments();

		$exist = $nteDrafts->where(['nd_filer_id' => Session::get('employee_id')])->first();

		if (count($exist) > 0)
		{
			$nteDraftsDepartments->where('nd_id', $exist->nd_id)->delete();
			$nteDraftsEmployees->where('nd_id', $exist->nd_id)->delete();
			$nteDraftsInfractions->where('nd_id', $exist->nd_id)->delete();
			$nteDraftsAttachments->where('nd_id', $exist->nd_id)->delete();
			$nteDrafts->where(['nd_filer_id' => Session::get('employee_id'), 'nd_id' => $exist->nd_id])->delete();
		}

		return;
	}

	public function _pushNteDraft()
	{
		$inputs = Input::all();

		DB::beginTransaction();

		$fm = new Libraries\FileManager;

		$this->_destroyExistingNteDrafts();

		try 
		{
			$nteDrafts = new NteDrafts();
			$nteDrafts->nd_filer_id = Session::get('employee_id');
			$nteDrafts->nd_year = $inputs['year_dd'];
			$nteDrafts->nd_month = $inputs['month_dd'];
			$nteDrafts->save();

			if (isset($inputs['department']))
			{
				foreach ($inputs['department'] as $dept) 
				{
					$nteDraftsDepartments = new NteDraftsDepartments();
					$nteDraftsDepartments->nd_id = $nteDrafts->nd_id;
					$nteDraftsDepartments->ndd_department_id = $dept;
					$nteDraftsDepartments->save();
				}
			}
			if (isset($inputs['employee'])) 
			{
				foreach ($inputs['employee'] as $emp) 
				{
					$nteDraftsEmployees = new NteDraftsEmployees();
					$nteDraftsEmployees->nd_id = $nteDrafts->nd_id;
					$nteDraftsEmployees->nde_employee_id = $emp;
					$nteDraftsEmployees->save();
				}
			}

			if (isset($inputs['infraction']))
			{
				foreach ($inputs['infraction'] as $crr) 
				{
					$nteDraftsInfractions = new NteDraftsInfractions();
					$nteDraftsInfractions->nd_id = $nteDrafts->nd_id;
					$nteDraftsInfractions->cb_crr_id = $crr;
					$nteDraftsInfractions->save();
				}
			}
			
			if(isset($inputs['multi'])) 
			{
				foreach ($inputs['multi'] as $attachment) 
				{
					$nteDraftsAttachments = new nteDraftsAttachments();
					$nteDraftsAttachments->nd_id = $nteDrafts->nd_id;
					$nteDraftsAttachments->filesize = $attachment['filesize'];
					$nteDraftsAttachments->mime_type = $attachment['mime_type'];
					$nteDraftsAttachments->original_extension = $attachment['original_extension'];
					$nteDraftsAttachments->original_filename = $attachment['original_filename'];
					$nteDraftsAttachments->random_filename = $attachment['random_filename'];
					$nteDraftsAttachments->save();			
					/*$hasFile = isset($attachment['random_filename']) ? TRUE : FALSE;
					$fileExist = $this->getStoragePath('nte_drafts').Session::get('employee_id').$attachment['random_filename'] ? TRUE : FALSE;
					$hasFile && $fileExist ? $fm->move($attachment['random_filename'], $this->getStoragePath('nte_drafts').Session::get('employee_id')) : '';*/
				}
			}
		} 
		catch (Exception $e) 
		{
			DB::rollback();
		}

		DB::commit();
	}

	public function _pushNteReturnAndApprove($array) 
	{
		$return = [];
		$arrayUnsetted = [];

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$data = $this->_getMessageAndStatus([$array['action']]);
		
		$array_count = count($array['sel_emp']);

		for ($j=0; $j < $array_count; $j++) 
		{ 
			if ($array['sel_emp'][$j] == 1) 
			{
				$arrayUnsetted['sel_emp'][] = $array['sel_emp'][$j];
				$arrayUnsetted['ref_num'][] = $array['ref_num'][$j];
				$arrayUnsetted['remarks'][] = $array['remarks'][$j];
				if (isset($array['files'][$j]))
					$arrayUnsetted['files'][] = $array['files'][$j];
			}
		}

		$arrayUnsetted['action'] = $array['action'];
		$arrayUnsetted['_token'] = $array['_token'];

		$array = $arrayUnsetted;

		if ($array['action'] == 4)
		{
			$arrayErrors[] = $this->_multipleRowsValidation($array['remarks'], 'required', 'Remarks column is required');

			$arrayErrors = array_filter($arrayErrors);
			$arrayErrors = array_values($arrayErrors);

			$nteCurrentId = $this->_pullNteFilerId(Session::get('employee_id'));
		}

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		if (count($return['msg']) == 0) 
		{
			$fm = new Libraries\FileManager;

			DB::beginTransaction();

			try 
			{
				for ($i=0; $i < count($array['sel_emp']); $i++) 
				{
					if ($array['sel_emp'][$i] == 1) 
					{
						if ($array['action'] == 5)
							$nteCurrentId = Ntem::select('nte_employee_id')->where(['nte_id' => $array['ref_num'][$i], 'is_deleted' => 0])->first()['nte_employee_id'];

						$ntem = Ntem::where(['nte_id' => $array['ref_num'][$i], 'is_deleted' => 0])->first();

						$pk = ['nte.nte_id' => $array['ref_num'][$i]];
						$old = ['nte_status' => $ntem->nte_status, 'nte_current_id' => $ntem->nte_current_id];
						$new = ['nte_status' => $data['status']];

						$ntem->nte_status = $data['status'];
						$ntem->nte_current_id = $nteCurrentId;
						$ntem->save();

						$nte_reference_number = $ntem->nte_reference_number;

						$this->_pushComments($array['ref_num'][$i], $array['remarks'][$i], 1);

						$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nte', $pk, $old, $new, $ntem->nte_reference_number);

						NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1, 'type' => 1])->delete();

						if (isset($array['files'])) 
						{
							if(isset($array['files'][$i]))
							{
								foreach($array['files'][$i] as $file){
									$this->_pushAttachments($ntem->nte_id, $file, 1, 1, $ntem->nte_reference_number);
									
									$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
									$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
									$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : '';
								}
							}
						}

						if ($array['action'] == 4)
							$this->_pushMailNteReturned($array['ref_num'][$i]);
						elseif ($array['action'] == 5)
							$this->_pushMailNteAccomplishment($array['ref_num'][$i]);

						$return['scs'] = TRUE;
						$return['msg'] = [$data['msg']];
					}
				}
			} catch (Exception $e) {
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}

			DB::Commit();
		}

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

	public function _pushNteReturnedToSend($array) 
	{
		$return = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		DB::beginTransaction();

		$fm = new Libraries\FileManager;

		try 
		{
			if($array['action'] == 6)
			{
				$nteCurrentId = Session::get('employee_id');
			}
			elseif ($array['action'] == 7) 
			{
				$nteCurrentId = $this->_pullNteApproverId(Session::get('employee_id'));
			}

			$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();

			$pk = ['nte.nte_id' => $array['ref_num']];
			$old = ['nte_day' => $ntem->nte_day, 'nte_month' => $ntem->nte_month, 'nte_year' => $ntem->nte_year, 'cb_crr_id' => $ntem->cb_crr_id, 'nte_total_incurred' => $ntem->nte_total_incurred, 'nte_recommended_disciplinary_action' => $ntem->nte_recommended_disciplinary_action, 'nte_status' => $ntem->status, 'nte_current_id' => $ntem->nte_current_id];
			$new = ['nte_day' => $array['day'], 'nte_month' => $array['month'], 'nte_year' => $array['year'], 'cb_crr_id' => $array['infraction'], 'nte_total_incurred' => $array['tim'], 'nte_recommended_disciplinary_action' => $array['rda'], 'nte_status' => $data['status']];

			$ntem->nte_day = $array['day'];
			$ntem->nte_month = $array['month'];
			$ntem->nte_year = $array['year'];
			$ntem->cb_crr_id = $array['infraction'];
			$ntem->nte_total_incurred = $array['tim'];
			$ntem->nte_recommended_disciplinary_action = $array['rda'];
			$ntem->nte_status = $data['status'];
			$ntem->nte_current_id = $nteCurrentId;
			$ntem->save();

			$nte_reference_number = $ntem->nte_reference_number;

			$this->_pushComments($array['ref_num'], $array['remarks'], 1);

			$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nte', $pk, $old, $new, $ntem->nte_reference_number);

			NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1, 'type' => 1])->delete();

			if (isset($array['files'])) 
			{
				foreach($array['files'] as $file){
					$this->_pushAttachments($ntem->nte_id, $file, 1, 1, $ntem->nte_reference_number);
					
					$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
					$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
					$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : '';
				}
			}

			$return['scs'] = TRUE;
			$return['msg'] = [$data['msg']];
		} 
		catch (Exception $e) 
		{
			DB::rollback();
			$return['scs'] = FALSE;
			$return['msg'] = [NTEMessages::_messages('i.error')];
		}

		DB::commit();

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

	public function _pushNteEmployeeToSend($array) 
	{
		$return = [];
		
		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		if ($array['action'] == 9)
		{
			$validator = validator::make($array, [
				'explanation' => 'required|max:5000'
			], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$validator->fails() ? $objectErrors[] = $validator->messages()->all() : '';
		}

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		if (count($return['msg']) == 0) 
		{
			$fm = new Libraries\FileManager;

			DB::beginTransaction();

			try 
			{
				$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();

				if($array['action'] == 8)
				{
					$nteCurrentId = Session::get('employee_id');
				}
				elseif ($array['action'] == 9) 
				{
					$nteCurrentId = $this->_pullDepartmentHead($ntem->nte_department_id);
				}

				$pk = ['nte.nte_id' => $array['ref_num']];
				$old = ['nte_status' => $ntem->nte_status, 'nte_explanation' => $ntem->nte_explanation, 'nte_current_id' => $ntem->nte_current_id];
				$new = ['nte_status' => $data['status'], 'nte_explanation' => $array['explanation']];

				$ntem->nte_status = $data['status'];
				$ntem->nte_explanation = $array['explanation'];
				$ntem->nte_current_id = $nteCurrentId;
				$ntem->save();

				$nte_reference_number = $ntem->nte_reference_number;

				$this->_pushComments($array['ref_num'], $array['remarks'], 1);

				$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nte', $pk, $old, $new, $ntem->nte_reference_number);

				NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1, 'type' => 2])->delete();

				if (isset($array['emp'])) 
				{
					foreach($array['emp'] as $file){
						$this->_pushAttachments($ntem->nte_id, $file, 1, 2, $ntem->nte_reference_number);
						
						$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
						$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
						$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : '';
					}
				}

				$this->_pushMailNteReview($array['ref_num']);

				$return['scs'] = TRUE;
				$return['msg'] = [$data['msg']];
			} 
			catch (Exception $e) 
			{
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}

			DB::commit();
		}

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

	public function _pushNteDepartmentToSend($array) 
	{
		$return = [];

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		if ($array['action'] == 11)
		{
			$vHD = validator::make($array, [
				'head_dec' => 'required'
			], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$vHD->fails() ? $objectErrors[] = $vHD->messages()->all() : '';

			if ($array['head_dec'] == 2) 
			{
				$vHA = validator::make($array, [
					'head_act' => 'required'
				], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

				if ($array['head_act'] == 4) 
				{
					$vSUS = validator::make($array, [
						'sus_from' => 'required',
						'sus_to' => 'required',
						'sus_no_days' => 'required|numeric'
					], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

					$dateFrom = date_create($array['sus_from']);
					$dateTo = date_create($array['sus_to']);

					$diff = date_diff($dateFrom, $dateTo)->format("%R%a days");

					if ($vSUS->fails()) 
					{
						$objectErrors[] = $vSUS->messages()->all();
					}
					else
					{
						($diff < 0) || (($diff + 1) < $array['sus_no_days']) || ($array['sus_no_days'] < 1) ? $arrayErrors[] = ['No. OF DAYS CANNOT EXCEED TOTAL DAYS IN FROM AND TO FIELDS.'] : '';
					}
				} 
				else 
				{
					$array['sus_from'] = NULL;
					$array['sus_to'] = NULL;
					$array['sus_no_days'] = NULL;
				}

				$vHA->fails() ? $objectErrors[] = $vHA->messages()->all() : '';	
			}
			else 
			{
				$array['head_act'] = NULL;
				$array['sus_from'] = NULL;
				$array['sus_to'] = NULL;
				$array['sus_no_days'] = NULL;
			}
		}
		else
		{
			$array['head_act'] = NULL;
			$array['sus_from'] = NULL;
			$array['sus_to'] = NULL;
			$array['sus_no_days'] = NULL;
		}

		if ($array['action'] == 10) 
		{
			$vComment = validator::make($array, [
				'remarks' => 'required|max:5000'
			], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$vComment->fails() ? $objectErrors[] = $vComment->messages()->all() : '';
		}

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		if (count($return['msg']) == 0) 
		{
			$fm = new Libraries\FileManager;

			DB::beginTransaction();

			try 
			{
				$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();

				if($array['action'] == 10)
				{
					$nteCurrentId = $ntem->nte_employee_id;
				}
				elseif ($array['action'] == 11) 
				{
					$nteCurrentId = $ntem->nte_filer_id;
				}

				$pk = ['nte.nte_id' => $array['ref_num']];
				$old = ['nte_status' => $ntem->nte_status, 'nte_head_decision' => $ntem->nte_head_decision, 'nte_disciplinary_action' => $ntem->nte_disciplinary_action, 'nte_submitted_chrd' => $ntem->nte_submitted_chrd, 'nte_head_remarks' => $ntem->nte_head_remarks, 'nte_start_suspension' => $ntem->nte_start_suspension, 'nte_end_suspension' => $ntem->nte_end_suspension, 'nte_no_of_days' => $ntem->nte_no_of_days, 'nte_current_id' => $ntem->nte_current_id];
				$new = ['nte_status' => $data['status'], 'nte_head_decision' => $array['head_dec'], 'nte_disciplinary_action' => $array['head_act'], 'nte_submitted_chrd' => Carbon::now(), 'nte_head_remarks' => $array['head_remarks'], 'nte_start_suspension' => date_format(date_create($array['sus_from']), 'Y-m-d'), 'nte_end_suspension' => date_format(date_create($array['sus_to']), 'Y-m-d'), 'nte_no_of_days' => $array['sus_no_days']];

				$ntem->nte_status = $data['status'];
				$ntem->nte_head_decision = $array['head_dec'];
				$ntem->nte_disciplinary_action = $array['head_dec'] == 1 ? 1 : $array['head_act'];
				$ntem->nte_submitted_chrd = Carbon::now();
				$ntem->nte_head_remarks = $array['head_remarks'];
				$ntem->nte_start_suspension = date_format(date_create($array['sus_from']), 'Y-m-d');
				$ntem->nte_end_suspension = date_format(date_create($array['sus_to']), 'Y-m-d');
				$ntem->nte_no_of_days = $array['sus_no_days'];
				$ntem->nte_current_id = $nteCurrentId;
				$ntem->save();

				$nte_reference_number = $ntem->nte_reference_number;

				$this->_pushComments($array['ref_num'], $array['remarks'], 1);

				$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nte', $pk, $old, $new, $ntem->nte_reference_number);

				NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1, 'type' => 3])->delete();
				if (isset($array['dept'])) 
				{
					foreach($array['dept'] as $file){
						$this->_pushAttachments($ntem->nte_id, $file, 1, 3, $ntem->nte_reference_number);
						
						$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
						$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
						$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : '';
					}
				}
			
				if ($array['action'] == 11) 
				{
					$this->_pushMailNteAcknowledgement($array['ref_num'], 'nte');
				} 
				
				$return['scs'] = TRUE;
				$return['msg'] = [$data['msg']];
			} 
			catch (Exception $e) 
			{
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}
		} 

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		DB::commit();
		return $return;
	}

	public function _pushNteAcknowledgementToSend($array)
	{
		$return = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		if ($array['action'] == 12) 
		{
			$validator = validator::make($array, [
				'remarks' => 'required'
			], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$validator->fails() ? $objectErrors[] = $validator->messages()->all() : '';
		}

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		DB::beginTransaction();

		if (count($return['msg']) == 0) 
		{
			$fm = new Libraries\FileManager;

			try {
				$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();

				if($array['action'] == 12)
				{
					$nteCurrentId = $this->_pullDepartmentHead($ntem->nte_department_id);
				}
				elseif ($array['action'] == 13) 
				{
					$nteCurrentId = $ntem->nte_filer_id;
				}

				$pk = ['nte.nte_id' => $array['ref_num']];
				$old = ['nte_status' => $ntem->nte_status, 'nte_acknowledged' => $ntem->nte_acknowledged, 'nte_current_id' => $ntem->nte_current_id];
				$new = ['nte_status' => $data['status'], 'nte_acknowledged' => Carbon::now()];

				$ntem->nte_status = $data['status'];
				$ntem->nte_acknowledged = Carbon::now();
				$ntem->nte_current_id = $nteCurrentId;
				$ntem->save();

				$nte_reference_number = $ntem->nte_reference_number;

				$this->_pushComments($array['ref_num'], $array['remarks'], 1);

				$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nte', $pk, $old, $new, $ntem->nte_reference_number);

				NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 1, 'type' => 1])->delete();
				if (isset($array['files'])) 
				{
					foreach($array['files'] as $file){
						$this->_pushAttachments($ntem->nte_id, $file, 1, 1, $ntem->nte_reference_number);
						
						$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
						$fileExist = $this->getStoragePath('nte').$nte_reference_number.$file['random_filename'] ? TRUE : FALSE;
						$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nte').$nte_reference_number) : '';
					}
				}

				$return['msg'] = [$data['msg']];
				$return['scs'] = TRUE;
			} catch (Exception $e) {
				DB::rollback();
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}
		}

		DB::commit();

		$return['inputs'] = $array;

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

	public function _pushNda($module, $array)
	{
		$return = [];

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$old = [];
		$new = [];
		$pk = ['nte.nte_id', $array['ref_num']];


		$data = $this->_getMessageAndStatus([$array['action']]);
		
		$validator = validator::make($array, [
			'remarks' => 'max:5000'
		], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

		$validator->fails() ? $objectErrors[] = $validator->messages()->all() : '';

		$validaArray = ['remarks' => 'required'];

		if ($array['action'] == 18)
			$validaArray['return_to'] = 'required';

		if ($array['action'] == 16 || $array['action'] == 18 || $array['action'] == 20) 
		{
			$vRemarks = validator::make($array, $validaArray, self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

			$vRemarks->fails() ? $objectErrors[] = $vRemarks->messages()->all() : '';
		}

		if ($array['action'] === 14) 
		{
			$return['scs'] = TRUE;
		}

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		DB::beginTransaction();

		if ($return['scs'] || count($return['msg']) == 0)
		{
			$fm = New Libraries\FileManager;
			$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();
			$nda_reference_number = $ntem->nda_reference_number;

			try {
				switch ($module) {
					case 'manager':
						$thisTime = Carbon::now();
						$ntem->nda_reference_number === '' ? $nda_reference_number = $this->_generateRef('nda') : '';
						$ntem->nda_reference_number = $nda_reference_number;
						$ntem->nda_submitted_chrd_manager = $thisTime;

						$ntem->nda_status = $data['status'];
						$ntem->nda_filer_id = Session::get('employee_id');
						if ($data['status'] == 9)
						{
							$nteCurrentId = Session::get('employee_id');
						}
						elseif ($data['status'] == 10)
						{
							$nteCurrentId = $this->_pullNdaCHRDId(3);
							$ntem->nte_return_filer_id = 0;
						}

						$ntem->nte_current_id = $nteCurrentId;

						$old = [
							'nda_reference_number' => $ntem->nda_reference_number,
							'nda_submitted_chrd_manager' => $ntem->nda_submitted_chrd_manager,
							'nda_status' => $ntem->nda_status,
							'nda_filer_id' => $ntem->nda_filer_id,
							'nte_current_id' => $ntem->nte_current_id,
							'nte_return_filer_id' => $ntem->nte_return_filer_id
						];

						$new = [
							'nda_reference_number' => $nda_reference_number,
							'nda_submitted_chrd_manager' => $thisTime,
							'nda_status' => $data['status'],
							'nda_filer_id' => Session::get('employee_id'),
							'nte_current_id' => $nteCurrentId,
							'nte_return_filer_id' => 0
						];
						break;
					case 'head':
						$thisTime = Carbon::now();
						$ntem->nda_status = $data['status'];
						$ntem->nda_submitted_chrd_head = $thisTime;
						if ($data['status'] == 11)
						{
							$nteCurrentId = $ntem->nda_filer_id;
						}
						elseif ($data['status'] == 12)
						{
							$nteCurrentId = $this->_pullNdaCHRDId(5);
						}

						$ntem->nte_return_filer_id = 0;

						$ntem->nte_current_id = $nteCurrentId;

						$old = [
							'nda_status' => $ntem->nda_status,
							'nda_submitted_department_head' => $ntem->nda_submitted_department_head,
							'nte_current_id' => $nteCurrentId,
						];
						$new = [
							'nda_status' => $data['status'],
							'nda_submitted_department_head' => $thisTime,
							'nte_current_id' => $nteCurrentId,
						];
						break;
					case 'department':
						$thisTime = Carbon::now();
						$thisStatus = $data['status'];
						$ntem->nte_return_filer_id = explode('|',$array['return_to'])[0];
						$ntem->nda_submitted_department_head = $thisTime;
						if ($data['status'] == 10)
						{
							$ntem->nte_return_filer_id = explode('|', $array['return_to'])[0];
							$nteCurrentId = explode('|',$array['return_to'])[0];

							if (!(explode('|',$array['return_to'])[1] == 3))
								$thisStatus = 11;

						}
						elseif ($data['status'] == 13)
						{
							$nteCurrentId = $this->_pullDepartmentHead($ntem->nte_department_id);
						}

						$ntem->nte_current_id = $nteCurrentId;
						$ntem->nda_status = $thisStatus;

						$old = [
							'nte_return_filer_id' => $ntem->nte_return_filer_id,
							'nda_status' => $ntem->nda_status,
							'nda_submitted_department_head' => $ntem->nda_submitted_department_head,
							'nte_current_id' => $ntem->nte_current_id
						];

						$new = [
							'nte_return_filer_id' => explode('|',$array['return_to'])[0],
							'nda_status' => $thisStatus,
							'nda_submitted_department_head' => $thisTime,
							'nte_current_id' => $ntem->nte_current_id
						];
						break;
					case 'employee':
						$ntem->nda_status = $data['status'];
						if ($data['status'] == 12)
						{
							$nteCurrentId = $this->_pullNdaCHRDId(5);
						}
						elseif ($data['status'] == 14)
						{
							$nteCurrentId = $ntem->nte_employee_id;
						}

						$ntem->nte_current_id = $nteCurrentId;

						$old = [
							'nda_status' => $ntem->nda_status,
							'nte_current_id' => $ntem->nte_current_id,
						];

						$new = [
							'nda_status' => $data['status'],
							'nte_current_id' => $nteCurrentId,
						];

						break;
					case 'acknowledge':
						$thisTime = Carbon::now();
						$ntem->nda_status = $data['status'];
						$ntem->nda_acknowledged = $thisTime;
						$ntem->nte_current_id = $ntem->nda_filer_id;

						$old = [
							'nda_status' => $ntem->nda_status,
							'nda_acknowledged' => $thisTime,
							'nte_current_id' => $ntem->nte_current_id,
						];

						$new = [
							'nda_status' => $data['status'],
							'nda_acknowledged' => $thisTime,
							'nte_current_id' => $ntem->nda_filer_id,
						];
						break;
					case 'cbrm':
						$ntem->nda_status = $data['status'];
						$ntem->nte_current_id = 0;

						$old = [
							'nda_status' => $ntem->nda_status,
							'nte_current_id' => $ntem->nte_current_id,
						];

						$old = [
							'nda_status' => $data['status'],
							'nte_current_id' => 0
						];
						break;
					
					default:
						# code...
						break;
				}
				$ntem->save();

				$data['status'] == 14 ? $this->_pushMailNteAcknowledgement($array['ref_num'], 'nda') : '';

				$override = $data['status'] == 9 ? true : false;
				
				$this->_pushComments($array['ref_num'], $array['remarks'], 2, $override);

				if ($module == 'manager' || $module == 'head') 
				{
					NteAttachments::where(['nte_id' => $ntem->nte_id, 'module' => 2, 'type' => ($module == 'manager' ? 4 : 5)])->delete();

					if (isset($array['ndaf']) && $module == 'manager')
					{
						foreach($array['ndaf'] as $file)
						{
							$this->_pushAttachments($ntem->nte_id, $file, 2, 4, $ntem->nda_reference_number);
							
							$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
							$fileExist = $this->getStoragePath('nda').$nda_reference_number.$file['random_filename'] ? TRUE : FALSE;
							$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nda').$nda_reference_number) : '';
						}
					}

					if (isset($array['ndah']) && $module == 'head')
					{
						foreach($array['ndah'] as $file)
						{
							$this->_pushAttachments($ntem->nte_id, $file, 2, 5, $ntem->nda_reference_number);
							
							$hasFile = isset($file['random_filename']) ? TRUE : FALSE;
							$fileExist = $this->getStoragePath('nda').$nda_reference_number.$file['random_filename'] ? TRUE : FALSE;
							$hasFile && $fileExist ? $fm->move($file['random_filename'], $this->getStoragePath('nda').$nda_reference_number) : '';
						}
					}
				}

				$this->_pushAuditTrail(MODULE_ID, 'AU004', 'nda', $pk, $old, $new, $ntem->nda_reference_number);

				$return['scs'] = TRUE;
				$return['msg'] = [$data['msg']];
			} catch (Exception $e) {
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}
		}

		DB::commit();
		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

	public function _pushCancel($module, $array)
	{
		$return = [];

		$objectErrors = [];
		$arrayErrors = [];

		$return['scs'] = FALSE;
		$return['msg'] = [];

		$data = $this->_getMessageAndStatus([$array['action']]);

		$validator = Validator::make($array, [
			'remarks' => 'required|max:5000'
		], self::VALIDATION_MESSAGES)->setAttributeNames(self::FIELD_NAMES);

		$validator->fails() ? $objectErrors[] = $validator->messages()->all() : '';

		$return['msg'] = array_merge($this->_generateObjectError($objectErrors), $this->_generateArrayError($arrayErrors));

		if (count($return['msg']) == 0)
		{
			DB::beginTransaction();
			try {
				$ntem = Ntem::where(['nte_id' => $array['ref_num'], 'is_deleted' => 0])->first();

				if ($module == 'nte') {
					$ntem->nte_status = $data['status'];
					$return['msg'] = [NTEMessages::_messages('nte.cancelled')];
				}
				else
				{
					$ntem->nda_status = $data['status'];	
					$return['msg'] = [NTEMessages::_messages('nda.cancelled')];
				}
				$ntem->nte_current_id = 0;
				$ntem->save();

				$this->_pushComments($array['ref_num'], $array['remarks'], 1, true);

				$return['scs'] = TRUE;
			} catch (Exception $e) {
				DB::rollback();
				$return['scs'] = FALSE;
				$return['msg'] = [NTEMessages::_messages('i.error')];
			}
			DB::Commit();
		}

		Session::put('NTEMessages', ['s' => $return['scs'], 'm' => $return['msg']]);
		return $return;
	}

    public function _pushMailNteAccomplishment($nte_id)
    {
        $nteMailNotifications = new NTEMailNotifications();
        $nteMailNotifications->_composeNteAccomplishmentMail($nte_id);
    }

    public function _pushMailNteReturned($nte_id)
    {
        $nteMailNotifications = new NTEMailNotifications();
        $nteMailNotifications->_composeNteReturnedMail($nte_id);
    }

    public function _pushMailNteReview($nte_id)
    {
        $nteMailNotifications = new NTEMailNotifications();
        $nteMailNotifications->_composeNteReviewMail($nte_id);
    }

    public function _pushMailNteAcknowledgement($nte_id, $type)
    {
        $nteMailNotifications = new NTEMailNotifications();
        $nteMailNotifications->_composeAcknowledgementMail($nte_id, $type);
    }
}
?>