<?php namespace RGAS\Modules\NTE;

use CrrBuildUp;
use CrrBuildUpDetail;
use Ntem;
use NTEController;
use Input;
use Session;

class NTELists extends NTEController
{
	public function _needsForNteList($data_column_array, $where_condition, $module, $limit = FALSE)
   	{
		$result_data = []; 

        Input::all();

		$iSortingCol = Input::get("iSortCol_0", null, true);
		$sSortDir = Input::get("sSortDir_0", null, true);
		$sSearch =  trim(strtolower(Input::get("sSearch", null, true)));
		if ($module === 'nte.all' || $module === 'nda.all') 
		{
			$sDepartment = Input::get("sDepartment", null, true);
				if (!empty($sDepartment))
					$where_condition .= " AND nte.nte_department_id = $sDepartment ";
			$sYear = Input::get("sYear", null, true);
				if (!empty($sYear))
					$where_condition .= " AND nte.nte_year = $sYear ";
			$sMonth = Input::get("sMonth", null, true);
				if (!empty($sMonth))
					$where_condition .= " AND nte.nte_month = $sMonth ";
		}

		if($sSearch != "") 
		{
			$where_condition .= " AND";
			$where_condition .= "(";
			for($i = 0; $i < count($data_column_array); $i++)
			{
				$sep = ( $i == count($data_column_array) -1 )?"":" OR ";
				$where_condition .=  $data_column_array[$i] ." Like '%{$sSearch}%'". $sep;
			}
			$where_condition .= ")";
		}

		if ($module == 'nte.approval') 
		{
			$returnedData = Ntem::_pullNteAllApproval($limit);
		}
		else 
		{
			$returnedData = Ntem::_pullNteAll($where_condition, $limit);
		}
        
        $result_data["iTotalDisplayRecords"] = $returnedData->count(); //total count filtered query
		$result_data["iTotalRecords"] = $returnedData->count();
        
        if ( $returnedData->count() > 0)
        {
            $ctr = 0;
             foreach($returnedData as $returned) 
             {
             	$current = $this->_pullEmployeeNameById($returned->nte_current_id);

             	$month = $this->_getMonthNameByMonthNumber($returned->nte_month);

             	$returned->nte_day != null ? $data_committed = $month . ' ' . $returned->nte_day . ', ' . $returned->nte_year : $data_committed = $month . ' ' . $returned->nte_year;

             	switch ($module) 
             	{
             		case 'nte.approval':
             			$result_data["aaData"][$ctr][] = $returned->dept_name;
						$result_data["aaData"][$ctr][] = $returned->count;
						$result_data["aaData"][$ctr][] = $month;
						$result_data["aaData"][$ctr][] = $returned->nte_year;
						$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_approval('.$returned->nte_department_id.','.$returned->nte_month.','.$returned->nte_year.', false)">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_approval('.$returned->nte_department_id.','.$returned->nte_month.','.$returned->nte_year.', true)">APPROVE</button>';
             			break;
         			case 'nte.department':
         				$result_data["aaData"][$ctr][] = $returned->nte_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $returned->nte_status;

						if ($returned->Stat == 6) 
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_department('.$returned->nte_id.')">PROCESS</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>PROCESS</button>';
         				break;

     				case 'nte.employee':
     					$result_data["aaData"][$ctr][] = $returned->nte_reference_number;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $returned->nte_status;
						if ($returned->Stat == 4 || $returned->Stat == 5) 
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_employee('.$returned->nte_id.')">PROCESS</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>PROCESS</button>';
     					break;

 					case 'nte.all':
		                $result_data["aaData"][$ctr][] = $returned->nte_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->Department;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $current;
						$result_data["aaData"][$ctr][] = $returned->nte_status;
						if ($returned->nte_filer_id == Session::get('employee_id')) {
							if ($returned->Stat == 8) 
							{
								/*if (($returned->nte_disciplinary_action === 3 || $returned->nte_disciplinary_action === 4) && ($returned->NdaStat == 0) || ($returned->NdaStat == 9)) 
								{
									$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_request('.$returned->nte_id.')">EDIT</button>';
								}
								else
								{*/
									$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
								/*}*/
									
							} 
							elseif ($returned->Stat == 7) 
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_request('.$returned->nte_id.')">PROCESS</button>';
							}
							elseif ($returned->Stat == 3)
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nte_delete('.$returned->nte_id.',this.parentNode.parentNode.rowIndex)">DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_returned('.$returned->nte_id.')">EDIT</button>';
							}
							elseif ($returned->Stat == 2)
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
							}
							else 
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
							}
						}
						else 
						{
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nte_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
						}
 						break;

					case 'nda.all':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->Department;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $current;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						if ($returned->nda_filer_id == Session::get('employee_id') && $returned->nte_return_filer_id == 0) 
						{
							if ($returned->NdaStat == 9 || $returned->NdaStat == 11)
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_request('.$returned->nte_id.')">EDIT</button>';
							}
							elseif ($returned->NdaStat == 15)
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_acknowledged('.$returned->nte_id.')">EDIT</button>';
							}
							elseif ($returned->NdaStat == 10 || $returned->NdaStat == 12)
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
							}
							else
							{
								$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
							}
						}
						elseif($returned->nte_return_filer_id == Session::get('employee_id') && $returned->NdaStat == 11)
						{
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_request('.$returned->nte_id.')">EDIT</button>';
						}
						else 
						{
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" disabled>DELETE</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
						}
						break;

					case 'nda.employee':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						if ($returned->NdaStat == 14)
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_employee('.$returned->nte_id.')">EDIT</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>EDIT</button>';
						break;

					case 'nda.department':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						if ($returned->NdaStat == 13)
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_department('.$returned->nte_id.')">APPROVE</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>APPROVE</button>';
						break;

					case 'nda.chrd.head':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->Department;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						if ($returned->NdaStat == 12)
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_approval_head('.$returned->nte_id.')">APPROVE</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>APPROVE</button>';
						break;

					case 'nda.chrd.manager':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->Department;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						if ($returned->NdaStat == 10)
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" onclick="nda_approval_manager('.$returned->nte_id.')">APPROVE</button>';
						else
							$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_view('.$returned->nte_id.')">VIEW</button>&nbsp;<button type="button" class="btn btn-default btndefault" disabled>APPROVE</button>';
						break;

					case 'nda.cbrm':
						$result_data["aaData"][$ctr][] = $returned->nda_reference_number;
						$result_data["aaData"][$ctr][] = $returned->firstname .' '. $returned->middlename .' '. $returned->lastname;
						$result_data["aaData"][$ctr][] = $returned->Department;
						$result_data["aaData"][$ctr][] = $data_committed;
						$result_data["aaData"][$ctr][] = $returned->cb_rule_name;
						$result_data["aaData"][$ctr][] = $returned->Recommended;
						$result_data["aaData"][$ctr][] = $returned->nda_status;
						$result_data["aaData"][$ctr][] = '<button type="button" class="btn btn-default btndefault" onclick="nda_cbrm('.$returned->nte_id.', true)">VIEW</button>';
						break;
             		
             		default:
             			# code...
             			break;
             	}

                $ctr++;
             }
        }
        else 
        {
            $result_data["aaData"] = $returnedData;
            $result_data["iTotalDisplayRecords"] = 0;
			$result_data["iTotalRecords"] = 0;
        }

        return $result_data;
   	}

   	public function _pullCrrBuildUpAll()
    {
  		$result_data = []; //result data on my itr lists
  		$crrIds = [];
  		$crrHeader = new CrrBuildUp();
          
  		$data_column_array = array(
	       "crrbuildup.cb_rule_name"
	       ,"crrbuildup.cb_rule_number"
	       ,"crrbuildup.cb_rule_description"
	       ,"crrbuildup.cb_rule_status"
	       ,"crrbuildup.rule_number"
		);

  		$data_column_detail_array = array(
	       "crrbuildupdetail.cbd_section_description"
	       ,"crrbuildupdetail.cbd_section_number"
	       ,"crrbuildupdetail.cbd_section_status"
	       ,"crrbuildupdetail.section_number"
      	);
      
	  	$where_condition = " (crrbuildup.cb_rule_status in ('1','2','0')) ";
	  	$where_condition_detail = " (crrbuildupdetail.cbd_section_status in ('1','2','0')) ";
      
      	$iSortingCol = Input::get("iSortCol_0", null, true);
  		$sSortDir = Input::get("sSortDir_0", null, true);
  		$sSearch =  trim(strtolower(Input::get("sSearch", null, true)));
      
  		if($sSearch != "") 
  		{
  			$where_condition .= " AND";
  			$where_condition .= "(";

  			for($i = 0; $i < count($data_column_array); $i++) 
  			{
  				$sep = ( $i == count($data_column_array) -1 )?"":" OR ";
  				$where_condition .=  $data_column_array[$i] ." Like '%{$sSearch}%'". $sep;
  			}

  			$where_condition .= " OR crrbuildup.status_name = '{$sSearch}'";

  			$where_condition .= ")";

        	$where_condition_detail .= " AND";
        	$where_condition_detail .= "(";

        	for($j = 0; $j < count($data_column_detail_array); $j++) 
        	{
            	$sep = ( $j == count($data_column_detail_array) -1 )?"":" OR ";
            	$where_condition_detail .=  $data_column_detail_array[$j] ." Like '%{$sSearch}%'". $sep;
        	}

        	$where_condition_detail .= ")";
  		}

	    $crr_list_detail_html = CrrBuildUpDetail::get_all_crr_details($where_condition_detail);
	    $crr_list_html1 = CrrBuildUp::get_all_crrs($where_condition . ' order by crrbuildup.cb_rule_name asc');
	    $crr_list_html2 = [];
	    $crr_list_html = [];
      	
	    if ( count($crr_list_html1) > 0 || $crr_list_detail_html->count() > 0 )
	    {
	    	if ($crr_list_detail_html->count() > 0) 
	    	{
	    		for ($q=0; $q<$crr_list_detail_html->count(); $q++) 
	    		{
	    			array_push($crrIds, $crr_list_detail_html[$q]->cb_crr_id);
	    		}
	    		$crrIds = array_unique($crrIds);
	    		
    			$crr_list_html2 = $crrHeader->whereIn('cb_crr_id', $crrIds)->orderBy('crrbuildup.cb_rule_name', 'asc')->get()->toArray();	
	    	}

	    	foreach ($crr_list_html1 as $list)
	    		$crr_list_html[] = json_decode(json_encode($list), True);;

	    	foreach ($crr_list_html as $list)
	    		if (!$this->checkIfExist($crr_list_html2, 'cb_crr_id', $list['cb_crr_id']) && 
	    			!$this->checkIfExist($crr_list_html, 'cb_crr_id', $list['cb_crr_id']))
	    			$crr_list_html[] = $list;

    		usort($crr_list_html, function ($elem1, $elem2) {
			     return strcmp($elem1['cb_rule_name'], $elem2['cb_rule_name']);
			});

    		if (count($crr_list_html) > 0)
		      	for($ctrh=0; $ctrh<count($crr_list_html); $ctrh++) 
		      	{
		      		$sectionDescription = '';
		      		$sectionNo = ', Section ';
		      		if ($crr_list_detail_html->count() > 0) { 
		        		for($ctrd=0; $ctrd<count($crr_list_detail_html); $ctrd++) {
		        			if ($crr_list_html[$ctrh]['cb_crr_id'] == $crr_list_detail_html[$ctrd]->cb_crr_id) 
		        			{
		        				if ($crr_list_detail_html[$ctrd]->cbd_section_status == 1) 
		        				{
					                if ($sectionNo === ', Section ') 
					                {
					                  $sectionNo .= ' ' . $crr_list_detail_html[$ctrd]->cbd_section_number;    
					                } 
					                else 
					                {
					                  $sectionNo .= ', ' . $crr_list_detail_html[$ctrd]->cbd_section_number;
					                }
		        				}

				                $crr_list_detail_html[$ctrd]->cbd_section_status == 1 ? 
		        				$sectionDescription .= '<br> &nbsp; &nbsp; &nbsp; Section ' . 
		        				$crr_list_detail_html[$ctrd]->cbd_section_number . ' - ' . $crr_list_detail_html[$ctrd]->cbd_section_description :
		        				$sectionDescription .= '<font color="#a9a9a9"><br> &nbsp; &nbsp; &nbsp; Section ' . 
		        				$crr_list_detail_html[$ctrd]->cbd_section_number . ' - ' . $crr_list_detail_html[$ctrd]->cbd_section_description.'</font>';
		        			}
		        		}
	            	}
		          	if (strlen($sectionNo) < 11) 
		          	{
		            	$sectionNo = '';
		          	}

		          	$result_data["aaData"][$ctrh][] = $crr_list_html[$ctrh]['cb_crr_id'];
		          	$result_data["aaData"][$ctrh][] = $crr_list_html[$ctrh]['cb_rule_status'] == 1 ? $crr_list_html[$ctrh]['cb_rule_name'] : '<font color="#a9a9a9">'.$crr_list_html[$ctrh]['cb_rule_name'].'</font>';
		          	$result_data["aaData"][$ctrh][] = $crr_list_html[$ctrh]['cb_rule_status'] == 1 ? 'Rule ' . $crr_list_html[$ctrh]['cb_rule_number'] . $sectionNo : '<font color="#a9a9a9">'.'Rule ' . $crr_list_html[$ctrh]['cb_rule_number'] . $sectionNo.'</font';
		          	$result_data["aaData"][$ctrh][] = $crr_list_html[$ctrh]['cb_rule_status'] == 1 ? '<strong> Rule ' . $crr_list_html[$ctrh]['cb_rule_number'] .
		          	' - ' . $crr_list_html[$ctrh]['cb_rule_description'] .
		          	'</strong>' . $sectionDescription : '<font color="#a9a9a9"><strong> Rule ' . $crr_list_html[$ctrh]['cb_rule_number'] .
		          	' - ' . $crr_list_html[$ctrh]['cb_rule_description'] .
		          	'</strong>' . $sectionDescription.'</font>';

		          	$result_data["aaData"][$ctrh][] = $crr_list_html[$ctrh]['cb_rule_status'] == 1 ? 'Active' : '<font color="#a9a9a9">Inactive</font>';

		          	$result_data["iTotalDisplayRecords"] =  count($crr_list_html);
					$result_data["iTotalRecords"] = count($crr_list_html);
		      	}
	      	else
	      	{
	      		$result_data["aaData"] = $crr_list_html;
	        	$result_data["iTotalDisplayRecords"] = 0;
				$result_data["iTotalRecords"] = 0;
	      	}
        }
      	else 
      	{
        	$result_data["aaData"] = $crr_list_html1;
        	$result_data["iTotalDisplayRecords"] = 0;
			$result_data["iTotalRecords"] = 0;
      	}

      	return $result_data;
    }

    public function checkIfExist($array, $check_field, $check_value)
    {
    	foreach ($array as $a)
    		if ($a[$check_field] == $check_value)
    			return TRUE;

		return FALSE;
    }
}