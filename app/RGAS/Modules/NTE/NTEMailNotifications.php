<?php namespace RGAS\Modules\NTE;

use Receivers;
use Employees;
use Departments;
use DB;
use Mail;
use Ntem;
use NteController;

define("EMPLOYEE_NAME", "CONCAT(IFNULL(employees.firstname, ''), ' ', IFNULL(employees.middlename, ''), ' ', IFNULL(employees.lastname, '')) name");
define("EMP_NAME", "CONCAT(IFNULL(emp.firstname, ''), ' ', IFNULL(emp.middlename, ''), ' ', IFNULL(emp.lastname, '')) name");

class NTEMailNotifications extends NteController
{
	public function _flyMail($data, $module, $messageContent)
	{
		if ($messageContent['email'] != '' || $messageContent['name'] != '')
			Mail::send('nte.mail', ['data' => $data, 'module' => $module], function($message) use ($messageContent)
		    {
		        $message->to($messageContent['email'], $messageContent['name'])->subject($messageContent['subject']);
		    });
	}

	public function _composeNteNotationMail()
	{
		$data = $this->_groupByReceiver(Ntem::_pullNteAllNotation());

		if (count($data) > 0)
		{
			for ($i=0; $i < count($data); $i++) 
			{ 
				$dataArray = $data[$i];
				$approver = Employees::select(DB::raw(EMPLOYEE_NAME), 'email')->where('id', $dataArray[$i]['approver'])->first();
				
				$module = 'nte.notation';
				$messageContent['subject'] = 'RGAS Notification: Notice to Explain for Notation';
				$messageContent['email'] = $approver['email'];
				$messageContent['name'] = $approver['name'];

				$this->_flyMail($dataArray, $module, $messageContent);
			}
		}
	}

	public function _composeNdaCHRDNotationMail($type)
	{
		if ($type == 'head')
		{
			$value = 5;
			$data = Ntem::_pullNdaAllCHRD('head');
		}
		elseif ($type == 'manager') 
		{
			$value = 4;
			$data = Ntem::_pullNdaAllCHRD('manager');
		}

		$receiver = Receivers::select('receivers.employeeid', 'emp.email', DB::raw(EMP_NAME))->where(['receivers.module' => 'nte', 'receivers.title' => 'CHR AER Filer', 'value' => $value])->leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->first();

		$count = 0;
		if (count($data) > 0)
		{
			foreach ($data as $ret) 
			{
				$count = $count + $ret->count;
			}
			
			$module = 'nte.notation';
			$messageContent['subject'] = 'RGAS Notification: Notice of Disciplinary Action for Notation';
			$messageContent['email'] = $receiver['email'];
			$messageContent['name'] = $receiver['name'];
			$data['count'] = $count;

			$this->_flyMail($data, $module, $messageContent);
		}
	}

	public function _composeNdaDepartmentHeadNotationMail()
	{
		$data = $this->_groupByDepartment(Ntem::_pullNdaDepartmentsNotation());

		if (count($data) > 0)
		{
			for ($i=0; $i < count($data); $i++) 
			{ 
				$receiver = Employees::select('employees.email', DB::raw(EMPLOYEE_NAME))->where(['employees.departmentid' => $data[$i][0]['nte_department_id'], 'desig_level' => 'head'])->first();

				$module = 'nda.department.notation';
				$messageContent['subject'] = 'RGAS Notification: Notice of Disciplinary Action for Notation';
				$messageContent['email'] = $receiver['email'];
				$messageContent['name'] = $receiver['name'];

				$this->_flyMail($data[$i], $module, $messageContent);
			}
		}
	}

	public function _composeAcknowledgementMail($id, $type)
	{
		$data = Ntem::_pullNteAccomplishmentByNteId($id);

		$emp_id = $type == 'nte' ? $data['nte_filer_id'] : $data['nte_employee_id'];
		$subject = $type == 'nte' ? 'RGAS Notification: Notice to Explain for Acknowledgement' : 'RGAS Notification: Notice to Explain with Disciplinary Action for Acknowledgement';
		$receiver = Employees::select('email', DB::raw(EMPLOYEE_NAME))->where(['id' => $emp_id])->first();

		if (count($data) > 0)
		{
			$module = 'acknowledgement';
			$messageContent['subject'] = $subject;
			$messageContent['email'] = $receiver['email'];
			$messageContent['name'] = $receiver['name'];

			$this->_flyMail($data, $module, $messageContent);
		}
	}

	public function _composeNteAccomplishmentMail($id)
	{
		$data = Ntem::_pullNteAccomplishmentByNteId($id);
		$receiver = Employees::select('email', DB::raw(EMPLOYEE_NAME))->where(['id' => $data['nte_employee_id']])->first();
		if (count($data) > 0)
		{
			$module = 'nte.accomplishment';
			$messageContent['subject'] = 'RGAS Notification: Notice to Explain for Accomplishment';
			$messageContent['email'] = $receiver['email'];
			$messageContent['name'] = $receiver['name'];

			$this->_flyMail($data, $module, $messageContent);
		}
	}

	public function _composeNteReturnedMail($id)
	{
		$data = Ntem::_pullNteAccomplishmentByNteId($id);
		$receiver = Employees::select('email', DB::raw(EMPLOYEE_NAME))->where(['id' => $data['nte_employee_id']])->first();
		if (count($data) > 0)
		{
			$module = 'nte.returned';
			$messageContent['subject'] = 'RGAS Notification: Returned Notice to Explain';
			$messageContent['email'] = $receiver['email'];
			$messageContent['name'] = $receiver['name'];

			$this->_flyMail($data, $module, $messageContent);
		}
	}

	public function _composeNteReviewMail($id)
	{
		$data = Ntem::_pullNteAccomplishmentByNteId($id);
		$receiver = Employees::select('employees.email', DB::raw(EMPLOYEE_NAME))->where(['employees.departmentid' => $data['nte_department_id'], 'employees.desig_level' => 'head'])->first();

		if (count($data) > 0)
		{
			$module = 'nte.review';
			$messageContent['subject'] = 'RGAS Notification: Notice to Explain for Review';
			$messageContent['email'] = $receiver['email'];
			$messageContent['name'] = $receiver['name'];

			$this->_flyMail($data, $module, $messageContent);
		}
	}

	public function _groupByReceiver($array)
	{
		$filers = $this->_pullFilerAll();

		$data = [];

		for ($i=0; $i < count($filers); $i++) 
		{ 
			for ($j=0; $j < count($array); $j++) 
			{ 
				if ($array[$j]['approver'] == $filers[$i]['id']) 
				{
					$data[$i][] = $array[$j];
					if (isset($data[$i]['count'])) 
					{
						$data[$i]['count'] = $data[$i]['count'] + $array[$j]['count'];
					}
					else
					{
						$data[$i]['count'] = $array[$j]['count'];
					}
				}
			}
		}

		return array_values($data);
	}

	public function _groupByDepartment($array)
	{
		$departments = $this->_pullDepartmentsAll();

		$data = [];
		
		for ($i=0; $i < count($departments); $i++) 
		{ 
			for ($j=0; $j < count($array); $j++) 
			{ 
				if ($array[$j]['nte_department_id'] == $departments[$i]['id']) 
				{
					$data[$i][] = $array[$j];
					if (isset($data[$i]['count'])) 
					{
						$data[$i]['count'] = $data[$i]['count'] + 1;
					}
					else
					{
						$data[$i]['count'] = 1;
					}
				}
			}
		}

		return array_values($data);
	}
}
?>