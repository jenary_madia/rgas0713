<?php namespace RGAS\Modules\NTE;

use NTEController;

class NTEMessages extends NTEController
{
	public static function _messages($callType, $mixed = '') 
	{
		$message = '';

		switch ($callType) 
		{
			case 'nda.cbrm':
				$message = "NDA SUCCESSFULLY SENT TO CHRD-CBRM FOR FILING";
				break;
			case 'i.error':
				$message = "INTERNAL ERROR!";
				break;
			case 'error':
				$message = "ERROR! PLEASE ACCOMPLISH REQUIRED FIELDS.";
				break;
			case 'cancel':
				$message = "ARE YOU SURE YOU WANT TO CANCEL NTE OF ".$mixed."?";
				break;
			case 'crr.add':
				$message = "CRR SUCCESSFULLY ADDED TO BUILD-UP";
				break;
			case 'crr.edit':
				$message = "CRR SUCCESSFULLY CHANGED";
				break;
			case 'crr.row.error':
				$message = $mixed . " ALREADY EXISTS";
				break;
			case 'nte.send.notation':
				$message = $mixed." NTE SUCCESSFULLY SENT TO CHRD-AER SUPERVISOR FOR NOTATION.";
				break;
			case 'nte.send':
				$message = "NTE SUCCESSFULLY SENT TO CHRD-AER SUPERVISOR FOR NOTATION.";
				break;
			case 'nte.save':
				$message = "NTE SUCCESSFULLY SAVED";
				break;
			case 'nte.send.personnel.accomplishment':
				$message = "NTE SUCCESSFULLY SENT TO CONCERNED PERSONNEL FOR ACCOMPLISHMENT";
				break;
			case 'nte.send.head.review':
				$message = "NTE SUCCESSFULLY SENT TO DEPARTMENT HEAD FOR REVIEW";
				break;
			case 'nte.send.acknowledgement':
				$message = "NTE SUCCESSFULLY SENT TO FILER FOR ACKNOWLEDGEMENT";
				break;
			case 'nte.return.filer':
				$message = "NTE SUCCESSFULLY RETURNED TO FILER";
				break;
			case 'nte.return.employee.revision':
				$message = "NTE SUCCESSFULLY SENT TO CONCERNED EMPLOYEE FOR REVISION";
				break;
			case 'nte.return.head.revision':
				$message = "NTE SUCCESSFULLY SENT TO CONCERNED DEPARTMENT HEAD FOR REVISION";
				break;
			case 'nte.return.department.head':
				$message = "NTE SUCCESSFULLY SENT TO CONCERNED DEPARTMENT HEAD FOR REVISION";
				break;
			case 'nte.acknowledged':
				$message = "NTE SUCCESSFULLY ACKNOWLEDGED";
				break;
			case 'nda.save':
				$message = "NDA SUCCESSFULLY SAVED";
				break;
			case 'nda.send.manager':
				$message = "NDA SUCCESSFULLY SENT TO CHRD-AER MANAGER FOR NOTATION";
				break;
			case 'nda.send.chrdhead':
				$message = "NDA SUCCESSFULLY SENT TO CHRD HEAD FOR NOTATION";
				break;
			case 'nda.return.filer':
				$message = "NDA SUCCESSFULLY RETURNED TO FILER";
				break;
			case 'nda.return.manager':
				$message = "NDA SUCCESSFULLY SENT TO CHRD-AER FOR REVISION";
				break;
			case 'nda.send.department.head':
				$message = "NDA SUCCESSFULLY SENT TO CONCERNED DEPARTMENT HEAD FOR NOTATION";
				break;
			case 'nda.send.employee':
				$message = "NDA SUCCESSFULLY SERVED TO CONCERNED EMPLOYEE";
				break;
			case 'nda.return.chrd.head':
				$message = "NDA SUCCESSFULLY SENT TO CHRD HEAD FOR REVISION";
				break;
			case 'nda.acknowledged':
				$message = "NDA SUCCESSFULLY ACKNOWLEDGED";
				break;
			case 'nda.cancelled':
				$message = "NDA SUCCESSFULLY CANCELLED.";
				break;
			case 'nte.cancelled':
				$message = "NTE SUCCESSFULLY CANCELLED";
				break;
			default:
				$message = '';
				break;
		}

		return $message;
	}
}