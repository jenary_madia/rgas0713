<?php namespace RGAS\Modules\LRF;

use LeaveRequestForm;
use Session;
use RGAS\Libraries;
use Validator;
use Leaves;
use Input;
use Redirect;
use Signatory;
use Receivers;
use Employees;
use LeaveBatch;
use LeaveBatchDetails;
use DB;
use Config;
use Mail;
use Response;
use URL;

class LRF
{
	protected $LRFHelper;
    protected $superiorEmail;
	protected $logs;

	public function __construct()
	{
        set_time_limit(0);
		$this->leaves = new Leaves;
        $this->superiorEmail = Employees::where('id',Session::get("superiorid"))->first()['email'];
        $this->logs = new Logs;
	}


	public function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'lrf/';
	}

	public function createAction()
	{
        $form_details = Input::get("form_details");
        $request_details = Input::get("request_details");
        $med_attachments = Input::get("med_attachments");
        $attachments = Input::get("attachments");
        $action = Input::get("action");
		if(Input::get('lrfLeaveType') == 'HomeVisit') {
            $homeVisitPerQuarter  = 7;
            $monthToday = Date("m");
            switch ($monthToday) {
                case (in_array($monthToday,['01','02','03'])):
                    $quarter = ['01','02','03'];
                    break;
                case (in_array($monthToday,['04','05','06'])):
                    $quarter = ['04','05','06'];
                    break;
                case (in_array($monthToday,['07','08','09'])):
                    $quarter = ['07','08','09'];
                    break;
                case (in_array($monthToday,['10','11','12'])):
                    $quarter = ['10','11','12'];
                    break;
            }
            $homeVisitSum = Leaves::getHomeVisitLeaves(Session::get("employee_id"),$quarter);
            $totalDaysToUse = Input::get("lrfTotalLeaveDays") + $homeVisitSum['sum'];
            if($totalDaysToUse > $homeVisitPerQuarter) {
                return Response::json([
                    'success' => false,
                    'errors' => ["You\"ve exceed on days allowed per quarter."],
                    200
                ]);
            }

		}

		$parameters = [
            'documentcode' => date('Y-m'),
            'codenumber' => $this->leaves->generateCodeNumber(),
            'employeeid' => Session::get('employeeid'),
            'firstname' => Session::get('firstname'),
            'middlename' => Session::get('middlename'),
            'lastname' => Session::get('lastname'),
            'company' => Session::get('company'),
            'date' => date('Y-m-d'),
            'appliedfor' => $request_details['leave_type'],
            'noofdays' => $request_details['total_days'],
            'reason' => $request_details['reason'],
            'ownerid' => Session::get('employee_id'),
            'datecreated' => date('Y-m-d'),
            'department' => Session::get('dept_name'),
            'section' => $form_details['section'],
            'contact_no' => $form_details['contact_number'],
            'schedule_type' => $request_details['schedule_type'],
            'rest_day' => json_encode($request_details['rest_day']),
            'duration' => $request_details['duration'],
            'period_from' => ($request_details['from_period'] == "half" ? 1 : 2),
            'comment' => json_encode([
                            'name'=>Session::get('employee_name'),
                            'datetime'=>date('m/d/Y g:i A'),
                            'message'=>Input::get('lrfComment')
                        ])

        ];

        if($request_details['duration'] == 'multiple') {
            $parameters['period_to'] = ($request_details['to_period'] == "half" ? 1 : 2);
            if($request_details['to_date']) {
                $parameters['to'] = $request_details['to_date'];
            }
        }

        if($request_details['from_date']) {
            $parameters['from'] = $request_details['from_date'];
        }


        if($action == "send") {
            $curr_emp = $this->routing();
        }else if($action == "save") {
            $curr_emp = [
                'id' => Session::get("employee_id"),
                'status' => 'NEW'
            ];
        }


        if(! $curr_emp['id']) {
            return Response::json(array(
                'success' => false,
                'errors' => ['No receiver declared.'],
                200
            ));
        }else{
            $parameters['curr_emp'] = $curr_emp['id'];
            $parameters['status'] = $curr_emp['status'];
        }

        $fm = new Libraries\FileManager;
        if($med_attachments) {
            if($action == "send") {
                $fm->move($med_attachments[0]['random_filename'], $this->getStoragePath() . $parameters['documentcode'] . '-' . $parameters['codenumber']);
            }
            $parameters['clinic_attachment'] = json_encode($med_attachments[0]);
        }

        if($attachments){
            $i = 1;
            foreach($attachments as $file){
                if($action == "send") {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $parameters['documentcode'] . '-' . $parameters['codenumber']);
                }
                $parameters['attach' . $i] = json_encode($file);
                $i++;
                if ($i >= 6) {
                    break;
                }
            }

            for ($x = $i; $x < 6; $x++) {
                $parameters['attach'.$x] = null;
            }

        }else{
            for ($x = 1; $x < 6; $x++) {
                $parameters['attach'.$x] = null;
            }
        }

        $this->leaves->store($parameters);

        if($action == "send") {
            $curr_emp_details = $this->getEmpData($curr_emp['id']);
            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $parameters['documentcode'].'-'.$parameters['codenumber'],
                Session::get('firstname').' '.Session::get('middlename').'. '.Session::get('lastname'),
                Session::get('dept_name'),
                $curr_emp_details['email'],
                $curr_emp_details
            );

            if(Session::get("desig_level") == "president") {
                $message = "Leave request successfully sent to CHRD for processing.";
            }else{
                $message = "Leave request successfully sent to ".$curr_emp['send_to']." for approval.";
            }

            return Response::json([
                'success' => true,
                'message' => $message,
                'url' => URL::to("lrf/leaves"),
                200
            ]);

        }else{
            $message = "Leave request successfully saved and can be edit later.";
        }

        return Response::json([
            'success' => true,
            'message' => $message,
            'url' => URL::to("lrf/leaves"),
            200
        ]);
	}

    public function recreateAction()
    {

        $form_details = Input::get("form_details");
        $request_details = Input::get("request_details");
        $med_attachments = Input::get("med_attachments");
        $attachments = Input::get("attachments");
        $action = Input::get("action");
        $leave_id = Input::get("leave_id");
        if(Input::get('lrfLeaveType') == 'HomeVisit') {
            $homeVisitPerQuarter  = 7;
            $monthToday = Date("m");
            switch ($monthToday) {
                case (in_array($monthToday,['01','02','03'])):
                    $quarter = ['01','02','03'];
                    break;
                case (in_array($monthToday,['04','05','06'])):
                    $quarter = ['04','05','06'];
                    break;
                case (in_array($monthToday,['07','08','09'])):
                    $quarter = ['07','08','09'];
                    break;
                case (in_array($monthToday,['10','11','12'])):
                    $quarter = ['10','11','12'];
                    break;
            }
            $homeVisitSum = Leaves::getHomeVisitLeaves(Session::get("employee_id"),$quarter);
            $totalDaysToUse = Input::get("lrfTotalLeaveDays") + $homeVisitSum['sum'];
            if($totalDaysToUse > $homeVisitPerQuarter) {
                return Response::json([
                    'success' => false,
                    'errors' => ["You\"ve exceed on days allowed per quarter."],
                    200
                ]);
            }

        }

        $leave = Leaves::where('leaves.ownerid',Session::get('employee_id'))
            ->where('leaves.id',$leave_id)
            ->first();

        if(! $leave) {
            return Response::json([
                'success' => false,
                'errors' => ['Invalid leave'],
                200
            ]);
        }

        $parameters = [
            'date' => date('Y-m-d'),
            'appliedfor' => $request_details['leave_type'],
            'noofdays' => $request_details['total_days'],
            'reason' => $request_details['reason'],
            'datecreated' => date('Y-m-d'),
            'section' => $form_details['section'],
            'contact_no' => $form_details['contact_number'],
            'schedule_type' => $request_details['schedule_type'],
            'rest_day' => json_encode($request_details['rest_day']),
            'duration' => $request_details['duration'],
            'period_from' => ($request_details['from_period'] == "half" ? 1 : 2),
        ];
        if($leave['status'] == 'NEW') {
            $parameters['comment'] = json_encode([
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            ]);
        } else{
            $parameters['comment'] = DB::raw("concat(comment,'|','".
                json_encode([
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                ])
                ."')");
        }

        if($request_details['duration'] == 'multiple') {
            $parameters['period_to'] = ($request_details['to_period'] == "half" ? 1 : 2);
            if($request_details['to_date']) {
                $parameters['to'] = $request_details['to_date'];
            }
        }

        if($request_details['from_date']) {
            $parameters['from'] = $request_details['from_date'];
        }


        if($action == "resend") {
            $curr_emp = $this->routing();
        }else if($action == "resave") {
            $curr_emp = [
                'id' => Session::get("employee_id"),
                'status' => 'NEW'

            ];
        }

        if(! $curr_emp['id']) {
            return Response::json(array(
                'success' => false,
                'errors' => ['No receiver declared.'],
                200
            ));
        }else{
            $parameters['curr_emp'] = $curr_emp['id'];
            $parameters['status'] = $curr_emp['status'];
        }

        $fm = new Libraries\FileManager;
        if($med_attachments) {
            if($action == "send") {
                $fm->move($med_attachments[0]['random_filename'], $this->getStoragePath() . $parameters['documentcode'] . '-' . $parameters['codenumber']);
            }
            $parameters['clinic_attachment'] = json_encode($med_attachments[0]);
        }

        if($attachments){
            $i = 1;
            foreach($attachments as $file){
                if($action == "send") {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $parameters['documentcode'] . '-' . $parameters['codenumber']);
                }
                $parameters['attach' . $i] = json_encode($file);
                $i++;
                if ($i >= 6) {
                    break;
                }
            }

            for ($x = $i; $x < 6; $x++) {
                $parameters['attach'.$x] = null;
            }

        }else{
            for ($x = 1; $x < 6; $x++) {
                $parameters['attach'.$x] = null;
            }
        }
        $leave->update($parameters);

        if($action == "resend") {
            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $leave['documentcode'].'-'.$leave['codenumber'],
                Session::get('firstname').' '.Session::get('middlename').'. '.Session::get('lastname'),
                Session::get('dept_name'),
                $leave['email'],
                $leave
            );

            if(Session::get("desig_level") == "president") {
                $message = "Leave request successfully sent to CHRD for processing.";
            }else{
                $message = "Leave request successfully sent to ".$curr_emp['send_to']." for approval.";
            }

            return Response::json([
                'success' => true,
                'message' => $message,
                'url' => URL::to("lrf/leaves"),
                200
            ]);

        }else{
            $message = "Leave request successfully saved and can be edit later.";
        }

        return Response::json([
            'success' => true,
            'message' => $message,
            'url' => URL::to("lrf/leaves"),
            200
        ]);
    }

	public function cancelRequest($id) {
		$requestLeaves = Leaves::where('id',$id)
			->where('ownerid',Session::get('employee_id'))
			->with('employee')
			->first();
        $curr_emp = $requestLeaves['curr_emp'];
		$validate = Validator::make(
			array(
				'lrfComment' => Input::get("lrfComment"),
			),
			array(
				'lrfComment' => 'required',
			),
			array(
				'lrfComment.required' => 'Please indicate reason for cancellation.',
			));

		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
		}

		if ($requestLeaves->status != "FOR APPROVAL") {
			goto invalidAction;
		}

		$paramsLeave = array(
			"leaveId" => $id,
			"status" => "CANCELLED",
			"nextApprover" => Session::get('employee_id'),
			"comment" => json_encode(array(
				'name'=>Session::get('employee_name'),
				'datetime'=>date('m/d/Y g:i A'),
				'message'=> Input::get("comment")
			))
		);

		$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);

		if ($updateLeaveStatus) {
            $this->sendMail(
                'CANCELLATION',
                'CANCELLED',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].'. '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $this->superiorEmail,
                $this->getEmpData($curr_emp)
            );
			return Redirect::to('lrf/leaves')
				->with('successMessage',"Leave request successfully cancelled.");
		}else{
			invalidAction:
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting leave.');
		}
	}
	
	public function processLeave($id)
	{
        if(! $id) {
            $id = explode('|',Input::get("lrfAction"))[1];
        }
		$action = Input::get('lrfAction');
		$otherSuperiors = Input::get('lrfOtherSuperiors');
        $requestLeaves = Leaves::where('id',$id)
            ->where('curr_emp',Session::get('employee_id'))
            ->with('employee')
            ->first();

        if(! $requestLeaves) {
            goto error;
        }

		if ($action == 'request') {
			$validate = Validator::make(
				    array(
				        'otherSuperiors' => Input::get('lrfOtherSuperiors')
				    ),
				    array(
				        'otherSuperiors' => 'required'
				    ),
                    array(
                        'otherSuperiors.required' => 'Please select Personnel from the drop-down list'
                    )
				);
			if ($validate->fails())
			{
				return Redirect::back()
			           ->withInput()
			           ->withErrors($validate)
			           ->with('message', 'Some fields are incomplete.');
				exit;

			}

			$paramsSignatory = array(
					"leaveId" => $id,
					"signatureType" => 1
				);

			$paramsLeave = array(		
					"leaveId" => $id,
					"status" => "FOR APPROVAL",
					"nextApprover" => $otherSuperiors,
					"comment" => json_encode(array(
											'name'=>Session::get('employee_name'),
											'datetime'=>date('m/d/Y g:i A'),
											'message'=>Input::get('lrfComment')
										))
				);

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].'. '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $this->getEmpData($otherSuperiors)['email'],
                $this->getEmpData($otherSuperiors)
            );

            $addSignatory = Signatory::store($paramsSignatory);
			$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);

        	// $this->logs->AU001($leaveBatch->id,'leavebatch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);

			if ($addSignatory && $updateLeaveStatus) {
				return Redirect::to("lrf/leaves")
			           ->with('successMessage', 'Leave request successfully sent to another superior for approval.');
				exit;
			}
            error:
			return Redirect::to("lrf/leaves")
		           ->with('errorMessage', 'Something went wrong upon submitting.');
			exit;
		}elseif ($action == 'send') {
			$hrReciever = Receivers::get("leaves","HR_LEAVE_RECEIVER",Session::get("company"));
            if(! $hrReciever) {
                return Redirect::back()
                    ->with("errorMessage","No HR_NOTIFICATION_RECEIVER declared.");
            }
            $paramsSignatory = array(
					"leaveId" => $id,
					"signatureType" => 2
				);
			$paramsLeave = array(		
					"leaveId" => $id,
					"status" => "APPROVED",
					"dateApproved" => date('Y-m-d'),
					"nextApprover" => $hrReciever['employeeid'],
					"comment" => json_encode(array(
											'name'=>Session::get('employee_name'),
											'datetime'=>date('m/d/Y g:i A'),
											'message'=>Input::get('lrfComment')
										))
				);

            $this->sendMail(
                'processing',
                'APPROVED',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].'. '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $hrReciever['employee']['email'],
                $this->getEmpData($hrReciever['employeeid'])
            );

			$addSignatory = Signatory::store($paramsSignatory);
			$updateLeaveStatus = Leaves::statusUpdate($paramsLeave);
			
			if ($addSignatory && $updateLeaveStatus) {

				if(in_array(Session::get("company"),json_decode(CORPORATE,true))) {
					return Redirect::to("lrf/leaves")
						->with('successMessage', "Leave request successfully sent to CHRD for processing.");
					exit;
				}
				return Redirect::to("lrf/leaves")
					->with('successMessage', "Leave request successfully sent to SHRD for processing.");
				exit;

			}
			return Redirect::back()
		           ->with('errorMessage', 'Something went wrong upon submitting.');
			exit;
		}elseif ($action == 'return') { //heto na
			$validate = Validator::make(
						array(
					        'lrfComment' => Input::get("lrfComment"),
					    ),
					    array(
					        'lrfComment' => 'required',
					    ),
					    array(
					        'lrfComment.required' => 'Please indicate reason for disapproval.',
					    ));

			if ($validate->fails())
		    {
		        return Redirect::back()
		           ->withInput()
		           ->withErrors($validate)
		           ->with('message', 'Some fields are incomplete.');
		    }

            $leave = Leaves::where("curr_emp", Session::get("employee_id"))
                ->with('signatories')
                ->where("id",$id)->first();
            if (! $leave)
            {
                return Redirect::to('submitted/leaves_notifications')
                    ->with('errorMessage', 'You\'ve looking for invalid notification.');
            }
            $leave->signatories()->delete();
            $leave->curr_emp = Input::get("lrfOwnerID");
            $leave->isdeleted = 0;
            $leave->status = "DISAPPROVED";
            $leave->comment = DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("lrfComment")
                ))
                ."')");
            $leave->save();


            return Redirect::to("lrf/leaves")
                   ->with('successMessage', "Leave successfully returned to Filer.");
		}elseif ($action == 'clinicToIS'){ //Satellite
			$paramsSignatory = array(
				"leaveId" => $id,
				"signatureType" => 1
			);
			$paramsLeave = array(		
				"leaveId" => $id,
				"ownerId" => Input::get("lrfOwnerID"),
				"status" => "FOR APPROVAL",
				"nextApprover" => "toClarify",
				"comment" => json_encode(array(
										'name'=>Session::get('employee_name'),
										'datetime'=>date('m/d/Y g:i A'),
										'message'=>Input::get('lrfComment')
									))
			);

            $updateLeaveStatus = Leaves::statusUpdate($paramsLeave);

            if($updateLeaveStatus === "ERROR003") {
                return Redirect::back()
                    ->with('errorMessage', 'No Immediate superior declared.');
                exit;
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $requestLeaves['documentcode'].'-'.$requestLeaves['codenumber'],
                $requestLeaves['firstname'].' '.$requestLeaves['middlename'].'. '.$requestLeaves['lastname'],
                $requestLeaves['department'],
                $requestLeaves['employee']['email'],
                $this->getEmpData($requestLeaves['ownerid'])
            );

            $addSignatory = Signatory::store($paramsSignatory);
            if ($addSignatory && $updateLeaveStatus) {
				return Redirect::to("lrf/leaves")
			           ->with('successMessage', 'Leave request successfully sent for approval.');
				exit;
			}
			return Redirect::to("lrf/leaves")
		           ->with('errorMessage', 'Something went wrong upon submitting.');
			exit;
		}
	}

	public function parseToInt($restDays)
	{
		$explodedRestDay = explode("&", $restDays);
		$intRestDays = array();
		$restDayConvertion = array(
				"Sunday" => 0,
				"Monday" => 1,
				"Tuesday" => 2,
				"Wednesday" => 3,
				"Thursday" => 4,
				"Friday" => 5,
				"Saturday" => 6
			);
		foreach ($explodedRestDay as $key) {
				array_push($intRestDays, $restDayConvertion[$key]);
		}
		return $intRestDays;
	}

	public function filterSearch()
	{
		$hasValues = [];
		foreach (Input::all() as $key => $value) {
		 	if ($value) {
		 		$hasValues[$key] = $value;
		 	}
	 	};

	 	return $hasValues;
		// if (Input::has("notificationType")) $parameters['department'] = Input::get("notificationType"); hide ko muna po ito
		

//		return $parameters;
	}

	public function routing()
	{
	    $company = Session::get("company");
	    $desig_level = Session::get("desig_level");
	    $superiorid = Session::get("desig_level");
		if ($company != "RBC-CORP") {
			if (Input::get('request_details')['leave_type'] == "Sick" && Input::get('request_details')['total_days'] > 3 || Input::get('request_details')['leave_type'] == "Mat/Pat"){
				$response = [
				    "id" => $this->getLeaveReceiver('clinic'),
                    "send_to" => "clinic",
                    "status" => "FOR APPROVAL"
				];
			}else{
                $dept = Session::get("dept_name");

                if(in_array($company,json_decode(MFC_ETC))) {
                    $response = [
                        "id" => $this->getDeptHead($superiorid)->id,
                        "send_to" => "Department Head",
                        "status" => "FOR APPROVAL"
                    ];
                }else{
                    $response = [
                        "id" => $superiorid,
                        "send_to" => "Immediate superior",
                        "status" => "FOR APPROVAL"
                    ];
                }

                if($dept == "OPERATION MANAGEMENT") {
                    $response = [
                        "id" => $superiorid,
                        "send_to" => "Immediate superior",
                        "status" => "FOR APPROVAL"
                    ];
                }

            }
		}else{
			if($desig_level == "head") {
                $response = [
                    "id" => $this->getEmpByDesigLevel("top mngt")->id,
                    "send_to" => "Top management",
                    "status" => "FOR APPROVAL"
                ];
            }elseif($desig_level =="top mngt") {
                $response = [
                    "id" => $this->getEmpByDesigLevel("president")->id,
                    "send_to" => "President",
                    "status" => "FOR APPROVAL"
                ];
            }elseif($desig_level =="president"){
                $response = [
                    "id" => $this->getLeaveReceiver(),
                    "send_to" => "HR",
                    "status" => "APPROVED"
                ];
            }else{
                $response = [
                    "id" => Session::get("superiorid"),
                    "send_to" => "Immediate superior",
                    "status" => "FOR APPROVAL"
                ];
            }
		}
		return $response;

	}

	public function PESaveLeave($id){
	    if($id) {
            $leaveBatch = LeaveBatch::find($id);
            LeaveBatchDetails::where('leave_batch_id',$id)->delete();
            $parameters = [
                'company' => Session::get("company"),
                'departmentid' => Input::get("department"),
                'sectionid' => Input::get("section"),
                'status' => 'NEW',
                'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                )),
                'datecreated' => date('Y-m-d'),
                'ownerid' => Session::get("employee_id"),
                'curr_emp' => 0
            ];
            $this->logs->AU004($leaveBatch->id,'leave_batch','id',json_encode($parameters),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],json_encode($leaveBatch));
            $leaveBatch->update($parameters);
        }else{
            $leaveBatch = new LeaveBatch();
            $leaveBatch->documentcode = date('Y-m');
            $leaveBatch->codenumber = (new LeaveBatch)->generateCodeNumber();
            $leaveBatch->company = Session::get("company");
            $leaveBatch->departmentid = Input::get("department");
            $leaveBatch->sectionid = Input::get("section");
            $leaveBatch->status = 'NEW';
            $leaveBatch->comment = json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get("comment")
            ));
            $leaveBatch->datecreated = date('Y-m-d');
            $leaveBatch->ownerid = Session::get("employee_id");
            $leaveBatch->curr_emp = 0;
            $leaveBatch->save();

            $this->logs->AU001($leaveBatch->id,'leave_batch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);
        }


        $batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);


        if ($leaveBatch->save() && $batchDetails) {
            return Redirect::to("lrf/leaves")
                ->with('successMessage', "Leave Successfully save");
            exit;
        }
        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting.');
        exit;
	}

	public function PECreateLeave(){
		$validate = Validator::make(
			array(
				'department' => Input::get('department'),
				'employees' => Input::get('employeesToFile')
			),
			array(
				'employees' => 'required|not_in:[]',
				'department' => 'required'
			),
			array(
				'employees.not_in' => 'Must add employee atleast one',
				'department.required' => 'Choose Department'
			)

		);
		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
			exit;

		}


		$leaveBatch = new LeaveBatch();
		$leaveBatch->documentcode = date('Y-m');
		$leaveBatch->codenumber = (new LeaveBatch)->generateCodeNumber();
		$leaveBatch->company = Session::get("company");
		$leaveBatch->departmentid = Input::get("department");
		$leaveBatch->sectionid = Input::get("section");
		$leaveBatch->status = 'FOR APPROVAL';
		$leaveBatch->comment = json_encode(array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=> Input::get("comment")
		));
		$leaveBatch->datecreated = date('Y-m-d');
		$leaveBatch->ownerid = Session::get("employee_id");
		$leaveBatch->curr_emp = Session::get("superiorid");
		$leaveBatch->save();

        $this->logs->AU001($leaveBatch->id,'leave_batch','id',json_encode($leaveBatch),$leaveBatch['documentcode'].'-'.$leaveBatch['codenumber']);
		$batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);
        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            date('Y-m').'-'.(new LeaveBatch)->generateCodeNumber(),
            Session::get('firstname').' '.Session::get('middlename').'. '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail,
            $this->getEmpData(Session::get("superiorid"))
        );

        if ($leaveBatch->save() && $batchDetails) {
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Leave request successfully sent for approval.");
			exit;
		}
		return Redirect::back()
			->with('errorMessage', 'Something went wrong upon submitting.');
		exit;
	}

    public function PECancelLeave($id) {
        $validate = Validator::make(
            array(
                'comment' => Input::get("comment"),
            ),
            array(
                'comment' => 'required',
            ),
            array(
                'comment.required' => 'Comment is required',
            ));

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        $leave = LeaveBatch::where('id',$id)
            ->where('ownerid',Session::get('employee_id'))
            ->where('status','FOR APPROVAL')
            ->with('owner')
            ->with('department');
        $leaveDetails = $leave->first();
        $this->logs->AU004(
            $leave->first()['id'],
            'leave_batch',
            'id',
            json_encode(array(
                'curr_emp' => 0,
                'status' => 'CANCELLED')),
            $leave->first()['documentcode'].'-'.$leave->first()['codenumber'],
            json_encode($leave->first())
        );

        if (count($leave->get()) == 0) {
            return Redirect::to('lrf/leaves')
                ->with('errorMessage', 'Invalid Process');
            exit;
        }
        $cancelLeave = $leave->update(array(
            'curr_emp' => 0,
            'status' => 'CANCELLED',
            'comment' => DB::raw("concat(comment,'|','".
                json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                ))
                ."')")
        ));
        if ($cancelLeave) {

            $this->sendMail(
                'CANCELLATION',
                'CANCELLED',
                $leaveDetails['documentcode'].'-'.$leaveDetails['codenumber'],
                $leaveDetails['owner']['firstname'].' '.$leaveDetails['owner']['middlename'].'. '.$leaveDetails['owner']['lastname'],
                $leaveDetails['department']['dept_name'],
                $this->superiorEmail,
                $this->getEmpData(Session::get("superiorid"))
            );

            return Redirect::to("lrf/leaves")
                ->with('successMessage', 'Leave successfully cancelled.');
        }
        return Redirect::to("lrf/leaves")
            ->with('errorMessage', 'Something went wrong upon submitting.');
    }

    public function PEResend($id)
    {
		$validate = Validator::make(
			array(
				'department' => Input::get('department'),
				'employees' => Input::get('employeesToFile')
			),
			array(
				'employees' => 'required|not_in:[]',
				'department' => 'required'
			),
			array(
				'employees.not_in' => 'Must add employee atleast one',
				'department.required' => 'Choose Department'
			)

		);
		if ($validate->fails())
		{
			return Redirect::back()
				->withInput()
				->withErrors($validate)
				->with('message', 'Some fields are incomplete.');
			exit;

		}

		LeaveBatchDetails::where('leave_batch_id',$id)->delete();
		$leaveBatch = LeaveBatch::find($id);
        $parameters = [
                'company' => Session::get("company"),
                'departmentid' => Input::get("department"),
                'sectionid' => Input::get("section"),
                'status' => 'FOR APPROVAL',
                'comment' => json_encode(array(
                    'name'=>Session::get('employee_name'),
                    'datetime'=>date('m/d/Y g:i A'),
                    'message'=> Input::get("comment")
                )),
                'datecreated' => date('Y-m-d'),
                'ownerid' => Session::get("employee_id"),
                'curr_emp' => Session::get("superiorid")
            ];

        $this->logs->AU004(
            $leaveBatch['id'],
            'leave_batch',
            'id',
            json_encode($parameters),
            $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
            json_encode($leaveBatch)
        );

		$leaveBatch->update($parameters);

		$batchDetails = (new LeaveBatchDetails())->newBatchDetails(Input::get("employeesToFile"),$leaveBatch->id);

        $this->sendMail(
            'checking',
            'FOR APPROVAL',
            $leaveBatch->documentcode.'-'.$leaveBatch->codenumber,
            Session::get('firstname').' '.Session::get('middlename').'. '.Session::get('lastname'),
            Session::get('dept_name'),
            $this->superiorEmail,
            $this->getEmpData(Session::get("superiorid"))
        );

        if ($leaveBatch->save() || $batchDetails) {
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Leave request successfully sent to another superior for approval.");
			exit;
		}
		return Redirect::back()
			->with('errorMessage', 'Something went wrong upon submitting.');
		exit;
    }

	public function PESend($id)
	{
        $validate = Validator::make(
            array('otherSuperior' => Input::get("otherSuperior")),
            array('otherSuperior' => 'required'),
            array('otherSuperior.required' => 'Please select Personnel from the drop-down list.')
        );
        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->with('errorMessage', 'Please select Personnel from the drop-down list.');
            exit;

        }

		DB::beginTransaction();
		try {
		    $leaveBatchDetails = LeaveBatchDetails::where("leave_batch_id",$id);
            $otherSuperiorsDetails = Employees::where('id',Input::get("otherSuperior"))
                ->first();
			$leaveBatch =  LeaveBatch::where("id",$id)
				->where('curr_emp',Session::get("employee_id"))
				->where('status',"FOR APPROVAL")
                ->with('department')
                ->with('owner')
				->first();
            if(!$leaveBatch) {
                goto error;
            }

            if((Input::get('empID') ? count(Input::get('empID')) : 0) < count($leaveBatch->batchDetails)) {
                DB::commit();
                return Redirect::back()
                    ->withInput()
                    ->with('errorMessage', 'Required to note all employees');
                exit;
            }

            $parameters = [
                'curr_emp'=>$otherSuperiorsDetails['id'],
                'status'=> 'FOR APPROVAL',
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $leaveBatchDetails->update(array("noted"=>0));

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                $leaveBatch['owner']['firstname'].' '.$leaveBatch['owner']['middlename'].'. '.$leaveBatch['owner']['lastname'],
                $leaveBatch['department']['dept_name'],
                $otherSuperiorsDetails['email'],
                $this->getEmpData($otherSuperiorsDetails['id'])
            );

			DB::commit();
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Leave request successfully sent to another superior for approval.");
			exit;


		}catch (Exception $e) {
		    error:
			DB::rollback();
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting.');
			exit;
		}
	}

	public function PESendToHR($id) {
		DB::beginTransaction();
        try {
            $hrReciever = Receivers::get("leaves","HR_LEAVE_RECEIVER",Session::get("company"));
            $leaveBatch =  LeaveBatch::where("id",$id)
				->with('batchDetails')
                ->with('owner')
                ->with('department')
				->where('curr_emp',Session::get("employee_id"))
				->where('status',"FOR APPROVAL")
				->first();

            if(!$leaveBatch) {
                goto error;
            }

            if((Input::get('empID') ? count(Input::get('empID')) : 0) < count($leaveBatch->batchDetails)) {
                DB::commit();
                return Redirect::back()
                    ->withInput()
                    ->with('errorMessage', 'Required to note all employees');
                exit;
            }
            
            $parameters = [
                'curr_emp' => $hrReciever["employeeid"],
                'status' => 'APPROVED',
                'dateapproved' => date('Y-m-d'),
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $this->sendMail(
                'processing',
                'APPROVED',
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                $leaveBatch['owner']['firstname'].' '.$leaveBatch['owner']['middlename'].'. '.$leaveBatch['owner']['lastname'],
                $leaveBatch['department']['dept_name'],
                $hrReciever['employee']['email'],
                $this->getEmpData($hrReciever["employeeid"])
            );


			DB::commit();
			return Redirect::to("lrf/leaves")
				->with('successMessage', "Leave request successfully sent to SHRD for processing.");
			exit;
		}catch (Exception $e) {
            error :
			DB::rollback();
			return Redirect::back()
				->with('errorMessage', 'Something went wrong upon submitting.');
			exit;
		}
	}

	public function PEReturn($id) {
        DB::beginTransaction();
        try {
            $leaveBatchDetails = LeaveBatchDetails::where("leave_batch_id",$id);
            $leaveBatch =  LeaveBatch::where("id",$id)
                ->where('curr_emp',Session::get("employee_id"))
                ->whereIn('status',array("FOR APPROVAL","RETURNED","APPROVED"))
                ->first();
            if(!$leaveBatch) {
                goto error;
            }

            $parameters = [
                'status' => "DISAPPROVED",
                'curr_emp'=> 0,
                'comment' => DB::raw("concat(comment,'|','".
                    json_encode(array(
                        'name'=>Session::get('employee_name'),
                        'datetime'=>date('m/d/Y g:i A'),
                        'message'=> Input::get("comment")
                    ))
                    ."')")
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);

            $leaveBatchDetails->update(array("noted"=>0));

            DB::commit();
            return Redirect::to("lrf/leaves")
                ->with('successMessage', "Leave request successfully returned to Filer.");
            exit;


        }catch (Exception $e) {
            error:
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting.');
            exit;
        }
    }

    public function PEDelete($id) {
        DB::beginTransaction();
        try {
            $leaveBatch =  LeaveBatch::where("id",$id)
//                ->with('batchDetails')
                ->where('ownerid',Session::get("employee_id"))
                ->first();
            if(!$leaveBatch) {
                goto error;
            }
            $parameters = [
                'status' => $leaveBatch['status']."/DELETED",
            ];

            $this->logs->AU004(
                $leaveBatch['id'],
                'leave_batch',
                'id',
                json_encode($parameters),
                $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'],
                json_encode($leaveBatch)
            );

            $leaveBatch->update($parameters);
            DB::commit();
            return Redirect::to("lrf/leaves")
                ->with('successMessage', "Leave request successfully deleted.");
            exit;


        }catch (Exception $e) {
            error:
            DB::rollback();
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong upon submitting.');
            exit;
        }
    }

    private function sendMail($action,$status,$refno,$filer,$department,$email,$receiver) {
        if  ($email) {
            $receiverName = $receiver['firstname'] . ' ' . $receiver['lastname'];
            if ($action == "CANCELLATION") {
                Mail::send('lrf.email.cancel', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function ($message) use ($email,$receiverName) {
                        $message->to($email,$receiverName)->subject('RGAS Notification Alert: Leave Cancellation');
                    }
                );
            } else {
                Mail::send('lrf.email.sample', array(
                    'action' => $action,
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function ($message) use ($email,$action, $receiverName) {
                        $message->to($email,$receiverName)->subject("RGAS Notification Alert: Leave for $action");
                    }
                );
            }
        }
    }

    private function getEmpData($id) {
        return Employees::where('id',$id)->first();
    }


    public function getDeptHead ($superiorId) {
        $personnel = Employees::where('id',$superiorId)->first(['desig_level','id']);
        if(! in_array($personnel['desig_level'],json_decode(HEAD_AND_ABOVE))) {
            while (! in_array($personnel['desig_level'],json_decode(HEAD_AND_ABOVE))) {
                $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
                $superiorId = $personnel['superiorid'];
            }
        }elseif (in_array($personnel['desig_level'],json_decode(HEAD_AND_ABOVE))){
            $personnel = Employees::where('id',$superiorId)->first(['desig_level','superiorid','id','firstname','middlename','lastname']);
        }
        
        return ($personnel ? $personnel : null);
    }

    private function getLeaveReceiver($type=null,$comp = null) {
        $company = $comp ? $comp : Session::get("company");
        if($type == "clinic") {
            return Receivers::where([
                "code" => "CLINIC_LEAVE_RECEIVER",
                "company" => $company
            ])->first()->employeeid;
        }else{
            return Receivers::where([
                "code" => "HR_LEAVE_RECEIVER",
                "company" => $company
            ])->first()->employeeid;
        }
    }

    private function getEmpByDesigLevel($desig_level,$company = null) {
	    return Employees::where([
	        'desig_level' => $desig_level,
	        'company' => $company,
            'active' => 1
        ])->first();
    }
}
?>
