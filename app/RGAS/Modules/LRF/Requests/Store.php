<?php namespace RGAS\Modules\LRF\Requests;

use Input;
use Validator;
use Session;
use Redirect;
use Response;

class Store extends RResult
{
	public function validate()
	{
	    $form_details = Input::get("form_details");
	    $request_details = Input::get("request_details");
        $inputs = [
            'EmployeeName' => $form_details['employee_name'],
            'EmployeeId' => $form_details['employee_number'],
            'DateFiled' => $form_details['date_filed'],
            'Company' => $form_details['company'],
            'Department' => $form_details['department'],
            'LeaveType' => $request_details['leave_type'],
            'TypeOfSchedule' => $request_details['schedule_type'],
            'DurationLeave' => $request_details['duration'],
            'TotalLeaveDays' => $request_details['total_days'],
            'RestDays' => $request_details['rest_day'],
            'Reason' => $request_details['reason'],
            'action' => Input::get('action')
        ];

        $rules = [
            'EmployeeName' => 'required',
            'EmployeeId' => 'required',
            'DateFiled' => 'required',
            'Company' => 'required',
            'Department' => 'required',
            'LeaveType' => 'required',
            'TypeOfSchedule' => 'required',
            'DurationLeave' => 'required',
            'DateFrom' => 'required|date_format:"Y-m-d"',
            'LeavePeriodFrom' => 'required',
            'TotalLeaveDays' => 'required',
            'Reason' => 'required',
            'action' => 'required',
            'RestDays' => 'required'
        ];

        $messages = [
            'EmployeeName.required' => 'Employee name is required',
            'EmployeeId.required' => 'Employee ID is required',
            'DateFiled.required' => 'Date filed is required',
            'Company.required' => 'Company is required',
            'Department.required' => 'Department is required',
            'LeaveType.required' => 'Leave type is required',
            'TypeOfSchedule.required' => 'Type of schedule is required',
            'DurationLeave.required' => 'Duration of leave is required',
            'DateFrom.required' => 'Date leave start is required',
            'DateFrom.date_format' => "The date leave start format is invalid ex. 2016-12-12",
            'LeavePeriodFrom.required' => 'Leave From period is required',
            'DateTo.required' => 'Date leave end is required',
            'DateTo.date_format' => "The date leave end format is invalid ex. 2016-12-12",
            'Reason.required' => 'Reason is required',
            'action.required' => 'Invalid action',
            'RestDays.required' => 'Rest day is required'
        ];

        if (Input::get('lrfLeaveType') == 'HomeVisit' ) {
            unset($inputs['TypeOfSchedule']);
            unset($inputs['RestDays']);
            unset($rules['TypeOfSchedule']);
            unset($rules['RestDays']);
            unset($messages['TypeOfSchedule.required']);
            unset($messages['RestDays.required']);
        }
        
        if ($request_details['duration'] == 'multiple' ) {
            $inputs['DateTo'] = $request_details['to_date'];
            $inputs['LeavePeriodTo'] = $request_details['to_period'];
            $rules['DateTo'] = 'required|date_format:"Y-m-d"';
            $rules['LeavePeriodTo'] = 'required';
            $messages['LeavePeriodTo.required'] = 'Leave To period is required';
            $messages['TotalLeaveDays.required'] = 'Total leave days is required';
        }


        if ($request_details['duration']) {
            $inputs['DateFrom'] = $request_details['from_date'];
            $inputs['LeavePeriodFrom'] = $request_details['from_period'];
        }


        if (($request_details['leave_type'] == "Sick" && $request_details['total_days'] >= 3) || $request_details['leave_type'] == "Mat/Pat"){
            $inputs['med_attachments'] = Input::get("med_attachments");
            $rules['med_attachments'] = 'required';
        }

		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);

        if ($validator->fails())
        {
            return [
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray(),
            ];
        }

        return [
            'success' => true,
        ];
	}
}
?>