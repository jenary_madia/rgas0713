<?php namespace RGAS\Modules\PMF\Requests;

use Input;
use Validator;
use Redirect;

class Store extends RResult
{
	public function validate($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'pmf' => 'required',
            'purpose' => 'required'
        ];


		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to($id ? "pmf/edit/$id" : 'pmf/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function assign($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'assign' => 'required'
        ];


		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to(  "pmf/receiver/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	public function validate_return($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'comment' => 'required'
        ];


		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to(  "pmf/approval/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	public function validate_return_submitted($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'comment' => 'required'
        ];


		$validator = Validator::make($input , $val_data);

		if ($validator->fails())
		{
			return Redirect::to(  "pmf/receiver/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_return_approve($id=null)
	{
		$input = Input::all();
		
		
		$val_data =  [
            'calibrated' => 'required'
        ];

        $messages['calibrated.required'] = 'Calibrated Rating is required';
		$validator = Validator::make($input , $val_data, $messages);

		if ($validator->fails())
		{
			return Redirect::to(  "pmf/receiver/$id" )
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}
}
?>