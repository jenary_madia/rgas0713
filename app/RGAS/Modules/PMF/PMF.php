<?php namespace RGAS\Modules\PMF;

use Session;
use RGAS\Libraries;
use Employees;
use Cache;
use Performance;
use DB;
use Input;
use Redirect;
use Config;
use ServiceVehicle;
use AuditTrails;
use Mail;
use URL;
use View;


class PMF extends PMFLogs implements IPMF
{
	//private $attachments_path = '/rgas/pmf';	
	public $cache_byEmployee;

	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_pmf');
		return $this->module_id;
	}
	
	public function create_save($sub)
	{	
		$reference = Performance::get_reference_no();

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;
		
		if($sub)
		{
		$parameters = array(
				'employee_id' => Input::get('employee_id'),
				'pmf_id' => Input::get('pmf'),
				'purpose' => Input::get('purpose'),
		 		'department_id' => Input::get('dept_id'),
		 		'department_hrms_cd' =>  Input::get('departmentid2'),
		 		'position' =>Input::get('designation'),
		 		'created_by' => Session::get('employee_id'),
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'status_id' => 1,
		 		'reference_no' => $reference,
		 		"attachments" => $attachments	
		 		);

			if(in_array(Session::get('desig_level') ,["president","top mngt"]))
			{
				$parameters["recommended_by"] =Session::get('employee_id');
				$parameters["recommended_date"] = date("Y-m-d H:i:s");
			}

			if(Session::get('desig_level') == "supervisor")
			{
				$parameters["noted_by"] =Session::get('employee_id');
				$parameters["completed_date"] = date("Y-m-d H:i:s");
			}
		}
		else
		{
			$parameters = array(
				'employee_id' => Session::get('employee_id'),
				'pmf_id' => Input::get('pmf'),
				'purpose' => Input::get('purpose'),
		 		'department_id' => Session::get('dept_id'),
		 		'department_hrms_cd' =>  Session::get('departmentid2'),
		 		'position' =>Session::get('designation'),
		 		'created_by' => Session::get('employee_id'),
		 		'updated_by' => Session::get('employee_id'),
		 		'acknowledged_by' =>  Session::get('employee_id'),
		 		'acknowledged_date' => date("Y-m-d H:i:s"), 
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'status_id' => 1,
		 		'reference_no' => $reference,
		 		"attachments" => $attachments	
		 		);
		}

		$store = Performance::create($parameters);

		//INSERT AUDIT
		$this->setTable('pmf_employees');
		$this->setPrimaryKey('pmf_employee_id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            //INSERT AUDIT
				$this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($store->id  ,  $file["original_filename"] ,  $reference  );
	      	}

	    endif;

		Performance::insert_detail($store->id ,$reference , 0);

		if ($store)
			return Redirect::to('pmf/report')
		           ->with('successMessage', 'PERFORMANCE MANAGEMENT SUCCESSFULLY SAVED.');
		else
			return Redirect::to('')
		           ->with('errorMessage', 'Something went wrong upon submitting pmf record.');
	}


	public function create_sent($sub)
	{	
	
		$reference = Performance::get_reference_no();

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;


	    //immediate superior
	    if(Session::get('desig_level') == "president")
		    $assign = Employees::where("pmf_users.employee_id",Session::get('employee_id'))
		    		->join("pmf_users","pmf_users.employee_id","=","id")
					->where("lvl" , 0)
					->where("status" , 1)
					->first(["firstname","lastname","email","id"]);
		else
			$assign = ServiceVehicle::superior( Session::get("superiorid")  );

		if( $assign["desig_level"] == "head" )
			$msg = "YOUR REQUEST HAS BEEN SUBMITTED TO YOUR DEPARTMENT HEAD FOR APPROVAL.";
		else
			$msg = "YOUR REQUEST HAS BEEN SUBMITTED TO YOUR SUPERIOR FOR APPROVAL.";
		


		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];
		$assign = $assign["id"];

		
		if($sub)
		{
		$parameters = array(
				'employee_id' => Input::get('employee_id'),
				'pmf_id' => Input::get('pmf'),
				'purpose' => Input::get('purpose'),
		 		'department_id' => Input::get('dept_id'),
		 		'department_hrms_cd' =>  Input::get('departmentid2'),
		 		'position' =>Input::get('designation'),
		 		'created_by' => Session::get('employee_id'),
		 		'current_approver_id' => Session::get('desig_level') == "president" ? 65535 : "",
		 		'current_id' => $assign,
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'status_id' => Session::get('desig_level') == "president" ? 4 : 3,
		 		'reference_no' => $reference,
		 		"attachments" => $attachments	
		 		);

			if(in_array(Session::get('desig_level') ,["president","top mngt"]))
			{
				$parameters["recommended_by"] =Session::get('employee_id');
				$parameters["recommended_date"] = date("Y-m-d H:i:s");
			}

			if(Session::get('desig_level') == "supervisor")
			{
				$parameters["noted_by"] =Session::get('employee_id');
				$parameters["completed_date"] = date("Y-m-d H:i:s");
			}
		}
		else
		{

			$parameters = array(
				'employee_id' => Session::get('employee_id'),
				'pmf_id' => Input::get('pmf'),
				'purpose' => Input::get('purpose'),
		 		'department_id' => Session::get('dept_id'),
		 		'department_hrms_cd' =>  Session::get('departmentid2'),
		 		'position' =>Session::get('designation'),
		 		'created_by' => Session::get('employee_id'),
		 		'current_id' => $assign,
		 		'updated_by' => Session::get('employee_id'),
		 		'acknowledged_by' =>  Session::get('employee_id'),
		 		'acknowledged_date' => date("Y-m-d H:i:s"), 
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'status_id' => 3,
		 		'reference_no' => $reference,
		 		"attachments" => $attachments	
		 		);
		}

		$store = Performance::create($parameters);

		//INSERT AUDIT
		$this->setTable('pmf_employees');
		$this->setPrimaryKey('pmf_employee_id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            //INSERT AUDIT
				$this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($store->id  ,  $file["original_filename"] ,  $reference  );
	      	}

	    endif;

		Performance::insert_detail($store->id ,$reference );


		Mail::send('pmf.email', array(
	                        'reference' => $reference,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "You have 1 Performance Management Form for approval." 
	                        ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF For Approval");
	                        }
	                    );


		if ($store) 
			return Redirect::to('pmf/report')
		           ->with('successMessage', $msg);
		else
			return Redirect::to('')
		           ->with('errorMessage', 'Something went wrong upon submitting pmf record.');
	}


	public function create_return()
	{	
	
		$reference = Performance::get_reference_no();

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;



		$name =Input::get('employee');
		$email = Input::get('email');
		$assign = Input::get('employee_id');

		
		$parameters = array(
				'employee_id' => Input::get('employee_id'),
				'pmf_id' => Input::get('pmf'),
				'purpose' => Input::get('purpose'),
		 		'department_id' => Input::get('dept_id'),
		 		'department_hrms_cd' =>  Input::get('departmentid2'),
		 		'position' =>Input::get('designation'),
		 		'created_by' => Session::get('employee_id'),

		 		'current_id' => $assign,
		 		'updated_by' => Session::get('employee_id'),
	
		 		'noted_by' => Session::get('employee_name') , //send to superior name
		 		
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'status_id' => 7,
		 		'reference_no' => $reference,
		 		"attachments" => $attachments	
		 		);


		$store = Performance::create($parameters);

		//INSERT AUDIT
		$this->setTable('pmf_employees');
		$this->setPrimaryKey('pmf_employee_id');
		$this->AU001($store->id , json_encode($parameters) , $reference );

		Performance::insert_detail($store->id ,$reference );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            //INSERT AUDIT
				$this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($store->id  ,  $file["original_filename"] ,  $reference  );
	      	}

	    endif;


	    Mail::send('pmf.email', array(
	                        'reference' => $reference ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "1 Performance Management Form  has been returned."),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF Returned");
	                        }
	                    );


		if ($store) 
			return Redirect::to('pmf/report')
		           ->with('successMessage', "THE PMF HAS BEEN RETURNED TO $name.");
		else
			return Redirect::to('')
		           ->with('errorMessage', 'Something went wrong upon submitting pmf record.');
	}


	public function edit_save($id)
	{	
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('reference') );
		      	
		      	$this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Performance::delete_attachment($id);

		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	
		 		);

			$emp = Session::get('employee_id');
			Performance::where("pmf_employee_id" , $id)
        	->whereRaw("(created_by = $emp || pmf_employees.employee_id = $emp)")
        	->wherein("status_id",[1,7])
        	->update($parameters);


        	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

        	Performance::insert_detail($id , Input::get('reference'), 0);

			return Redirect::to("pmf/edit/$id")
		           ->with('successMessage', 'PERFORMANCE MANAGEMENT REQUEST SUCCESSFULLY SAVED.');
	}

	public function edit_sent($id)
	{	
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('reference') );
	            $this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Performance::delete_attachment($id);


	    //immediate superior
	    if(Session::get('desig_level') == "president")
		    $assign = Employees::where("pmf_users.employee_id",Session::get('employee_id'))
		    		->join("pmf_users","pmf_users.employee_id","=","id")
					->where("lvl" , 0)
					->where("status" , 1)
					->first(["firstname","lastname","email","id"]);
		else
			$assign = ServiceVehicle::superior( Session::get("superiorid")  );

		if( $assign["desig_level"] == "head" )
			$msg = "YOUR REQUEST HAS BEEN SUBMITTED TO YOUR DEPARTMENT HEAD FOR APPROVAL.";
		else
			$msg = "YOUR REQUEST HAS BEEN SUBMITTED TO YOUR SUPERIOR FOR APPROVAL.";
		


		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];
		$assign = $assign["id"];


		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	,
		 		'current_approver_id' => Session::get('desig_level') == "president" ? 65535 : "",
		 		'current_id' => $assign,
		 		'status_id' => Session::get('desig_level') == "president" ? 4 : 3,
		 		);

			$emp = Session::get('employee_id');
			Performance::where("pmf_employee_id" , $id)
        	->whereRaw("(created_by = $emp || pmf_employees.employee_id = $emp)")
        	->wherein("status_id",[1,7])
        	->update($parameters);


        	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

        	Performance::insert_detail($id , Input::get('reference'));

        	Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "You have 1 Performance Management Form for approval." ),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF For Approval");
	                        }
	                    );

			return Redirect::to("pmf/report")
		           ->with('successMessage', $msg);
	}


	public function edit_return($id)
	{	

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . $reference );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;



		$name =Input::get('employee');
		$email = Input::get('email');
		$assign = Input::get('employee_id');

		
		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	,
		 		'current_approver_id' =>$assign,
		 		'current_id' => $assign,
		 		'status_id' =>7,
		 		);


		$emp = Session::get('employee_id');
		$store=	Performance::where("pmf_employee_id" , $id)
        	->whereRaw("(created_by = $emp)")
        	->wherein("status_id",[1,7])
        	->update($parameters);

       	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
    	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

		Performance::insert_detail($id , Input::get('reference')  );

		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            //INSERT AUDIT
				$this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id  ,  $file["original_filename"] , Input::get('reference')  );
	      	}

	    endif;


	    Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "1 Performance Management Form  has been returned."),
	                        function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF Returned");
	                        }
	                    );


		if ($store) 
			return Redirect::to('pmf/report')
		           ->with('successMessage', "THE PMF HAS BEEN RETURNED TO $name.");
		else
			return Redirect::to('')
		           ->with('errorMessage', 'Something went wrong upon submitting pmf record.');
	}

	public function approval_save($id)
	{	
		
		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('reference') );
	            $this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Performance::delete_attachment($id);

		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	
		 		);

			Performance::where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->where("status_id", 3)
        	->update($parameters);


        	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

        	Performance::insert_detail($id , Input::get('reference'), 0);

			return Redirect::to("pmf/report")
		           ->with('successMessage', "PERFORMANCE MANAGEMENT SUCCESSFULLY SAVED." );
	}

	public function approval_send($id)
	{	
		$status = 3;
		if(Input::get('action') == "send"):
	    	$assign = ServiceVehicle::superior( Session::get("superiorid")  );

	    elseif(Input::get('action') == "hr"):
		    $assign = Employees::join("pmf_users","pmf_users.employee_id","=","id")
					->where("lvl" , 0)
					->where("status" , 1)
					->first(["firstname","lastname","email","id"]);
			$status = 4;

		else:
			$assign = Employees::where("departmentid",Session::get('dept_id'))
				->where("desig_level" , "head")
				->first(["firstname","lastname","email","id","desig_level"]);
		endif;

		if( ! $assign["desig_level"]  )
			$msg = "PMF HAS BEEN SUBMITTED TO HR FOR FINAL APPROVAL.";
		elseif($assign["desig_level"]  == "head")
			$msg = 'YOUR REQUEST HAS BEEN SUBMITTED TO YOUR DEPARTMENT HEAD FOR APPROVAL.';
		else
			$msg = "YOUR REQUEST HAS BEEN SUBMITTED TO YOUR SUPERIOR FOR APPROVAL.";

		$name = $assign["firstname"] . " " . $assign["lastname"];
		$email = $assign["email"];
		$assign = $assign["id"];


		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('reference') );
	            $this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Performance::delete_attachment($id);

		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	,
		 		"current_id" => $assign,
		 		'current_approver_id' => $assign,
		 		"status_id" => $status
		 		);

		if(in_array(Session::get('desig_level') ,["president","top mngt","head"]))
		{
			$parameters["recommended_by"] =Session::get('employee_id');
			$parameters["recommended_date"] = date("Y-m-d H:i:s");
		}

		if(Session::get('desig_level') == "head")
		{
			$parameters["reviewed_by"] =Session::get('employee_id');
			$parameters["reviewed_date"] = date("Y-m-d H:i:s");
		}

		if(Session::get('desig_level') == "supervisor")
		{
			$parameters["noted_by"] =Session::get('employee_id');
			$parameters["completed_date"] = date("Y-m-d H:i:s");
		}


			Performance::where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->whereIn("status_id", [3,7])
        	->update($parameters);


        	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

        	Performance::insert_detail($id , Input::get('reference'));

        	//INSERT AUDIT
			$old = ["current_id"=> Session::get("employee_id"),"current_approver_id"=> Session::get("employee_id"), "status_id"  => $status];
			$new = ["current_id"=>  $assign,"current_approver_id"=>  $assign,"status_id" => $status];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );


			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => $status == 3 ? "You have 1 Performance Management Form for approval." : "You have 1 Performance Management Form for processing."),
	                        function($message ) use ($email ,  $name ,$status)
	                        {
	                            $message->to( $email, $name )
	                            	->subject($status == 4 ? "RGAS Notification Alert: PMF For Processing"  : "RGAS Notification Alert: PMF For Approval");
	                        }
	                    );

			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' =>  "Your Performance Management Form has been submitted to $name"),
	                        function($message )
	                        {
	                            $message->to(  Input::get('email') , Input::get('employee') )
	                            	->subject("RGAS Notification Alert: PMF For Approval");
	                        }
	                    );

			//supervisor
			$sup = ServiceVehicle::superior( Input::get("superior")  );
			$supname = $sup["firstname"] . " " . $sup["lastname"];
			$supemail = $sup["email"];
			$supassign = $sup["id"];
			if( Session::get("employee_id")!= $supassign)
			{
				Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "The Performance Management Form of " . Input::get('employee') . " has been approved by " . Session::get("full_name") ),
	                         function($message ) use ($supemail ,  $supname )
	                        {
	                            $message->to( $supemail, $supname )
	                            	->subject("RGAS Notification Alert: PMF For Approval");
	                        }
	                    );
			}
			

			return Redirect::to("pmf/report")
		           ->with('successMessage', $msg );
	}

	public function approval_return($id)
	{	
		if(Input::get("superiorid") == Session::get("employee_id"))
		{
			$name =Input::get("employee_name");
			$email = Input::get("email");
			$assign = Input::get("employee_id");

			$status = 7;
			$msg = "PERFORMANCE MANAGEMENT REQUEST SUCCESSFULLY RETURNED.";
		}
		else
		{
			$assign = ServiceVehicle::superior( Input::get("superiorid")  );
			$name = $assign["firstname"] . " " . $assign["lastname"];
			$email = $assign["email"];
			$superior = $assign["superiorid"];
			$assign = $assign["id"];

			while($superior != Session::get("employee_id"))
			{
				$assign = ServiceVehicle::superior( $superior  );
				$name = $assign["firstname"] . " " . $assign["lastname"];
				$email = $assign["email"];
				$superior = $assign["superiorid"];
				$assign = $assign["id"];			
			}	

			//$status = 3;
			//jma change 3 to 7
			$status = 7;
			$msg = "THE PMF HAS BEEN RETURNED TO " . ucwords(strtolower($name)) . ".";
		}


		//insert attach
    	$files = Input::get("files");
        if(!empty($files)):
	        $fm = new Libraries\FileManager;

	    	foreach($files as $file)
	        {
	            $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('reference') );
	            $this->setTable('pmf_employees');
				$this->setPrimaryKey('pmf_employee_id');
				$this->AU002($id ,  $file["original_filename"] , Input::get("reference") );
	      	}

	      	 $attachments = json_encode($files);
	    else:
	    	 $attachments = "";
	    endif;

	    Performance::delete_attachment($id);

		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"attachments" => $attachments	,
		 		"current_id" => $assign,
		 		'current_approver_id' => $assign,
		 		"status_id" => $status
		 		);

			Performance::where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->whereIn("status_id", [3,7])
        	->update($parameters);


        	DB::table("pmf_employee_totals")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_policies_and_procedures")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_discipline")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_purposes")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_core_values")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kpis")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_kras")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_recommendations")->where(["pmf_employee_id"=>$id])->delete();
        	DB::table("pmf_employee_comments")->where(["pmf_employee_id"=>$id])->delete();

        	Performance::insert_detail($id , Input::get('reference'));

        	//INSERT AUDIT
			$old = ["current_id"=> Session::get("employee_id"),"current_approver_id"=> Session::get("employee_id"), "status_id"  => 3];
			$new = ["current_id"=>  $assign,"current_approver_id"=>  $assign,"status_id" => $status];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "1 Performance Management Form  has been returned."),
	                         function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF Returned");
	                        }
	                    );


			return Redirect::to("pmf/report")
		           ->with('successMessage', $msg );
	}


	public function submitted_return($id)
	{	
		    $record = Performance::join("employees","pmf_employees.created_by" , "=" , "employees.id")
	                ->where("pmf_employee_id" , $id )
	              	->where("status_id",4)
	              	->where("current_id" , Session::get("employee_id"))
	                ->get(["firstname","email","lastname","superiorid","pmf_employees.employee_id","created_by"]);

	        if($record[0]->employee_id == $record[0]->created_by)
	        {
	        	$assign = ServiceVehicle::superior(  $record[0]->superiorid   );
				$name = $assign["firstname"] . " " . $assign["lastname"];
				$email = $assign["email"];
				$assign = $assign["id"];
	        }
	        else
	        {
	        	$name = $record[0]->firstname . " " . $record[0]->lastname;
				$email = $record[0]->email;
				$assign = $record[0]->created_by;
	        }

		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"current_id" => $assign,
		 		'current_approver_id' => $assign,
		 		"status_id" => 7,
		 		'checker_id' =>  DB::raw("concat(checker_id , ?)")		 	
		 		);

			Performance::addBinding( "," . Session::get('employee_id') . ","   )->where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->where("status_id", 4)
        	->update($parameters);

        	$comments =  array(
                        "employee_id" => Session::get('employee_id'),
                        "comment" => Input::get('message') .  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    );

        	 DB::table("pmf_employee_comments")
        	 	->where("pmf_employee_id",$id)
        	 	->where("designation","general")
        	 	->update(  $comments ); 

        	//INSERT AUDIT
			$old = ["current_id"=> Session::get("employee_id"),"current_approver_id"=> Session::get("employee_id"), "status_id"  => 4];
			$new = ["current_id"=>  $assign,"current_approver_id"=>  $assign,"status_id" => 7];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "1 Performance Management Form  has been returned."),
	                         function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF Returned");
	                        }
	                    );


			return Redirect::to("pmf/submitted")
		           ->with('successMessage', "THE PMF HAS BEEN REVIEWED AND SENT BACK TO THE CONCERNED IMMEDIATE SUPERIOR." );
	}


	public function submitted_approve($id)
	{	
		    $record = Performance::join("employees","pmf_employees.employee_id" , "=" , "employees.id")
	                ->where("pmf_employee_id" , $id )
	              	->where("status_id",4)
	              	->where("current_id" , Session::get("employee_id"))
	                ->get(["firstname","email","lastname","superiorid","employees.id"]);

	        $name = $record[0]->firstname . " " . $record[0]->lastname;
			$email = $record[0]->email;
			$assign = $record[0]->id;

	        $sub = ServiceVehicle::superior(  $record[0]->superiorid   );
			$sub_name = $sub["firstname"] . " " . $assign["lastname"];
			$sub_email = $sub["email"];
			$sub_assign = $sub["id"];

	        
		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"current_id" => $assign,
		 		'current_approver_id' => $assign,
		 		"status_id" => 11,
		 		'calibrated_rating' => Input::get('calibrated'),
		 		'score' => Input::get('totalscore'),
		 		'classification' => Input::get('classification'),
		 		'approved' => 1,

		 		);


			Performance::where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->where("status_id", 4)
        	->update($parameters);


        	$comments =  array(
                        "employee_id" => Session::get('employee_id'),
                        "comment" => Input::get('message') .  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    );

        	 DB::table("pmf_employee_comments")
        	 	->where("pmf_employee_id",$id)
        	 	->where("designation","general")
        	 	->update(  $comments ); 

        	//INSERT AUDIT
			$old = ["current_id"=> Session::get("employee_id"), "current_approver_id"=> Session::get("employee_id"), "status_id"  => 4];
			$new = ["current_id"=>  $assign,"current_approver_id"=>  $assign,"status_id" => 11];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' => "Your Performance Management Form has been approved"),
	                         function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF Approved");
	                        }
	                    );

			//0522 JMA REMOVE EMAIL FOR IMMEDIATE WHEN APPROVED
			// Mail::send('pmf.email', array(
	  //                       'reference' => Input::get('reference') ,
	  //                       'filer' => Input::get('employee'),
	  //                       'department' => Input::get('dept_name') ,
	  //                       'body' => "RGAS Notification Alert: PMF Approved"),
	  //                        function($message ) use ($sub_email ,  $sub_name )
	  //                       {
	  //                           $message->to( $sub_email, $sub_name )
	  //                           	->subject("RGAS Notification Alert: PMF has been Approved");
	  //                       }
	  //                   );

			return Redirect::to("pmf/submitted")
		           ->with('successMessage', "PMF RECORD HAS BEEN CALIBRATED." );
	}

	public function submitted_assign($id)
	{	
		 $record = Employees::where("id" , Input::get('assign') )
            	->get(["firstname","lastname","email", "id"]);

		$name = $record[0]->firstname . " " . $record[0]->lastname;
		$email = $record[0]->email;
		$assign = $record[0]->id;

	        
		$parameters = array(				
		 		'updated_by' => Session::get('employee_id'),
		 		'ts_updated' => date("Y-m-d H:i:s"),
		 		"current_id" => $assign,
		 		'current_approver_id' => $assign
		 		);

			Performance::where("pmf_employee_id" , $id)
        	->where("current_id" , Session::get("employee_id"))
        	->where("status_id", 4)
        	->update($parameters);

        	$comments =  array(
                        "employee_id" => Session::get('employee_id'),
                        "comment" => Input::get('message') .  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    );

        	 DB::table("pmf_employee_comments")
        	 	->where("pmf_employee_id",$id)
        	 	->where("designation","general")
        	 	->update(  $comments ); 

        	//INSERT AUDIT
			$old = ["current_id"=> Session::get("employee_id"),"current_approver_id"=> Session::get("employee_id")];
			$new = ["current_id"=>  $assign,"current_approver_id"=>  $assign];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

			Mail::send('pmf.email', array(
	                        'reference' => Input::get('reference') ,
	                        'filer' => Input::get('employee'),
	                        'department' => Input::get('dept_name') ,
	                        'body' =>  "You have 1 Performance Management Form for processing."),
	                         function($message ) use ($email ,  $name )
	                        {
	                            $message->to( $email, $name )
	                            	->subject("RGAS Notification Alert: PMF For Processing");
	                        }
	                    );

			return Redirect::to("pmf/submitted")
		           ->with('successMessage', "PMF RECORD HAS BEEN DELEGATED TO YOUR STAFF." );
	}

	public function get_record()
	{	
			$result_data = array();
			$requests = Performance::where("employee_id" , Session::get("employee_id"))
				->join("employees" , "employees.id","=","current_id","left")
				->join("pmf" , "pmf.pmf_id","=","pmf_employees.pmf_id")
				->whereNull("isdeleted")
				->get(["year","current_id","firstname","lastname","reference_no","ts_created" , "period","pmf_employee_id as id","status_id","created_by"]);

	    	if ( count($requests) > 0)
	        {
	        	$status[1] = "NEW";
	        	$status[3] = "FOR APPROVAL";
	        	$status[4] = "SUBMITTED";
	        	$status[7] = "RETURNED";
	        	$status[11] = "CALIBRATED";

	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disable_edit = ($req->status_id == 7 && ($req->current_id ==  Session::get('employee_id') || $req->current_id ==0) ) || ($req->status_id == 1 &&  $req->created_by ==  Session::get('employee_id')) ? "" : "disabled";
	                    $disable_delete = in_array($req->status_id, [1,11]) ? "" : "disabled";
	                    
	                    $result_data[$ctr][] = $req->reference_no;
	    				$result_data[$ctr][] = date("Y-m-d",strtotime($req->ts_created ));
	    				$result_data[$ctr][] = strtoupper( $req->year . " (" . $req->period . ")" );
	    				$result_data[$ctr][] = $status[$req->status_id];
	    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr][] = "
	    					<button  $disable_delete type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/delete/$req->id") . "'\" >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("pmf/edit/$req->id?page=view") . "' >VIEW</a>
	    					<button $disable_edit type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/edit/$req->id") . "'\" >EDIT</button>
	    					";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	public function submitted_record()
	{	
			$result_data = array();
			$requests = Performance::join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
				->join("pmf" , "pmf.pmf_id","=","pmf_employees.pmf_id")
				->join("pmf_purposes" , "pmf_purposes.purpose_id","=","pmf_employees.purpose")
				->join("departments" , "departments.id","=","e.departmentid","left")
				->whereNull("isdeleted")
				->get(["pmf_purposes.purpose","year","dept_name","current_id","checker_id","e.firstname as efirstname","e.lastname as elastname" ,"reference_no","ts_created" , "period","pmf_employee_id as id","status_id"]);

	    	if ( count($requests) > 0)
	        {
	        	$status[1] = "NEW";
	        	$status[3] = "FOR APPROVAL";
	        	$status[4] = "SUBMITTED";
	        	$status[7] = "RETURNED";
	        	$status[11] = "CALIBRATED";

	                $ctr = 0;
	                foreach($requests as $req) 
	                 {					                    
	                    $result_data[$ctr][] = $req->reference_no;
	                    $result_data[$ctr][] = $req->efirstname . " " . $req->elastname;
	    				$result_data[$ctr][] = date("Y-m-d",strtotime($req->ts_created ));
	    				$result_data[$ctr][] = strtoupper( $req->year . " (" . $req->period . ")" );
	    				$result_data[$ctr][] = $status[$req->status_id];
	    				$result_data[$ctr][] = "<a ' class='btn btn-default btndefault' href='" . url("pmf/receiver/$req->id?page=view") . "' >VIEW</a>";
	    				$result_data[$ctr][] = "<a ' class='btn btn-default btndefault' href='" . url("pmf/receiver/$req->id") . "' >APPROVE</a>";
	    				$result_data[$ctr][] = $req->current_id;
	    				$result_data[$ctr][] = $req->dept_name;
	    				$result_data[$ctr][] = strtoupper($req->purpose);
	    				$result_data[$ctr][] = $req->checker_id;
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}


	public function sub_record()
	{	
			$result_data = array();
			$requests = Performance::where("employee_id" , "!=" , Session::get("employee_id"))
				->join("employees as c" , "c.id","=","current_id","left")
				->join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
				->join("pmf" , "pmf.pmf_id","=","pmf_employees.pmf_id")
				->where("created_by" , Session::get("employee_id"))
				->whereNull("isdeleted")
				->get(["year","current_id","e.firstname as efirstname","e.lastname as elastname", "c.firstname as firstname","c.lastname as lastname","reference_no","ts_created" , "period","pmf_employee_id as id","status_id"]);

	    	if ( count($requests) > 0)
	        {
	        	$status[1] = "NEW";
	        	$status[3] = "FOR APPROVAL";
	        	$status[4] = "SUBMITTED";
	        	$status[7] = "RETURNED";
	        	$status[11] = "CALIBRATED";

	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disable_edit = ($req->status_id == 7 && $req->current_id ==  Session::get('employee_id')) || $req->status_id == 1 ? "" : "disabled";
	                    $disable_delete = in_array($req->status_id, [1,11]) ? "" : "disabled";
	                    
	                    $result_data[$ctr][] = $req->reference_no;
	                    $result_data[$ctr][] = $req->efirstname . " " . $req->elastname;
	    				$result_data[$ctr][] = date("Y-m-d",strtotime($req->ts_created ));
	    				$result_data[$ctr][] = strtoupper( $req->year . " (" . $req->period . ")" );
	    				$result_data[$ctr][] = $status[$req->status_id];
	    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr][] = "
	    					<button  $disable_delete type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/delete/$req->id") . "'\" >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("pmf/edit/$req->id?page=view") . "' >VIEW</a>
	    					<button $disable_edit type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/edit/$req->id") . "'\" >EDIT</button>
	    					";
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}


	public function forapproval_record()
	{	
			$result_data = array();
			$requests = Performance::where("current_id" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
				->join("departments" , "departments.id","=","e.departmentid","left")
				->join("pmf" , "pmf.pmf_id","=","pmf_employees.pmf_id")
				->whereIn("status_id" , [3,7])
				->get(["year","e.firstname as efirstname","e.lastname as elastname" , "reference_no",
					"ts_created" , "period","pmf_employee_id as id","status_id","dept_name"]);

	    	if ( count($requests) > 0) 
	        {
	        		$stats[3] = "FOR APPROVAL";
	        		$stats[7] = "RETURNED";

	                $ctr = 0;
	                foreach($requests as $req)
	                {
	                    $result_data[$ctr][] = $req->reference_no;
	                    $result_data[$ctr][] = $req->efirstname . " " . $req->elastname;
	    				$result_data[$ctr][] = date("Y-m-d",strtotime($req->ts_created ));
	    				$result_data[$ctr][] = strtoupper( $req->year . " (" . $req->period . ")" );
	    				$result_data[$ctr][] = $stats[$req->status_id];
	    				$result_data[$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("pmf/approval/$req->id?page=view") . "' >VIEW</a>
	    					<button  type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/approval/$req->id") . "'\" >APPROVE</button>
	    					";
	    				$result_data[$ctr][] = $req->dept_name;
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}


	public function get_reviewer()
	{	
			$result_data["aaData"] = array();
			$requests = Performance::where("current_id" , Session::get("employee_id"))
				->join("employees as e" , "e.id","=","pmf_employees.employee_id","left")
				->join("departments" , "departments.id","=","e.departmentid","left")
				->join("pmf" , "pmf.pmf_id","=","pmf_employees.pmf_id")
				->whereIn("status_id" , [3,7])
				->limit(5)
				->get(["year","e.firstname as efirstname","e.lastname as elastname" , "reference_no",
					"ts_created" , "period","pmf_employee_id as id","status_id","dept_name"]);
		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {
	        		$stats[3] = "FOR APPROVAL";
	        		$stats[7] = "RETURNED";
	                 $ctr = 0;
	                foreach($requests as $req)
	                {
	                    $result_data["aaData"][$ctr][] = $req->reference_no;
	                    $result_data["aaData"][$ctr][] = $req->efirstname . " " . $req->elastname;
	    				$result_data["aaData"][$ctr][] = date("Y-m-d",strtotime($req->ts_created ));
	    				$result_data["aaData"][$ctr][] = strtoupper( $req->year . " (" . $req->period . ")" );
	    				$result_data["aaData"][$ctr][] =  $stats[$req->status_id];
	    				$result_data["aaData"][$ctr][] = "
	    					<a ' class='btn btn-default btndefault' href='" . url("pmf/approval/$req->id?page=view") . "' >VIEW</a>
	    					<button  type='button'  class='btn btn-default btndefault' onClick=\"window.location='" . url("pmf/approval/$req->id") . "'\" >APPROVE</button>
	    					"; 
	                    $ctr++;
	                 }         
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}


	public function form_print($id)
	{	
		$data["base_url"] = URL::to("/");       
        $data["record"] =  Performance::submmited_record($id);
        $data["id"] = $id;  
        
       
        $data["kra_cluster"] =  Performance::cluster( $data["record"]["pmf"] );
        $data["behavior"] =  Performance::behavior( $data["record"]["pmf"] );
        $data["build"] =  Performance::build( $data["record"]["pmf"] );
        $data["reccomendation"] =  Performance::reccomendation( $data["record"]["pmf"]  );

       
        ob_start();  

         echo View::make("pmf.print", $data);

        $output = ob_get_clean();

        $path = 'assets/files/pmf/';
        
        $prnt_tstamp = date('YmdHis',time());
        $user = Session::get('employee_name');
        $fname = "PMF_$user" . "_$prnt_tstamp";
        $c = $path . $fname. '.html';
        $o = $path . $fname. '.pdf';

        file_put_contents($c ,$output);

        $fle = "wkhtmltopdf --dpi 125 --disable-external-links --disable-internal-links --orientation portrait --margin-top 12.7mm --margin-right 6.35mm --margin-bottom 12.7mm --margin-left 6.35mm --page-width 215.9mm --page-height 330.2mm \"$c\" \"$o\"";
        
         shell_exec($fle);

        //INSERT AUDIT
        $parameters["pmf_employee_id"] = $id ;
        $this->AU006(json_encode($parameters) , $fname . ".pdf" );

         return Redirect::to("/$o");

	}



	public function deleterecord($id)
	{	
		$store =Performance::where("pmf_employee_id",$id)
			->where("created_by" , Session::get("employee_id"))
			->wherein("status_id" , [1,11] )
			->update(["isdeleted"=>1]);
	                 
		if($store)
		{		
			 $rec =Performance::where("pmf_employee_id",$id)
			->where("created_by" , Session::get("employee_id"))
			->wherein("status_id" , [1,11] )
			 ->get(["reference_no"])->first();

			//INSERT AUDIT
			$old = ["isdeleted"=> 0];
			$new = ["isdeleted"=> 1];
			$this->setTable('pmf_employees');
			$this->setPrimaryKey('pmf_employee_id');
			$this->AU004($id , json_encode($old),  json_encode($new) , $rec["reference_no"] );

			return Redirect::to("pmf/report")
		           ->with('successMessage', 'PMF RECORD SUCCESSFULLY DELETED.');
		}
		else
			return Redirect::to("pmf/report")
		           ->with('errorMessage', 'Something went wrong upon submitting pmf record.');
		
	}


	/****************************************************************************************/


	public function getSubordinate()
	{
		$requests = Employees::where("company","RBC-CORP")
				->get(["new_employeeid","firstname","lastname","sectionid","departmentid2","superiorid","id"]);

        foreach($requests as $req) 
        {				
           $result_data[$req->superiorid][$req->id] = implode("/", array($req->id , $req->firstname . " " .  $req->lastname));
          // $cache_employee[$req->id] = $req->firstname . " " . $req->lastname;
        }

        $this->employees = $result_data;
        $this->recursion( Session::get("employee_id"));

        //PASS ALL RECORDS TO CACHE VARIABLE
        Cache::put("cache_Employee",  $this->cache_byEmployee , 1440);
	}

	//RETRIEVE ALL SUBORDINATE EMPLOYEE
	public function recursion($eid)
	{
		if(isset($this->employees[$eid]))
		{
			$emp = $this->employees[$eid];
			foreach($emp as $id => $param)
			{
				 $erecords = explode("/", $param);
				 $this->cache_byEmployee[ $erecords[0] ] = $erecords[1];

				 if(!in_array(Session::get('desig_level') ,["president" , "top mngt"]))
				 	$this->recursion( $erecords[0] );
			}
		}
	}
}
?>