<?php namespace RGAS\Modules\GD;

use Session;
use RGAS\Libraries;
use Input;
use GeneralDocuments;
use DB;
use Redirect;
use Employees;
use ClearanceCertification;
use RGAS\Libraries\AuditTrail;
use Config;
use Mail;
use FPDI;

class GD extends GDLogs implements IGD 
{
	   private function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'gd/';
    }

    public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_gd');
		return $this->module_id;
	}

	
	public function gd_save()
	{	
		$parameters = array(
		 		'reference_no' => Input::get('reference'),
		 		'created_by' => Session::get("employee_id"),
		 		'employee_no' => Session::get("employeeid"),
		 		'current_id' => "",
		 		'department_id' => Session::get("dept_id"),
		 		'trans_date' => date("Y-m-d"),
		 		'document_name' => Input::get('doc_name'),
		 		'document_type_id' => Input::get('doc_type'),
		 		'other_document_type' => Input::get('doc_others'),
		 		'confidential' => Input::get('confidential'),
		 		'section_id' => Input::get('section'),
		 		'contact_no' => Input::get('contact'),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d"),
		 		'comments_save' => Input::get('comment'),
		 		'status_id' => 1
		 		);

		$store =GeneralDocuments::create($parameters);

		//INSERT AUDIT
		$this->setTable('documents');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , Input::get('reference') );

		GeneralDocuments::InsertSignatory($store->id);
		
		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($store->id);

		if ($store) 
			return Redirect::to('gd/report')
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY CREATED.');
		else
			return Redirect::to('gd/create')
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function gd_sent()
	{
		$prepared = Input::get("txtPrepared");
		$parameters = array(
		 		'reference_no' => Input::get('reference'),
		 		'created_by' => Session::get("employee_id"),
		 		'employee_no' => Session::get("employeeid"),
		 		'current_id' => $prepared[0],
		 		'department_id' => Session::get("dept_id"),
		 		'trans_date' => date("Y-m-d"),
		 		'document_name' => Input::get('doc_name'),
		 		'document_type_id' => Input::get('doc_type'),
		 		'other_document_type' => Input::get('doc_others'),
		 		'confidential' => Input::get('confidential'),
		 		'section_id' => Input::get('section'),
		 		'contact_no' => Input::get('contact'),
		 		'ts_created' => date("Y-m-d H:i:s"),
		 		'ts_updated' => date("Y-m-d"),
		 		'status_id' => 2,
		 		'comments' => ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n",
		 		);

		$store =GeneralDocuments::create($parameters);

		//INSERT AUDIT
		$this->setTable('documents');
		$this->setPrimaryKey('id');
		$this->AU001($store->id , json_encode($parameters) , Input::get('reference') );

		GeneralDocuments::InsertSignatory($store->id);
	

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($store->id);

		if ($store) 
			return Redirect::to('gd/report')
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY SENT.');
		else
			return Redirect::to('gd/create')
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function edit_save($id)
	{	
		$prepared = Input::get("txtPrepared");
		$parameters = array(
		 		'reference_no' => Input::get('reference'),
		 		'current_id' => "",
		 		'trans_date' => date("Y-m-d"),
		 		'document_name' => Input::get('doc_name'),
		 		'document_type_id' => Input::get('doc_type'),
		 		'other_document_type' => Input::get('doc_others'),
		 		'confidential' => Input::get('confidential'),
		 		'section_id' => Input::get('section'),
		 		'contact_no' => Input::get('contact'),
		 		'ts_updated' => date("Y-m-d"),
		 		'comments_save' => Input::get('comment')
		 		);

		$store =GeneralDocuments::where("id" , $id)
        	->where("created_by" , Session::get("employee_id"))
        	->update($parameters);

        DB::table("document_signatories")->where("document_id" , $id)->delete();

		GeneralDocuments::InsertSignatory($id);

		//DELETE ATTACHMENT
		GeneralDocuments::delete_attachment($id);		

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($id);
		

		return Redirect::to("gd/report")
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY SAVED AND CAN BE EDITED LATER.');

	}

	
	public function edit_sent($id)
	{	
		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n";
		$prepared = Input::get("txtPrepared");
		$parameters = array(
		 		'reference_no' => Input::get('reference'),
		 		'current_id' => $prepared[0],
		 		'trans_date' => date("Y-m-d"),
		 		'document_name' => Input::get('doc_name'),
		 		'document_type_id' => Input::get('doc_type'),
		 		'other_document_type' => Input::get('doc_others'),
		 		'confidential' => Input::get('confidential'),
		 		'section_id' => Input::get('section'),
		 		'contact_no' => Input::get('contact'),
		 		'ts_updated' => date("Y-m-d"),
		 		'status_id' => 2,
		 		'comments_save' => "",
		 		'comments' => DB::raw("concat(comments , '$comment')")	,
		 		); 

		$store =GeneralDocuments::where("id" , $id)
        	->where("created_by" , Session::get("employee_id"))
        	->update($parameters);

        DB::table("document_signatories")->where("document_id" , $id)->delete();

		GeneralDocuments::InsertSignatory($id);
		
		//DELETE ATTACHMENT
		GeneralDocuments::delete_attachment($id);		

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($id);

		if ($store) 
			return Redirect::to("gd/report")
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY SENT.');
		else
			return Redirect::to("gd/edit/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function view_cancel($id)
	{	
		$notify =GeneralDocuments::where("document_id" , $id)
			->join("document_signatories","document_id","=","documents.id")
			->join("employees","employee_id","=","employees.id")
			->whereRaw("(approval_date is not null or current_id = employee_id)")
        	->where("created_by" , Session::get("employee_id"))
        	->whereIn("status_id" , [2,3,4,8,9,11])
        	->get(["firstname","lastname","email"]);

        //notify all user
        foreach($notify as $rs)
        {
        	//SEND EMAIL NOTIFICATION
			Mail::send('gd.email.cancel', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get("filer"),
	                        'department' => Input::get("dept_name")),
	                        function($message ) use ($rs)
	                        {
	                            $message->to( $rs->email, $rs->firstname . " " . $rs->lastname )
	                            	->subject("RGAS Notification Alert: Document for Cancelled");
	                        }
	                    );
        }

        //DELETE ATTACHMENT
		GeneralDocuments::delete_attachment($id);		

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($id);

        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n";
        $parameters = array(
		 		'status_id' => 10,
		 		'current_id'=>Session::get("employee_id"),
		 		'comments' =>  DB::raw("concat(comments , '$comment')")		 		
		 		);

		$store =GeneralDocuments::where("id" , $id)
        	->where("created_by" , Session::get("employee_id"))
        	->whereIn("status_id" , [2,3,4,8,9,11])
        	->update($parameters);


		if ($store) 
			return Redirect::to("gd/report")
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY CANCELLED.');
		else
			return Redirect::to("gd/view/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function review_approve($id)
	{	
	
		//avoid user fraud
		if(Input::get("holder") != Session::get("employee_id"))
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');

		$reviewer = DB::table("document_signatories")
			->join("employees","employee_id","=","employees.id")
			->join("departments","departmentid","=","departments.id")
			->join("employees as e","notify_id","=","e.id","left")
			->where("document_id" , $id)
			->get(["signature_type","signature_type_seq","employee_id","notify_id",
				"employees.firstname" , "employees.lastname" , "employees.email","e.email as e_email",
				"e.firstname as e_firstname" , "e.lastname as e_lastname","approval_date","dept_code"]);
		
		$signatory = array();
		$current = "";
		$president_stats = "";
		$president = "";
		foreach($reviewer as $rs)
		{
			if(Session::get("employee_id") == $rs->employee_id) $current = $rs->signature_type_seq;
			$signatory[$rs->signature_type_seq]["approver"] = $rs->employee_id;
			$signatory[$rs->signature_type_seq]["notify"] = $rs->notify_id;
			$signatory[$rs->signature_type_seq]["type"] = $rs->signature_type;
			$signatory[$rs->signature_type_seq]["approver_name"] = $rs->firstname . " " . $rs->lastname;
			$signatory[$rs->signature_type_seq]["approver_email"] = $rs->email;
			$signatory[$rs->signature_type_seq]["notify_name"] = $rs->e_firstname . " " . $rs->e_lastname;
			$signatory[$rs->signature_type_seq]["notify_email"] = $rs->e_email;
			if(1 == $rs->employee_id) $president_stats = $rs->signature_type;	
			if(1 == $rs->employee_id) $president = $rs->signature_type_seq;			
		}

		//DELETE ATTACHMENT
		GeneralDocuments::delete_attachment($id);

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($id);

		$reviewer_stats = array(1=>"Review",2=>"Review",5=>"Recommendation",3=>"Acknowledgement",4=>"Approval");
        $reference = Input::get("reference");
        $filer = Input::get("filer");
        $department = Input::get("department");
        $comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n";	

        //gen docs status
		$stats[1] = 2;
		$stats[2] = 2;
		$stats[3] = 3;
		$stats[4] = 4;
		$stats[5] = 11;

		if(Input::get("status") == "Pending at PRES")
		{
			//submit to president
			$param["status_id"] = $stats[ $president_stats ];
			$param["checker_id"] = 0;
			$param["comments_ea"] =  DB::raw("concat(comments_ea , '$comment')");
			//$param["completed_date"] = date("Y-m-d");
			
			$store = GeneralDocuments::where("id",$id)
				->where("checker_id" , Session::get("employee_id"))
				->update($param);

			$msg = "DOCUMENT SUCCESSFULLY SENT TO THE PRESIDENT.";

	        //SEND EMAIL NOTIFICATION
			Mail::send('gd.email.approve', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get("filer"),
	                        'department' => Input::get("dept_name"),
	                        'status' => $reviewer_stats[$president_stats] ),
	                        function($message ) use ($signatory , $reviewer_stats,$president,$president_stats)
	                        {
	                            $message->to( $signatory[$president]["approver_email"] , $signatory[$president]["approver_name"])
	                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$president_stats]);
	                        }
	                    );

	        //check if president has notifier.
            if($signatory[$president]["notify_email"])
            {
            	//SEND EMAIL NOTIFICATION
            	Mail::send('gd.email.approve', array(
	                        'reference' => Input::get("reference"),
	                        'filer' => Input::get("filer"),
	                        'department' => Input::get("dept_name"),
	                        'status' => $reviewer_stats[$president_stats] ),
	                        function($message ) use ($signatory , $reviewer_stats,$president,$president_stats)
	                        {
	                            $message->to( $signatory[$president]["notify_email"] , $signatory[$president]["notify_name"])
	                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$president_stats]);
	                        }
	                    );
            }      

		}
		//CHECK IF THERE IS NEXT APPROVER AVAILABLE
		else if(isset($signatory[$current + 1]))
		{
			$subject = "RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]];
	        $message = "";

			//check first if next president is approver
			if($signatory[$current + 1]["approver"] == 1 && !Input::get("confidential"))
			{				
				$ea_id = DB::table("employees")
					->join('document_dept_lists', function ($join) 
		            {
		                $join->on('dept_hrms_code', '=', 'departmentid2')
		                     ->on('department_id', '=', "departmentid");
		            })->where("employees.id" , Input::get("employeeid"))
					->get(["document_dept_lists.employee_id","firstname","lastname","email"]);

				if(isset($ea_id[0]->employee_id))
				{
					$eapersonnel = Employees::where("id",$ea_id[0]->employee_id)
					->get(["firstname","lastname","id","email"]);

					//pending at pres
					$param["status_id"] = 8;
					$param["checker_id"] = $ea_id[0]->employee_id;//ea personnel

					//SEND EMAIL NOTIFICATION TO EA
	            	Mail::send('gd.email.approve', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>  $reviewer_stats[$signatory[$current + 1]["type"]]  ),
		                        function($message ) use ($eapersonnel , $reviewer_stats,$signatory,$current)
		                        {
		                            $message->to($eapersonnel[0]->email , $eapersonnel[0]->firstname . " " . $eapersonnel[0]->lastname  )
		                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]] );
		                        }
		                    );
				}
				else
				{
					$param["status_id"] = $stats[$signatory[$current + 1]["type"]] == 5 ? 11 : $stats[$signatory[$current + 1]["type"]];

					//SEND EMAIL NOTIFICATION
	                Mail::send('gd.email.approve', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>  $reviewer_stats[$signatory[$current + 1]["type"]]  ),
		                        function($message ) use ($signatory , $reviewer_stats,$current)
		                        {
		                            $message->to(  $signatory[$current + 1]["approver_email"]  , $signatory[$current + 1]["approver_name"]  )
		                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]] );
		                        }
		                    );

	                //check if next approver has notifier.
	                if($signatory[$current + 1]["notify_email"])
	                {
	                	//SEND EMAIL NOTIFICATION
	                	Mail::send('gd.email.approve', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>  $reviewer_stats[$signatory[$current + 1]["type"]]  ),
		                        function($message ) use ($signatory , $reviewer_stats,$current)
		                        {
		                            $message->to(  $signatory[$current + 1]["notify_email"]  , $signatory[$current + 1]["notify_name"]  )
		                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]] );
		                        }
		                    );               
	                }
				}				
			}
			else
			{
				//SEND EMAIL NOTIFICATION
	                	Mail::send('gd.email.approve', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>  $reviewer_stats[$signatory[$current + 1]["type"]]  ),
		                        function($message ) use ($signatory , $reviewer_stats,$current)
		                        {
		                            $message->to(  $signatory[$current + 1]["approver_email"]  , $signatory[$current + 1]["approver_name"]  )
		                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]] );
		                        }
		                    );    

                //check if next approver has notifier.
                if($signatory[$current + 1]["notify_email"])
                {
                	//SEND EMAIL NOTIFICATION
	                	Mail::send('gd.email.approve', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>  $reviewer_stats[$signatory[$current + 1]["type"]]  ),
		                        function($message ) use ($signatory , $reviewer_stats,$current)
		                        {
		                            $message->to(  $signatory[$current + 1]["notify_email"]  , $signatory[$current + 1]["notify_name"]  )
		                            	->subject("RGAS Notification Alert: Document for " . $reviewer_stats[$signatory[$current + 1]["type"]] );
		                        }
		                    );    
                }

				$param["status_id"] = $stats[$signatory[$current + 1]["type"]] == 5 ? 11 : $stats[$signatory[$current + 1]["type"]];
			}			
			
			$param["current_id"] = $signatory[$current + 1]["approver"];
			$param["comments"] =  DB::raw("concat(comments , '$comment')");
			$param["confidential"] =  Input::get("confidential");
			//$param["completed_date"] = date("Y-m-d");
			$store = GeneralDocuments::where("id",$id)
				->where("current_id" , Session::get("employee_id"))
				->update($param);

			$arr["approval_date"] = date("Y-m-d");
			$arr["signed"] = 1;

			DB::table("document_signatories")->where("document_id",$id)
				->where("employee_id" , Session::get("employee_id"))
				->update($arr);

			$msg = "DOCUMENT SUCCESSFULLY SENT.";

			//INSERT AUDIT
			$old = ["current_id"=>Session::get("employee_id")];
			$new = ["current_id"=> $signatory[$current + 1]["approver"]];
			$this->setTable('documents');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );
		}
		else
		{
			$param["status_id"] = 5;
			$param["current_id"] = 0;//filer
			$param["completed_date"] = date("Y-m-d");
			$param["comments"] =  DB::raw("concat(comments , '$comment')");

			$store = GeneralDocuments::where("id",$id)
				->where("current_id" , Session::get("employee_id"))
				->update($param);

			$arr["approval_date"] = date("Y-m-d");
			$arr["signed"] = 1;

			DB::table("document_signatories")->where("document_id",$id)
				->where("employee_id" , Session::get("employee_id"))
				->update($arr);


			//APPEND SIGNATORY
			foreach($reviewer as $rs)
			{
				$approver[$rs->signature_type][] = array(
						"name" => $rs->firstname . " " . $rs->lastname,
						"department" => $rs->dept_code,
						 "signed" => $rs->approval_date ? $rs->approval_date  : date("Y-m-d")
					);
			}

			$loop = [];
			foreach($approver as $index=>$arr)
			{
				if(count($arr) > count($loop))
				{
					$loop = $arr;
				}
			}


			$html = "<html>
				<head>
				<style>
				body,table
				{
					font-family:Arial;
					font-size:12px !important;
				}
				</style>
				</head>
	            <body>
	            	<table width='100%'>
	            	<tr>
	            		<th align='left' >PREPARED:</th>
	            		<th align='left' >" . (isset($approver[2]) ?  "REVIEWED:" : "") . "</th>
	            		<th align='left' >" . (isset($approver[5]) ?  "RECOMMENDED:" : "") . "</th>
	            		<th align='left' >" . (isset($approver[3]) ?  "ACKNOWLEDGED:" : "") . "</th>
	            		<th align='left' >" . (isset($approver[4]) ?  "APPROVED:" : "") . "</th>
	            	</tr>";

	            foreach($loop as $index =>$e):

	            $html .="<tr>
	            		<td>&nbsp;</td>
	            		<td>&nbsp;</td>
	            		<td>&nbsp;</td>
	            		<td>&nbsp;</td>
	            		<td>&nbsp;</td>
	            	</tr>
	            	<tr>
	            		<th align='left' >" . (isset($approver[1][$index]) ?  $approver[1][$index]["name"] : "") . "</th>
	            		<th align='left' >" . (isset($approver[2][$index]) ?  $approver[2][$index]["name"] : "") . "</th>
	            		<th align='left' >" . (isset($approver[5][$index]) ?  $approver[5][$index]["name"] : "") . "</th>
	            		<th align='left' >" . (isset($approver[3][$index]) ?  $approver[3][$index]["name"] : "") . "</td>
	            		<th align='left' >" . (isset($approver[4][$index]) ?  $approver[4][$index]["name"] : "") . "</th>
	            	</tr>
	            	<tr>
	            		<td>" . (isset($approver[1][$index]) ?  "(" . $approver[1][$index]["department"] . ")" : "") . "</td>
	            		<td>" . (isset($approver[2][$index]) ?  "(" . $approver[2][$index]["department"] . ")" : "") . "</td>
	            		<td>" . (isset($approver[5][$index]) ?  "(" . $approver[5][$index]["department"] . ")" : "") . "</td>
	            		<td>" . (isset($approver[3][$index]) ?  "(" . $approver[3][$index]["department"] . ")" : "") . "</td>
	            		<td>" . (isset($approver[4][$index]) ?  "(" . $approver[4][$index]["department"] . ")" : "") . "</td>
	            	</tr>
	            	<tr>
	            		<td>" . (isset($approver[1][$index]) ? "SIGNED: " .  date("Y-m-d",strtotime($approver[1][$index]["signed"])) : "") . "</td>
	            		<td>" . (isset($approver[2][$index]) ? "SIGNED: " .  date("Y-m-d",strtotime($approver[2][$index]["signed"])) : "") . "</td>
	            		<td>" . (isset($approver[5][$index]) ? "SIGNED: " .  date("Y-m-d",strtotime($approver[5][$index]["signed"])) : "") . "</td>
	            		<td>" . (isset($approver[3][$index]) ? "SIGNED: " .  date("Y-m-d",strtotime($approver[3][$index]["signed"])) : "") . "</td>
	            		<td>" . (isset($approver[4][$index]) ? "SIGNED: " .  date("Y-m-d",strtotime($approver[4][$index]["signed"])) : "") . "</td>
	            	</tr>";
	            endforeach;

	            $html .= "</table>
	            <body>
	            </html>";

	        ob_start();  
	        echo $html;
	        $output = ob_get_clean();

	        $path = Config::get('rgas.rgas_storage_path') . "gd/" .  $id . "/";
	        $c = $path . 'remarks.html';
	        $o = $path . 'remarks.pdf';

	        file_put_contents($c ,$output);

	        $fle = "wkhtmltopdf --disable-external-links --disable-internal-links --orientation portrait --margin-top 12.7mm --margin-right 6.35mm --margin-bottom 12.7mm --margin-left 6.35mm --page-width 215.9mm --page-height 279.4mm \"$c\" \"$o\"";
				

	        shell_exec($fle);

			//UPDATE ATTACHED FILE NAME
			$attached_file = GeneralDocuments::attachments($id);
			foreach ($attached_file as $key => $rs) 
			{
				if($rs->type == 1)
				{

					$file_name =  strtoupper(pathinfo($rs->fn,PATHINFO_FILENAME )) . "-SIGNED." . pathinfo($rs->fn,PATHINFO_EXTENSION  );
					$parameters = array("fn"=>$file_name);
					DB::table("document_attachments")
		                ->where("document_id" , $id)
		                ->where("id",$rs->id)
		                ->update($parameters); 


		            $attach = json_decode($rs->code);            
		            $source = $path .  $attach->random_filename;
		            rename($source , $source . ".pdf");

		            shell_exec("pdftk $source.pdf " . $path . "remarks.pdf cat output $source" . "_out.pdf");
		            
		            rename($source . "_out.pdf" , $source . ".pdf");
		            rename($source  . ".pdf" , $source );
		        }
			}

			$msg = "DOCUMENT SUCCESSFULLY APPROVED.";
		}
		

		if ($store) 
			return Redirect::to("gd/report")
		           ->with('successMessage', $msg);
		else
			return Redirect::to("gd/review/form/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function review_return($id)
	{	
		//avoid user fraud
		if(Input::get("holder") != Session::get("employee_id"))
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');

		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n";
		$reviewer = DB::table("document_signatories")
			->where("document_id" , $id)
			->where("employee_id" , Input::get("assign"))
			->where("signed",1)
			->get(["signature_type","signature_type_seq","employee_id"]);

			if(isset($reviewer[0]->signature_type_seq)):
			DB::table("document_signatories")
				->where("signature_type_seq","=", $reviewer[0]->signature_type_seq + 1)
				->where("document_id" , $id)
				->update(["signed"=> 1,"approval_date"=> null]);
			endif;

			//DELETE ATTACHMENT
			GeneralDocuments::delete_attachment($id);

			//INSERT ATTACHMENT
			GeneralDocuments::insert_attachment($id);

			$assign = Employees::where("id",Input::get("assign"))
				->get(["firstname","lastname","email"])->toArray();
						
			$param["current_id"] = isset($reviewer[0]->employee_id) ? $reviewer[0]->employee_id : Input::get("assign");
			//$param["comments"] =  DB::raw("concat(comments , '$comment')");
			if(Input::get("status") == "Pending at PRES")
			{
				$param["status_id"] = 9;
				$param["checker_id"] = 0;
				$param["confidential"] =  Input::get("confidential");
				$param["comments"] =	DB::raw("concat(comments , ?)");
			
				$store = GeneralDocuments::addBinding($comment)->where("id",$id)
					->where("checker_id" , Session::get("employee_id"))
					->update($param);
				$status = "for Clarification or Revision";
				$msg = 'DOCUMENT SUCCESSFULLY RETURNED TO ' . $assign[0]["firstname"] . ' FOR CLARIFICATION OR REVISION.';
			}
			else
			{
				$param["status_id"] = 6;
				$param["confidential"] =  Input::get("confidential");
				$param["comments"] =	DB::raw("concat(comments , ?)");
			
				$store = GeneralDocuments::addBinding($comment)->where("id",$id)
					->where("current_id" , Session::get("employee_id"))
					->update($param);
				$status = "Disapproved";
				$msg = 'DOCUMENT SUCCESSFULLY RETURNED TO ' . $assign[0]["firstname"] . ' FOR CHECKING.';
			}		

	        //SEND EMAIL NOTIFICATION
          	Mail::send('gd.email.return', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name"),
		                        'status' =>   $status ),
		                        function($message ) use ($assign , $status)
		                        {
		                            $message->to(   $assign[0]["email"] , $assign[0]["firstname"] . " " . $assign[0]["lastname"]   )
		                            	->subject("RGAS Notification Alert: Document " . $status );
		                        }
		                    );    
	
          	//INSERT AUDIT
			$old = ["current_id"=>Session::get("employee_id")];
			$new = ["current_id"=> Input::get("assign")];
			$this->setTable('documents');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("gd/report")
		           ->with('successMessage', $msg);
		else
			return Redirect::to("gd/review/form/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

	
	public function review_return_to_ea($id)
	{	
		//avoid user fraud
		if(Input::get("holder") != Session::get("employee_id"))
			return Redirect::to("")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');

		$param["checker_id"] = Input::get("assign_to_ea");
		$param["status_id"] = 8;
		$comment = ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . Input::get('comment') . "\n";
		$param["comments_ea"] =  DB::raw("concat(comments_ea , '$comment')");

		//DELETE ATTACHMENT
		GeneralDocuments::delete_attachment($id);

		//INSERT ATTACHMENT
		GeneralDocuments::insert_attachment($id);

		$store = GeneralDocuments::where("id",$id)
			->where("current_id" , Session::get("employee_id"))
			->update($param);

		DB::table("document_signatories")
				->where("employee_id",1)
				->where("document_id" , $id)
				->update(["signed"=> 1,"approval_date"=> null]);

		$ea = Employees::where("id",Input::get("assign_to_ea"))
				->get(["firstname","lastname","email"])->toArray();
       
        //SEND EMAIL NOTIFICATION
          	Mail::send('gd.email.return_ea', array(
		                        'reference' => Input::get("reference"),
		                        'filer' => Input::get("filer"),
		                        'department' => Input::get("dept_name") ),
		                        function($message ) use ($ea )
		                        {
		                            $message->to(   $ea[0]["email"] , $ea[0]["firstname"] . " " . $ea[0]["lastname"]   )
		                            	->subject("RGAS Notification Alert: Document PENDING AT PRES" );
		                        }
		                    );   

          //INSERT AUDIT
			$old = ["checker_id"=>Session::get("employee_id")];
			$new = ["checker_id"=> Input::get("assign_to_ea")];
			$this->setTable('documents');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

		if ($store) 
			return Redirect::to("gd/report")
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY SENT TO ' .  $ea[0]["firstname"]  . ' FOR CHECKING.');
		else
			return Redirect::to("gd/review/form/$id")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
	}

		

	public function get_record()
	{	
			$result_data = array();
			$requests = GeneralDocuments::where("created_by" , Session::get("employee_id"))
				->join("employees" , "employees.id","=","current_id","left")
				->where("hidden" , 0)
				->get(["ts_created","firstname","lastname","reference_no","document_name","trans_date" , "completed_date","documents.id","status_id","current_id"]);

	    	if ( count($requests) > 0)
	        {
	        	$status[1] = "New";
	        	$status[2] = "For Review";
	        	$status[3] = "For Acknowledgement";
	        	$status[4] = "For Approval";
	        	$status[5] = "Approved";
	        	$status[6] = "Disapproved";
	        	$status[7] = "Published";
	        	$status[8] = "Pending at PRES";
	        	$status[9] = "For Clarification or Revision";
	        	$status[10] = "Cancelled";
	        	$status[11] = "For Recommendation";

	                $ctr = 0;
	                foreach($requests as $req) 
	                 {				
	                 	$disable_edit = in_array($req->status_id, [1,6,10,9]) && in_array($req->current_id, [0 , Session::get("employee_id")]) ? "" : "disabled";
	                    $disable_delete = in_array($req->status_id, [1,6]) ? "" : "disabled";
	                    
	                    $result_data[$ctr][] = $req->reference_no;
	    				$result_data[$ctr][] = $req->trans_date ;
	    				$result_data[$ctr][] = $req->document_name;
	    				$result_data[$ctr][] = $status[$req->status_id];
	    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname;
	    				$result_data[$ctr][] = $req->completed_date ? date("Y-m-d" , strtotime($req->completed_date)) : "";
	    				$result_data[$ctr][] = "
	    					<button $disable_delete  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/delete/$req->id") . "'\" >DELETE</button>
	    					<a ' class='btn btn-default btndefault' href='" . url("gd/view/$req->id") . "' >VIEW</a>
	    					<button  type='button' $disable_edit class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/edit/$req->id") . "'\" >EDIT</button>
	    					";
	    				 $result_data[$ctr][] = $req->ts_created;
	                    $ctr++;
	                 }
	        }

			return $result_data;
	}

	

	public function get_reviewer_record()
	{	
			$approver = array();
			$document = GeneralDocuments::join("document_signatories","documents.id" , "=","document_id")
				->whereNull("approval_date")
				->where("hidden" , 0)
				->whereNotin("status_id" , [1] )
				->groupby("document_id")
				->get(["document_id","signature_type_seq"]);

			foreach($document as $req) 
            {			
            	$approver[$req->document_id] = $req->signature_type_seq;
            }

			$result_data = array();	
			$requests = GeneralDocuments::join("document_signatories","documents.id" , "=","document_id")
				->join("employees","employees.id" , "=","created_by")
				->where("hidden" , 0)
				->whereNotin("status_id" , [1] )
				->whereRaw("(employee_id = ? or notify_id = ? or checker_id = ? or signed=1)" , [Session::get("employee_id"),Session::get("employee_id"),Session::get("employee_id")] )
				->groupby("document_id")
				->groupby("employee_id")//JM added employee id
				->selectRaw("ts_created,max(signature_type_seq) as signature_type_seq, 
					created_by,employee_id, min(signed) as signed ,checker_id,current_id,firstname,lastname,
					document_id,signature_type,reference_no,document_name,trans_date , completed_date,status_id,notify_id")	
				->get();

//dito na ako jm to check if tma ba
//$req->employee_id,$req->notify_id,$req->checker_id

	    	if ( count($requests) > 0)
	        {	        	
	        	$status[1] = "New";
	        	$status[2] = "For Review";
	        	$status[3] = "For Acknowledgement";
	        	$status[4] = "For Approval";
	        	$status[5] = "Approved";
	        	$status[6] = "Disapproved";
	        	$status[7] = "Published";
	        	$status[8] = "Pending at PRES";
	        	$status[9] = "For Clarification or Revision";
	        	$status[10] = "Cancelled";
	        	$status[11] = "For Recommendation";

                $ctr = 0;
                $docs = array();
                foreach($requests as $req) 
                {				
//                	echo $req->reference_no . " " . $req->signed . " eid " . $req->employee_id . "<br>";

                 	if( ($req->signature_type_seq === (isset($approver[$req->document_id]) ? $approver[$req->document_id] : "") && $req->signed == 0) || ($req->signed == 1 && in_array(Session::get("employee_id"),[$req->employee_id,$req->notify_id,$req->checker_id]) ))
	        		{
	        			if($req->status_id == 8 && Session::get("employee_id") == 1){}
	        			else if(Session::get("employee_id") == $req->created_by ) {}
						else 
						{     
							if(!in_array($req->document_id , $docs))
							{
								$docs[] = $req->document_id;   			
			                    $result_data[$ctr][] = $req->reference_no;
			    				$result_data[$ctr][] = $req->trans_date ;
			    				$result_data[$ctr][] = $req->document_name;
			    				$result_data[$ctr][] = $status[$req->status_id];
			    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname;
			    				$result_data[$ctr][] = $req->completed_date ? date("Y-m-d" , strtotime($req->completed_date)) : "";
			    				$result_data[$ctr][] = "
			    					<a ' class='btn btn-default btndefault' href='" . url("gd/review/form/$req->document_id?page=view") . "' >VIEW</a>" . 
			    					(in_array(Session::get("employee_id") , [$req->checker_id,$req->current_id] ) ? "<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/review/form/$req->document_id") . "'\" >APPROVE</button>" : "");
			                    $result_data[$ctr][] = $req->ts_created ;
			                    $ctr++;

							}							
		                }
                 	}

             	}
	        }

			return $result_data;
	}


	public function get_reviewer_dashbard()
	{	
		$result_data["aaData"] = array();
	
			$requests = GeneralDocuments::where("created_by" , Session::get("employee_id"))
				->join("employees" , "employees.id","=","current_id","left")
				->whereNotin("status_id",[1,5,7,10])
				->limit(5)
				->orderBy("ts_created","desc")
				->where("hidden" , 0)
				->get(["documents.id as document_id","current_id","firstname","lastname","reference_no","document_name","trans_date" , "completed_date","status_id","ts_created"]);
				
		
			$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
	    	$result_data["iTotalRecords"] = count($requests);
	    
	    	if ( count($requests) > 0)
	        {

	        		$status[1] = "New";
		        	$status[2] = "For Review";
		        	$status[3] = "For Acknowledgement";
		        	$status[4] = "For Approval";
		        	$status[5] = "Approved";
		        	$status[6] = "Disapproved";
		        	$status[7] = "Published";
		        	$status[8] = "Pending at PRES";
		        	$status[9] = "For Clarification or Revision";
		        	$status[10] = "Cancelled";
		        	$status[11] = "For Recommendation";

	                $ctr = 0;
	                foreach($requests as $req) 
	                {			
	                	if($ctr != 5)
	                	{

		        			if($req->status_id == 8 && Session::get("employee_id") == 1){}
							else 
							{        			
			                    $result_data["aaData"][$ctr][] = $req->reference_no;
			    				$result_data["aaData"][$ctr][] = $req->ts_created;
			    				$result_data["aaData"][$ctr][] = date("Y-m-d",strtotime($req->trans_date)) ;
			    				$result_data["aaData"][$ctr][] = $req->document_name;
			    				$result_data["aaData"][$ctr][] = $status[$req->status_id];
			    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
			    				$result_data["aaData"][$ctr][]= $req->completed_date ? date("Y-m-d" , strtotime($req->completed_date)) : "";
			    				if($req->status_id == 6)
			    				{
			    					$result_data["aaData"][$ctr][] =  "
			    					<a ' class='btn btn-default btndefault' href='" . url("gd/view/$req->document_id") . "' >VIEW</a>
			    					<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/edit/$req->document_id") . "'\" >EDIT</button>
			    					";
			    				}
			    				else
			    				{
			    					$result_data["aaData"][$ctr][] =  "
			    					<a ' class='btn btn-default btndefault' href='" . url("gd/view/$req->document_id") . "' >VIEW</a>
			    					";
			    				}
			    				
					        
					            $ctr++;
			                }
			            }
	             	}	            
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		
		return $result_data;
	}


	public function get_reviewer()
	{	
		$result_data["aaData"] = array();
	
			$requests = GeneralDocuments::join("employees","employees.id" , "=","created_by")
				->where("hidden" , 0)
				->whereNotIn("status_id" ,[5,1,10])
				//->limit(5)
				->orderBy("ts_created","desc")
				->whereRaw("(current_id = ? or checker_id = ?)" , [Session::get("employee_id"),Session::get("employee_id")] )		
				->get(["created_by","documents.id as document_id","checker_id","current_id","firstname","lastname","reference_no","document_name","trans_date" , "completed_date","status_id","ts_created"]);
				
		
			
	    	$ctr = 0;
	    	if ( count($requests) > 0)
	        {

	        		$status[1] = "New";
		        	$status[2] = "For Review";
		        	$status[3] = "For Acknowledgement";
		        	$status[4] = "For Approval";
		        	$status[5] = "Approved";
		        	$status[6] = "Disapproved";
		        	$status[7] = "Published";
		        	$status[8] = "Pending at PRES";
		        	$status[9] = "For Clarification or Revision";
		        	$status[10] = "Cancelled";
		        	$status[11] = "For Recommendation";

	                
	                foreach($requests as $req) 
	                {			
	                	if($ctr != 5)
	                	{
						if($req->status_id == 6 && Session::get("employee_id") == $req->created_by){}
		        			else if($req->status_id == 8 && Session::get("employee_id") == 1){}
							else 
							{        			
			                    $result_data["aaData"][$ctr][] = $req->reference_no;
			    				$result_data["aaData"][$ctr][] = $req->ts_created ;
			    				$result_data["aaData"][$ctr][] = date("Y-m-d",strtotime($req->trans_date)) ;
			    				$result_data["aaData"][$ctr][] = $req->document_name;
			    				$result_data["aaData"][$ctr][] = $status[$req->status_id];
			    				$result_data["aaData"][$ctr][] = $req->firstname . " " . $req->lastname;
			    				$result_data["aaData"][$ctr][]= $req->completed_date ? date("Y-m-d" , strtotime($req->completed_date)) : "";
			    				$result_data["aaData"][$ctr][] = 
			    				"<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/review/form/$req->document_id?page=view") . "'\" >VIEW</button>" . 
			    				 (in_array(Session::get("employee_id") , [$req->checker_id,$req->current_id] ) ? "<button  type='button' class='btn btn-default btndefault' onClick=\"window.location='" . url("gd/review/form/$req->document_id") . "'\" >APPROVE</button>" : "");
			                    $ctr++;
			                }
			            }
	             	}	            
	        }
	        else
	        {
	            $result_data["aaData"] = $requests;
	        }
			
		$result_data["iTotalDisplayRecords"] = $ctr; //total count filtered query
	    $result_data["iTotalRecords"] = $ctr ;

		return $result_data;
	}


	
	public function deleterecord($id)
	{	
		$store =GeneralDocuments::where("id",$id)
			->whereIn("status_id",[1,6])
			->where("created_by" , Session::get("employee_id"))
			->update(["hidden"=>1]);

		if($store)
		{		
			$rec =GeneralDocuments::where("id",$id)
			->where("created_by" , Session::get("employee_id"))
			->get(["reference_no"])->first();

			//INSERT AUDIT
			$old = ["hidden"=> 0];
			$new = ["hidden"=> 1];
			$this->setTable('documents');
			$this->setPrimaryKey('id');
			$this->AU004($id , json_encode($old),  json_encode($new) , $rec["reference_no"] );
			
			return Redirect::to("gd/report")
		           ->with('successMessage', 'DOCUMENT SUCCESSFULLY DELETED.');
		}
		else
			return Redirect::to("gd/report")
		           ->with('errorMessage', 'Something went wrong upon submitting general documents.');
		
	}
}
?>