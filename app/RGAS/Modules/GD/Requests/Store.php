<?php namespace RGAS\Modules\GD\Requests;

use Input;
use Validator;
use Redirect;

class Store extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
            'reference' => 'required|unique:documents,reference_no',
             'doc_type' => 'required',
             'doc_name' => 'required',
             'txtPrepared' => 'required',
             'contact' => 'numeric'
        ],["reference.unique"=>"Reference number already exists."]);
		
		if(isset($_POST['txtPrepared'])) $validator->each('txtPrepared', ['required']);

		if ($validator->fails())
		{
			return Redirect::to('gd/create')
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function validate_edit($id)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
            'reference' => "required|unique:documents,reference_no,$id",
             'doc_type' => 'required',
             'doc_name' => 'required',
             'txtPrepared' => 'required',
             'contact' => 'numeric'
        ],["reference.unique"=>"Reference number already exists."]);
		
		if(isset($_POST['txtPrepared'])) $validator->each('txtPrepared', ['required']);

		if ($validator->fails())
		{
			return Redirect::to("gd/edit/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}

	public function review_validate($id)
	{
		$input = Input::all();
		
		
		$val_data = array();
		if(Input::get('action') == "return")
		{
			$val_data = array_merge($val_data, ["assign"=>"required","comment"=>"required"]);
		}

		if(Input::get('action') == "return_to_ea")
		{
			$val_data = array_merge($val_data, ["assign_to_ea"=>"required"]);
		}

		$messages['assign_to_ea.required'] = 'EA Field is required.';
		$validator = Validator::make($input , $val_data, $messages);

		if ($validator->fails())
		{
			return Redirect::to("gd/review/form/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}


	public function cancel_validate($id)
	{
		$input = Input::all();
		
		$validator = Validator::make($input , [
            'comment' => 'required'
        ]);
		

		if ($validator->fails())
		{
			return Redirect::to("gd/view/$id")
		           ->withInput()
		           ->withErrors($validator);
		}

		return true;
	}	
}
?>