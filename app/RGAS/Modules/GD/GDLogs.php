<?php namespace RGAS\Modules\GD;

use AuditTrail;
use Session;

class GDLogs 
{
	private $table;
	private $primaryKey;

	public function setTable($table)
	{
		$this->table = $table;
	}
	
	public function setPrimaryKey($primaryKey)
	{
		$this->primaryKey = $primaryKey;
	}
	
	//Create
	protected function AU001($id, $new, $ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU001",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			"",
			$new,
			$ref_num
		);
	}
	
	//Upload File
	public function AU002($id, $new, $ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU002",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			"",
			$new,
			$ref_num
		);
	}
	
	//Retrieve
	protected function AU003($id, $ref_num)
	{

		AuditTrail::store(
			Session::get('employee_id'),
			"AU003",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			"",
			"",
			$ref_num
		);

		
	}
	
	//Edit and Update(workflow of request)
	protected function AU004($id, $old, $new, $ref_num)
	{
		AuditTrail::store(		
			Session::get('employee_id'),
			"AU004",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			$old,
			$new,
			$ref_num
		);
	}
	
	//Delete
	public function AU005($id, $old, $ref_num)
	{		
		AuditTrail::store(
			Session::get('employee_id'),
			"AU005",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			$old,
			"",
			$ref_num
		);
	}
	
	//Print
	protected function AU006($id, $ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU006",
			$this->getModuleId(),
			"",
			json_encode(array("{$this->primaryKey}"=>$id)),
			"",
			"",
			$ref_num
		);
	}

	//Download
	protected function AU008($id, $new, $ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU008",
			$this->getModuleId(),
			"",
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$id)),
			"",
			$new,
			$ref_num
		);
	}
	

	
	

}
?>