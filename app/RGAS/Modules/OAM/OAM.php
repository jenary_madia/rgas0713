<?php namespace RGAS\Modules\OAM;

use OnlineAttendanceMonitoring;
use TMSTimeOutLog;
use TMSDtr;
use TMSHoliday;
use TMSExcusedNotify;
use Session;
use RGAS\Libraries;
use Employees;
use Departments;
use Cache;
use DB;
use Input;
use Config;

class OAM extends OAMLogs implements IOAM
{
	public $employees;
	public $department; 
	public $section; 

	public $cache_byEmployee;
	public $cache_byDepartment;
	public $cache_bySection; 
	public $cache_Department = array();
	public $cache_Section = array(); 


    public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_oam');
		return $this->module_id;
	}

	public function checkDivision()
	{
		$div = DB::table('divisions')
				->where('employeeid' , Session::get("employee_id") )
				->get(["div_code"]);
		
		$div_code = isset($div[0]->div_code) ? $div[0]->div_code : "";

		 //DETECT IF CURRENT USER IS IN DIVISION TABLE ELSE GET ALL RECORDS BY SUPERIOD ID
		 if( $div_code )
		 {
		 	$requests = Employees::where("company","RBC-CORP")
				->where("div_code", $div_code)
				->where("new_employeeid" , "!=" , "")
				->get(["new_employeeid","firstname","lastname","sectionid","departmentid2","id"]);

		 	$dept = Departments::get_departments();
			foreach($dept as $rs)
			{
				$this->department[$rs->dept_code] = $rs->dept_name;
			}

			$q = DB::table("sections")->get(["id","sect_name"]);
			$this->section[""] = "";
			foreach($q as $rs)
			{
				$this->section[$rs->id] = $rs->sect_name;
			}

			foreach($requests as  $req)
			{
				$cache_employee[$req->new_employeeid] = $req->firstname . " " . $req->lastname;
           		$cache_employee_lastname[$req->new_employeeid] =  $req->lastname;

				 $this->cache_byEmployee[] = $req->new_employeeid;
				 $this->cache_byDepartment[ $req->departmentid2 ][] = $req->new_employeeid;
				 $this->cache_bySection[ $req->sectionid ][] = $req->new_employeeid;

				 $this->cache_Department[ $req->departmentid2 ] = isset($this->department[ $req->departmentid2 ]) ? $this->department[ $req->departmentid2 ] : $req->departmentid2;
				 if( $req->sectionid ) $this->cache_Section[ $req->departmentid2 ][ $req->sectionid ] = $this->section[ $req->sectionid ] ;
			}

			//PASS ALL RECORDS TO CACHE VARIABLE
			Cache::put("cache_Employee",  $cache_employee , 1440);
	        Cache::put("cache_employee_lastname",  $cache_employee_lastname , 1440);
	        Cache::put("cache_byEmployee", $this->cache_byEmployee , 1440);
	        Cache::put("cache_byDepartment", $this->cache_byDepartment, 1440);
	        Cache::put("cache_bySection", $this->cache_bySection, 1440);
		 }
		 else
		 {
		 	$this->getSubordinate();
		 }

		 return $div_code;
	}



	public function getSubordinate()
	{
		$requests = Employees::where("company","RBC-CORP")
				->where("new_employeeid" , "!=" , "")
				->get(["new_employeeid","firstname","lastname","sectionid","departmentid2","superiorid","id"]);
		$dept = Departments::get_departments();

		foreach($dept as $rs)
		{
			$this->department[$rs->dept_code] = $rs->dept_name;
		}

		$q = DB::table("sections")->get(["id","sect_name"]);
		$this->section[""] = "";
		foreach($q as $rs)
		{
			$this->section[$rs->id] = $rs->sect_name;
		}

        foreach($requests as $req) 
        {				
           $result_data[$req->superiorid][$req->id] = implode("/", array($req->new_employeeid , $req->departmentid2 , $req->sectionid));
           $cache_employee[$req->new_employeeid] = $req->firstname . " " . $req->lastname;
           $cache_employee_lastname[$req->new_employeeid] =  $req->lastname;
        }

        $this->employees = $result_data;
        $this->recursion( Session::get("employee_id"));

        //PASS ALL RECORDS TO CACHE VARIABLE
        Cache::put("cache_Employee",  $cache_employee , 1440);
        Cache::put("cache_employee_lastname",  $cache_employee_lastname , 1440);
        Cache::put("cache_byEmployee", $this->cache_byEmployee , 1440);
        Cache::put("cache_byDepartment", $this->cache_byDepartment, 1440);
        Cache::put("cache_bySection", $this->cache_bySection, 1440);
	}

	//RETRIEVE ALL SUBORDINATE EMPLOYEE
	public function recursion($eid)
	{
		if(isset($this->employees[$eid]))
		{
			$emp = $this->employees[$eid];
			foreach($emp as $id => $param)
			{
				 $erecords = explode("/", $param);
				 $this->cache_byEmployee[] = $erecords[0];
				 $this->cache_byDepartment[ $erecords[1] ][] = $erecords[0];
				 $this->cache_bySection[ $erecords[2] ][] = $erecords[0];
				 $this->recursion($id);

				 $this->cache_Department[ $erecords[1] ] = isset($this->department[ $erecords[1] ]) ? $this->department[ $erecords[1] ] : $erecords[1] ;
				 if($erecords[2]) $this->cache_Section[ $erecords[1] ][ $erecords[2] ] = $this->section[$erecords[2]];
			}
		}
	}

	//OAM OF CURRENT USER'S SUBORDINATE
	public function attendanceList()
	{
		$month = Input::get("month");
		$year = Input::get("year");
		$start_date = date("$month/01/$year") ;
		$end_date = date("$month/t/$year" ,strtotime(date("$month/01/$year")) );
		if(Input::get("section"))
		{
			$emp_by = Cache::get("cache_bySection");
			$employee =  $emp_by[ Input::get("section") ];
		}
		elseif(Input::get("department"))
		{
			$emp_by = Cache::get("cache_byDepartment");
			$employee =  $emp_by[ Input::get("department") ];
		}
		else
		{
			$employee =  Cache::get("cache_byEmployee");	
		}

		//AUDIT TRAIL
		$parameter["employee"] = "My Subordinates";
		$parameter["month"] = $month;
		$parameter["year "] = $year;
		$this->setTable('tms_timeoutlogs');
		$this->AU003(($parameter) );					

		$table='<thead style="display:block" ><tr><th class="labels2 text-left" rowspan="2" width="160" >EMPLOYEE NAME</th>
                <th class="th_style" rowspan="2" width="140">ATTENDANCE LOGS</th>';
        $header1 = $header2 = "";
		$i = strtotime($start_date);
		while($i <= strtotime( $end_date  ) )
		{
			$header1 .= '<th class="th_style" width="60" >' . (date("d",$i) + 0 ). '</th>';
			$header2 .= '<th class="th_style">' . strtoupper(date("D",$i)) . '</th>';
			$i = $i + 86400;
		} 

		$table .= $header1 . '<th class="th_style"  rowspan="2" width="80">NUMBER OF DAYS WORKED</th>
                   <th class="th_style"  colspan="2">LEAVE</th>
                   <th class="th_style"  colspan="2">LATE</th>
                   <th class="th_style"  rowspan="2" width="90" >FREQUENCY OF LATES</th></tr>';

        $table .= "<tr>" . $header2 . '<th class="th_style" width="80" >EXCUSED</th>
                   <th class="th_style" width="80" >UNEXCUSED</th>
                   <th class="th_style" width="80" >EXCUSED</th>
                   <th class="th_style" width="80" >UNEXCUSED</th></tr></thead><tbody  style="width:2687px;display:block;max-height:450px;overflow-y: scroll;"  >';

        $record = TMSTimeOutLog::getlogs($start_date , $end_date , $employee);
		$record_detail = TMSDtr::getlogs($start_date , $end_date , $employee);

        foreach($record as $eid=>$resource)
        {
        	$name = explode("/", $eid);

        	$employee_name = Cache::get("cache_Employee");
        	$table .= "<tr><td rowspan='5' valign='top' width='160' >" . $employee_name[ $name[1] ]  . "</td><td width='140' >TIME IN</td>";
        	$timeout = "<tr><td>TIME OUT</td>";
        	$workhours = "<tr><td>WORKED HOURS</td>";
        	$lateminutes = "<tr><td>LATE MINUTES</td>";
        	$remarks = "<tr><td>REMARKS</td>";

        	$i = strtotime($start_date);
			while($i <= strtotime( $end_date  ) )
			{
				if(isset($record[$eid]["timein"][date("d",$i)]))
					$table .= "<td align='right' width='60' >" . $record[$eid]["timein"][date("d",$i)] . "</td>";			
				else
					$table .= "<td width='60' ></td>";

				if(isset($record[$eid]["timeout"][date("d",$i)]))
					$timeout .= "<td align='right' >" . $record[$eid]["timeout"][date("d",$i)] . "</td>";
				else
					$timeout .= "<td></td>";
				
				if(isset($record_detail[$name[1]]["workhours"][date("d",$i)]))
					$workhours .= "<td align='right' >" . $record_detail[$name[1]]["workhours"][date("d",$i)] . "</td>";
				else
					$workhours .= "<td></td>";

				if(isset($record_detail[$name[1]]["unexcusedlate"][date("d",$i)]) || isset($record_detail[$name[1]]["excusedlate"][date("d",$i)]) )
					$lateminutes .= "<td align='right' >" . (isset($record_detail[$name[1]]["unexcusedlate"][date("d",$i)]) ? $record_detail[$name[1]]["unexcusedlate"][date("d",$i)] : "")  . (isset($record_detail[$name[1]]["excusedlate"][date("d",$i)]) ? $record_detail[$name[1]]["excusedlate"][date("d",$i)] : "") . "</td>";
				else
					$lateminutes .= "<td></td>";

				if(isset($record_detail[$name[1]]["remarks"][date("d",$i)]))
				{
					$class =  in_array($record_detail[$name[1]]["remarks"][date("d",$i)],["LGH_HR","LGRST_HR","SH_HR","SRST_HR","RD","RDUWRK_HR","UWRK_HR"])  || strpos($record_detail[$name[1]]["remarks"][date("d",$i)] , ' RD ') !== false ? 'label-default' : "";
					$remarks .= "<td align='center' class='$class' >" . (str_replace("UWRK_HR","",str_replace("SH_HR","", $record_detail[$name[1]]["remarks"][date("d",$i)])) ) . "</td>";
				}
				else
					$remarks .= "<td></td>";

				$i = $i + 86400;
			}

			//JMA FIXED THIS 5 LINES CHANGE $eid TO $name[1] . 011817
			$workdays = isset($record_detail[$name[1]]["actualdays"]) ? array_sum($record_detail[$name[1]]["actualdays"]) : "";
			$unexcusedleave = isset($record_detail[$name[1]]["unexcusedleave"]) ? array_sum($record_detail[$name[1]]["unexcusedleave"]) : "";
			$excusedleave = isset($record_detail[$name[1]]["excusedleave"]) ? array_sum($record_detail[$name[1]]["excusedleave"]) : "";
			$unexcusedlate = isset($record_detail[$name[1]]["unexcusedlate"]) ? array_sum($record_detail[$name[1]]["unexcusedlate"]) : "";
			$excusedlate = isset($record_detail[$name[1]]["excusedlate"]) ? array_sum($record_detail[$name[1]]["excusedlate"]) : "";
			$funexcusedlate = isset($record_detail[$name[1]]["unexcusedlate"]) ? count($record_detail[$name[1]]["unexcusedlate"]) : "";
			$fexcusedlate = isset($record_detail[$name[1]]["excusedlate"]) ? count($record_detail[$name[1]]["excusedlate"]) : "";
			

			$table .= "<td width='80' align='center' >" . ($workdays ? $workdays : "") . "</td>
					   <td width='80' align='center'>" . ($excusedleave ? $excusedleave : "") . "</td>
					   <td width='80' align='center'>" . ($unexcusedleave ? $unexcusedleave : "") . "</td>
					   <td width='80' align='center'>" . ($excusedlate ? $excusedlate : "") . "</td>
					   <td width='80' align='center'>" . ($unexcusedlate ? $unexcusedlate : "") . "</td>
					   <td width='90' align='center' >" . ( ($fexcusedlate + $funexcusedlate) ? ($fexcusedlate + $funexcusedlate) : ""). "</td></tr>" . 
						$timeout . "<td></td><td></td><td></td><td></td><td></td><td></td></tr>" . 
						$workhours . "<td></td><td></td><td></td><td></td><td></td><td></td></tr>" . 
						$lateminutes . "<td></td><td></td><td></td><td></td><td></td><td></td></tr>" . 
						$remarks . "<td></td><td></td><td></td><td></td><td></td><td></td></tr>";
        }


        return "</tbody>" . $table;
	}


	public function legend()
	{
		$month = Input::get("month");
		$year = Input::get("year");
		$start_date = date("$month/01/$year") ;
		$end_date = date("$month/t/$year" ,strtotime(date("$month/01/$year")) );
	
        $holiday = TMSHoliday::gethol($start_date , $end_date);
        $notify = TMSExcusedNotify::getnotify($start_date , $end_date);

        if( isset($holiday["suspension"]) )
        {
        	$notify["suspension"] = array_merge(isset($notify["suspension"]) ? $notify["suspension"] : array(),$holiday["suspension"]);
        
        	sort($notify["suspension"]);
        }


        $count = isset($holiday["holiday"]) && count($holiday["holiday"]) > 0 ? count($holiday["holiday"]) : 0;
        $count = isset($holiday["special"]) && count($holiday["special"]) > $count ? count($holiday["special"]) : $count;
        $count = isset($notify["suspension"]) && count($notify["suspension"]) > $count ? count($notify["suspension"]) : $count;
        $count = isset($notify["excused"]) && count($notify["excused"]) > $count ? count($notify["excused"]) : $count;

		$legend = "<tr>
                                <td width='10%'><label class='labels'></label></td>
                                <td width='20%'><label class='labels'> <label class='labels'>REGULAR HOLIDAYS</label></td>
                                <td width='30%'><label class='labels'><label class='labels'>SPECIAL (NON WORKING) DAYS</label></td>
                                <td width='20%'><label class='labels'><label class='labels'>WORK SUSPENSION</label></td>                                
                                <td width='20%'><label class='labels'><label class='labels'>DAYS FOR EXCUSED LATES</label></td>                                 
                        </tr>";

		for ($i=0; $i < $count; $i++) 
		{ 
			$hol = isset($holiday["holiday"][$i]) ? $holiday["holiday"][$i] : "";
			$spe = isset($holiday["special"][$i]) ? $holiday["special"][$i] : "";
			$sus = isset($notify["suspension"][$i]) ? $notify["suspension"][$i] : "";
			$exc = isset($notify["excused"][$i]) ? $notify["excused"][$i] : "";

			$legend .= "<tr>
                                <td width='10%'></td>
                                <td width='20%' valign='top' ><span class='tbl_fonts'>$hol</span></td>
                                <td width='30%' valign='top' ><span class='tbl_fonts'>$spe</span></td>
                                <td width='20%' valign='top' ><span class='tbl_fonts'>$sus</span></td>                                
                                <td width='20%' valign='top' ><span class='tbl_fonts'>$exc</span></td>                                 
                        </tr>";
		}

		return $legend;
		
	}


	//OAM OF CURRENT USER
	public function attendanceself()
	{
		$nid = Session::get("new_employeeid");
		$month = Input::get("month");
		$year = Input::get("year");
		$start_date = date("$month/01/$year") ;
		$end_date = date("$month/t/$year" ,strtotime(date("$month/01/$year")) );

		$record = TMSTimeOutLog::getlog($start_date , $end_date , $nid);
		$record_detail = TMSDtr::getlogs($start_date , $end_date ,$nid);

		//AUDIT TRAIL
		$parameter["employee"] = "My Attendance";
		$parameter["month"] = $month;
		$parameter["year "] = $year;
		$this->setTable('tms_timeoutlogs');
		$this->AU003(($parameter) );

		$table='';
		$i = strtotime($start_date);
		while($i <= strtotime( $end_date  ) )
		{
			$table .= "<tr  ><td align='center' >" . (date("d",$i) + 0) . "</td>
						   <td align='center' >" . strtoupper(date("D",$i)) . "</td>";

				if(isset($record[$nid]["timein"][date("d",$i)]))
				{
					if(isset($record_detail[$nid]["unexcusedlate"][date("d",$i)]))
						$table .= "<td align='center' class='text-danger' >" . $record[$nid]["timein"][date("d",$i)] . "</td>";
					else
						$table .= "<td align='center' >" . $record[$nid]["timein"][date("d",$i)] . "</td>";			
				}
				else
				{
					if(isset($record_detail[$nid]["remarks"][date("d",$i)]) && ( in_array($record_detail[$nid]["remarks"][date("d",$i)] , ["LGH_HR","LGRST_HR","SH_HR","SRST_HR","RD","RDUWRK_HR","UWRK_HR","VL","SL","OB","UT","ML","PL","EL","BL","OS"]) || strpos($record_detail[$nid]["remarks"][date("d",$i)] , ' RD ') !== false)   )
						$table .= "<td class='' ></td>";
					else
					{
						if(strtotime(date("$month/" . (date("d",$i) + 0) . "/$year")) <= strtotime(date("m/d/Y")) && !isset($record_detail[$nid]["workhours"][date("d",$i)]))
							$table .= "<td align='center' class='nologs text-danger' >-XXX-</td>";
						else
							$table .= "<td align='center' class='' ></td>";
					}
				}

					
				// if(isset($record[$nid]["timeout"][date("d",$i)]) && isset($record_detail[$nid]["remarks"][date("d",$i)]) && $record_detail[$nid]["remarks"][date("d",$i)] == "UT")		
				// 	$table .= "<td align='center' class='text-danger' >" . $record[$nid]["timeout"][date("d",$i)] . "</td>";
				// else
				 if(isset($record[$nid]["timeout"][date("d",$i)]))
					$table .= "<td align='center' >" . $record[$nid]["timeout"][date("d",$i)] . "</td>";
				else
				{
					if(isset($record_detail[$nid]["remarks"][date("d",$i)]) && ( in_array($record_detail[$nid]["remarks"][date("d",$i)] , ["LGH_HR","LGRST_HR","SH_HR","SRST_HR","RD","RDUWRK_HR","UWRK_HR","VL","SL","OB","UT","ML","PL","EL","BL","OS"])  || strpos($record_detail[$nid]["remarks"][date("d",$i)] , ' RD ') !== false)  )
					{
						if(isset($record[$nid]["timein"][date("d",$i)]))
						{
							$table .= "<td align='center' class='nologs text-danger' >-XXX-</td>";
						}
						else
						{
							$table .= "<td class='' ></td>";
						}
						
					}	
					else
					{
						if(strtotime(date("$month/" . (date("d",$i) + 0) . "/$year")) <= strtotime(date("m/d/Y")) && !isset($record_detail[$nid]["workhours"][date("d",$i)])   )
							$table .= "<td align='center' class='nologs text-danger' >-XXX-</td>";
						else
							$table .= "<td align='center' class='' ></td>";
					}
				}

				if(isset($record_detail[$nid]["workhours"][date("d",$i)]))
					$table .= "<td align='center' >" . $record_detail[$nid]["workhours"][date("d",$i)] . "</td>";
				else
					$table .= "<td></td>";

				if(isset($record_detail[$nid]["unexcusedlate"][date("d",$i)]) || isset($record_detail[$nid]["excusedlate"][date("d",$i)]) )
					$table .= "<td align='center' >" . (isset($record_detail[$nid]["unexcusedlate"][date("d",$i)]) ? $record_detail[$nid]["unexcusedlate"][date("d",$i)] : "")  . (isset($record_detail[$nid]["excusedlate"][date("d",$i)]) ? $record_detail[$nid]["excusedlate"][date("d",$i)] : "") . "</td>";
				else
					$table .= "<td></td>";
				// if(isset($record_detail[$nid]["unexcusedlate"][date("d",$i)]))
				// 	$table .= "<td align='center' >" . $record_detail[$nid]["unexcusedlate"][date("d",$i)] . "</td>";
				// else
				//	$table .= "<td></td>";

				if(isset($record_detail[$nid]["remarks"][date("d",$i)]))
				{
					$class = $record_detail[$nid]["remarks"][date("d",$i)] == "A" ? "nologs text-danger" : ( ( in_array($record_detail[$nid]["remarks"][date("d",$i)] , ["LGH_HR","LGRST_HR","SH_HR","SRST_HR","RD","RDUWRK_HR","UWRK_HR"])  || strpos($record_detail[$nid]["remarks"][date("d",$i)] , ' RD ') !== false)  ? "restday" : "");
					$table .= "<td align='center' class='$class' >" . (str_replace("UWRK_HR","", str_replace("SH_HR","",$record_detail[$nid]["remarks"][date("d",$i)] ) )) . "</td>";
				}
				else
					$table .= "<td></td>";	

			$table .= "</tr>";
			$i = $i + 86400;
		} 

		$workdays = isset($record_detail[$nid]["actualdays"]) ? array_sum($record_detail[$nid]["actualdays"]) : "";
		$unexcusedleave = isset($record_detail[$nid]["unexcusedleave"]) ? array_sum($record_detail[$nid]["unexcusedleave"]) : "";
		$excusedleave = isset($record_detail[$nid]["excusedleave"]) ? array_sum($record_detail[$nid]["excusedleave"]) : "";
		$unexcusedlate = isset($record_detail[$nid]["unexcusedlate"]) ? array_sum($record_detail[$nid]["unexcusedlate"]) : "";
		$excusedlate = isset($record_detail[$nid]["excusedlate"]) ? array_sum($record_detail[$nid]["excusedlate"]) : "";
		$funexcusedlate = isset($record_detail[$nid]["unexcusedlate"]) ? count($record_detail[$nid]["unexcusedlate"]) : "";
		$fexcusedlate = isset($record_detail[$nid]["excusedlate"]) ? count($record_detail[$nid]["excusedlate"]) : "";


		//JMA 011817 CHANGE FREQUENCY VALUE
		$total = "<tr>
				<td align='center' >$workdays &nbsp;</td>
				<td align='center' >" . ($excusedleave ? $excusedleave : "") . "</td>
				<td align='center' >" . ($unexcusedleave ? $unexcusedleave : "" ) . "</td>
				<td align='center' >$excusedlate</td>
				<td align='center' >$unexcusedlate</td>
				<td align='center' >" . ( ($funexcusedlate  + $fexcusedlate ) ? ($funexcusedlate  + $fexcusedlate ) : ""). "</td>
				</tr>";
		
		return array(
			"table"=>$table,
			"total" => $total
			);
	}

}
?>