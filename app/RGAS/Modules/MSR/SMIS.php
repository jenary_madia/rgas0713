<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 16/11/2016
 * Time: 3:55 PM
 */
namespace RGAS\Modules\MSR;
use RGAS\Libraries;
use Input;
use Session;
use Redirect;
use Validator;
use Config;
use Mail;
use Sections;
use ManpowerService;
use ProjectRequirements;
use ManpowerAgency;
use ManpowerserviceAttachments;
use ManpowerSignatories;
use Receivers;
use DB;
use Employees;


class SMIS
{
    protected $logs;
    public function __construct()
    {
        $this->logs = new Logs;
    }
    public function getStoragePath()
    {
        return Config::get('rgas.rgas_storage_path').'msr/';
    }

    public function send() {
        /**** SETTING UP PARAMETERS *****/
        $currEmp = $this->getCurrEmp(Input::get("source"));
        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'reference_no' => (new ManpowerService)->generateRefNo(),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => $currEmp['status'],
            'panels' => Input::get('panel'),
            'projectname' => Input::get('projType'),
            'panelsize' => Input::get('sizePerPanel'),
            'objective' => Input::get('objective'),
            'otherinfo' => Input::get('otherInformation'),
            'curr_emp' => $currEmp['id'],
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            )),
        ];


        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("source") == "internal") {
            $manpowerParams['internal'] = 1;
        }elseif (Input::get("source") == "external") {
            $manpowerParams['external'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }



        /******** INSERTING DATA ON MANPOWER SERVICES *********/
        $manpowerCreate = ManpowerService::create($manpowerParams);


        /******** INSERTING ATTACHMENTS ON MANPOWER SERVICES *********/
        $files = Input::get('files');

        if($files){

            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $manpowerParams['reference_no']);
                }
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $manpowerCreate['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::insert($attachParams);
        }


        /**** SETTING UP PARAMETERS FOR PROJECT REQUIREMENTS*****/
        $projectRequirements = json_decode(Input::get("projectRequirements"),true);

        $SMISParams = [];
        foreach ($projectRequirements as $key) {
            array_push($SMISParams,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'position_jobtitle' => $key['jobTitle'],
                'rec_dateneeded' => ($key['recruitment']['from'] != "" ? $key['recruitment']['from'] : null),
                'rec_dateneeded_to' => ($key['recruitment']['to'] != "" ? $key['recruitment']['to'] : null),
                'rec_pax' => ($key['recruitment']['pax'] != "" ? $key['recruitment']['pax'] : null),
                'prep_dateneeded' => ($key['preparation']['from'] != "" ? $key['preparation']['from'] : null),
                'prep_dateneeded_to' => ($key['preparation']['to'] != "" ? $key['preparation']['to'] : null),
                'prep_pax' => ($key['preparation']['pax'] != "" ? $key['preparation']['pax'] : null),
                'brif_dateneeded' => ($key['briefing']['from'] != "" ? $key['briefing']['from'] : null),
                'brif_dateneeded_to' => ($key['briefing']['to'] != "" ? $key['briefing']['to'] : null),
                'brif_pax' => ($key['briefing']['pax'] != "" ? $key['briefing']['pax'] : null),
                'fw_dateneeded' => ($key['fieldWork']['from'] != "" ? $key['fieldWork']['from'] : null),
                'fw_dateneeded_to' => ($key['fieldWork']['to'] != "" ? $key['fieldWork']['to'] : null),
                'fw_pax' => ($key['fieldWork']['pax'] != "" ? $key['fieldWork']['pax'] : null),
                'fw_area' => ($key['fieldWork']['area'] != "" ? $key['fieldWork']['area'] : null),
                'edit_dateneeded' => ($key['editing']['from'] != "" ? $key['editing']['from'] : null),
                'edit_dateneeded_to' => ($key['editing']['to'] != "" ? $key['editing']['to'] : null),
                'edit_pax' => ($key['editing']['pax'] != "" ? $key['editing']['pax'] : null),
                'code_dateneeded' => ($key['coding']['from'] != "" ? $key['coding']['from'] : null),
                'code_dateneeded_to' => ($key['coding']['to'] != "" ? $key['coding']['to'] : null),
                'code_pax' => ($key['coding']['pax'] != "" ? $key['coding']['pax'] : null),
                'enc_dateneeded' => ($key['encoding']['from'] != "" ? $key['encoding']['from'] : null),
                'enc_dateneeded_to' => ($key['encoding']['to'] != "" ? $key['encoding']['to'] : null),
                'enc_pax' => ($key['encoding']['pax'] != "" ? $key['encoding']['pax'] : null),
                'dc_dateneeded' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_dateneeded_to' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_pax' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null)
            ]);
        }

        /******** INSERTING DATA ON PROJECT REQUIREMENTS *********/
        if ($SMISParams != []) {
            $SMISReqInsert = ProjectRequirements::insert($SMISParams);
        }
        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        /******** INSERTING DATA ON MANPOWER AGENCY *********/
        if ($agenciesParam != []) {
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerCreate && $SMISReqInsert) {
            $this->logs->AU001($manpowerCreate['id'],'MSR','id',json_encode($manpowerParams),$manpowerParams['reference_no']);

            if($files) {
                $this->logs->AU002($manpowerCreate['id'], 'MSR', 'id', json_encode($manpowerParams),$manpowerParams['reference_no']);
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $manpowerParams['reference_no'],
                Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
                Session::get('dept_name'),
                $this->getEmail($currEmp['id'])
            );
            return Redirect::to('msr/')
                ->with('successMessage', $currEmp['message']);

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function save() {

        /**** SETTING UP PARAMETERS *****/
        $manpowerParams = [
            'employeeid' => Session::get('employee_id'),
            'departmentid' => Session::get('dept_id'),
            'reference_no' => (new ManpowerService)->generateRefNo(),
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => 'NEW',

            'panels' => Input::get('panel'),
            'projectname' => Input::get('projType'),
            'panelsize' => Input::get('sizePerPanel'),
            'objective' => Input::get('objective'),
            'otherinfo' => Input::get('otherInformation'),
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            )),
        ];

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("source") == "internal") {
            $manpowerParams['internal'] = 1;
        }elseif (Input::get("source") == "external") {
            $manpowerParams['external'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        /******** INSERTING DATA ON MANPOWER SERVICES *********/
        $manpowerCreate = ManpowerService::create($manpowerParams);


        /******** INSERTING ATTACHMENTS ON MANPOWER SERVICES *********/
        $files = Input::get('files');

        if($files){

            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $manpowerCreate['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::insert($attachParams);
        }

        /******** INSERTING PROJECT REQUIREMENTS ON MANPOWER SERVICES *********/
        $projectRequirements = json_decode(Input::get("projectRequirements"),true);

        /**** SETTING UP PARAMETERS FOR PROJECT REQUIREMENTS*****/
        $SMISParams = [];
        foreach ($projectRequirements as $key) {
            array_push($SMISParams,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'position_jobtitle' => $key['jobTitle'],
                'rec_dateneeded' => ($key['recruitment']['from'] != "" ? $key['recruitment']['from'] : null),
                'rec_dateneeded_to' => ($key['recruitment']['to'] != "" ? $key['recruitment']['to'] : null),
                'rec_pax' => ($key['recruitment']['pax'] != "" ? $key['recruitment']['pax'] : null),
                'prep_dateneeded' => ($key['preparation']['from'] != "" ? $key['preparation']['from'] : null),
                'prep_dateneeded_to' => ($key['preparation']['to'] != "" ? $key['preparation']['to'] : null),
                'prep_pax' => ($key['preparation']['pax'] != "" ? $key['preparation']['pax'] : null),
                'brif_dateneeded' => ($key['briefing']['from'] != "" ? $key['briefing']['from'] : null),
                'brif_dateneeded_to' => ($key['briefing']['to'] != "" ? $key['briefing']['to'] : null),
                'brif_pax' => ($key['briefing']['pax'] != "" ? $key['briefing']['pax'] : null),
                'fw_dateneeded' => ($key['fieldWork']['from'] != "" ? $key['fieldWork']['from'] : null),
                'fw_dateneeded_to' => ($key['fieldWork']['to'] != "" ? $key['fieldWork']['to'] : null),
                'fw_pax' => ($key['fieldWork']['pax'] != "" ? $key['fieldWork']['pax'] : null),
                'fw_area' => ($key['fieldWork']['area'] != "" ? $key['fieldWork']['area'] : null),
                'edit_dateneeded' => ($key['editing']['from'] != "" ? $key['editing']['from'] : null),
                'edit_dateneeded_to' => ($key['editing']['to'] != "" ? $key['editing']['to'] : null),
                'edit_pax' => ($key['editing']['pax'] != "" ? $key['editing']['pax'] : null),
                'code_dateneeded' => ($key['coding']['from'] != "" ? $key['coding']['from'] : null),
                'code_dateneeded_to' => ($key['coding']['to'] != "" ? $key['coding']['to'] : null),
                'code_pax' => ($key['coding']['pax'] != "" ? $key['coding']['pax'] : null),
                'enc_dateneeded' => ($key['encoding']['from'] != "" ? $key['encoding']['from'] : null),
                'enc_dateneeded_to' => ($key['encoding']['to'] != "" ? $key['encoding']['to'] : null),
                'enc_pax' => ($key['encoding']['pax'] != "" ? $key['encoding']['pax'] : null),
                'dc_dateneeded' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_dateneeded_to' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_pax' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null)
            ]);
        }

        /******** INSERTING DATA ON PROJECT REQUIREMENTS *********/
        if ($SMISParams != []) {
            $SMISReqInsert = ProjectRequirements::insert($SMISParams);
        }
        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $manpowerCreate['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        /******** INSERTING DATA ON MANPOWER AGENCY *********/
        if ($agenciesParam != []) {
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerCreate) {
            
            $this->logs->AU001($manpowerCreate['id'],'MSR','id',json_encode($manpowerParams),$manpowerParams['reference_no']);

            if($files) {
                $this->logs->AU002($manpowerCreate['id'], 'MSR', 'id', json_encode($manpowerParams),$manpowerParams['reference_no']);
            }

            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully saved');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');


    }

    public function resend($id) {
        $msr = ManpowerService::where('id',$id)
            ->where('employeeid',Session::get("employee_id"))->first();
        /**** SETTING UP PARAMETERS *****/
        $currEmp = $this->getCurrEmp(Input::get("source"));
        $manpowerParams = [
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => $currEmp['status'],
            'panels' => Input::get('panel'),
            'projectname' => Input::get('projType'),
            'panelsize' => Input::get('sizePerPanel'),
            'objective' => Input::get('objective'),
            'otherinfo' => Input::get('otherInformation'),
            'curr_emp' => $currEmp['id'],
        ];

        if ($msr['status'] == 'NEW') {
            $manpowerParams['comment'] = json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment')
            ));
        }else{
            $comment = json_encode([
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=> Input::get('comment')
            ]);
            $manpowerParams['comment'] = $msr['comment'] .'|'. $comment;
        }

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("source") == "internal") {
            $manpowerParams['internal'] = 1;
        }elseif (Input::get("source") == "external") {
            $manpowerParams['external'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        /******** UPDATE DATA ON MANPOWER SERVICES *********/
        $manpowerUpdate = $msr->update($manpowerParams);

        /******** INSERTING ATTACHMENTS ON MANPOWER SERVICES *********/
        $files = Input::get('files');

        if($files){

            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                if(file_exists(Config::get('rgas.rgas_temp_storage_path').$file['random_filename'])) {
                    $fm->move($file['random_filename'], $this->getStoragePath() . $msr['reference_no']);
                }
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $msr['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
            ManpowerserviceAttachments::insert($attachParams);
        }else{
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
        }

        /**** SETTING UP PARAMETERS FOR PROJECT REQUIREMENTS*****/
        $projectRequirements = json_decode(Input::get("projectRequirements"),true);

        $SMISParams = [];
        foreach ($projectRequirements as $key) {
            array_push($SMISParams,[
                'manpowerserviceid' => $msr['id'],
                'position_jobtitle' => $key['jobTitle'],
                'rec_dateneeded' => ($key['recruitment']['from'] != "" ? $key['recruitment']['from'] : null),
                'rec_dateneeded_to' => ($key['recruitment']['to'] != "" ? $key['recruitment']['to'] : null),
                'rec_pax' => ($key['recruitment']['pax'] != "" ? $key['recruitment']['pax'] : null),
                'prep_dateneeded' => ($key['preparation']['from'] != "" ? $key['preparation']['from'] : null),
                'prep_dateneeded_to' => ($key['preparation']['to'] != "" ? $key['preparation']['to'] : null),
                'prep_pax' => ($key['preparation']['pax'] != "" ? $key['preparation']['pax'] : null),
                'brif_dateneeded' => ($key['briefing']['from'] != "" ? $key['briefing']['from'] : null),
                'brif_dateneeded_to' => ($key['briefing']['to'] != "" ? $key['briefing']['to'] : null),
                'brif_pax' => ($key['briefing']['pax'] != "" ? $key['briefing']['pax'] : null),
                'fw_dateneeded' => ($key['fieldWork']['from'] != "" ? $key['fieldWork']['from'] : null),
                'fw_dateneeded_to' => ($key['fieldWork']['to'] != "" ? $key['fieldWork']['to'] : null),
                'fw_pax' => ($key['fieldWork']['pax'] != "" ? $key['fieldWork']['pax'] : null),
                'fw_area' => ($key['fieldWork']['area'] != "" ? $key['fieldWork']['area'] : null),
                'edit_dateneeded' => ($key['editing']['from'] != "" ? $key['editing']['from'] : null),
                'edit_dateneeded_to' => ($key['editing']['to'] != "" ? $key['editing']['to'] : null),
                'edit_pax' => ($key['editing']['pax'] != "" ? $key['editing']['pax'] : null),
                'code_dateneeded' => ($key['coding']['from'] != "" ? $key['coding']['from'] : null),
                'code_dateneeded_to' => ($key['coding']['to'] != "" ? $key['coding']['to'] : null),
                'code_pax' => ($key['coding']['pax'] != "" ? $key['coding']['pax'] : null),
                'enc_dateneeded' => ($key['encoding']['from'] != "" ? $key['encoding']['from'] : null),
                'enc_dateneeded_to' => ($key['encoding']['to'] != "" ? $key['encoding']['to'] : null),
                'enc_pax' => ($key['encoding']['pax'] != "" ? $key['encoding']['pax'] : null),
                'dc_dateneeded' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_dateneeded_to' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_pax' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null)
            ]);
        }

        /******** INSERTING DATA ON PROJECT REQUIREMENTS *********/
        if ($SMISParams != []) {
            ProjectRequirements::where('manpowerserviceid',$msr['id'])->delete();
            $SMISReqInsert = ProjectRequirements::insert($SMISParams);
        }
        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $msr['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        /******** INSERTING DATA ON MANPOWER AGENCY *********/
        if ($agenciesParam != []) {
            ManpowerAgency::where('manpowerserviceid',$msr['id'])->delete();
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerUpdate && $SMISReqInsert) {
            $this->logs->AU001($msr['id'],'MSR','id',json_encode($manpowerParams),$msr['reference_no']);

            if($files) {
                $this->logs->AU002($msr['id'], 'MSR', 'id', json_encode($manpowerParams),$msr['reference_no']);
            }

            $this->sendMail(
                'checking',
                'FOR APPROVAL',
                $msr['reference_no'],
                Session::get('firstname').' '.Session::get('middlename').' '.Session::get('lastname'),
                Session::get('dept_name'),
                $this->getEmail(Session::get("superiorid"))
            );
            return Redirect::to('msr/')
                ->with('successMessage', $currEmp['message']);

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function resave($id) {
        $msr = ManpowerService::where('id',$id)
            ->where('employeeid',Session::get("employee_id"))->first();
        /**** SETTING UP PARAMETERS *****/

        $manpowerParams = [
            'datefiled' => date('Y-m-d'),
            'contactno' => Input::get('contactNumber'),
            'status' => 'NEW',
            'panels' => Input::get('panel'),
            'projectname' => Input::get('projType'),
            'panelsize' => Input::get('sizePerPanel'),
            'objective' => Input::get('objective'),
            'otherinfo' => Input::get('otherInformation'),
            'comment' => json_encode(array(
                'name'=>Session::get('employee_name'),
                'datetime'=>date('m/d/Y g:i A'),
                'message'=>Input::get('comment'))),
        ];

        $section = Sections::where('sect_name',Input::get("section"))->count();
        if($section < 1) {
            $manpowerParams['othersection'] = Input::get("section") ;
        }else{
            $manpowerParams['sectionid'] = Session::get('sect_id');
        }

        if (Input::get("source") == "internal") {
            $manpowerParams['internal'] = 1;
        }elseif (Input::get("source") == "external") {
            $manpowerParams['external'] = 1;
        }

        if (Input::get("urgency") == "rush") {
            $manpowerParams['rush'] = 1;
        }elseif (Input::get("urgency") == "normal") {
            $manpowerParams['normalprio'] = 1;
        }

        /******** UPDATE DATA ON MANPOWER SERVICES *********/
        $manpowerUpdate = $msr->update($manpowerParams);

        /******** INSERTING ATTACHMENTS ON MANPOWER SERVICES *********/
        $files = Input::get('files');

        if($files){

            $fm = new Libraries\FileManager;
            $i = 1;
            $attachParams = [];
            foreach($files as $file){
                array_push($attachParams,[
                    'random_filename' => $file['random_filename'],
                    'original_filename' => $file['original_filename'],
                    'filesize' => $file['filesize'],
                    'mrf_id' => $msr['id']
                ]);
                $i++;
            }
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
            ManpowerserviceAttachments::insert($attachParams);
        }else{
            ManpowerserviceAttachments::where('mrf_id',$msr['id'])->delete();
        }

        /**** SETTING UP PARAMETERS FOR PROJECT REQUIREMENTS*****/
        $projectRequirements = json_decode(Input::get("projectRequirements"),true);

        $SMISParams = [];
        foreach ($projectRequirements as $key) {
            array_push($SMISParams,[
                'manpowerserviceid' => $msr['id'],
                'position_jobtitle' => $key['jobTitle'],
                'rec_dateneeded' => ($key['recruitment']['from'] != "" ? $key['recruitment']['from'] : null),
                'rec_dateneeded_to' => ($key['recruitment']['to'] != "" ? $key['recruitment']['to'] : null),
                'rec_pax' => ($key['recruitment']['pax'] != "" ? $key['recruitment']['pax'] : null),
                'prep_dateneeded' => ($key['preparation']['from'] != "" ? $key['preparation']['from'] : null),
                'prep_dateneeded_to' => ($key['preparation']['to'] != "" ? $key['preparation']['to'] : null),
                'prep_pax' => ($key['preparation']['pax'] != "" ? $key['preparation']['pax'] : null),
                'brif_dateneeded' => ($key['briefing']['from'] != "" ? $key['briefing']['from'] : null),
                'brif_dateneeded_to' => ($key['briefing']['to'] != "" ? $key['briefing']['to'] : null),
                'brif_pax' => ($key['briefing']['pax'] != "" ? $key['briefing']['pax'] : null),
                'fw_dateneeded' => ($key['fieldWork']['from'] != "" ? $key['fieldWork']['from'] : null),
                'fw_dateneeded_to' => ($key['fieldWork']['to'] != "" ? $key['fieldWork']['to'] : null),
                'fw_pax' => ($key['fieldWork']['pax'] != "" ? $key['fieldWork']['pax'] : null),
                'fw_area' => ($key['fieldWork']['area'] != "" ? $key['fieldWork']['area'] : null),
                'edit_dateneeded' => ($key['editing']['from'] != "" ? $key['editing']['from'] : null),
                'edit_dateneeded_to' => ($key['editing']['to'] != "" ? $key['editing']['to'] : null),
                'edit_pax' => ($key['editing']['pax'] != "" ? $key['editing']['pax'] : null),
                'code_dateneeded' => ($key['coding']['from'] != "" ? $key['coding']['from'] : null),
                'code_dateneeded_to' => ($key['coding']['to'] != "" ? $key['coding']['to'] : null),
                'code_pax' => ($key['coding']['pax'] != "" ? $key['coding']['pax'] : null),
                'enc_dateneeded' => ($key['encoding']['from'] != "" ? $key['encoding']['from'] : null),
                'enc_dateneeded_to' => ($key['encoding']['to'] != "" ? $key['encoding']['to'] : null),
                'enc_pax' => ($key['encoding']['pax'] != "" ? $key['encoding']['pax'] : null),
                'dc_dateneeded' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_dateneeded_to' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null),
                'dc_pax' => ($key['dataCleaning']['pax'] != "" ? $key['dataCleaning']['pax'] : null)
            ]);
        }

        /******** INSERTING DATA ON PROJECT REQUIREMENTS *********/
        if ($SMISParams != []) {
            ProjectRequirements::where('manpowerserviceid',$msr['id'])->delete();
            $SMISReqInsert = ProjectRequirements::insert($SMISParams);
        }
        $agencies = json_decode(Input::get("agencies"),true);
        $agenciesParam = [];
        foreach ($agencies as $key) {
            array_push($agenciesParam,[
                'manpowerserviceid' => $msr['id'],
                'agency' => $key['agency'],
                'agency_info' => $key['contactInfo']
            ]);
        }

        /******** INSERTING DATA ON MANPOWER AGENCY *********/
        if ($agenciesParam != []) {
            ManpowerAgency::where('manpowerserviceid',$msr['id'])->delete();
            ManpowerAgency::insert($agenciesParam);
        }

        if($manpowerUpdate) {
            $this->logs->AU001($msr['id'],'MSR','id',json_encode($manpowerParams),$msr['reference_no']);

            if($files) {
                $this->logs->AU002($msr['id'], 'MSR', 'id', json_encode($manpowerParams),$msr['reference_no']);
            }

            return Redirect::to('msr/')
                ->with('successMessage', 'MSR successfully updated');

        }

        return Redirect::back()
            ->with('errorMessage', 'Something went wrong upon submitting MSR');
    }

    public function toIQuest($id) {
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR APPROVAL')
            ->where("curr_emp",Session::get("employee_id"));
        $msrDetails = $msr->with('owner')
            ->with('department')
            ->first();
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }
        /************RETRIEVING RECEIVER***************/
        $receiver = Receivers::where("code",MSR_IQUEST_RECEIVER_CODE)->first();

        if (! $receiver) {
            return Redirect::back()
                ->with('errorMessage', 'No receiver declared');
        }


        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'FOR PROCESSING',
            'curr_emp' => $receiver['employeeid'],
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        /************INSERTING ENDORSED SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 1,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => Session::get("employee_id"),
            'approval_date' => date('Y-m-d')
        ]);

        /************INSERTING RECEIVED SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 2,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => $receiver['employeeid'],
            'approval_date' => date('Y-m-d')
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        $this->sendMail(
            'processing',
            'FOR PROCESSING',
            $msrDetails['reference_no'],
            $msrDetails['owner']['firstname'].' '.$msrDetails['owner']['middlename'].' '.$msrDetails['owner']['lastname'],
            $msrDetails['department']['dept_name'],
            $this->getEmail($receiver['employeeid'])
        );

        return Redirect::to('msr/')
            ->with('successMessage', 'MSR successfully sent to IQuest for processing.');
    }

    public function saveToArchive($id) {
        /************RETRIEVING DATA***************/
        $msr = ManpowerService::where('id',$id)
            ->where('status','FOR PROCESSING')
            ->where("curr_emp",Session::get("employee_id"));
        if(! $msr) {
            return Redirect::to('msr/')
                ->with('errorMessage', 'Something went wrong upon processing');
        }

        /************UPDATING DATA***************/
        $comment = json_encode([
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get('comment')
        ]);

        $update = $msr->update([
            'status' => 'PROCESSED',
            'comment' => DB::raw("concat(comment,'|','$comment')")
        ]);

        /************INSERTING SIGNATORIES***************/
        $signatureSeq = ManpowerSignatories::where('manpowerserviceid',$id)->max('signature_type_seq') + 1;
        ManpowerSignatories::insert([
            'manpowerserviceid' => $id,
            'signature_type' => 3,
            'signature_type_seq' => ($signatureSeq ? $signatureSeq : 1),
            'employee_id' => Session::get("employee_id"),
            'approval_date' => date('Y-m-d')
        ]);

        if (! $update) {
            return Redirect::back()
                ->with('errorMessage', 'Something went wrong');
        }

        return Redirect::to('msr/smis/submitted_msr')
            ->with('successMessage', 'MSR successfully archived.');
    }

    public function sendMail($action,$status,$refno,$filer,$department,$email) {
        if($email) {
            if ($action == "DISAPPROVED") {
                Mail::send('msr.email.msr', array(
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
//                    function($message) use ($email,$filer){
//                        $message->to($email,$filer)->subject('RGAS Notification Alert: Disapproved MSR');
//                    }
                    function($message) use ($email,$filer,$action){
                        $message->to($email,$filer)->subject("RGAS Notification Alert: MSR for $action");
                    }
                );
            }else{
                Mail::send('msr.email.msr', array(
                    'action' => $action,
                    'status' => $status,
                    'referenceNumber' => $refno,
                    'filer' => $filer,
                    'department' => $department,
                ),
                    function($message) use ($email,$filer,$action){
                        $message->to($email,$filer)->subject("RGAS Notification Alert: MSR for $action");
                    }
                );
            }
        }

    }

    private function getEmail($id) {
        return Employees::where('id',$id)->first()['email'];
    }

    private function getSectionHead() {
        $sectHeadID = Sections::where('id',Session::get('sect_id'))->first()['sect_head'];

        $secHead = Employees::find($sectHeadID);

        if($secHead){
            return $secHead->id;
        }else{
            return Session::get("superiorid");
        }
    }

    public function getReceiver($code) {
        $receiver = Receivers::where("code",$code)->first();
        $receiver = Employees::find($receiver->employeeid);
        if (! $receiver) {
            return Redirect::back()
                ->with('errorMessage', 'No receiver declared');
        }
        return $receiver;
    }

    private function getCurrEmp($source){
        $deptID =  Session::get('dept_id');
        $empID = Session::get('employee_id');
        $IsSecHead = Sections::where('sect_head',$empID)
            ->where('departmentid',$deptID)
            ->first();
        if ($source == "internal") {
            if($IsSecHead || Session::get("desig_level") == "head" || Session::get("is_smis_adh")) {
                $receiver = $this->getReceiver(MSR_IQUEST_RECEIVER_CODE);
                return [
                    'id' => $receiver->id,
                    'status' => "FOR PROCESSING",
                    'message' => "MSR successfully sent to IQuest for processing."
                ];
            }else{//employee/supervisor
                return [
                    'id' => $this->getSectionHead(),
                    'status' => "FOR APPROVAL",
                    'message' => "MSR successfully sent to Section Head/Immediate Superior for approval."
                ];
            }
        }else {
            $PDReceiver = $this->getReceiver(MSR_RECEIVER_CODE);
            $ADH = $this->getReceiver(MSR_SMIS_ADH);
            $Head = Employees::where([
                "departmentid" => Session::get("dept_id"),
                "desig_level" => "head",
            ])->first();
            if($IsSecHead) {
                return [
                    'id' => $ADH->id,
                    'status' => "FOR APPROVAL",
                    'message' => "MSR successfully sent to Assistant Department Head for approval."
                ];
            }elseif (Session::get("is_smis_adh")) {
                return [
                    'id' => $Head->id,
                    'status' => "FOR APPROVAL",
                    'message' => "MSR successfully sent to Department Head for approval."
                ];
            }elseif(Session::get("desig_level") == "head"){
                return [
                    'id' => $PDReceiver->id,
                    'status' => "FOR PROCESSING",
                    'message' => "MSR successfully sent to Purchasing for processing."
                ];
            }else{
                return [
                    'id' => $this->getSectionHead(),
                    'status' => "FOR APPROVAL",
                    'message' => "MSR successfully sent to Section Head/Immediate Superior for approval."
                ];
            }
        }

    }
}