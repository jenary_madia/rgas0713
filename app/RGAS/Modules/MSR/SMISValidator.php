<?php
/**
 * Created by PhpStorm.
 * User: octal
 * Date: 16/11/2016
 * Time: 3:56 PM
 */

namespace RGAS\Modules\MSR;
use Validator;
use Input;
use Redirect;


class SMISValidator
{
    public function send() {
        foreach (Input::all() as $key => $value) {
            $inputs[$key] = $value;
        }
        $rules = [
            "source" => "required",
            "panel" => "required",
            "urgency" => "required",
            "projType" => "required",
            "sizePerPanel" => "required",
            "objective" => "required",
            "projectRequirements" => "required|not_in:[]",
            "otherInformation" => "required",
        ];

        $message = [
            "source.required" => "Source is required",
            "panel.required" => "Panel is required",
            "urgency.required" => "Urgency is required",
            "projType.required" => "Project type/name is required",
            "sizePerPanel.required" => "Sample size per panel is required",
            "objective.required" => "Objective is required",
            "projectRequirements.not_in" => "At least one project requirement is required",
            "otherInformation.required" => "Other Information is required"
        ];

        $validate = Validator::make(
            $inputs,
            $rules,
            $message
        );

        if ($validate->fails())
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($validate)
                ->with('message', 'Some fields are incomplete.');
        }

        return true;

    }
}