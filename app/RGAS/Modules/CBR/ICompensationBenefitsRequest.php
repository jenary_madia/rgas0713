<?php
namespace app\CBR;

interface ICompensationBenefitsRequest
{
	public function assignRequest($input);
	public function returnRequest($input);
	public function forAcknowledgement($input);
}
?>