<?php namespace app\CBR;

use CompensationBenefits;
use CompensationBenefitsRequestedDocs;
use Session;

class CompensationBenefitsRequest implements ICompensationBenefitsRequest{
	
	
	public function assignRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cbrd->cbrd_assigned_to = $input['emp_id'];
		$cbrd->cbrd_assigned_date = date("Y-m-d");
		$cbrd->cbrd_remarks = $input['cbrd_remarks'];
		$cbrd->cbrd_current = $input['emp_id'];
		$cbrd->cbrd_status = 'For Processing';
		$cbrd->save();
	}
	
	public function returnRequest($input)
	{
		$cbrd = $this->find(Input::get('cbrd_cb_id'));
		$cbrd->cbrd_status = 'Returned';
		$cbrd->cbrd_remarks = Input::get('cbrd_remarks');
		$cbrd->save();
	}
	
	public function forAcknowledgement($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",$input['cbrd_id'])->get();
		$cbrd = CompensationBenefitsRequestedDocs::find($cbrd[0]['cbrd_id']);
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		echo "<pre>",print_r($cbrd),"</pre>";
		echo "<pre>",print_r($cb),"</pre>";
		echo "<pre>",print_r($input),"</pre>";
		if($input['comment'] != "") {
			$comments = (array)json_decode($cb->cb_comments);
			$comments[] = array(
				'name' => Session::get('employee_name'),
				'datetime' => date('m/d/Y g:i A'),
				'message' => $input['comment']
			);
			echo "<pre>", print_r($comments), "</pre>";
			$cb->cb_comments = json_encode($comments);
		}
		$cb->save();
		
		$cbrd->cbrd_current = $cb->cb_employees_id;
		$cbrd->cbrd_status = 'For Acknowledgement';
		$cbrd->cbrd_endorsed_to_requestor_date = $input['date_endorsed_to_requestor'];
		$cbrd->save();
		
	}
	
	public function doSomethings()
	{
		echo "trace";
	}
}
?>