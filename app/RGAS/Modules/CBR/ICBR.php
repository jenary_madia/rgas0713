<?php namespace RGAS\Modules\CBR;

interface ICBR
{
	// private function getStoragePath();
	public function getModuleId();
	public function updateDraftRequest($cb_ref_num, $input);
	public function forAcknowledgement($input);
	public function storeRequest($input);
	public function assignRequest($input);
	public function returnRequest($input);
	public function returnedEdit($input);
	public function reSubmitRequest($input);
	public function acknowledgedRequest($input);
	public function deleteRequest($status,$ref_num);
	public function draftviewLogCbr($cb_ref_num);
	public function viewLogs($cbrd_id, $cbrd_ref_num);
	public function downloadLog($cb_ref_num);
}
?>