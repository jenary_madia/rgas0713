<?php namespace RGAS\Modules\CBR;

use AuditTrail;
use Session;

class CBRLogs {
	private $table;
	private $primaryKey;

	public function setTable($table)
	{
		$this->table = $table;
	}
	
	public function setPrimaryKey($primaryKey)
	{
		$this->primaryKey = $primaryKey;
	}
	
	//Create
	protected function AU001($cb_id, $new, $cb_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU001",
			// $this->module_id,
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cb_id)),
			"",
			$new,
			$cb_ref_num
		);
	}
	
	//Upload File
	protected function AU002($cb_id, $new, $cb_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU002",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cb_id)),
			"",
			$new,
			$cb_ref_num
		);
	}
	
	//Retrieve
	protected function AU003($cbrd_id, $cbrd_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU003",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cbrd_id)),
			"",
			"",
			$cbrd_ref_num
		);
	}
	
	//Edit and Update(workflow of request)
	protected function AU004($cbrd_id, $old, $new, $cbrd_ref_num)
	{
		AuditTrail::store(		
			Session::get('employee_id'),
			"AU004",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cbrd_id)),
			$old,
			$new,
			$cbrd_ref_num
		);
	}
	
	//Delete
	protected function AU005($cb_id, $old_cb_status, $new_cb_status, $cb_ref_num)
	{		
		AuditTrail::store(
			Session::get('employee_id'),
			"AU005",
			$this->getModuleId(),
			$this->table,
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cb_id)),
			$old_cb_status,
			$new_cb_status,
			$cb_ref_num
		);
	}
	
	//Download
	protected function AU008($cbrd_id, $new, $cbrd_ref_num)
	{
		AuditTrail::store(
			Session::get('employee_id'),
			"AU008",
			$this->getModuleId(),
			"",
			json_encode(array("{$this->table}.{$this->primaryKey}"=>$cbrd_id)),
			"",
			$new,
			$cbrd_ref_num
		);
	}

}
?>



