<?php namespace RGAS\Modules\CBR;

use Receivers;
use Employees;
use CompensationBenefits;

class EmailReceivers extends CBRLogs{
	function getReceiver(){
		$receiver = Receivers::where('code','=','CBR_RECEIVER')->where('module','=','cbr')->first();
		return Employees::find($receiver->employeeid);
	}
	
	function getStaff(){
		$receiver = Receivers::where('code','=','CBR_STAFF')->where('module','=','cbr')->first();
		return Employees::find($receiver->employeeid);
	}
	
	function getFilerFromCompensationBenefits($id)
	{
		return CompensationBenefits::find($id);
	}
}

?>