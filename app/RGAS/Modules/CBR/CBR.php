<?php namespace RGAS\Modules\CBR;

use CompensationBenefits;
use CompensationBenefitsRequestedDocs;
use ReferenceNumbers;
use Session;
use RGAS\Libraries;
use RGAS\Repositories\CBRRepository;
use Mail; 	
use RGAS\Libraries\AuditTrail;
use Config;
use Input;
use Employees;
use RGAS\Helpers\EmployeesHelper;


class CBR extends EmailReceivers  implements ICBR
{
	public $attachments_path = 'cbr/';
	protected $module_id;
	
	private function getStoragePath()
	{
		return Config::get('rgas.rgas_storage_path').'cbr/';
	}
	
	public function getModuleId()
	{
		$this->module_id = Config::get('rgas.module_id_cbr');
		return $this->module_id;
	}

	public function updateDraftRequest($cb_ref_num, $input)
	{
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbRefNum($cb_ref_num);
		$cbr_receiver = $cbrd->getCbrReceiver();		
		
		$old = json_encode($request['attributes']);

		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = @json_decode($old_files, true);
			}
		}
	
		if($input['action'] == 'save')
		{
			$issent = 0;
			$status = "New";
			$current = Session::get('employee_id');
		}
		elseif($input['action'] == 'send')
		{
			$issent = 1;
			$status = "For Processing";
			$current = $cbr_receiver->employeeid;
		}
		
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbRefNum($cb_ref_num);
		
		$cb = CompensationBenefits::find($request->cb_id);		
        $cb->cb_employees_id = Session::get('employee_id');
        $cb->cb_emp_name = Session::get('employee_name');
		$cb->cb_emp_id = Session::get('employeeid');
		$cb->cb_company = Session::get('company');
		$cb->cb_department = Session::get('dept_name');
		$cb->cb_section = $input['sect_name'];
		$cb->cb_isdeleted = 0;
		$cb->cb_ref_num = $input['cb_ref_num'];
        $cb->cb_date_filed = date('Y-m-d');
        $cb->cb_status = $status;
        $cb->cb_contact_no = $input['cb_contact_number'];
        $cb->cb_urgency = $input['cb_urgency'];
        $cb->cb_date_needed = $input['date_needed'];
		$cb->cb_purpose_of_request = $input['cb_purpose_of_request'];
		$cb->cb_attachments = @json_encode($input['files']);
		// if($input['comment'] != ""){
			$cb->cb_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>$input['comment']
					)
				)
			);
		// }
		// $comments = (array) json_decode($cb->cb_comments);
		// $comments[] = array(
			// 'name'=>Session::get('employee_name'),
			// 'datetime'=>date('m/d/Y g:i A'),
			// 'message'=>$input['comment']
		// );
		// $cb->cb_comments = json_encode($comments);
		$cb->cb_issent = $issent;

		try{
			$cb->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			//ReferenceNumbers::increment_reference_number('cbr');
			//$cb->cb_ref_num = ReferenceNumbers::get_current_reference_number('cbr');

			$cb->cb_ref_num =ReferenceNumbers::get_current_reference_number_cbr();
			$cb->save();
		}
		$new = json_encode($cb['attributes']);
		
		$this->setTable('compensationbenefits');
		$this->setPrimaryKey('cb_id');
		$this->AU004($cb->cb_id, $old, $new, $cb->cb_ref_num);
		
		$oldCbrd = CompensationBenefitsRequestedDocs::where("cbrd_cb_id","=",$cb->cb_id)->first();
		$oldVal = json_encode($oldCbrd['attributes']);
		
		$cbrd = new CompensationBenefitsRequestedDocs;
		$cbrd->where('cbrd_cb_id', '=', $cb->cb_id)->delete();
		$ctr = 1;
		$b = array();
		
		foreach($input['requested_documents'] as $c => $document){
			$a = array();
			if(array_key_exists('name',$document)){
				$a['cbrd_cb_id'] = $cb->cb_id;
				$a['cbrd_req_docs'] = json_encode(array($c =>$document));
				$a['cbrd_ref_num'] = $cb->cb_ref_num . '-' . $ctr;
				$a['cbrd_status'] = $status;
				$a['cbrd_date_filed'] = $input['cb_date_filed'];
				$a['cbrd_current'] = $current;
				
				$cbrd_id = $cbrd->insertGetId($a);
				$this->setTable('compensationbenefitsrequesteddocs');
				$this->setPrimaryKey('cbrd_id');
				$this->AU004($cbrd_id, $oldVal, json_encode($a), $a['cbrd_ref_num']);
				
				unset($a);
				$ctr++;
			}
		}
		if(count($b) > 0) 
		{
			$cbrd->insert($b);
			
			foreach($b as $c)
			{
				AuditTrail::store(
					Session::get('employee_id'),
					"AU004",
					$this->module_id,
					"compensationbenefitsrequesteddocs",
					json_encode(array_keys($c)),
					"",
					json_encode(array_values($c)),
					$c['cbrd_ref_num']
				);
				
				
			}
		}
		
		if($issent == 1){
			Mail::send('cbr.email_templates.request_for_processing_receiver', array(
				'status' => $status,
				'ref_num' => $cb->cb_ref_num,
				'requestor' => $cb->cb_emp_name,
				'department' => $cb->cb_department),
				function($message){
					$message->to($this->getReceiver()->email, $this->getReceiver()->firstname." ".$this->getReceiver()->lastname)->subject('RGTS Notification: CBR Request for Processing');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					$fm->move($file['random_filename'], $this->getStoragePath().$cb->cb_ref_num); //glenn
				}
			}
		}
		
		return true;
	}
	
	public function storeRequest($input)
	{
		$cbrR = new CBRRepository;
		$cbr_receiver = $cbrR->getCbrReceiver();
		
		if($input['action'] == 'save')
		{
			$issent = 0;
			$status = "New";
			$current = Session::get('employee_id');
		}
		elseif($input['action'] == 'send')
		{
			$issent = 1;
			$status = "For Processing";
			$current = $cbr_receiver->employeeid;
		}
		
		$cb = new CompensationBenefits;
        $cb->cb_employees_id = Session::get('employee_id');
        $cb->cb_emp_name = Session::get('employee_name');
		$cb->cb_emp_id = Session::get('employeeid');
		$cb->cb_company = Session::get('company');
		$cb->cb_department = Session::get('dept_name');
		$cb->cb_section = $input['sect_name'];
		//$cb->cb_ref_num = ReferenceNumbers::get_current_reference_number('cbr');
		$cb->cb_ref_num =ReferenceNumbers::get_current_reference_number_cbr();
        $cb->cb_date_filed = $input['cb_date_filed'];
        $cb->cb_status = $status;
		$cb->cb_isdeleted = 0;
        $cb->cb_contact_no = $input['cb_contact_number'];
        $cb->cb_urgency = $input['cb_urgency'];
        $cb->cb_date_needed = $input['date_needed'] ? $input['date_needed'] : NULL;
		$cb->cb_purpose_of_request = $input['cb_purpose_of_request'];
        $cb->cb_attachments = @json_encode($input['files']);
		// if($input['comment'] != ""){
			$cb->cb_comments = json_encode(
				array(
					array(
						'name'=>Session::get('employee_name'),
						'datetime'=>date('m/d/Y g:i A'),
						'message'=>$input['comment']
					)
				)
			);
		// }
		$cb->cb_issent = $issent;

		try{
			$cb->save();
		}
		catch(\Illuminate\Database\QueryException $e){
			ReferenceNumbers::increment_reference_number('cbr');
			//$cb->cb_ref_num = ReferenceNumbers::get_current_reference_number('cbr');
			$cb->cb_ref_num =ReferenceNumbers::get_current_reference_number_cbr();
			$cb->save();
		}
		
		$this->setTable('compensationbenefits');
		$this->setPrimaryKey('cb_id');
		$this->AU001($cb->cb_id, json_encode($cb['attributes']), $cb->cb_ref_num);
		
		if(@count($input['files']) > 0){
			foreach($input['files'] as $file){
				$this->AU002($cb->cb_id, json_encode($file), $cb->cb_ref_num);
			}	
		}
       	
		
		$cbrd = new CompensationBenefitsRequestedDocs;
		$ctr = 1;
		$b = array();

		foreach($input['requested_documents'] as $c => $document){
			$a = array();
		
			
			if(array_key_exists('name',$document)){

				$a['cbrd_cb_id'] = $cb->cb_id;
				$a['cbrd_req_docs'] = json_encode(array($c =>$document));
				$a['cbrd_ref_num'] = $cb->cb_ref_num . '-' . $ctr;
				$a['cbrd_status'] = $status;
				$a['cbrd_date_filed'] = $input['cb_date_filed'];
				$a['cbrd_current'] = $current;
				
				$cbrd_id = $cbrd->insertGetId($a);
				$this->setTable('compensationbenefitsrequesteddocs');
				$this->setPrimaryKey('cbrd_id');
				$this->AU001($cbrd_id, json_encode($a), $a['cbrd_ref_num']);

				unset($a);
				$ctr++;
			}
		}
		
		if($issent == 1){
			Mail::send('cbr.email_templates.request_for_processing_receiver', array(
				'status' => $status,
				'ref_num' => $cb->cb_ref_num,
				'requestor' => $cb->cb_emp_name,
				'department' => $cb->cb_department),
				function($message){
					$message->to($this->getReceiver()->email, $this->getReceiver()->firstname." ".$this->getReceiver()->lastname)->subject('RGTS Notification: CBR Request for Processing');
				}
			);
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					$fm->move($file['random_filename'], $this->getStoragePath().$cb->cb_ref_num);
				}
			}
		}
		return true;
	}
	
	public function assignRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$old = '"'.$cbrd->cbrd_assigned_to.'","'.$cbrd->cbrd_assigned_date.'","'.$cbrd->cbrd_current.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_remarks.'"';
		
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$cbrd->cbrd_assigned_to = $input['emp_id'];
		$cbrd->cbrd_assigned_date = date("Y-m-d");
		$cbrd->cbrd_current = $input['emp_id'];
		$cbrd->cbrd_status = 'For Processing';
		$cbrd->cbrd_remarks = $input['cbrd_remarks'];
		$cbrd->save();
		
		$new = '"'.$cbrd->cbrd_assigned_to.'","'.$cbrd->cbrd_assigned_date.'","'.$cbrd->cbrd_current.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_remarks.'"';
		
		Mail::send('cbr.email_templates.request_for_processing_staff', array(
			'status' => $cbrd->cbrd_status,
			'ref_num' => $cbrd->cbrd_ref_num,
			'requestor' => $cb->cb_emp_name,
			'department' => $cb->cb_department),
			function($message){
				$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",Input::get('cbrd_id'))->first();
				$emp = Employees::where("id","=",$cbrd->cbrd_assigned_to)->first();
				$message->to($emp->email, $emp->firstname." ".$emp->lastname)->subject('RGTS Notification: CBR Request for Processing');
			}
		);
		
		$this->setTable('compensationbenefitsrequesteddocs');
		$this->setPrimaryKey('cbrd_id');
		$this->AU004($cbrd->cbrd_cb_id, $old, $new, $cbrd->cbrd_ref_num);

			
		return true;
	}
	
	public function returnRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$old = '"'.$cbrd->cbrd_assigned_to.'","'.$cbrd->cbrd_assigned_date.'","'.$cbrd->cbrd_current.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_remarks.'"';
	 
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$cbrd->cbrd_assigned_to = '';
		$cbrd->cbrd_assigned_date = '';
		$cbrd->cbrd_current = $cb->cb_employees_id;
		$cbrd->cbrd_status = 'Returned';
		$cbrd->cbrd_remarks = $input['cbrd_remarks'];
		$cbrd->save();
		
		$new = '"'.$cbrd->cbrd_assigned_to.'","'.$cbrd->cbrd_assigned_date.'","'.$cbrd->cbrd_current.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_remarks.'"';
		
		$this->setTable('compensationbenefitsrequesteddocs');
		$this->setPrimaryKey('cbrd_id');
		$this->AU004($cbrd->cbrd_cb_id, $old, $new, $cbrd->cbrd_ref_num);


		Mail::send('cbr.email_templates.for_acknowledgement', array(
			'status' => 'Returned' ,
			'ref_num' => $cbrd->cbrd_ref_num,
			'cbr_staff' => Session::get('employee_name')),
			
			function($message) use ($cb) {
				$emp = Employees::where("id","=", $cb->cb_employees_id )->first();

				$message->to($emp->email, $emp->firstname . " " . $emp->lastname)->subject('RGTS Notification: Returned CBR Request ');
			}
		);

		return true;
	}
	
	public function returnedEdit($input)
	{
		$cbrd = new CBRRepository;
		$request = $cbrd->getRequestByCbRefNum($input['cb_ref_num']);
		$cbr_receiver = $cbrd->getCbrReceiver();
		if($input['action'] == 'save')
		{
			$status = "Returned";
			$curr = Input::get('cb_employees_id');
		}
		
		elseif($input['action'] == 'send')
		{
			$status = "For Processing";
			$curr = $cbr_receiver->employeeid;
			
			if(isset($input['files']) && count($input['files'] > 0)){
				$fm = new Libraries\FileManager;	
				foreach($input['files'] as $file){
					$fm->move($file['random_filename'], $this->getStoragePath().$input['cb_ref_num']); //glenn
				}
			}
		}
		
		
		
		$cb = CompensationBenefits::find($request->cb_id);

		if(isset($input['current_files']) && is_array($input['current_files']))
		{
			foreach($input['current_files'] as $old_files)
			{
				$input['files'][] = json_decode($old_files, true);
			}
		}

        $cb->cb_status = $status;
        $cb->cb_contact_no = $input['cb_contact_number'];
        $cb->cb_urgency = $input['cb_urgency'];
        $cb->cb_date_needed = $input['date_needed'];
		$cb->cb_purpose_of_request = $input['cb_purpose_of_request'];
		$cb->cb_attachments = @json_encode($input['files']);
		
		if($input['action'] == 'send')
		{
			$comments = (array) json_decode($cb->cb_comments);
			$comments[] = array(
				'name'=>Session::get('employee_name'),
				'datetime'=>date('m/d/Y g:i A'),
				'message'=>$input['comment']
			);
			$cb->cb_comments = json_encode($comments);
        }
        else
        {
        	//ADD HERE FOR COMMENT SAVE
        }

        $cb->save();
		
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);

		// $a = array( key($input['requested_documents']) => $input['requested_documents']{key($input['requested_documents'])} );
		$cbrd->cbrd_req_docs = json_encode( 
			array( 
				key($input['requested_documents']) => $input['requested_documents']{key($input['requested_documents'])}
			) 
		);
		$cbrd->cbrd_assigned_to = NULL;
		$cbrd->cbrd_status = $status;
		$cbrd->cbrd_current = $curr;
		$cbrd->save();
		
		
		Mail::send('cbr.email_templates.request_for_processing_receiver', array(
			'status' => $status,
			'ref_num' => $cbrd->cbrd_ref_num,
			'requestor' => $cb->cb_emp_name,
			'department' => $cb->cb_department),
			function($message){
				$message->to($this->getReceiver()->email, $this->getReceiver()->firstname." ".$this->getReceiver()->lastname)->subject('RGTS Notification: CBR Request for Processing');
			}
		);
		
		return true;		
	}
	
	public function reSubmitRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		$cbrd->cbrd_status = 'New';
		$cbrd->save();
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
		$comments = (array) json_decode($cb->cb_comments);
		if($input['comment'] != ""){
			$comments[] = array(
				'name'=>Session::get('employee_name'),
				'datetime'=>date('m/d/Y g:i A'),
				'message'=>$input['comment']
			);
			$cb->cb_comments = json_encode($comments);
		}

		$fm = new Libraries\FileManager;		
		$filenames = $fm->upload($this->attachments_path);
		if(is_array($filenames) && count($filenames) > 0){
			$attachments = json_decode($cb->cb_attachments,'array');
			if( (is_array($attachments) && count($attachments) > 0)){
				$filenames = array_merge($filenames, $attachments);
			}
			// $cb->cb_attachments = json_encode($filenames);
			$cb->cb_attachments = @json_encode($input['files']);
		}
		$cb->cb_issent = 1;
		$cb->save();
		
		Mail::send('cbr.email_templates.request_for_processing_receiver', array(
			'status' => $status,
			'ref_num' => $cbrd->cbrd_ref_num,
			'requestor' => $cb->cb_emp_name,
			'department' => $cb->cb_department),
			function($message){
				$message->to($this->getReceiver()->email, $this->getReceiver()->firstname." ".$this->getReceiver()->lastname)->subject('RGTS Notification: CBR Request for Processing');
			}
		);
		
		return true;
	}
	
	public function forAcknowledgement($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",$input['cbrd_id'])->get();
			
		$cbrd = CompensationBenefitsRequestedDocs::find($cbrd[0]['cbrd_id']);
		$cb = CompensationBenefits::find($cbrd->cbrd_cb_id);
	
		$comments = (array) json_decode($cb->cb_comments);
		$comments[] = array(
			'name'=>Session::get('employee_name'),
			'datetime'=>date('m/d/Y g:i A'),
			'message'=>$input['comment']
		);
		
		$cb->cb_comments = json_encode($comments);
		$cb->save();
		
		$cbrd->cbrd_current = $cb->cb_employees_id;
		$cbrd->cbrd_status = 'For Acknowledgement';
		$cbrd->cbrd_endorsed_to_requestor_date = $input['date_endorsed_to_requestor'];
		
		$cbrd->save();	
		
		Mail::send('cbr.email_templates.for_acknowledgement', array(
			'status' => $cbrd->cbrd_status,
			'ref_num' => $cbrd->cbrd_ref_num,
			'cbr_staff' => EmployeesHelper::getEmployeeNameById($cbrd->cbrd_assigned_to)),
			
			function($message){
				$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",Input::get('cbrd_id'))->first();
				$cbr = CompensationBenefits::find($cbrd->cbrd_cb_id);
				$emp = Employees::where("id","=",$cbr->cb_employees_id)->first();
				$message->to($emp->email, $cbr->cb_emp_name)->subject('RGTS Notification: CBR Request for Acknowledgement');
			}
		);
		
		return true;
	}
	
	public function acknowledgedRequest($input)
	{
		$cbrd = CompensationBenefitsRequestedDocs::find($input['cbrd_id']);
		
		$old = '"'.$cbrd->cbrd_acknowledged_date.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_completed_date.'"';
		
		$cbrd->cbrd_acknowledged_date = date("Y-m-d");
		$cbrd->cbrd_status = "Acknowledged";
		$cbrd->cbrd_completed_date = date("Y-m-d");
		$cbrd->save();
		
		Mail::send('cbr.email_templates.acknowledged', array(
			'status' => $cbrd->cbrd_status,
			'ref_num' => $cbrd->cbrd_ref_num,
			'requestor' => $this->getFilerFromCompensationBenefits($cbrd->cbrd_cb_id)->cb_emp_name,
			'department' => $this->getFilerFromCompensationBenefits($cbrd->cbrd_cb_id)->cb_department),
			
			function($message){
				$cbrd = CompensationBenefitsRequestedDocs::where("cbrd_id","=",Input::get('cbrd_id'))->first();
				$cbr = CompensationBenefits::find($cbrd->cbrd_cb_id);
				$emp = Employees::where("id","=",$cbr->cb_employees_id)->first();
				$message->to($emp->email, $cbr->cb_emp_name)->subject('RGTS Notification: CBR Request for Acknowledgement');
			}
		);
		
		
		$new = '"'.$cbrd->cbrd_acknowledged_date.'","'.$cbrd->cbrd_status.'","'.$cbrd->cbrd_completed_date.'"';
		
		return true;
	}
	
	/*
	public function deleteRequest($ref_num)
	{	
		$cbr = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->first();
		
		if(count($cbr) > 0){
			$old_cb_status = $cbr->cbrd_status;
		}
		
		$affectedRows = 0;
		if(count($cbr) > 0)
		{
		$cb = CompensationBenefits::where('cb_id','=',$cbr['cbrd_cb_id'])->first();
			// $cbrd = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->first();
			$cbrd = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->first();
			
			if($cbrd){
				$old_cbrd_status = $cbrd->cbrd_status;
				
				$cbrd->cbrd_status = $cbrd->cbrd_status."/Deleted";
				$cbrd->cbrd_isdeleted = 1;
				$cbrd->save();
				
				$new_cbrd_status = $cbrd->cbrd_status;
				
				$cbrD = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->get();
				foreach($cbrD as $a)
				{
					$this->setTable('compensationbenefitsrequesteddocs');
					$this->setPrimaryKey('cbrd_cb_id');
					$this->AU005($a->cbrd_cb_id, $old_cbrd_status, $new_cbrd_status, $a->cbrd_ref_num);
			
				}
			}

			$cb->cb_status = $cb->cb_status."/Deleted";
			$cb->cb_isdeleted = 1;
			$cb->save();
			
			$new_cb_status = $cb->cb_status;
			
			$this->setTable('compensationbenefits');
			$this->setPrimaryKey('cb_id');
			$this->AU005($cb->cb_id, $old_cb_status, $new_cb_status, $cb->cb_ref_num);
		}
		
		else
		{
			$cbrd = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->first();
			$old_cbrd_status = $cbrd->cbrd_status;
			
			$cbrd->cbrd_status = $cbrd->cbrd_status."/Deleted";
			$cbrd->cbrd_isdeleted = 1;
			$cbrd->save();
			
			$new_cbrd_status = $cbrd->cbrd_status;
			
			$cbrD = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->get();
			foreach($cbrD as $a)
			{
				$this->setTable('compensationbenefitsrequesteddocs');
				$this->setPrimaryKey('cbrd_cb_id');
				$this->AU005($a->cbrd_cb_id, $old_cbrd_status, $new_cbrd_status, $a->cbrd_ref_num);
		
			}
		}
		
	}*/
	
	public function deleteRequest($status,$ref_num)
	{	

		switch($status){
			case "New":
				$cb = CompensationBenefits::where('cb_ref_num','=',$ref_num)->first();
				$oldCbStat = $cb->cb_status;
				$cbrd = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb->cb_id)->get(); // this should be loop
				
				foreach($cbrd as $a){
					$oldCbrDStat = $a['cbrd_status'];
				}
				
				CompensationBenefits::where('cb_ref_num','=',$ref_num)->update(['cb_status'=>$status."/Deleted",'cb_isdeleted'=>1]);
				CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb->cb_id)->update(['cbrd_status'=>$status."/Deleted",'cbrd_isdeleted'=>1]);
				$cb_id = $cb->cb_id; 
				$newCbStat = $cb->cb_status;
				
				// logs goes here
				$this->setTable('compensationbenefits');
				$this->setPrimaryKey('cb_id');
				$this->AU005($cb->cb_id, $oldCbStat, $newCbStat, $cb->cb_ref_num);
				
			break;
			
			case "Acknowledged":
				$cbrd = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->first(); // single return
				$oldCbrDStat = $cbrd['cbrd_status'];
				$cb = CompensationBenefits::where('cb_id','=',$cbrd->cbrd_cb_id)->first();
				CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->update(['cbrd_status'=>$status."/Deleted"]);
				$newCbrDStat = $cbrd['cbrd_status'];
				CompensationBenefits::where('cb_id','=',$cbrd->cbrd_cb_id)->update(['cb_status'=>$status."/Deleted" ]);
				
				// logs goes here
				$this->setTable('compensationbenefitsrequesteddocs');
				$this->setPrimaryKey('cbrd_cb_id');
				$this->AU005($cbrd->cbrd_cb_id, $oldCbrDStat, $newCbrDStat, $cbrd->cbrd_ref_num);
				

			break;
		}
		
		// $cb->cb_status = $status."/Deleted";
		// $cb->cb_isdeleted = 1;
		// $cb->save();
		
		// $cbrd->cbrd_status = $status."/Deleted";
		// $cbrd->cbrd_isdeleted = 1;
		// $cbrd->save();
				
		// $cbr = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->first();
		
		// if(count($cbr) > 0){
			// $old_cb_status = $cbr->cbrd_status;
		// }
		
		// $affectedRows = 0;
		// if(count($cbr) > 0)
		// {
			// $cb = CompensationBenefits::where('cb_id','=',$cbr['cbrd_cb_id'])->first();
			// // $cbrd = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->first();
			// $cbrd = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->first();
			
			// if($cbrd){
				// $old_cbrd_status = $cbrd->cbrd_status;
				
				// $cbrd->cbrd_status = $cbrd->cbrd_status."/Deleted";
				// $cbrd->cbrd_isdeleted = 1;
				// $cbrd->save();
				
				// // $new_cbrd_status = $cbrd->cbrd_status;
				
				// // $cbrD = CompensationBenefitsRequestedDocs::where('cbrd_cb_id','=',$cb['cb_id'])->get();
				// // foreach($cbrD as $a)
				// // {
					// // $this->setTable('compensationbenefitsrequesteddocs');
					// // $this->setPrimaryKey('cbrd_cb_id');
					// // $this->AU005($a->cbrd_cb_id, $old_cbrd_status, $new_cbrd_status, $a->cbrd_ref_num);
			
				// // }
			// }

			// $cb->cb_status = $cb->cb_status."/Deleted";
			// $cb->cb_isdeleted = 1;
			// $cb->save();
			
			// // $new_cb_status = $cb->cb_status;
			
				// // $this->setTable('compensationbenefits');
				// // $this->setPrimaryKey('cb_id');
				// // $this->AU005($cb->cb_id, $old_cb_status, $new_cb_status, $cb->cb_ref_num);
		// }
		
		// else
		// {
			// // $cbrd = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->first();
			
			// // $old_cbrd_status = $cbrd->cbrd_status;
			
			// // $cbrd->cbrd_status = $cbrd->cbrd_status."/Deleted";
			// // $cbrd->cbrd_isdeleted = 1;
			// // $cbrd->save();
			
			// // $new_cbrd_status = $cbrd->cbrd_status;
			
			// // $cbrD = CompensationBenefitsRequestedDocs::where('cbrd_ref_num','=',$ref_num)->get();
			// // foreach($cbrD as $a)
			// // {
				// // $this->setTable('compensationbenefitsrequesteddocs');
				// // $this->setPrimaryKey('cbrd_cb_id');
				// // $this->AU005($a->cbrd_cb_id, $old_cbrd_status, $new_cbrd_status, $a->cbrd_ref_num);
		
			// // }
		// }
		
	}
	
	public function doSomethings()
	{
	}
	
	public function draftviewLogCbr($cb_ref_num)
	{
		$cbr = new CBRRepository;
		$request = $cbr->getRequestByCbRefNum($cb_ref_num);
		
		$this->setTable('compensationbenefits');
		$this->setPrimaryKey('cb_id');
		$this->AU003($request->cb_id, $cb_ref_num);
	}
	
	public function viewLogs($cbrd_id, $cbrd_ref_num)
	{
		$this->setTable('compensationbenefitsrequesteddocs');
		$this->setPrimaryKey('cbrd_id');
		$this->AU003($cbrd_id, $cbrd_ref_num);
	}
	
	public function downloadLog($cb_ref_num)
	{
		$cb = new CBRRepository;
		$request = $cb->getRequestByCbRefNum($cb_ref_num);
		
		$this->setTable('compensationbenefits');
		$this->setPrimaryKey('cb_id');
	
		$cb_id = $request->cb_id;
		$cb_ref_num = $request->cb_ref_num;
		
		foreach(json_decode($request['cb_attachments']) as $attachment)
			$new = $attachment->original_filename;
		$this->AU008($cb_id, $new, $cb_ref_num);
	}
	
}
?>