<?php namespace RGAS\Modules\CBR\Requests;

use Input;
use Validator;
use Session;
use Redirect;
use Request;

class Store
{
	public function validate()
	{
		$input = Input::all();
		// echo "<pre>";
			// print_r($input);
		// echo "</pre>";
		// die();
		// edit starts here
		$inputs['employee_name'] = $input['employee_name'];
		$rules['employee_name'] = 'required';
		$messages['employee_name.required'] = 'Employee Name is required';
		
		$inputs['employeeid'] = $input['employeeid'];
		$rules['employeeid'] = 'required';
		$messages['employeeid.required'] = 'Employee ID is required';
		
		$inputs['comp_name'] = $input['comp_name'];
		$rules['comp_name'] = 'required';
		$messages['comp_name.required'] = 'Company name is required';
		
		$inputs['dept_name'] = $input['dept_name'];
		$rules['dept_name'] = 'required';
		$messages['dept_name.required'] = 'Department name is required';
		
		$inputs['sect_name'] = $input['sect_name'];
		$rules['sect_name'] = '';
		$messages['sect_name.required'] = 'Section name is required';
		
		// $inputs['cb_ref_num'] = $input['cb_ref_num'];
		// $rules['cb_ref_num'] = 'required';
		// $messages['cb_ref_num.required'] = 'Reference number is required';
		
		$inputs['cb_date_filed'] = $input['cb_date_filed'];
		$rules['cb_date_filed'] = 'required|date';
		$messages['cb_date_filed.required'] = 'Date filed is required';
		$messages['cb_date_filed.date'] = 'Date filed must be a valid date';
		
		$inputs['cb_status'] = $input['cb_status'];
		$rules['cb_status'] = 'required';
		$messages['cb_status.required'] = 'Status is required';
		
		$inputs['cb_contact_number'] = $input['cb_contact_number'];
		$rules['cb_contact_number'] = 'max:50';
		$messages['cb_contact_number.max'] = 'Contact Number field length must not be greater 50 characters';
		
		$inputs['date_needed'] = $input['date_needed'];
		$rules['date_needed'] = 'date';
		$messages['date_needed.date'] = 'Date needed must be a valid date';

		
		if($input['cb_urgency'] == "Immediate Attention Needed")
		{
			$inputs['date_needed'] = $input['date_needed'];
			$rules['date_needed'] = 'required|date';
			$messages['date_needed.required'] = 'Date needed is required';
			$messages['date_needed.date'] = 'Date needed must be a valid date';
		}
		
		$inputs['cb_urgency'] = $input['cb_urgency'];
		$rules['cb_urgency'] = 'required';
		$messages['cb_urgency.required'] = 'Urgency field is required';
		
		$inputs['cb_purpose_of_request'] = $input['cb_purpose_of_request'];
		$rules['cb_purpose_of_request'] = 'required|max:5000';
		$messages['cb_purpose_of_request.required'] = 'Purpose of request is required';
		$messages['cb_purpose_of_request.max'] = 'Purpose of request length must not be greater 5000 characters';

		if(@$input['requested_documents']['pagibig']['name'] != ""){
			$inputs['pagibig_duration'] = $input['requested_documents']['pagibig']['duration'];
			$rules['pagibig_duration'] = 'required';
			$messages['pagibig_duration.required'] = 'Please select duration for Pag-Ibig';
		}
		
		if(@$input['requested_documents']['philhealth']['name'] != ""){
			$inputs['philhealth_duration'] = $input['requested_documents']['philhealth']['duration'];
			$rules['philhealth_duration'] = 'required';
			$messages['philhealth_duration.required'] = 'Please select duration for Philhealth';
		}
		
		if(@$input['requested_documents']['sss']['name'] != ""){
			$inputs['sss_duration'] = $input['requested_documents']['sss']['duration'];
			$rules['sss_duration'] = 'required';
			$messages['sss_duration.required'] = 'Please select duration for SSS';
		}

		if(@$input['requested_documents']['coe']['name'] != ""){
			$inputs['coe_number_of_copies'] = $input['requested_documents']['coe']['number_of_copies'];
			$rules['coe_number_of_copies'] = 'required|numeric';
			$messages['coe_number_of_copies.required'] = 'Please input number of copies for Certificate of Employment';
			$messages['coe_number_of_copies.numeric'] = 'Number of copies in Certificate of Employment must be numeric';
		}
		
		if(@$input['requested_documents']['coec']['name'] != ""){
			$inputs['coec_number_of_copies'] = $input['requested_documents']['coec']['number_of_copies'];
			$rules['coec_number_of_copies'] = 'required|numeric';
			$messages['coec_number_of_copies.required'] = 'Please input number of copies for Certificate of Employment with Compensation';
			$messages['coec_number_of_copies.numeric'] = 'Number of copies in Certificate of Employment with Compensation must be numeric';
		}		
		
		if(@$input['requested_documents']['emaf']['name'] != ""){
			$emaf_blank_proposed_status_counter = 0;		
			for($a=0; $a<=4; $a++){
				for($b=1; $b<=3; $b++){
					if($b == 2){
						if($input['requested_documents']['emaf']['table'][$a][$b] == ""){
							$emaf_blank_proposed_status_counter++;
						}					
					}
				
					if($input['requested_documents']['emaf']['nature_of_movement'] == "Others"){
						$inputs['emaf_others'] = $input['requested_documents']['emaf']['others'];
						$rules['emaf_others'] = 'required|max:100';
						$messages['emaf_others.required'] = 'EMAF Others field must be filled up';
						$messages['emaf_others.max'] = 'EMAF Others field only allows 100 characters';
					};
					
					if(strlen($input['requested_documents']['emaf']['table'][$a][$b]) > 100){
						$inputs['emaf_fields_string_length'] = strlen($input['requested_documents']['emaf']['table'][$a][$b]);
						$rules['emaf_fields_string_length'] = 'numeric|max:100';
						$messages['emaf_fields_string_length.max'] = "EMAF fields characters must not be more than 100 characters.";							
					}
				}
			}
			$inputs['emaf_blank_proposed_status_counter'] = $emaf_blank_proposed_status_counter;
			$rules['emaf_blank_proposed_status_counter'] = 'numeric|max:0';
			$messages['emaf_blank_proposed_status_counter.numeric'] = 'EMAF proposed status fields must be filled up all.';
			$messages['emaf_blank_proposed_status_counter.max'] = "EMAF proposed status fields must be filled up all.";
			
			$inputs['emaf_effectivity_date'] = $input['requested_documents']['emaf']['effectivity_date'];
			$rules['emaf_effectivity_date'] = 'required|date';
			$messages['emaf_effectivity_date.required'] = 'EMAF effectivity date is required.';
			$messages['emaf_effectivity_date.date'] = "EMAF effectivity date is invalid format";
			
		}

		if(@$input['requested_documents']['emp_attendance']['name'] != ""){
			$inputs['emp_attendance_from'] = $input['requested_documents']['emp_attendance']['from'];
			$rules['emp_attendance_from'] = 'required|date';
			$messages['emp_attendance_from.required'] = 'Please input date(from) for Employee Attendance';
			$messages['emp_attendance_from.date'] = 'Invalid date(from) for Employee Attendance';
			
			$inputs['emp_attendance_to'] = $input['requested_documents']['emp_attendance']['to'];
			$rules['emp_attendance_to'] = 'required|date';
			$messages['emp_attendance_to.required'] = 'Please input date(to) for Employee Attendance';
			$messages['emp_attendance_to.date'] = 'Invalid date(to) for Employee Attendance';
		}
		
		if(@$input['requested_documents']['others']['name'] != ""){
			$inputs['others_value'] = $input['requested_documents']['others']['value'];
			$rules['others_value'] = 'required';
		}
		
		$doc_counter = "";
		
		foreach($input['requested_documents'] as $document)
		{
			if(array_key_exists('name', $document))
			{
				$doc_counter++;
			}
		}
		
		$inputs['requested_documents_count'] = 	$doc_counter;
		$rules['requested_documents_count'] = 'required|numeric';
		$messages['requested_documents_count.required'] = 'Please select at least 1 document';	
		$messages['requested_documents_count.numeric'] = 'Please select at least 1 document';
		
		$rules['requested_documents_count'] = 'required|numeric';
		
		$file_counter = 0;
		$fs = 0;
		if(@count($input['files']) > 1){
			// $inputs['files_count'] = count($input['files']);
			$file_counter = count($input['files']);
			$fs = 0;
			foreach($input['files'] as $a => $b){
				$fs += $b['filesize'];
			}
		}
		$inputs['files_count'] = $file_counter;
		$rules['files_count'] = 'numeric|max:5';
		$messages['files_count.numeric'] = 'Attachment count must be numeric';
		$messages['files_count.max'] = 'Attachment must not succeed more than 5';
		
		$inputs['files_size'] = $fs;
		$rules['files_size'] = 'numeric|max:20971520';
		$messages['files_size.numeric'] = 'Filesize must be numeric';
		$messages['files_size.max'] = 'Attachment filesize must not succeed more than 20MB in total';
		
		// edit until here
		
		// Do not edit beyond this point
		$validator = Validator::make($inputs, $rules, $messages);
		
		//$this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url() )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Some fields are incomplete.');
			exit;
			//$this->setResult(false);
		}
		
		return true;
		
		//$messages = $validator->messages();
		
		//$this->setMessages($messages->all());
		
		//return $this->getMessages();
	}
}
?>