<?php namespace RGAS\Modules\CBR\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class Assign extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		$inputs['emp_id'] = $input['emp_id'];
		$rules['emp_id'] = 'required';
		$messages['emp_id.required'] = 'Please select CBR Staff';
		
		$validator = Validator::make($inputs, $rules, $messages);
		
		// $this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url() )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error in processing');
			exit;
			// $this->setResult(false);
		}
		
		// $messages = $validator->messages();
		
		// $this->setMessages($messages->all());
		
		return true;
	}
}
?>