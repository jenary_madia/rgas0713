<?php namespace RGAS\Modules\CBR\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class SendBack extends RResult
{
	public function validate()
	{
		$input = Input::all();
		
		$inputs['cbrd_remarks'] = $input['cbrd_remarks'];
		$rules['cbrd_remarks'] = 'required';
		$messages['cbrd_remarks.required'] = 'Please input comment';
		
		$validator = Validator::make($inputs, $rules, $messages);

		// $this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to('cbr/review-request/'.Request::segment(3))
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error in processing');
			exit;
			// $this->setResult(false);
		}
		
		// $messages = $validator->messages();
		
		// $this->setMessages($messages->all());
		
		return true;
	}
}
?>