<?php namespace RGAS\Modules\CBR\Requests;

use Input;
use Validator;
use Redirect;
use Request;

class ForAcknowledgement extends RResult
{
	public function validate()
	{
		$input = Input::all();

		$inputs['date_endorsed_to_requestor'] = $input['date_endorsed_to_requestor'];
		$rules['date_endorsed_to_requestor'] = 'required';
		$messages['date_endorsed_to_requestor.required'] = 'Please input date';
		//$messages['date_endorsed_to_requestor.date'] = 'Please input date';
		//$messages['date_endorsed_to_requestor.date_format'] = 'Please input proper date format';
		
		$validator = Validator::make($inputs, $rules, $messages);
		
		// $this->setResult(true);
		if ($validator->fails())
		{
			return Redirect::to( Request::url() )
		           ->withInput()
		           ->withErrors($validator)
		           ->with('message', 'Error in processing');
			exit;
			// $this->setResult(false);
		}
		
		// $messages = $validator->messages();
		
		// $this->setMessages($messages->all());
		
		return true;
	}
}
?>