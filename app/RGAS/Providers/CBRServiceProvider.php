<?php namespace app\RGAS\Providers;

use app\CBR;
use Illuminate\Support\ServiceProvider;

class CBRServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['cbr'] = $this->app->share(function($app)
        {
            return new CBR\CBR;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('CBR', 'app\RGAS\Facades\CBR');
        });
    }
}