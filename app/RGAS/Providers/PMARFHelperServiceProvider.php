<?php namespace RGAS\Providers;

use RGAS\Helpers;
use Illuminate\Support\ServiceProvider;

class PMARFHelperServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['pmarfhelper'] = $this->app->share(function($app)
        {
            return new Helpers\PMARFHelper;
        });

        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('PMARFHelper', 'RGAS\Facades\PMARFHelper');
        });
    }
}