<?php namespace RGAS\Providers;

use RGAS;
use Illuminate\Support\ServiceProvider;

class AuditTrailsServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
		
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['at'] = $this->app->share(function($app)
        {
            return new RGAS\Libraries\AuditTrail;
        });

        // Shortcut so developers don't need to add an Alias in app/config/app.php
        $this->app->booting(function()
        { 
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('AuditTrail', 'RGAS\Facades\AT');
        });
    }
}