<?php namespace RGAS\Libraries;
use Response;
use Input;
use Config;

class FileManager implements IFileManager
{	
	public function upload($path)
	{
		$attachments = Input::file('attachments');
		if($attachments){
			foreach($attachments as $key => $file){				
				if($file){
					$filenames[$key]['random_filename'] = md5(rand().time());
					$filenames[$key]['original_filename'] = $file->getClientOriginalName();
					$filenames[$key]['filesize'] = $file->getSize();
					$filenames[$key]['mime_type'] = $file->getMimeType();
					$filenames[$key]['original_extension'] = $file->getClientOriginalExtension();
					$file->move($path, $filenames[$key]['random_filename']);
				}
			}
			return $filenames;
		}
		return;
	}

	public function uploadFile($path, $attachments)
	{
		if($attachments) {
			foreach($attachments as $key => $file){				
				if($file) {
					$filenames[$key]['random_filename'] = md5(rand().time());
					$filenames[$key]['original_filename'] = $file['original_filename'];
					$filenames[$key]['filesize'] = $file['filesize'];
					$filenames[$key]['mime_type'] = $file['mime_type'];
					$filenames[$key]['original_extension'] = $file['original_extension'];
					$filenames[$key]['existing_random_filename'] = $file['random_filename'];
					$this->copyFile($filenames[$key]['random_filename'], $filenames[$key]['existing_random_filename'], $path);
				}
			}
			return $filenames;
		}
		return;
	}
	
	public function download($filepath, $filename)
	{
		if (file_exists($filepath))
		{
			// Send Download
			return Response::download($filepath, $filename, [
				'Content-Length: '. filesize($filepath)
			]);
		}
		else
		{
			// Error
			// exit('Requested file does not exist on our server!');
			return false;
		}
	}
	
	public function move($random_filename, $new_path, $old_path=null)
	{
		if(!file_exists($new_path))
		{
			mkdir($new_path, 0777, true);
		}

		if(file_exists(Config::get('rgas.rgas_temp_storage_path').$random_filename))
			rename(Config::get('rgas.rgas_temp_storage_path').$random_filename, $new_path.'/'.$random_filename);

		//ADD THIS TO SPECIFY PATCH LOCATION
		if(file_exists( $old_path .'/'. $random_filename ))
			rename( $old_path .'/'. $random_filename , $new_path.'/'.$random_filename);

		return;
	}

	public function copyFile($random_filename, $existing_random_filename, $new_path)
	{
		if(!file_exists($new_path))
		{
			mkdir($new_path, 0777, true);
		}
		if(file_exists(Config::get('rgas.rgas_temp_storage_path').$existing_random_filename))
			copy(Config::get('rgas.rgas_temp_storage_path').$existing_random_filename, $new_path.'/'.$random_filename);

		return;
	}
}
?>