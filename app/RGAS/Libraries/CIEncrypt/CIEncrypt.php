<?php namespace RGAS\Libraries;

class CIEncrypt implements ICIEncrypt
{
	function decode($data)
    {
        $m_string = substr($data, 0, -1);

        $m_string = str_replace(array('p____p', 's____s', 'e____e'), array('+', '/', '='), $m_string);

        for ($m_level = 0; $m_level < substr($data, -1); $m_level++)
        {
            $m_string = base64_decode($m_string);
        }
        return $m_string;
    }

	function encode($string, $depth = null)
    {
        $m_depth = ($depth != null) ? $depth : rand(5, 8);

        for ($m_level = 0; $m_level < $m_depth; $m_level++)
        {
            $string = base64_encode($string);
        }
        $string = str_replace(array('+', '/', '='), array('p____p', 's____s', 'e____e'), $string);

        return str_replace('=', null, sprintf('%s%d', $string, $m_level));
    }
}
?>