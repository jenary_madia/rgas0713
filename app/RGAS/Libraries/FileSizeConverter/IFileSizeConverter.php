<?php namespace RGAS\Libraries;

interface IFileSizeConverter
{
	public function convert_size($bytes);
}
?>