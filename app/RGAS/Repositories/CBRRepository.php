<?php namespace RGAS\Repositories;

use DB;
use Session;
use URL;
use CompensationBenefits;
use CompensationBenefitsRequestedDocs;
use RGAS\Helpers\EmployeesHelper;
use AuditTrail;
use RGAS\Modules\CBR\CBR as CBR;
use Receivers;

class CBRRepository
{
	
    public function getRequestByCbRefNum($cb_ref_num)
    {
        $request = CompensationBenefits::with('cbrd')->where("cb_ref_num","=",$cb_ref_num)->first();
        return $request; 
    }
	
	public function getRequestByCbrdRefNum($cbrd_ref_num)
	{
	
		$request = CompensationBenefitsRequestedDocs::with('cb')->where("cbrd_ref_num","=",$cbrd_ref_num)->first();

		return $request;
	}
	
	public function getCbrReceiver()
	{
		$cbr_receiver = Receivers::where("code","=","CBR_RECEIVER")->first();
					
		return $cbr_receiver;
	}
	
	public function getDashboardFiler()
	{	
		
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				->where('cb_issent','=',1);
        })
		->where('compensationbenefits.cb_employees_id', '=', Session::get("employee_id"))
		->groupBy('compensationbenefitsrequesteddocs.cbrd_ref_num')
		->limit(5)
		->orderBy("cbrd_ref_num","desc")
		->get();
		

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {
				$a = json_decode($req->cbrd_req_docs);
				
                $result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_status;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
				<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
				<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">VIEW</a>
				<a class="btn btndefault btn-default" href="" disabled>EDIT</a></div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getDashboardStaff()
	{	
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				->where('compensationbenefitsrequesteddocs.cbrd_current', '=', Session::get("employee_id"))
				->where('cb_issent','=',1);
        })->groupBy('compensationbenefitsrequesteddocs.cbrd_ref_num')
        ->limit(5)
        ->orderBy("cbrd_ref_num","desc")
        ->get();

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {

				$a = json_decode($req->cbrd_req_docs);

                $result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = $req->cb_status;
				$result_data["aaData"][$ctr][] = 
					'<div class="divtwobtn">
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/process/'.$req->cbrd_ref_num).'">PROCESS</a>
					</div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
    
    public function getRequestsMy()
	{	
		//JMA 0329 ADD cb_status not in ('Acknowledged/Deleted' , 'Returned/Deleted','New/Deleted')
        $requests = DB::select("
    		SELECT '' as cbrd_current , cb_id as cb_id, cb_ref_num as ref_num, cb_date_filed as date_filed, null as document, cb_status as status, cb_issent
            FROM `compensationbenefits` 
            WHERE cb_issent = 0 and cb_status not in ('New/Deleted')
            AND cb_employees_id = ".Session::get("employee_id")."
            
            UNION ALL
            
            SELECT cbrd_current,  cbrd_cb_id as cb_id, cbrd_ref_num as ref_num, cb_date_filed as date_filed, cbrd_req_docs as document, cbrd_status as status, cb_issent
            FROM `compensationbenefitsrequesteddocs` cbrd 
            LEFT JOIN `compensationbenefits` cb ON cbrd.cbrd_cb_id = cb.cb_id
            WHERE cb.cb_issent = 1 and cbrd_status not in ('Acknowledged/Deleted' , 'Returned/Deleted','New/Deleted')
            AND cb.cb_employees_id = ".Session::get("employee_id")."
        ");
        
		
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
		
		if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {
             	$a = json_decode($req->document);
             	
                $result_data["aaData"][$ctr][] = $req->ref_num;
				$result_data["aaData"][$ctr][] = $req->date_filed;
				$result_data["aaData"][$ctr][] = @$a->{key($a)}->name;
				$result_data["aaData"][$ctr][] =  $req->cbrd_current ? EmployeesHelper::getEmployeeNameById($req->cbrd_current) : Session::get("employee_name")  ;
				$result_data["aaData"][$ctr][] = $req->status;
				if($req->cb_issent == 0 && $req->status == "New"){
					$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
						<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->ref_num.'" href="'.URL::to('/cbr/delete/'.$req->status.'/'.$req->ref_num).'">DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/draft/view/'.$req->ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/draft/edit/'.$req->ref_num).'">EDIT</a>
						</div>';
				}
				elseif($req->cb_issent == 1 && $req->status == "Returned"){
					$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
						<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/returned/view/'.$req->ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/returned/edit/'.$req->ref_num).'">EDIT</a></div>';
				}
				elseif($req->cb_issent == 1 && $req->status == "For Acknowledgement"){
					$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
						<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>EDIT</a></div>';
				}
				elseif($req->cb_issent == 1 && $req->status == "For Processing"){
					$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
						<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>EDIT</a></div>';
				}
				elseif($req->cb_issent == 1 && $req->status == "Acknowledged"){
					$result_data["aaData"][$ctr][] = '<div class="divMyRequests_">
						<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->ref_num.'" href="'.URL::to('/cbr/delete/'.$req->status.'/'.$req->ref_num).'">DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->ref_num).'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>EDIT</a></div>';
				}
				else{
					$result_data["aaData"][$ctr][] = '';
				}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getRequestsNew()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                
				->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				->where('cb_issent','=',1)
				->whereNull('cbrd_assigned_to');
				// ->where('cbrd_assigned_to','=','');
				// ->whereNull('cbrd_assigned_to','=','');
        })
		->groupBy('cbrd_cb_id')
		->get();
		
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->cb_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = '<div class="divtwobtn"><a class="btn btndefault btn-default" href="'.URL::to('/cbr/review-request/'.$req->cb_ref_num).'/view">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/cbr/review-request/'.$req->cb_ref_num).'">PROCESS</a></div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getRequest($cb_ref_num)
	{
		$request = CompensationBenefits::with(
			array('cbrd' => function($q){
				$q->where("cbrd_status","=","For Processing");
				$q->whereNull('cbrd_assigned_to');
			})
		)->where("cb_ref_num","=",$cb_ref_num)->get();

		return $request;
	}
	
	public function getRequestsInProcess()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id');
            	//JMA REMOVE THIS CONDITION.. OR STATEMENT BREAK THE FLOW
     //             ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				 // ->orWhere('compensationbenefitsrequesteddocs.cbrd_status','=','For Acknowledgement');             				 
        })
        ->whereNotNull('compensationbenefitsrequesteddocs.cbrd_assigned_to')
        ->Where('compensationbenefitsrequesteddocs.cbrd_assigned_to','!=','')
        //JMA ADD THIS CONDITION HERE
        ->whereIn("compensationbenefitsrequesteddocs.cbrd_status", ['For Processing','For Acknowledgement'] )
        ->groupBy('compensationbenefitsrequesteddocs.cbrd_ref_num')->get();
	
		
		$result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {	

             	$a = json_decode($req->cbrd_req_docs);
             	
                $result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = $a->{key($a)}->name;
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_assigned_to);
				if($req->cbrd_assigned_date != '0000-00-00'){
					$result_data["aaData"][$ctr][] = $req->cbrd_assigned_date;
				}
				else{
					$result_data["aaData"][$ctr][] = '';
				}
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_current);
				$result_data["aaData"][$ctr][] = $req->cbrd_status;
				if($req->cbrd_assigned_to == Session::get('employee_id') && $req->cbrd_status == "For Processing"){
				$result_data["aaData"][$ctr][] = '<div class="divForprocessing"><a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/cbr/process/'.$req->cbrd_ref_num).'">PROCESS</a></div>';
				}
				else{
					$result_data["aaData"][$ctr][] = '<a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">VIEW</a>';
				}
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		
		return $result_data;
	}
	
	public function getRequestsAcknowledged()
	{
		$requests = DB::table('compensationbenefits')
        ->join('compensationbenefitsrequesteddocs', function($join)
        {
            $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id');
        })
        ->whereIn('compensationbenefitsrequesteddocs.cbrd_status', ['Acknowledged','Acknowledged/Deleted'] )
        ->get();
		
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
				$result_data["aaData"][$ctr][] = $req->cbrd_ref_num;
				$result_data["aaData"][$ctr][] = $req->cb_emp_name;
				$result_data["aaData"][$ctr][] = $req->cb_date_filed;
				$result_data["aaData"][$ctr][] = json_decode($req->cbrd_req_docs)->{key(json_decode($req->cbrd_req_docs))}->name;
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->cbrd_assigned_to);
				$result_data["aaData"][$ctr][] = $req->cbrd_assigned_date;
				$result_data["aaData"][$ctr][] = $req->cbrd_acknowledged_date;
				$result_data["aaData"][$ctr][] = '<div class="divForprocessing"><a class="btn btndefault btn-default" href="'.URL::to('/cbr/view/'.$req->cbrd_ref_num).'">VIEW</a></div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }

		return $result_data;
	}
	
	
}