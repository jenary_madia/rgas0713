<?php namespace RGAS\Repositories;

use DB;
use Session;
use URL;
use PromoAndMerchandising;
use Employees;
use RGAS\Helpers\EmployeesHelper;
use Receivers;

class PMARFRepository
{
	public function getRequest($pmarf_id)
	{
		$request = PromoAndMerchandising::find($pmarf_id);
		
		return $request;
	}
	
	public function getMyRequests()
	{
        $requests = PromoAndMerchandising::where("pmarf_emp_id","=",Session::get("employee_id"))
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req){				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				
				if($req->pmarf_current > 0){
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_current);
				}
				else{
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				}
					if($req->pmarf_status == "New"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="'.URL::to('/pmarf/delete/'.$req['pmarf_id']).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/edit/'.$req['pmarf_id']).'">EDIT</a>							
							</div>';
					}
					elseif($req->pmarf_status == "For Conforme"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/confirmation/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
					elseif(($req->pmarf_status == "Disapproved" || $req->pmarf_status == "Returned") && ($req->pmarf_current != $req->pmarf_emp_id )){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="" disabled>DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>							
							</div>';
					}
					elseif(($req->pmarf_status == "Disapproved" || $req->pmarf_status == "Returned") && ($req->pmarf_current == $req->pmarf_emp_id )){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="'.URL::to('/pmarf/delete/'.$req['pmarf_id']).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/edit/'.$req['pmarf_id']).'">EDIT</a>							
							</div>';
					}
					elseif($req->pmarf_status == "For Approval" || $req->pmarf_status == "For Assessment" || $req->pmarf_status == "For Execution"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
					elseif($req->pmarf_status == "Closed" || $req->pmarf_status == "Cancelled"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="'.URL::to('/pmarf/delete/'.$req['pmarf_id']).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
					$ctr++;
             }
        }
		
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}	
	
	public function getIsRequests()
	{
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
				$supervisor_info = json_decode($req['pmarf_supervisor_info']);
				if($supervisor_info->id == Session::get('employee_id')){				
					$result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
					$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);

					if($req->pmarf_status == "For Approval"){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req['pmarf_id'])	.'">APPROVE</a>
						</div>';
					}
					elseif($req->pmarf_status == "Returned"){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req['pmarf_id'])	.'">APPROVE</a>
						</div>';
					}

				}
				else{
					continue;
				}
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getClosedRequests()
	{
        $requests = PromoAndMerchandising::where('pmarf_status','=','Closed')
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
				$supervisor_info = json_decode($req['pmarf_supervisor_info']);
				if($supervisor_info->id == Session::get('employee_id')){				
					$result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
					$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);				
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>APPROVE</a>
					</div>';					
				}
				else{
					continue;
				}
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getClosedRequestsDh()
	{
        $requests = PromoAndMerchandising::where('pmarf_status','=','Closed')
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
				$supervisor_info = json_decode($req['pmarf_depthead_info']);
				if($supervisor_info->id == Session::get('employee_id')){				
					$result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
					$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);				
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>APPROVE</a>
					</div>';					
				}
				else{
					continue;
				}
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getDhRequests()
	{
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
		->where('pmarf_isdeleted','=',0)
		->whereIn('pmarf_status',['For Approval',"Returned"])
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {
				$depthead_info = json_decode($req['pmarf_depthead_info']);
				if($depthead_info->id == Session::get('employee_id')){			 
					$result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
					$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
					$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
					$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);

					if($req->pmarf_status == "For Approval"){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/dept-head-approve/'.$req['pmarf_id']).'">APPROVE</a>
						</div>';
					}
					elseif($req->pmarf_status == "Returned"){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/dept-head-approve/'.$req['pmarf_id'])	.'">APPROVE</a>
						</div>';
					}
					elseif($req->pmarf_status == "For Conforme"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/confirmation/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
					elseif(($req->pmarf_status == "Closed" || $req->pmarf_status == "Cancelled") && ($req->pmarf_emp_id == Session::get('employee_id'))){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="'.URL::to('/pmarf/delete/'.$req['pmarf_id']).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}

				}
				$ctr++;
			}
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getNpmdHeadRequests()
	{
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
		->where('pmarf_isdeleted','=',0)
		->where("pmarf_status","!=","For Approval")
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = $req->pmarf_department;
				if($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-HEAD"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-approve/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-HEAD-ACCEPT"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-acceptance/'.$req['pmarf_id']).'">PROCESS</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Execution" && $req->pmarf_level == "NPMD-HEAD-NOTATION"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-notation/'.$req['pmarf_id']).'">PROCESS</a>
					</div>';
				}

				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}

	public function getNpmdPersonnelRequests_limit()
	{
		$cur = Session::get('employee_id');
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
        ->whereRaw("((pmarf_emp_id != $cur and pmarf_status = 'Returned') or  pmarf_status != 'Returned')")
		->where('pmarf_isdeleted','=',0)
		->whereIn('pmarf_status' , ["For Assessment","For Execution",'For Approval',"Returned"])
		->limit(5)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = $req->pmarf_department;
				if($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-PERSONNEL-ASSIGN"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-process/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}
				if($req->pmarf_status == "For Execution" && $req->pmarf_level == "NPMD-PERSONNEL-EXECUTE"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-execution/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}

				if($req->pmarf_status == "Returned" && $req->pmarf_level == "NPMD-RETURNED"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-process/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}


				if($req->pmarf_status == "Returned" && $req->pmarf_level == "NPMD-PERSONNEL-RETURNED"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-execution/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}

				// if($req->pmarf_status == "Returned"){
				// 	$result_data["aaData"][$ctr][] = 
				// 	'<div class=" divMyRequests_">
				// 		<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
				// 		<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-process/'.$req['pmarf_id'])	.'">PROCESS</a>
				// 	</div>';
				// }

				if($req->pmarf_status == "For Approval" || $req->pmarf_status == "Returned" )
				{
					if(Session::get('desig_level') == 'supervisor'){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req['pmarf_id']).'">APPROVE</a>
						</div>';
					}
					elseif(Session::get('desig_level') == 'head'){
						$result_data["aaData"][$ctr][] = 
						'<div class=" divMyRequests_">
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/dept-head-approve/'.$req['pmarf_id']).'">APPROVE</a>
						</div>';
					}
				}
			
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}


	public function getNpmdHeadRequests_limit()
	{
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
        ->whereIn('pmarf_status' , ["For Assessment","For Execution","For Approval"])
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->limit(5)
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = $req->pmarf_department;
				if($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-HEAD"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-approve/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-HEAD-ACCEPT"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-acceptance/'.$req['pmarf_id']).'">PROCESS</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Execution" && $req->pmarf_level == "NPMD-HEAD-NOTATION"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-head-notation/'.$req['pmarf_id']).'">PROCESS</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Approval" )
				{
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/dept-head-approve/'.$req['pmarf_id']).'">APPROVE</a>
					</div>';
				}

				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	// All Pmarf Requests
	public function getAllPmarfRequests()
	{
        $requests = PromoAndMerchandising::where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = $req->pmarf_department;
				$result_data["aaData"][$ctr][] = 
				'<div class=" divMyRequests_">
					<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
					<a class="btn btndefault btn-default" href="" disabled>PROCESS</a>
				</div>';
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getNpmdPersonnelRequests()
	{
		$cur = Session::get('employee_id');
        $requests = PromoAndMerchandising::where("pmarf_current","=",Session::get('employee_id'))
        ->whereRaw("((pmarf_emp_id != $cur and pmarf_status = 'Returned') or  pmarf_status != 'Returned')")
		->whereIn('pmarf_status' , ["For Assessment","For Execution" ,"Returned"])
		->where('pmarf_isdeleted','=',0)
		->orderBy('pmarf_ref_num','desc')
		->get();	
		
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = $req->pmarf_department;
				if($req->pmarf_status == "For Assessment" && $req->pmarf_level == "NPMD-PERSONNEL-ASSIGN"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-process/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}
				if($req->pmarf_status == "For Execution" && $req->pmarf_level == "NPMD-PERSONNEL-EXECUTE"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-execution/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}

				if($req->pmarf_status == "Returned" && $req->pmarf_level == "NPMD-PERSONNEL-RETURNED"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-execution/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}

				if($req->pmarf_status == "Returned"){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id'])	.'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/npmd-personnel-process/'.$req['pmarf_id'])	.'">PROCESS</a>
					</div>';
				}
			
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}

	
	public function get_supervisor($nEmpId)
	{
        $oEmp  = Employees::get($nEmpId);
        $oSup  = Employees::get($oEmp->superiorid);
        
        if(count($oSup)>0)
        {
           return array("id"=>$oSup->id,
                        "firstname"=>$oSup->firstname,
                        "middlename"=>$oSup->middlename,
                        "lastname"=>$oSup->lastname,
                        "designation"=>$oSup->designation);   
        }else{
           return false; 
        }
    }

	//NPMD-HEAD
	public function getNpmdHead()
	{
		$npmdHead = Employees::where("desig_level","=","head")->where("departmentid2","=","PMMD")->orWhere("departmentid2","=","NPMD")
					->first(["id","employeeid", "firstname", "middlename", "lastname"]);
					
		return $npmdHead;
	}
	
	//NPMD Personnel 
	public function getNpmdPersonnel()
	{
		$npmdPersonnel = Employees::where("desig_level","=","supervisor")->where("departmentid2","=","PMMD")
		->orWhere("departmentid2","=","NPMD")
		->get();
					
		return $npmdPersonnel;
	}
	
	public function getSuperior($pmarf_id)
	{
		$request = PromoAndMerchandising::find($pmarf_id);
		
		return $request;
		
	}
	
	public function get_department_head($nEmpId)
	{
		$oEmp       = Employees::get($nEmpId);
		$oDepthead  = Employees::get_department_head($oEmp->departmentid);

		if(count($oDepthead)>0)
		{
		   return array(
				"id"=>$oDepthead->id,
				"firstname"=>$oDepthead->firstname,
				"middlename"=>$oDepthead->middlename,
				"lastname"=>$oDepthead->lastname,
				"designation"=>$oDepthead->designation
			);   
		}
		else{
		   return false; 
		}
    }
	
	public function getRequestByRefNum($ref_num)
	{
		$request = PromoAndMerchandising::where('pmarf_ref_num','=',$ref_num)->first();
		return $request;
	}
	
	public function getPmarfFilerDashboard()
	{
		$cur = Session::get('employee_id');
		$requests = PromoAndMerchandising::where('pmarf_emp_id','=',Session::get('employee_id'))
			->whereRaw("(pmarf_status = 'For Approval' || pmarf_status = 'For Conforme' || (pmarf_current = $cur && pmarf_status in ('Returned','Disapproved')))")
			->where('pmarf_isdeleted','=',0)
			->limit(5)
			->orderBy("pmarf_ref_num","desc")
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_current);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				if($req->pmarf_status == "For Approval"){
					$result_data["aaData"][$ctr][] =
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="" disabled>EDIT</a>

					</div>';
				}
				elseif($req->pmarf_status == "Returned" || $req->pmarf_status == "Disapproved"){
					$result_data["aaData"][$ctr][] =
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default remove-request" data-request-ref-num="'.$req->pmarf_ref_num.'" href="'.URL::to('/pmarf/delete/'.$req['pmarf_id']).'">DELETE</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/edit/'.$req['pmarf_id']).'">EDIT</a>
					</div>';
				}
				elseif($req->pmarf_status == "For Conforme"){
						$result_data["aaData"][$ctr][] = 
							'<div class=" divMyRequests_">
								<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/confirmation/'.$req['pmarf_id']).'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
				
				$ctr++;
				
             }
        }
		else {
			$result_data["aaData"] = $requests;
		}
		return $result_data;
	}
	
	public function getPmarfApproverDashboard()
	{
		$requests = PromoAndMerchandising::where('pmarf_current','=',Session::get('employee_id'))
			 ->where('pmarf_emp_id','!=',Session::get('employee_id'))
			->whereIn('pmarf_status',['For Approval',"Returned"])
			->where('pmarf_isdeleted','=',0)
			->orderBy("pmarf_ref_num","desc")
			->limit(5)
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->pmarf_ref_num;
				$result_data["aaData"][$ctr][] = $req->pmarf_date_filed;
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->pmarf_emp_id);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_activity_type);
				$result_data["aaData"][$ctr][] = strtoupper($req->pmarf_status);
				if(Session::get('desig_level') == 'supervisor'){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/approve/'.$req['pmarf_id']).'">APPROVE</a>
					</div>';
				}
				elseif(Session::get('desig_level') == 'head'){
					$result_data["aaData"][$ctr][] = 
					'<div class=" divMyRequests_">
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/view/'.$req['pmarf_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/pmarf/dept-head-approve/'.$req['pmarf_id']).'">APPROVE</a>
					</div>';
				}
				
				$ctr++;	
             }
        }
		else {
			$result_data["aaData"] = $requests;
		}
		return $result_data;
	}
}