<?php namespace RGAS\Repositories;

use DB;
use Session;
use URL;
use TrainingEndorsements;
use Employees;
use Receivers;
use RGAS\Helpers\EmployeesHelper;

class TERepository
{
	
	public function getRequest($te_id)
	{
		$request = TrainingEndorsements::find($te_id);
		
		return $request;
	}
	
	public function getEndorsementsMy()
	{
		$requests = TrainingEndorsements::where("te_employees_id","=",Session::get("employee_id"))
			->where("te_isdeleted","=",0)
			->orderBy('te_ref_num','desc')
			->get();

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = '<div >' .$req->te_emp_name .'</div>';
				$result_data["aaData"][$ctr][] = $req->te_training_title;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = '<div >' .strtoupper($req->te_status) .'</div>';
				$result_data["aaData"][$ctr][] = '<div >'. EmployeesHelper::getEmployeeNameById($req->te_current) .'</div>';
				
					if(!$req->te_issent && $req->te_status == "NEW"){
						$result_data["aaData"][$ctr][] = 
							'<div class="divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-training-title="'.$req->te_training_title.'" href="'.URL::to('/te/delete/'.$req->te_id).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/te/edit/'.$req['te_id'])	.'">EDIT</a>							
							</div>';
					}
					elseif(strtoupper($req->te_status) == "FOR ASSESSMENT"){
						$result_data["aaData"][$ctr][] = 
						'<div class="divMyRequests_">
							<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
						</div>';
					}
					elseif($req->te_issent && strtoupper($req->te_status) == "FOR APPROVAL"){
						$result_data["aaData"][$ctr][] = 
						'<div class="divMyRequests_">
							<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
						</div>';
					}
					elseif(strtoupper($req->te_status) == "APPROVED"){
						$result_data["aaData"][$ctr][] =
							'<div class="divMyRequests_">
								<a class="btn btndefault btn-default remove-request" data-request-training-title="'.$req->te_training_title.'" href="'.URL::to('/te/delete/'.$req->te_id).'">DELETE</a>
								<a class="btn btndefault btn-default" href="'.URL::to('/te/view-approved/'.$req['te_id'])	.'">VIEW</a>
								<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
							</div>';
					}
					elseif(!$req->te_issent && strtoupper($req->te_status) == "DISAPPROVED" && $req->te_level == "CHRD-TOD"){
						$result_data["aaData"][$ctr][] = 
						'<div class="divMyRequests_">
							<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
						</div>';
					}
					elseif(!$req->te_issent && strtoupper($req->te_status) == "DISAPPROVED" && $req->te_employees_id == Session::get('employee_id')){
						$result_data["aaData"][$ctr][] = 
						'<div class="divMyRequests_">
							<a class="btn btndefault btn-default remove-request" data-request-training-title="'.$req->te_training_title.'" href="'.URL::to('/te/delete/'.$req->te_id).'">DELETE</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/edit/'.$req['te_id']).'">EDIT</a>
						</div>';
					}
					else{
						$result_data["aaData"][$ctr][] = 
						'<div class="divMyRequests_">
							<a class="btn btndefault btn-default remove-request" data-request-training-title="'.$req->te_training_title.'" href="'.URL::to('/te/delete/'.$req->te_id).'">DELETE</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/edit/'.$req['te_id']).'">update</a>
						</div>';
					}
					
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsForAssessment()
	{
		$requests = TrainingEndorsements::whereRaw("(te_status = 'APPROVED/DELETED' or te_status = 'APPROVED' or te_level in ('CHRD-TOD','VPforCHR','SVPforCS','PRES') )")
			//->where("te_isdeleted","=",0)
			->orderBy('te_ref_num','desc')
			->get();	
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query xxx
		$result_data["iTotalRecords"] = count($requests);

		$int_stats["FOR ASSESSMENT"] = 1;
		$int_stats["FOR APPROVAL"] = 2;
		$int_stats["APPROVED"] = 3;
		$int_stats["DISAPPROVED"] = 4;
        
        if ( count($requests) > 0)
        {
            $ctr = 0;
             foreach($requests as $req) 
             {
             	$real_stats = explode("/",$req->te_status);
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = isset($int_stats[ $real_stats[0] ]) ? $int_stats[ $real_stats[0] ] : 5;
				$result_data["aaData"][$ctr][] = strtoupper( $real_stats[0] );
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				
				if(strtoupper($real_stats[0]) == "FOR ASSESSMENT")
				{
					$result_data["aaData"][$ctr][] =
						'<div class="divAssess_">
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/assess/'.$req['te_id']).'">PROCESS</a>
						</div>';
				}
				else if(strtoupper( $real_stats[0] ) == "DISAPPROVED")
				{
					$result_data["aaData"][$ctr][] =
						'<div class="divAssess_">
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">VIEW</a>
							<a class="btn btndefault btn-default" href="'.URL::to('/te/assess/'.$req['te_id']).'">EDIT</a>
						</div>';
				}
				else
				{
					$result_data["aaData"][$ctr][] =
						'<div class="divAssess_">
							<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id']).'">VIEW</a>
						</div>';
				}

                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsChrdVpApproval()
	{
		$requests = TrainingEndorsements::where('te_level','=','VPforCHR')
			->where('te_current','=',Session::get('employee_id'))
			->where('te_status','=','FOR APPROVAL')
			->where("te_isdeleted","=",0)
			->orderBy('te_ref_num','desc')
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->te_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] =
					'<div class="divAssess_">
						<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">APPROVE</a>
					</div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsSvpCsApproval()
	{
		$requests = TrainingEndorsements::where('te_level','=','SVPforCS')
			->where('te_status','=','FOR APPROVAL')
			->where('te_current','=',Session::get('employee_id'))
			->where("te_isdeleted","=",0)
			->orderBy('te_ref_num','desc')
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->te_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] =
					'<div class="divAssess_">
						<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">APPROVE</a>
					</div>';
				$ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getEndorsementsPresApproval()
	{
		$requests = TrainingEndorsements::where('te_level','=','PRES')
			->where('te_status','=','FOR APPROVAL')
			->where('te_current','=',Session::get('employee_id'))
			->where("te_isdeleted","=",0)
			->orderBy('te_ref_num','desc')
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_company;
				$result_data["aaData"][$ctr][] = $req->te_department;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->te_status);
				$result_data["aaData"][$ctr][] = EmployeesHelper::getEmployeeNameById($req->te_current);
				$result_data["aaData"][$ctr][] =
					'<div class="divAssess_">
						<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">APPROVE</a>
					</div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getVPforCHR()
	{
		// $vpforchr = Employees::where("designation","=","VP FOR CORP. HUMAN RESOURCE")
		// 			->first(["id","firstname", "middlename", "lastname"]);	

		$vpforchr = Employees::join("receivers","receivers.employeeid","=","employees.id")
					->where("code","TE_VP")
					->first(["employees.id","employees.employeeid", "firstname", "middlename", "lastname"]);

		return $vpforchr;				
	}
	
	public function getSVPforCS()
	{
		// $svpforcs = Employees::where("designation","=","SVP FOR CORP. SERVICES")
		// 			->first(["id","employeeid", "firstname", "middlename", "lastname"]);
					
		$svpforcs = Employees::join("receivers","receivers.employeeid","=","employees.id")
					->where("code","TE_SVP")
					->first(["employees.id","employees.employeeid", "firstname", "middlename", "lastname"]);

		return $svpforcs;
	}
	
	public function getPres()
	{
		// take note of this
		// $request = $this->find($te_id);
				
		// return $request;
		$pres = Employees::where("designation","=","PRESIDENT")
					->where("Active","!=",0)
					->first(["id","employeeid", "firstname", "middlename", "lastname"]);
					
		return $pres;
	}
	
	public function getChrd()
	{
		$chrdRec = Receivers::where("code", "=", "TE_CHRD")->where("module", "=", "te")->first(["employeeid"]);
		$emp = $chrdRec->employeeid;
	
		$chrd = Employees::where("id","=",$emp)->first(["id","employeeid","firstname","middlename","lastname"]);

		return $chrd;
	}
	
	public function getParticipants($te_id)
	{
		$this->te_id = $te_id;
		// $request = TrainingEndorsements::find($te_id);
		$tepRecord = DB::table('trainingendorsements')
        ->join('trainingendorsementsparticipants', function($join)
        {
            $join->on('trainingendorsements.te_id', '=', 'trainingendorsementsparticipants.tep_te_id')
				 ->where('trainingendorsementsparticipants.tep_te_id',"=",$this->te_id);
        })->get();
        
		$queries = DB::getQueryLog();
		$last_query = end($queries);
		
		return $tepRecord;
	}
	
	public function getFilerDashboard()
	{
		$requests = TrainingEndorsements::where("te_employees_id","=",Session::get("employee_id"))
			->wherein("te_status",["FOR APPROVAL","DISAPPROVED","FOR ASSESSMENT"])
			->where("te_isdeleted","=",0)
			->limit(5)
			->get();

        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) 
             {				
             	$disabled = $req->te_current == Session::get("employee_id") ? "" : "disabled";
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = '<div >' .strtoupper($req->te_status) .'</div>';
				$result_data["aaData"][$ctr][] = '<div >'. EmployeesHelper::getEmployeeNameById($req->te_current) .'</div>';
				$result_data["aaData"][$ctr][] = $req->te_status == "FOR APPROVAL" ? 
				'<div class="divMyRequests_">
					<a class="btn btndefault btn-default" href="" disabled>DELETE</a>
					<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
					<a class="btn btndefault btn-default" href="" disabled>EDIT</a>
				</div>':
				'<div class="divMyRequests_">
					<a class="btn btndefault btn-default" ' . $disabled . ' href="'.URL::to('/te/delete/'.$req['te_id'])	.'">DELETE</a>
					<a class="btn btndefault btn-default" href="'.URL::to('/te/view/'.$req['te_id'])	.'">VIEW</a>
					<a class="btn btndefault btn-default" ' . $disabled . ' href="'.URL::to('/te/edit/'.$req['te_id'])	.'">EDIT</a>
				</div>';;
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getApproverDashboard()
	{
		$requests = TrainingEndorsements::where('te_current','=',Session::get('employee_id'))
			->where('te_status','=','FOR APPROVAL')
			->where("te_isdeleted","=",0)
			->limit(5)
			->get();		
        
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
		$result_data["iTotalRecords"] = count($requests);
        
        if ( count($requests) > 0){
            $ctr = 0;
             foreach($requests as $req) {				
                $result_data["aaData"][$ctr][] = $req->te_ref_num;
				$result_data["aaData"][$ctr][] = $req->te_emp_name;
				$result_data["aaData"][$ctr][] = $req->te_date_filed;
				$result_data["aaData"][$ctr][] = strtoupper($req->te_status);
				$result_data["aaData"][$ctr][] =
					'<div class="divAssess_">
						<a class="btn btndefault btn-default" href="'.URL::to('/te/view-for-approval/'.$req['te_id']).'">VIEW</a>
						<a class="btn btndefault btn-default" href="'.URL::to('/te/process-for-approval/'.$req['te_id']).'">APPROVE</a>
					</div>';
                $ctr++;
             }
        }
        else {
            $result_data["aaData"] = $requests;
        }
		return $result_data;
	}
	
	public function getRequestByRefNum($ref_num)
	{
		$request = TrainingEndorsements::where('te_ref_num','=',$ref_num)->first();
		return $request;
	}
	
	
}
