<?php namespace RGAS\Facades;

use Illuminate\Support\Facades\Facade;

class FileSizeConverter extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'filesizeconverter'; }

}