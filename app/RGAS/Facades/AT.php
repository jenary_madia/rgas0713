<?php namespace RGAS\Facades;

use Illuminate\Support\Facades\Facade;

class AT extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'at'; }

}