<?php namespace RGAS\Facades;

use Illuminate\Support\Facades\Facade;

class EmployeesHelper extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'employeeshelper'; }

}