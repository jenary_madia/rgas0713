<?php namespace app\RGAS\Facades;

use Illuminate\Support\Facades\Facade;

class RGASHelpers extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'rgashelpers'; }

}