@extends('template/header')

@section('content')
        <form class="form-inline" action="{{ Request::url() }}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ $request->cb->cb_emp_name }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $request->cb->cb_ref_num }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="hidden" class="form-control" name="cb_employees_id" value="{{ $request->cb->cb_employees_id }}" />
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ $request->cb->cb_emp_id }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->cb->cb_date_filed }}" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_company }}" name="comp_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="{{ $request->cbrd_status }}" name="cb_status" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_department }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ $request->cb->cb_contact_no }}" name="cb_contact_number" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_section }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">URGENCY:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="cb_urgency" id="urgency">
									<option @if($request->cb->cb_urgency == 'Normal Priority') selected="selected" @endif value="Normal Priority">Normal Priority</option>
									<option @if($request->cb->cb_urgency == 'Immediate Attention Needed') selected="selected" @endif value="Immediate Attention Needed">Immediate Attention Needed</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
								@if($request['cb']['cb_urgency'] == 'Immediate Attention Needed')
									<label class="labels required" id="date-needed-color">DATE NEEDED:</label>
								@else
									<label class="labels" id="date-needed-color">DATE NEEDED:</label>
								@endif
                            </div>
							<div class="col2_form_container input-group">
                                <input type="text" class="form-control date_picker" value="@if($request->cb_date_needed != '0000-00-00'){{ $request->cb_date_needed }} @endif"  name="date_needed" id="date_needed"/>
								<label class="input-group-addon btn" for="date_needed">
								   <span class="glyphicon glyphicon-calendar"></span>
								</label>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label required">REQUESTED DOCUMENTS:</label>
					<input type="hidden" value="{{ $request->cbrd_id }}" name="cbrd_id" />
                    
                    <table border = "1" cellpadding = "0" class="tbl3">
						<th class="th_style td_height">Document</th>
						<th class="th_style td_height">Reference No.</th>
						<!--<th class="th_style td_height">Assigned To</th>-->
						<th class="th_style td_height">CBR Remarks</th>
						<tr>
							<td class="td_style td_height">
							    <input type="hidden" name="requested_documents[{{ key($request->cbrd_req_docs)}}][name]" value="{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}" />
							     @if(key($request->cbrd_req_docs) == 'emaf')
							        <a href="#" data-toggle="modal" data-target="#emafModal">{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}</a>
							        <!-- <a href="javascript:void(0);" id="btnEmafDialog">{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}</a> -->
							         <!-- Modal -->
        							<div id="emafModal" class="modal fade" role="dialog" data-rel="dialog">
        								<div class="modal-dialog">
        									<!-- Modal content-->
        									<div class="modal-content">
        								  		<div class="modal-header">
        											Employment Movement Action Form (EMAF) <button type="button" class="close" data-dismiss="modal">&times;</button>
        								  		</div>
        								  		<div class="modal-body">
        											<div id="divEmaf" style="display: block">
        	                            				<div class="extra_labels">
        	                                				<label class="labels">Nature of Movement:</label>
        	                            				</div>
        	                            			<div class="col2_form_container">
        				                                <select class="form-control emaf_form_select" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" id="nom">
        				                                    <option value="Transfer" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
        				                                    <option value="Promotion" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
        				                                    <option value="Temporary Assignment" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
        				                                    <option value="Change of Position Title" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
        				                                    <option value="Salary Adjustment" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
        				                                    <option value="Others" id="emaf_others" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
        				                                </select>
        	                            			</div>
        	                            			<div class="clear_10"></div>
        											@if($request->cbrd_req_docs->emaf->nature_of_movement == "Others")
        											<div id="div_nom_others">
            											<div class="extra_labels">
            												<label class="labels">Others:</label>
            											</div>
            			                                <div class="col2_form_container">
            			                                    <input type="text" class="form-control" name="requested_documents[emaf][others]" value="{{ $request->cbrd_req_docs->emaf->others }}">  
            			                                </div>
            			                                <div class="clear_10"></div>
            			                             </div>
        											@endif
        				                            <div class="extra_labels">
        				                                <label class="labels">Effectivity Date:</label>
        				                            </div>
        				                            <div class="col2_form_container input-group">
        				                                <input type="text" id="effdate" class="form-control date_picker emaf_form" id="effdate" name="requested_documents[emaf][effectivity_date]" value="{{ $request->cbrd_req_docs->emaf->effectivity_date }}" />
        				                            	<label class="input-group-addon btn" for="effdate">
        												   <span class="glyphicon glyphicon-calendar"></span>
        												</label>
        							                </div>
        				                            <div class="clear_10"></div>
        	                            			<div class="tbl-responsive">
        	                                			<table border = "1" cellpadding = "0" class="">
        				                                    <th class="th_style">DETAILS</th>
        				                                    <th class="th_style">PRESENT STATUS</th>
        				                                    <th class="th_style">PROPOSED STATUS</th>
        				                                    <th class="th_style">REMARKS</th>
        	
        				                                    <tr>
        				                                        <td class="td_style">Company</td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][0][1]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{1} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][0][2]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{2} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][0][3]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{3} }}" /></td>
        				                                    </tr>
        				                                    <tr>
        				                                        <td class="td_style">Department</td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][1][1]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{1} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][1][2]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{2} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][1][3]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{3} }}" /></td>
        				                                    </tr>
        				                                    <tr>
        				                                        <td class="td_style">Designation</td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][2][1]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{1} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][2][2]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{2} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][2][3]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{3} }}" /></td>
        				                                    </tr>
        				                                    <tr>
        				                                        <td class="td_style">Job Level</td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][3][1]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{1} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][3][2]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{2} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][3][3]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{3} }}" /></td>
        				                                    </tr>
        				                                    <tr>
        				                                        <td class="td_style">Salary Rate</td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][4][1]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{1} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][4][2]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{2} }}" /></td>
        				                                        <td><input type="text" class="form-control emaf_form" id="" name="requested_documents[emaf][table][4][3]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{3} }}" /></td>
        				                                    </tr>
        	                                			</table>
        	                            			</div>
        	                        			</div>
        								  	</div>
        									<div class="modal-footer">
        										<button type="button" class="btn btn-default" id="emaf_dialog_ok_action" data-dismiss="modal">OK</button>
        									</div>
        								</div>
        							  </div>
        							</div>
							     @else
							        {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}
							     @endif
							     
                                @if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->duration)
        						<br/>Duration: 
        						<!--{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->duration }}-->
        						@endif
        						
        						@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->number_of_copies)
        						<br/>No. of Copies: 
        						<!--{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->number_of_copies }}-->
        						@endif
        						
        						@if(key($request->cbrd_req_docs) == 'pagibig')
                                <select class="form-control" style="" name="requested_documents[pagibig][duration]">
                                    <option @if(@$request->cbrd_req_docs->pagibig->duration == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@$request->cbrd_req_docs->pagibig->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
        						@endif

                                @if(key($request->cbrd_req_docs) == 'philhealth')
                                <select class="form-control" style="width: 241px" name="requested_documents[philhealth][duration]">
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'Latest 6 months') selected="selected" @endif value="Latest 6 months">Latest 6 months</option>
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'Latest 12 months') selected="selected" @endif value="Latest 12 months">Latest 12 months</option>
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                                @endif
                                
                                @if(key($request->cbrd_req_docs) == 'sss')
                                <select class="form-control" style="width: 241px" name="requested_documents[sss][duration]">
                                    <option @if(@$request->cbrd_req_docs->sss->duration == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@$request->cbrd_req_docs->sss->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                                @endif
                                
                                @if(key($request->cbrd_req_docs) == 'coe')
                                <input type="text" class="text-center" name="requested_documents[coe][number_of_copies]" value="{{ @$request->cbrd_req_docs->coe->number_of_copies }}" style="width: 70px"/>
                                @endif

                                @if(key($request->cbrd_req_docs) == 'coec')
                                <input type="text" class="text-center" name="requested_documents[coec][number_of_copies]" value="{{ @$request->cbrd_req_docs->coec->number_of_copies }}" style="width: 70px"/>
                                @endif
                                
                                @if(key($request->cbrd_req_docs) == 'emp_attendance')
                                    <br /> 
                                    <div class="" style="float:left; padding-right: 10px">
                                        From:
                                    </div>
							        <div class="col2_form_container input-group">
        							    <input type="text" class="form-control date_picker" id="fr" name="requested_documents[emp_attendance][from]" value="{{ @$request->cbrd_req_docs->emp_attendance->from }}" />
        								<label class="input-group-addon btn" for="fr">
        								   <span class="glyphicon glyphicon-calendar"></span>
        								</label>	
                                    </div>
    								<div class="clear_10"></div>
                                    <div class="" style="float:left; padding-right: 24px">
                                        To:
                                    </div>
                                    <div class="col2_form_container input-group">
                                        <input type="text" class="form-control date_picker" id="to" name="requested_documents[emp_attendance][to]" value="{{ @$request->cbrd_req_docs->emp_attendance->to }}" />
                                        <label class="input-group-addon btn" for="to">
    							           <span class="glyphicon glyphicon-calendar"></span>
    							        </label>
                                    </div>
                                @endif
                                
                                @if(key($request->cbrd_req_docs) == 'others')
                                    <br />
                                    <input type="text" name="requested_documents[others][value]" value="{{ $request->cbrd_req_docs->others->value }}" />
                                @endif
                            </td>
							<td class="td_style td_height">{{ $request->cbrd_ref_num }}</td>
							<!--<td class="td_style td_height">{{ EmployeesHelper::getEmployeeNameById($request->cbrd_assigned_to) }}</td>-->
							<td class="td_style td_height">{{ $request->cbrd_remarks }}</td>
						</tr>
					</table>

                    <div class="clear_20"></div>

                    <label class="form_label required">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea rows="4" cols="50" class="form-control textarea_width" name="cb_purpose_of_request" id="cb_purpose_of_request">{{ $request->cb->cb_purpose_of_request }}</textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: <span class="char_max_limit">5000</span>)</strong> Characters left: <span id="char_counter">0</span>
                    </label>

                    <div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <!--<div class="attachment_container">
                    	@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
                    		<div>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/') . '/' . $request->cb->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a> | <a href="javascript:void(0);" class="delete_file">Delete</a><br />
                    		</div>
                    		@endforeach
                    	@endif
                    	<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>
                    	<div id="attachments"></div>
                    </div>-->
					<div class="attachment_container">
                   
                    	<div id="attachments">
							@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
                    		<p>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/'). '/' . $request->cb->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><button class='remove-fn btn btn-xs btn-danger btn_del'>DELETE</button><br />
                    		</p>
                    		@endforeach
							@endif
						</div>
						<span class="btn btn-success btnbrowse fileinput-button">
							<span>BROWSE</span>
							<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
						</span>
                    </div>

            </div><!-- end of form_container -->
            
            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                	<div class="row">
                		<label class="textarea_inside_label">MESSAGE:</label>
                		<textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['cb']['cb_comments'] != '') @foreach(json_decode($request['cb']['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
                	</div>
                </div>
				<div class="clear_10"></div>
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>

            <div class="text-center">
                <a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
            </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
	var allowed_file_count = 5;
	var allowed_total_filesize = 20971520;
    $(function () {
        // $(".delete_file").click(function(){
            // $(this).parent("div").remove()
        // });
        
		//$(".date_picker").datepicker({ format: 'yyyy-mm-dd' });
		$('.date_picker').datepicker({
			dateFormat : 'yy-mm-dd'
		});	

		// $('.remove_file_attachment').live('click', function() {
			// $(this).parent().remove();
		// });
		
		$(".remove-fn").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length < 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment btn-danger' type='button'>DELETE</button></div>");
			}
		});
		
        $("#chkPhilhealth").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPhilhealth").show();
            } else {
                $("#dvPhilhealth").hide();
            }
        });

        $("#chkPagibig").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPagibig").show();
            } else {
                $("#dvPagibig").hide();
            }
        });

        $("#chkSss").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divSss").show();
            } else {
                $("#divSss").hide();
            }
        });

        $("#chkCoe").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoe").show();
            } else {
                $("#divCoe").hide();
            }
        });

        $("#chkCoec").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoec").show();
            } else {
                $("#divCoec").hide();
            }
        });

        $("#chkEmaf").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divEmaf").show();
            } else {
                $("#divEmaf").hide();
            }
        });

        $("#chkAttendance").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divAttendance").show();
            } else {
                $("#divAttendance").hide();
            }
        });
		
		/* Character Counter */
		var text_max = $(".char_max_limit").html();
		$('#char_counter').html(text_max);
		
		$('#cb_purpose_of_request').keyup(function() { 
			update_char_count(text_max);
		});
		
		$('document').ready(function() { 
			update_char_count(text_max);
		});
		/* End Character Counter */
		
		$('#urgency').on('change', function() {
			console.log( this.value );
			  if ( this.value == 'Immediate Attention Needed'){
				$("#date-needed-color").css("color", "#c80b31");
			  }
			  else if(this.value == 'Normal Priority'){
				$("#date-needed-color").css("color", "#383c40");
			  }
		});
		
		$("#emaf_dialog_cancel_action").live('click', function(e){
		   $(".emaf_form").val(function() {
                return this.defaultValue;
            });
            $('.emaf_form_select option').prop('selected', function() {
                return this.defaultSelected;
            });
            $("#emafModal").modal('toggle');
		});
		
	    $('#nom').on('change', function() {
		    if ( this.value == 'Others'){
			    $('#div_nom_others').show();
            }
            else{
			    $('#div_nom_others').hide();
            }
		});
		
    });
	function update_char_count(text_max){
		var text_length = $('#cb_purpose_of_request').val().length;
		var text_remaining = text_max - text_length;
		 
		$('#char_counter').html(text_remaining);
	}
</script>

@stop