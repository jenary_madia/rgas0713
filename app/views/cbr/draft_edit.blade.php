@extends('template/header')

@section('content')
        <form class="form-inline" action="{{ url('cbr/draft/edit/'.$request->cb_ref_num) }}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ $request->cb_emp_name }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $request->cb_ref_num }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ $request->cb_emp_id }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->cb_date_filed }}" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb_company }}" name="comp_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels ">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="{{ $request->cb_status }}" name="cb_status" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb_department }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ Input::old('cb_contact_number') != '' ? Input::old('cb_contact_number') :  $request->cb_contact_no }}" name="cb_contact_number" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input   type="text" class="form-control" value="{{ $request->cb_section }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">URGENCY:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="cb_urgency" id="urgency">
									<option @if($request->cb_urgency == 'Normal Priority') selected="selected" @endif value="Normal Priority">Normal Priority</option>
									<option @if($request->cb_urgency == 'Immediate Attention Needed') selected="selected" @endif value="Immediate Attention Needed">Immediate Attention Needed</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
								@if($request['cb_urgency'] == 'Immediate Attention Needed')
									<label class="labels required" id="date-needed-color">DATE NEEDED:</label>
								@else
									<label class="labels" id="date-needed-color">DATE NEEDED:</label>
								@endif
                            </div>
                            <div class="col2_form_container input-group">
                                <input type="text" class="form-control date_picker" value="@if($request->cb_date_needed != '0000-00-00'){{ $request->cb_date_needed }} @endif"  name="date_needed" id="date_needed"/>
								<label class="input-group-addon btn" for="date_needed">
								   <span class="glyphicon glyphicon-calendar"></span>
								</label>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label required">REQUESTED DOCUMENTS:</label>
					
					@if( Input::old() )
						@include('cbr.partials.requested_documents_from_send')
					@else
						@include('cbr.partials.requested_documents_from_list')
					@endif
					

                    <div class="clear_20"></div>

                    <label class="form_label required">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea rows="4" cols="50" class="form-control textarea_width" name="cb_purpose_of_request" id="cb_purpose_of_request">{{ Input::old('cb_purpose_of_request') != '' ? Input::old('cb_purpose_of_request') :  $request->cb_purpose_of_request }}</textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: <span class="char_max_limit">5000</span>)</strong> Characters left: <span id="char_counter">0</span>
                    </label>
					
					<div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
						<div id="attachments">
                    	@if(count(json_decode($request['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb_attachments']) as $attachment)
                    		<div>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/'). '/' . $request->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
							<button class="btn btn-xs btn-danger remove-fn">DELETE</button><br />
                    		</div>
                    		@endforeach
                    	@endif
						</div>
						<span class="btn btn-success btnbrowse fileinput-button">
							<span>BROWSE</span>
							<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
						</span>
                    </div>
            </div><!-- end of form_container -->
            
            <div class="clear_20"></div>

            <div class="form_container"><span class="legend-action">ACTION</span>
				<!--
                <div class="textarea_messages_container">
                	<div class="row">
                		<label class="textarea_inside_label">MESSAGE:</label>
                		<textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['cb_comments'] != '') @foreach(json_decode($request['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
                	</div>
                </div>
				-->
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment">@if( $request['cb_comments'] != '')@foreach(json_decode($request['cb_comments']) as $comment){{ $comment->message }}@endforeach @endif</textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>

            <div class="text-center">
                <a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
            </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
	var allowed_file_count = 5;
	var allowed_total_filesize = 20971520;
    $(function () {
        // $(".delete_file").click(function(){
            // $(this).parent("div").remove()
        // });
        
		//$(".date_picker").datepicker({ format: 'yyyy-mm-dd' });
		
		$('.date_picker').datepicker({
			dateFormat : 'yy-mm-dd'
		});	
		
		// $('.remove_file_attachment').live('click', function() {
			// $(this).parent().remove();
		// });
		
		// $(".remove_file_attachment").live('click', function() {
			// var a = confirm("Are you sure you want to delete this attachment?");
			// if (a == true) {
				// $(this).parent().remove();
			// }
			// return a;
		// });
		
		$(".remove-fn").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length < 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment remove-fn btn-danger'>DELETE</button></div>");
			}
		});
		
        $("#chkPhilhealth").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPhilhealth").show();
            } else {
                $("#dvPhilhealth").hide();
            }
        });

        $("#chkPagibig").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPagibig").show();
            } else {
                $("#dvPagibig").hide();
            }
        });

        $("#chkSss").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divSss").show();
            } else {
                $("#divSss").hide();
            }
        });

        $("#chkCoe").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoe").show();
            } else {
                $("#divCoe").hide();
            }
        });

        $("#chkCoec").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoec").show();
            } else {
                $("#divCoec").hide();
            }
        });

        $("#chkEmaf").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divEmaf").show();
            } else {
                $("#divEmaf").hide();
            }
        });

        $("#chkAttendance").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divAttendance").show();
            } else {
                $("#divAttendance").hide();
            }
        });
        
        $('#nom').on('change', function() {
			  if ( this.value == 'Others'){
				$('#div_nom_others').show();
			  }
			  else{
				$('#div_nom_others').hide();
			  }
		});
		
		/* Character Counter */
		var text_max = $(".char_max_limit").html();
		$('#char_counter').html(text_max);
		
		$('#cb_purpose_of_request').keyup(function() { 
			update_char_count(text_max);
		});
		
		$('document').ready(function() { 
			update_char_count(text_max);
		});
		/* End Character Counter */
		
		$('#urgency').on('change', function() {
			console.log( this.value );
			  if ( this.value == 'Immediate Attention Needed'){
				$("#date-needed-color").css("color", "#c80b31");
			  }
			  else if(this.value == 'Normal Priority'){
				$("#date-needed-color").css("color", "#383c40");
			  }
		});
		
    });
	function update_char_count(text_max){
		var text_length = $('#cb_purpose_of_request').val().length;
		var text_remaining = text_max - text_length;
		 
		$('#char_counter').html(text_remaining);
	}
</script>

@stop