@extends('template/header')

@section('content')
<div class="wrapper">
	<div class="clear_20"></div>
		<div class="datatable_holder">
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY CBR REQUEST</span>
		<table id="requests_my_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_my_list">
			<thead> 
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Current</th>
					<th align="center">Status</th>
					<th align="center">Action</th>
				</tr>							
			</thead>
		</table>
		</div>
		<div class="clear_20"></div> 	
	
</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this request "+$(this).data('request-ref-num')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop
