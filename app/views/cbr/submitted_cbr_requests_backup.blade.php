@extends('template/header')

@section('content')
<div class="wrapper">
	<div class="clear_20"></div>
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >My CBR Requests</span>
		<table id="requests_my_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_my_list">
			<thead> 
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Status</th>
					<th align="center">Action</th>
				</tr>							
			</thead>
		</table>
		<div class="clear_20"></div> 	
		<hr />
		@if(Session::get('is_cbr_receiver'))
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >Received CBR Requests</span>
			<table id="requests_new_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_new_list">
				<thead>
					<tr role="row">
						<th align="center">Reference No.</th>
						<th align="center">Requestor</th>
						<th align="center">Date Filed</th>
						<th align="center">Action</th>
					</tr>							
				</thead>
			</table>
			<div class="clear_20"></div> 
		@endif
		@if(Session::get('is_cbr_receiver') || Session::get('is_cbr_staff'))
		<hr />
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >In Process CBR Requests</span>
		<table id="requests_in_process_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_in_process_list">
			<thead>
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Requestor</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Assigned To</th>
					<th align="center">Date Assigned</th>
					<th align="center">Current</th>
					<th align="center">Status</th>
					<th align="center">Action</th>
				</tr>							
			</thead>
		</table>
		<div class="clear_20"></div> 
		<hr />
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >Acknowledged CBR Requests</span>
		<table id="requests_acknowledged_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_acknowledged_list">
			<thead> 
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Requestor</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Assigned To</th>
					<th align="center">Date Assigned</th>
					<!--<th align="center">Date Completed</th>-->
					<th align="center">Date Acknowledged</th>
				</tr>							
			</thead>
		</table>
		<div class="clear_20"></div>
		@endif
</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this request "+$(this).data('request-ref-num')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop
