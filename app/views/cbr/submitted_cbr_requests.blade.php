@extends('template/header')

@section('content')
<div class="wrapper">
	<div class="clear_20"></div>
		 
		@if(Session::get('is_cbr_receiver'))
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >RECEIVED CBR REQUEST</span>
			<table id="requests_new_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_new_list">
				<thead> 
					<tr role="row">
						<th align="center">Reference No.</th>
						<th align="center">Requestor</th>
						<th align="center">Date Filed</th>
						<th align="center">Action</th>
					</tr>							
				</thead>
			</table>
			</div>
			<div class="clear_20"></div> 
			<hr />
		@endif
		@if(Session::get('is_cbr_receiver') || Session::get('is_cbr_staff'))
		
		<div class="datatable_holder">
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >IN PROCESS CBR REQUEST</span>
		<table id="requests_in_process_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_in_process_list">
			<thead> 
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Requestor</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Assigned To</th>
					<th align="center">Date Assigned</th>
					<th align="center">Current</th>
					<th align="center">Status</th>
					<th align="center">Action</th>
				</tr>							
			</thead>
		</table>
		</div>
		<div class="clear_20"></div> 
		<hr />
		<div class="datatable_holder">
		<span class="list-title" role="columnheader" rowspan="1" colspan="5" >ACKNOWLEDGED CBR REQUEST</span>
		<table id="requests_acknowledged_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_acknowledged_list">
			<thead> 
				<tr role="row">
					<th align="center">Reference No.</th>
					<th align="center">Requestor</th>
					<th align="center">Date Filed</th>
					<th align="center">Requested Document</th>
					<th align="center">Assigned To</th>
					<th align="center">Date Assigned</th>
					<th align="center">Date Acknowledged</th>
					<th align="center">Action</th>
				</tr>							
			</thead>
		</table>
		</div>
		<div class="clear_20"></div>
		@endif
</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this request "+$(this).data('request-ref-num')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop
