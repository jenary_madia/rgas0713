@extends('template/header')

@section('content')
        
	<div class="form_container">
		<label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>
			
			@include('cbr.partials.request_info', $request)
			
			<div class="clear_20"></div>

			@include('cbr.partials.requested_docs1', $request)

			<div class="clear_20"></div>

			@include('cbr.partials.purpose_of_request', $request)

			<div class="clear_20"></div>
			
			@include('cbr.partials.attachments', array('browse'=>0))
	</div><!-- end of form_container -->

	<div class="clear_20"></div>

	<div class="form_container ">
		@include('cbr.partials.messages', $request)
		@if($request['cbrd_status'] == "For Acknowledgement" && $request['cbrd_current'] == Auth::user()->id)
		<form action="{{ URL('cbr/acknowledged') }}" method="post">
		<input type="hidden" name="cbrd_id" value="{{ $request['cbrd_id'] }}">

		<div class="clear_10"></div>
		<div class="row">
			<div class="clear_10"></div>
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes"><strong>ACKNOWLEDGE</strong> COMPLETION OF REQUESTED DOCUMENT</label>
				</div> 

				 <div class="comment_button" style="padding-left:170px;">
                    <button type="submit" class="btn btn-default btndefault"  style="width:100px" >ACKNOWLEDGE</button>
                </div>
			</div>
			<div class="clear_10"></div>
		</div>
		@endif
		</form>
		<div class="clear_20"></div>
			<div class="text-center">
				<a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
			</div>
	</div><!-- end of form_container -->
        
@stop