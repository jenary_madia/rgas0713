<div class="container-header"><h5 class="text-center lined sub-header "><strong>ATTACHMENTS</strong></h5></div>
<label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
<div class="attachment_container">
	@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
		<a href="{{ URL::to('/cbr/download/') . '/' . $request['cb']['cb_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
		@endforeach
	@endif
		<div id="attachments"></div>
		<div class="clear_10"></div>
	@if( $browse == 1)
	<button type="button" class="btn btnbrowse btn-default" id="btnCbrAddAttachment">BROWSE</button><br/>
	<div id="attachments"></div>
	@endif
</div>