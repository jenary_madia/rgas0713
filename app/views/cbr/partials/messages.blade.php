<div class="textarea_messages_container">
	<div class="row">
		<label class="textarea_inside_label">MESSAGE:</label>
		<textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['cb']['cb_comments'] != '') @foreach(json_decode($request['cb']['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
	</div>
</div>