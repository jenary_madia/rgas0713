<label class="form_label label_required">REQUESTED DOCUMENTS:</label>
<table border = "1" cellpadding = "0" class="tbl3">
	<th class="th_style td_height">Document</th>
	<th class="th_style td_height">Reference No.</th>
	<th class="th_style td_height">Assigned To</th>
	<th class="th_style td_height">CBR Remarks</th>
	<th class="th_style td_height">Date Document is Endorsed to Requestor</th>

	<tr>
		<td class="td_style td_height">
			@if(key(json_decode($cbrd_req_docs)) == 'emaf')
			<a href="#" data-toggle="modal" data-target="#emafModal">{{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->name }}</a>
			<!-- Modal -->
			<div id="emafModal" class="modal fade" role="dialog" data-rel="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
				  		<div class="modal-header">
							Employment Movement Action Form (EMAF) <button type="button" class="close" data-dismiss="modal">&times;</button>
				  		</div>
				  		<div class="modal-body">
							<div id="divEmaf" style="display: block">
                				<div class="extra_labels">
                    				<label class="labels">Nature of Movement:</label>
                				</div>
                			<div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" disabled>
                                    <option value="Transfer" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
                                    <option value="Promotion" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
                                    <option value="Temporary Assignment" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
                                    <option value="Change of Position Title" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
                                    <option value="Salary Adjustment" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
                                    <option value="Others" id="emaf_others" @if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
                                </select>
                			</div>
                			<div class="clear_10"></div>
							@if(@json_decode($cbrd_req_docs)->emaf->nature_of_movement == "Others")
							<div class="extra_labels">
								<label class="labels">Others:</label>
							</div>
                            <div class="col2_form_container">
                                <input disabled type="text" class="form-control" name="requested_documents[emaf][others]" value="{{ @json_decode($cbrd_req_docs)->emaf->others }}">  
                            </div>
                            <div class="clear_10"></div>
							@endif
                            <div class="extra_labels">
                                <label class="labels">Effectivity Date:</label>
                            </div>
                            <div class="col2_form_container input-group">
                                <input disabled type="text" class="form-control date_picker" id="effdate" name="requested_documents[emaf][effectivity_date]" value="{{ @json_decode($cbrd_req_docs)->emaf->effectivity_date }}" />
                            	<label class="input-group-addon btn" for="effdate">
								   <span class="glyphicon glyphicon-calendar"></span>
								</label>
			                </div>
                            <div class="clear_10"></div>
                			<div class="tbl-responsive">
                    			<table border = "1" cellpadding = "0" class="">
                                    <th class="th_style">DETAILS</th>
                                    <th class="th_style">PRESENT STATUS</th>
                                    <th class="th_style">PROPOSED STATUS</th>
                                    <th class="th_style">REMARKS</th>

                                    <tr>
                                        <td class="td_style">Company</td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[0]->{1} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[0]->{2} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[0]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Department</td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[1]->{1} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[1]->{2} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[1]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Designation</td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[2]->{1} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[2]->{2} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[2]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Job Level</td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[3]->{1} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[3]->{2} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[3]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Salary Rate</td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[4]->{1} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[4]->{2} }}" /></td>
                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ @json_decode($cbrd_req_docs)->emaf->table[4]->{3} }}" /></td>
                                    </tr>
                    			</table>
                			</div>
            			</div>
				  	</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" id="tep_add_action" data-dismiss="modal">OK</button>
						<button type="button" class="btn btn-default" id="cancel_btn" data-dismiss="modal">CANCEL</button>
					</div>
				</div>
			  </div>
			</div>
		@else
			{{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->name }}
		@endif
		
		@if(@json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->duration)
		<br/>Duration: {{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->duration }}
		@endif
		
		@if(@json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->number_of_copies)
		<br/>No. of Copies: {{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->number_of_copies }}
		@endif
		
		@if(@json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->from)
		<br/>From: {{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->from }}
		@endif
		
		@if(@json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->to)
		<br/>To: {{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->to }}
		@endif
		
		@if(@json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->value)
		<br/>{{ json_decode($cbrd_req_docs)->{key(json_decode($cbrd_req_docs))}->value }}
		@endif
		
		</td>
		<td class="td_style td_height">{{ $cbrd_ref_num }}</td>
		<td class="td_style td_height">{{ EmployeesHelper::getEmployeeNameById($cbrd_assigned_to) }}</td>
		<td class="td_style td_height">{{ $cbrd_remarks }}</td>
		<td class="td_style td_height input-group border-none">
			<input type="text" class="form-control date_picker" name="date_endorsed_to_requestor" id="date_endorsed_to_requestor"/>
			<label class="input-group-addon btn" for="date_endorsed_to_requestor">
			   <span class="glyphicon glyphicon-calendar"></span>
			</label>
		</td>
	</tr>
</table>