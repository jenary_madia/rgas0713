<div class="checkbox_container">
                        <div class="col_checkbox">
							<input @if(@array_key_exists("name",Input::old('requested_documents')['pagibig'])) checked="checked" @endif type="checkbox" id="chkPagibig" name="requested_documents[pagibig][name]" value="Certificate of Premium Contributions � Pag-IBIG"/>						
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPagibig">
                                Certificate of Premium Contributions � Pag-IBIG (for resigned employees)<i class="itext">(This document is secured directly from Pag-IBIG office by CHRD Representative)</i>
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="dvPagibig" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['pagibig'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[pagibig][duration]">
                                    <option @if(@Input::old('requested_documents')['pagibig']["duration"] == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@Input::old('requested_documents')['pagibig']["duration"] == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['philhealth'])) checked="checked" @endif type="checkbox" id="chkPhilhealth" name="requested_documents[philhealth][name]" value="Certificate of Premium Contributions � Philhealth" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPhilhealth">
                                Certificate of Premium Contributions � Philhealth
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="dvPhilhealth" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['philhealth'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[philhealth][duration]">
                                    <option @if(@Input::old('requested_documents')['philhealth']["duration"] == 'Latest 6 months') selected="selected" @endif value="Latest 6 months">Latest 6 months</option>
                                    <option @if(@Input::old('requested_documents')['philhealth']["duration"] == 'Latest 12 months') selected="selected" @endif value="Latest 12 months">Latest 12 months</option>
                                    <option @if(@Input::old('requested_documents')['philhealth']["duration"] == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>


                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['sss'])) checked="checked" @endif type="checkbox" id="chkSss" name="requested_documents[sss][name]" value="Certificate of Premium Contributions � SSS"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkSss">
                                Certificate of Premium Contributions � SSS
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div class="" id="divSss" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['sss'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[sss][duration]">
                                    <option @if(@Input::old('requested_documents')['sss']["duration"] == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@Input::old('requested_documents')['sss']["duration"] == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>


                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['coe'])) checked="checked" @endif type="checkbox" id="chkCoe" name="requested_documents[coe][name]" value="Certificate of Employment" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoe">
                                Certificate of Employment
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divCoe" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['coe'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" name="requested_documents[coe][number_of_copies]" value="{{ @Input::old('requested_documents')['coe']['number_of_copies'] }}" />
                            </div>
                        </div>

                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['coec'])) checked="checked" @endif type="checkbox" id="chkCoec" name="requested_documents[coec][name]" value="Certificate of Employment with Compensation" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoec">
                                Certificate of Employment with Compensation
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divCoec" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['coec'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" name="requested_documents[coec][number_of_copies]" value="{{ @Input::old('requested_documents')['coec']['number_of_copies'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['clearance_certification'])) checked="checked" @endif type="checkbox" name="requested_documents[clearance_certification][name]" value="Clearance Certification" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Clearance Certification (for resigned employees) � <i class="itext">Released only upon receipt of final pay from Accounting Department</i>
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['itr'])) checked="checked" @endif type="checkbox" name="requested_documents[itr][name]" value="Income Tax Return" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Income Tax Return
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['pagibig_esav'])) checked="checked" @endif type="checkbox" name="requested_documents[pagibig_esav][name]" value="Pag-IBIG Employee Statement of Accumulated Value (ESAV)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Pag-IBIG Employee Statement of Accumulated Value (ESAV) (for housing loan application)
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['ph_claim'])) checked="checked" @endif type="checkbox" name="requested_documents[ph_claim][name]" value="Philhealth Claim Form 1" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Claim Form 1
                            </label>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['ph_mdr'])) checked="checked" @endif type="checkbox" name="requested_documents[ph_mdr][name]" value="Philhealth Member Data Record (MDR)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Member Data Record (MDR)
                            </label>
                        </div>		
						
						<?php if(Session::get('desig_level') != 'employee' && Session::get('desig_level') != 'supervisor'):?>
							<div class="clear_10"></div>
							<div class="col_checkbox">
								<input @if(@array_key_exists("name",Input::old('requested_documents')['copy_of_201_file'])) checked="checked" @endif type="checkbox" name="requested_documents[copy_of_201_file][name]" value="Copy of 201 File" />
							</div>
							<div class="checkbox_label">
								<label class="labels">
									Copy of 201 File
								</label>
							</div>
						<?php endif;?>
						
                        <div class="clear_10"></div>
						
						<?php if(Session::get('desig_level') != 'employee') : ?>
							<div class="col_checkbox">
								<input @if(@array_key_exists("name",Input::old('requested_documents')['emaf'])) checked="checked" @endif type="checkbox" id="chkEmaf" name="requested_documents[emaf][name]" value="Employee Movement Action Form (EMAF)" />
							</div>
							<div class="checkbox_label">
								<label class="labels" for="chkEmaf">
									Employee Movement Action Form (EMAF)
								</label>
							</div>

							<!-- Show if above is selected -->
							<div id="divEmaf" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['emaf'])) block @else none @endif">
								<div class="extra_labels">
									<label class="labels">Nature of Movement:</label>
								</div>
								<div class="col2_form_container">
									<select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" id="nom">
										<option value="Transfer" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == 'Transfer') selected="selected" @endif>Transfer</option>
										<option value="Promotion" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == 'Promotion') selected="selected" @endif>Promotion</option>
										<option value="Temporary Assignment" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == 'Temporary Assignment') selected="selected" @endif>Temporary Assignment</option>
										<option value="Change of Position Title" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == '="Change of Position Title') selected="selected" @endif>Change of Position Title</option>
										<option value="Salary Adjustment" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == 'Salary Adjustment') selected="selected" @endif>Salary Adjustment</option>
										<option value="Others" id="emaf_others" @if(@Input::old('requested_documents')['emaf']['nature_of_movement'] == 'Others') selected="selected" @endif>Others</option>
									</select>
								</div>

								<div class="extra_labels">
									<label class="labels">Effectivity Date:</label>
								</div>
								<div class="col2_form_container input-group">
									<input type="text" style="width: 241px" id="eff_date" class="form-control date_picker" name="requested_documents[emaf][effectivity_date]" value="{{ @$documents["emaf"]["effectivity_date"] }}"/>                        
								    <label class="input-group-addon btn" for="eff_date">
    								   <span class="glyphicon glyphicon-calendar"></span>
    								</label>
								</div>
								
    							<div class="clear_10"></div>
    							<div id="div_nom_others" style='display: @if(@$documents["emaf"]["nature_of_movement"] == "Others") block 
    							@elseif(Input::old("requested_documents[emaf][nature_of_movement]") == "Others") block @else none @endif'>
    								<div class="extra_labels">
    									<label class="labels">Others: </label>
    								</div>
                                    <div class="col2_form_container">
                                        <input type="text" class="form-control" name="requested_documents[emaf][others]" value='{{ @$documents["emaf"]["others"] }}'>  
                                    </div>
                                </div>
                            
								<div class="clear_10"></div>
								<div class="tbl">
									<table border = "1" cellpadding = "0" class="tbl">
										<th class="th_style">DETAILS</th>
										<th class="th_style">PRESENT STATUS</th>
										<th class="th_style">PROPOSED STATUS</th>
										<th class="th_style">REMARKS</th>

										<tr>
											<td class="td_style">Company</td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ @Input::old('requested_documents')['emaf'][table][0][1] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ @Input::old('requested_documents')['emaf'][table][0][2] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ @Input::old('requested_documents')['emaf'][table][0][3] }}" /></td>
										</tr>
										<tr>
											<td class="td_style">Department</td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ @Input::old('requested_documents')['emaf'][table][1][1] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ @Input::old('requested_documents')['emaf'][table][1][2] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ @Input::old('requested_documents')['emaf'][table][1][3] }}" /></td>
										</tr>
										<tr>
											<td class="td_style">Designation</td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ @Input::old('requested_documents')['emaf'][table][2][1] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ @Input::old('requested_documents')['emaf'][table][2][2] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ @Input::old('requested_documents')['emaf'][table][2][3] }}" /></td>
										</tr>
										<tr>
											<td class="td_style">Job Level</td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ @Input::old('requested_documents')['emaf'][table][3][1] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ @Input::old('requested_documents')['emaf'][table][3][2] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ @Input::old('requested_documents')['emaf'][table][3][3] }}" /></td>
										</tr>
										<tr>
											<td class="td_style">Salary Rate</td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ @Input::old('requested_documents')['emaf'][table][4][1] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ @Input::old('requested_documents')['emaf'][table][4][2] }}" /></td>
											<td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ @Input::old('requested_documents')['emaf'][table][4][3] }}" /></td>
										</tr>
									</table>
								</div>
							</div>
							<div class="clear_10"></div>
						<?php endif; ?>
						
                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['emp_attendance'])) checked="checked" @endif type="checkbox" id="chkAttendance" name="requested_documents[emp_attendance][name]" value="Employee Attendance" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkAttendance">
                                Employee Attendance
                            </label>
                        </div>

                        <!-- Show if above is selected -->
                        <div id="divAttendance" style="display: @if (@array_key_exists("name",Input::old('requested_documents')['emp_attendance'])) block @else none @endif">
                            <div class="extra_labels">
                                <label class="labels">From:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][from]" value="{{ @Input::old('requested_documents')['emp_attendance']['from'] }}" />
                            </div>
                            <br/>
                            <div class="clear_10"></div>
                            <div class="extra_labels">
                                <label class="labels">To:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][to]" value="{{ @Input::old('requested_documents')['emp_attendance']['to'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>

                        <div class="col_checkbox">
                            <input @if(@array_key_exists("name",Input::old('requested_documents')['others'])) checked="checked" @endif type="checkbox" name="requested_documents[others][name]" value="Others"  />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Others:  <input type="text" name="requested_documents[others][value]" value="{{ @Input::old('requested_documents')['others']['value'] }}" />
                            </label>
                        </div>
                    </div><!-- end of checkbox_container -->