<div class="row">
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">EMPLOYEE NAME:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_emp_name'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">REFERENCE NUMBER:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_ref_num'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">EMPLOYEE NUMBER:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_emp_id'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">DATE FILED:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_date_filed'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">COMPANY:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_company'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">STATUS:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cbrd_status'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">DEPARTMENT:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_department'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels">CONTACT NUMBER:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_contact_no'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">SECTION:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_section'] }}" />
							</div>
						</div>
						<div class="row_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">URGENCY:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_urgency'] }}" />
							</div>
						</div>
						<div class="clear_10"></div>
						<div class="col3_form_container">
							<div class="col1_form_container">
								<label class="labels label_required">DATE NEEDED:</label>
							</div>
							<div class="col2_form_container">
								<input readonly type="text" class="form-control" value="{{ $request['cb']['cb_date_needed'] }}" />
							</div>
						</div>
					</div>