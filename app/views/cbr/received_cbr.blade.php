@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">
<div id="wrap">
	@include('template/sidebar')
    <div class="container">
        <form class="form-inline">
            <div class="form_container">
                <label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_emp_name'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_ref_num'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_emp_id'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control placeholders" value="{{ $request['cb_date_filed'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_company'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels ">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_status'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_department'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_contact_no'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_section'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">URGENCY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly type="text" class="form-control" value="{{ $request['cb_urgency'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
								@if($request['cb_urgency'] == 'Immediate Attention Needed')
									<label class="labels required">DATE NEEDED:</label>
								@else
									<label class="labels">DATE NEEDED:</label>
								@endif
                            </div>
                            <div class="col2_form_container input-group">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request['cb_date_needed'] }}" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label required">REQUESTED DOCUMENTS:</label>

                    <table border = "1" cellpadding = "0" class="tbl2">
						<th class="th_style td_height">Document</th>
						<th class="th_style td_height">Reference No.</th>
						<th class="th_style td_height">Assigned To</th>
						<th class="th_style td_height">Date Assigned</th>
						<th class="th_style td_height">Action</th>
						<th class="th_style td_height">CBR Remarks</th>
						@foreach($request['cbrd'] as $cbrd)
						<tr>
							<td class="td_style td_height">{{ json_decode($cbrd->cbrd_req_docs)->name }}</td>
							<td class="td_style td_height">{{ $cbrd->cbrd_ref_num }}</td>
							<td class="td_style td_height">{{ $cbrd->cbrd_assigned_to }}</td>
							<td class="td_style td_height">{{ $cbrd->cbrd_date_assigned }}</td>
							<td class="td_style">
                                <button type="submit" class="btn btn2 btn-default" value="">ASSIGN</button>
                                <button type="submit" class="btn btn2 btn-default" value="">RETURN</button>
                            </td>
							<td class="td_style td_height">{{ $cbrd->cbrd_remarks }}</td>
						</tr>
						@endforeach
                    </table>

                    <div class="clear_20"></div>

                    <label class="form_label required">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea readonly name="purpose_request" rows="4" cols="50" class="form-control textarea_width" placeholder="<Purpose of the request goes here>">{{ $request['cb_purpose_of_request'] }}</textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: 5000)</strong> Characters left: 4966
                    </label>


                    <div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
                    <div class="attachment_container">
                        @foreach(json_decode($request['cb_attachments']) as $attachment)
						<a href="{{ URL::to('/cbr/download/') . '/' . $request['cb_ref_num'] . '/' . $attachment }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
						@endforeach
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea readonly name="purpose_request" rows="3" class="form-control textarea_inside_width">@if( $request['cb_comments'] != '') {{ json_decode($request['cb_comments'])->name }} {{ json_decode($request['cb_comments'])->datetime }}: {{ json_decode($request['cb_comments'])->message }}@endif</textarea>
                    </div>
                </div>                
            </div><!-- end of form_container -->

        </form>
        <div class="clear_20"></div>
    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop