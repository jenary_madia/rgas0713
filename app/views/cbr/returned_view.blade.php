@extends('template/header')

@section('content')
        <form class="form-inline" action="{{ Request::url() }}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ $request->cb->cb_emp_name }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $request->cb->cb_ref_num }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ $request->cb->cb_emp_id }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->cb->cb_date_filed }}" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_company }}" name="comp_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="{{ $request->cbrd_status }}" name="cb_status" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_department }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ $request->cb->cb_contact_no }}" name="cb_contact_number" disabled/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels ">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_section }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">URGENCY:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="cb_urgency" disabled>
									<option @if($request->cb->cb_urgency == 'Normal Priority') selected="selected" @endif value="Normal Priority">Normal Priority</option>
									<option @if($request->cb->cb_urgency == 'Immediate Attention Needed') selected="selected" @endif value="Immediate Attention Needed">Immediate Attention Needed</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
								@if($request['cb']['cb_urgency'] == 'Immediate Attention Needed')
									<label class="labels required">DATE NEEDED:</label>
								@else
									<label class="labels">DATE NEEDED:</label>
								@endif
                            </div>
							<div class="col2_form_container input-group">
                                <input readonly="readonly" type="text" class="form-control date_picker" value="@if($request->cb->cb_date_needed != '0000-00-00'){{ $request->cb->cb_date_needed }} @endif"  name="date_needed" disabled/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label required">REQUESTED DOCUMENTS:</label>

                    
                    <table border = "1" cellpadding = "0" class="tbl3">
						<th class="th_style td_height">Document</th>
						<th class="th_style td_height">Reference No.</th>
						<!--<th class="th_style td_height">Assigned To</th>-->
						<th class="th_style td_height">CBR Remarks</th>
						<tr>
							<td class="td_style td_height">
							    @if(key($request->cbrd_req_docs) == 'emaf')
							    <a href="#" data-toggle="modal" data-target="#emafModal">{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}</a>
    							<!-- Modal -->
    							<div id="emafModal" class="modal fade" role="dialog" data-rel="dialog">
    								<div class="modal-dialog">
    									<!-- Modal content-->
    									<div class="modal-content">
    								  		<div class="modal-header">
    											Employment Movement Action Form (EMAF) <button type="button" class="close" data-dismiss="modal">&times;</button>
    								  		</div>
    								  		<div class="modal-body">
    											<div id="divEmaf" style="display: block">
    	                            				<div class="extra_labels">
    	                                				<label class="labels">Nature of Movement:</label>
    	                            				</div>
    	                            			<div class="col2_form_container">
    				                                <select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" disabled>
    				                                    <option value="Transfer" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
    				                                    <option value="Promotion" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
    				                                    <option value="Temporary Assignment" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
    				                                    <option value="Change of Position Title" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
    				                                    <option value="Salary Adjustment" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
    				                                    <option value="Others" id="emaf_others" @if($request->cbrd_req_docs->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
    				                                </select>
    	                            			</div>
    	                            			<div class="clear_10"></div>
    											@if($request->cbrd_req_docs->emaf->nature_of_movement == "Others")
    											<div class="extra_labels">
    												<label class="labels">Others:</label>
    											</div>
    			                                <div class="col2_form_container">
    			                                    <input disabled type="text" class="form-control" name="requested_documents[emaf][others]" value="{{ $request->cbrd_req_docs->emaf->others }}">  
    			                                </div>
    			                                <div class="clear_10"></div>
    											@endif
    				                            <div class="extra_labels">
    				                                <label class="labels">Effectivity Date:</label>
    				                            </div>
    				                            <div class="col2_form_container input-group">
    				                                <input disabled type="text" class="form-control date_picker" id="effdate" name="requested_documents[emaf][effectivity_date]" value="{{ $request->cbrd_req_docs->emaf->effectivity_date }}" />
    				                            	<label class="input-group-addon btn" for="effdate">
    												   <span class="glyphicon glyphicon-calendar"></span>
    												</label>
    							                </div>
    				                            <div class="clear_10"></div>
    	                            			<div class="tbl-responsive">
    	                                			<table border = "1" cellpadding = "0" class="">
    				                                    <th class="th_style">DETAILS</th>
    				                                    <th class="th_style">PRESENT STATUS</th>
    				                                    <th class="th_style">PROPOSED STATUS</th>
    				                                    <th class="th_style">REMARKS</th>
    	
    				                                    <tr>
    				                                        <td class="td_style">Company</td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{1} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{2} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ $request->cbrd_req_docs->emaf->table[0]->{3} }}" /></td>
    				                                    </tr>
    				                                    <tr>
    				                                        <td class="td_style">Department</td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{1} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{2} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ $request->cbrd_req_docs->emaf->table[1]->{3} }}" /></td>
    				                                    </tr>
    				                                    <tr>
    				                                        <td class="td_style">Designation</td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{1} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{2} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ $request->cbrd_req_docs->emaf->table[2]->{3} }}" /></td>
    				                                    </tr>
    				                                    <tr>
    				                                        <td class="td_style">Job Level</td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{1} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{2} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ $request->cbrd_req_docs->emaf->table[3]->{3} }}" /></td>
    				                                    </tr>
    				                                    <tr>
    				                                        <td class="td_style">Salary Rate</td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{1} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{2} }}" /></td>
    				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ $request->cbrd_req_docs->emaf->table[4]->{3} }}" /></td>
    				                                    </tr>
    	                                			</table>
    	                            			</div>
    	                        			</div>
    								  	</div>
    									<div class="modal-footer">
    										<button type="button" class="btn btn-default" id="tep_add_action" data-dismiss="modal">OK</button>
    									</div>
    								</div>
    							  </div>
    							</div>
							
							    @else
							    {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}
							    @endif
							    
    							@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->duration)
        						<br/>Duration: {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->duration }}
        						@endif
        						
        						@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->number_of_copies)
        						<br/>No. of Copies: {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->number_of_copies }}
        						@endif
        						
        						@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->from)
        						<br/>From: {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->from }}
        						@endif
        						
        						@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->to)
        						<br/>To: {{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->to }}
        						@endif
        						
        						@if(@$request->cbrd_req_docs->{key($request->cbrd_req_docs)}->value)
        						<br/>{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->value }}
        						@endif
							 </td>
							<td class="td_style td_height">{{ $request->cbrd_ref_num }}</td>
							<!--<td class="td_style td_height">{{ EmployeesHelper::getEmployeeNameById($request->cbrd_assigned_to) }}</td>-->
							<td class="td_style td_height">{{ $request->cbrd_remarks }}</td>
						</tr>
					</table>

                    <div class="clear_20"></div>

                    <label class="form_label required">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea rows="4" cols="50" class="form-control textarea_width" name="cb_purpose_of_request" disabled>{{ $request->cb->cb_purpose_of_request }}</textarea>
                    </div>

					<div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <div class="attachment_container">
                    	@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
                    		<div>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/') . '/' . $request->cb->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
                    		</div>
                    		@endforeach
                    	@endif
                    	<!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                    	<div id="attachments"></div>
                    </div>

            </div><!-- end of form_container -->
            
            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                	<div class="row">
                		<label class="textarea_inside_label">MESSAGE:</label>
                		<textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['cb']['cb_comments'] != '') @foreach(json_decode($request['cb']['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
                	</div>
                </div>

                <div class="clear_20"></div>
            <div class="text-center">
                <a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
            </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
        $(".delete_file").click(function(){
            $(this).parent("div").remove()
        });
        
		//$(".date_picker").datepicker({ format: 'yyyy-mm-dd' });
		$('.date_picker').datepicker({
			dateFormat : 'yy-mm-dd'
		});	
		
		$('.remove_file_attachment').live('click', function() {
			$(this).parent().remove();
		});
		
		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length < 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment btn-danger' type='button'>DELETE</button></div>");
			}
		});
		
        $("#chkPhilhealth").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPhilhealth").show();
            } else {
                $("#dvPhilhealth").hide();
            }
        });

        $("#chkPagibig").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPagibig").show();
            } else {
                $("#dvPagibig").hide();
            }
        });

        $("#chkSss").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divSss").show();
            } else {
                $("#divSss").hide();
            }
        });

        $("#chkCoe").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoe").show();
            } else {
                $("#divCoe").hide();
            }
        });

        $("#chkCoec").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoec").show();
            } else {
                $("#divCoec").hide();
            }
        });

        $("#chkEmaf").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divEmaf").show();
            } else {
                $("#divEmaf").hide();
            }
        });

        $("#chkAttendance").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divAttendance").show();
            } else {
                $("#divAttendance").hide();
            }
        });
		
    });
</script>

@stop