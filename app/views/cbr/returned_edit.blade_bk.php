@extends('template/header')

@section('content')
        <form class="form-inline" action="{{ Request::url() }}" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="employee_name" name="employee_name" value="{{ $request->cb->cb_emp_name }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="cb_ref_num" value="{{ $request->cb->cb_ref_num }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ $request->cb->cb_emp_id }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->cb->cb_date_filed }}" name="cb_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_company }}" name="comp_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="{{ $request->cbrd_status }}" name="cb_status" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_department }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ $request->cb->cb_contact_no }}" name="cb_contact_number" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $request->cb->cb_section }}" name="sect_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">URGENCY:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="cb_urgency">
									<option @if($request->cb->cb_urgency == 'Normal Priority') selected="selected" @endif value="Normal Priority">Normal Priority</option>
									<option @if($request->cb->cb_urgency == 'Immediate Attention Needed') selected="selected" @endif value="Immediate Attention Needed">Immediate Attention Needed</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE NEEDED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" value="{{ $request->cb->cb_date_needed }}" name="date_needed"/>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label required">REQUESTED DOCUMENTS:</label>
					<input type="hidden" value="{{ $request->cbrd_id }}" name="cbrd_id" />
					<!--<div class="checkbox_container">
					    
					    @if(key($request->cbrd_req_docs) == 'pagibig')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkPagibig" name="requested_documents[pagibig][name]" value="Certificate of Premium Contributions – Pag-IBIG"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPagibig">
                                Certificate of Premium Contributions – Pag-IBIG (for resigned employees)<i class="itext">(This document is secured directly from Pag-IBIG office by CHRD Representative)</i>
                            </label>
                        </div>
                        <div class="" id="dvPagibig" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[pagibig][duration]">
                                    <option @if(@$request->cbrd_req_docs->pagibig->duration == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@$request->cbrd_req_docs->pagibig->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'philhealth')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkPhilhealth" name="requested_documents[philhealth][name]" value="Certificate of Premium Contributions – Philhealth" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkPhilhealth">
                                Certificate of Premium Contributions – Philhealth
                            </label>
                        </div>

                        <div class="" id="dvPhilhealth" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[philhealth][duration]">
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'Latest 6 months') selected="selected" @endif value="Latest 6 months">Latest 6 months</option>
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'Latest 12 months') selected="selected" @endif value="Latest 12 months">Latest 12 months</option>
                                    <option @if(@$request->cbrd_req_docs->philhealth->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'sss')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkSss" name="requested_documents[sss][name]" value="Certificate of Premium Contributions – SSS"/>
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkSss">
                                Certificate of Premium Contributions – SSS
                            </label>
                        </div>

                        <div class="" id="divSss" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">Duration:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[sss][duration]">
                                    <option @if(@$request->cbrd_req_docs->sss->duration == '12 months and below') selected="selected" @endif value="12 months and below">12 months and below</option>
                                    <option @if(@$request->cbrd_req_docs->sss->duration == 'All') selected="selected" @endif value="All">All</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'coe')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkCoe" name="requested_documents[coe][name]" value="Certificate of Employment" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoe">
                                Certificate of Employment
                            </label>
                        </div>

                        <div id="divCoe" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" name="requested_documents[coe][number_of_copies]" value="{{ @$request->cbrd_req_docs->coe->number_of_copies }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'coec')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkCoec" name="requested_documents[coec][name]" value="Certificate of Employment with Compensation" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkCoec">
                                Certificate of Employment with Compensation
                            </label>
                        </div>

                        <div id="divCoec" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">No. of Copies:</label>
                            </div>
                            <div class="textbox_sm">
                                <input type="text" class="form-control" name="requested_documents[coec][number_of_copies]" value="{{ @$request->cbrd_req_docs->coec->number_of_copies }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'clearance_certification')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[clearance_certification][name]" value="Clearance Certification" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Clearance Certification (for resigned employees) – <i class="itext">Released only upon receipt of final pay from Accounting Department</i>
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'itr')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[itr][name]" value="Income Tax Return" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Income Tax Return
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'pagibig_esav')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[pagibig_esav][name]" value="Pag-IBIG Employee Statement of Accumulated Value (ESAV)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Pag-IBIG Employee Statement of Accumulated Value (ESAV) (for housing loan application)
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'ph_claim')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[ph_claim][name]" value="Philhealth Claim Form 1" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Claim Form 1
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'ph_mdr')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[ph_mdr][name]" value="Philhealth Member Data Record (MDR)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Philhealth Member Data Record (MDR)
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
						<?php if(Session::get('desig_level') != "employee" && Session::get('desig_level') != "supervisor"):?>
							@if(key($request->cbrd_req_docs) == 'copy_of_201_file')
							<div class="col_checkbox">
								<input checked="checked" type="checkbox" name="requested_documents[copy_of_201_file][name]" value="Copy of 201 File" />
							</div>
							<div class="checkbox_label">
								<label class="labels">
									Copy of 201 File
								</label>
							</div>
							<div class="clear_10"></div>
							@endif
						<?php endif; ?>
                        
                        @if(key($request->cbrd_req_docs) == 'emaf')
                        <div class="col_checkbox">
							<input checked="checked" type="checkbox" id="chkEmaf" name="requested_documents[emaf][name]" value="Employee Movement Action Form (EMAF)" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkEmaf">
                                Employee Movement Action Form (EMAF)
                            </label>
                        </div>

                        <div id="divEmaf" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">Nature of Movement:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]">
                                    <option value="Transfer" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
                                    <option value="Promotion" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
                                    <option value="Temporary Assignment" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
                                    <option value="Change of Position Title" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
                                    <option value="Salary Adjustment" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
                                    <option value="Others" id="emaf_others" @if(@$request->cbrd_req_docs->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
                                </select>
                            </div>

                            <div class="extra_labels">
                                <label class="labels">Effectivity Date:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" style="width: 241px" class="form-control date_picker" name="requested_documents[emaf][effectivity_date]" value="{{ @$request->cbrd_req_docs->emaf->effectivity_date }}" />
                            </div>
                            <div class="clear_10"></div>
                            <div class="tbl">
                                <table border = "1" cellpadding = "0" class="tbl">
                                    <th class="th_style">DETAILS</th>
                                    <th class="th_style">PRESENT STATUS</th>
                                    <th class="th_style">PROPOSED STATUS</th>
                                    <th class="th_style">REMARKS</th>

                                    <tr>
                                        <td class="td_style">Company</td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ @$request->cbrd_req_docs->emaf->table[0]->{1} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ @$request->cbrd_req_docs->emaf->table[0]->{2} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ @$request->cbrd_req_docs->emaf->table[0]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Department</td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ @$request->cbrd_req_docs->emaf->table[1]->{1} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ @$request->cbrd_req_docs->emaf->table[1]->{2} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ @$request->cbrd_req_docs->emaf->table[1]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Designation</td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ @$request->cbrd_req_docs->emaf->table[2]->{1} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ @$request->cbrd_req_docs->emaf->table[2]->{2} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ @$request->cbrd_req_docs->emaf->table[2]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Job Level</td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ @$request->cbrd_req_docs->emaf->table[3]->{1} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ @$request->cbrd_req_docs->emaf->table[3]->{2} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ @$request->cbrd_req_docs->emaf->table[3]->{3} }}" /></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style">Salary Rate</td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ @$request->cbrd_req_docs->emaf->table[4]->{1} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ @$request->cbrd_req_docs->emaf->table[4]->{2} }}" /></td>
                                        <td><input type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ @$request->cbrd_req_docs->emaf->table[4]->{3} }}" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'emp_attendance')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" id="chkAttendance" name="requested_documents[emp_attendance][name]" value="Employee Attendance" />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels" for="chkAttendance">
                                Employee Attendance
                            </label>
                        </div>

                        <div id="divAttendance" style="display: block">
                            <div class="extra_labels">
                                <label class="labels">From:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][from]" value="{{ @$request->cbrd_req_docs->emp_attendance->from }}" />
                            </div>
                            <br/>
                            <div class="clear_10"></div>
                            <div class="extra_labels">
                                <label class="labels">To:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control date_picker" name="requested_documents[emp_attendance][to]" value="{{ @$request->cbrd_req_docs->emp_attendance->to }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                        
                        @if(key($request->cbrd_req_docs) == 'others')
                        <div class="col_checkbox">
                            <input checked="checked" type="checkbox" name="requested_documents[others][name]" value="Others"  />
                        </div>
                        <div class="checkbox_label">
                            <label class="labels">
                                Others:  <input type="text" name="requested_documents[others][value]" value="{{ $request->cbrd_req_docs->others->value }}" />
                            </label>
                        </div>
                        <div class="clear_10"></div>
                        @endif
                    </div>-->
                    
                    <table border = "1" cellpadding = "0" class="tbl3">
						<th class="th_style td_height">Document</th>
						<th class="th_style td_height">Reference No.</th>
						<!--<th class="th_style td_height">Assigned To</th>-->
						<th class="th_style td_height">CBR Remarks</th>
						<tr>
							<td class="td_style td_height">{{ $request->cbrd_req_docs->{key($request->cbrd_req_docs)}->name }}</td>
							<td class="td_style td_height">{{ $request->cbrd_ref_num }}</td>
							<!--<td class="td_style td_height">{{ EmployeesHelper::getEmployeeNameById($request->cbrd_assigned_to) }}</td>-->
							<td class="td_style td_height">{{ $request->cbrd_remarks }}</td>
						</tr>
					</table>

                    <div class="clear_20"></div>

                    <label class="form_label required">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea rows="4" cols="50" class="form-control textarea_width" name="cb_purpose_of_request">{{ $request->cb->cb_purpose_of_request }}</textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: 5000)</strong> Characters left: 4966
                    </label>

                    <div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <!--<div class="attachment_container">
                    	@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
                    		<div>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/') . '/' . $request->cb->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a> | <a href="javascript:void(0);" class="delete_file">Delete</a><br />
                    		</div>
                    		@endforeach
                    	@endif
                    	<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>
                    	<div id="attachments"></div>
                    </div>-->
					<div class="attachment_container">
                    	@if(count(json_decode($request['cb']['cb_attachments'])) > 0)
                    		@foreach(json_decode($request['cb']['cb_attachments']) as $attachment)
                    		<div>
                    		<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
                    		<a href="{{ URL::to('/cbr/download/'). '/' . $request->cb->cb_ref_num . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
							<button class='remove-fn btn btn-xs btn-danger btn_del'>DELETE</button><br />
                    		</div>
                    		@endforeach
                    	@endif
						<span class="btn btn-success btnbrowse fileinput-button">
							<span>BROWSE</span>
							<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
						</span>
                   
                    	<div id="attachments"></div>
                    </div>

            </div><!-- end of form_container -->
            
            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                	<div class="row">
                		<label class="textarea_inside_label">MESSAGE:</label>
                		<textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['cb']['cb_comments'] != '') @foreach(json_decode($request['cb']['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
                	</div>
                </div>

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script type="text/javascript">
	var allowed_file_count = 5;
	var allowed_total_filesize = 20971520;
    $(function () {
        // $(".delete_file").click(function(){
            // $(this).parent("div").remove()
        // });
        
		$(".date_picker").datepicker({ format: 'yyyy-mm-dd' });
		
		// $('.remove_file_attachment').live('click', function() {
			// $(this).parent().remove();
		// });
		
		$(".remove-fn").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$("#btnCbrAddAttachment").click(function(){
			if($("#attachments input:file").length < 5){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment btn-danger' type='button'>DELETE</button></div>");
			}
		});
		
        $("#chkPhilhealth").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPhilhealth").show();
            } else {
                $("#dvPhilhealth").hide();
            }
        });

        $("#chkPagibig").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#dvPagibig").show();
            } else {
                $("#dvPagibig").hide();
            }
        });

        $("#chkSss").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divSss").show();
            } else {
                $("#divSss").hide();
            }
        });

        $("#chkCoe").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoe").show();
            } else {
                $("#divCoe").hide();
            }
        });

        $("#chkCoec").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divCoec").show();
            } else {
                $("#divCoec").hide();
            }
        });

        $("#chkEmaf").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divEmaf").show();
            } else {
                $("#divEmaf").hide();
            }
        });

        $("#chkAttendance").click(function () {
            if ($(this).is(":checked")) {
                console.log('checked');
                $("#divAttendance").show();
            } else {
                $("#divAttendance").hide();
            }
        });

    });
</script>

@stop