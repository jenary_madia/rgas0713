@extends('template/header')

@section('content')

<form method="post" action="{{ URL::to('cbr/process/'.$request['cbrd_ref_num']) }}">	
	<input name="cbrd_id" type="hidden" value="{{ $request['cbrd_id'] }}"/>
    <div class="form_container">
		<label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>
			@include('cbr.partials.request_info', $request)

			<div class="clear_20"></div>

			@include('cbr.partials.requested_docs_process', $request)

			<div class="clear_20"></div>

			@include('cbr.partials.purpose_of_request', $request)

			<div class="clear_20"></div>
			
			@include('cbr.partials.attachments', array('browse'=>0))

	</div><!-- end of form_container -->

	<div class="clear_20"></div>
	<div class="form_container">
	
		@include('cbr.partials.messages', $request['cb'])
	
		<div class="clear_10"></div>
		
		@include('cbr.partials.commentbox', $request['cb'])

		<div class="clear_10"></div>
		<div class="row">
			<div class="clear_10"></div>
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes">FORWARD TO REQUESTOR FOR ACKNOWLEDGEMENT</label>
				</div> 

				<div class="comment_button" style="padding-left:100px;">
                    <button type="submit" class="btn btn-default btndefault"  style="width:185px" >FOR ACKNOWLEDGEMENT</button>
                </div>
			</div>

			<div class="clear_20"></div>
			<div class="text-center">
				<a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
			</div>
		</div>
	</div><!-- end of form_container -->
    <div class="clear_60"></div>
</form>
@stop
@section('js_ko')
<script type="text/javascript">
    $(function () {
		// var date = new Date();
        // date.setDate(date.getDate()-1);
		// $(".date_picker").datepicker({ format: 'yyyy-dd-mm', startDate: date });
		
		//$(".date_picker").datepicker({ format: 'yyyy-mm-dd'});

		//JMA 0329 CHANGE DATE FORMAT
		$('.date_picker').datepicker({
			dateFormat : 'yy-mm-dd',
			 maxDate: 0 
		});	
	});
</script>
@stop