@extends('template/header')

@section('content')
<link href="<?php echo $base_url; ?>/assets/css/builds/new_style.css" rel="stylesheet">
<div id="wrap">
	@include('template/sidebar')
    <div class="container">
        <form class="form-inline">
            <div class="form_container">
                <label class="form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control text-left" placeholder="yyyy-mm-dd" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" placeholder="Returned"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Section:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Urgency:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Needed:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 241px">
                                    <option value="">----------</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label">REQUESTED DOCUMENTS:</label>

                    <table border = "1" cellpadding = "0" class="tbl3">
                        <th class="th_style td_height">Document</th>
                        <th class="th_style td_height">Reference No.</th>
                        <th class="th_style td_height">CBR Remarks</th>

                        <tr>
                            <td class="td_style td_height">Certificate of Employment with Compensation</td>
                            <td class="td_style td_height">2014-00550-2</td>
                            <td class="td_style td_height">CBR 08/05/2014 11:30 AM: Please attach pay slip</td>
                        </tr>
                    </table>


                    <div class="clear_20"></div>

                    <label class="form_label">PURPOSE OF REQUEST:</label>
                    <div class="textarea">
                        <textarea name="purpose_request" rows="4" cols="50" class="form-control textarea_width"> </textarea>
                    </div>
                    <label class="form_note">
                        <strong>(Maximum characters: 5000)</strong> Characters left: 4966
                    </label>


                    <div class="clear_20"></div>
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="submit" class="btn btnbrowse btn-default" value="">BROWSE</button><br/>
                        <div class="sample_file">SAMPLE 1 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
                        <div class="sample_file">SAMPLE 2 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
                        <div class="sample_file">SAMPLE 3 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" placeholder="DEBORAH METRA 08/04/2014 8:30 AM: Comment of requestor."></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" value="">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CHRD FOR PROCESSING</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" value="">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
        <div class="clear_20"></div>
    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop