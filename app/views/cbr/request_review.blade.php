@extends('template/header')

@section('content')    
	
	<div class="form_container">
		<label class="cbr_form_title">COMPENSATION, BENEFITS AND RECORDS REQUEST FORM xxx</label>

			<div class="row">
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">EMPLOYEE NAME:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_name'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">REFERENCE NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_ref_num'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">EMPLOYEE NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_emp_id'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">DATE FILED:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control text-left" value="{{ $request['cb_date_filed'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">COMPANY:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_company'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">STATUS:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_status'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">DEPARTMENT:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_department'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">CONTACT NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_contact_no'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels ">SECTION:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_section'] }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">URGENCY:</label>
					</div>
					<div class="col2_form_container">
						<input readonly type="text" class="form-control" value="{{ $request['cb_urgency'] }}" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="col3_form_container">
					<div class="col1_form_container">
						@if($request['cb_urgency'] == 'Immediate Attention Needed')
							<label class="labels required">DATE NEEDED:</label>
						@else
							<label class="labels">DATE NEEDED:</label>
						@endif
					</div>
					<div class="col2_form_container input-group">
						<input readonly type="text" class="form-control" value="{{ $request['cb_date_needed'] }}" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div>
			</div>

			<div class="clear_20"></div>

			<label class="form_label required">REQUESTED DOCUMENTS:</label>
			@if(!$view)
			<table border = "1" cellpadding = "0" class="tbl2">
				<th class="th_style td_height">Document</th>
				<th class="th_style td_height">Reference No.</th>
				<th class="th_style td_height">Assigned To</th>
				<th class="th_style td_height">Date Assigned</th>
				<th class="th_style td_height">Action</th>
				<th class="th_style td_height">CBR Remarks</th>
				@foreach($request['cbrd'] as $cbrd)
				<form action="{{ url('cbr/review-request/'.$request['cb_ref_num']); }}" method="post">
				<input type="hidden" value="{{ csrf_token() }}">
				<input type="hidden" name="cbrd_id" value="{{ $cbrd->cbrd_id }}">
				<input type="hidden" name="cbrd_ref_num" value="{{ $cbrd->cbrd_ref_num }}">
				<input type="hidden" name="cbrd_cb_id" value="{{ $cbrd->cbrd_cb_id }}">
				<tr>
					<td class="cbr_review_td_style td_height" style="width: 230px">
						@if(key(json_decode($cbrd->cbrd_req_docs)) == 'emaf')
							<a href="#" data-toggle="modal" data-target="#emafModal">{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->name }}</a>
							<!-- Modal -->
							<div id="emafModal" class="modal fade" role="dialog" data-rel="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
								  		<div class="modal-header">
											Employment Movement Action Form (EMAF) <button type="button" class="close" data-dismiss="modal">&times;</button>
								  		</div>
								  		<div class="modal-body">
											<div id="divEmaf" style="display: block">
	                            				<div class="extra_labels">
	                                				<label class="labels">Nature of Movement:</label>
	                            				</div>
	                            			<div class="col2_form_container">
				                                <select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" disabled>
				                                    <option value="Transfer" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
				                                    <option value="Promotion" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
				                                    <option value="Temporary Assignment" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
				                                    <option value="Change of Position Title" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
				                                    <option value="Salary Adjustment" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
				                                    <option value="Others" id="emaf_others" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
				                                </select>
	                            			</div>
	                            			<div class="clear_10"></div>
											@if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Others")
											<div class="extra_labels">
												<label class="labels">Others:</label>
											</div>
			                                <div class="col2_form_container">
			                                    <input disabled type="text" class="form-control" name="requested_documents[emaf][others]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->others }}">  
			                                </div>
			                                <div class="clear_10"></div>
											@endif
				                            <div class="extra_labels">
				                                <label class="labels">Effectivity Date:</label>
				                            </div>
				                            <div class="col2_form_container input-group">
				                                <input disabled type="text" class="form-control date_picker" id="effdate" name="requested_documents[emaf][effectivity_date]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->effectivity_date }}" />
				                            	<label class="input-group-addon btn" for="effdate">
												   <span class="glyphicon glyphicon-calendar"></span>
												</label>
							                </div>
				                            <div class="clear_10"></div>
	                            			<div class="tbl-responsive">
	                                			<table border = "1" cellpadding = "0" class="">
				                                    <th class="th_style">DETAILS</th>
				                                    <th class="th_style">PRESENT STATUS</th>
				                                    <th class="th_style">PROPOSED STATUS</th>
				                                    <th class="th_style">REMARKS</th>
	
				                                    <tr>
				                                        <td class="td_style">Company</td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{1} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{2} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{3} }}" /></td>
				                                    </tr>
				                                    <tr>
				                                        <td class="td_style">Department</td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{1} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{2} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{3} }}" /></td>
				                                    </tr>
				                                    <tr>
				                                        <td class="td_style">Designation</td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{1} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{2} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{3} }}" /></td>
				                                    </tr>
				                                    <tr>
				                                        <td class="td_style">Job Level</td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{1} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{2} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{3} }}" /></td>
				                                    </tr>
				                                    <tr>
				                                        <td class="td_style">Salary Rate</td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{1} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{2} }}" /></td>
				                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{3} }}" /></td>
				                                    </tr>
	                                			</table>
	                            			</div>
	                        			</div>
								  	</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" id="tep_add_action" data-dismiss="modal">OK</button>
									</div>
								</div>
							  </div>
							</div>
						@else
							{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->name }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->duration)
						<br/>Duration: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->duration }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->number_of_copies)
						<br/>No. of Copies: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->number_of_copies }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->from)
						<br/>From: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->from }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->to)
						<br/>To: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->to }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->value)
						<br/>{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->value }}
						@endif
					</td>
					<td class="cbr_review_td_style td_height">{{ $cbrd->cbrd_ref_num }}</td>
					<td class=" td_height" style="width: 170px">

						<select name="emp_id" class="form-control">
							<option value=""> - </option>
							@foreach($chrd as $_chrd)
								@if($cbrd->cbrd_assigned_to == $_chrd['id']) <option selected="selected" value="{{ $_chrd['id'] }}">{{ $_chrd['firstname'] }} {{ $_chrd['lastname'] }}</option> @endif
								<option value="{{ $_chrd['id'] }}">{{ $_chrd['firstname'] }} {{ $_chrd['lastname'] }}</option>
							@endforeach
						</select>

					</td>
					@if($cbrd->cbrd_assigned_date == '0000-00-00')
						<td class="cbr_review_td_style td_height"></td>
					@else
						<td class="cbr_review_td_style td_height">{{ $cbrd->cbrd_assigned_date }}</td>
					@endif
					<td class="cbr_review_td_style border" >
						<div class="btn-group" role="group">
							<button type="submit" class="btn btn-sm btn-default" name="action" value="assign" >ASSIGN</button>
							@if($cbrd->cbrd_assigned_to != "")
							<button type="submit" disabled class="btn btn-sm btn-default" name="action" value="return">RETURN</button>
							@else
							<button type="submit" class="btn btn-sm btn-default" name="action" value="return">RETURN</button>
							@endif
						<div>
					</td>
					<td class="cbr_review_td_style td_height">
						@if($cbrd->cbrd_remarks == "")
							@if($cbrd->cbrd_assigned_to != "")
							<input type="text" readonly name="cbrd_remarks" />
							@else
							<input type="text" name="cbrd_remarks" />
							@endif
						@else
							@if($cbrd->cbrd_assigned_to != "")
							<input type="text" readonly name="cbrd_remarks" value="{{ $cbrd->cbrd_remarks }}" />
							@else
							<input type="text" name="cbrd_remarks" value="{{ $cbrd->cbrd_remarks }}" />
							@endif
						@endif
					</td>
				</tr>
				</form>
				@endforeach
			</table>
			@else
			<table border = "1" cellpadding = "0" class="tbl2">
				<th class="th_style td_height">Document</th>
				<th class="th_style td_height">Reference No.</th>
				<th class="th_style td_height">Assigned To</th>
				<th class="th_style td_height">Date Assigned</th>
				<th class="th_style td_height">CBR Remarks</th>
				@foreach($request['cbrd'] as $cbrd)
				<tr>
					<td class="td_style td_height">
						@if(key(json_decode($cbrd->cbrd_req_docs)) == 'emaf')
							<a href="#" data-toggle="modal" data-target="#emafModal">{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->name }}</a>
							<!-- Modal -->
							<div id="emafModal" class="modal fade" role="dialog" data-rel="dialog">
								<div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
								  		<div class="modal-header">
											Employment Movement Action Form (EMAF)<button type="button" class="close" data-dismiss="modal">&times;</button>
								  		</div>
								  		<div class="modal-body">
											<div id="divEmaf" style="display: block">
	                            				<div class="extra_labels">
	                                				<label class="labels">Nature of Movement:</label>
	                            				</div>
		                            			<div class="col2_form_container">
						                                <select class="form-control" style="width: 241px" name="requested_documents[emaf][nature_of_movement]" disabled>
					                                    <option value="Transfer" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Transfer") selected="selected" @endif>Transfer</option>
					                                    <option value="Promotion" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Promotion") selected="selected" @endif>Promotion</option>
					                                    <option value="Temporary Assignment" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Temporary Assignment") selected="selected" @endif>Temporary Assignment</option>
					                                    <option value="Change of Position Title" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Change of Position Title") selected="selected" @endif>Change of Position Title</option>
					                                    <option value="Salary Adjustment" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Salary Adjustment") selected="selected" @endif>Salary Adjustment</option>
					                                    <option value="Others" id="emaf_others" @if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Others") selected="selected" @endif>Others</option>
					                                </select>
		                            			</div>
		                            			<div class="clear_10"></div>
												@if(@json_decode($cbrd->cbrd_req_docs)->emaf->nature_of_movement == "Others")
												<div class="extra_labels">
													<label class="labels">Others:</label>
												</div>
				                                <div class="col2_form_container">
				                                    <input type="text" class="form-control" name="requested_documents[emaf][others]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->others }}" disabled>  
				                                </div>
				                                <div class="clear_10"></div>
												@endif
					                            <div class="extra_labels">
					                                <label class="labels">Effectivity Date:</label>
					                            </div>
					                            <div class="col2_form_container input-group">
					                                <input type="text" class="form-control date_picker" id="effdate" name="requested_documents[emaf][effectivity_date]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->effectivity_date }}" disabled/>
					                            	<label class="input-group-addon btn" for="effdate">
													   <span class="glyphicon glyphicon-calendar"></span>
													</label>
					                            </div>
					                            <div class="clear_10"></div>
		                            			<div class="tbl-responsive">
		                                			<table border = "1" cellpadding = "0" class="">
					                                    <th class="th_style">DETAILS</th>
					                                    <th class="th_style">PRESENT STATUS</th>
					                                    <th class="th_style">PROPOSED STATUS</th>
					                                    <th class="th_style">REMARKS</th>
		
					                                    <tr disabled>
					                                        <td class="td_style">Company</td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{1} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{2} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][0][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[0]->{3} }}" /></td>
					                                    </tr>
					                                    <tr>
					                                        <td class="td_style">Department</td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{1} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{2} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][1][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[1]->{3} }}" /></td>
					                                    </tr>
					                                    <tr>
					                                        <td class="td_style">Designation</td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{1} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{2} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][2][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[2]->{3} }}" /></td>
					                                    </tr>
					                                    <tr>
					                                        <td class="td_style">Job Level</td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{1} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{2} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][3][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[3]->{3} }}" /></td>
					                                    </tr>
					                                    <tr>
					                                        <td class="td_style">Salary Rate</td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][1]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{1} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][2]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{2} }}" /></td>
					                                        <td><input disabled type="text" class="form-control" id="" name="requested_documents[emaf][table][4][3]" value="{{ @json_decode($cbrd->cbrd_req_docs)->emaf->table[4]->{3} }}" /></td>
					                                    </tr>
	                                				</table>
	                            				</div>
	                        				</div>
								  		</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" id="tep_add_action" data-dismiss="modal">OK</button>
										</div>
								</div>
							  </div>
							</div>
						@else
							{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->name }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->duration)
						<br/>Duration: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->duration }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->number_of_copies)
						<br/>No. of Copies: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->number_of_copies }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->from)
						<br/>From: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->from }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->to)
						<br/>To: {{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->to }}
						@endif
						
						@if(@json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->value)
						<br/>{{ json_decode($cbrd->cbrd_req_docs)->{key(json_decode($cbrd->cbrd_req_docs))}->value }}
						@endif
					</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_ref_num }}</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_assigned_to }}</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_assigned_date }}</td>
					<td class="td_style td_height">{{ $cbrd->cbrd_remarks }}</td>
				</tr>
				@endforeach
			</table>
			@endif
			<div class="clear_20"></div>

			<label class="form_label required">PURPOSE OF REQUEST:</label>
			<div class="textarea">
				<textarea readonly rows="4" cols="50" class="form-control textarea_width" placeholder="<Purpose of the request goes here>">{{ $request['cb_purpose_of_request'] }}</textarea>
			</div>

			<div class="clear_20"></div>
			<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
			<label class="attachment_note"><strong>ATTACHMENTS:</strong></label><br/>
			<div class="attachment_container">
				@if(count(json_decode($request['cb_attachments'])) > 0)
					@foreach(json_decode($request['cb_attachments']) as $attachment)
					<a href="{{ URL::to('/cbr/download/') . '/' . $request['cb_ref_num'] . '/' . $attachment->random_filename .'/' . $attachment->original_filename }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
					@endforeach
				@endif
			</div>

	</div><!-- end of form_container -->

	<div class="clear_20"></div>

	<div class="form_container">
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea readonly name="purpose_request" rows="3" class="form-control textarea_inside_width">@if( $request['cb_comments'] != '') @foreach(json_decode($request['cb_comments']) as $comment) {{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10; @endforeach @endif</textarea>
			</div>
		</div>   

		<div class="clear_20"></div>
			<div class="text-center">
				<a class="btn btn-default back_buttons_spacing" href="{{ URL::to('cbr/submitted-cbr-requests') }}">BACK</a>
			</div>             
	</div><!-- end of form_container -->

	
	<div class="clear_20"></div>
@stop