<html>
<head></head>
<body>
Good day!
<br /><br />
You have 1 CBR Request for acknowledgement.
<br /><br />
STATUS: {{ $status }}<br />
REFERENCE NUMBER: {{ $ref_num }}<br />
ATTENDING HR STAFF: {{ $cbr_staff }}<br />
<br /><br />
Click here to <a href="{{ URL::to('/') }}">log-in</a> and check the file.<br />
<br /><br />
DO NOT REPLY. THIS IS A SYSTEM-GENERATED MESSAGE.<br />
Service made possible by CSMD - Application Development.
</body>
<html>
