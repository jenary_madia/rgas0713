@extends('template/header')

@section('content')
    {{ Form::open(array('url' => 'submitted/pe_print', 'method' => 'post', 'id' => 'receiverSearchPE' )) }}
    <div class="form_container" id="divPEReceiver" v-cloak>
        <h5 class="text-center"><strong>SUBMITTED LEAVE AND NOTIFICATION</strong></h5>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DEPARTMENT</label>
                </div>
                <div class="col2_form_container">
                    {{ Form::select('department',$departments, Input::old('department', 0), ['class'=>'form-control otherFields','id'=>'schedule_typeOffset','v-model'=>'departmentID']) }}
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">SECTION</label>
                </div>

                <input type="text" value="{{ Input::old('section') }}" v-model="sectionID" hidden>

                <div class="col2_form_container">
                    <select class="form-control" :disabled="sections.length == 0" v-model="sectionID" name="section">
                        <option v-for="section in sections" value="@{{ section.id }}">@{{ section.sect_name }}</option>
                    </select>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DATE:</label>
                </div>
                <div class="col2_form_container">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" value="{{ Input::old("dateFrom") }}" name="dateFrom" id="dateFrom" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                    <br>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text" value="{{ Input::old("dateTo") }}" name="dateTo" id="dateTo" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">MODULE:</label>
                </div>
                <div class="col2_form_container">
                    {{ Form::checkbox('application[]', "leaves", Input::old('application.0', true))  }} Leave
                    {{ Form::checkbox('application[]', "notifications", Input::old('application.1', true))  }} Notification
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <button class="btn btn-default btndefault pull-right" style="margin-right: 10px;" id="btnPrint">PRINT</button>
                <button class="btn btn-default btndefault pull-right" style="margin-right: 10px;" id="btnSearchPE">SEARCH</button>
            </div>
        </div>
    </div><!-- end of form_container -->
    {{ Form::close() }}


    @include('submitted_leaves_notifications.pe_data_tables.leaves')
    @include('submitted_leaves_notifications.pe_data_tables.notification')


    <hr />

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/bootstrap-multiselect.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/submitted_leaves_notifications/vue-pe.js') }}
    {{ HTML::script('/assets/js/submitted_leaves_notifications/leaves_notifs.js') }}
@stop