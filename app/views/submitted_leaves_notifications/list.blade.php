@extends('template/header')

@section('content')
    {{ Form::open(array('url' => 'submitted/print', 'method' => 'post', 'files' => true ,'id' => 'receiverSearch')) }}
        <div class="form_container">
        <div class="row">
            <h4 class="text-center">Submitted Leave and Notification</h4>
            <hr />
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required text-uppercase">Department:</label>
                </div>
                <div class="col2_form_container">
                    {{ Form::select('department', $departments, Input::old('department', 'all'), ['id' => 'notificationType','class'=> 'form-control' ]) }}
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels text-uppercase">Last Name:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" name="lastName" value="{{ Input::old('lastName') }}" />
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required text-uppercase">Date:</label>
                </div>
                <div class="col2_form_container">
                    <div class="input-group bootstrap-timepicker timepicker pull-left"  style="width: 100px;">
                        <input type="text" value="{{ Input::old("dateFrom") }}" name="dateFrom" id="dateFrom" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                    <div class="input-group bootstrap-timepicker timepicker pull-left"  style="width: 100px; margin-left: 10px;">
                        <input type="text" value="{{ Input::old("dateTo") }}" name="dateTo" id="dateTo" class="date_picker form-control input-small">
                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels text-uppercase">First Name:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" name="firstName" value="{{ Input::old('firstName') }}" />
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required text-uppercase">Module:</label>
                </div>
                <div class="col2_form_container">
                    <input type="checkbox" class="pull-left" id="leave" checked value="leave" style="width: 50px;" name="module[]" />
                    <span class="pull-left">Leave</span>
                    <input type="checkbox" class="pull-left" id="notif" checked value="notification" style="width: 50px;" name="module[]" />
                    <span class="pull-left">Notification</span>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels text-uppercase">Middle Initial:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" name="middleInitial" value="{{ Input::old('middleInitial') }}" />
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">

                <div class="col1_form_container">
                    <label class="labels required text-uppercase">Leave Type:</label>
                </div>
                <div class="col2_form_container">
                    <div class="col2_form_container">
                        <input type="checkbox" id="leaveAll" class="pull-left" style="width: 50px;" checked name="leaveType[]" />
                        <span class="pull-left">All</span>
                    </div>
                    @foreach($leaveTypes as $leaveType)
                        <div class="col2_form_container">
                            <input type="checkbox"  checked class="pull-left leaveType" style="width: 50px;" checked name="leaveType[]" value="{{ $leaveType->item }}" />
                            <span class="pull-left">{{ $leaveType->text }}</span>
                        </div>
                    @endforeach
                </div>
            </div>

	            <div class="clear_10"></div>
		            <div class="row_form_container">
		                <div class="col1_form_container">
		                    <label class="labels required text-uppercase">Notification Type:</label>
		                </div>
						<div class="col2_form_container">
			                <div class="col2_form_container">
			                    <input type="checkbox"  checked class="pull-left" id="notifAll" style="width: 50px;" name="notificationType[]" />
			                    <span class="pull-left">All</span>
			                </div>
			                @foreach($notificationTypes as $notificationType)
                                <div class="col2_form_container">
                                    <input type="checkbox" checked class="pull-left notifType" style="width: 50px;" value="{{ $notificationType->item }}" name="notificationType[]" />
                                    <span class="pull-left">{{ $notificationType->text }}</span>
                                </div>
							@endforeach
                    </div>
                </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <button class="btn btn-default btndefault pull-right" style="margin-right: 10px;" id="btnPrint">PRINT</button>
                <button class="btn btn-default btndefault pull-right" style="margin-right: 10px;" id="btnSearch">SEARCH</button>
            </div>

        </div>


    </div><!-- end of form_container -->
    {{ Form::close() }}


    @include('submitted_leaves_notifications.data_tables.leaves')
    @include('submitted_leaves_notifications.data_tables.notification_undertime')
    @include('submitted_leaves_notifications.data_tables.notification_offset')
    @include('submitted_leaves_notifications.data_tables.notification_timekeeping')
    @include('submitted_leaves_notifications.data_tables.notification_official')
    @include('submitted_leaves_notifications.data_tables.notification_cuttime')

    <hr />

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/submitted_leaves_notifications/leaves_notifs.js') }}
    <script type="text/javascript">
        $(document).on('ready',function(){
            $('.date_picker').datepicker({
                dateFormat : 'yy-mm-dd',
                beforeShowDay : function(date) {
                    var day = date.getDay();
                    return [(day != 6 && day != 0)];
                }
            });

            $("#leave").on("change" , function () {
                if($(this).is(":checked")) {
                    $("#leaveAll").prop("checked",true);
                    $(".leaveType").prop("checked",true);
                }else{
                    $("#leaveAll").prop("checked",false);
                    $(".leaveType").prop("checked",false);
                }
            });

            $("#notif").on("change" , function () {
                if($(this).is(":checked")) {
                    $("#notifAll").prop("checked",true);
                    $(".notifType").prop("checked",true);
                }else{
                    $("#notifAll").prop("checked",false);
                    $(".notifType").prop("checked",false);
                }
            });

            $("#leaveAll").on("change" , function () {
                if($(this).is(":checked")) {
                    $(".leaveType").prop("checked",true)
                }else{
                    $(".leaveType").prop("checked",false)
                }
            });

            $(".leaveType").on("change" , function () {
                if(! $(this).is(":checked")) {
                    $("#leaveAll").prop("checked",false)
                }
            });

            $("#notifAll").on("change" , function () {
                if($(this).is(":checked")) {
                    $(".notifType").prop("checked",true)
                }else{
                    $(".notifType").prop("checked",false)
                }
            });

            $(".notifType").on("change" , function () {
                if(! $(this).is(":checked")) {
                    $("#notifAll").prop("checked",false)
                }
            });

        });
    </script>
@stop