<div class="clear_20"></div>
{{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formProcessLeave')) }}
<div class="datatable_holder">
    <span class="list-title text-uppercase">Submitted Notification - Cut Time</span>
    <table id="notif_cuttime" cellpadding="0" cellspacing="0" border="0" class="display dataTable submittedEntries" width="100%" aria-describedby="notif_cuttime">
        <thead>
        <tr role="row">
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Approved</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Employee Name</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Total Number of Days</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Total Number of Hours</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Date of Cut Time</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Reason</th>
            <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
        </tr>
        </thead>
    </table>
</div>
{{ Form::close() }}