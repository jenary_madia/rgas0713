@extends('template.header')

@section('content')
    <div id="peISView" v-cloak>
    <div class="form_container">
        <h5 class="text-center"><strong>NOTIFICATION SLIP</strong></h5>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">DEPARTMENT</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{ $notification['department']['dept_name'] }}">
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">REFERENCE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" name="reference" readonly value="{{ $notification['documentcode'].'-'.$notification['codenumber'] }}">
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">SECTION</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{ $notification['section']['sect_name'] }}">
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">DATE FILED:</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{ $notification['datecreated'] }}">
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">Employee Name</th>
                            <th class="text-center">Employee Number</th>
                            <th class="text-center">Notification Type</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Reason</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="employee in employeesToFile"  v-bind:class="{ 'active' : employee.active }" @click="chooseEmployee($index,employee)">
                            <td>@{{ employee.employee_name }}</td>
                            <td>@{{ employee.employee_number }}</td>
                            <td>@{{ employee.notif.text }}</td>
                            <td>@{{ employee.from_date }}</td>
                            <td>@{{ employee.reason }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear_10"></div>
            </div>
        </div>
    </div><!-- end of form_container -->
    <div class="clear_20"></div>
    {{ Form::open(array('url' => array('submitted/pe_notification',$notification['id']), 'method' => 'post', 'files' => true)) }}
    <span class="action-label labels">ACTION</span>
    <div class="form_container">
            <div class="clear_20"></div>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach ( explode('|',$notification['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="col-md-12 text-center">
                    @if( $method == 'process')
                        <button class="text-center btn btn-default btndefault btnReceiver" name="action" value="return">RETURN TO FILER</button>
                        <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                        <button  type="submit" class="btn btn-default btndefault btnReceiver" name="action" value="cancel">CANCEL NOTIFICATION</button>
                    @else
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                    @endif
                </div>
            </div>
        </div><!-- end of form_container -->

    {{ Form::close() }}
</div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/notification/vue-pe-ISview.js') }}

@stop