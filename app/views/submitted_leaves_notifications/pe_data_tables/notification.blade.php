
    <div class="clear_20"></div>
    {{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formProcessLeave')) }}
    <div class="datatable_holder"
         @if (Session::has("application"))
            @if(! in_array('notifications',Session::get("application")))
                hidden
            @endif
        @endif
    >
        <span class="list-title">Submitted Notifications</span>
        <table id="PE_receiver_notif" cellpadding="0" cellspacing="0" border="0" class="display dataTable submittedEntries" width="100%" aria-describedby="PE_receiver_notif">
            <thead>
            <tr role="row">
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Approved</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Department</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Section</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                <th style="text-align:left;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
            </tr>
            </thead>
        </table>
    </div>
    {{ Form::close() }}
