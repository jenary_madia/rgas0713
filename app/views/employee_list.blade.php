@extends('template/header')

@section('content')

<div id="wrap">

	@include('template/sidebar')
    
    <div class="container">
    
        <div id="employee_filter_con">
            
            <div class="leftfloat">
                <label>Company:</label>
                <select class="form-control" >
                    <option value="">----</option>
                    <?php echo $company_list_html; ?>
                </select>
            </div>
            
            <div class="leftfloat">
                <label>Department:</label>
                <select class="form-control" >
                    <option value="">----</option>
                    <?php echo $department_list_html; ?>
                </select>
            </div>
            
            <div class="leftfloat">
                <label>Lastname or Firstname:</label>
                <input type="text" class="form-control" />
            </div>
            
            <div class="clear_10"></div>
        </div>
        <div class="clear_20"></div>
        
        <?php echo $emp_list_html; ?>
        
    </div>
	
    
	<div class="clear_60"></div>
    
</div>

	
@stop