@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("gd/view/action/$id") }}" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_label pm_form_title ">GENERAL DOCUMENTS AND REPORTS</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input  readonly="readonly"type="text" class="form-control" name="filer" value="{{ $record['employee'] }}" />
                                <input  readonly="readonly"type="hidden" class="form-control" name="employeeid" value="{{ $record['employeeid'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly class="form-control" name="reference"  value="{{ $record['reference_no'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="emp_no" value="{{ $record['employeeno'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['trans_date'] }}" name=""/>
                            </div>
                        </div>
						<div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="division" value="{{ Session::get('company') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['status'] }}" name="status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="department"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly class="form-control" name="contact" value="{{ $record['contact'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                    <input readonly type="text" class="form-control" name="section" value="{{ $record['section'] }}" />                            
                            </div>
                        </div>
                       
                        <div class="clear_10"></div>
                    </div>

                   <div class="container-header"><h5 class="text-center lined sub-header"><strong>DOCUMENTS DETAILS</strong></h5></div>
                    
                    
                    <div class="row">
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT TYPE:</label>
                            </div>
                            <div class="col4_form_container">                           
                                <input readonly {{ $record['doc_type'] == 4 ? "" : "disabled" }}  type="text" class="form-control" name="doc_others" value="{{ $record['doc_name'] }}" /> 
                            </div>
                            <div class="col4_form_container">
                                <input readonly {{ $record['doc_type'] == 4 ? "" : "disabled" }}  type="text" class="form-control" name="doc_others" value="{{ $record['doc_others'] }}" />                                
                            </div>
                            <div class="col4_form_container">&nbsp;
                            </div>
                            <div class="col4_form_container confidential {{ in_array(1,isset($signatory["reviewer"]) ? $signatory["reviewer"] : [] ) ? '' : 'hide' }}">
                                <input value=1 type="checkbox" disabled="" {{ $record['confidential'] ? "checked" : "" }} />                                
                                <input value=1 type="checkbox" class="hide" name="confidential" {{ $record['confidential'] ? "checked" : "" }} />                                
                               
                                CONFIDENTIAL
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT NAME:</label>
                            </div>
                            <div class="col20_form_container">
                                <input type="text" class="form-control" readonly name="doc_name" value="{{ $record['document_name'] }}" />
                            </div>
                        </div>

                       
                        <div class="clear_10"></div>
                    </div>

                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>SIGNATORIES</strong></h5></div>
                    
					
                    <div class="">
                        <table >
                            <tr>
                              <td width="15%">PREPARED:</td>                              
                              <td width="25%">
                                @if(isset($signatory[1]))
                                    1. {{ $signatory[1][0]["name"] }}
                                @endif
                              </td>
                              <td width="60%">
                                @if(isset($signatory[1]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[1][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[1]))
                                @foreach($signatory[1] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[1][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[1][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>REVIEWED:</td>
                              <td>
                                @if(isset($signatory[2]))
                                    1. {{ $signatory[2][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[2]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[2][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[2]))
                                @foreach($signatory[2] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
										<td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[2][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[2][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>RECOMMENDED:</td>
                              <td>
                                @if(isset($signatory[5]))
                                    1. {{ $signatory[5][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[5]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[5][0]["date"] }}" />
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[5]))
                                @foreach($signatory[5] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[5][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[5][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>ACKNOWLEDGED:</td>
                              <td>
                                @if(isset($signatory[3]))
                                    1. {{ $signatory[3][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[3]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[3][0]["date"] }}" />
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[3]))
                                @foreach($signatory[3] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[3][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[3][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>APPROVED:</td>
                              <td>
                                @if(isset($signatory[4]))
                                    1. {{ $signatory[4][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[4]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[4][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[4]))
                                @foreach($signatory[4] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " . $signatory[4][$key]["name"] }}
                                        </td>
                                        <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[4][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif
                            
                        </table>
                    </div>

                   <div class="container-header"><h5 class="text-center lined sub-header "><strong>DOCUMENTS</strong></h5></div>
                    

                    <label class="attachment_note text-danger">DOCUMENT FOR SIGNATURE</label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 1)
                        @if($attachment->code)
                        <?php $attach = json_decode($attachment->code);?>
                        <a href="{{ URL::to('/gd/download/') . '/' .$id . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a><br />
                        @else
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attachment->fn .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn }}</a><br />                        
                        @endif
                        @endif
                        @endforeach 

                       
                                            
                    </div>

                    <div class="clear_20"></div>

                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 2)
                        @if($attachment->code)
                        <?php $attach = json_decode($attachment->code);?>
                        <a href="{{ URL::to('/gd/download/') . '/' . $id. '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a><br />
                        @else
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attachment->fn .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn }}</a><br />                        
                        @endif                        
                        @endif
                        @endforeach

                  
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <span class="legend-action">ACTION</span>
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" disabled>{{ $record['comments'] }}</textarea>
                    </div>
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                @if( in_array($record['status'], ['For Review','For Acknowledgement','For Approval','Pending at PRES','For Clarification or Revision']) )                
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>CANCEL</strong> DOCUMENT</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="cancel">CANCEL</button>
                        </div>
                    </div>
                </div>
                @endif
                 @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop