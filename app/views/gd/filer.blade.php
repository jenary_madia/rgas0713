@extends('template/header')

@section('content')

<div id="wrap">
    <!-- @include('template/sidebar') -->
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">

             
    <div class="clear_10"></div>    

    <div class="text-right">
        <a href="{{ url::to("gd/create") }}" class="btn btn-default" >CREATE DOCUMENT</a>
    </div>

    <div class="clear_10"></div>

    <div class="">  
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY DOCUMENTS</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DOCUMENT NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COMPLETED</th>                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[7] }}</td>  
                    <td>{{ date("Y-m-d",strtotime($rs[1])) }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>
                    <td>{{ $rs[6] }}</td>   
                    <td>{{ $rs[7] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  
    </div>

    <div class="clear_20"></div>  

    <div class="" >
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
            <table id="reviewer_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DOCUMENT NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COMPLETED</th>                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($reviewer_record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[7] }}</td>  
                    
                    <td>{{ date("Y-m-d",strtotime($rs[1])) }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>
                    <td>{{ $rs[6] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table> 
            </div>
    </div>

</div>
</div>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/gd/filer.js') }}
@stop