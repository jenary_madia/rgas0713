@extends('template/header')

@section('content')

<form class="form-inline" method="post" enctype="multipart/form-data">
    <input type="hidden" value="{{ csrf_token() }}">
         
    <div class="row">   
         
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >GENERAL DOCUMENTS</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable">
                <thead>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DOCUMENT NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COMPLETED</th>                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>
                    <td>{{ $rs[6] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

</form>


@stop
@section('js_ko')
{{ HTML::script('/assets/js/gd/reviewer.js') }}
@stop