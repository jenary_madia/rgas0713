@extends('template/header')

@section('content')
        

        <form class="form-inline" method="post" action="{{ url("gd/review/form/action/$id") }}" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_label pm_form_title ">GENERAL DOCUMENTS AND REPORTS</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input  readonly="readonly"type="text" class="form-control" name="filer" value="{{ $record['employee'] }}" />
                                <input  readonly="readonly"type="hidden" class="form-control" name="employeeid" value="{{ $record['employeeid'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly class="form-control" name="reference"  value="{{ $record['reference_no'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="emp_no" value="{{ $record['employeeno'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['trans_date'] }}" name=""/>
                            </div>
                        </div>
						<div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="division" value="{{ $record['company'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['status'] }}" name="status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['dept_name'] }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly class="form-control" name="contact" value="{{ $record['contact'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                    <input readonly type="text" class="form-control" name="section" value="{{ $record['section'] }}" />                            
                            </div>
                        </div>
                       
                        <div class="clear_10"></div>
                    </div>

                    <input readonly="readonly" type="hidden"  name="holder" value="{{ $record['current'] }}" />

                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>DOCUMENTS DETAILS</strong></h5></div>
                    

                    <div class="row">
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT TYPE:</label>
                            </div>
                            <div class="col4_form_container">                           
                                <input readonly {{ $record['doc_type'] == 4 ? "" : "disabled" }}  type="text" class="form-control" name="doc_others" value="{{ $record['doc_name'] }}" /> 
                            </div>
                            <div class="col4_form_container">
                                <input {{ $record['doc_type'] == 4 ? "" : "disabled" }}  type="text" class="form-control" name="doc_others" value="{{ $record['doc_others'] }}" />                                
                            </div>
                            <div class="col4_form_container">&nbsp;
                            </div>
                            <div class="col4_form_container confidential {{ in_array(1,$signatory["reviewer"]) ? '' : 'hide' }}">
                                
                                @if($record['departmentid'] == Session::get('dept_id'))
                                <input value=1 type="checkbox" name="confidential" {{ $record['confidential'] ? "checked" : "" }} />  
                                @else
                                <input value=1 type="checkbox" disabled="" {{ $record['confidential'] ? "checked" : "" }} />                                
                                <input value=1 type="checkbox" class="hide" name="confidential" {{ $record['confidential'] ? "checked" : "" }} />                                
                                @endif
                                CONFIDENTIAL
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT NAME:</label>
                            </div>
                            <div class="col20_form_container">
                                <input type="text" class="form-control" readonly name="doc_name" value="{{ $record['document_name'] }}" />
                            </div>
                        </div>

                       
                        <div class="clear_10"></div>
                    </div>

                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>SIGNATORIES</strong></h5></div>
                    


					
                    <div class="">
                        <table >
                            <tr>
                              <td width="15%">PREPARED:</td>                              
                              <td width="25%">
                                @if(isset($signatory[1]))
                                    1. {{ $signatory[1][0]["name"] }}
                                @endif
                              </td>
                              <td width="60%">
                                @if(isset($signatory[1]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[1][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[1]))
                                @foreach($signatory[1] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[1][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[1][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>REVIEWED:</td>
                              <td>
                                @if(isset($signatory[2]))
                                    1. {{ $signatory[2][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[2]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[2][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[2]))
                                @foreach($signatory[2] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
										<td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[2][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[2][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>RECOMMENDED:</td>
                              <td>
                                @if(isset($signatory[5]))
                                    1. {{ $signatory[5][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[5]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[5][0]["date"] }}" />
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[5]))
                                @foreach($signatory[5] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[5][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[5][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>ACKNOWLEDGED:</td>
                              <td>
                                @if(isset($signatory[3]))
                                    1. {{ $signatory[3][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[3]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[3][0]["date"] }}" />
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[3]))
                                @foreach($signatory[3] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " .$signatory[3][$key]["name"] }}
                                        </td>
                                         <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[3][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td>APPROVED:</td>
                              <td>
                                @if(isset($signatory[4]))
                                    1. {{ $signatory[4][0]["name"] }}
                                @endif
                              </td>
                              <td>
                                @if(isset($signatory[4]))
                                    <input type="text" class="sig_date" readonly value="{{ $signatory[4][0]["date"] }}" />
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[4]))
                                @foreach($signatory[4] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td ></td>
                                        <td>
                                            {{ ($key + 1) . " " . $signatory[4][$key]["name"] }}
                                        </td>
                                        <td>
                                            <input type="text" class="sig_date" readonly value="{{ $signatory[4][$key]["date"] }}" />
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif
                            
                        </table>
                    </div>

                    @if(!$record['notifier'] || ($record['notifier'] && $record['notify_view'] == 1))
                
                   <div class="container-header"><h5 class="text-center lined sub-header "><strong>DOCUMENTS</strong></h5></div>
                    

	                <label class="attachment_note text-danger">DOCUMENT FOR SIGNATURE</label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 1)
                        @if($attachment->code)
                        <?php $attach = json_decode($attachment->code);?>
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB" }}</a><br />
                        @else
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attachment->fn .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn }}</a><br />                        
                        @endif
                        @endif
                        @endforeach 
                                            
                    </div>

                    <div class="clear_20"></div>
                    @if(!$record['notifier'] && ( (Session::get('is_division_head') && Session::get('div_code') == $record['div_code']) || Session::get('desig_level') == "president" || Session::get('dept_id') == $record['departmentid'] ))
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    @else
                    <label class="attachment_note"><strong>ATTACHMENTS</strong></label><br/>
                    @endif
                    <div class="attachment_container">
                        
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 2)
                        <?php $attach = json_decode($attachment->code);?>        
                                <p>
                                <a href="{{ URL::to('/gd/download/') . '/' .$id . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a>
                                    @if(!$record['notifier'] && ( (Session::get('is_division_head') && Session::get('div_code') == $record['div_code']) || Session::get('desig_level') == "president" || Session::get('dept_id') == $record['departmentid'] ))
                                    @if($attachment->type == 1)
                                    <?php $attach = json_decode($attachment->code);?>
                                    <input class='attachment-filesize' type='hidden' value='{{ $attach->filesize }}' />                        
                                    @if(!$readonly)<button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}" attach-id="{{ $attachment->id }}" >DELETE</button>@endif
                                    @else
                                    <input class='attachment-filesize' type='hidden' value='' />                        
                                    @if(!$readonly)<button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}" attach-id="{{ $attachment->id }}" >DELETE</button>@endif
                                    
                                    @endif
                                    @endif <br /></p>
                            @endif
                        @endforeach

                        @if(!$readonly)
                        @if(!$record['notifier'] && ( (Session::get('is_division_head') && Session::get('div_code') == $record['div_code']) || Session::get('desig_level') == "president" || Session::get('dept_id') == $record['departmentid'] ))
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif 
                        @endif            
                    </div>
                     @endif   

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <span class="legend-action">ACTION</span>
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" disabled>{{ $record['comments'] }}</textarea>
                    </div>
                    <div class="clear_20"></div>
                    @if($record['comments_ea'] && ( Session::get("employee_id") == 1 || $record['status'] == "Pending at PRES" ) )
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE WITH {{ $record['status'] == "Pending at PRES" ? "PRESIDENT" : "EA" }}:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" disabled>{{ $record['comments_ea'] }}</textarea>
                    </div>
                    <div class="clear_20"></div>
                    @endif
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea {{ $readonly }}  rows="3" class="form-control textarea_inside_width" name="comment">{{ Input::old("comment") }}</textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                @if(!$readonly)
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes">{{ $record['status'] == "Pending at PRES" ? "<strong>SEND</strong> TO PRESIDENT" : "<strong>APPROVE</strong> DOCUMENT" }}</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }}  type="submit" class="btn btn-default btndefault" name="action" value="approve">APPROVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO</label>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <select {{ $readonly }}  name="assign" >
                                <option></option>
                                <?php $approver = [];?>
                                @foreach($approve_list as $key=>$val)
				    @if(Session::get('employee_id') != $key)
                                <option {{ Input::old("assign") == $key ? "selected" : "" }}  value="{{ $key }}">{{ $val }}</option>
                                <?php $approver[] = $key; ?>
				    @endif
                                @endforeach

                                @if(!in_array($record['employeeid'] , $approver))
                                <option {{ Input::old("assign") == $record['employeeid'] ? "selected" : "" }}  value="{{ $record['employeeid'] }}">{{ ucwords(strtolower($record['employee'])) . " (" . strtoupper($record['departmentid2']) . ")" }}</option>
                                @endif
                            </select>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }}  type="submit" class="btn btn-default btndefault" name="action" value="return">RETURN</button>
                        </div>
                    </div>
                    @if(Session::get("employee_id") == 1)
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes">FOR <strong>CHECKING</strong> by EA - see comments</label>
                             <select {{ $readonly }}  name="assign_to_ea" >
                                <option></option>
                                @foreach($receiver_list as $key=>$val)
                                <option  {{ Input::old("assign_to_ea") == $key ? "selected" : "" }} value="{{ $key }}">{{ $val }}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }}  type="submit" class="btn btn-default btndefault" name="action" value="return_to_ea">SEND</button>
                        </div>
                    </div>
                    @endif
                </div>
                @endif

                 @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/reviewerform.js') }}
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop