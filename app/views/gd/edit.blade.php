@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("gd/edit/action/$id") }}" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_label pm_form_title ">GENERAL DOCUMENTS AND REPORTS</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input  readonly="readonly"type="text" class="form-control" name="filer" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" maxlength="100" class="form-control" name="reference"  value="{{ $record['reference_no'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="emp_no" value="{{ Session::get('employeeid') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['trans_date'] }}" name="dept_head"/>
                            </div>
                        </div>
						<div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="division" value="{{ Session::get('company') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['status'] }}" name="status"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="dept_name"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control isNumeric" maxlength="11" name="contact" value="{{ $record['contact'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" name="section" style="width:100%">
                                        <option value=""></option>
                                        @foreach($section as $key => $val)                                      
                                        <option {{ $record['section'] == $key ? "selected" : "" }} value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                       
                        <div class="clear_10"></div>
                    </div>


             
                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>DOCUMENTS DETAILS</strong></h5></div>
                    

                    <div class="row">
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT TYPE:</label>
                            </div>
                            <div class="col4_form_container">                           
                                <select class="form-control" name="doc_type" style="width:100%">
                                        <option value=""></option>
                                        @foreach($doc_type as $key => $val)                                      
                                        <option {{ $record['doc_type'] == $key ? "selected" : "" }} value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="col4_form_container">
                                <input {{ $record['doc_type'] == 4 ? "" : "disabled" }}  type="text" class="form-control" name="doc_others" value="{{ $record['doc_others'] }}" />                                
                            </div>
                            <div class="col4_form_container">&nbsp;
                            </div>
                            <div class="col4_form_container confidential {{ in_array(1,isset($signatory["reviewer"]) ? $signatory["reviewer"] : [] ) ? '' : 'hide' }}">
                                <input value=1 type="checkbox" name="confidential" {{ $record['confidential'] ? "checked" : "" }} />                                
                                CONFIDENTIAL
                            </div>
                        </div>

                        <div class="clear_10"></div>
                        <div class="row3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DOCUMENT NAME:</label>
                            </div>
                            <div class="col20_form_container">
                                <input type="text" maxlength="200" class="form-control" name="doc_name" value="{{ $record['document_name'] }}" />
                            </div>
                        </div>

                       
                        <div class="clear_10"></div>
                    </div>

              
					  <div class="container-header"><h5 class="text-center lined sub-header "><strong>SIGNATORIES</strong></h5></div>
                    
                    <div class="">
                        <table >
                            <tr>
                              <td colspan="4" ></td> 
                              <td colspan="2" >NOTIFY THE FOLLOWING UPON<br> SUBMISSION TO THE SIGNATORY:</td>
                            </tr>

                            <tr>
                              <td width="120" valign="top" >PREPARED:</td>
                              <td width="220" valign="top" >
                                <select class="employee form-control" id="prepared" name="prepared" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                              </td>
                              <td valign="top" ><input type="button" value="ADD" id="btnPrepared"  /></td>
                              <td width="220" class='signatory_list' >
                                  @if(isset($signatory[1]))
                                    <span class='spanPrepared'>1</span>. {{ $signatory[1][0]["name"] }}
                                    <button type='button' name='' class='delete-row' data-span='spanPrepared' ><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                  @endif
                              </td>
                              <td>
                                @if(isset($signatory[1]))
                                    <div {{ $signatory[1][0]["notify"] ? "class='hide'" : "" }} >
                                        <select class='employee' name='Prepared_{{ $signatory[1][0]["employee"] }}' >
                                                <option value=''></option>
                                                @foreach($employees as $key => $val) 
                                                    <option value='{{ $key }}'>{{ $val }}</option> 
                                                @endforeach
                                        </select>
                                        <input type='hidden' name='txtPrepared[]' value='{{ $signatory[1][0]["employee"] }}' />
                                        <input type='button' value='ADD' class='notify' id ='{{ $signatory[1][0]["employee"] }}' />
                                    </div>
                                    <div class='signatory_list' >
                                        @if($signatory[1][0]["notify"])
                                            {{ $signatory[1][0]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                            <input type='hidden' name='Prepared_{{ $signatory[1][0]["employee"] }}' value='{{ $signatory[1][0]["notify"] }}' /><input type='checkbox' {{ $signatory[1][0]["view"] ? "checked" : "" }} name='notify_{{ $signatory[1][0]["employee"] }}' value='1' /> VIEW DOCUMENT
                                        @endif
                                    </div>
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[1]))
                                @foreach($signatory[1] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td colspan='3' ></td>
                                        <td class='signatory_list' > <span class='spanPrepared'>{{ $key + 1 }}</span>. {{ $signatory[1][$key]["name"] }} <button type='button' name='' class='delete-row' data-span='spanPrepared' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>
                                        <td>
                                            <div {{ $signatory[1][$key]["notify"] ? "class='hide'" : "" }} >
                                                <select class='employee form-control' name='Prepared_{{ $signatory[1][$key]["employee"] }}' >
                                                        <option value=''></option>
                                                        @foreach($employees as $e => $val) 
                                                            <option value='{{ $e }}'>{{ $val }}</option>
                                                        @endforeach
                                                </select>
                                                <input type='hidden' name='txtPrepared[]' value='{{ $signatory[1][$key]["employee"] }}' />
                                                 <input type='button' value='ADD' class='notify' id ='{{ $signatory[1][$key]["employee"] }}' />
                                            </div>
                                            <div class='signatory_list' >
                                                 @if($signatory[1][$key]["notify"])
                                                    {{ $signatory[1][$key]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                                    <input type='hidden' name='Prepared_{{ $signatory[1][$key]["employee"] }}' value='{{ $signatory[1][$key]["notify"] }}' /><input type='checkbox' {{ $signatory[1][$key]["view"] ? "checked" : "" }} name='notify_{{ $signatory[1][$key]["employee"] }}' value='1' /> VIEW DOCUMENT
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td valign="top" >REVIEWED:</td>
                              <td valign="top" >
                                <select class="employee form-control" id="reviewed" name="reviewed" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                              </td>
                              <td valign="top" ><input type="button" value="ADD" id="btnReviewed" /></td>
                              <td class='signatory_list'>
                                  @if(isset($signatory[2]))
                                    <span class='spanReviewed'>1</span>. {{ $signatory[2][0]["name"] }}
                                    <button type='button' name='' class='delete-row' data-span='spanReviewed' ><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                  @endif
                              </td>
                              <td>
                                @if(isset($signatory[2]))
                                    <div {{ $signatory[2][0]["notify"] ? "class='hide'" : "" }} >
                                        <select class='employee' name='Reviewed_{{ $signatory[2][0]["employee"] }}' >
                                                <option value=''></option>
                                                @foreach($employees as $key => $val) 
                                                    <option value='{{ $key }}'>{{ $val }}</option> 
                                                @endforeach
                                        </select>
                                        <input type='hidden' name='txtReviewed[]' value='{{ $signatory[2][0]["employee"] }}' />
                                        <input type='button' value='ADD' class='notify' id ='{{ $signatory[2][0]["employee"] }}' />
                                    </div>
                                    <div class='signatory_list' >
                                        @if($signatory[2][0]["notify"])
                                            {{ $signatory[2][0]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                            <input type='hidden' name='Reviewed_{{ $signatory[2][0]["employee"] }}' value='{{ $signatory[2][0]["notify"] }}' /><input type='checkbox' {{ $signatory[2][0]["view"] ? "checked" : "" }} name='notify_{{ $signatory[2][0]["employee"] }}' value='2' /> VIEW DOCUMENT
                                        @endif
                                    </div>
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[2]))
                                @foreach($signatory[2] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td colspan='3' ></td>
                                        <td class='signatory_list' > <span class='spanReviewed'>{{ $key + 1}}</span>. {{ $signatory[2][$key]["name"] }} <button type='button' name='' class='delete-row' data-span='spanReviewed' ><i class='fa fa-minus-circle fa-xs faplus'></i></button></td>
                                        <td  >
                                            <div {{ $signatory[2][$key]["notify"] ? "class='hide'" : "" }} >
                                                <select class='employee form-control' name='Reviewed_{{ $signatory[2][$key]["employee"] }}' >
                                                        <option value=''></option>
                                                        @foreach($employees as $e => $val) 
                                                            <option value='{{ $e }}'>{{ $val }}</option>
                                                        @endforeach
                                                </select>
                                                <input type='hidden' name='txtReviewed[]' value='{{ $signatory[2][$key]["employee"] }}' />
                                                 <input type='button' value='ADD' class='notify' id ='{{ $signatory[2][$key]["employee"] }}' />
                                            </div>
                                            <div class='signatory_list' >
                                                 @if($signatory[2][$key]["notify"])
                                                    {{ $signatory[2][$key]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-minus-circle fa-xs faplus'></i></button>
                                                    <input type='hidden' name='Reviewed_{{ $signatory[2][$key]["employee"] }}' value='{{ $signatory[2][$key]["notify"] }}' /><input type='checkbox' {{ $signatory[2][$key]["view"] ? "checked" : "" }} name='notify_{{ $signatory[2][$key]["employee"] }}' value='2' /> VIEW DOCUMENT
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td valign="top" >RECOMMENDED:</td>
                              <td valign="top" >
                                <select class="employee form-control" id="recommended" name="recommended" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                              </td>
                              <td valign="top" ><input type="button" value="ADD" id="btnRecommended" /></td>
                              <td class='signatory_list' >
                                  @if(isset($signatory[5]))
                                    <span class='spanRecommended'>1</span>. {{ $signatory[5][0]["name"] }}
                                    <button type='button' name='' class='delete-row' data-span='spanRecommended' ><i class='fa fa-minus-circle fa-xs faplus'></i></button>
                                  @endif
                              </td>
                              <td>
                                @if(isset($signatory[5]))
                                    <div {{ $signatory[5][0]["notify"] ? "class='hide'" : "" }} >
                                        <select class='employee' name='Recommended_{{ $signatory[5][0]["employee"] }}' >
                                                <option value=''></option>
                                                @foreach($employees as $key => $val) 
                                                    <option value='{{ $key }}'>{{ $val }}</option> 
                                                @endforeach
                                        </select>
                                        <input type='hidden' name='txtRecommended[]' value='{{ $signatory[5][0]["employee"] }}' />
                                        <input type='button' value='ADD' class='notify' id ='{{ $signatory[5][0]["employee"] }}' />
                                    </div>
                                    <div class='signatory_list' >
                                        @if($signatory[5][0]["notify"])
                                            {{ $signatory[5][0]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-minus-circle fa-xs faplus'></i></button>
                                            <input type='hidden' name='Recommended_{{ $signatory[5][0]["employee"] }}' value='{{ $signatory[5][0]["notify"] }}' /><input type='checkbox' {{ $signatory[5][0]["view"] ? "checked" : "" }} name='notify_{{ $signatory[5][0]["employee"] }}' value='5' /> VIEW DOCUMENT
                                        @endif
                                    </div>
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[5]))
                                @foreach($signatory[5] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td colspan='3' ></td>
                                        <td class='signatory_list'  > <span class='spanRecommended'>{{ $key + 1}}</span>. {{ $signatory[5][$key]["name"] }} <button type='button' name='' class='delete-row' data-span='spanRecommended' ><i class='fa fa-minus-circle fa-xs faplus'></i></button></td>
                                        <td >
                                            <div {{ $signatory[5][$key]["notify"] ? "class='hide'" : "" }} >
                                                <select class='employee form-control' name='Recommended_{{ $signatory[5][$key]["employee"] }}' >
                                                        <option value=''></option>
                                                        @foreach($employees as $e => $val) 
                                                            <option value='{{ $e }}'>{{ $val }}</option>
                                                        @endforeach
                                                </select>
                                                <input type='hidden' name='txtRecommended[]' value='{{ $signatory[5][$key]["employee"] }}' />
                                                 <input type='button' value='ADD' class='notify' id ='{{ $signatory[5][$key]["employee"] }}' />
                                            </div>
                                            <div class='signatory_list' >
                                                 @if($signatory[5][$key]["notify"])
                                                    {{ $signatory[5][$key]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                                    <input type='hidden' name='Recommended_{{ $signatory[5][$key]["employee"] }}' value='{{ $signatory[5][$key]["notify"] }}' /><input type='checkbox' {{ $signatory[5][$key]["view"] ? "checked" : "" }} name='notify_{{ $signatory[5][$key]["employee"] }}' value='5' /> VIEW DOCUMENT
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td valign="top" >ACKNOWLEDGED:</td>
                              <td valign="top" >
                                <select class="employee form-control" id="acknowledged" name="acknowledged" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                              </td>
                              <td valign="top" ><input type="button" value="ADD" id="btnAcknowledged" /></td>
                              <td class='signatory_list' >
                                  @if(isset($signatory[3]))
                                    <span class='spanAcknowledged'>1</span>. {{ $signatory[3][0]["name"] }}
                                    <button type='button' name='' class='delete-row' data-span='spanAcknowledged' ><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                  @endif
                              </td>
                              <td>
                                @if(isset($signatory[3]))
                                    <div {{ $signatory[3][0]["notify"] ? "class='hide'" : "" }} >
                                        <select class='employee' name='Acknowledged_{{ $signatory[3][0]["employee"] }}' >
                                                <option value=''></option>
                                                @foreach($employees as $key => $val) 
                                                    <option value='{{ $key }}'>{{ $val }}</option> 
                                                @endforeach
                                        </select>
                                        <input type='hidden' name='txtAcknowledged[]' value='{{ $signatory[3][0]["employee"] }}' />
                                        <input type='button' value='ADD' class='notify' id ='{{ $signatory[3][0]["employee"] }}' />
                                    </div>
                                    <div class='signatory_list' >
                                        @if($signatory[3][0]["notify"])
                                            {{ $signatory[3][0]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                            <input type='hidden' name='Acknowledged_{{ $signatory[3][0]["employee"] }}' value='{{ $signatory[3][0]["notify"] }}' /><input type='checkbox' {{ $signatory[3][0]["view"] ? "checked" : "" }} name='notify_{{ $signatory[3][0]["employee"] }}' value='3' /> VIEW DOCUMENT
                                        @endif
                                    </div>
                                @endif
                              </td>
                           </tr>

                           @if(isset($signatory[3]))
                                @foreach($signatory[3] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td colspan='3' ></td>
                                        <td class='signatory_list' > <span class='spanAcknowledged'>{{ $key + 1 }}</span>. {{ $signatory[3][$key]["name"] }} <button type='button' name='' class='delete-row' data-span='spanAcknowledged' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>
                                        <td  >
                                            <div {{ $signatory[3][$key]["notify"] ? "class='hide'" : "" }} >
                                                <select class='employee form-control' name='Acknowledged_{{ $signatory[3][$key]["employee"] }}' >
                                                        <option value=''></option>
                                                        @foreach($employees as $e => $val) 
                                                            <option value='{{ $e }}'>{{ $val }}</option>
                                                        @endforeach
                                                </select>
                                                <input type='hidden' name='txtAcknowledged[]' value='{{ $signatory[3][$key]["employee"] }}' />
                                                 <input type='button' value='ADD' class='notify' id ='{{ $signatory[3][$key]["employee"] }}' />
                                            </div>
                                            <div class='signatory_list' >
                                                 @if($signatory[3][$key]["notify"])
                                                    {{ $signatory[3][$key]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                                    <input type='hidden' name='Acknowledged_{{ $signatory[3][$key]["employee"] }}' value='{{ $signatory[3][$key]["notify"] }}' /><input type='checkbox' {{ $signatory[3][$key]["view"] ? "checked" : "" }} name='notify_{{ $signatory[3][$key]["employee"] }}' value='3' /> VIEW DOCUMENT
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif

                            <tr>
                              <td valign="top" >APPROVED:</td>
                              <td valign="top" >
                                <select class="employee form-control" id="approved" name="approved" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                              </td>
                              <td valign="top" ><input type="button" value="ADD" id="btnApproved" /></td>
                              <td class='signatory_list' >
                                  @if(isset($signatory[4]))
                                    <span class='spanApproved'>1</span>. {{ $signatory[4][0]["name"] }}
                                    <button type='button' name='' class='delete-row' data-span='spanApproved' ><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                  @endif
                              </td>
                              <td>
                                @if(isset($signatory[4]))
                                    <div {{ $signatory[4][0]["notify"] ? "class='hide'" : "" }} >
                                        <select class='employee' name='Approved_{{ $signatory[4][0]["employee"] }}' >
                                                <option value=''></option>
                                                @foreach($employees as $key => $val) 
                                                    <option value='{{ $key }}'>{{ $val }}</option> 
                                                @endforeach
                                        </select>
                                        <input type='hidden' name='txtApproved[]' value='{{ $signatory[4][0]["employee"] }}' />
                                        <input type='button' value='ADD' class='notify' id ='{{ $signatory[4][0]["employee"] }}' />
                                    </div>
                                    <div class='signatory_list' >
                                        @if($signatory[4][0]["notify"])
                                            {{ $signatory[4][0]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                            <input type='hidden' name='Approved_{{ $signatory[4][0]["employee"] }}' value='{{ $signatory[4][0]["notify"] }}' /><input type='checkbox' {{ $signatory[4][0]["view"] ? "checked" : "" }} name='notify_{{ $signatory[4][0]["employee"] }}' value='4' /> VIEW DOCUMENT
                                        @endif
                                    </div>
                                @endif
                              </td>
                            </tr>

                            @if(isset($signatory[4]))
                                @foreach($signatory[4] as $key=>$rs)
                                @if($key != 0)
                                    <tr>
                                        <td colspan='3' ></td>
                                        <td class='signatory_list'  > <span class='spanApproved'>{{ $key + 1 }}</span>. {{ $signatory[4][$key]["name"] }} <button type='button' name='' class='delete-row' data-span='spanApproved' ><i class='fa fa-times-circle fa-xs faplus'></i></button></td>
                                        <td >
                                            <div {{ $signatory[4][$key]["notify"] ? "class='hide'" : "" }} >
                                                <select class='employee form-control' name='Approved_{{ $signatory[4][$key]["employee"] }}' >
                                                        <option value=''></option>
                                                        @foreach($employees as $e => $val) 
                                                            <option value='{{ $e }}'>{{ $val }}</option>
                                                        @endforeach
                                                </select>
                                                <input type='hidden' name='txtApproved[]' value='{{ $signatory[4][$key]["employee"] }}' />
                                                 <input type='button' value='ADD' class='notify' id ='{{ $signatory[4][$key]["employee"] }}' />
                                            </div>
                                            <div class='signatory_list' >
                                                 @if($signatory[4][$key]["notify"])
                                                    {{ $signatory[4][$key]["notify_name"] }} <button type='button' name='' class='delete-notify'><i class='fa fa-times-circle fa-xs faplus'></i></button>
                                                    <input type='hidden' name='Approved_{{ $signatory[4][$key]["employee"] }}' value='{{ $signatory[4][$key]["notify"] }}' /><input type='checkbox' {{ $signatory[4][$key]["view"] ? "checked" : "" }} name='notify_{{ $signatory[4][$key]["employee"] }}' value='4' /> VIEW DOCUMENT
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            @endif
                            
                        </table>
                    </div>
 
                     <div class="container-header"><h5 class="text-center lined sub-header "><strong>DOCUMENTS</strong></h5></div>
                    

	                <label class="attachment_note text-danger">DOCUMENT FOR SIGNATURE</label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 1)
                        <?php $attach = json_decode($attachment->code);?>
                        <p>
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a>
                        <input class='signatory-file' type='hidden'  />
                        <button class="btn btn-sm btn-danger remove-fn remove-signature" attach-name="{{ $attach->original_filename }}" attach-id="{{ $attachment->id }}" >DELETE</button><br /></p>
                        @endif
                        @endforeach 

                        <div id="attachments-signatory"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-signatory" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>

                    <div class="clear_20"></div>

                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        
                        @foreach($attachments as $attachment)
                        @if($attachment->type == 2)
                        <?php $attach = json_decode($attachment->code);?>
                        <p>
                        <a href="{{ URL::to('/gd/download/') . '/' . $id . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attach->filesize }}' />
                        <button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}" attach-id="{{ $attachment->id }}" >DELETE</button><br /></p>
                        @endif
                        @endforeach

                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <span class="legend-action">ACTION</span>
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" disabled>{{ $record['comments'] }}</textarea>
                    </div>
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ $record['comments_save'] }}</textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> FOR APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
                 @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>
var emp_rec =  @foreach($employees as $key => $val)"<option value='{{ $key }}'>{{ $val }}</option>" + @endforeach "";
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/edit.js') }}
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop