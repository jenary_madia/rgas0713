<div class="noprint">
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

		<ul class="nav navbar-nav menu_bar noprint">

			<li class="active">
				<a href="#" onfocus="this.blur()">Home</a>
			</li>
			
			<li>
				<a href="<?php echo $base_url ?>/employees/list" onfocus="this.blur()">Employees List</a>
			</li>

			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  Admin / Gen Doc <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="<?php echo $base_url ?>/lrf/leaves">Leave</a>
						<a href="#">Travel Authorization</a>
						<!--JMA ADD THIS CODE-->
						@if( Session::get('company') == "RBC-CORP" )<a href="{{ url('oam') }}">Online Attendance</a>@endif
						<a href="{{ url('gd/report') }}" onfocus="this.blur()">General Documents</a>
						<a href="{{ url('svr/report') }}" onfocus="this.blur()">Service Vehicle</a>
						<a href="{{ url('os/report') }}" onfocus="this.blur()">Office Sales</a>
						<!--JMA END THIS CODE-->
						<!--JENARY ADD THIS CODE -->
						<a href="<?php echo $base_url ?>/ns/notifications">Notification</a>
					</li>
				</ul>
			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  CHRD <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="<?php echo $base_url ?>/cbr/cbr-requests">Compensation, Benefits and Records Requests</a>
					</li>
					
					@if(Session::get("company") == "RBC-CORP")
					<li>
						<a href="{{ url('pmf/report') }}">Performance Management Form</a>
					</li>
					@endif
					
					<?php if(!Session::get('is_te_chrd') && (Session::get("desig_level") == "head" || Session::get('designation') == 'SVP FOR CORP. SERVICES' || Session::get('desig_level') == 'president' || Session::get('designation') == 'VP FOR CORP. HUMAN RESOURCE'   ) ): ?>
					<li>
						<a href="<?php echo $base_url ?>/te/submitted-te-requests">Training Endorsement Form</a>
					</li>
					<?php endif; ?>

					<li>
						<a href="{{route('nte.list', 'CHRD')}}">Notice to Explain</a>
					</li>
					<li>
						<a href="{{route('nda.list', 'CHRD')}}">Notice of Disciplinary Action</a>
					</li>
					
					<!--JMA ADD THIS CODE-->
					@if(Session::get("is_cc_cbr_filer"))
					<li> 
						<a href="<?php echo $base_url ?>/cc/report" onfocus="this.blur()">Clearance Certification</a>
					</li>
					@endif
					
					<li> 
						<a href="<?php echo $base_url ?>/mrf/list-view">MANPOWER REQUEST</a>
					</li>
					
					<!--JMA END THIS CODE-->
				</ul>

			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  CSMD <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<!--JENARY END THIS CODE -->
					<li>
						<a href="<?php echo $base_url ?>/moc">MOC/Process Service Request</a>
					</li>
				</ul>
			</li>

			<!-- NTE -->
			
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  CITD <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="#">IT Service Request</a>
					</li>
				</ul>
			</li>

			@if(in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  NPMD <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="<?php echo $base_url ?>/pmarf/pmarf-requests">Promo and Merchandising Activity Request</a>
					</li>
				</ul>
			</li>
			@endif

			@if( Session::get('company') == "RBC-CORP" )
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
				  MKTG and SMIS <span class="caret"></span>
				</a>

				<ul class="dropdown-menu" role="menu">
					<li>
						<a href="{{ url('art/report') }}">Art Design</a>
					</li>
					<!--FOR MSR-->
						@if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)) || in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
							<li>
								<a href="<?php echo $base_url ?>/msr">Manpower Service Request</a>
							</li>
						@endif
					<!--END MSR-->
				</ul>
			</li>
			@endif
			
		</ul>
		
		<ul class="nav navbar-nav navbar-right menu_bar">
			<li>
				<a href="<?php echo $base_url ?>/sign-out">Log out</a>
			</li>
		</ul>

	</div>
</div>