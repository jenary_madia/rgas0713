<div class="noprint">
	<div class="nav_tab_container metro leftfloat">
		<nav class="sidebar light">
			<ul>
				<li>
					<a href="javascript::void(0)" id="create_overtime" onfocus="this.blur();">Create Overtime</a>
				</li>
				
				<li>
					<a href="javascript::void(0)" id="create_taf" onfocus="this.blur();">Create TAF</a>
				</li>
				
				<?php if(Session::get("is_taf_receiver")): ?>
				<li>
					<a href="#"></i>Submitted TAF</a>
				</li>
				<?php endif ?>
				<?php if(Session::get("is_svp_for_cs")): ?>
				<li>
					<a href="javascript::void(0)" id="create_itr" onfocus="this.blur();">Create ITR</a>
				</li>
				 <?php endif ?>
					
				<?php if (Session::get("is_itr_bsa") ): ?>
				
				<li>
					<a href="javascript::void(0)" id="assess_itr" onfocus="this.blur();">Submitted ITR</a>
				</li>
				
				<?php endif; ?>
				
				<?php if (Session::get("is_system_satellite") ): ?>
					 
				<li>
					<a href="javascript::void(0)" id="assess_itr_satellite" onfocus="this.blur();">Submitted ITR</a>
				</li>
				<?php endif; ?>
				
				
				<?php if (Session::get("is_citd_employee")): ?>
				
				<li>
					<a href="javascript::void(0)" id="custom_itr" onfocus="this.blur();">Create Custom ITR</a>
				</li>
				
				<?php endif; ?> 
				
				<li>
					<a href="javascript::void(0)" id="create_sap_access" onfocus="this.blur();">Create SAP Access Request</a>
				</li>
				
				<li>
					<a href="<?php echo $base_url ?>/cbr/create" onfocus="this.blur()">Create CBR</a>
				</li>
				
				<?php if(Session::get('is_cbr_receiver')): ?>
					<li>
						<a href="<?php echo $base_url ?>/cbr/submitted-cbr-requests" onfocus="this.blur()">Submitted CBR Request</a> 
					</li>
				<?php endif; ?>

				<?php if(Session::get('dept_id') == 24 || Session::get('dept_id') == 144): ?>
				<li>
					<a href="<?php echo $base_url ?>/pmarf/create" onfocus="this.blur()">Create PMARF</a>
				</li>
				<?php endif; ?>
				
				<li>
					<a href="<?php echo $base_url ?>/pmarf/submitted-pmarf-requests" onfocus="this.blur()">Submitted PMARF Request</a>
				</li>
				
				<?php if(Session::get("desig_level") == "head"): ?>
				<li> 
					<a href="<?php echo $base_url ?>/te/create" onfocus="this.blur()">Create TE</a>
				</li>
				<?php endif; ?>
				
				<?php if(Session::get('is_te_chrd')) : ?>
						<li>
							<a href="<?php echo $base_url ?>/te/submitted-te-requests" onfocus="this.blur()">Submitted TE Request</a>
						</li>
				<?php endif; ?>
				
				<li> 
					<a href="<?php echo $base_url ?>/pmf/create" onfocus="this.blur()">Create PMF</a>
				</li>
			
				
				<li> 
					<a href="<?php echo $base_url ?>/lrf/create" onfocus="this.blur()">Create Leave</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/ns/create" onfocus="this.blur()">Create Notification</a>
				</li>
				@if( in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
	                <li>
	                    <a href="<?php echo $base_url ?>/lrf/pe_create" onfocus="this.blur()">Create Leave (Production Encoder)</a>
	                </li>

	                <li>
	                    <a href="<?php echo $base_url ?>/ns/pe_create" onfocus="this.blur()">Create Notification (Production Encoder)</a>
	                </li>
	            @endif
				@if(Session::get("is_lrf_receiver") || Session::get("is_notif_receiver"))
					<li>
						<a href="<?php echo $base_url ?>/submitted/leaves_notifications" onfocus="this.blur()">Submitted Applications</a>
					</li>

					<li>
						<a href="<?php echo $base_url ?>/submitted/pe_leaves_notifications" onfocus="this.blur()">Submitted Applications (Production Encoders)</a>
					</li>
				@endif

				@if(Session::get('is_chrd_aer_staff') == 1 || Session::get('is_chrd_aer_staff') == 2 || Session::get('is_chrd_aer_staff') == 3 || Session::get('is_chrd_aer_staff') == 4 || Session::get('is_chrd_aer_staff') == 5)
				<li> 
					<a href="<?php echo $base_url ?>/nte/nte/create" onfocus="this.blur()">Create Notice to Explain</a>
				</li>

				<li> 
					<a href="<?php echo $base_url ?>/nte/nda/create" onfocus="this.blur()">Create Notice of Disciplinary Action</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/nte/crr/list" onfocus="this.blur()">CRR Build-up</a>
				</li>
				@endif
				
				@if(Session::get("is_cc_cbr_filer"))
				<li> 
					<a href="<?php echo $base_url ?>/cc/create" onfocus="this.blur()">Create Clearance Certification</a>
				</li>
				@endif
				
				<li> 
					<a href="{{ url('gd/create') }}" onfocus="this.blur()">Create General Document</a>
				</li>
			</ul>
		</nav>
	</div>
</div>