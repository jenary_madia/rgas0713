<!doctype html>
<html>
	<head>
		<title>{{-- Rebisco General Approval System --}}</title>
		
    @section('head')
		
		<!-- General Style -->
		{{ Minify::stylesheet(
			array('/assets/css/bootstrap.css'
				 ,'/assets/css/metro-bootstrap.css'
			     ,'/assets/css/main.css'
			     ,'/assets/css/employee.css'
                 ,'/assets/css/validation_engine.css'
                 ,'/assets/css/datepicker.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
          		 ,'/assets/css/bootstrap-timepicker.min.css'
                 ,'/assets/css/bootstrap-multiselect.css'
                 ,'/assets/css/new_style.css'
				,'/assets/css/standards.css'
                 ,'/assets/css/font-awesome/css/font-awesome.css'

                 ,'/assets/css/multiselect/jquery.multiselect.1.13.css'
                 ,'/assets/css/multiselect/jquery.multiselect.filter.css'
                 ,'/assets/css/nte/nte.css'
                 ,'/assets/css/nte/jQuery-1-11-4-smoothness.css'
				 ,'/assets/css/chosen.min.css'
			))->withFullUrl()
		}}
		
		<base href="<?php echo $base_url; ?>" />
		
    @show
	</head>	
	
    <body>
		
		<div class="company_logo noprint">
			<!--<img src="<?php echo $base_url;?>/assets/img/rebisco_logo.png" width="72" height="71" />-->
		</div>
		
		<div class="system_logo noprint">
			{{-- <h4>Rebisco General Approval System</h4> --}}
		</div>
		
		<?php if( Session::has("employee_id") ): ?>
		
			<div class="login_info noprint">
				Welcome <?php echo ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )); ?>
				
				<div class="clear"></div>

				<?php echo Session::get("company_name"); ?>
			</div>
			<div class="clear"></div>
			
			@include('template/menubar')
			
			<hr class="header_hr" />
	
		<?php else: ?>
		
			<div class="clear"></div>
		
			<hr class="header_hr" />
            
		<?php endif; ?>
		
		<div id="wrap">
			@if(Session::get("logged_in"))
			@include('template/sidebar')
			@endif
			
			<div class="container">
				@if(isset($nte_messages))
					<div class="alert alert-success">
						@foreach($nte_messages as $message)
					        <p>{{ $message }}</p>
				    	@endforeach
				    </div>
				@endif
				@if (Session::has('successMessage'))
			    <div class="alert alert-success">
			        <p>{{ Session::get('successMessage') }}</p>
			    </div>
			    @endif
				@if (Session::has('errorMessage'))
			    <div class="alert alert-danger">
			        <p>{{ Session::get('errorMessage') }}</p>
			    </div>
			    @endif
				@if ($errors->any())
				<div id="global_message" class="alert alert-danger">
				<ul>
				    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
				</ul>
				</div>
				@endif
				@if(@count($messages) > 0)
				<div id="global_message" class="alert alert-danger">
					<ul>
					@if (isset($messages))
						@foreach ($messages as $message)
							<li>{{ $message }}</li>
						@endforeach
					@endif
					</ul>
				</div>
				@endif
				@yield('content')
				<div class="clear_20"></div>
			</div><!-- container -->
			
			<div class="clear_60"></div>
		</div><!-- wrap -->
		<div class="clear_20"></div>
        
		<div class="noprint">
		<div id="footer">
		
			Copyright &copy; REBISCO 2014. All Rights Reserved.
			<br />
			System Provided by Corporate Information Technology Department 
			
		</div>
		</div>
	<script type="text/javasript">
	@if(isset($homepage))
		var homepage = "{{ $homepage }}";
	@endif
	</script>
	
	
    @section('js')

		{{ HTML::script('/assets/js/jquery-1.10.1.min.js') }}
		{{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.js') }}
		{{ HTML::script('/assets/js/jquery-migrate-1.2.1.min.js') }}
		{{ HTML::script('/assets/js/bootstrap.js') }}
		{{ HTML::script('/assets/js/jquery.mockjax.js') }}
		{{ HTML::script('/assets/js/bootstrap-typeahead.js') }}
		<!--{{ HTML::script('/assets/js/bootstrap-datepicker.js') }}-->
 		{{ HTML::script('/assets/js/bootstrap-timepicker.min.js') }}
		{{ HTML::script('/assets/js/chosen.jquery.min.js') }}

		
		{{ HTML::script('/assets/js/jquery.dataTables.min.js') }}
		{{ HTML::script('/assets/js/datatables.fnReloadAjax.js') }}
		{{ HTML::script('/assets/js/login.js') }}
		{{ HTML::script('/assets/js/employee.js') }}
		{{ HTML::script('/assets/js/cbr_datatables.js') }}
		{{ HTML::script('/assets/js/te_datatables.js') }}
		{{ HTML::script('/assets/js/pmarf_datatables.js') }}
		{{ HTML::script('/assets/js/common.js') }}
		<!-- General JScript -->
		
        
		<!-- Script for jquery fileuploader -->
		{{ HTML::script('/assets/js/fileupload/jquery.ui.widget.js') }}
		<!-- {{ HTML::script('/assets/js/fileupload/tmpl.js') }} -->
		<!-- {{ HTML::script('/assets/js/fileupload/load-image.min.js') }} -->
		<!-- {{ HTML::script('/assets/js/fileupload/canvas-to-blob.min.js') }} -->
		{{ HTML::script('/assets/js/fileupload/jquery.iframe-transport.js') }}
		{{ HTML::script('/assets/js/fileupload/jquery.fileupload.js') }}
		<!-- {{ HTML::script('/assets/js/fileupload/jquery.fileupload-process.js') }} -->
		<!-- {{ HTML::script('/assets/js/fileupload/jquery.fileupload-ui.js') }} -->
		<!-- {{ HTML::script('/assets/js/fileupload/locale.js') }} -->
		
		{{ HTML::script('/assets/js/jquery.validationEngine.js') }}
		{{ HTML::script('/assets/js/jquery.validationEngine-en.js') }}
		{{ HTML::script('/assets/js/text-area-autosize.js') }}
		{{ HTML::script('/assets/js/FileSizeConverter.js') }}
		{{ HTML::script('/assets/js/file-upload-generic.js') }}

		<!-- NTE -->
		{{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.autocomplete.js') }}
		<!-- {{ HTML::script('/assets/js/nte/ajax-file-browse.js') }}-->
		
		

    @show
@yield('js_ko')

</body>
</html>
