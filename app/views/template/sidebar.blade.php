<div class="noprint">
	<div class="nav_tab_container metro leftfloat">
		<nav class="sidebar light">
			<ul>
				<li>
					<a href="javascript::void(0)" id="create_overtime" onfocus="this.blur();">Create Overtime</a>
				</li>
				
				<li>
					<a href="javascript::void(0)" id="create_taf" onfocus="this.blur();">Create TAF</a>
				</li>
				
				
				<?php if(Session::get("is_svp_for_cs")): ?>
				<li>
					<a href="javascript::void(0)" id="create_itr" onfocus="this.blur();">Create ITR</a>
				</li>
				 <?php endif ?>
					
				
				<?php if (Session::get("is_citd_employee")): ?>
				
				<li>
					<a href="javascript::void(0)" id="custom_itr" onfocus="this.blur();">Create Custom ITR</a>
				</li>
				
				<?php endif; ?> 
				
				<li>
					<a href="javascript::void(0)" id="create_sap_access" onfocus="this.blur();">Create SAP Access Request</a>
				</li>
				
				<li>
					<a href="<?php echo $base_url ?>/cbr/create" onfocus="this.blur()">Create CBR</a>
				</li>
				

				@if(in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
				<li>
					<a href="<?php echo $base_url ?>/pmarf/create" onfocus="this.blur()">Create PMARF</a>
				</li>
				@endif
				
				
				
				<?php if(Session::get("desig_level") == "head"): ?>
				<li> 
					<a href="<?php echo $base_url ?>/te/create" onfocus="this.blur()">Create TE</a>
				</li>
				<?php endif; ?>
				

			
				
				<li> 
					<a href="<?php echo $base_url ?>/lrf/create" onfocus="this.blur()">Create Leave</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/ns/create" onfocus="this.blur()">Create Notification</a>
				</li>
				@if( in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
	                <li>
	                    <a href="<?php echo $base_url ?>/lrf/pe_create" onfocus="this.blur()">Create Leave (Production Encoder)</a>
	                </li>

	                <li>
	                    <a href="<?php echo $base_url ?>/ns/pe_create" onfocus="this.blur()">Create Notification (Production Encoder)</a>
	                </li>
	            @endif
				
				@if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)) || in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
					<li>
						<a href="<?php echo $base_url ?>/msr/create" onfocus="this.blur()">Create MSR</a>
					</li>
            	@endif
            	
				@if(Session::get('is_chrd_aer_staff') == 1 || Session::get('is_chrd_aer_staff') == 2 || Session::get('is_chrd_aer_staff') == 3 || Session::get('is_chrd_aer_staff') == 4 || Session::get('is_chrd_aer_staff') == 5)
				<li> 
					<a href="<?php echo $base_url ?>/nte/nte/create" onfocus="this.blur()">Create Notice to Explain</a>
				</li>

				<li> 
					<a href="<?php echo $base_url ?>/nte/nda/create" onfocus="this.blur()">Create Notice of Disciplinary Action</a>
				</li>
					@if (Session::get('is_chrd_aer_staff') < 4)
						<li>
							<a href="<?php echo $base_url ?>/nte/crr/list" onfocus="this.blur()">CRR Build-up</a>
						</li>
					@endif
				@endif
				
				
				
				@if(Session::get("is_cc_cbr_filer"))
				<li> 
					<a href="<?php echo $base_url ?>/cc/create" onfocus="this.blur()">Create Clearance Certification</a>
				</li>
				@endif
				
				<li> 
					<a href="{{ url('gd/create') }}" onfocus="this.blur()">Create General Document</a>
				</li>
				
				@if( Session::get('company') == "RBC-CORP" )
				<li> 
					<a href="{{ url('art/create') }}" onfocus="this.blur()">Create Art Design</a>
				</li>
				@endif
				
				
				<li> 
					<a href="{{ url('os/create') }}" onfocus="this.blur()">Create Office Sales</a>
				</li>
				
				<li> 
					<a href="{{ url('svr/create') }}" onfocus="this.blur()">Create Service Vehicle Request</a>
				</li>

				<li>
					<a href="<?php echo $base_url ?>/moc/create" onfocus="this.blur()">Create PSR/MOC</a>
				</li>
				
				@if(Session::get("company") == "RBC-CORP")
				<li> 
					<a href="<?php echo $base_url ?>/pmf/create" onfocus="this.blur()">Create PMF</a>
				</li>
				@endif

				
				{{--			@if(Session::get("company") != "RBC-CORP" && Session::get("desig_level") == "head")--}}
				<li>
					<a href="<?php echo $base_url ?>/mrf/create" onfocus="this.blur()">Create MRF</a>
				</li>
			{{--@endif--}}


				<li>
					<a href="{{route('nte.list', 'Submitted')}}">Submitted Notice to Explain</a>
				</li>
				<li>
					<a href="{{route('nda.list', 'Submitted')}}">Submitted Notice of Disciplinary Action</a>
				</li>
				
				
				<?php if (Session::get("is_pmf_receiver") != "") : ?>
				<li>
					<a href="<?php echo $base_url ?>/pmf/submitted" onfocus="this.blur()">Submitted PMF</a>
				</li>
				<?php endif ?>

				<?php if(Session::get("is_taf_receiver")): ?>
				<li>
					<a href="#"></i>Submitted TAF</a>
				</li>
				<?php endif ?>


				@if(Session::get('is_pmarf_npmd_head') || Session::get('is_pmarf_npmd_personnel') )
						<li>
							<a href="<?php echo $base_url ?>/pmarf/submitted-pmarf-requests" onfocus="this.blur()">Submitted PMARF</a>
						</li>
				@endif



				<?php if(Session::get('is_te_chrd')) : ?>
						<li>
							<a href="<?php echo $base_url ?>/te/submitted-te-requests" onfocus="this.blur()">Submitted TE Request</a>
						</li>
				<?php endif; ?>


				<?php if (Session::get("is_itr_bsa") ): ?>
				
				<li>
					<a href="javascript::void(0)" id="assess_itr" onfocus="this.blur();">Submitted ITR</a>
				</li>
				
				<?php endif; ?>
				
				<?php if (Session::get("is_system_satellite") ): ?>
					 
				<li>
					<a href="javascript::void(0)" id="assess_itr_satellite" onfocus="this.blur();">Submitted ITR</a>
				</li>
				<?php endif; ?>

				<?php if(Session::get('is_cbr_receiver')  || Session::get('is_cbr_staff')  ): ?>
					<li>
						<a href="<?php echo $base_url ?>/cbr/submitted-cbr-requests" onfocus="this.blur()">Submitted CBR Request</a> 
					</li>
				<?php endif; ?>


				@if(Session::get("is_lrf_receiver") || Session::get("is_notif_receiver"))
					<li>
						<a href="<?php echo $base_url ?>/submitted/leaves_notifications" onfocus="this.blur()">Submitted Applications</a>
					</li>
					@if(Session::get("company") != "RBC-CORP")
						<li>
							<a href="<?php echo $base_url ?>/submitted/pe_leaves_notifications" onfocus="this.blur()">Submitted Applications (Production Encoders)</a>
						</li>
					@endif
				@endif
				
				@if(Session::get("is_msr_receiver"))
					@if(! in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
						<li>
							<a href="<?php echo $base_url ?>/msr/submitted_msr" onfocus="this.blur()">Submitted MSR (MKTG)</a>
						</li>
					@endif
					<li>
						<a href="<?php echo $base_url ?>/msr/smis/submitted_msr" onfocus="this.blur()">Submitted MSR (SMIS)</a>
					</li>
				@endif

				@if((in_array(Session::get('dept_id'),[CSMD_DEPT_ID]) && Session::get('desig_level') != 'employee') || Session::get('is_bsa') || Session::get('is_ssmd'))
					<li>
						<a href="<?php echo $base_url ?>/moc/submitted-moc" onfocus="this.blur()">Submitted PSR/MOC</a>
					</li>
				@endif

				<?php if (Session::get("dept_id") == 17) : ?>
				<li> 
					<a href="{{ url('art/reviewer/report') }}" onfocus="this.blur()">Submitted Art Design</a>
				</li>
				<?php endif ?>

				
				<?php if(Session::get("is_service_vehicle_receiver")): ?>
				<li> 
					<a href="<?php echo $base_url ?>/svr/reviewer" onfocus="this.blur()">Submitted Service Vehicle</a>
				</li>
				<?php endif; ?>		

				<?php if(Session::get("is_office_receiver")): ?>
				<li> 
					<a href="{{ url('os/receiver/report') }}" onfocus="this.blur()">Submitted Office Sales</a>
				</li>

				<li> 
					<a href="{{ url('os/food/report') }}" onfocus="this.blur()">Product Build-up</a>
				</li>

				<li> 
					<a href="{{ url('os/export/employees/food') }}" onfocus="this.blur()">Export Office Sales</a>
				</li>

				<li> 
					<a href="{{ url('os/export/affiliate') }}" onfocus="this.blur()">Export Office Sales(Affiliate)</a>
				</li>
				@endif


				<?php if(Session::get("is_office_receiver_non_food")): ?>
				<li> 
					<a href="{{ url('os/receiver/report') }}" onfocus="this.blur()">Submitted Office Sales</a>
				</li>

				<li> 
					<a href="{{ url('os/non-food/report') }}" onfocus="this.blur()">Project Build-up</a>
				</li>

				<li> 
					<a href="{{ url('os/export/employees/non_food') }}" onfocus="this.blur()">Export Office Sales</a>
				</li>

				@endif

			{{--MRF--}}

			<li>
				<a href="<?php echo $base_url ?>/mrf/list-view/submitted" onfocus="this.blur()">Submitted MRF</a>
			</li>
			{{--MRF--}}
				
				<?php if(Session::get("is_audit_receiver")): ?>
				<li> 
					<a href="{{ url('audit') }}" onfocus="this.blur()">Audit Trail</a>
				</li>

				@endif
				
			</ul>
		</nav>
	</div>
</div>