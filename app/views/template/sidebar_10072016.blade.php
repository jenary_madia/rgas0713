<div class="nav_tab_container metro leftfloat">

	<nav class="sidebar light">
		<ul>
            <li>
                <a href="javascript::void(0)" id="create_overtime" onfocus="this.blur();">Create Overtime</a>
            </li>
            
			<li>
				<a href="javascript::void(0)" id="create_taf" onfocus="this.blur();">Create TAF</a>
			</li>
            
            <?php if(Session::get("is_taf_receiver")): ?>
			<li>
                <a href="#"></i>Submitted TAF</a>
			</li>
            <?php endif ?>
			
            <li>
				<a href="javascript::void(0)" id="create_itr" onfocus="this.blur();">Create ITR</a>
			</li>
            
            <?php if (Session::get("is_itr_bsa") ): ?>
            
            <li>
				<a href="javascript::void(0)" id="assess_itr" onfocus="this.blur();">Submitted ITR</a>
			</li>
            
            <?php endif; ?>
            
            <?php if (Session::get("is_system_satellite") ): ?>
                 
            <li>
				<a href="javascript::void(0)" id="assess_itr_satellite" onfocus="this.blur();">Submitted ITR</a>
			</li>
            <?php endif; ?>
            
			
            <?php if (Session::get("is_citd_employee")): ?>
            
            <li>
				<a href="javascript::void(0)" id="custom_itr" onfocus="this.blur();">Create Custom ITR</a>
			</li>
            
            <?php endif; ?> 
            
            <li>
				<a href="javascript::void(0)" id="create_sap_access" onfocus="this.blur();">Create SAP Access Request</a>
			</li>
            
            
		</ul>
    </nav>
	
</div>