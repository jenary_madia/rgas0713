<!doctype html>
<html>
	<head>
		<title>Rebisco General Approval System</title>
		
    @section('head')
		
		<!-- General Style -->
		{{ Minify::stylesheet(
			array('/assets/css/bootstrap.css'
				 ,'/assets/css/metro-bootstrap.css'
			     ,'/assets/css/main.css'
			     ,'/assets/css/employee.css'
                 ,'/assets/css/validation_engine.css'
                 ,'/assets/css/datepicker.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
			))->withFullUrl()
		}}
		

		<base href="<?php echo $base_url; ?>" />
		
    @show
		
	</head>
	
    <body>
	
		<div class="company_logo">
			<img src="<?php echo $base_url;?>/assets/img/rebisco_logo.png" width="72" height="71" />
		</div>
		
		<div class="system_logo">
			<h4>Rebisco General Approval System</h4>
		</div>
		
		<?php if( Session::has("employee_id") ): ?>
		
			<div class="login_info">
				Welcome <?php echo ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname"))); ?>
				
				<div class="clear"></div>

				<?php echo Session::get("company_name"); ?>
			</div>
			<div class="clear"></div>
			
			@include('template/menubar')
			
			<hr class="header_hr" />
	
		<?php else: ?>
		
			<div class="clear"></div>
		
			<hr class="header_hr" />
            
		<?php endif; ?>
        
		@yield('content')
		
		<div class="clear_20"></div>
        
		<div id="footer">
		
			Copyright &copy; REBISCO 2014. All Rights Reserved.
			<br />
			System Provided by Corporate Information Technology Department 
			
		</div>
 
	</body>
	
    @section('js')

		
		<!-- General JScript -->
		{{ Minify::javascript(array(
			'/assets/js/jquery-1.10.1.min.js',
			'/assets/js/jquery-ui-1.10.4.custom.js',
			'/assets/js/jquery-migrate-1.2.1.min.js',
			'/assets/js/bootstrap.js',
			'/assets/js/jquery.mockjax.js',
			'/assets/js/bootstrap-typeahead.js',
			'/assets/js/bootstrap-datepicker.js',
			'/assets/js/bootstrap-timepicker.min.js',
			'/assets/js/jquery.dataTables.min.js',
			'/assets/js/datatables.fnReloadAjax.js',
			'/assets/js/login.js',
			'/assets/js/employee.js',
			'/assets/js/common.js'
		))->withFullUrl() }}
		
		
        
		<!-- Script for jquery fileuploader -->
		{{ Minify::javascript(array('/assets/js/fileupload/jquery.ui.widget.js',
			'/assets/js/fileupload/tmpl.js',
			'/assets/js/fileupload/load-image.min.js',
			'/assets/js/fileupload/canvas-to-blob.min.js',
			'/assets/js/fileupload/jquery.iframe-transport.js',
			'/assets/js/fileupload/jquery.fileupload.js',
			'/assets/js/fileupload/jquery.fileupload-process.js',
			'/assets/js/fileupload/jquery.fileupload-ui.js',
			'/assets/js/fileupload/locale.js',
            '/assets/js/jquery.validationEngine.js',
			'/assets/js/jquery.validationEngine-en.js',
			'/assets/js/text-area-autosize.js'
		))->withFullUrl() }}
        
    @show
	
</html>
