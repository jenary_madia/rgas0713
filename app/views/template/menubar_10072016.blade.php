
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

	<ul class="nav navbar-nav menu_bar">

		<li class="active">
			<a href="#" onfocus="this.blur()">Home</a>
		</li>
        
        <li>
			<a href="<?php echo $base_url ?>/employees/list" onfocus="this.blur()">Employees List</a>
		</li>

		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  Admin / Gen Doc <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">Leave</a>
					<a href="#">Travel Authorization</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CHRD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">Performance Management Form</a>
				</li>
			</ul>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CSMD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">MOC / Process Service Request</a>
				</li>
			</ul>
		</li>
        
        <li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#" onfocus="this.blur()">
			  CITD <span class="caret"></span>
			</a>

			<ul class="dropdown-menu" role="menu">
				<li>
					<a href="#">IT Service Request</a>
				</li>
			</ul>
		</li>
		
	</ul>
	
	<ul class="nav navbar-nav navbar-right menu_bar">
		<li>
			<a href="<?php echo $base_url ?>/sign-out">Log out</a>
		</li>
	</ul>

</div>