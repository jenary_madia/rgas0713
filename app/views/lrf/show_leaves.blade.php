<!DOCTYPE html>
<html lang="en">
<style>
	.bg-red {
		background: red;
	}
</style>
<body id="home">

	<table border="1" style="text-transform: uppercase; font-family: Arial">
		<thead>
			<tr>
				@if($type != "HV")
				<th></th>
				@else
					<th class="bg-red">TYPE OF LEAVE</th>
				@endif
				<th class="bg-red">LEAVE ENTITLEMENT (IN DAYS)</th>
				<th class="bg-red">LEAVE USED (IN DAYS)</th>
			</tr>
		</thead>
		<tbody>
			@foreach($leaveTypes as $type)
				@if($type->item == "HomeVisit")
					<tr>
						<td>
							{{ $type->text }}
							(FOR EXPATS ONLY)
						</td>
						<td>
							<table border="1" style="width: 100%;">
								<tr>
									<th>Q1</th>
									<th>Q2</th>
									<th>Q3</th>
									<th>Q4</th>
								</tr>
								<tr>
									<td>7</td>
									<td>7</td>
									<td>7</td>
									<td>7</td>
								</tr>
							</table>

						</td>
						<td>
							<table border="1" style="width: 100%;">
								<tr>
									<th>Q1</th>
									<th>Q2</th>
									<th>Q3</th>
									<th>Q4</th>
								</tr>
									<tr>
										@foreach($HomeVisits as $HV)
												<td>{{ ($HV ? $HV : 0) }}</td>
										@endforeach
									</tr>
							</table>
						</td>
					</tr>
				@else
	 				<tr>
						<td>{{ $type->text }}</td>
						<td>
						 	@if($type->item == "Birthday")
						 		1
					 		@elseif($type->item == "Sick" || $type->item == "Vacation")
					 			18
				 			@endif
						</td>
							@foreach($LeaveApprovedTotal as $total)
								@if($total["appliedfor"] == $type->item)
									<td>
										{{ $total["sum"] }}
									</td>
								@endif
							@endforeach
					</tr>
				@endif
			@endforeach
		</tbody>
		<tfoot style="border: 0;">
			<tr>
				<td colspan="3" style="border: 0;">Note</td>
			</tr>
			<tr>
				<td style="border: 0;"></td>
				<td colspan="2" style="border: 0;">For Maternity</td>
			</tr> 
			<tr>
				<td style="border: 0;"></td>
				<td style="text-align:center; border: 0;">Normal Delivery</td>
				<td style="border: 0;">60</td>
			</tr> 
			<tr>
				<td style="border: 0;"></td>
				<td style="text-align:center; border: 0;">caesarean section</td>
				<td style="border: 0;">78</td>
			</tr> 
			<tr>
				<td style="border: 0;"></td>
				<td colspan="2" style="border: 0;">For Paternity</td>
			</tr> 
			<tr>
				<td style="border: 0;"></td>
				<td style="text-align:center; border: 0;">First 5 births</td>
				<td style="border: 0;">7</td>
			</tr> 
			<tr>
				<td style="border: 0;"></td>
				<td style="text-align:center; border: 0;">Succeeding birth</td>
				<td style="border: 0;">5</td>
			</tr> 
		</tfoot>
	</table>

</body>
</html>