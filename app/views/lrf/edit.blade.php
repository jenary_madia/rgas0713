@extends('template/header')

@section('content')
    <div id="leave" v-cloak>
        {{--FOR for message prompt--}}
        <div class="alert alert-success" v-if="submitStatus.submitted && submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div id="global_message" class="alert alert-danger" v-if="submitStatus.submitted && ! submitStatus.success">
            <ul>
                <li v-for="error in submitStatus.errors">@{{ error }}</li>
            </ul>
        </div>
        {{ Form::open(array('url' => 'lrf/action', 'method' => 'post', 'files' => true,'id' => 'formCreateLRF')) }}
        <div class="form_container">
            <div class="container-header"><h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5></div>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NAME:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.employee_name" value="{{ Session::get('employee_name') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" value="{{ $leave_details['documentcode'].'-'.$leave_details['codenumber'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.employee_number" value="{{ Session::get('employeeid') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.date_filed" value="{{ $leave_details['datecreated'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">COMPANY:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.company" value="{{ Session::get('company') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">STATUS:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.status" value="NEW" />
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" v-model="form_details.department" value="{{ Session::get('dept_name') }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">CONTACT NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" v-model="form_details.contact_number" maxlength="20" value="{{ $leave_details['contact_no'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" maxlength="25" v-model="form_details.section" value="{{ $leave_details['section'] }}"/ />
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">TYPE OF LEAVE:</label>
                    </div>
                    <div class="col2_form_container">
                        {{ Form::select('size', $leave_types, $leave_details['appliedfor'],["v-model"=>"request_details.leave_type","class"=>"form-control text-uppercase"]) }}
                    </div>
                    @include('lrf.modals.home-visit-error')
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="container-header" v-if="! request_details.leave_type"><label class="labels required">SELECT A LEAVE TYPE</label></div>
            <div v-show="request_details.leave_type">
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined sub-header"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">TYPE OF SCHEDULE:</label>
                        </div>
                        <div class="col2_form_container">
                            {{ Form::select('size', ['compressed' => 'Compressed', 'regular' => 'Regular', 'special' => 'Special'], $leave_details['schedule_type'],["v-model"=>"request_details.schedule_type","class"=>"form-control text-uppercase"]) }}
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels labelRestday">REST DAY:</label>
                        </div>
                        <div class="col2_form_container">
                            <select name="lrfRestDay[]" v-model="request_details.rest_day" id="lrfRestDay" class="text-uppercase" multiple="multiple"
                                {{ $leave_details['schedule_type'] != 'special' ? 'disabled' : '' }}
                            >
                                @for ($i = 0; $i < 7; $i++)
                                    <option value="{{ $i }}"
                                            {{ in_array($i,json_decode($leave_details['rest_day'])) ? 'selected' : '' }}
                                    >
                                        @if( $i == 0) Sunday
                                        @elseif( $i == 1) Monday
                                        @elseif( $i == 2) Tuesday
                                        @elseif( $i == 3) Wednesday
                                        @elseif( $i == 4) Thursday
                                        @elseif( $i == 5) Friday
                                        @elseif( $i == 6) Saturday
                                        @endif
                                    </option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear_20"></div>
            <div v-show="request_details.leave_type">
                <div class="container-header"><h5 class="text-center lined sub-header"><strong>LEAVE DETAILS</strong></h5></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DURATION OF LEAVE:</label>
                        </div>
                        <div class="col2_form_container">
                            {{ Form::select('', ['half'=>'Half Day','whole'=>'One(1) Day','multiple' => 'Multiple Days'], $leave_details['duration'],["v-model"=>"request_details.duration","class"=>"form-control text-uppercase"]) }}
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col2_form_container">
                            @if(in_array(Session::get("employee_id"),json_decode(EXPATRIATES,true)))
                                <a class="btn btn-default btndefault" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.Session::get("employee_id").'/hv')}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
                            @else
                                <a class="btn btn-default btndefault" id="showLeaves" onClick="MyWindow=window.open('{{url('lrf/show_leaves/'.Session::get("employee_id"))}}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
                            @endif
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-4 bordered">
                            <label class="labels">FROM:</label>
                            <br>
                            <label class="labels required">DATE:</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" value="{{ $leave_details['from'] }}" v-model="request_details.from_date" class="date_picker form-control input-small" id="lrfDateFrom" name="lrfDateFrom"/>
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>

                            <div class="clear_10"></div>
                            <label class="labels required">LEAVE PERIOD:</label>
                            {{ Form::select('', [''=>'--Choose Leave Period--','whole'=>'whole','half' => 'Half'], $leave_details['period_from'] == 1 ? 'half' : 'whole',[":disabled"=>"request_details.duration != 'multiple'", "v-model"=>"request_details.from_period","class"=>"form-control pull-right text-uppercase"]) }}
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 bordered" v-show="request_details.duration == 'multiple' ">
                            <label class="labels">TO:</label>
                            <br>
                            <label class="labels required">DATE:</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" value="{{ $leave_details['to'] }}" v-model="request_details.to_date" class="form-control date_picker" name="lrfDateTo" id="lrfDateTo"/>
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                            <div class="clear_10"></div>
                            <label class="labels required">LEAVE PERIOD:</label>
                            {{ Form::select('', [''=>'--Choose Leave Period--','whole'=>'whole','half' => 'Half'], $leave_details['period_to'] == 1 ? 'half' : 'whole',["v-model"=>"request_details.to_period","class"=>"form-control pull-right text-uppercase"]) }}

                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <div class="clear_20"></div>
                    <div class="clear_20"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">TOTAL LEAVE DAYS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input min="0" type="number" max="99"  readonly step="any" v-model="request_details.total_days" value="@{{ getTotalDays }}" name="lrfTotalLeaveDays" id="lrfTotalLeaveDays" class="form-control"/>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="textarea_messages_container" style="border: 0;">
                            <div class="row">
                                <label class="textarea_inside_label required">REASON:</label>
                                <textarea rows="3" class="form-control textarea_inside_width" name="lrfReason" v-model="request_details.reason" >{{ $leave_details['reason'] }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="attachment_container document_form">
                            <p><strong class="labels">FILER : </strong></p>
                            <div id="med_cert"
                                 @if(Session::get("company") != "RBC-CORP")
                                 v-show="(request_details.total_days >= 3 && request_details.leave_type == 'Sick') || request_details.leave_type == 'Mat/Pat'"
                                 @else
                                 hidden
                                    @endif
                            >
                                <label class="attachment_note required">MEDICAL CERTIFICATE:
                                    <span class="btn btn-success btnbrowse fileinput-button">
                                        <span>BROWSE</span>
                                        <input id="med_fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                    </span>
                                </label>
                                <div id="med_attachments">
                                    @if($leave_details["clinic_attachment"])
                                        <p>
                                            {{ json_decode($leave_details["clinic_attachment"],true)["original_filename"].' | '.json_decode($leave_details["clinic_attachment"],true)["filesize"]  }}
                                            <input type='hidden' class='med_attachmentData' value='{{ $leave_details["clinic_attachment"] }}'>
                                            <button class='btn btn-sm btn-danger remove-fn confirm-delete'>DELETE</button>
                                        </p>
                                    @endif
                                </div>
                                <br>
                            </div>
                            <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                            <?php $count = 1 ?>
                            <div id="attachments">
                                @for ($i = 1; $i <= 5 ; $i++)
                                    @if($leave_details["attach$i"])
                                        <p>
                                            {{ json_decode($leave_details["attach$i"],true)["original_filename"].' | '.json_decode($leave_details["attach$i"],true)["filesize"]  }}
                                            <input type='hidden' class='attachmentData' value='{{ $leave_details["attach$i"] }}'>
                                            <button class='btn btn-sm btn-danger remove-fn confirm-delete'>DELETE</button>
                                        </p>
                                    @endif
                                @endfor
                            </div>
                            <span class="btn btn-success btnbrowse fileinput-button">
                                <span>BROWSE</span>
                                <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                            </span>
                        </div>
                    </div>
                    @if($leave_details['receiver_attachments'])
                        <div class="col-md-6">
                            <div class="attachment_container document_form">
                                <p><strong>CHRD : </strong></p>
                                @foreach(json_decode($leave_details['receiver_attachments'],true) as $key)
                                    <a href="{{ URL::to('/ns/download'.'/'.$leave_details['documentcode'].'-'.$leave_details['codenumber'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key["original_filename"])) }}">{{ $key["original_filename"] }}</a><br />
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            @if($leave_details['status'] == 'NEW')
                <div class="textarea_messages_container">
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment" v-model="comment">{{ json_decode($leave_details['comment'],true)['message'] }}</textarea>
                    </div>
                </div>
            @else
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$leave_details['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                    </div>
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width"  v-model="comment" name="comment"></textarea>
                    </div>
                </div>
            @endif
            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div>
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" @click.prevent="recreate('resave',{{ $leave_details['id'] }})">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        @if(Session::get("desig_level") == "president")
                            <label class="button_notes"><strong>SEND</strong> TO HR FOR PROCESSING
                        @else
                            @if(Session::get("company") != "RBC-CORP")
                                <label class="button_notes" v-if="(request_details.total_days >= 3 && request_details.leave_type == 'Sick') || request_details.leave_type == 'Mat/Pat'"><strong>SEND</strong>TO CLINIC FOR APPROVAL</label>
                                <label class="button_notes" v-else><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                            @else
                                <label class="button_notes"><strong>SEND</strong>TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                            @endif
                        @endif
                    </div>
                    <div class="comment_button">
                        <button type="submit" id="btnSend" class="btn btn-default btndefault" @click.prevent="recreate('resend',{{ $leave_details['id'] }})">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

        {{ Form::close() }}
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/bootstrap-multiselect.js') }}
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/leaves/leaveCreateFunctions.js') }}
    {{ HTML::script('/assets/js/leaves/vue-scripts.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });

        var med_file_counter = 0;
        var med_allowed_file_count = 1;
        var med_allowed_total_filesize = 20971520;


        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;
    </script>
    {{ HTML::script('/assets/js/leaves/file-upload.js') }}
@stop