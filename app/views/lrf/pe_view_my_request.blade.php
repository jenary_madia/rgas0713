@extends('template.header')

@section('content')
    <div id="peISView" v-cloak>
        {{ Form::open(array('url' => array('lrf/pe_action',$leaveBatch['id']), 'method' => 'post', 'files' => true)) }}
        <div class="form_container">
            <h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DEPARTMENT</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['department']['dept_name'] }}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">REFERENCE NUMBER</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="reference" readonly value="{{ $leaveBatch['documentcode'].'-'.$leaveBatch['codenumber'] }}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['section']['sect_name'] }}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" disabled value="{{ $leaveBatch['datecreated'] }}">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center text-uppercase">Employee Name</th>
                                <th class="text-center text-uppercase">Employee Number</th>
                                <th class="text-center text-uppercase">Leave Type</th>
                                <th class="text-center text-uppercase">Duration of Leave</th>
                                <th class="text-center text-uppercase">Total Leave Days</th>
                                <th class="text-center text-uppercase">Reason</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--<input type="hidden" value='{{ $leaveBatch['batchDetails'] }}' v-model="batchDetails">--}}
                            <tr v-for="employee in employeesToFile">
                                <td class="text-center text-uppercase" >@{{ employee.employeeName }}</td>
                                <td class="text-center text-uppercase" >@{{ employee.employeeNumber }}</td>
                                <td class="text-center text-uppercase" >@{{ employee.leaveName }}</td>
                                <td class="text-center text-uppercase" >@{{ durationLeave($index) }}</td>
                                <td class="text-center text-uppercase" >@{{ employee.totalDays }}</td>
                                <td class="text-center text-uppercase" >@{{ employee.reason }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clear_10"></div>
                    {{--<div class="col-md-12" style="margin-left: 30px;">--}}
                        {{--<a class="btn btn-default btndefault" disabled>ADD</a>--}}
                        {{--<a class="btn btn-default btndefault" :disabled="! hasChosen" data-toggle="modal" data-target="#editNotifDetails">EDIT</a>--}}
                        {{--<button class="btn btn-default btndefault" disabled>DELETE</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>
        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            @if($leaveBatch['status'] == 'NEW')
                <div class="textarea_messages_container">
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">@foreach (explode('|',$leaveBatch['comment']) as $message){{ json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                    </div>
                </div>
            @else
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$leaveBatch['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                    </div>
                    <div class="clear_20"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" {{ ( $leaveBatch['status'] == 'FOR APPROVAL' ? "" : "disabled") }} class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>
            @endif
            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <button value="cancel" name="action" class="btn btn-default btndefault" style="width:100px;" {{ $leaveBatch['status'] == 'FOR APPROVAL' ? "" : "disabled" }}>Cancel Request</button>
                        </div>
                        <div class="col-md-6 text-center">
                            <a class="btn btn-default btndefault" href="{{ url('/lrf/leaves') }}">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
    {{ HTML::script('/assets/js/leaves/vue-pe-ISview.js') }}

@stop