<div class="modal fade" id="homeVisitError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog homeVisitError" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <p>You have exceeded the maximum number of Home Visitation Leave per quarter. Kindly file the excess days under a different type of a leave. </p>
                <button type="button" class="btn btn-default btn-xs" @click="resetLeaveForm">EDIT LEAVE DETAILS</button>
            </div>
        </div>
    </div>
</div>