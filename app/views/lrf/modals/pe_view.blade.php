<div class="modal fade" id="EditLeaveDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog EditLeaveDetails" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Leave Details</h4>
            </div>
            <div class="modal-body">
                <form class="form-inline">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="" class="labels required">EMPLOYEE NAME</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control" readonly value="@{{ toEdit.employeeName }}">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">EMPLOYEE NUMBER</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-xs" readonly value="@{{ toEdit.employeeNumber }}">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">LEAVE TYPE</label>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" value="@{{ toEdit.leaveType }}" class="form-control text-uppercase" disabled>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div id="divLeaveDetails">
                        <div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="labels required">DURATION OF LEAVE:</label>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input type="text" value="@{{ toEdit.duration }}" class="form-control text-uppercase" disabled>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4 well col-md-offset-1">
                                <label class="labels">FROM:</label>
                                <br>
                                <label class="labels required">DATE:</label>

                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input readonly="readonly" type="text"  value="@{{ toEdit.fromDate }}" class="form-control text-uppercase" id="lrfDateFrom" name="lrfDateFrom"/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>

                                <div class="clear_10"></div>
                                <label class="labels required">Leave Period:</label>
                                <div class="form-group">
                                    <input type="text" value="@{{ toEdit.fromPeriod }}" class="form-control text-uppercase" disabled>
                                </div>
                            </div>
                            <div class="col-md-4 divTo well col-md-offset-1" v-show="toEdit.duration == 'multiple'">
                                <label class="labels">TO:</label>
                                <br>
                                <label class="labels required">DATE:</label>

                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input readonly="readonly" type="text" value="@{{ toEdit.toDate }}" class="form-control text-uppercase"/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>

                                <div class="clear_10"></div>
                                <label class="labels required">LEAVE PERIOD:</label>
                                <div class="form-group">
                                    <input type="text" value="@{{ toEdit.toPeriod }}" class="form-control text-uppercase" disabled>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-12">
                                <label class="labels required">TOTAL LEAVE DAYS:</label>
                                <input min="0" type="number" max="99"  readonly name="lrfTotalLeaveDays" value="@{{ toEdit.totalDays }}" id="lrfTotalLeaveDays" class="form-control text-uppercase"/>
                            </div>
                            <div class="col-md-12">
                                <label class="labels required">REASON:</label>
                                <textarea rows="3" class="form-control text-uppercase" name="lrfReason" disabled>@{{ toEdit.reason }}</textarea>
                            </div>
                        </div>
                        <input type="checkbox" v-model="toEdit.noted"> noted by
                    </div>
            </div>
            <div class="modal-footer">
                @if(Session::get("desig_level") == "head")
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" @click="saveEdit(toEdit.id,toEdit.employeeNumber,{{ $leaveBatch['id'] }})" data-dismiss="modal">Save</button>
                @else
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" @click="saveEdit(toEdit.id,toEdit.employeeNumber,{{ $leaveBatch['id'] }})">Save</button>
                @endif
            </div>
        </div>
    </div>
</div>