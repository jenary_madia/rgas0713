@extends('template/header')

@section('content')
	{{ Form::open(array('url' => array('lrf/processing',$leaveDetails['id']), 'method' => 'post', 'files' => true,'id' => 'formCreateLRF')) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
                <div class="container-header"><h5 class="text-center"><strong>LEAVE APPLICATION FORM</strong></h5></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                            <input type="hidden" name="lrfOwnerID" value="{{ $leaveDetails['ownerId'] }}"/>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['fullName'] }}"/>
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['referenceNo'] }}" />
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['employeeId'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['dateCreated'] }}" />
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['company'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['status'] }}" />
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $leaveDetails['department'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" readonly value="{{ $leaveDetails['contactNo'] }}"/>
                        </div>
                    </div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" value="{{ $leaveDetails['section'] }}" readonly/>
                        </div>
                    </div>

					<div class="clear_10"></div>
					<div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">TYPE OF LEAVE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control text-uppercase" readonly 
                            
                                @if ($leaveDetails["leaveType"] == "Birthday")
                                    value="Birthday Leave"
                                @elseif ($leaveDetails["leaveType"] == "HomeVisit")
                                    value="Home Visitation Leave"
                                @elseif ($leaveDetails["leaveType"] == "Emergency")
                                    value="Emergency Leave"
                                @elseif ($leaveDetails["leaveType"] == "Mat/Pat")
                                    value="Maternity/Paternity Leave"
                                @elseif ($leaveDetails["leaveType"] == "Sick")
                                    value="Sick Leave"
                                @elseif ($leaveDetails["leaveType"] == "Vacation")
                                    value="Vacation Leave"
                                @endif

                            />
                        </div>
                    </div>
                </div>
                <div class="LRFAdditionalFields">
                    <div {{ ($leaveDetails['leaveType']  == "HomeVisit" ? "hidden" : "" ) }}>
                        <div class="clear_20"></div>
                        <div class="container-header"><h5 class="text-center lined"><strong>WORK WEEK SCHEDULE DETAILS</strong></h5></div>

                        <div class="row">
                            <div class="row_form_container">
                                <div class="col1_form_container">
                                    <label class="labels required">TYPE OF SCHEDULE:</label>
                                </div>
                                <div class="col2_form_container">
                                    <input type="text" readonly class="form-control text-uppercase" value="{{ $leaveDetails['scheduleType'] }}" />
                                </div>
                            </div>
                            <div class="clear_10"></div>
                            <div class="row_form_container">
                                <div class="col1_form_container">
                                    <label class="labels">REST DAY:</label>
                                </div>
                                <div class="col2_form_container">
                                    <input type="text" class="form-control text-uppercase" value='{{ $leaveDetails['restDay'] }}' readonly/>
                                </div>
                            </div>
                        </div>
                    </div>
	             	<div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined"><strong>LEAVE DETAILS</strong></h5></div>
					<div class="row">
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">DURATION OF LEAVE:</label>
	                        </div>
	                        <div class="col2_form_container">
                            	<input type="text" class="form-control text-uppercase" 

                            		@if($leaveDetails["duration"] =="half")
                            			value="Half Day" 
                            		@elseif($leaveDetails["duration"] =="whole")
                            			value="Whole Day" 
                            		@else
                            			value="Multiple" 
                        			@endif
                            	
                            	readonly/>
	                        </div>
	                    </div>
	                    <div class="row_form_container">
	                        <div class="col2_form_container">
	  							<a class="btn btn-default btndefault" onClick="MyWindow=window.open('{{ ($leaveDetails["leaveType"] == "HomeVisit" ? url('lrf/show_leaves/'.$leaveDetails['ownerId'].'/hv') : url('lrf/show_leaves/'.$leaveDetails['ownerId'])) }}','MyWindow',width=200,height=200); return false;">SHOW APPROVED LEAVES FOR THE YEAR</a>
	                        </div>
	                    </div>
	                    <div class="clear_10"></div>
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4 bordered">
                                <div class="col2_form_container pull-left">
                                    <label class="labels">FROM:</label>
                                    <br>
                                    <label class="labels required">DATE:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" class="form-control" id="lrfDateFrom" value='{{ $leaveDetails['from'] }}'/>
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    <div class="clear_10"></div>
                                    <label class="labels required">LEAVE PERIOD:</label>
                                    <input readonly="readonly" type="text" class="form-control text-uppercase" id="lrfLeavePeriodFrom" value='{{ ($leaveDetails['periodFrom'] == 1 ? "half" : "whole") }}'/>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-4 divTo bordered" {{ ($leaveDetails["duration"] != "multiple" ? "hidden" : "") }}>
                                <div class="col2_form_container">
                                    <label class="labels">TO:</label>
                                    <br>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input readonly="readonly" type="text" class="form-control" id="lrfDateTo" value="{{ $leaveDetails['to'] }}"/>
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                    <label class="labels required">DATE:</label>

                                    <div class="clear_10"></div>
                                    <label class="labels required">LEAVE PERIOD:</label>
                                    <input readonly="readonly" type="text" class="form-control text-uppercase" id="lrfLeavePeriodTo" value='{{ ($leaveDetails['periodTo'] == 1 ? "half" : "whole") }}'/>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
						<div class="clear_20"></div>
						<div class="clear_20"></div>
	                    <div class="row_form_container">
	                        <div class="col1_form_container">
	                            <label class="labels required">TOTAL LEAVE DAYS:</label>
	                        </div>
	                        <div class="col2_form_container">
	                            <input min="0" type="number" max="99"  readonly id="lrfTotalLeaveDays" class="form-control" value="{{ $leaveDetails["totalLeaveDays"] }}"/>
	                        </div>
	                    </div>
	                    <div class="clear_10"></div>
	                    <div class="row_form_container">
						    <div class="textarea_messages_container" style="border: 0;">
						        <div class="row">
						            <label class="textarea_inside_label required">REASON:</label>
						            <textarea rows="3" class="form-control textarea_inside_width" readonly>{{ $leaveDetails['reason'] }}</textarea>
						        </div>
						    </div>
	                    </div>
	                </div>
					<div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
					<div class="clear_20"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <p><strong class="labels">FILER : </strong></p>
                            @if($leaveDetails['clinic_attachment'])
                                MEDICAL CERFTIFICATE :
                                <br>
                                <a href="{{ URL::to('/lrf/download'.'/'.$leaveDetails['referenceNo'].'/'.$leaveDetails['clinic_attachment']["random_filename"].'/'.CIEncrypt::encode($leaveDetails['clinic_attachment']["original_filename"])) }}">{{ $leaveDetails['clinic_attachment']["original_filename"] }}</a><br />
                                <br>
                            @endif
                            ATTACHMENTS :
                            <br>
                            @for ($i = 1; $i < 6; $i++)
                                <a href="{{ URL::to('/lrf/download'.'/'.$leaveDetails['referenceNo'].'/'.json_decode($leaveDetails["attach$i"],true)["random_filename"]).'/'.CIEncrypt::encode(json_decode($leaveDetails["attach$i"],true)["original_filename"])}}">{{ json_decode($leaveDetails["attach$i"],true)["original_filename"] }}</a><br />
                            @endfor
                        </div>
                        @if($leaveDetails['receiver_attachments'])
                            <div class="col-md-6">
                                <p><strong>CHRD : </strong></p>
                                <div class="attachment_container">
                                    @foreach($leaveDetails['receiver_attachments'] as $key)
                                        <a href="{{ URL::to('/lrf/download'.'/'.$leaveDetails['referenceNo'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key["original_filename"])) }}">{{ $key["original_filename"] }}</a><br />
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                    <div {{ (count($leaveSignatories) == 0 ? "hidden" : "") }}>
                        <div class="clear_20"></div>
                        <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>

                        <br>
                        @foreach($leaveSignatories as $signatory)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col1_form_container">
                                        <label class="labels">{{ ($signatory["signature_type"] == 1 ? "NOTED" : "APPROVED") }} :</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group pull-right" style="width: 200px">
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </div>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                        @endforeach
                    </div>
                </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>
        <span class="action-label labels">ACTION</span>
        <div class="form_container">
            <div class="clear_20"></div>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach ($leaveDetails['message'] as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="lrfComment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                @if($method == 'approve')
                    <div>
                        @if($clinic_receiver && count($leaveSignatories) === 0)
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>SEND</strong> FOR APPROVAL</label>
                                </div>
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="clinicToIS">SEND</button>
                                </div>
                            </div>

                            <div class="clear_10"></div>
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                                </div>
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="return">RETURN</button>
                                </div>
                            </div>
                        @else
                            @if(in_array(Session::get("company"),json_decode(CORPORATE,true)))
                                <div class="comment_container" {{ in_array(Session::get("desig_level"),json_decode(APPROVERS_CORP,true)) ? "hidden" : "" }}>
                            @elseif(in_array(Session::get("company"),json_decode(MFC_ETC,true)))
                                <div class="comment_container" {{ in_array(Session::get("desig_level"),json_decode(LEAVE_APPROVERS_MFC_ETC,true)) ? "hidden" : "" }}>
                            @else
                                <div class="comment_container" {{ in_array(Session::get("desig_level"),json_decode(LEAVE_APPROVERS_OTHER_ETC,true)) ? "hidden" : "" }}>
                            @endif
                                <div class="comment_notes">
                                    <label class="button_notes pull-left" style=" margin: 5px 0 0 0"><strong>REQUEST</strong> FOR APPROVAL OF </label>
                                    <select class="form-control pull-left" style="width:250px; margin: 0 0 0 50px;" name="lrfOtherSuperiors">
                                        <option selected disabled></option>
                                        @foreach($otherApprovers as $approver)
                                            <option value="{{ $approver->id }}"> {{ $approver->firstname.' '.$approver->middlename.' '.$approver->lastname.' ('.$approver->desig_level.')' }} </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="request">REQUEST</button>
                                </div>
                            </div>
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO HR FOR PROCESSING</label>
                                </div>
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="send">SEND</button>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                            <div class="comment_container">
                                <div class="comment_notes">
                                    <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                                </div>
                                <div class="comment_button">
                                    <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="return">RETURN</button>
                                </div>
                            </div>
                        @endif
                    </div>
                @elseif($method=='my_view')
                    <div class="col-md-12 text-center">
                        @if($leaveDetails["status"] == "FOR APPROVAL")
                            <button type="submit" class="btn btn-default btndefault" name="lrfAction" value="cancelRequest">CANCEL REQUEST</button>
                        @endif
                        <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                    </div>
                @else
                    <div class="comment_container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop
@section('js_ko')
	<script>
	    $("#lrfRestDay").chosen({
	    	max_selected_options : 2,
	    	width: '100%'
	    });
	</script>
@stop