@extends('../template/header')
@section('content')

<div class="alert alert-danger container_message" id="container_message"></div> 
<form class="form-inline" action="#" method="post" enctype="multipart/form-data" id="nda_add_form" name="nda_add_form">
    <input type="hidden" value="{{ csrf_token() }}">
    <div class="form_container">
        <div class="container-header">
            <h5 class="text-center">
                <strong>NOTICE OF DISCIPLINARY ACTION</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">EMPLOYEE NAME</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{isset($returned['name']) ? $returned['name'] : ''}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">REFERENCE NUMBER</label>
                    <input type="hidden" name="action" id="action">
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="nda_ref" value="{{isset($returned['nda_reference_number']) ? $returned['nda_reference_number'] : ''}}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">EMPLOYEE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="emp_num" value="{{isset($returned['emp_id']) ? $returned['emp_id'] : ''}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DATE FILED</label>
                    <input type="hidden" id="ref_num" name="ref_num" value="{{isset($returned['nte_id']) ? $returned['nte_id'] : ''}}">
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="nda_date" value="{{isset($returned['nda_submitted_chrd_manager']) ? explode(' ', $returned['nda_submitted_chrd_manager'])[0] : ''}}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DEPARTMENT</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="dept" value="{{isset($returned['dept_name']) ? $returned['dept_name'] : ''}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">STATUS</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="status" value="{{isset($returned['stats']) ? $returned['stats'] : ''}}"/>
                </div>
            </div>
        </div> 
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">SECTION</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="sect" value="{{isset($returned['sect_name']) ? $returned['sect_name'] : ''}}"/>
                </div>
            </div>
        </div>   
        <div class="container-header">
            <h5 class="text-center lined">
                <strong>DISCIPLINARY ACTION</strong>
            </h5>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label class="labels">After a thourough investigation and evaluation of your case, including your letter of explanation we recieved last <font color="red">{{$nte_submitted_chrd_aer2}}</font>, with Notice to Explain <font color="red">NTE-{{$returned['nte_reference_number']}}</font>, the Management concluded among others that you violated the following Rules and Regulations and will be given the respective Disciplinary Action.</label>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <table width="100%">
                        <thead>
                            <tr>
                                <th width="50%" class="red"><label class="labels">INFRACTION</label></th>
                                <th width="50%" class="red"><label class="labels">DISCIPLINARY ACTION</label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="data">Rule {{$returned['cb_rule_number']}} - {{$returned['cb_rule_description']}}</td>
                                <td class="data">{{$returned['disact']}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <label class="labels">Further, we expect you to improve your performance. If there is no improvement on your side or repetition of the same and similar offense in the future are made, we will be constrained to do appropriate actions and shall be dealt with accordingly.</label>
            </div>
        </div>
        <div class="container-header">
            <h5 class="text-center lined">
                <strong>ATTACHMENTS</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="nda_attachments">{{ isset($ndaf_attch_html) ? $ndaf_attch_html : '' }}</div>
        </div>
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="nda_attachments">{{ isset($ndah_attch_html) ? $ndah_attch_html : '' }}</div>
        </div>
        <div class="clear_20"></div>
    </div>
    <div class="clear_20"></div>
    <span class="action-label labels">ACTION</span>
    <div class="form_container">
        <div class="clear_20"></div>
        <div class="textarea_messages_container">
            <div class="row">
                <label class="textarea_inside_label">MESSAGE:</label>
                <textarea rows="3" class="form-control textarea_inside_width" disabled>{{$message != '' ? $message : ''}}</textarea>
            </div>
            @if ($editable || (!($returned['nda_status'] == 15 || $returned['nda_status'] == 16 || $returned['nda_status'] == 11 || $returned['nda_status'] == 13) && Session::get('employee_id') == $returned['nda_filer_id']))
                <div class="clear_10"></div>
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width ib" name="remarks">{{isset($my_comment) ? $my_comment : ''}}</textarea>
                </div>
            @endif
        </div>
        <div class="clear_10"></div>
        <div class="row">
             @if(isset($buttons))
                @foreach($buttons as $button)
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes">{{$button['btn_lbl']}}</label>
                        </div> 
                        <div class="comment_button" style="padding-left: 167px;">
                            <button type="button" class="btn btn-default btndefault" id="{{$button['btn_id']}}" style="width: 110px;">{{$button['btn_txt']}}</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                @endforeach
            @endif
            <div class="comment_container">
                <div class="inthemiddle">
                    @if (($returned['nda_status'] == 10 || $returned['nda_status'] == 12) && $returned['nda_filer_id'] == Session::get('employee_id'))
                        <button type="button" class="btn btn-default btndefault" id="cancel">CANCEL REQUEST</button>
                    @endif
                    <a type="button" href="{{ URL::previous() }}" class="btn btn-default btndefault">BACK</a>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    var	token = '{{Session::token()}}';
    var urlNdaPrint = '{{route('nda.print')}}';
    var action = 0;
    var urlUpload = '{{ route('file-uploader.store') }}';
    var stat = '{{isset($returned['nda_status']) ? $returned['nda_status'] : ''}}';
    var url = '{{route('nte.add')}}';

    var file_counter = {{isset($last_count) ? $last_count : 0}};
    var file_size_counter = {{isset($last_size) ? $last_size : 0}};
    var file_count_limit = 10;
    var file_size_limit = 20971520;

    var home = '{{URL::to('/')}}';
    var urlNdaListCHRD = '{{route('nda.list', 'CHRD')}}';
</script>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/nte/nte_browse_file.js') }}
    {{ HTML::script('/assets/js/nte/nda.js') }}
@endsection