@extends('../template/header')
@section('content')
<div class="alert alert-danger container_message" id="container_message"></div>
<form class="form-inline" action="#" method="post" enctype="multipart/form-data" id="crr_add_form">
    <input type="hidden" value="{{ csrf_token() }}">
    <div class="form_container">
    @if(isset($header))
        <label>EDIT COMPANY RULES AND REGULATION</label>
        <br><br>
        <div class="row">
           	<label class="col-sm-2 labels required">RULE NAME:</label>
		    <div class="col-sm-10">
                <input type="text" class="form-control" name="rule_name" readonly value="{{isset($header->cb_rule_name) ? $header->cb_rule_name : ''}}">
		    </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
           	<label class="col-sm-2 labels required">RULE NO.:</label>
		    <div class="col-sm-10">
		      	<input type="text" class="form-control" name="rule_number" maxlength="5" readonly value="{{isset($header->cb_rule_number) ? $header->cb_rule_number : ''}}">
		    </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
           	<label class="col-sm-2 labels required">RULE DESCRIPTION:</label>
		    <div class="col-sm-10">
		      	<textarea class="form-control" name="rule_description" maxlength="1000" readonly rows="5">{{isset($header->cb_rule_description) ? $header->cb_rule_description : ''}}</textarea>
		    </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
           	<label class="col-sm-2 labels required">STATUS:</label>
		    <div class="col-sm-10">
		      	<select class="form-control" name="rule_status">
		      		<option value="1" {{isset($header->cb_rule_status) ? $header->cb_rule_status == 1 ? 'Selected="Selected"' : '' : ''}}>Active</option>
					<option value="0" {{isset($header->cb_rule_status) ? $header->cb_rule_status == 0 ? 'Selected="Selected"' : '' : ''}}>Inactive</option>
		      	</select>
		    </div>
        </div>
    @else
        <label>ADD COMPANY RULES AND REGULATION</label>
        <br><br>
        <div class="row">
            <label class="col-sm-2 labels required">RULE NAME:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="rule_name">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <label class="col-sm-2 labels required">RULE NO.:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="rule_number" maxlength="5">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <label class="col-sm-2 labels required">RULE DESCRIPTION:</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="rule_description" maxlength="1000" rows="5"></textarea>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <label class="col-sm-2 labels required">STATUS:</label>
            <div class="col-sm-10">
                <select class="form-control" name="rule_status">
                    <option value="1" {{isset($header->cb_rule_status) ? $header->cb_rule_status == 1 ? 'Selected="Selected"' : '' : ''}}>Active</option>
                    <option value="0" {{isset($header->cb_rule_status) ? $header->cb_rule_status == 0 ? 'Selected="Selected"' : '' : ''}}>Inactive</option>
                </select>
            </div>
        </div>
    @endif
        <div class="clear_10"></div>
        <div class="row">
        	<div class="col-sm-2">
           		<button type="button" class="btn btn-default" id="add_section">ADD SECTION</button>
           	</div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
        	<table class="table table-bordered" id="section_table">
        		<thead>
    				<tr>
    					<th width="20%" class="inthemiddle">SECTION NO.</th>
            			<th width="60%" class="inthemiddle">SECTION DESCRIPTION</th>
            			<th width="20%" class="inthemiddle">STATUS</th>
    				</tr>
        		</thead>
        		<tbody id="section_table_body">
                    @if(isset($details))
                        @for($i=0;$i<count($details);$i++)
                            <tr>
                                <td width="20%"> 
                                    <input type="hidden" Class="crr_id" name="crr_id[{{$i}}]" value="{{$details[$i]->cbd_detail_id}}">
                                    <input type="text" Class="form-control section_no" name="section_no[{{$i}}]" value="{{$details[$i]->cbd_section_number}}" readonly>
                                </td>
                                <td width="60%"> 
                                    <textarea class="form-control section_desc" name="section_desc[{{$i}}]" readonly>{{$details[$i]->cbd_section_description}}</textarea>
                                </td>
                                <td width="20%"> 
                                    <select class="form-control section_status" name="section_status[{{$i}}]">
                                        <option value="1" {{$details[$i]->cbd_section_status == 0 ? 'Selected="Selected"' : '' }}>Active</option>
                                        <option value="0" {{$details[$i]->cbd_section_status == 0 ? 'Selected="Selected"' : '' }}>Inactive</option>
                                    </select>
                                </td>
                            </tr>
                        @endfor
                    @else 
            			<tr>
            				<td width="20%">
                                <input type="hidden" Class="crr_id" name="crr_id[0]">
                                <input type="text" class="add form-control" name="section_no[0]" maxlength="5">
                            </td>
            				<td width="60%"> <textarea class="add form-control" name="section_desc[0]" maxlength="1000"></textarea>
                            </td>
            				<td width="20%"> 
                                <select class="form-control" name="section_status[0]">
            					   <option value="1">Active</option>
            					   <option value="0">Inactive</option>
            				    </select> 
                                <button type="button" class="removebutton btn btn-danger btn-sm" title="Remove this row">X</button>
                            </td>
            			</tr>
                    @endif
        		</tbody>
        	</table>
        </div>
        <div class="clear_10"></div>
        <div class="row">
        	<div class="col-sm-4"></div>
        	<div class="col-sm-2"><button type="button" class="btn btn-default btndefault" id="save_crr">SAVE</button></div>
        	<div class="col-sm-2"><a href="{{ URL::previous() }}" type="button" class="btn btn-default btndefault">BACK</a></div>
        	<div class="col-sm-4"></div>
    	</div>
    </div>
</form>
<script>
var	token = '{{Session::token()}}';
var url = '{{route('nte.add')}}';
var urlList = '{{route('crr.list')}}';
var hId = '{{isset($header->cb_crr_id) ? $header->cb_crr_id : ''}}';
var action = 0;
</script>    
@stop
@section('js_ko')
{{ HTML::script('/assets/js/nte/crr_build_up.js') }}
@endsection