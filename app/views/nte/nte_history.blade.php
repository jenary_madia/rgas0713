<div class="modal fade" tabindex="-1" role="dialog" id="history">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">History</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <label for="status" class="col-sm-4 labels">EMPLOYEE NAME</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" disabled id="his_emp_name">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <label for="status" class="col-sm-4 labels">DEPARTMENT</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" disabled id="his_dept_name">
                    </div>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="inthemiddle">
                <label class="form-label">Previous Infractions and Corresponding Disciplinary Actions</label>
            </div>
            <div class="clear_10"></div>
                <div class="row">
                    <label for="status" class="col-sm-2 col-sm-offset-4 labels">As Of</label>
                    <div class="col-sm-2">
                        <select class="form-control" id="year_h">
                            <option value="">--Select--</option>
                            @foreach($dates[8] as $year)
                                <option value="{{$year}}" {{$year == $dates[0] ? 'selected' : ''}}>{{$year}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            <div class="clear_10"></div>
            <div class="row">
                <table class='table-bordered' style="width:90%;">
                    <thead>
                        <tr>
                            <th width="20%" class="red"><label class="labels">MONTH</label></th>
                            <th width="20%" class="red"><label class="labels">INFRACTION</label></th>
                            <th width="30%" class="red"><label class="labels">RECOMMENDED DISCIPLINARY ACTION</label></th>
                            <th width="30%" class="red"><label class="labels">APPROVED DISCIPLINARY ACTION</label></th>
                        </tr>
                    </thead>
                    <tbody id="history_body" class="hover hand">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer inthemiddle">
                <button type="button" class="btn btn-default" data-dismiss="modal">BACK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="nte">
    <div class="modal-dialog" style="width: 85%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Accomplished Notice to Explain</h4>
            </div>
            <div class="modal-body crr">
                <div class="form_container" style="width: 100%;">
                    <div class="container-header">
                        <h5 class="text-center lined">
                            <strong>NOTICE TO EXPLAIN</strong>
                        </h5>
                    </div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">EMPLOYEE NAME</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_name"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">REFERENCE NUMBER</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled  id="history_reference_number"/>
                            </div>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                    <input type="hidden" name="ref_num" value="">
                                <label class="labels">EMPLOYEE NUMBER</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_number"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">DATE FILED</label>
                                <input type="hidden" name="action" id="action">
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_date_filed"/>
                            </div>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">DEPARTMENT</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_department"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">STATUS</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_status"/>
                            </div>
                        </div>
                    </div> 
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="his form-control" disabled id="history_section"/>
                            </div>
                        </div>
                    </div>   
                    <div class="container-header">
                        <h5 class="text-center lined">
                            <strong>INFRACTION</strong>
                        </h5>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels">DATE COMMITED</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control" disabled id="history_date_committed"/>
                                    </div>
                                </div>
                            </div>   
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels">TOTAL INCCURED FOR THE MONTH</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control" disabled  id="history_tim"/>
                                    </div>
                                </div>
                            </div>        
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels">TOTAL TIMES VIOLATED FOR THE YEAR</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control" disabled id="history_tvy"/>
                                    </div>
                                </div>
                            </div>        
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels">RECOMMENDED DISCIPLINARY ACTION</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control" disabled id="history_rda"/>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="labels">COMPANY RULES AND REGULATIONS VIOLATED</label>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea rows="7" class="his form-control" disabled id="history_crr"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="labels">EXPLANATION FOR INFRACTION</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea rows="3" class="his form-control" disabled id="history_explanation"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels">DEPARTMENT HEAD&apos;S DECISION</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <select class="form-control" disabled id="history_decision">
                                            <option value="0">--Select--</option> 
                                            <option value="1">Excused</option>
                                            <option value="2">Unexcused</option>
                                        </select>
                                    </div>
                                </div>
                            </div>   
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="act labels hidden">DISCIPLINARY ACTION</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <select class="act form-control hidden" disabled id="history_da"></select>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels sus hidden">DAYS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FROM</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control sus hidden" disabled id="history_from"/>
                                    </div>
                                </div>
                            </div>   
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels sus hidden">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TO</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text" class="his form-control sus hidden" disabled id="history_to"/>
                                    </div>
                                </div>
                            </div>   
                            <div class="clear_10"></div>
                            <div class="row">
                                <div class="row_form_container">
                                    <div class="col1_form_container">
                                        <label class="labels sus hidden">NO OF DAYS</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <input type="text"class="his form-control sus hidden" disabled id="history_nod"/>
                                    </div>
                                </div>
                            </div>   
                        </div>
                        <div class="clear_10"></div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="labels">REMARKS</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <textarea rows="3" class="his form-control" disabled id="history_remarks"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-header">
                        <h5 class="text-center lined">
                            <strong>ATTACHMENTS</strong>
                        </h5>
                    </div>
                    <div class="clear_10"></div>
                    <div class="attachment_container">
                        <div class="atch" id="his_files_attachments"></div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="attachment_container">
                        <div class="atch" id="his_emp_attachments"></div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="attachment_container">
                        <div class="atch" id="his_dept_attachments"></div>
                    </div>
                    <div class="clear_20"></div>
                </div>
                <div class="clear_20"></div>
                <span class="action-label labels">ACTION</span>
                <div class="form_container" style="width: 100%;">
                    <div class="clear_20"></div>
                    <div class="textarea_messages_container">
                        <div class="row">
                            <label class="textarea_inside_label">MESSAGE:</label>
                            <textarea rows="3" class="his form-control textarea_inside_width" disabled id="history_message"></textarea>
                        </div>
                        <div class="clear_10"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer inthemiddle">
                <button type="button" class="btn btn-default" data-dismiss="modal">BACK</button>
            </div>
        </div>
    </div>
</div>