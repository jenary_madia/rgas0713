@extends('../template/header')
@section('content')

<div class="wrapper">
@if($userrights[0])
    <div class="clear_20"></div>
    <form method="POST" action="{{$form_action_request}}" id="{{$form_id_request}}">
        <input type="hidden" id="ref_num_request" name="ref_num">
        <input type="hidden" id="bool_request" name="bool">
    </form>

    <form method="POST" action="{{$form_action_returned}}" id="{{$form_id_returned}}">
        <input type="hidden" id="ref_num_returned" name="ref_num">
        <input type="hidden" id="bool_returned" name="bool">
    </form>
    <div class="row4_form_container">
        <div class="col4_form_container">
            <label class="labels">DEPARTMENT:</label> 
        </div>
        <div class="col2_form_container">
            <select class="form-control s-nte-department">
                <option value="">ALL</option>
                @foreach($departments as $department)
                    <option value="{{$department->id}}">{{$department->dept_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="row4_form_container">
        <div class="col4_form_container">
            <label class="labels">YEAR:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-control s-nte-year">
                <option value="">ALL</option>
                @for($i = ($date[0]); $i > 1980; $i--)
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="row4_form_container">
        <div class="col4_form_container">
            <label class="labels">MONTH:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-control s-nte-month">
                <option value="">ALL</option>
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
            </select>
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="datatable_holder">
        <span class="list-title" role="columnheader" rowspan="1" colspan="5">{{ $module == 'Submitted' ? 'ALL' : 'MY' }} NOTICE TO EXPLAIN</span>
        <table id='nte_list_request' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
            <thead>
                <tr role="row">
                    <th align='center'>REFERENCE NUMBER</th>
                    <th align='center'>EMPLOYEE NAME</th>
                    <th align='center'>DEPARTMENT</th>
                    <th align='center'>DATE COMMITTED</th>
                    <th align='center'>INFRACTION</th>
                    <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                    <th align='center'>CURRENT</th>
                    <th align='center'>STATUS</th>
                    <th align='center'>ACTION</th>
                </tr>   
            </thead>
        </table>
    </div>
    <br><br><hr>
@endif

@if($userrights[1])
    <div class="clear_20"></div>
    <form method="POST" action="{{$form_action_approval}}" id="{{$form_id_approval}}">
        <input type="hidden" id="ref_num_approval" name="ref_num">
        <input type="hidden" id="ref_num_approval2" name="ref_num2">
        <input type="hidden" id="ref_num_approval3" name="ref_num3">
        <input type="hidden" id="bool_approval" name="bool">
    </form>
    <div class="datatable_holder">
        <span class="list-title" role="columnheader" rowspan="1" colspan="5">NOTICE TO EXPLAIN FOR APPROVAL</span>
        <table id='nte_list_approval' cellpadding='0' cellspacing='0' border='0' class='display' width='100%' >
            <thead>
                <tr role="row">
                    <th align='center'>DEPARTMENT</th>
                    <th align='center'>NUMBER OF NTE</th>
                    <th align='center'>MONTH</th>
                    <th align='center'>YEAR</th>
                    <th align='center'>ACTION</th>
                </tr>   
            </thead>
        </table>
    </div>
    <br><br><hr>
@endif

@if (isset($userrights[2]))
    @if($userrights[2])
        <div class="clear_20"></div>
        <form method="POST" action="{{$form_action_department}}" id="{{$form_id_department}}">
            <input type="hidden" id="ref_num_department" name="ref_num">
            <input type="hidden" id="bool_department" name="bool">
        </form>
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5">NOTICE TO EXPLAIN OF EMPLOYEE&apos;S CONCERNED DEPARTMENT HEAD</span>
            <table id='nte_list_department' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                <thead>
                    <tr role="row">
                        <th align='center'>REFERENCE NUMBER</th>
                        <th align='center'>EMPLOYEE NAME</th>
                        <th align='center'>INFRACTION</th>
                        <th align='center'>DATE COMMITTED</th>
                        <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                        <th align='center'>STATUS</th>
                        <th align='center'>ACTION</th>
                    </tr>   
                </thead>
            </table>
        </div>
        <br><br><hr>
    @endif
@endif
@if (isset($userrights[2]))
    @if($userrights[3])
        <div class="clear_20"></div>
        <form method="POST" action="{{$form_action_employee}}" id="{{$form_id_employee}}">
            <input type="hidden" id="ref_num_employee" name="ref_num">
            <input type="hidden" id="bool_employee" name="bool">
        </form>
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5">NOTICE TO EXPLAIN OF CONCERNED EMPLOYEE</span>
            <table id='nte_list_employee' cellpadding='0' cellspacing='0' border='0' class='display' width='100%' >
                <thead>
                    <tr role="row">
                        <th align='center'>REFERENCE NUMBER</th>
                        <th align='center'>INFRACTION</th>
                        <th align='center'>DATE COMMITTED</th>
                        <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                        <th align='center'>STATUS</th>
                        <th align='center'>ACTION</th>
                    </tr>   
                </thead>
            </table>
        </div>
    @endif
@endif

    <br><br><br><br><br><br><br><br>
    <form method="POST" action="{{$form_action_view}}" id="{{$form_id_view}}">
        <input type="hidden" id="ref_num_view" name="ref_num">
        <input type="hidden" id="bool_view" name="bool">
    </form>
    <form method="POST" action="{{$form_action_view}}" id="{{$form_id_view}}">
        <input type="hidden" id="ref_num_view" name="ref_num">
        <input type="hidden" id="bool_view" name="bool">
    </form>
</div>
<script>
    var	token = '{{Session::token()}}';
    var urlDes = '{{route('nte.des')}}';
    var module = '{{$module}}';
</script>
@stop
    
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/nte/nte_lists.js') }}
@endsection

