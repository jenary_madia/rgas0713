@extends('../template/header')
@section('content')
<div class="alert alert-danger container_message" id="container_message"></div> 
<form class="form-inline" action="#" method="post" enctype="multipart/form-data" id="nte_returned_form" name="nte_returned_form">
    <input type="hidden" value="{{ csrf_token() }}">
    <div class="form_container">
        <div class="container-header">
            <h5 class="text-center">
                <strong>NOTICE TO EXPLAIN</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">EMPLOYEE NAME</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{$returned['name']}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">REFERENCE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" {{ $returned['nte_status'] == 8 ? 'name="reference"' : '' }} readonly value="{{$returned['nte_reference_number']}}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <input type="hidden" id="emp_name_page" value="{{$returned['nte_employee_id']}}">
                <input type="hidden" id="dept_name" value="{{$returned['nte_department_id']}}">
                <input type="hidden" id="crr" value="{{$returned['cb_crr_id']}}">
                <div class="col1_form_container">
                    @if ($editable || $returned['nte_status'] == 2 || $returned['nte_status'] == 8)
                        <input type="hidden" name="ref_num" value="{{$returned['nte_id']}}">
                    @endif
                    <label class="labels required">EMPLOYEE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{$returned['emp_id']}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DATE FILED</label>
                    <input type="hidden" name="action" id="action">
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{ explode(' ', $returned['nte_submitted_chrd_aer'])[0] }}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DEPARTMENT</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{$returned['dept_name']}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">STATUS</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{$returned['statuses']}}"/>
                </div>
            </div>
        </div> 
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">SECTION</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled value="{{$returned['sect_name']}}"/>
                </div>
            </div>
        </div>   
        <div class="container-header">
            <h5 class="text-center lined">
                <strong>INFRACTION</strong>
            </h5>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <a href="#" data-toggle="modal" data-target="#history" class="btn btn-default btndefault" style="width: 110px;" id="show_history_page">SHOW HISTORY</a>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DATE COMMITED</label>
                        </div>
                        <div class="col2_form_container">
                            @if ($edit_returned)
                                <select class="form-control" style="width: 40%;" id="month" name="month">
                                </select>
                                <select class="form-control" style="width: 20%;" id="day" name="day">
                                </select>
                                <select class="form-control" style="width: 35%;" id="year" name="year">
                                </select>
                            @else
                                @if ($returned['nte_day'] == '')
                                    <input type="text" class="form-control" disabled value="{{$month_name . ' ' . $returned['nte_year']}}"/>
                                @else
                                    <input type="text" class="form-control" disabled value="{{$month_name . ' ' . $returned['nte_day'] . ', ' . $returned['nte_year']}}"/>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>   
                <div class="clear_10"></div>
                @if ($returned['nte_status'] == 3)
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">INFRACTION <a href="#" data-toggle="modal" data-target="#help"><i class="fa fa-question-circle fa-lg icon-cog" aria-hidden="true"></i></a></label>
                            </div>
                            <div class="col2_form_container">
                                <select class="form-control" style="width: 100%;" {{$edit_returned ? 'name="infraction"' : 'disabled'}}>
                                @foreach($crrbuildups as $crrbuildup)
                                    <option value="{{$crrbuildup->cb_crr_id}}" {{$returned['cb_crr_id'] == $crrbuildup->cb_crr_id ? "selected" : ''}}>{{$crrbuildup->cb_rule_name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>      
                    <div class="clear_10"></div>
                @endif
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">TOTAL INCCURED FOR THE MONTH</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" {{$edit_returned ? 'name="tim"' : 'disabled'}} value="{{$returned['nte_total_incurred']}}"/>
                        </div>
                    </div>
                </div>        
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">TOTAL TIMES VIOLATED FOR THE YEAR</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled value="{{count($tvy) > 0 ? $tvy[0]['count'] : ''}}"/>
                        </div>
                    </div>
                </div> 
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">RECOMMENDED DISCIPLINARY ACTION</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="form-control" style="width: 100%;" {{$edit_returned ? 'name="rda"' : 'disabled'}}>
                            @foreach($rdas as $rda)
                                <option value="{{$rda->id}}" {{$returned['nte_recommended_disciplinary_action'] == $rda->id ? "selected" : ''}}>{{$rda->name}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>   
            </div>
            @if ($returned['nte_status'] > 3)
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <label class="labels">COMPANY RULES AND REGULATIONS VIOLATED</label>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <textarea rows="7" class="form-control" disabled> {{$crr}} </textarea>
                    </div>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <label class="labels required">EXPLANATION FOR INFRACTION</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <textarea rows="7" class="form-control ib" {{!$editable || $returned['nte_status'] > 5 ? 'disabled' : ''}} {{$editable ? 'name="explanation"' : ''}}>{{$returned['nte_explanation']}}</textarea>
                    </div>
                </div>
            </div>
            @endif
            @if ($returned['nte_status'] > 5)
            <div class="clear_10"></div>
            <div class="col-md-6">
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required" {{$returned['nte_head_decision'] > 0 || $returned['nte_status'] == 6 ? 'style="display: block;"' : 'style="display: none;"'}}>DEPARTMENT HEAD&apos;S DECISION</label>
                        </div>
                        <div class="col2_form_container">
                            <select type="text" {{$returned['nte_head_decision'] > 0 || $returned['nte_status'] == 6  ? 'style="display: block;"' : 'style="display: none;"'}} class="form-control ib" {{!$editable || $returned['nte_status'] > 6 ? 'disabled' : ''}} name="head_dec" id="head_dec">
                                <option value="">--Select--</option>
                                <option value="1" {{$returned['nte_head_decision'] == 1 ? 'Selected' : ''}}>Excused</option>
                                <option value="2" {{$returned['nte_head_decision'] == 2 ? 'Selected' : ''}}>Unexcused</option>
                            </select>
                        </div>
                    </div>
                </div>   
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="act labels required" {{$returned['nte_head_decision'] == 2 ? 'style="display: block;"' : 'style="display: none;"'}}>DISCIPLINARY ACTION</label>
                        </div>
                        <div class="col2_form_container">
                            <select type="text" {{$returned['nte_head_decision'] == 2 ? 'style="display: block;"' : 'style="display: none;"'}} class="act form-control ib" {{!$editable || $returned['nte_status'] > 6 ? 'disabled' : ''}} name="head_act" id="head_act">
                                <option value="">--Select--</option>
                                @for($i=1;$i<count($rdas);$i++)
                                    <option value="{{$rdas[$i]['id']}}" {{$rdas[$i]['id'] == $returned['nte_disciplinary_action'] ? 'Selected' : ''}}>{{$rdas[$i]['name']}}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="col-md-6 sus" {{$returned['nte_disciplinary_action'] == 4 ? 'style="display: block;"' : 'style="display: none;"'}}>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DAYS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; FROM</label>
                        </div>
                        <div class="col2_form_container">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="date_picker form-control input-small ib" {{!$editable || $returned['nte_status'] > 6  ? 'disabled' : ''}} id="sus_from" name="sus_from" value="{{ $returned['nte_start_suspension'] != 0 ? date_format(date_create($returned['nte_start_suspension']), "Y/m/d") : '' }}" />
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar sus_from"></i></span>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TO</label>
                        </div>
                        <div class="col2_form_container">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="date_picker form-control input-small ib" {{!$editable || $returned['nte_status'] > 6  ? 'disabled' : ''}} id="sus_to" name="sus_to" value="{{ $returned['nte_end_suspension'] != 0 ? date_format(date_create($returned['nte_end_suspension']), "Y/m/d") : '' }}" />
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar sus_to"></i></span>
                            </div>
                        </div>
                    </div>
                </div>   
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">NO OF DAYS</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text"class="form-control ib" {{!$editable || $returned['nte_status'] > 6  ? 'disabled' : ''}} id="sus_no_days" name="sus_no_days" value="{{$returned['nte_no_of_days'] != 0 ? $returned['nte_no_of_days'] : ''}}" />
                        </div>
                    </div>
                </div>   
            </div>
            <div class="clear_10"></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <label class="labels">REMARKS</label>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <textarea rows="7" class="form-control ib" {{!$editable || $returned['nte_status'] > 6  ? 'disabled' : ''}} name="head_remarks">{{$returned['nte_head_remarks']}}</textarea>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="container-header">
            <h5 class="text-center lined">
                <strong>ATTACHMENTS</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        @if($filer_upload)
            <label class="labels">ADD ATTACHMENTS (MAXIMUM OF 10 ATTACHMENTS)</label>
            <div class="clear_10"></div>
            <span class="btn btn-success btnbrowse fileinput-button">
                <span >BROWSE</span>
                <input id="files" type="file" class="browse" name="attachments[]" multiple>
            </span>
        @endif
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="files_attachments">{{ $files_attch_html }}</div>
        </div>
        <div class="clear_10"></div>
        @if($emp_upload)
            <label class="labels">ADD ATTACHMENTS (MAXIMUM OF 5 ATTACHMENTS)</label>
            <div class="clear_10"></div>
            <span class="btn btn-success btnbrowse fileinput-button">
                <span >BROWSE</span>
                <input id="emp" type="file" class="browse" name="attachments[]" multiple>
            </span>
        @endif
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="emp_attachments">{{ $emp_attch_html }}</div>
        </div>
        <div class="clear_10"></div>
        @if($dept_upload)
            <label class="labels">ADD ATTACHMENTS (MAXIMUM OF 5 ATTACHMENTS)</label>
            <div class="clear_10"></div>
            <span class="btn btn-success btnbrowse fileinput-button">
                <span >BROWSE</span>
                <input id="dept" type="file" class="browse" name="attachments[]" multiple>
            </span>
        @endif
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="dept_attachments">{{ $dept_attch_html }}</div>
        </div>
        <div class="clear_20"></div>
    </div>
    <div class="clear_20"></div>
    <span class="action-label labels">ACTION</span>
    <div class="form_container">
        <div class="clear_20"></div>
        <div class="textarea_messages_container">
            <div class="row">
                <label class="textarea_inside_label">MESSAGE:</label>
                <textarea rows="3" class="form-control textarea_inside_width" disabled>{{$message}}</textarea>
            </div>
            <div class="clear_10"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" class="form-control textarea_inside_width ib" {{$editable || ($returned['nte_status'] == 2 && $returned['nte_filer_id'] == Session::get('employee_id')) ? '' : 'disabled'}} {{$editable || ($returned['nte_status'] == 2 && $returned['nte_filer_id'] == Session::get('employee_id'))  ? 'name="remarks"' : ''}}>{{isset($my_comment) ? $my_comment : ''}}</textarea>
            </div>
        </div>
        
        <div class="clear_10"></div>
        <div class="row">
            @if(isset($buttons))
                @foreach($buttons as $button)
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes">{{$button['btn_lbl']}}</label>
                        </div> 
                        <div class="comment_button" style="padding-left: 167px;">
                            <button type="button" class="btn btn-default btndefault" id="{{$button['btn_id']}}" style="width: 110px;">{{$button['btn_txt']}}</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                @endforeach
            @endif
            <div class="comment_container">
                <div class="inthemiddle">
                    <a type="button" href="{{ URL::previous() }}" class="btn btn-default btndefault" id="send">BACK</a>
                </div>
            </div>
        </div>
    </div>
</form>
@include('/nte/crr')
@include('/nte/nte_history')
<script>
    var	token = '{{Session::token()}}';
    var url = '{{route('nte.add')}}';
    var urlNtePrint = '{{route('nte.print')}}';
    var urlNdaCreate = '{{route('nte.nda')}}';
    var nte_emp = '';
    var home = '{{URL::to('/')}}';
    var urlGh = '{{route('nte.get_history_header')}}';
    var urlGd = '{{route('nte.get_history_details')}}';
    var urlHis = '{{route('nte.history')}}';
    var action = 0;
    var stat = '{{isset($returned['nte_status']) ? $returned['nte_status'] : ''}}';
    var urlUpload = '{{ route('file-uploader.store') }}';

    var files_ctr = {{$files_last_count}};
    var files_size_ctr = {{$files_last_size}};

    var emp_ctr = {{$emp_last_count}};
    var emp_size_ctr = {{$emp_last_size}};
    
    var dept_ctr = {{$dept_last_count}};
    var dept_size_ctr = {{$dept_last_size}};

    var urlDownload = '{{ route('nte.download') }}';
    var urlNteListCHRD = '{{route('nte.list', 'CHRD')}}';

    var cM = {{$dates[1]}};
    var cY = {{$dates[0]}};
    var tD = {{$dates[2]}};

    var month = {{$returned['nte_month']}};
    var year = {{$returned['nte_year']}};
    var day = {{$returned['nte_day'] == '' ? 00 : $returned['nte_day']}};
</script>
    
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/nte/nte_browse_file.js') }}
    {{ HTML::script('/assets/js/nte/nte.js') }}
    {{ HTML::script('/assets/js/nte/nte_history.js') }}
@endsection