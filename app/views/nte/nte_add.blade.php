@extends('../template/header')
@section('content')
<div class="alert container_message" id="container_message"></div>
<form class="form-inline" action="{{route('nte.add')}}" method="post" enctype="multipart/form-data" id="nte_add_form">
    <input type="hidden" value="{{ csrf_token() }}">
    <div class="form_container">
        <div class="container-header">
            <h5 class="text-center">
                <strong>NOTICE TO EXPLAIN</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        @if ($isEdit && $selectedFromApproval == "" && $selectedFromMultipleEntries == "") 
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{route('nte.create_multi')}}" class="btn btn-default">ENCODE MULTIPLE ENTRIES</a>
                </div>
            </div>
        @endif
        <div class="clear_20"></div>
        <div class="row">
            <label for="status" class="col-sm-2 labels required">DEPARTMENT</label>
            <div class="col-sm-10">
                <select class="form-control chosen-select" {{!$isEdit || $selectedFromApproval != "" ? 'disabled' : ''}} {{$isEdit && $selectedFromApproval == "" ? 'id="department" multiple ' : ''}}>
                    @for($g=0; $g<count($departments); $g++)
                        <?php $yes = true?>
                        @if ($selectedFromMultipleEntries != null)
                            @for($f=0; $f<count($selectedFromMultipleEntries['department']); $f++)
                                @if ($selectedFromMultipleEntries['department'][$f] == $departments[$g]['id']) 
                                    <option value="{{$departments[$g]['id']}}" selected>{{$departments[$g]['dept_name']}}</option>
                                    <?php $yes = false ?>
                                @endif
                            @endfor
                            @if ($yes)
                                <option value="{{$departments[$g]['id']}}">{{$departments[$g]['dept_name']}}</option>        
                            @endif
                        @elseif ($selectedFromApproval != null)
                            <option value="{{$departments[$g]['id']}}" {{$selectedFromApproval['NTEs'] != Null ? $selectedFromApproval['NTEs'][0]['nte_department_id'] != NULL && $selectedFromApproval['NTEs'][0]['nte_department_id'] == $departments[$g]['id'] ? 'Selected' : '' : ''}}>{{$departments[$g]['dept_name']}}</option>
                        @else
                            <option value="{{$departments[$g]['id']}}">{{$departments[$g]['dept_name']}}</option>   
                        @endif
                    @endfor
                </select>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <label for="status" class="col-sm-2 labels required">YEAR</label>
            <div class="col-sm-10">
                <select class="form-control" {{!$isEdit || $selectedFromApproval != "" ? 'disabled' : ''}} {{$isEdit && $selectedFromApproval == '' ? 'id="year_dd"  name="year"' : ''}}>
                    @foreach($dates[8] as $year)
                        @if ($selectedFromMultipleEntries != null) 
                            <option value="{{$year}}" {{$selectedFromMultipleEntries['year_dd'] == $year ? 'Selected' : ''}}>{{$year}}</option>
                            <?php $year_dd = $selectedFromMultipleEntries['year_dd']; ?>
                        @elseif ($selectedFromApproval != null)
                            <option value="{{$year}}" {{$selectedFromApproval['NTEs'] != Null ? $selectedFromApproval['NTEs'][0]['nte_year']  != NULL && $selectedFromApproval['NTEs'][0]['nte_year'] == $year ? 'Selected' : '' : $year == $dates[0] ? 'selected' : ''}}>{{$year}}</option>
                            <?php $year_dd =  $selectedFromApproval['NTEs'][0]['nte_year']; ?>
                        @else
                            <option value="{{$year}}" {{$year == $dates[0] ? 'selected' : ''}}>{{$year}}</option>
                            <?php $year_dd =  $dates[0]; ?>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <label for="status" class="col-sm-2 labels required">MONTH</label>
            <div class="col-sm-10">
                <select class="form-control" {{!$isEdit || $selectedFromApproval != "" ? 'disabled' : ''}} {{$isEdit && $selectedFromApproval == '' ? 'id="month_dd" name="month"' : ''}}>
                    @if ($selectedFromMultipleEntries != null)
                        <?php $month_dd =  $selectedFromMultipleEntries['month_dd']; ?>
                    @elseif ($selectedFromApproval != null)
                        <option value="01" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "01" ? "selected" : ""}}>January</option>
                        <option value="02" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "02" ? "selected" : ""}}>February</option>
                        <option value="03" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "03" ? "selected" : ""}}>March</option>
                        <option value="04" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "04" ? "selected" : ""}}>April</option>
                        <option value="05" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "05" ? "selected" : ""}}>May</option>
                        <option value="06" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "06" ? "selected" : ""}}>June</option>
                        <option value="07" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "07" ? "selected" : ""}}>July</option>
                        <option value="08" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "08" ? "selected" : ""}}>August</option>
                        <option value="09" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "09" ? "selected" : ""}}>September</option>
                        <option value="10" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "10" ? "selected" : ""}}>October</option>
                        <option value="11" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "11" ? "selected" : ""}}>November</option>
                        <option value="12" {{$selectedFromApproval['NTEs'][0]['nte_month'] == "12" ? "selected" : ""}}>December</option>
                        <?php $month_dd =  $selectedFromApproval['NTEs'][0]['nte_month']; ?>
                    @else
                        <?php $month_dd =  $dates[1]; ?>
                    @endif
                </select>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="nte-wrapper wrapper">
                <table class="table table-bordered" id="nte_table" width="100%">
                    <thead>
                        <tr>
                            <th width="10%">
                                <input type="checkbox" {{ $module || $isEdit ? '' : 'disabled' }} id="chk_all">
                                <label class="labels">EMPLOYEE NAME</label>
                            </th>
                            <th width="10%">
                                <label class="labels">DATE COMMITTED</label>
                            </th>
                            <th width="10%">
                                <label class="labels">INFRACTION 
                                    <a href="#" data-toggle="modal" data-target="#help">
                                        <i class="fa fa-question-circle fa-lg icon-cog" aria-hidden="true">
                                        </i>
                                    </a>
                                </label>
                            </th>
                            <th width="10%">
                                <label class="labels">TOTAL INCURRED FOR THE MONTH</label>
                            </th>
                            <th width="10%">
                                <label class="labels">TIMES VIOLATED FOR THE YEAR<label>
                            </th>
                            <th width="10%">
                                <label class="labels">HISTORY OF INFRACTIONS</label>
                            </th>
                            <th width="10%">
                                <label class="labels">RECOMMENDED DISCIPLINARY ACTION</label>
                            </th>
                            <th width="10%">
                                <label class="labels">ATTACHMENTS</label>
                            </th>
                            <th width="10%">
                                <label class="labels">REFERENCE NUMBER</label>
                            </th>
                            <th width="10%">
                                <label class="labels">STATUS</label>
                            </th>
                            <th width="10%">
                                <label class="labels">REMARKS</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="nte_table_body">
                        @if ($selectedFromMultipleEntries != NULL)
                            <?php $m = 0; ?>

                            @for ($i = 0; $i<count($selectedFromMultipleEntries['employee']); $i++)
                                @for ($j=0; $j<count($selectedFromMultipleEntries['infraction']); $j++)
                                    <tr>
                                        <td>
                                            <div class="ui-widget">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="hidden" class="sel_box" name="sel_emp[]" value="0">
                                                        <input type="checkbox" class="sel_chk">
                                                    </span>
                                                    <select class="emp" name="emp_name[]">
                                                        <option value=""></option>
                                                        @foreach($employees as $employee)
                                                            <option value="{{$employee->id}}|{{$employee->departmentid}}" {{$employee->id == $selectedFromMultipleEntries['employee'][$emp_keys[$i]] ? 'Selected' : ''}}>{{$employee->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td> 
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <label class="labels">{{$dates[10].' '.$selectedFromMultipleEntries['year_dd']}}</label>
                                                </span>
                                                <select class="comm form-control" name="date_comm[]">
                                                    <option value=""></option>
                                                        @for($z=1;$z<=$dates[7];$z++) 
                                                            <option value="{{strlen($z) < 2 ? '0'.$z : $z}}" {{$z == $dates[2] ? 'selectedFromMultipleEntries' : ''}}>{{strlen($z) < 2 ? '0'.$z : $z}}</option>
                                                        @endfor
                                                </select>
                                            </div>
                                        </td>
                                        <td> 
                                            <select name="infraction[]" class="infra form-control">
                                                <option value="">--Select--</option>
                                                @foreach($crrbuildups as $crrbuildup)
                                                    <option value="{{$crrbuildup->cb_crr_id}}" {{$crrbuildup->cb_crr_id == $selectedFromMultipleEntries['infraction'][$j] ? 'Selected' : ''}}>{{$crrbuildup->cb_rule_name}}</option>
                                                @endforeach
                                            </select> 
                                        </td>
                                        <td> 
                                            <input type="text" name="tim[]" class="incc form-control" width="70%">
                                        </td>
                                        <td> 
                                            <p class="form-control tvy"></p> 
                                        </td>
                                        <td> 
                                            <a href="#" data-toggle="modal" data-target="#history" class="btn btn-default btndefault history" style="width: 110px;">SHOW HISTORY</a> 
                                        </td>
                                        <td> 
                                            <select name="rda[]" class="recc form-control"> 
                                                <option value="">--Select--</option>
                                                <option value="1">Excused</option>
                                                <option value="2">Verbal Reprimand</option>
                                                <option value="3">Written Reprimand</option>
                                                <option value="4">Suspension</option>
                                                <option value="5">Dismissal</option>
                                            </select> 
                                        </td>
                                        <td> 
                                            <div class="attachment_container nte-row-container"><div class="atch" id="attachments"><input type="hidden" value="{{ isset($selectedFromMultipleEntries['files']) ? $selectedFromMultipleEntries['files'][$m]['last_size'] : ''}}"><input type="hidden" value="{{ isset($selectedFromMultipleEntries['files']) ? $selectedFromMultipleEntries['files'][$m]['last_count'] : ''}}">{{ isset($selectedFromMultipleEntries['files']) ? $selectedFromMultipleEntries['files'][$m]['attachment'] : ''}}</div>
                                                <span class="btn btn-success btnbrowse fileinput-button nte-row-upload">
                                                    <span >+</span>
                                                    <input class="flup" type="file" name="attachments[]" multiple>
                                                </span>
                                            </div>
                                        </td>
                                        <td> 
                                            <input type="text" readonly="" class="form-control ref inthemiddle" name="ref_num[]" width="80%"> 
                                        </td>
                                        <td> 
                                            <label class="labels"></label>
                                        </td>
                                        <td>
                                            <textarea name="remarks[]" class="rem form-control" style="width: 150px"></textarea>
                                        </td>
                                    </tr>
                                    <?php $m++; ?>
                                @endfor
                            @endfor
                        @elseif($selectedFromApproval != NULL)
                            @for ($j=0; $j<count($selectedFromApproval['NTEs']); $j++)
                                <tr>
                                    <td>
                                        <div class="ui-widget">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <input type="hidden" class="sel_box" name="sel_emp[]" value="0"><input type="checkbox" class="sel_chk" {{$module ? '' : 'disabled' }}>
                                                </span>
                                                <select class="form-control" disabled style="width: 200px; font-size: small;">
                                                    <option value=""></option>
                                                    @foreach($employees as $employee)
                                                        @if ($employee->id == $selectedFromApproval['NTEs'][$j]['nte_employee_id'])
                                                            <option value="{{$employee->id}}|{{$employee->departmentid}}" Selected>{{$employee->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                    <td> 
                                        <div class="input-group">
                                            <input disabled class="form-control" style="width: 150px;" value="{{ $selectedFromApproval['NTEs'][$j]['nte_day'] != '' ? $selectedFromApproval['NTEs'][$j]['month_name'].' '.$selectedFromApproval['NTEs'][$j]['nte_day'].', '.$selectedFromApproval['NTEs'][$j]['nte_year'] : $selectedFromApproval['NTEs'][$j]['month_name'].' '.$selectedFromApproval['NTEs'][$j]['nte_year']}}">
                                        </div>
                                    </td>
                                    <td> 
                                        <select disabled class="form-control">
                                            <option value="">--Select--</option>
                                            @foreach($crrbuildups as $crrbuildup)
                                                <option value="{{$crrbuildup->cb_crr_id}}" {{$crrbuildup->cb_crr_id == $selectedFromApproval['NTEs'][$j]['cb_crr_id'] ? 'Selected' : ''}}>{{$crrbuildup->cb_rule_name}}</option>
                                            @endforeach
                                        </select> 
                                    </td>
                                    <td> 
                                        <input type="text" disabled class="form-control" value="{{$selectedFromApproval['NTEs'][$j]['nte_total_incurred']}}">
                                    </td>
                                    <td> 
                                        <p disabled class="form-control">{{count($selectedFromApproval['tvy'][$j]) > 0 ? $selectedFromApproval['tvy'][$j][0]['count'] : ''}}</p>
                                    </td>
                                    <td>
                                        <a href="#" data-toggle="modal" data-target="#history" class="btn btn-default btndefault history" style="width: 110px;">SHOW HISTORY</a>
                                    </td>
                                    <td>
                                        <select disabled class="recc form-control"> 
                                            <option value="">--Select--</option>
                                            <option value="1" {{$selectedFromApproval['NTEs'][$j]['nte_recommended_disciplinary_action'] == 1 ? 'Selected' : ''}}>Excused</option>
                                            <option value="2" {{$selectedFromApproval['NTEs'][$j]['nte_recommended_disciplinary_action'] == 2 ? 'Selected' : ''}}>Verbal Reprimand</option>
                                            <option value="3" {{$selectedFromApproval['NTEs'][$j]['nte_recommended_disciplinary_action'] == 3 ? 'Selected' : ''}}>Written Reprimand</option>
                                            <option value="4" {{$selectedFromApproval['NTEs'][$j]['nte_recommended_disciplinary_action'] == 4 ? 'Selected' : ''}}>Suspension</option>
                                            <option value="5" {{$selectedFromApproval['NTEs'][$j]['nte_recommended_disciplinary_action'] == 5 ? 'Selected' : ''}}>Dismissal</option>
                                        </select> 
                                    </td>
                                    <td> 
                                        @foreach ($selectedFromApproval['attachments'] as $attachment)
                                            @if($attachment['nte_id'] == $selectedFromApproval['NTEs'][$j]['nte_id'])
                                            <div class="attachment_container nte-row-container"><div class="atch" id="attachments"><input type="hidden" value="{{$attachment['last_size']}}"><input type="hidden" value="{{$attachment['last_count']}}">{{$attachment['attachment']}}</div>
                                                @if ($module)
                                                    <span class="btn btn-success btnbrowse fileinput-button nte-row-upload">
                                                        <span >+</span>
                                                        <input class="flup" type="file" name="attachments[]" multiple>
                                                    </span>
                                                @endif
                                            </div>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td> 
                                        <input type="text" readonly="" class="form-control inthemiddle" style="width: 100px" value="{{$selectedFromApproval['NTEs'][$j]['nte_reference_number']}}"> <input type="hidden" readonly="" class="ref" name="ref_num[]" value="{{$selectedFromApproval['NTEs'][$j]['nte_id']}}"> 
                                    </td>
                                    <td> 
                                        @foreach($statuses as $status)
                                            @if ($status->id == $selectedFromApproval['NTEs'][$j]['nte_status'])
                                                <label class="labels">{{$status->name}}</label>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td> 
                                        @if ($isEdit || $module)<textarea name="remarks[]" class="rem form-control" style="width: 150px"></textarea>@endif
                                            @if ($selectedFromApproval['comments'] != null)
                                            <?php $comment = ''?>

                                                @for ($b=0;$b<count($selectedFromApproval['comments']);$b++)
                                                    @if ($selectedFromApproval['comments'][$b]['nte_id'] == $selectedFromApproval['NTEs'][$j]['nte_id'])
                                                        <?php $comment .= $selectedFromApproval['comments'][$b]['name'] . ' ' . $selectedFromApproval['comments'][$b]['comment_date'] . ': ' . $selectedFromApproval['comments'][$b]['comment'] . '&#013&#013'; ?>
                                                    @endif
                                                @endfor

                                                @if ($comment != '') 
                                                    <textarea disabled class="form-control" style="width: 150px">{{$comment}}</textarea>
                                            @endif
                                        @endif
                                    <input type="file" name="attachments[]" class="hidden">
                                    </td>
                                </tr>
                            @endfor
                        @endif
                    </tbody>
                </table>
            </div>
            @if ($isEdit && $selectedFromApproval == "")
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-sm-6">
                        &nbsp; &nbsp; &nbsp; 
                        <button type="button" class="btn btn-default btndefault" id="add_nte" {{$selectedFromMultipleEntries != "" ? '' : 'disabled'}} >ADD</button>
                        <button type="button" class="btn btn-default btndefault" id="delete_nte" {{$selectedFromMultipleEntries != "" ? '' : 'disabled'}}>DELETE</button>
                    </div>
                </div>
            @endif
            <div class="clear_20"></div>
        </div>
    </div>
    <div class="clear_20"></div>
    <span class="action-label labels">ACTION</span>
    <div class="form_container">
        <div class="clear_20"></div>
        <div class="row">
            @if ($isEdit || $module)
                @if(isset($buttons))
                    @foreach($buttons as $button)
                        <div class="comment_container">
                            <div class="comment_notes">
                                <label class="button_notes">{{$button['btn_lbl']}}</label>
                            </div> 
                            <div class="comment_button">
                                <button type="button" class="btn btn-default btndefault" id="{{$button['btn_id']}}">{{$button['btn_txt']}}</button>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                    @endforeach
                @endif
            @endif
            <div class="comment_container">
                <div class="inthemiddle">
                    <a type="button" href="{{ URL::previous() }}" class="btn btn-default btndefault">BACK</a>
                </div>
            </div>
        </div>
    </div>
</form>
@include('/nte/crr')
@include('/nte/nte_history')
<?php if ($selectedFromMultipleEntries != '') { ?>
    <script> 
        var employees_selection = {{json_encode($employees)}}; 
    </script>
<?php } else { ?>
    <script>
        var employees_selection = [];
    </script>
<?php } ?>
<script>
    var	token = '{{Session::token()}}';
    var s = '';
    var ls = '';
    var nte_emp = '';

    var statuses = {{$statuses}};
    var crrbuildup = {{$crrbuildups}};
    var rda = {{$rdas}};

    var urlGdft = '{{route('nte.get_all_drafted')}}';
    
    var url = '{{route('nte.add')}}';
    var urlDrow = '{{route('nte.destroy')}}';
    var urlGh = '{{route('nte.get_history_header')}}';
    var urlGd = '{{route('nte.get_history_details')}}';
    var urlGtvy = '{{route('nte.get_total_violated_year')}}';
    var urlHis = '{{route('nte.history')}}';
    var urlUpload = '{{ route('file-uploader.store') }}';
    var urlDownload = '{{ route('nte.download') }}';
    var home = '{{URL::to('/')}}';
    var urlNteListCHRD = '{{route('nte.list', 'CHRD')}}';

    var cM = {{$dates[1]}};
    var cY = {{$dates[0]}};
    var cD = {{$dates[7]}};
    var tD = {{$dates[2]}};
    var month_dd = {{$month_dd}};
    var year_dd = {{$year_dd}};

    var file_size_counter = 0;
    var file_counter = 0;
    var file_count_limit = 10;
    var file_size_limit = 20971520;

    var action = 0;
    var isCreate = '{{$selectedFromApproval != null || $selectedFromMultipleEntries != null ? false : true}}';
    var isFromMultiple = '{{$selectedFromMultipleEntries != null ? false : true}}';
</script>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/nte/nte_browse_file.js') }}
    {{ HTML::script('/assets/js/nte/nte_history.js') }}
    {{ HTML::script('/assets/js/jquery-autocomplete.js') }}
    {{ HTML::script('/assets/js/multiselect/jquery.multiselect.1.13.js') }}
    {{ HTML::script('/assets/js/multiselect/jquery.multiselect.filter.js') }}
    
    @if ($isEdit && !$module) 
        {{ HTML::script('/assets/js/nte/nte_add.js') }}
    @else 
        {{ HTML::script('/assets/js/nte/nte_approval.js') }}
    @endif
@endsection