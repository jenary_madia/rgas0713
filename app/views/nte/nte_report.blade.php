<html>
	<head>
		<title>Notice To Explain Report</title>
		<link href="<?php echo $base_url; ?>/assets/css/nte/nte.css" rel="stylesheet">
		<style type="text/css">
			label 
			{
			    font-family:Arial;
			    font-size: 12px;
			}
			td.bordered
			{
				border: 1px solid black;
			}
		</style>
	</head>
	<body>
		<div>
			@if ($module == 'nte')
				<label class="documentname">NE.COSA.HRMD.02.10</label>
			@else
				<label class="documentname">DA.COSA.HRMD.00.09</label>
			@endif
			<div class="inthemiddle">
				<img src="{{URL::to('assets/img/rebisco_logo.png')}}"> <span class="logo"> &nbsp;&nbsp;&nbsp; <label style="font-size: 14px;"><strong>Rebisco Group of Food Companies</strong></label> </span>
			</div>
		</div>
		<br>
		<div class="inthemiddle"><label style="font-size: 14px;"><strong>{{ $module == 'nte' ? 'NOTICE TO EXPLAIN' : 'NOTICE OF DISCIPLINARY ACTION' }}</strong></label></div>
		<br>
		<div class="data">
			<table align="center" width="100%;">
				<tbody>
					<tr>
						<td><label><strong>Employee Name:</strong></label></td> 
						<td><label>{{$data['name']}}</label></td>
						<td><label><strong>Reference Number:</strong></label></td> 
						<td><label>{{$data['nte_reference_number']}}</label></td>
					</tr>
					<tr>
						<td><label><strong>Employee Number:</strong></label></td>
						<td><label>{{$data['employeeid']}}</label></td>
						<td><label><strong>Date Filed:</strong></label></td>
						<td><label>{{ explode(' ', $data['nte_submitted_chrd_aer'])[0] }}</label></td>
					</tr>
					<tr>
						<td><label><strong>Department:</strong></label></td>
						<td><label>{{$data['dept_name']}}</label></td>
						<td><label><strong>Date Acknowledged:</strong></label></td>
						<td><label>{{ explode(' ', $data['nte_acknowledged'])[0] }}</label></td>
					</tr>
					<tr>
						<td><label><strong>Section:</strong></label></td>
						<td><label>{{$data['sect_name']}}</label></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
		<br>
		<div class="data">
		@if ($module == 'nte')
			<label>For the month of {{$month_nte}}-{{$data['nte_year']}}, {{$data['name']}} was recommended for the following disciplinary action based on the committed infraction, number of times the infraction was violated for the month, and his explanation for the infraction committed.</label>
			<br>
			<br>
			<table align="center" width="90%">
				<thead>
					<tr>
						<th width="25%" class="red"><label>INFRACTION</label></th>
						<th width="25%" class="red"><label>TIMES VIOLATED FOR THE MONTH</label></th>
						<th width="25%" class="red"><label>EXPLANATION FOR INFRACTION</label></th>
						<th width="25%" class="red"><label>DISCIPLINARY ACTION RECOMMENDED BY DEPARTMENT HEAD</label></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="data"><label>{{$data['cb_rule_name']}}</label></td>
						<td class="data"><label>{{$data['nte_total_incurred']}}</label></td>
						<td class="data"><label>{{$data['nte_explanation']}}</label></td>
						<td class="data"><label>{{$data['disact']}}</label></td>
					</tr>
				</tbody>
			</table>
			<br>
			<label>Below is a summary of previous infractions and corresponding disciplinary action received by the employee:</label>
			<br>
			<br>
            <p class="inthemiddle"><label><strong>Previous Infractions and Corresponding Disciplinary Actions</strong></label></p>
            <br>
	        <div class="inthemiddle">
		        <label><strong>As of &nbsp;&nbsp;</strong></label>
		        <input type="text" value="{{$data['nte_year']}}">
	        </div>
	        <br>
            <table align="center" width="90%">
                <thead>
                    <tr>
                        <th width="20%" class="red"><label>MONTH</label></th>
                        <th width="40%" class="red"><label>INFRACTION</label></th>
                        <th width="40%" class="red"><label>APPROVED DISCIPLINARY ACTION</label></th>
                    </tr>
                </thead>
                <tbody>
            		{{$table}}
                </tbody>
            </table>
		@else
			<label>After a thourough investigation and evaluation of your case, including your letter of explanation we recieved last {{$nte_submitted_chrd_aer}}, with Notice to Explain NTE-{{$data['nte_reference_number']}}, the Management concluded among others that you violated the following Rules and Regulations and will be given the respective Disciplinary Action.</label>
			<br>
			<br>
			<table align="center" width="90%">
                <thead>
                    <tr>
                        <th width="50%" class="red"><label>INFRACTION</label></th>
                        <th width="50%" class="red"><label>DISCIPLINARY ACTION</label></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="data"><label>Rule {{$data['cb_rule_number']}} - {{$data['cb_rule_description']}}</label></td>
                        <td class="data"><label>{{$data['disact']}}</label></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <label>Further, we expect you to improve your performance. If there is no improvement on your side or repetition of the same and similar offense in the future are made, we will be constrained to do appropriate actions and shall be dealt with accordingly.</label>
		@endif
		</div>
		<br>
		<div class="data">
			<table align="center" width="100%">
				<tbody>
					<tr>
						@if ($module == 'nte')
							<td><label>Acknowledged By:</label></td>
							<td><label>Noted By:</label></td>
							<td><label>Noted By:</label></td>
						@else
							<td><label>Issued By:</label></td>
							<td><label>Noted By:</label></td>
							<td><label>Acknowledged By:</label></td>
						@endif
					</tr>
					<tr><td><br/></td></tr>
					<tr><td><br/></td></tr>
					<tr>
						@if ($module == 'nte')
							<td><label>{{$data['name']}}</label></td>
							<td><label>{{$supervisor['name']}}</label></td>
							<td><label>{{$department_head}}</label></td>
						@else
							<td><label>{{$chrd_head}}</label></td>
							<td><label>{{$department_head}}</label></td>
							<td><label>{{$data['name']}}</label></td>
						@endif
					</tr>
				</tbody>
			</table>
		</div>
	</body>
</html>
