<html>
	<head></head>
	<body>
		Good day!
		<br /><br />
		@if($module == 'nte.notation')
			You have {{$data['count']}} Notice to Explain for notation
			<br /><br />
			<ul>
			@for($i=0; $i<count($data)-1; $i++)
				<li>{{$data[$i]['dept_name']}} - {{$data[$i]['count']}}</li>
			@endfor
			</ul>
		@elseif($module == 'nda.department.notation')
			You have {{$data['count']}} Notice of Disciplinary Action for notation
			<br /><br />
			<ul>
			@for($i=0; $i<count($data)-1; $i++)
				<li>{{$data[$i]['name']}}</li>
			@endfor
			</ul>
		@elseif($module == 'nte.accomplishment')
			You have 1 new Notice to Explain for checking.
			<br /><br />
			STATUS: {{$data['status_name']}} <br />
			REFERENCE NO.: {{$data['nte_reference_number']}} <br />
			INFRACTION: {{$data['cb_rule_name']}} <br />
			<br /><br />
			Please be advised that you are given 48-hours upon receipt of this notice to accomplish the Explanation Field.
		@elseif($module == 'nte.returned')
			You have 1 new Notice to Explain for checking.
			<br /><br />
			STATUS: {{$data['status_name']}} <br />
			REFERENCE NO.: {{$data['nte_reference_number']}} <br />
			INFRACTION: {{$data['cb_rule_name']}} <br />
			<br /><br />
		@elseif($module == 'nte.review')
			You have 1 new Notice to Explain for checking.
			<br /><br />
			STATUS: {{$data['status_name']}} <br />
			EMPLOYEE NAME: {{$data['name']}} <br />
			REFERENCE NO.: {{$data['nte_reference_number']}} <br />
			INFRACTION: {{$data['cb_rule_name']}} <br />
		@elseif($module == 'acknowledgement')
			You have 1 new Notice of Disciplinary Action for checking.
			<br /><br />
			STATUS: {{$data['status_name']}} <br />
			EMPLOYEE NAME: {{$data['name']}} <br />
			REFERENCE NO.: {{$data['nte_reference_number']}} <br />
			INFRACTION: {{$data['cb_rule_name']}} <br />
		@endif
		<br /><br />
		Click <a href="{{ URL::to('/') }}">here</a> to log-in and check the file.<br />
		<br /><br />
		DO NOT REPLY. THIS IS A SYSTEM-GENERATED MESSAGE.<br />
		Service made possible by CSMD - Application Development.
	</body>
<html>
