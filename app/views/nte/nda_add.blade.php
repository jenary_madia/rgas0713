@extends('../template/header')
@section('content') 
<div class="alert alert-danger container_message" id="container_message"></div> 
<form class="form-inline" action="#" method="post" enctype="multipart/form-data" id="nda_add_form" name="nda_add_form">
    <input type="hidden" value="{{ csrf_token() }}">
    <div class="form_container">
        <div class="container-header">
            <h5 class="text-center">
                <strong>NOTICE OF DISCIPLINARY ACTION</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">EMPLOYEE NAME</label>
                </div>
                <div class="col2_form_container">
                    @if (($returned['nte_status'] == 8 && $returned['nda_status'] == 0) && $editable && isset($isNew))
                    <div class="ui-widget">
                        <div class="input-group">
                            <select class="emp ib" id="emp_name" name="emp_name">
                                <option value=""></option>
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->firstname . ' ' . $employee->middlename . ' ' . $employee->lastname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @else 
                        <input type="text" class="form-control" disabled value="{{isset($returned['name']) ? $returned['name'] : ''}}">
                    @endif
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">REFERENCE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="nda_ref" value="{{isset($returned['nda_reference_number']) ? $returned['nda_reference_number'] : ''}}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">EMPLOYEE NUMBER</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="emp_num" value="{{isset($returned['emp_id']) ? $returned['emp_id'] : ''}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DATE FILED</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="nda_date" value="{{ isset($returned['nda_submitted_chrd_manager']) ? $returned['nda_submitted_chrd_manager'] != 0 ? explode(' ', $returned['nda_submitted_chrd_manager'])[0] : '' : ''}}"/>
                </div>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">DEPARTMENT</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="dept" value="{{isset($returned['dept_name']) ? $returned['dept_name'] : ''}}"/>
                </div>
            </div>
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels required">STATUS</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="status" value="{{isset($returned['stats']) ? $returned['stats'] : ''}}"/>
                </div>
            </div>
        </div> 
        <div class="clear_10"></div>
        <div class="row">
            <div class="row_form_container">
                <div class="col1_form_container">
                    <label class="labels">SECTION</label>
                </div>
                <div class="col2_form_container">
                    <input type="text" class="form-control" disabled id="sect" value="{{isset($returned['sect_name']) ? $returned['sect_name'] : ''}}"/>
                </div>
            </div>
        </div>   
        <div class="container-header">
            <h5 class="text-center lined">
                <input type="hidden" id="ref_num" name="ref_num" value="{{isset($returned['nte_id']) ? $returned['nte_id'] : ''}}">
                <strong>DISCIPLINARY ACTION</strong>
            </h5>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">NOTICE TO EXPLAIN REFERENCE NUMBER</label>
                        </div>
                        <div class="col2_form_container">
                            @if (($returned['nte_status'] == 8 && $returned['nda_status'] == 0) && $editable && isset($isNew))
                                <select class="form-control ib" id="ref" width="100%"></select>
                            @else
                                <input type="text" class="form-control" disabled value="NTE-{{isset($returned['nte_reference_number']) ? $returned['nte_reference_number'] : ''}}">
                            @endif
                        </div>
                    </div>
                </div>  
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE NOTICE TO EXPLAIN WAS RECIEVED BY CHRD</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled id="chrd_date" value="{{isset($returned['nte_submitted_chrd_aer']) ? explode(' ', $returned['nte_submitted_chrd_aer'])[0] : ''}}"/>
                        </div>
                    </div>
                </div>        
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY RULES AND REGULATIONS VIOLATED</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled id="crr" value="{{isset($returned['cb_rule_name']) ? $returned['cb_rule_name'] : ''}}"/>
                        </div>
                    </div>
                </div>        
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DISCIPLINARY ACTION</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" disabled id="da" value="{{isset($returned['disact']) ? $returned['disact'] : ''}}"/>
                        </div>
                    </div>
                </div>  
                @if ($returned['nte_status'] > 7) 
                <div class="sus" {{ isset($returned['nte_disciplinary_action']) ? $returned['nte_disciplinary_action'] === 4 ? 'style="display: block;"' : 'style="display: none;"' : 'style="display: none;"'}}>
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE OF SUSPENSION  FROM</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" disabled id="sus_from" value="{{isset($returned['nte_start_suspension']) ? $returned['nte_start_suspension'] : ''}}"/>
                            </div>
                        </div>
                    </div>   
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  TO</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" disabled id="sus_to" value="{{isset($returned['nte_end_suspension']) ? $returned['nte_end_suspension'] : ''}}"/>
                            </div>
                        </div>
                    </div>   
                    <div class="clear_10"></div>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">NUMBER OF DAYS OF SUSPENSION</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" disabled id="sus_no" value="{{isset($returned['nte_no_of_days']) ? $returned['nte_no_of_days'] : ''}}"/>
                            </div>
                        </div>
                    </div>
                </div>
                @endif   
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <label class="labels">
                            <input type="hidden" name="action" id="action">
                            COMPANY RULES AND REGULATIONS DESCRIPTION
                        </label>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-12">
                        <textarea rows="15" class="form-control" disabled id="crr_desc">{{isset($crr) ? $crr : ''}}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-header">
            <h5 class="text-center lined">
                <strong>ATTACHMENTS</strong>
            </h5>
        </div>
        <div class="clear_10"></div>
        @if($filer_upload)
            <label class="labels">ADD ATTACHMENTS (MAXIMUM OF 5 ATTACHMENTS)</label>
            <br>
            <span class="btn btn-success btnbrowse fileinput-button">
                <span >BROWSE</span>
                <input id="ndaf" type="file" class="browse" name="attachments[]" multiple>
            </span>
        @endif
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="ndaf_attachments">{{ isset($ndaf_attch_html) ? $ndaf_attch_html : '' }}</div>
        </div>
        <div class="clear_10"></div>
        @if($manager_upload)
            <label class="labels">ADD ATTACHMENTS (MAXIMUM OF 5 ATTACHMENTS)</label>
            <br>
            <span class="btn btn-success btnbrowse fileinput-button">
                <span >BROWSE</span>
                <input id="ndah" type="file" class="browse" name="attachments[]" multiple>
            </span>
        @endif
        <div class="clear_10"></div>
        <div class="attachment_container">
            <div class="atch" id="ndah_attachments">{{ isset($ndah_attch_html) ? $ndah_attch_html : '' }}</div>
        </div>
        <div class="clear_20"></div>
    </div>
    <div class="clear_20"></div>
    <span class="action-label labels">ACTION</span>
    <div class="form_container">
        <div class="clear_20"></div>
        <div class="textarea_messages_container">
            @if($returned['nda_status'] > 9)
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" disabled>{{$message != '' ? $message : ''}}</textarea>
                </div>
                <div class="clear_10"></div>
            @endif
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" class="form-control textarea_inside_width ib" {{$editable || (($returned['nda_status'] == 10 || $returned['nda_status'] == 12) && Session::get('employee_id') == $returned['nda_filer_id']) ? 'name="remarks"' : 'disabled'}}>{{ isset($my_comment) ? $my_comment : '' }}</textarea>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row">
            @if(isset($buttons))
                @foreach($buttons as $button)
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes">{{$button['btn_lbl']}}</label>
                        </div> 
                        <div class="comment_button" style="padding-left: 167px;">
                            <button type="button" class="btn btn-default btndefault" id="{{$button['btn_id']}}" style="width: 110px;">{{$button['btn_txt']}}</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                @endforeach
            @endif
            <div class="comment_container">
                <div class="inthemiddle">
                    @if (($returned['nda_status'] == 10 || $returned['nda_status'] == 12) && $returned['nda_filer_id'] == Session::get('employee_id'))
                        <button type="button" class="btn btn-default btndefault" id="cancel">CANCEL REQUEST</button>
                    @endif
                    <a type="button" href="{{   URL::previous() }}" class="btn btn-default btndefault">BACK</a>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    var	token = '{{Session::token()}}';
    var url = '{{route('nte.add')}}';
    var urlGbEI = '{{route('nda.get_bei')}}';
    var urlGbEIRN = '{{route('nda.get_beirn')}}';
    var action = 0;
    var urlUpload = '{{ route('file-uploader.store') }}';
    var stat = '{{isset($returned['nte_status']) ? $returned['nte_status'] : ''}}';

    var ndaf_ctr = {{isset($ndaf_last_count) ? $ndaf_last_count : 0}};
    var ndaf_size_ctr = {{isset($ndaf_last_size) ? $ndaf_last_size : 0}};

    var ndah_ctr = {{isset($ndah_last_count) ? $ndah_last_count : 0}};
    var ndah_size_ctr = {{isset($ndah_last_size) ? $ndah_last_size : 0}};

    var home = '{{URL::to('/')}}';
    var urlNdaListCHRD = '{{route('nda.list', 'CHRD')}}';
</script>
@stop

@section('js_ko')
    {{ HTML::script('/assets/js/nte/nte_browse_file.js') }}
    {{ HTML::script('/assets/js/nte/nda.js') }}
    {{ HTML::script('/assets/js/jquery-autocomplete.js') }}
@endsection
