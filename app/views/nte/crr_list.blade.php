@extends('../template/header')
@section('content')
<div class="wrapper">
    <a href="{{route('crr.create')}}" type="button" class="btn btn-default btndefault">ADD CRR</a>
    <div class="clear_20"></div>
    <table id="crr_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable hand" width="100%">
        <thead>
        <tr role="row">
            <th style="text-align:center;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ID</th>
            <th style="text-align:center;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RULE NAME</th>
            <th style="text-align:center;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RULE NO.</th>
            <th style="text-align:center;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RULE DESCRIPTION</th>
            <th style="text-align:center;" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
        </tr>
        </thead>
    </table>

    <form method="POST" action="{{route('crr.edit')}}">
    	<input hidden="text" id="header_rule" name="header_rule">
    	<input type="submit" id="edit_submit" class="hidden">
    </form>
</div>

<script>
	var	token = '{{Session::token()}}';
	var url = '{{route('crr.get')}}';
</script>
@stop
    
@stop
@section('js_ko')
	{{ HTML::script('/assets/js/nte/crr_build_up.js') }}
@endsection