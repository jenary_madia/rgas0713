@extends('template/header')

@section('content')
<div id="wrap">
    @include('template/sidebar')
    <div class="container">
        <form class="form-inline">

            <div class="form_container">
                <label class="form_title">EXTERNAL TRAINING ENDORSEMENT FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Name:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Reference Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Employee Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Date Filed:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control placeholders" placeholder="yyyy-mm-dd" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Company:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Status:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Department:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Contact Number:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Designation:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label training_details">TRAINING DETAILS</label>

                    <div class="clear_20"></div>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Training Title:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Vendor:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Training Date:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Training Venue:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">Total Amount:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels">Echo Training Date:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        


                        <div class="radiobutton_container">
                            <div class="radiobtn ">
                                <input type="radio" name="" value=""> 5,000 and Below
                            </div>
                            <div class="radiobtn ">
                               <input type="radio" name="" value=""> 5,001 to 50,000
                            </div>
                            <div class="radiobtn ">
                                <input type="radio" name="" value=""> 50,001 and Above
                            </div>
                        </div>  
                        <div class="clear_20"></div>

                        <table border = "1" cellpadding = "0" class="tbl3">
                            <th class="th_style td_height">Employee Name</th>
                            <th class="th_style td_height">Department</th>
                            <th class="th_style td_height">Designation</th>

                            <tr>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                            </tr>
                            <tr>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                            </tr>
                            <tr>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                                <td class="td_style td_height"></td>
                            </tr>
                        </table>
                        <div class="ete_btns">
                            <button type="submit" class="btn btn2 btn-default" value="" id="add_employee">Add</button>
                            <button type="submit" class="btn btn2 btn-default" value="">Edit</button>
                            <button type="submit" class="btn btn2 btn-default" value="">Delete</button>
                        </div>

                        <div class="clear_20"></div>
                        <div class="panel panel-default panel_width">
                          <div class="panel-body panel_text">
                            <strong>Note: </strong>The above mentioned employees is/are required to submit a photocopy of his/her/their
                            <strong>Seminar Certificate(s), a </strong><strong>Seminar Feedback Report </strong>including the Action Plan on
                            the Application of Learning to Training - Corp. HRD, and conduct an <strong>Echo Session </strong>(as applicable)
                            2 weeks after seminar date to peers. Kindly indicate the <strong>Date  of Echo Training </strong>above.
                          </div>
                        </div>


                    </div><!-- end of form_container -->
                    
                    <div class="clear_20"></div>

                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <button type="submit" class="btn btnbrowse btn-default" value="">BROWSE</button><br/>
                        <div class="sample_file">SAMPLE 1 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
                        <div class="sample_file">SAMPLE 2 FILENAME</div>
                        <div class="delete_file">
                            <button type="submit" class="btn btndefault btn-default" value="">DELETE</button><br/>
                        </div>
                    </div>

                    <div class="clear_20"></div>

                    <label class="form_label pmarf_form_title">ASSESSMENT AND RECOMMENDATIONS</label>

                    <div class="clear_20"></div>

                    <label class="pmarf_textarea_labels">1. Target competencies to be developed/acquired in the performance of current function:</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">2. Target competencies to be developed/acquired to perform a NEW FUNCTION or PROJECT:</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="te_textarea_labels"><i>(Please provide a brief description of the new function or project)</i></label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <table border = "1" cellpadding = "0" class="tbl_te">
                        <th class="th_style th_left">Requirements Checklist</th>
                        <th class="th_style">YES</th>
                        <th class="th_style">NO</th>
                        <th class="th_style">N/A</th>

                        <tr>
                            <td class="td_style ">1.Is the participant a regular employee? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="regular_employee" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="regular_employee" value="no"></td>
                            <td class="td_radios "><input type="radio" name="regular_employee" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">2.Has the participant submitted ALL the requirements from the previous external seminars attended? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="submit_req" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="submit_req" value="no"></td>
                            <td class="td_radios "><input type="radio" name="submit_req" value="na"></td>
                        </tr>

                         <tr>
                            <td class="td_style ">3.Is this a new seminar nobody in the department has attended? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="new_seminar" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="new_seminar" value="no"></td>
                            <td class="td_radios "><input type="radio" name="new_seminar" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">4.Is there an accomplished Individual Development Plan (IDP)?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="idp" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="idp" value="no"></td>
                            <td class="td_radios "><input type="radio" name="idp" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">5.Is there an accomplished Trainining Grant Agreement (for trainings worth 15,000.00 or more?)</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="tga" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="tga" value="no"></td>
                            <td class="td_radios "><input type="radio" name="tga" value="na"></td>
                        </tr>

                        <tr>
                            <th class="th_style th_left">Evaluation Criteria</th>
                        </tr>

                        <tr>
                            <td class="td_style ">6.Is the target competency requiring proficiency level 2 or higher?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="proficiency_level" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="proficiency_level" value="no"></td>
                            <td class="td_radios "><input type="radio" name="proficiency_level" value="na"></td>
                        </tr>
                        <tr>
                            <td class="td_style ">7.Is the competency gap result 1.0 or higher?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="gap_result" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="gap_result" value="no"></td>
                            <td class="td_radios "><input type="radio" name="gap_result" value="na"></td>
                        </tr>
                        <tr>
                            <td class="td_style ">8.is this required in more than 50% of the participant's work function or required for new assignment or project?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="required" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="required" value="no"></td>
                            <td class="td_radios "><input type="radio" name="required" value="na"></td>
                        </tr>
                    </table>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">Training and OD - Corporate HR Overall Recommendation</label>
                    <div class="ete_radio_btn">
                        <input type="radio" name="recommendation" value=""> Strongly Recommended (Yes: 6-8)<br>
                        <input type="radio" name="recommendation" value=""> Recommended with Reservations (Yes: 4-5)<br>
                        <input type="radio" name="recommendation" value=""> Not Recommended (Yes:1-3)<br>
                    </div>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">External Training attended: (If Any)/Dates</label>
                    <div class="pmarf_textarea">
                        <textarea name="purpose_request" rows="2" cols="10" class="form-control"> </textarea>
                    </div>

            </div> <!-- end of form_container--> 

            <div class="clear_20"></div>
            <div class="form_container">
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea name="purpose_request" rows="3" class="form-control textarea_inside_width" disabled></textarea>
                    </div>
                    <div class="clear_10"></div>
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea name="purpose_request" rows="3" class="form-control textarea_inside_width"></textarea>
                    </div>
                </div>

                <div class="clear_20"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>APPROVE</strong> REQUEST</label>
                        </div> 
                        <div class="pmarf_buttons_spacing">
                            <button type="submit" class="btn btn-default pmarf_buttons" value="">APPROVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container"> 
                        <div class="comment_notes">
                            <label class="button_notes"><strong>DISAPPROVE</strong> REQUEST</label>
                        </div> 
                        <div class="pmarf_buttons_spacing">
                            <button type="submit" class="btn btn-default pmarf_buttons" value="">DISAPPROVE</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container --> 
            <div class="clear_20"></div>
        </form>
        <div class="clear_20"></div>

    </div><!-- container -->  
    <div class="clear_60"></div>
</div><!-- wrap -->

@stop