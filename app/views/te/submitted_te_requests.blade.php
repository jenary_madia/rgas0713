@extends('template/header')

@section('content')
	<div class="wrapper">
		<div class="clear_10"></div>
		@if(Session::get('desig_level') == 'head')
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY TRAINING ENDORSEMENT REQUESTS</span>
            <table id="endorsements_my_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="endorsements_my_list">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NUMBER</th>
						<th align="center">FILER</th>
						<th align="center">TRAINING TITLE</th>
						<th align="center">DATE FILED</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
			<div class="clear_20"></div>
			<hr /> 
		@endif
		
		
		@if(Session::get('is_te_chrd'))
			
			<div class="datatable_holder">
	     <table id="endorsements_new_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="endorsements_new_list">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NO.</th> 
						<th align="center">FILER</th>
						<th align="center">COMPANY</th>
						<th align="center">DEPARTMENT</th>
						<th align="center">DATE FILED</th>
						<th align="center">STATUS</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center">ACTION</th>	
					</tr>							
				</thead>
			</table>
			</div>
					<div class="clear_20"></div> 
							<hr />

		@endif

		@if(Session::get('designation') == 'VP FOR CORP. HUMAN RESOURCE')
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
			<table id="endorsements_chrd_vp_approval" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="endorsements_chrd_vp_approval">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">FILER</th>
						<th align="center">COMPANY</th>
						<th align="center">DEPARTMENT</th>
						<th align="center">DATE FILED</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
					<div class="clear_20"></div> 
							<hr />
		@endif

		@if(Session::get('designation') == 'SVP FOR CORP. SERVICES')

			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
			<table id="endorsements_svp_for_cs_approval" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="endorsements_svp_for_cs_approval">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">FILER</th>
						<th align="center">COMPANY</th>
						<th align="center">DEPARTMENT</th>
						<th align="center">DATE FILED</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
		<div class="clear_20"></div> 
		<hr />

		@endif
		@if(Session::get('desig_level') == 'president')
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
			<table id="endorsements_pres_approval" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="endorsements_svp_for_cs_approval">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">FILER</th>
						<th align="center">COMPANY</th>
						<th align="center">DEPARTMENT</th>
						<th align="center">DATE FILED</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
					<hr />

		@endif
	</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this endorsement "+$(this).data('request-training-title')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop


