                    <label class="form_label pmarf_form_title">ASSESSMENT AND RECOMMENDATIONS</label>

                    <div class="clear_20"></div>

                    <label class="pmarf_textarea_labels">1. Target competencies to be developed/acquired in the performance of current function:</label>
                    <div class="pmarf_textarea">
                        <textarea name="teq_target_performance" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">2. Target competencies to be developed/acquired to perform a NEW FUNCTION or PROJECT:</label>
                    <div class="pmarf_textarea">
                        <textarea name="teq_target_competency" rows="2" cols="10" class="form-control"> </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="te_textarea_labels"><i>(Please provide a brief description of the new function or project)</i></label>
                    <div class="pmarf_textarea">
                        <textarea name="teq_newfunction_description" rows="2" cols="10" class="form-control"> </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <table border = "1" cellpadding = "0" class="tbl_te">
                        <th class="th_style th_left">Requirements Checklist</th>
                        <th class="th_style">YES</th>
                        <th class="th_style">NO</th>
                        <th class="th_style">N/A</th>

                        <tr>
                            <td class="td_style ">1.Is the participant a regular employee? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_reg_emp" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_reg_emp" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_reg_emp" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">2.Has the participant submitted ALL the requirements from the previous external seminars attended? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_submitted_req" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_submitted_req" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_submitted_req" value="na"></td>
                        </tr>

                         <tr>
                            <td class="td_style ">3.Is this a new seminar nobody in the department has attended? </td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_new_seminar" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_new_seminar" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_new_seminar" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">4.Is there an accomplished Individual Development Plan (IDP)?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_idp" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_idp" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_idp" value="na"></td>
                        </tr>

                        <tr>
                            <td class="td_style ">5.Is there an accomplished Trainining Grant Agreement (for trainings worth 15,000.00 or more?)</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_tga" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_tga" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_tga" value="na"></td>
                        </tr>

                        <tr>
                            <th class="th_style th_left">Evaluation Criteria</th>
                        </tr>

                        <tr>
                            <td class="td_style ">6.Is the target competency requiring proficiency level 2 or higher?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_target_2_higher" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_target_2_higher" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_target_2_higher" value="na"></td>
                        </tr>
                        <tr>
                            <td class="td_style ">7.Is the competency gap result 1.0 or higher?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_result_1_higher" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_result_1_higher" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_result_1_higher" value="na"></td>
                        </tr>
                        <tr>
                            <td class="td_style ">8.is this required in more than 50% of the participant's work function or required for new assignment or project?</td>
                            <td class="td_radios" class="text-center"><input type="radio" name="teq_required" value="yes"></td>
                            <td class="td_radios "><input type="radio" name="teq_required" value="no"></td>
                            <td class="td_radios "><input type="radio" name="teq_required" value="na"></td>
                        </tr>
                    </table>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">Training and OD - Corporate HR Overall Recommendation</label>
                    <div class="ete_radio_btn">
                        <input type="radio" name="teq_overall_recom" value="strongly_recomm"> Strongly Recommended (Yes: 6-8)<br>
                        <div class="clear_10"></div>
                        <input type="radio" name="teq_overall_recom" value="recomm_reservation"> Recommended with Reservations (Yes: 4-5)<br>
                        <div class="clear_10"></div>
                        <input type="radio" name="teq_overall_recom" value="not_recomm"> Not Recommended (Yes:1-3)<br>
                    </div>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">External Training attended: (If Any)/Dates</label>
                    <div class="pmarf_textarea">
                        <textarea name="teq_eta" rows="2" cols="10" class="form-control"> </textarea>
                    </div>