
        <form class="form-inline" action="{{URL::current()}}" method="post" enctype="multipart/form-data" id="example">
			<input name="_token" type="hidden" value="{{ csrf_token() }}"/>
			<input name="te_id" type="hidden" value="{{ $request['te_id'] }}"/>
			
            <div class="form_container" id="">
                <label class="form_title">EXTERNAL TRAINING ENDORSEMENT FORM</label>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE_NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_emp_name'] }}"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_ref_num'] }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_emp_id'] }}"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control text-left" readonly="readonly" value="{{ $request['te_date_filed'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_company'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_status'] }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_department'] }}"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_contact_no'] }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_emp_section'] }}"/>
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>TRAINING DETAILS</strong></h5></div>
                    
                    <div class="clear_20"></div>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TRAINING TITLE:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_training_title'] }}"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels required">VENDOR:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_vendor'] }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TRAINING DATE:</label>
                            </div>
                            <div class="col2_form_container input-group">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_training_date'] }}"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels required">TRAINING VENUE:</label>
                            </div>
                            <div class="col5_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_training_venue'] }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TOTAL AMOUNT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_total_amount'] }}"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels required">ECHO TRAINING DATE:</label>
                            </div>
                            <div class="col5_form_container input-group">
                                <input type="text" class="form-control" readonly="readonly" value="{{ $request['te_echo_training_date'] }}"/>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
                            </div>
                        </div>
					
                        <div class="clear_20"></div>
						<table border = "1" cellpadding = "0" class="tbl3" id="tep_participants">
						<th class="th_style td_height">EMPLOYEE NAME</th>
                        <th class="th_style td_height">DEPARTMENT</th>
                        <th class="th_style td_height">DESIGNATION</th>
						@foreach($tepRecord as $tepr)
							<tr>
								<td><input type="hidden" name="p_name[]">{{ $tepr->tep_emp_name }}</td>
								<td><input type="hidden" name="p_department[]">{{ $tepr->tep_dep_name }}</td>
								<td><input type="hidden" name="p_designation[]">{{ $tepr->tep_designation ? $tepr->tep_designation : "" }}</td>
							</tr>
						@endforeach					
					</table>
						<div class="clear_20"></div>
						
                    </div><!-- end of form_container -->
					<div class="clear_20"></div>
                    <label class="form_label pmarf_form_title">ASSESSMENT AND RECOMMENDATIONS</label>

                    <div class="clear_20"></div>

                    <label class="pmarf_textarea_labels">1. Target competencies to be developed/acquired in the performance of current function:</label>
                    <div class="pmarf_textarea">
                        <textarea name="teq_target_performance" rows="2" cols="10" class="form-control" disabled>{{ $request['teq_target_performance'] }} </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels">2. Target competencies to be developed/acquired to perform a NEW FUNCTION or PROJECT:</label>
                    <div class="pmarf_textarea">
                        <textarea name="target_competency" rows="2" cols="10" class="form-control" disabled>{{ $request['teq_target_competency'] }}  </textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="te_textarea_labels"><i>(Please provide a brief description of the new function or project)</i></label>
                    <div class="pmarf_textarea">
                        <textarea name="newfunction_description" rows="2" cols="10" class="form-control" disabled>{{ $request['teq_newfunction_description'] }} </textarea>
                    </div>

                    <div class="clear_20"></div>
                    <table border = "1" cellpadding = "0" class="tbl_te">
                        <th class="th_style th_left text-left">REQUIREMENTS CHECKLIST</th>
                        <th class="th_style">YES</th>
                        <th class="th_style">NO</th>
                        <th class="th_style">N/A</th>

                        <tr>
                            <td class="td_style ">1.Is the participant a regular employee? </td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="regular_employee" value="yes"
								<?php if ($request['teq_reg_emp'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="regular_employee" value="no"
								<?php if ($request['teq_reg_emp'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="regular_employee" value="na"
								<?php if ($request['teq_reg_emp'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>

                        <tr>
                            <td class="td_style ">2.Has the participant submitted ALL the requirements from the previous external seminars attended? </td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="submit_req" value="yes"
								<?php if ($request['teq_submitted_req'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="submit_req" value="no"
								<?php if ($request['teq_submitted_req'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="submit_req" value="na"
								<?php if ($request['teq_submitted_req'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>

                         <tr>
                            <td class="td_style ">3.Is this a new seminar nobody in the department has attended? </td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="new_seminar" value="yes"
								<?php if ($request['teq_new_seminar'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="new_seminar" value="no"
								<?php if ($request['teq_new_seminar'] == "no"): ?> checked = "checked" <?php endif; ?>>
								</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="new_seminar" value="na"
								<?php if ($request['teq_new_seminar'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>

                        <tr>
                            <td class="td_style ">4.Is there an accomplished Individual Development Plan (IDP)?</td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="idp" value="yes"
								<?php if ($request['teq_idp'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="idp" value="no"
								<?php if ($request['teq_idp'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="idp" value="na"
								<?php if ($request['teq_idp'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>

                        <tr>
                            <td class="td_style ">5.Is there an accomplished Trainining Grant Agreement (for trainings worth 15,000.00 or more?)</td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="tga" value="yes"
								<?php if ($request['teq_tga'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="tga" value="no"
								<?php if ($request['teq_tga'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="tga" value="na"
								<?php if ($request['teq_tga'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>

                        <tr>
                            <th class="th_style th_left text-left">EVALUATION CRITERIA</th>
                        </tr>

                        <tr>
                            <td class="td_style ">6.Is the target competency requiring proficiency level 2 or higher?</td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="proficiency_level" value="yes"
								<?php if ($request['teq_target_2_higher'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="proficiency_level" value="no"
								<?php if ($request['teq_target_2_higher'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="proficiency_level" value="na"
								<?php if ($request['teq_target_2_higher'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>
                        <tr>
                            <td class="td_style ">7.Is the competency gap result 1.0 or higher?</td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="gap_result" value="yes"
								<?php if ($request['teq_result_1_higher'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="gap_result" value="no"
								<?php if ($request['teq_result_1_higher'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="gap_result" value="na"
								<?php if ($request['teq_result_1_higher'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>
                        <tr>
                            <td class="td_style ">8.is this required in more than 50% of the participant's work function or required for new assignment or project?</td>
                            <td class="td_radios" class="text-center">
								<input disabled="disable" type="radio" name="required" value="yes"
								<?php if ($request['teq_required'] == "yes"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="required" value="no"
								<?php if ($request['teq_required'] == "no"): ?> checked = "checked" <?php endif; ?>>
							</td>
                            <td class="td_radios ">
								<input disabled="disable" type="radio" name="required" value="na"
								<?php if ($request['teq_required'] == "na"): ?> checked = "checked" <?php endif; ?>>
							</td>
                        </tr>
                    </table>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">TRAINING AND OD – CORPORATE HR OVERALL RECOMMENDATION:</label>
                    <div class="ete_radio_btn">
                        <input disabled="disable" type="radio" name="overall_recom" value="strongly_recommended" 
						<?php if ($request['teq_overall_recom'] == "strongly_recommended"): ?> checked = "checked" <?php endif; ?>>
						Strongly Recommended (Yes: 6-8)<br>
                        <div class="clear_10"></div>
                        <input disabled="disable" type="radio" name="overall_recom" value="recommended_with_reservations"
						<?php if ($request['teq_overall_recom'] == "recommended_with_reservations"): ?> checked = "checked" <?php endif; ?>> Recommended with Reservations (Yes: 4-5)<br>
                        <div class="clear_10"></div>
                        <input disabled="disable" type="radio" name="overall_recom" value="not_recommended"
						<?php if ($request['teq_overall_recom'] == "not_recommended"): ?> checked = "checked" <?php endif; ?>> Not Recommended (Yes:1-3)<br>
                    </div>

                    <div class="clear_20"></div>
                    <label class="pmarf_textarea_labels">EXTERNAL TRAINING/S ATTENDED, DATES (IF ANY):</label>
                    <div class="pmarf_textarea">
                        <textarea name="eta" rows="2" cols="10" class="form-control" disabled>{{ $request['teq_eta'] }} </textarea>
                    </div>
					
					<div class="clear_20"></div>
					<div class="row signatures border">	
						<div class="col-md-12"><h6>For CHRD:</h6></div>
						<div class="clear_60"></div>
						<div class="col-md-4">Assigned To:</div>
						<div class="col-md-4">Date Assigned:</div>
						<div class="col-md-4">Date Completed:</div>
						<div class="clear_20"></div>
					</div>
					
					<div class="clear_20"></div>
					<div class="row signatures border">	
						<div class="col-md-12"><h6>REVIEW AND APPROVAL</h6></div>
						<div class="col-md-6"><h6>Reviewed By:</h6></div>
						<div class="col-md-6"><h6>Approved By:</h6></div>
						<div class="clear_60"></div>
						<div class="col-md-6"><strong>VP of Corporate Human Resource</strong></div>
						<div class="col-md-6"><strong>SVP of Corporate Services/President</strong></div>
						<div class="clear_20"></div>
					</div>
					
					<div class="clear_20"></div>
					
            </div> <!-- end of form_container--> 

            <div class="clear_20"></div>
            <div class="form_container">
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea name="te_comments" rows="3" class="form-control textarea_inside_width" readonly="readonly">@if( $request['te_comments'] != '')@foreach(json_decode($request['te_comments']) as $comment){{ $comment->name }} {{ $comment->datetime }} {{ $comment->message }}&#13;&#10;@endforeach @endif</textarea>
                    </div>
                </div>
            </div><!-- end of form_container --> 
            <div class="clear_20"></div>
        </form>
		<div class="print">
						<a class="btn btn-default btndefault" id="printThis" href="{{ URL::to('te/print/').'/'.$request['te_id'] }}">PRINT</a>
					</div>
        <div class="clear_20"></div>

    <div class="clear_60"></div>



