@extends('template/header')

@section('content')
	<form class="form-inline" action="{{URL::current()}}" method="post" enctype="multipart/form-data">
		<input type="hidden" value="{{ csrf_token() }}">
		<input type="hidden" value="{{ $request['te_id'] }}">
		<input type="hidden" value="{{ $request['te_ref_num'] }}">
		<div class="form_container">
			<label class="form_title">EXTERNAL TRAINING ENDORSEMENT FORM</label>

				<div class="row">
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">EMPLOYEE NAME:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="employee_name" value="{{ $request['te_emp_name'] }}"/>
						</div>
					</div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">REFERENCE NUMBER:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="te_ref_num" value="{{ $request['te_ref_num'] }}"/>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">EMPLOYEE NUMBER:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="employeeid" value="{{ $request['te_emp_id'] }}"/>
						</div>
					</div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">DATE FILED:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control text-left" name="te_date_filed" readonly="readonly" value="{{ $request['te_date_filed'] }}" />
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">COMPANY:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="comp_name" value="{{ $request['te_company'] }}" />
						</div>
					</div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">STATUS:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="te_status" value="{{ $request['te_status'] }}"/>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">DEPARTMENT:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" readonly="readonly" name="dept_name" value="{{ $request['te_department'] }}"/>
						</div>
					</div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels">CONTACT NUMBER:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" name="te_contact_number" value="{{ Input::old('te_contact_no') != '' ? Input::old('te_contact_no') :  $request['te_contact_no'] }}"/>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels">SECTION:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" name="section"  value="{{ $request['te_emp_section'] }}"/>
						</div>
					</div>
				</div>

				<div class="clear_20"></div>

				 <div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>TRAINING DETAILS</strong></h5></div>
                    
				<div class="clear_20"></div>

				<div class="row">
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">TRAINING TITLE:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" name="te_training_title" value="{{ Input::old('te_training_title') != '' ? Input::old('te_training_title') :  $request['te_training_title'] }}" />
						</div>
					</div>
					<div class="row_form_container">
						<div class="col4_form_container">
							<label class="labels required">VENDOR:</label>
						</div>
						<div class="col5_form_container">
							<input type="text" class="form-control" name="te_vendor" value="{{ Input::old('te_vendor') != '' ? Input::old('te_vendor') : $request['te_vendor'] }}"/>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">TRAINING DATE:</label>
						</div>
						<div class="col2_form_container input-group">
							<input type="text" class="form-control date_picker" name="te_training_date" id="te_training_date" value="{{ Input::old('te_training_date') != '' ? Input::old('te_training_date') : $request['te_training_date'] }}"/>
							<label class="input-group-addon btn" for="te_training_date">
							   <span class="glyphicon glyphicon-calendar"></span>
							</label>
						</div>
					</div>
					<div class="row_form_container">
						<div class="col4_form_container">
							<label class="labels required">TRAINING VENUE:</label>
						</div>
						<div class="col5_form_container">
							<input type="text" class="form-control" name="te_training_venue" value="{{ Input::old('te_training_venue') != '' ? Input::old('te_training_venue') : $request['te_training_venue'] }}"/>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="row_form_container">
						<div class="col1_form_container">
							<label class="labels required">TOTAL AMOUNT:</label>
						</div>
						<div class="col2_form_container">
							<input type="text" class="form-control" name="te_total_amount" value="{{ Input::old('te_total_amount') != '' ? Input::old('te_total_amount') : $request['te_total_amount'] }}"/>
						</div>
					</div>
					<div class="row_form_container">
						<div class="col4_form_container">
							<label class="labels required">ECHO TRAINING DATE:</label>
						</div>
						<div class="col5_form_container input-group">
							<input type="text" class="form-control date_picker" id="te_echo_training_date" name="te_echo_training_date" value="{{ Input::old('te_echo_training_date') != '' ? Input::old('te_echo_training_date') : $request['te_echo_training_date'] }}"/>
							<label class="input-group-addon btn" for="te_echo_training_date">
							   <span class="glyphicon glyphicon-calendar"></span>
							</label>
						</div>
					</div>
					
					<div class="clear_20"></div>
					<table border = "1" cellpadding = "0" class="tbl3" id="tep_participants">
						<th class="th_style td_height" style="border-top: 1px solid #000000">EMPLOYEE NAME</th>
						<th class="th_style td_height" style="border-top: 1px solid #000000">DEPARTMENT</th>
						<th class="th_style td_height" style="border-top: 1px solid #000000">DESIGNATION</th>
						@foreach($tepRecord as $tepr)
							<tr class="row-selected" class="del" data-department-id="{{ $tepr->tep_dep_id }}" data-employee-id="{{ $tepr->tep_emp_id }}">
							<td class="td_style td_height text-center"><input class="del" type="hidden" name="p_name[]" value="{{ $tepr->tep_emp_name }}" id="p_name[]">{{ $tepr->tep_emp_name }}<input type="hidden" name="p_id[]" value="{{ $tepr->tep_emp_id }}" id="p_id[]"></td>
								<td class="td_style td_height text-center"><input type="hidden" name="p_department[]" value="{{ $tepr->tep_dep_name }}" id="p_department[]">{{ $tepr->tep_dep_name }}<input type="hidden" name="p_dep_id[]" value="p_dep_id[]"></td>
								<td class="td_style td_height text-center"><input type="hidden" name="p_designation[]" value="{{ $tepr->tep_designation }}">{{ $tepr->tep_designation ? $tepr->tep_designation : "" }}</td>
							</tr>
						@endforeach	
					</table>
					<div class="ete_btns">
						<button type="button" class="btn btn2 btn-xs btn-default" id="btnAdd" name="add_employee">ADD</button>
						<button type="button" class="btn btn2 btn-xs btn-default btnEditDelete" value="" id="btnEdit" disabled="disabled" name="edit_employee">EDIT</button>
						<button type="button" class="btn btn2 btn-xs btn-default btnEditDelete" value="" id="btnDelete" disabled="disabled" name="delete_employee">DELETE</button>
					</div>
					
					<div class="clear_20"></div>
					<div class="panel panel-default panel_width">
					  <div class="panel-body panel_text">
						<strong>Note: </strong>The above mentioned employees is/are required to submit a photocopy of his/her/their
						<strong>Seminar Certificate(s), a </strong><strong>Seminar Feedback Report </strong>including the Action Plan on
						the Application of Learning to Training - Corp. HRD, and conduct an <strong>Echo Session </strong>(as applicable)
						2 weeks after seminar date to peers. Kindly indicate the <strong>Date  of Echo Training </strong>above.
					  </div>
					</div>
					
				</div><!-- end of form_container -->
				<div class="clear_20"></div>
					<div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
				<label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
				<div class="attachment_container">
					@if(count(json_decode($request['te_attachments'])) > 0)
						@foreach(json_decode($request['te_attachments']) as $attachment)
						<div>
						<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
						<a href="{{ URL::to('/te/download/') . '/' . $request['te_ref_num']. '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
						<button class='remove_file_attachment btn btn-xs btn-danger btn_del'>DELETE</button><br />
						</div>
						@endforeach
					@endif
					<div id="attachments"></div>
					<span class="btn btn-success btnbrowse fileinput-button">
						<span>BROWSE</span>
						<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
					</span>		   
				</div>
				
		</div> <!-- end of form_container--> 

		<div class="clear_20"></div>
		<div class="form_container"><span class="legend-action">ACTION</span>
			@if($request['te_status'] == 'DISAPPROVED')
			<div class="textarea_messages_container">
				<div class="row">
					<label class="textarea_inside_label">MESSAGE:</label>
					<textarea name="te_comments" rows="3" class="form-control textarea_inside_width" readonly="readonly">@if( $request['te_comments'] != '')@foreach(json_decode($request['te_comments']) as $comment){{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10;@endforeach @endif</textarea>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="textarea_messages_container">
				<div class="row">
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea name="comment" rows="3" class="form-control textarea_inside_width"></textarea>
				</div>
			</div>
			@else
			<div class="textarea_messages_container">
				<div class="row">
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea name="comment" rows="3" class="form-control textarea_inside_width">@if( $request['te_comments'] != '')@foreach(json_decode($request['te_comments']) as $comment){{ $comment->message }}&#13;&#10;@endforeach @endif</textarea>
				</div>
			</div>
			@endif
			<div class="clear_20"></div>
			<div class="row">
				<div class="comment_container">
					<div class="comment_notes">
						<label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
					</div> 
					<div class="comment_button">
						<button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="comment_container">
					<div class="comment_notes">
						<label class="button_notes"><strong>SEND</strong> TO CHRD FOR ASSESSMENT</label>
					</div> 
					<div class="comment_button">
						<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
					</div>
				</div>
			</div>
		</div><!-- end of form_container --> 
		<div class="clear_20"></div>
		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog" data-rel="dialog">
		  <div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			  </div>
			  <div class="modal-body">
				<label class="labels">DEPARTMENT:</label><br/>
				<select class="form-control" id="sel_department_name" name="tep_dep_id" style="width: 558px;">
					<option value=""> - </option>
					@foreach($departments as $department)
					<option name="tep_dep_id" value='{{ $department->id }}'>{{ $department->dept_name }} ({{ $department->comp_code }})</option>
					@endforeach
				</select>
				
				<div class="clear_10"></div>
				
				<label class="labels">EMPLOYEE NAME:</label><br/>
				<select class="form-control" id="sel_employee_name" style="width: 558px;">
					<option value=""> - </option>
				</select>
				
				<div class="clear_10"></div>
				
				<input readonly="readonly" type="hidden" class="form-control" id="txt_employee_id" value="" name="" />
				<label class="labels">DESIGNATION:</label><br/>
				<input readonly="readonly" type="text" class="form-control" id="emp_desig" name="" value="" >
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" id="tep_add_action" data-dismiss="modal">SAVE</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
			  </div>
			</div>
		  </div>
		</div>
		
		<!--Delete Modal -->
			<div id="deleteModal" class="modal fade" role="dialog" data-rel="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>
						<div class="modal-body">
							<p>Are you sure you want to delete <strong><span id="del_participant_name"></span></strong>? </p>
							<p id="del"></p>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-danger" id="delete_tep" data-dismiss="modal">DELETE</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
						</div>
					</div>
				</div>
			</div>
		
	</form>

@stop
@section('js_ko')
<script type="text/javascript">
	var allowed_file_count = 10;
	var allowed_total_filesize = 20971520;

	var row_to_delete;
	var selected_row;
	$(function () {
		var e = {};
		var row_to_delete;
		
		$('.date_picker').datepicker({
			dateFormat : 'yy-mm-dd'
		});	

		
		$(".remove_file_attachment").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$('#btnAdd').live('click', function(){
			selected_row = "";
			$("#sel_department_name").val('');
			$("#sel_employee_name").val('');
			$("#txt_employee_id").val('');
			$("#emp_desig").val('');
			$(".btnEditDelete").prop('disabled',true);	
			$('.row-selected').css('background-color','rgba(0, 0, 0, 0)');
			$("#myModal").modal("show");
			
		});
		
		$('.row-selected').live('click',function() {
			selected_row =  this;
			$('.row-selected').css('background-color','rgba(0, 0, 0, 0)');
			$(this).css('background-color','#CCC');
			$(".btnEditDelete").prop('disabled',false);			
		});
		
		$("#btnAddAttachment").click(function(){
			if($("#attachments input:file").length < 10){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment btn btn-xs btn-danger btn_del'>DELETE</button></div>");
			}
		});
		
		$("#sel_department_name").change(function(){
			// $("#sel_employee_name").val('');
			$('#sel_employee_name')
			.find('option')
			.remove()
			.end()
			.append('<option value=""> - </option>')
			.val('');
			$("#txt_employee_id").val('');
			// $.getJSON("public/employees_by_department/"+$(this).val()).done(function(employees){
			$.getJSON("{{ url('employees_by_department') }}/"+$(this).val()).done(function(employees){
				$.each(employees, function(i, employee){
					console.log(employee)
					e[employee.id] = employee;
					$("#sel_employee_name").append("<option value='"+employee.id+"'>"+employee.name+"</option>");
				});
			});			
		});
		
		$("#sel_employee_name").change(function(){
			$("#txt_employee_id").val($(this).val());
			$("#emp_desig").val(e[$("#txt_employee_id").val()].designation);
			// alert(e[$(this).val()].name);
		});
		
		$("#tep_add_action").click(function(){
			console.log("clicked tep_add_action");
			tr_start = '<tr class="row-selected del" data-employee-id="'+$("#txt_employee_id").val()+'" data-department-id="'+$("#sel_department_name option:selected").val()+'" data-employee-name="'+e[$("#txt_employee_id").val()].name+'" data-department-name="'+$("#sel_department_name option:selected").text()+'" data-designation-name="'+e[$("#txt_employee_id").val()].designation+'">';
				td = '<td class="td_style td_height text-center">';
					td += e[$("#txt_employee_id").val()].name;
					td += '<input type="hidden" name="p_name[]" value="'+e[$("#txt_employee_id").val()].name+'">';
					td += '<input type="hidden" name="p_id[]" value="'+e[$("#txt_employee_id").val()].id+'">';
				td += '</td>';
				td += '<td class="td_style td_height text-center">';
					td += $("#sel_department_name option:selected").text();
					td += '<input type="hidden" name="p_department[]" value="'+$("#sel_department_name option:selected").text()+'">';
					td += '<input type="hidden" name="p_dep_id[]" value="'+$("#sel_department_name option:selected").val()+'">';
				td += '</td>';
				td += '<td class="td_style td_height text-center">';
					td += e[$("#txt_employee_id").val()].designation;
					td += '<input type="hidden" name="p_designation[]" value="'+e[$("#txt_employee_id").val()].designation+'">';
				td += '</td>';
			tr_end = '</tr>';
			if(selected_row){
				$(selected_row).data('employee-id', $("#txt_employee_id").val());
				$(selected_row).data('department-id', $("#sel_department_name option:selected").val());
				$(selected_row).data('employee-name', e[$("#txt_employee_id").val()].name);
				$(selected_row).data('department-name', $("#sel_department_name option:selected").text());
				$(selected_row).data('designation-name', e[$("#txt_employee_id").val()].designation);
				$(selected_row).html(td);
				selected_row = '';
				$('.row-selected').css('background-color','rgba(0, 0, 0, 0)');
				$(".btnEditDelete").prop("disabled",true);
			}
			else{
				$("#tep_participants").append(tr_start + td + tr_end);
			}
			$("#sel_department_name").val('');
			$("#sel_employee_name").val('');
			$("#txt_employee_id").val('');
			$("#emp_desig").val('');
			
			$('.btnEditDelete').show();
		});

		$('#btnEdit').live('click', function(){
			$("#myModal").modal("show");
			console.log(selected_row);
			pdid = $(selected_row).data('department-id');
			$('#sel_department_name > option[value="'+pdid+'"]').prop('selected', true);
			
			$('#sel_employee_name')
			.find('option')
			.remove()
			.end() 
			.append('<option value=""> - </option>')
			.val('');
			$("#txt_employee_id").val('');
			// $.getJSON("public/employees_by_department/"+pdid).done(function(employees){
			$.getJSON("{{ url('employees_by_department') }}/" +pdid).done(function(employees){
				$.each(employees, function(i, employee){
					e[employee.id] = employee;
					if(employee.id == $(selected_row).data('employee-id')){
						selected = "selected='selected'";
						$("#emp_desig").val(employee.designation);
					}
					else{
						selected = "";
					}
					$("#sel_employee_name").append("<option "+selected+" value='"+employee.id+"'>"+employee.name+"</option>");
					
				});
			});	
			$("#txt_employee_id").val($(selected_row).data('employee-id'));
		});
		
		$("#tep_participants").on('click', '#delete_tep', function () {
			$(this).closest('tr').remove();
		});
		
		$("#delete_tep").click(function () {
			$(selected_row).remove();
			selected_row = "";
			$('.row-selected').css('background-color','rgba(0, 0, 0, 0)');
			$("#sel_department_name").val('');
			$("#sel_employee_name").val('');
			$("#txt_employee_id").val('');
			$("#emp_desig").val('');
			$(".btnEditDelete").prop('disabled',true);	
			$('#deleteModal').modal('hide');
		});
		
		$('#btnDelete').live('click', function(){
			$("#del_participant_name").html($(selected_row).data('employee-name'));
			$("#deleteModal").modal("show");
		});
		
	});
</script>

@stop