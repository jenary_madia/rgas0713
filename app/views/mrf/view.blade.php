@extends('template/header')

@section('content')
    <div id="mrf_create" v-cloak>
        {{--FOR for message prompt--}}
        <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div class="alert alert-info msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <ul>
                <li v-for="error in submitStatus.errors">@{{ error }}</li>
            </ul>
        </div>
        {{--END for message prompt--}}
        <div class="form_container msr-form-container">
            @include('mrf/layouts/request-details')
            @include('mrf/layouts/other-details')
        </div><!-- end of form_container -->
        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    <div>
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea disabled rows="3" class="form-control pull-left" name="comment"> @foreach (json_decode($mrfDetails->comment) as $comment) {{ $comment->name .' '.$comment->datetime.' : '.$comment->message }} &#013;@endforeach</textarea>
                    </div>
                    <label class="labels pull-left">COMMENT:</label>
                    <textarea rows="3" class="form-control pull-left" name="comment" v-model="comment"></textarea>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    @if($method == "view")
                        @include('mrf.actions.view')
                    @else
                        @if($mrfDetails->company == 'RBC-CORP')
                            @if($mrfDetails->status == "FOR ENDORSEMENT")
                                @include('mrf.actions.for-endorsement')
                            @elseif($mrfDetails->status == "APPROVED")
                                @include('mrf.actions.chrd-dh')
                            @elseif($mrfDetails->status == "NOTED")
                                @include('mrf.actions.recruitment-staff')
                            @elseif($mrfDetails->status == "FOR PROCESSING")
                                @include('mrf.actions.chrd-recipient')
                            @elseif($mrfDetails->status == "FOR ASSESSMENT")
                                @if( ! $mrfDetails->csmdadhassessedby && ! $mrfDetails->bsaassessedby)
                                    @include('mrf.actions.csmd-adh')
                                @elseif($mrfDetails->csmdadhassessedby && $lastSig != 3)
                                    @include('mrf.actions.csmd-bsa')
                                @elseif(($mrfDetails->csmdadhassessedby || $mrfDetails->bsaassessedby )&& $lastSig == 3)
                                    @include('mrf.actions.csmd-dh')
                                @endif
                            @elseif($mrfDetails->status == "FOR APPROVAL")
                                @if($mrfDetails->svpapprovedby && $lastSig == 3)
                                    @include('mrf.actions.president')
                                @else
                                    @include('mrf.actions.division-head')
                                @endif
                            @elseif($mrfDetails->status == "FOR FURTHER ASSESSMENT")
                                @include('mrf.actions.for-further-assessment')
                            @endif

                        @else
                            @if($mrfDetails->status == "FOR ASSESSMENT")
                                @include('mrf.actions.ssmd')
                            @elseif($mrfDetails->status == "FOR APPROVAL")
                                @if($lastSig != 4)
                                    @include('mrf.actions.vpo')
                                @else
                                    @include('mrf.actions.president')
                                @endif
                            @elseif($mrfDetails->status == "APPROVED")
                                @include('mrf.actions.chrd-dh')
                            @elseif($mrfDetails->status == "NOTED")
                                @include('mrf.actions.recruitment-staff')
                            @endif
                        @endif
                    @endif
                </div>
            </div>
        </div><!-- end of form_container -->
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    <script>
        var mrfID = {{ $mrfID }};
    </script>
    @if($method == "view")
        {{ HTML::script('/assets/js/mrf/vue-form.js') }}
    @else
        {{ HTML::script('/assets/js/mrf/vue-process.js') }}
    @endif
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;

        var PDQfile_counter = 0;
        var PDQallowed_file_count = 5;
        var PDQallowed_total_filesize = 20971520;

        var OCfile_counter = 0;
        var OCallowed_file_count = 5;
        var OCallowed_total_filesize = 20971520;

        var CSMDfile_counter = 0;
        var CSMDallowed_file_count = 10;
        var CSMDallowed_total_filesize = 20971520;

        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
        $('body').on('click','.toggleDatePicker',function () {
            $(this).closest( "div" ).find(".date_picker").datepicker("show");
        });
    </script>
    {{ HTML::script('/assets/js/mrf/file-upload.js') }}
@stop
