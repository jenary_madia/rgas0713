<div class="container-header"><h5 class="text-center"><strong>{{ MRF_FORM_TITLE }}</strong></h5></div>
<div class="clear_20"></div>
<div class="clear_20"></div>
<div class="row employee-details">
    <div class="col-md-6">
        <label class="labels required pull-left">EMPLOYEE NAME:</label>
        <input value="{{ $mrfDetails->owner->firstname.' '.$mrfDetails->owner->middlename.' '.$mrfDetails->owner->lastname }}" readonly="readonly" type="text" class="form-control pull-right" name="employeeName"/>
    </div>
    <div class="col-md-6 pull-left">
        <label class="labels required">REFERENCE NUMBER:</label>
        <input readonly="readonly" type="text" value="{{ $mrfDetails->reference_no }}" class="form-control pull-right"/>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-6">
        <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
        <input  readonly="readonly" type="text" value="{{ $mrfDetails->owner->employeeid }}" class="form-control pull-right" name="employeeNumber"/>
    </div>
    <div class="col-md-6">
        <label class="labels required pull-left">DATE FILED:</label>
        <input readonly="readonly" type="text" value="{{ $mrfDetails->datefiled }}" class="form-control pull-right"/>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-6">
        <label class="labels required pull-left">COMPANY:</label>
        <input readonly="readonly" type="text" value="{{ $mrfDetails->company }}" class="form-control pull-right" name="company"/>
    </div>
    <div class="col-md-6">
        <label class="labels required pull-left">STATUS:</label>
        <input readonly="readonly" type="text" value="{{ $mrfDetails->status }}" class="form-control pull-right" name="status"/>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-6">
        <label class="labels required pull-left">DEPARTMENT:</label>
        <input readonly="readonly" type="text" value="{{ $mrfDetails->department->dept_name }}" class="form-control pull-right" name="department"/>
    </div>
    <div class="col-md-6">
        <label class="labels">CONTACT NUMBER:</label>
        <input disabled value="{{ $mrfDetails->contact_no }}" type="text" class="form-control pull-right" name="contactNumber" maxlength="20"/>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-6">
        <label class="labels pull-left">SECTION:</label>
        <input disabled value="{{ $mrfDetails->sectionid ? $mrfDetails->section->sect_name : $mrfDetails->other_section }}" type="text" class="form-control pull-right" name="section" maxlength="25" />
    </div>
</div>
<div class="clear_20"></div>
<div class="clear_20"></div>
<div class="container-header"><h5 class="text-center lined"><strong>REQUEST DETAILS</strong></h5></div>
<div class="clear_20"></div>
@if(Session::get("company") == "RBC-CORP")
    <div class="clear_20"></div>
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required">REQUEST FOR:</label>
        </div>
        <div class="col2_form_container">
            <label><input disabled type="radio" class="ib" {{ $mrfDetails->request_for == 1 ? 'checked' : ''}} name="requestFor"> EXISTING ORGANIZATION</label>
        </div>
    </div>
    <div class="row_form_container">
        <div class="col1_form_container">
            <label><input disabled type="radio" class="ib" {{ $mrfDetails->request_for == 2 ? 'checked' : ''}} name="requestFor"> NEW ORGANIZATION</label>
        </div>
        <div class="col2_form_container">
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">COMPANY NAME:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" disabled class="form-control ib" value="{{ $mrfDetails->company_name }}" >
            </div>
        </div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">DEPARTMENT NAME:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" disabled class="form-control ib" value="{{ $mrfDetails->department_name }}">
            </div>
        </div>
    </div>
    <div class="clear_10"></div>
@endif
<div class="row">
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required" id="lbl_pc">POSTITION TITLE:</label>
        </div>
        <div class="col2_form_container">
            <input type="text" disabled class="form-control ib" value="{{ $mrfDetails->position_jobtitle }}" name="positionTitle">
        </div>
    </div>
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required">DATE NEEDED:</label>
        </div>
        <div class="col2_form_container">
            <div class="input-group bootstrap-timepicker timepicker">
                <input type="text" disabled name="dateNeeded" value="{{ $mrfDetails->dateneeded }}" readonly class="form-control input-small">
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </div>
</div>
<div class="clear_10"></div>
<div class="row">
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required">NATURE OF REQUEST</label>
        </div>
        <div class="col2_form_container">
            <input type="text" disabled class="form-control ib" value="{{ $mrfDetails->requestCode ? $mrfDetails->requestCode->name : "" }}">
        </div>
    </div>
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required">SOURCE:</label>
        </div>
        <div class="col2_form_container">
            <table>
                <tr>
                    <td style="text-align: center;">
                        <div class="radio">
                            <label><input disabled type="checkbox" {{ $mrfDetails->internal ? 'checked' : '' }}><b>&nbsp;INTERNAL</b></label>
                        </div>
                    </td>
                    <td style="text-align: center;">
                        <div class="radio">
                            <label><input disabled type="checkbox" {{ $mrfDetails->external ? 'checked' : '' }} ><b>&nbsp;EXTERNAL</b></label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="clear_10"></div>
<div class="row">
    <div class="row_form_container">
        @if(Session::get("company") == "RBC-CORP")
            <div class="col1_form_container">
                <label class="labels required">REQUESTED MANPOWER COUNT:</label>
            </div>
            <div class="col2_form_container">
                <table>
                    <td>
                        <input type="text" disabled class="form-control ib" value="{{ $mrfDetails->manpower_count }}" style="width: 60px;">
                    </td>
                    <td></td>
                    <td>
                        <input {{ $mrfDetails->manpower_count != 0 ? 'type="hidden"' : '' }} disabled value="{{ $mrfDetails->manpower_count }}" style="width: 80px;" max="99" class="form-control ib" name="irequestedcount">
                    </td>
                </table>
            </div>
        @else
            <div class="col1_form_container">
                <label class="labels required"></label>
            </div>
            <div class="col2_form_container">
                <table>
                    
                </table>
            </div>
        @endif
    </div>
    <div class="row_form_container">
        <div class="col1_form_container">
            <label class="labels required" id="lbl_pc">PERIOD COVERED:</label>
        </div>
        <div class="col2_form_container">
            <input type="text" disabled class="form-control ib" name="period_covered" value="{{ $mrfDetails->periodcovered }}"/>
        </div>
    </div>
    <div class="clear_10"></div>
    <div class="clear_10"></div>
    <div class="col-md-12">
        <label class="labels pull-left"> NOTE: Please specify the required gender of requested employee/s in the reason column.</label>
    </div>
    <div class="col-md-12">
        <br>
        <label class="labels required pull-left">REASON &nbsp</label>
        <textarea disabled name="" id="" class="form-control" rows="7">{{ $mrfDetails->reason }}</textarea>
    </div>
</div>
<div class="clear_20"></div>