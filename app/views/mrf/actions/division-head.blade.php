<div class="row actions">
    <div class="col-md-7">
        <label class="button_notes"> <strong>APPROVE</strong></label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendForProcess">APPROVE</button>
    </div>
    <div class="clear_10"></div>
    <div class="col-md-7">
        <label class="button_notes"> <strong>DISAPPROVE</strong></label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="processFor('disapprove')">DISAPPROVE</button>
    </div>
    <div class="clear_10"></div>

    <div class="col-md-7">
        <label class="button_notes">
            <span class="pull-left"><strong>ASSIGN</strong> TO </span>
            <select name="" id="" v-model="deptHead" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                <option value=""></option>\
                <option value="{{ $csmdHead->id }}">{{ $csmdHead->firstname.' '.$csmdHead->middlename.' '.$csmdHead->lastname }}</option>
                <option value="{{ $chrdHead->employeeid }}">{{ $chrdHead->employee->firstname.' '.$chrdHead->employee->middlename.' '.$chrdHead->employee->lastname }}</option>
            </select>
            <span class="pull-left"> FOR FURTHER ASSESSMENT</span>
        </label>
    </div>
    <div class="col-md-5 text-right">
        <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="assignProcess">ASSIGN</button>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="row">
            <div class="col-md-12 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        </div>
    </div>
</div>