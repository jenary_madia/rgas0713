<div class="col-md-10 col-md-offset-1">
    <div class="row">
        <div class="col-md-4 text-center">
            <button value="cancel" name="action" class="btn btn-default btndefault" @click="saveRecruitment">SAVE</button>
        </div>
        @if($mrfDetails->company = "RBC-CORP")
            @if($mrfDetails->requestCode->name == "Temporary" && $mrfDetails->receivedby == null)
                <div class="col-md-4 text-center">
                    <button value="cancel" name="action" class="btn btn-default btndefault" @click.prevent="sendForProcess">SEND</button>
                </div>
            @endif
        @endif
        <div class="col-md-4 text-center">
            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
        </div>
    </div>
</div>