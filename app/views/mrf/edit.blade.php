@extends('template/header')

@section('content')
    <div id="mrf_create" v-cloak>
        {{--FOR for message prompt--}}
        <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div class="alert alert-info msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <ul>
                <li v-for="error in submitStatus.errors">@{{ error }}</li>
            </ul>
        </div>
        {{--END for message prompt--}}
        {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
        <div class="form_container msr-form-container">
            <div class="container-header">
                <span class="text-center module-header">
                    {{ MRF_FORM_TITLE }}
                </span>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.employeeName" class="form-control pull-right" name="employeeName" value="{{ $mrfDetails->owner->firstname.' '.$mrfDetails->owner->middlename.' '.$mrfDetails->owner->lastname }}" />
                </div>
                <div class="col-md-6 pull-left">
                    <label class="labels required">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.referenceNo" value="{{ $mrfDetails->reference_no }}" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                    <input  readonly="readonly" type="text" v-model="formDetails.employeeNumber" value="{{ $mrfDetails->employeeid }}" class="form-control pull-right" name="employeeNumber"/>
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DATE FILED:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.dateFiled" class="form-control pull-right" value="{{ $mrfDetails->datefiled }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">COMPANY:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.company" class="form-control pull-right" name="company" value="{{ $mrfDetails->company }}"/>
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">STATUS:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.status" class="form-control pull-right" name="status" value="{{ $mrfDetails->status }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DEPARTMENT:</label>
                    <input readonly="readonly" type="text" v-model="formDetails.department" class="form-control pull-right" name="department" value="{{ $mrfDetails->department->dept_name }}"/>
                </div>
                <div class="col-md-6">
                    <label class="labels">CONTACT NUMBER:</label>
                    <input v-model="formDetails.contactNumber" type="text" class="form-control pull-right" name="contactNumber" maxlength="20" value="{{ $mrfDetails->contact_no }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels pull-left">SECTION:</label>
                    <input v-model="formDetails.section" type="text" class="form-control pull-right" name="section" maxlength="25" value="{{ $mrfDetails->sectionid ? $mrfDetails->section->sect_name : $mrfDetails->other_section }}" />
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header">
                <span class="module-sub-header text-center lined">
                    <strong>REQUEST DETAILS</strong>
                </span>
            </div>
            <div class="clear_20"></div>
            @if(Session::get("company") == "RBC-CORP")
                <div class="clear_20"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REQUEST FOR:</label>
                    </div>
                    <div class="col2_form_container">
                        <label><input type="radio" class="ib" v-model="requestDetails.requestFor" {{ $mrfDetails->request_for == 1 ? 'checked' : ''}}  value="1" name="requestFor"> EXISTING ORGANIZATION</label>
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label><input type="radio" class="ib" v-model="requestDetails.requestFor" {{ $mrfDetails->request_for == 2 ? 'checked' : ''}} value="2" name="requestFor"> NEW ORGANIZATION</label>
                    </div>
                    <div class="col2_form_container">
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control ib" v-model="requestDetails.company" value="{{ $mrfDetails->company_name }}">
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control ib" name="department_name" v-model="requestDetails.department"  value="{{ $mrfDetails->department_name }}">
                        </div>
                    </div>
                </div>
                <div class="clear_10"></div>
            @endif
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required" id="lbl_pc">POSTITION TITLE:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control ib" v-model="requestDetails.positionTitle"  name="positionTitle" value="{{ $mrfDetails->position_jobtitle }}">
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE NEEDED:</label>
                    </div>
                    <div class="col2_form_container">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" name="dateNeeded" v-model="requestDetails.dateNeeded" readonly class="date_picker form-control input-small" value="{{ $mrfDetails->dateneeded }}">
                            <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">NATURE OF REQUEST</label>
                    </div>
                    <div class="col2_form_container">
                        {{ Form::select('size', $MRFRequests, $mrfDetails->requestCode ? $mrfDetails->requestCode->id : '',["v-if"=>"requestDetails.requestFor != 2" ,"v-model"=>"requestDetails.nature","class"=>"form-control","name"=>"request"]) }}
                        {{ Form::select('size', [1 => 'New Job Position',2 => 'Additional Staff',4 => 'Temporary'], $mrfDetails->requestCode ? $mrfDetails->requestCode->id : '',["v-else","v-model"=>"requestDetails.nature","class"=>"form-control","name"=>"request"]) }}
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">SOURCE:</label>
                    </div>
                    <div class="col2_form_container">
                        <table>
                            <tr>
                                <td style="text-align: center;">
                                    <div class="radio">
                                        <label><input type="checkbox" v-model="requestDetails.internal" value="1" {{ $mrfDetails->internal ? 'checked' : '' }}><b>&nbsp;INTERNAL</b></label>
                                    </div>
                                </td>
                                <td style="text-align: center;">
                                    <div class="radio">
                                        <label><input type="checkbox" v-model="requestDetails.external" value="1" {{ $mrfDetails->external ? 'checked' : '' }}><b>&nbsp;EXTERNAL</b></label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear_10"></div>
            <div class="row">
                <div class="row_form_container">
                    @if(Session::get("company") == "RBC-CORP")
                        <div class="col1_form_container">
                            <label class="labels required">REQUESTED MANPOWER COUNT:</label>
                        </div>
                        <div class="col2_form_container">
                            <table>
                                <td>
                                    <select class="form-control ib" v-model="requestDetails.count" style="width: 80px;">
                                        @for($i = 0 ; $i < 12 ; $i++)
                                            <option value="{{$i}}" {{ $i === $mrfDetails->manpower_count ? "selected" : ""}}>{{ $i == 11 ? "other" : $i }}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td></td>
                                <td>
                                    <input v-show="requestDetails.count == 11" v-model="requestDetails.otherCount" style="width: 80px;" type="number" max="99" class="form-control ib" name="irequestedcount" }} value="{{ $mrfDetails->manpower_count }}">
                                </td>
                            </table>
                        </div>
                    @else
                    <div class="col1_form_container">
                            <label class="labels required"></label>
                        </div>
                        <div class="col2_form_container">
                            <table>
                            </table>
                        </div>
                    @endif
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required" id="lbl_pc">PERIOD COVERED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control ib" name="period_covered" v-model="requestDetails.periodCovered" value="{{ $mrfDetails->periodcovered }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="clear_10"></div>
                <div class="col-md-12">
                    <label class="labels pull-left"> NOTE: Please specify the required gender of requested employee/s in the reason column.</label>
                </div>
                <div class="col-md-12">
                    <br>
                    <label class="labels required pull-left">REASON &nbsp</label>
                    <textarea name="" v-model="requestDetails.reason" id="" class="form-control" rows="7">{{ $mrfDetails->periodcovered }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        {{--MRAF--}}
                        <label class="attachment_note"><strong>PLEASE ATTACH THE FOLLOWING</strong></label><br/>
                        <label style="margin-right : 50px;">MRAF</label> <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        <div id="attachments">
                            @if($mrfDetails->mraffile)
                                @foreach(json_decode($mrfDetails->mraffile) as $key)
                                    <p>
                                        {{ $key->original_filename.' | '.$key->filesize  }}
                                        <input type='hidden' class='attachmentData' value='{{ json_encode($key) }}'>
                                        <button class="btn btn-xs btn-danger remove-fn confirm-delete">DELETE</button>
                                    </p>
                                @endforeach
                            @endif
                        </div>
                        <div class="clear_10"></div>
                        {{--PDQ--}}
                        <label style="margin-right : 50px;">PDQ</label> <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="PDQfileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        <div id="PDQattachments">
                            @if($mrfDetails->pdq)
                                @foreach(json_decode($mrfDetails->pdq) as $key)
                                    <p>
                                        {{ $key->original_filename.' | '.$key->filesize  }}
                                        <input type='hidden' class='PDQattachmentData' value='{{ json_encode($key) }}'>
                                        <button class="btn btn-xs btn-danger remove-fn confirm-delete">DELETE</button>
                                    </p>
                                @endforeach
                            @endif
                        </div>
                        <div class="clear_10"></div>
                        {{--OC--}}
                        <label style="margin-right : 50px;">ORGANIZATIONAL CHART</label> <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="OCfileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        <div id="OCattachments">
                            @if($mrfDetails->org_chart)
                                @foreach(json_decode($mrfDetails->org_chart) as $key)
                                    <p>
                                        {{ $key->original_filename.' | '.$key->filesize  }}
                                        <input type='hidden' class='OCattachmentData' value='{{ json_encode($key)}}'>
                                        <button class="btn btn-xs btn-danger remove-fn confirm-delete">DELETE</button>
                                    </p>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- end of form_container -->
        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    <div>
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea disabled rows="3" class="form-control pull-left" name="comment"> @foreach (json_decode($mrfDetails->comment) as $comment) {{ $comment->name .' '.$comment->datetime.' : '.$comment->message }} &#013;@endforeach</textarea>
                    </div>
                    <label class="labels pull-left">COMMENT:</label>
                    <textarea rows="3" class="form-control pull-left" name="comment" v-model="comment">{{ json_decode($mrfDetails->comment)[0]->message }}</textarea>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row actions">
                        <div class="col-md-6">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="createMRF('save')">SAVE</button>
                        </div>
                    </div>
                    <div class="row actions">
                        <div class="col-md-8">
                            @if(in_array(Session::get("desig_level"),['employee','supervisor']))
                                <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR ENDORSEMENT</label>
                            @else
                                <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
                            @endif
                        </div>
                        <div class="col-md-4 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="createMRF('send')">SEND</button>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    <script>
        var mrfID = {{ $mrfID }};
    </script>
    {{ HTML::script('/assets/js/mrf/vue-form.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
        var file_counter = 0;
        var allowed_file_count = 5;
        var allowed_total_filesize = 20971520;

        var PDQfile_counter = 0;
        var PDQallowed_file_count = 5;
        var PDQallowed_total_filesize = 20971520;

        var OCfile_counter = 0;
        var OCallowed_file_count = 5;
        var OCallowed_total_filesize = 20971520;


        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
        $('body').on('click','.toggleDatePicker',function () {
            $(this).closest( "div" ).find(".date_picker").datepicker("show");
        });
    </script>
    {{ HTML::script('/assets/js/mrf/file-upload.js') }}
@stop
