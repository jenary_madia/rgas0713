@extends('template/header')

@section('content')
    <div id="wrap">
        @if($type != "submitted")
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">MY MRF</span>
                    {{ Form::open(array('url' => 'mrf/delete-mrf', 'method' => 'post', 'files' => true,'id' => 'formMyMRF')) }}
                    <table id="myMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                    {{ Form::close() }}
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">FOR ENDORSEMENT</span>
                    <table id="forEndorsementMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">FOR PROCESSING</span>
                    <table id="forProcessingMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forProcessingMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        @else
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">APPROVED</span>
                    <table id="approvedMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="approvedMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">NOTED</span>
                    <table id="notedMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="notedMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>  
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">FOR ASSESSMENT</span>
                    <table id="forAssessmentMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forAssessmentMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">FOR APPROVAL</span>
                    <table id="forApprovalMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApprovalMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="wrapper">
                <div class="datatable_holder">
                    <span class="list-title">FOR FURTHER ASSESSMENT</span>
                    <table id="forFurtherMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forFurtherMRF">
                        <thead>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        @endif
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/mrf/mrf-datatables.js') }}
@stop
