@extends('template/header')

@section('content')

    <div id="wrap">
        <div class="wrapper">
            <div class="clear_20"></div>
            <hr/>
            <h3>Waiting for your Approval</h3>
			<!--Added by jenary 220317 -->
			<div class="clear_20"></div>
            <div class="homeborder">
                <div class="wrapper">
                    <table id="forEndorsementMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forEndorsementMRF">
                        <thead>
                        <tr role="row">
                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR ENDORSEMENT MRF</th>
                        </tr>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="wrapper">
                    <table id="forProcessingMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forProcessingMRF">
                        <thead>
                        <tr role="row">
                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR PROCESSING MRF</th>
                        </tr>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- Ended by jenary 220317-->
                <!-- Updated by Lucille 12192016-->
                @if(Session::get('is_cbr_staff'))
                    <table id="requests_dashboard_staff_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_dashboard_staff_list">
                        <thead>
                        <tr role="row">
                            <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">CBR REQUESTS</th>
                        </tr>
                        <tr role="row">
                            <th align="center">REFERENCE NO.</th>
                            <th align="center">FILER</th>
                            <th align="center">DATE FILED</th>
                            <th align="center">STATUS</th>
                            <th align="center">ACTION</th>
                        </tr>
                        </thead>
                    </table>
                @endif

                @if(Session::get('designation') == 'VP FOR CORP. HUMAN RESOURCE' || Session::get('designation') == 'SVP FOR CORP. SERVICES' || Session::get('designation') == 'PRESIDENT')
                    <hr/>
                    <table id="approver_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="approver_dashboard">
                        <thead>
                        <tr role="row">
                            <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">TRAINING ENDORSEMENTS</th>
                        </tr>
                        <tr role="row">
                            <th align="center">REFERENCE NO.</th>
                            <th align="center">FILER</th>
                            <th align="center">DATE FILED</th>
                            <th align="center">STATUS</th>
                            <th align="center">ACTION</th>
                        </tr>
                        </thead>
                    </table>
                @endif

                <div class="clear_10"></div>
                <table id="sv_approval_dashboard" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                    <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">SERVICE VEHICLE</th></tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200">REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="150">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="250">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="170">ACTION</th>
                    </tr>
                    </thead>
                </table>

                @if(in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
                    @if(Session::get('desig_level') != 'employee')
                        <div class="clear_20"></div>
                        <table id="pmarf_approver" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="pmarf_approver">
                            <thead>
                            <tr role="row">
                                <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">Promo and Merchandising Activity Request</th>
                            </tr>
                            <tr role="row">
                                <th align="center">REFERENCE NO.</th>
                                <th align="center">DATE FILED</th>
                                <th align="center">FILER</th>
                                <th align="center">TYPE OF ACTIVITY</th>
                                <th align="center">STATUS</th>
                                <th align="center">ACTION</th>
                            </tr>
                            </thead>
                        </table>
                    @endif
                @endif



                @if(Session::get('is_pmarf_npmd_head'))
                    <div class="clear_20"></div>
                        <table id="npmd_head_requests_limit" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="npmd_head_requests">
                            <thead>
                                <tr role="row">
                                    <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">Promo and Merchandising Activity Request</th>
                                </tr>
                                <tr role="row">
                                    <th align="center">REFERENCE NO.</th>
                                    <th align="center">DATE FILED</th>
                                    <th align="center">TYPE OF ACTIVITY</th>
                                    <th align="center">STATUS</th>
                                    <th align="center">REQUESTOR</th>
                                    <th align="center">DEPARTMENT</th>
                                    <th align="center">ACTION</th>
                                </tr>
                            </thead>
                        </table>
                    @endif

                    @if(Session::get('is_pmarf_npmd_personnel'))
                    <div class="clear_20"></div>
                        <table id="npmd_personnel_requests_limit" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="npmd_personnel_requests">
                            <thead>
                                <tr role="row">
                                    <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">Promo and Merchandising Activity Request</th>
                                </tr>
                                <tr role="row">
                                    <th align="center">REFERENCE NO.</th>
                                    <th align="center">DATE FILED</th>
                                    <th align="center">TYPE OF ACTIVITY</th>
                                    <th align="center">STATUS</th>
                                    <th align="center">REQUESTOR</th>
                                    <th align="center">DEPARTMENT</th>
                                    <th align="center">ACTION</th>
                                </tr>
                            </thead>
                        </table>

                    @endif


                {{--MOC--}}
                <table id="waitingForApprovalMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="waitingForApprovalMOC">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">MOC</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                @if(Session::get('is_div_head') || in_array(Session::get('desig_level'),['admin-aom','plant-aom']))
                    <div class="clear_20"></div>
                    <div class="wrapper">
                        <table id="forApprovalMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApprovalMOC">
                            <thead>
                            <tr role="row">
                                <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL</th>
                            </tr>
                            <tr role="row">
                                <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                                <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                @endif
                {{--end MOC--}}

                <div class="clear_20"></div>
                <table id="requests_dashboard_filer_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_dashboard_filer_list">
                    <thead>
                    <tr role="row">
                        <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="4" aria-label="Company">MY CBR REQUESTS</th>
                    </tr>
                    <tr role="row">
                        <th align="center">REFERENCE NO.</th>
                        <th align="center">FILER</th>
                        <th align="center">STATUS</th>
                        <th align="center">DATE FILED</th>
                        <th align="center">ACTION</th>
                    </tr>
                    </thead>
                </table>
                <div class="clear_20"></div>

                @if(Session::get('desig_level') == 'head')
                <div class="clear_20"></div>
                <table id="filer_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="filer_dashboard">
                    <thead>
                    <tr role="row">
                        <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">TRAINING ENDORSEMENT REQUESTS</th>
                    </tr>
                    <tr role="row">
                        <th align="center">REFERENCE NO.</th>
                        <th align="center">DATE FILED</th>
                        <th align="center">STATUS</th>
                        <th align="center">CURRENT</th>
                        <th align="center">ACTION</th>
                    </tr>
                    </thead>
                </table>
                @endif

                        <!-- End of Updated by Lucille 12192016-->
                <div class="clear_20"></div>

                @if(in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
                    <div class="clear_20"></div>
                    <table id="pmarf_filer" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="pmarf_filer">
                        <thead>
                        <tr role="row">
                            <th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">PMARF REQUESTS</th>
                        </tr>
                        <tr role="row">
                            <th align="center">REFERENCE NO.</th>
                            <th align="center">DATE FILED</th>
                            <th align="center">STATUS</th>
                            <th align="center">CURRENT</th>
                            <th align="center">ACTIVITY TYPE</th>
                            <th align="center">ACTION</th>
                        </tr>
                        </thead>
                    </table>
                @endif

                <div class="clear_20"></div>

                <!-- added by jenary -->
                <table id="forApproveLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="forApproveLeaves">
                    <thead>
                    <tr role="row">
                        <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="8" aria-label="Company">For Approval Leave Request</th>
                    </tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                <div class="clear_20"></div>
                {{ Form::open(array('url' => 'ns/process', 'method' => 'post', 'files' => true,'id' => 'formApproveNotif')) }}
                <table id="superiorNotification" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorNotification">
                    <thead>
                    <tr role="row">
                        <th class="text-left" role="columnheader" rowspan="1" colspan="8">For Approval Notification Request</th>
                    </tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Notification</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Notification</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                {{ Form::close() }}
                <div class="clear_20"></div>
                @if(! in_array(Session::get("company"),json_decode(CORPORATE,true)))
                    <table id="superiorPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorPELeaves">
                        <thead>
                        <tr role="row">
                            <th class="" role="columnheader" rowspan="1" colspan="8">Production Encoders For Approval Leave Requests</th>
                        </tr>
                        <tr role="row">
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                    <div class="clear_20"></div>
                @endif
                @if(! in_array(Session::get("company"),json_decode(CORPORATE,true)))
                <div class="clear_20"></div>
                <table id="superiorPENotification" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorPENotification">
                    <thead>
                    <tr role="row">
                        <th class="" role="columnheader" rowspan="1" colspan="7">Production Encoders For Approval Notification Request</th>
                    </tr>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Department</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                @endif
                        <!-- end by jenary -->
                <!-- added by jenary 19/12/16-->
                @if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)))
                    <table id="superiorMSR" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorMSR">
                        <thead>
                        <tr role="row">
                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">FOR APPROVAL MSR</th>
                        </tr>
                        <tr role="row">
                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason of Request</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                @elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
                <table id="superiorMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorMSR-SMIS">
                    <thead>
                    <tr role="row">
                        <th class="" style="text-align : left" role="columnheader" rowspan="1">FOR APPROVAL</th>
                    </tr>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
                @endif
                        <!-- end by jenary 19/12/16-->
                <div class="clear_10"></div>
                <table id="gd_approval_dashboard" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                    <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">DOCUMENTS</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DOCUMENT NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200px" >FROM</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COMPLETED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>
                    </thead>
                </table>

                @if(Session::get("desig_level") == "head"  || Session::get("dept_id") == 17 )
                    <div class="clear_10"></div>
                    <table id="art_approval_dashboard" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                        <thead>
                        <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">DESIGN</th></tr>
                        <tr role="row">
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200">REFERENCE NUMBER</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="230">FROM</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="170">ACTION</th>
                        </tr>
                        </thead>
                    </table>
                    @endif
                <div class="clear_20"></div>

                    <table id="pmf_approval_dashboard" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                        <thead>
                            <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="6">PMF</th></tr>
                            <tr role="row">
                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                                <th width="150" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                                <th width="200" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                            </tr>
                        </thead>
                    </table>
                <div class="clear_20"></div>

                @if(Session::get("desig_level") == "head" || Session::get("dept_id") == 17  )
                    <div class="clear_10"></div>
                    <table id="art_approval_dashboard" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                        <thead>
                        <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">DESIGN</th></tr>
                        <tr role="row">
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200">REFERENCE NUMBER</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="230">FROM</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="170">ACTION</th>
                        </tr>
                        </thead>
                    </table>
                @endif
                <div class="clear_10"></div>
                <form method="POST" action="{{$form_action_nte_employee}}" id="{{$form_id_nte_employee}}">
                    <input type="hidden" id="ref_num_employee" name="ref_num">
                    <input type="hidden" id="bool_employee" name="bool">
                </form>
                <table id='nte_list_employee' cellpadding='0' cellspacing='0' border='0' class='display' width='100%' >
                    <thead>
                        <tr role="row">
                            <th style="text-align:left;" colspan="9"><strong>NOTICE TO EXPLAIN of Concerned Employee</strong></th>
                        </tr>
                        <tr role="row">
                            <th align='center'>REFERENCE NUMBER</th>
                            <th align='center'>INFRACTION</th>
                            <th align='center'>DATE COMMITTED</th>
                            <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                            <th align='center'>STATUS</th>
                            <th align='center'>ACTION</th>
                        </tr>
                    </thead>
                </table>
                @if(Session::get("desig_level") == "head")
                    <div class="clear_10"></div>
                    <table id='nte_list_department' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                        <thead>
                            <tr role="row">
                                <th style="text-align:left;" colspan="9"><strong>NOTICE TO EXPLAIN</strong></th>
                            </tr>
                            <tr role="row">
                                <th align='center'>REFERENCE NUMBER</th>
                                <th align='center'>EMPLOYEE NAME</th>
                                <th align='center'>INFRACTION</th>
                                <th align='center'>DATE COMMITTED</th>
                                <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                                <th align='center'>STATUS</th>
                                <th align='center'>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                    <form method="POST" action="{{$form_action_nte_department}}" id="{{$form_id_nte_department}}">
                        <input type="hidden" id="ref_num_department" name="ref_num">
                        <input type="hidden" id="bool_department" name="bool">
                    </form>
                @endif
                @if(Session::get('is_chrd_aer_staff') > 1 && Session::get('is_chrd_aer_staff') < 6)
                    <table id='nte_list_approval' cellpadding='0' cellspacing='0' border='0' class='display' width='100%' >
                        <thead>
                            <tr role="row">
                                <th style="text-align:left;" colspan="9"><strong>NOTICE TO EXPLAIN</strong></th>
                            </tr>
                            <tr role="row">
                                <th align='center'>DEPARTMENT</th>
                                <th align='center'>NUMBER OF NTE</th>
                                <th align='center'>MONTH</th>
                                <th align='center'>YEAR</th>
                                <th align='center'>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                    <form method="POST" action="{{$form_action_approval}}" id="{{$form_id_approval}}">
                        <input type="hidden" id="ref_num_approval" name="ref_num">
                        <input type="hidden" id="ref_num_approval2" name="ref_num2">
                        <input type="hidden" id="ref_num_approval3" name="ref_num3">
                        <input type="hidden" id="bool_approval" name="bool">
                    </form>
                @endif
                @if(Session::get('is_chrd_aer_staff') == 3)
                    <div class="clear_20"></div>
                    <form method="POST" action="{{$form_action_manager}}" id="{{$form_id_manager}}">
                        <input type="hidden" id="ref_num_approval_m" name="ref_num">
                        <input type="hidden" id="bool_approval_m" name="bool">
                    </form>
                    <table id='nda_list_approval_manager' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                        <thead>
                            <div class="clear_10"></div>
                            <tr role="row">
                                <th style="text-align:left;" colspan="9"><strong>Notice of Disciplinary Action of CHRD-Manager</strong></th>
                            </tr>
                            <tr>
                                <th align='center'>REFERENCE NUMBER</th>
                                <th align='center'>EMPLOYEE NAME</th>
                                <th align='center'>DEPARTMENT</th>
                                <th align='center'>DATE COMMITTED</th>
                                <th align='center'>INFRACTION</th>
                                <th align='center'>DISCIPLINARY ACTION</th>
                                <th align='center'>STATUS</th>
                                <th align='center'>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><hr>
                @endif
                @if(Session::get('is_chrd_aer_staff') == 5)
                    <div class="clear_20"></div>
                    <form method="POST" action="{{$form_action_head}}" id="{{$form_id_head}}">
                        <input type="hidden" id="ref_num_approval_h" name="ref_num">
                        <input type="hidden" id="bool_approval_h" name="bool">
                    </form>
                    <table id='nda_list_approval_head' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                        <thead>
                            <div class="clear_10"></div>
                            <tr role="row">
                                <th style="text-align:left;" colspan="9"><strong>Notice of Disciplinary Action of CHRD Head</strong></th>
                            </tr>
                            <tr>
                                <th align='center'>REFERENCE NUMBER</th>
                                <th align='center'>EMPLOYEE NAME</th>
                                <th align='center'>DEPARTMENT</th>
                                <th align='center'>DATE COMMITTED</th>
                                <th align='center'>INFRACTION</th>
                                <th align='center'>DISCIPLINARY ACTION</th>
                                <th align='center'>STATUS</th>
                                <th align='center'>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                    <br><br><hr>
                @endif
                @if(Session::get('desig_level') == 'head')
                    <div class="clear_20"></div>
                    <div class="clear_20"></div>
                    <form method="POST" action="{{$form_action_nda_department}}" id="{{$form_id_nda_department}}">
                        <input type="hidden" id="ref_num_department" name="ref_num">
                        <input type="hidden" id="bool_department" name="bool">
                    </form>
                    <table id='nda_list_department' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                        <thead>
                            <div class="clear_10"></div>
                            <tr role="row">
                                <th style="text-align:left;" colspan="9"><strong>Notice of Disciplinary Action of Employee&apos;s Concerned Department Head</strong></th>
                            </tr>
                            <tr>
                                <th align='center'>REFERENCE NUMBER</th>
                                <th align='center'>EMPLOYEE NAME</th>
                                <th align='center'>DATE COMMITTED</th>
                                <th align='center'>INFRACTION</th>
                                <th align='center'>STATUS</th>
                                <th align='center'>ACTION</th>
                            </tr>
                        </thead>
                    </table>
                @endif
                <div class="clear_20"></div>
                <form method="POST" action="{{$form_action_nda_employee}}" id="{{$form_id_nda_employee}}">
                    <input type="hidden" id="ref_num_employee" name="ref_num">
                    <input type="hidden" id="bool_employee" name="bool">
                </form>
                <table id='nda_list_employee' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                    <thead>
                        <div class="clear_10"></div>
                        <tr role="row">
                            <th style="text-align:left;" colspan="9"><strong>Notice of Disciplinary Action of Concerned Employee</strong></th>
                        </tr>
                        <tr>
                            <th align='center'>REFERENCE NUMBER</th>
                            <th align='center'>DATE COMMITTED</th>
                            <th align='center'>INFRACTION</th>
                            <th align='center'>STATUS</th>
                            <th align='center'>ACTION</th>
                        </tr>
                    </thead>
                </table>

                 <div class="clear_10"></div>
                            <table id="cc_assesment_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" >
                                    <thead>
                                    <tr role="row">
                                        <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="4">CLEARANCE ASSESMENT</th>
                                    </tr>
                                    <tr role="row">
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER &nbsp;&nbsp;</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED &nbsp;&nbsp;&nbsp;</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RESIGNEE &nbsp;&nbsp;&nbsp;</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT &nbsp;&nbsp;&nbsp;</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EFFECTIVE DATE OF RESIGNATION</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS &nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">AGE DAYS &nbsp;&nbsp;&nbsp;</th>
                                        <th width="200"  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >ACTION</th>
                                    </tr>
                                    </thead>
                                </table>
            </div>

                        <h3>Pending</h3>

            <div class="homeborder">
                            <div class="clear_10"></div>
                            <table id="my_design" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                                <thead>
                                <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="6">DESIGN</th></tr>
                                <tr role="row">
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED &nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE &nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200" >CURRENT</th>                        
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="250">ACTION</th>
                                </tr>  
                                </thead>
                            </table>
							
							<!-- added by jenary -->
							<div class="clear_20"></div>
							<div class="wrapper">
								{{ Form::open(array('url' => 'mrf/delete-mrf', 'method' => 'post', 'files' => true,'id' => 'formMyMRF')) }}
								<table id="myMRF" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMRF">
									<thead>
									<tr role="row">
										<th class="" style="text-align : center" role="columnheader" rowspan="1" colspan="5">My MRF</th>
									</tr>
									<tr role="row">
										<th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
										<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
										<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Job Title/Position</th>
										<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
										<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
										<th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
									</tr>
									</thead>
								</table>
								{{ Form::close() }}
							</div>
                            {{ Form::open(array('url' => 'lrf/processing', 'method' => 'post', 'files' => true,'id' => 'formMyleaves')) }}
                            <table id="myLeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myLeaves">
                                <thead>
                                <tr role="row">
                                    <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="8" aria-label="Company">Leaves</th>
                                </tr>
                                <tr role="row">
                                    <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Leave</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Leave</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                </tr>
                                </thead>
                            </table>
                            {{ Form::close() }}
                            <div class="clear_20"></div>
                            {{ Form::open(array('url' => 'ns/process', 'method' => 'post', 'files' => true,'id' => 'formMyNotifications')) }}
                            <table id="myNotifications" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myNotifications">
                                <thead>
                                <tr role="row">
                                    <th class="text-left" role="columnheader" rowspan="1" colspan="8">My Notification</th>
                                </tr>
                                <tr role="row">
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Notification</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Notification</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                </tr>
                                </thead>
                            </table>
                            {{ Form::close() }}
                            <div class="clear_20"></div>
                            {{ Form::open(array('url' => 'lrf/pe_action', 'method' => 'post', 'files' => true,'id' => 'formMyPEleaves')) }}
                                @if(in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
                                    <table id="myPELeaves" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myPELeaves">
                                        <thead>
                                        <tr role="row">
                                            <th class="" role="columnheader" rowspan="1" colspan="8">Production Encoders Leave Requests</th>
                                        </tr>
                                        <tr role="row">
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <div class="clear_20"></div>
                                @endif
                            {{ Form::close() }}
                        @if(in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
                                {{ Form::open(array('url' => 'ns/pe-process', 'method' => 'post', 'files' => true,'id' => 'formPEMyNotifications')) }}
                                <div class="clear_20"></div>
                                <table id="myPENotifications" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myPENotifications">
                                    <thead>
                                    <tr role="row">
                                        <th class="" role="columnheader" rowspan="1" colspan="8">Production Encoder Notification Requests</th>
                                    </tr>
                                    <tr role="row">
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                    </tr>
                                    </thead>
                                </table>
                                {{ Form::close() }}
                                @endif
                                        <!-- end by jenary -->

                                <!-- added by jenary 19/12/16-->
                                @if( in_array(Session::get("dept_code"),json_decode(MSR_NPMD_DEPARTMENTS,true)))
                                    {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMSR')) }}
                                    <table id="myMSR" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMSR">
                                        <thead>
                                        <tr role="row">
                                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">MSR</th>
                                        </tr>
                                        <tr role="row">
                                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason of Request</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    {{ Form::close() }}
                                @elseif(in_array(Session::get("dept_code"),json_decode(MSR_SMIS_DEPARTMENTS,true)))
                                    {{ Form::open(array('url' => 'msr/process', 'method' => 'post', 'files' => true,'id' => 'formMyMSR')) }}
                                    <table id="myMSR-SMIS" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMSR-SMIS">
                                        <thead>
                                        <tr role="row">
                                            <th class="" style="text-align : left" role="columnheader" rowspan="1">MSR</th>
                                        </tr>
                                        <tr role="row">
                                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Project Type/Name</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    {{ Form::close() }}
                                    @endif
                                            <!-- added by jenary 19/12/16-->

                                   

                                    <div class="clear_10"></div>
                                    <table id="cc_resignee_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" >
                                        <thead>
                                        <tr role="row">
                                            <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="4">CLEARANCE CERTIFICATION</th>
                                        </tr>
                                        <tr role="row">
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RESIGNEE</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EFFECTIVE DATE OF RESIGNATION</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FILER &nbsp;&nbsp;&nbsp;</th>
                                            <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >ACTION</th>
                                        </tr>
                                        </thead>
                                        </thead>
                                    </table>


                                 


                                    @if(Session::get('is_cc_cbr_filer'))
                                        <div class="clear_10"></div>
                                        <table id="cc_filer_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" >
                                            <thead>
                                            <tr role="row">
                                                <th align="" width="" class="text-left" role="columnheader" rowspan="1" colspan="5">CLEARANCE CERTIFICATION</th>
                                            </tr>
                                            <tr role="row">
                                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RESIGNEE</th>
                                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EFFECTIVE DATE OF RESIGNATION</th>
                                                <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS &nbsp;&nbsp;&nbsp;</th>
                                                <th width="200"  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >ACTION</th>
                                            </tr>
                                            </thead>
                                            </thead>
                                        </table>
                                    @endif

                                    <table id="myMOC" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myMOC">
                                        <thead>
                                        <tr role="row">
                                            <th class="" style="text-align : left" role="columnheader" rowspan="1" colspan="5">PSR/MOC</th>
                                        </tr>
                                        <tr role="row">
                                            <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                        </tr>
                                        </thead>
                                    </table>

                                @if(Session::get('is_chrd_aer_staff') > 0 && Session::get('is_chrd_aer_staff') < 6)
                                    <table id='nte_list_request' cellpadding='0' cellspacing='0' border='0' class='display dataTable' width='100%'>
                                        <thead>
                                            <tr role="row">
                                                <th style="text-align:left;" colspan="9"><strong>NOTICE TO EXPLAIN</strong></th>
                                            </tr>
                                            <tr role="row">
                                                <th align='center'>REFERENCE NUMBER</th>
                                                <th align='center'>EMPLOYEE NAME</th>
                                                <th align='center'>DEPARTMENT</th>
                                                <th align='center'>DATE COMMITTED</th>
                                                <th align='center'>INFRACTION</th>
                                                <th align='center'>RECOMMENDED DISCIPLINARY ACTION</th>
                                                <th align='center'>CURRENT</th>
                                                <th align='center'>STATUS</th>
                                                <th align='center'>ACTION</th>
                                            </tr>   
                                        </thead>
                                    </table>
                                    <form method="POST" action="{{$form_action_nte_request}}" id="{{$form_id_nte_request}}">
                                        <input type="hidden" id="ref_num_request" name="ref_num">
                                        <input type="hidden" id="bool_request" name="bool">
                                    </form>
                                    <form method="POST" action="{{$form_action_returned}}" id="{{$form_id_returned}}">
                                        <input type="hidden" id="ref_num_returned" name="ref_num">
                                        <input type="hidden" id="bool_returned" name="bool">
                                    </form>
                                    <div class="clear_20"></div>
                                    <table id='nda_list_request' cellpadding='0' cellspacing='0' border='0' class='display' width='100%'>
                                        <form method="POST" action="{{$form_action_nda_request}}" id="{{$form_id_nda_request}}">
                                            <input type="hidden" id="ref_num_request" name="ref_num">
                                            <input type="hidden" id="bool_request" name="bool">
                                        </form>
                                        <thead>
                                            <tr role="row">
                                                <th style="text-align:left;" colspan="9"><strong>Notice of Disciplinary Action of CHRD</strong></th>
                                            </tr>
                                            <tr>
                                                <th align='center'>REFERENCE NUMBER</th>
                                                <th align='center'>EMPLOYEE NAME</th>
                                                <th align='center'>DEPARTMENT</th>
                                                <th align='center'>DATE COMMITTED</th>
                                                <th align='center'>INFRACTION</th>
                                                <th align='center'>DISCIPLINARY ACTION</th>
                                                <th align='center'>CURRENT</th>
                                                <th align='center'>STATUS</th>
                                                <th align='center'>ACTION</th>
                                            </tr>   
                                        </thead>        
                                    </table>
                                @endif
            </div>

                                    <div class="clear_60"></div>
        </div>
    </div>
    <form method="POST" action="{{$form_action_nte_view}}" id="{{$form_id_nte_view}}">
        <input type="hidden" id="ref_num_view" name="ref_num">
        <input type="hidden" id="bool_view" name="bool">
    </form>
    <form method="POST" action="{{$form_action_nda_view}}" id="{{$form_id_nda_view}}">
        <input type="hidden" id="ref_num_view" name="ref_num">
        <input type="hidden" id="bool_view" name="bool">
    </form>
@stop
@section('js_ko')
    <script type="text/javascript">
        var module = 'home';
    </script>
    {{ HTML::script('/assets/js/leave_notif.js') }}
    {{ HTML::script('/assets/js/cc/home.js?v2') }}
    {{ HTML::script('/assets/js/msr/home-datatables.js') }}
    {{ HTML::script('/assets/js/moc/home-datatables.js') }}
	{{--{{ HTML::script('/assets/js/mrf/home-datatables.js') }}--}}
    {{--{{ HTML::script('/assets/js/nte/nte_lists.js') }}--}}
@stop

