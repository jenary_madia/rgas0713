<div v-if="notificationType == 'cut_time'" class="text-uppercase">
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>CUT-TIME DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">TOTAL NUMBER OF DAYS:</label>
	        </div>
	        <div class="col2_form_container">
				<input type="number" class="form-control" v-model="cutTime.totalDays" min="0">
	        </div>
	    </div>
	    <div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">TOTAL NUMBER OF HOURS:</label>
	        </div>
	        <div class="col2_form_container">
				<input type="number" class="form-control" v-model="cutTime.totalHours" min="0">
	        </div>
	    </div>

		<div class="clear_10"></div>
		<div class="row">
		  <div class="col-md-4 from bordered">
		    <label class="labels">FROM:</label>
		    <br>
		    <label class="labels required">DATE:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.dateFrom" class="date_picker form-control input-small">
			    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		    <div class="clear_10"></div>
		    <label class="labels required">TIME IN/OUT:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.fromTimeInOut" class="time_picker form-control input-small">
			    <span class="input-group-addon "><i class="glyphicon glyphicon-time"></i></span>
			</div>
		  </div>
		  <div class="col-md-4 divTo to bordered">
		    <label class="labels">TO:</label>
		    <br>
		    <label class="labels required">DATE:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.dateTo" class="date_picker form-control input-small">
			    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		    <div class="clear_10"></div>
		    <label class="labels required">TIME IN/OUT:</label>
			<div class="input-group bootstrap-timepicker timepicker">
			    <input type="text" v-model="cutTime.toTimeInOut" class="time_picker form-control input-small">
			    <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			</div>
		  </div>
		  <div class="col-md-1"></div>
		</div>
	    <div class="clear_10"></div>
		<div class="row3_form_container">
			<div class="col1_form_container">
				<label class="labels required">REASON:</label>
			</div>
			<div class="textarea_form_container">
				<textarea rows="3" class="form-control textarea_inside_width" v-model="cutTime.reason"></textarea>
			</div>
		</div>
	</div>
</div>