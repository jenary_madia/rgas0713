<div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>OFFSET DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">SCHEDULE TYPE:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control text-uppercase" value="{{ $notifDetails["schedule_type"] }}" readonly>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> DURATION OF OFFSET:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control text-uppercase" value="{{ $notifDetails["duration_type"] }}" readonly>
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row">
            <div class="col-md-4 from bordered">
                <label class="labels">FROM:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <input type="text" class="form-control" value="{{ $notifDetails["from_date"] }}" readonly>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                <input type="text" class="form-control text-uppercase" value="{{ $notifDetails["from_absentperiod"] }}" readonly>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input type="text" class="form-control" value="{{ $notifDetails["from_absenthours"] }}" readonly>
            </div>
            <div class="col-md-4 divTo to bordered"
                 @if($notifDetails["duration_type"] != "multiple")
                 hidden
                 @endif
            >
                <label class="labels">TO:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <input type="text" class="form-control" value="{{ $notifDetails["to_date"] }}" readonly>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                <input type="text" class="form-control text-uppercase" value="{{ $notifDetails["to_absentperiod"] }}" readonly>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input type="text" class="form-control" value="{{ $notifDetails["to_absenthours"] }}" readonly>
            </div>
            <div class="col-md-1"></div>
        </div>


        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels rea"> TOTAL OFFSET DAYS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control" value="{{ $notifDetails["totaldays"] }}" readonly>
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels"> TOTAL OFFSET HOURS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control" value="{{ $notifDetails["totalhours"] }}" readonly>
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="textarea_form_container">
                <textarea rows="3" class="form-control textarea_inside_width" name="reason" readonly>{{ $notifDetails["reason"] }}</textarea>
            </div>
        </div>
    </div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>TIME OFFSET REFERENCE</strong></h5></div>
    <div class="row">
        <div class="col-md-12">
            <table class="table bordered">
                <thead>
                <tr>
                    <td>OT/OB DATE</td>
                    <td>TYPE</td>
                    <td>TIME IN/START TIME</td>
                    <td>TIME OUT/END TIME</td>
                    <td>OT/OB DURATION</td>
                    <td>REASON</td>
                    <td>OB DESTINATION</td>
                </tr>
                </thead>
                <tbody>
                @foreach($notifAdditionalDetails as $addtionalDetail)
                    <tr>
                        <td>{{ $addtionalDetail->date }}</td>
                        <td>{{ $addtionalDetail->type }}</td>
                        <td>{{ $addtionalDetail->time_start }}</td>
                        <td>{{ $addtionalDetail->time_end }}</td>
                        <td>{{ $addtionalDetail->total_time }}</td>
                        <td>{{ $addtionalDetail->reason }}</td>
                        <td>{{ ($addtionalDetail->type == 'OB' ? $addtionalDetail->destination : "----") }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="clear_20"></div>
        <div class="col-md-3">
            <label class="labels required"> TOTAL NUMBER OF HOURS:</label>
        </div>
        <div class="col-md-1">
            <input type="text" name="totalListOffsetHours" class="form-control" readonly value="{{ $total }}">
        </div>
    </div>
</div>

