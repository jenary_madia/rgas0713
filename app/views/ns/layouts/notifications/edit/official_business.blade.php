<div v-show="notificationType == 'official'" class="text-uppercase">
	<div class="clear_20"></div>
	<div class="container-header"><h5 class="text-center lined"><strong>OFFICIAL BUSINESS DETAILS</strong></h5></div>
	<div class="row">
	    <div class="row_form_container">
	        <div class="col1_form_container">
	            <label class="labels required">OB DURATION:</label>
	        </div>
	        <div class="col2_form_container">
				{{ Form::select('OBdurationType', array('' => '--SELECT DURATION OF OB--', 'half' => 'HALF DAY','one' => 'ONE (1) DAY', 'multiple' => 'MULTIPLE DAYS'), $notifDetails['duration_type'], ['class'=>'form-control otherFields noselectAll durationType','v-model'=>'officialBusiness.duration']) }}
	        </div>
	    </div>
		<div class="clear_10"></div>
		<div class="row">
		  <div class="col-md-4 from bordered">
		    <label class="labels">FROM:</label>
		    <br>
		    <label class="labels required">DATE:</label>
				<div class="input-group bootstrap-timepicker timepicker">
				    <input type="text" class="date_picker form-control input-small" value="{{  $notifDetails['from_date'] }}" v-model="officialBusiness.dateFrom">
				    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
		    <div class="clear_10"></div>
		    <label class="labels required">START TIME:</label>
			  <div class="input-group bootstrap-timepicker timepicker">
				  <input type="text" v-model="officialBusiness.fromStart" value="{{ $notifDetails['from_inout'] }}" id="fromStartTime" class="time_picker form-control input-small">
				  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			  </div>
		  	<div class="clear_10"></div>
		    <label class="labels required">END TIME:</label>
			  <div class="input-group bootstrap-timepicker timepicker">
				  <input type="text" v-model="officialBusiness.fromEnd" value="{{ $notifDetails['from_out'] }}" id="fromEndTime" class="time_picker form-control input-small">
				  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			  </div>
		  </div>
		  <div class="col-md-4 divTo to bordered"
			   @if($notifDetails['duration_type'] != "multiple")
					hidden
			   @endif
          >
		    <label class="labels">TO:</label>
		    <br>
		    <label class="labels required">DATE:</label>
				<div class="input-group bootstrap-timepicker timepicker">
				    <input type="text" class="date_picker form-control input-small" value="{{ $notifDetails['to_date'] }}" v-model="officialBusiness.dateTo">
				    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
				</div>
		    <div class="clear_10"></div>
		    <label class="labels required">START TIME:</label>
			  <div class="input-group bootstrap-timepicker timepicker">
                  <input type="text" v-model="officialBusiness.toStart" value="{{ $notifDetails['to_inout'] }}" id="toStartTime" class="time_picker form-control input-small">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
			  </div>
		    <div class="clear_10"></div>
		    <label class="labels required">END TIME:</label>
              <div class="input-group bootstrap-timepicker timepicker">
                  <input type="text" v-model="officialBusiness.toEnd" value="{{ $notifDetails['to_out'] }}" id="toEndTime" class="time_picker form-control input-small">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
              </div>
		  </div>
		  <div class="col-md-1"></div>
		</div>
		<div class="clear_10"></div>
	    <div class="row_form_container">
	        <div class="col1_form_container">
				<label class="labels required">TOTAL OB DAYS:</label>
	        </div>
	        <div class="col2_form_container">
				<input type="text" class="form-control" value="@{{ getOBTotalDays }}" readonly name="OBtotalDays" v-model="officialBusiness.totalDays">
			</div>
	    </div>
		<div class="clear_10"></div>
		<div class="row3_form_container">
			<div class="col1_form_container">
				<label class="labels required">REASON:</label>
			</div>
			<div class="textarea_form_container">
				<textarea rows="3" style="width:720px" class="form-control textarea_inside_width" v-model="officialBusiness.reason">{{ $notifDetails['reason'] }}</textarea>
			</div>
		</div>
        <div class="clear_10"></div>
		<div class="row_form_container">
			<div class="col1_form_container">
				<label class="labels required">DESTINATION:</label>
			</div>
			<div class="col2_form_container">
				<textarea rows="3" style="width:720px" class="form-control textarea_inside_width" v-model="officialBusiness.destination">{{ $notifDetails['destination'] }}</textarea>
			</div>
		</div>
	</div>
</div>

