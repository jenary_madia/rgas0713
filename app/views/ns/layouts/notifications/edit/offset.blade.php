<div v-show="notificationType == 'offset'"  class="text-uppercase">
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>OFFSET DETAILS</strong></h5></div>
    <div class="row">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">SCHEDULE TYPE:</label>
            </div>
            <div class="col2_form_container">
                {{ Form::select('schedule_type', array('' => '--Choose Schedule Type--', 'compressed' => 'Compressed','regular' => 'Regular', 'special' => 'Special'),  $notifDetails['schedule_type'], ['class'=>'form-control otherFields text-uppercase','id'=>'schedule_typeOffset','v-model'=>'offset.scheduleType']) }}
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> DURATION OF OFFSET:</label>
            </div>
            <div class="col2_form_container">
                {{ Form::select('durationType', array('' => '--Select Duration of Offset--', 'half' => 'HALF DAY','one' => 'ONE (1) DAY', 'multiple' => 'MULTIPLE DAYS'),  $notifDetails['duration_type'], ['class'=>'form-control otherFields noselectAll durationType text-uppercase','v-model'=>'offset.duration']) }}
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row">
            <div class="col-md-4 from bordered">
                <label class="labels">FROM:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input type="text" value="{{ $notifDetails['from_date'] }}" name="dateFrom" id="dateFrom" v-model="offset.dateFrom" class="date_picker offset-date form-control input-small">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                {{ Form::select('periodFrom', array("" => "--CHOOSE LEAVE PERIOD--","morning" => "MORNING","afternoon" => "AFTERNOON"),  $notifDetails['from_absentperiod'], ['class'=>'form-control pull-right','v-model'=>'offset.periodFrom','id'=>'periodFrom','v-if' => "offset.duration == 'half'"]) }}
                {{ Form::select('periodFrom', array("whole" => "WHOLE"),  "whole", ['class'=>'form-control pull-right','v-model'=>'offset.periodFrom','id'=>'periodFrom','v-if' => "offset.duration == 'one'"]) }}
                {{ Form::select('periodFrom', array("" => "--CHOOSE LEAVE PERIOD--","morning" => "MORNING","afternoon" => "AFTERNOON","whole" => "WHOLE"),  $notifDetails['from_absentperiod'], ['class'=>'form-control pull-right','v-model'=>'offset.periodFrom','id'=>'periodFrom','v-if' => "offset.duration == 'multiple' || offset.duration == ''"]) }}
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input name="hoursFrom" :disabled="! offsetPeriodEditable" value="{{ $notifDetails['from_absenthours'] }}" type="number" max="99"  class="form-control" v-model="offset.hoursFrom"/>
            </div>
            <div class="col-md-4 divTo to bordered"
                 @if( $notifDetails['duration_type'])
                     @if( $notifDetails['duration_type'] != "multiple")
                        hidden
                     @endif
                 @else
                        hidden
                 @endif

            >
                <label class="labels">TO:</label>
                <br>
                <label class="labels required">OFFSET DATE:</label>
                <div class="input-group bootstrap-timepicker timepicker">
                    <input type="text" name="dateTo" value="{{ $notifDetails['to_date'] }}" id="dateTo" v-model="offset.dateTo" class="date_picker offset-date form-control input-small">
                    <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                </div>
                <div class="clear_10"></div>
                <label class="labels required">OFFSET PERIOD:</label>
                {{ Form::select('periodTo', array("" => "--CHOOSE LEAVE PERIOD--","morning" => "MORNING","afternoon" => "AFTERNOON","whole" => "WHOLE"),  $notifDetails['to_absentperiod'], ['class'=>'form-control pull-right','v-model'=>'offset.periodTo','id'=>'periodTo']) }}
                <div class="clear_10"></div>
                <label class="labels required">OFFSET HOURS:</label>
                <input name="hoursTo" :disabled="! offsetPeriodEditable" value="{{ $notifDetails['to_absenthours'] }}" type="number" max="99"  class="form-control" v-model="offset.hoursTo"/>
            </div>
            <div class="col-md-1"></div>
        </div>


        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> TOTAL OFFSET DAYS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" name="totalDays" id="totalDays" class="form-control" readonly value="@{{ getTotalDays }}" v-model="offset.totalDays">
            </div>
        </div>
        <div class="clear_10"></div>
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required"> TOTAL OFFSET HOURS:</label>
            </div>
            <div class="col2_form_container">
                <input type="text" class="form-control" name="totalHours"
                            value="@{{ getOffsetTotalHours }}"
                       readonly v-model="offset.totalHours">
            </div>
        </div>

        <div class="clear_10"></div>
        <div class="row3_form_container">
            <div class="col1_form_container">
                <label class="labels required">REASON:</label>
            </div>
            <div class="textarea_form_container">
                <textarea rows="3" class="form-control textarea_inside_width" name="reason" v-model="offset.reason">{{ $notifDetails['reason'] }}</textarea>
            </div>
        </div>
    </div>
    <div class="clear_20"></div>
    <div class="container-header"><h5 class="text-center lined"><strong>TIME OFFSET REFERENCE</strong></h5></div>
    <div class="row">
        <div class="col-md-12">
            <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table class="table table-bordered notif-table">
                    <thead>
                    <tr>
                        <td><span class="labels2">OT/OB DATE</span></td>
                        <td><span class="labels2">TYPE</span></td>
                        <td><span class="labels2">TIME IN/START TIME</span></td>
                        <td><span class="labels2">TIME OUT/END TIME</span></td>
                        <td><span class="labels2">OT/OB DURATION</span></td>
                        <td><span class="labels2">REASON</span></td>
                        <td><span class="labels2">OB DESTINATION</span></td>
                    </tr>
                    </thead>
                    <tbody>
                        <tr v-for="LOR in offset.listOffsetReferences" v-bind:class="{ 'active' : LOR.active }" @click="chooseOffsetReference(LOR,$index)">
                            <td>@{{ LOR.OTDate }}</td>
                            <td>@{{ LOR.type }}</td>
                            <td>@{{ LOR.timeInStartTime }}</td>
                            <td>@{{ LOR.timeOutEndTime }}</td>
                            <td>@{{ LOR.OTDuration }}</td>
                            <td>@{{ LOR.reason }}</td>
                            <td>@{{ LOR.OBDestination }}</td>
                        </tr>
                    </tbody>
                    {{--<input type="hidden" value="@{{ listOffsetReferences | json }}" id="username" name="listReference">--}}
                    {{--<input type="hidden" v-model="listOffsetReferenceInputOld" value='{{ Input::old("listReference") }}'>--}}
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <button :disabled="! addable" v-on:click="clearData" data-toggle="modal" data-target="#offsetAddModal">ADD</button>
            <button :disabled="! addable" @click.prevent="editOffsetReference">EDIT</button>
            <button :disabled="! addable" class="offsetDelete" @click.prevent="deleteOffsetReference">DELETE</button>
        </div>
        <div class="clear_20"></div>
        <div class="col-md-3">
            <label class="labels"> TOTAL NUMBER OF HOURS:</label>
        </div>
        <div class="col-md-1">
            <input type="text" name="totalListOffsetHours" class="form-control" readonly value="@{{ getTotalListOffsetHours }}" v-model="offset.totalListOffsetHours">
        </div>
    </div>
</div>