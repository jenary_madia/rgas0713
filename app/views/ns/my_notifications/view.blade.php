@extends('template/header')

@section('content')
    <div id="app">
        <div class="form_container">
            <div class="container-header"><h5 class="text-center"><strong>NOTIFICATION APPLICATION FORM</strong></h5></div>
            <div class="row">
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NAME:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeName" value="{{ $notifDetails['firstname'].' '.$notifDetails['middlename'].' '.$notifDetails['lastname'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">REFERENCE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" value="{{ $notifDetails['documentcode'].'-'.$notifDetails['codenumber'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">EMPLOYEE NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="employeeNumber" value="{{ $notifDetails['employeeid'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DATE FILED:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="dateFiled" value="{{ $notifDetails['datecreated'] }}"/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">COMPANY:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="company" value="{{ $notifDetails['company'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">STATUS:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="status" value="{{ $notifDetails['status'] }}" />
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">DEPARTMENT:</label>
                    </div>
                    <div class="col2_form_container">
                        <input readonly="readonly" type="text" class="form-control" name="department" value="{{ $notifDetails['department'] }}" />
                    </div>
                </div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">CONTACT NUMBER:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="contactNumber" maxlength="20" value="{{ $notifDetails['contact_no'] }}" readonly/>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels">SECTION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control" name="section" value="{{ $notifDetails['section'] }}" readonly/>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="clear_10"></div>
                <div class="row_form_container">
                    <div class="col1_form_container">
                        <label class="labels required">TYPE OF NOTIFICATION:</label>
                    </div>
                    <div class="col2_form_container">
                        <input type="text" class="form-control text-uppercase" value="{{ $notifDetails['notif_name']['text']}}" readonly>
                    </div>
                </div>
            </div>
            @if($notifDetails['noti_type'] == "offset")
                @include("ns.layouts.notifications.offset")
            @elseif($notifDetails['noti_type'] == "undertime")
                @include("ns.layouts.notifications.undertime")
            @elseif($notifDetails['noti_type'] == "cut_time")
                @include("ns.layouts.notifications.cut_time")
            @elseif($notifDetails['noti_type'] == "TKCorction")
                @include("ns.layouts.notifications.TKCorction")
            @elseif($notifDetails['noti_type'] == "official")
                @include("ns.layouts.notifications.official")
            @endif
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
            <div class="row">
                <div class="col-md-6">
                    <p style="font-size: 12px"><strong>FILER : </strong></p>
                    <!-- ATTACHMENTS : -->
                    <br>
                    @for ($i = 1; $i < 6; $i++)
                        <a href="{{ URL::to('/ns/download'.'/'.$notifDetails['documentcode'].'-'.$notifDetails['codenumber'].'/'.json_decode($notifDetails["attach$i"],true)["random_filename"]).'/'.CIEncrypt::encode(json_decode($notifDetails["attach$i"],true)["original_filename"])}}">{{ json_decode($notifDetails["attach$i"],true)["original_filename"] }}</a><br />
                    @endfor
                </div>
                @if($notifDetails['receiver_attachments'])
                    <div class="col-md-6">
                        <p><strong>CHRD : </strong></p>
                        @foreach(json_decode($notifDetails['receiver_attachments'],true) as $key)
                            <a href="{{ URL::to('/ns/download'.'/'.$notifDetails['documentcode'].'-'.$notifDetails['codenumber'].'/'.$key["random_filename"].'/'.CIEncrypt::encode($key["original_filename"])) }}">{{ $key["original_filename"] }}</a><br />
                        @endforeach
                    </div>
                @endif
            </div>
            <div {{ (count($notifSignatories) == 0 && $notifDetails['status'] != 'DISAPPROVED' ? "hidden" : "") }}>
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>

                <br>
                @foreach($notifSignatories as $signatory)
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col1_form_container">
                                <label class="labels">{{ ($signatory["signature_type"] == 1 ? "NOTED" : "APPROVED") }} :</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right" style="width: 200px">
                                <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                            </div>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                @endforeach
            </div>
        </div><!-- end of form_container -->
        @if($method == 'view')
            {{ Form::open(array('url' => array('ns/my_notif_actions',$notifDetails['id']), 'method' => 'post')) }}
            @include("ns.actions.my_notifications.view")
            {{ Form::close() }}
        @endif
    </div>
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/jquery-ui-1.10.4.custom.js') }}
    {{ HTML::script('/assets/js/notification/general.js') }}
@stop