<div class="clear_20"></div>
<span class="action-label labels">ACTION</span>
<div class="form_container">
    <div class="clear_20"></div>
    @if($notifDetails['status'] == 'NEW')
        <div class="textarea_messages_container">
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" class="form-control textarea_inside_width" name="comment">@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
        </div>
    @else
        <div class="textarea_messages_container">
            <div class="row">
                <label class="textarea_inside_label">MESSAGE:</label>
                <textarea rows="3" class="form-control textarea_inside_width" readonly>@foreach (explode('|',$notifDetails['comment']) as $message){{ json_decode($message,true)['name'].' '.json_decode($message,true)['datetime'].' : '.json_decode($message,true)['message'] }} &#013;@endforeach</textarea>
            </div>
            <div class="clear_20"></div>
            <div class="row">
                <label class="textarea_inside_label">COMMENT:</label>
                <textarea rows="3" {{ ($notifDetails['status'] != 'FOR APPROVAL' ? "disabled" : "") }} class="form-control textarea_inside_width" name="comment"></textarea>
            </div>
        </div>
    @endif
    <div class="clear_10"></div>
    <div class="row">
        <div class="comment_container">
            <div class="row">
                <div class="col-md-12 text-center">
                    @if($notifDetails['status'] == 'FOR APPROVAL')
                    <button value="cancel" name="action" class="btn btn-default btndefault">CANCEL REQUEST</button>
                    @endif
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                </div>
            </div>
        </div>
    </div>
</div>