@extends('template/header')

@section('content')

    <div id="wrap">
        <div class="clear_20"></div>
        <div class="wrapper">
            <div class="datatable_holder">
                <span class="list-title">MY NOTIFICATION</span>
                {{ Form::open(array('url' => 'ns/process', 'method' => 'post', 'files' => true,'id' => 'formMyNotifications')) }}
                <table id="myNotifications" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myNotifications">
                    <thead>
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Notification</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Notification</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
            {{ Form::close() }}
            <div class="clear_20"></div>
            @if(in_array(Session::get("username"),json_decode(PRODUCTION_ENCODERS_FILER,true)))
                <div class="datatable_holder">
                    <span class="list-title text-uppercase">MY NOTIFICATION (PRODUCTION ENCODER)</span>
                    {{ Form::open(array('url' => 'ns/pe-process', 'method' => 'post', 'files' => true,'id' => 'formPEMyNotifications')) }}
                        <table id="myPENotifications" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="myPENotifications">
                        <thead>
                        <tr role="row">
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Department</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Current</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                        </table>
                    {{ Form::close() }}
                </div>
            @endif
            <div class="clear_20"></div>
            @if(Session::get("desig_level") != "employee")
                <hr />
                <div class="clear_20"></div>
                <div class="datatable_holder">
                    <span class="list-title text-uppercase">For Approval</span>
                    {{ Form::open(array('url' => 'ns/process', 'method' => 'post', 'files' => true,'id' => 'formApproveNotif')) }}
                    <table id="superiorNotification" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorNotification">
                        <thead>
                        <tr role="row">
                            <th class="" role="columnheader" rowspan="1"><input type="checkbox" id="chooseAll_superiorNotification"> SELECT ALL</th>
                        </tr>
                        <tr role="row">
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Type of Notification</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date of Notification</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                            <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                        </tr>
                        </thead>
                    </table>
                    {{ Form::close() }}
                </div>
                    <div class="clear_20"></div>
                    <hr />
                    <div class="clear_20"></div>
                    @if(! in_array(Session::get("company"),json_decode(CORPORATE,true)))
                        <div class="clear_20"></div>
                        <div class="datatable_holder">
                            <span class="list-title text-uppercase">FOR APPROVAL (PRODUCTION ENCODER)</span>
                            <table id="superiorPENotification" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" width="100%" aria-describedby="superiorPENotification">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Department</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Section</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Status</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                                    <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    @endif
            @endif
        </div>
    </div>
    <hr />
    @stop
    @section('js_ko')
        {{ HTML::script('/assets/js/notification/mynotifications.js') }}
        {{ HTML::script('/assets/js/notification/superior_notification.js') }}
        {{ HTML::script('/assets/js/notification/my_pe_notifications.js') }}
        {{ HTML::script('/assets/js/notification/superior_pe_notifications.js') }}
    @stop
