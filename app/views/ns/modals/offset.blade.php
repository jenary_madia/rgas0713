<div class="modal fade" id="offsetAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD TIME OFFSET REFERENCE DETAILS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <validator name="offsetReferenceValidation">
                        <div class="col-md-4">
                            <label for="" class="labels required">OT/OB DATE</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="date_picker form-control input-small otDate" v-validate:otDate="['required']" v-model="offsetReference.OTDate">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar toggleDatePicker"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required offsetType">TYPE</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="" id="" class="form-control" v-model="offsetReference.type">
                                    <option value="OT">OVERTIME</option>
                                    <option value="OB">OB/HOLIDAYS</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">TIME IN/START TIME</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="time_picker form-control input-small" v-validate:timeStart="['required']" v-model="offsetReference.timeInStartTime">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time toggleDatePicker"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">TIME OUT/END TIME</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="time_picker form-control input-small" v-validate:timeEnd="['required']" v-model="offsetReference.timeOutEndTime">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">OT/OB DURATION</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" class="form-control" v-model="offsetReference.OTDuration">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">REASON</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea rows="3" v-model="offsetReference.reason" v-validate:reason="['required']" class="form-control" name="lrfReason"></textarea>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div v-if="offsetReference.type =='OB' ">
                            <div class="col-md-4">
                                <label for="" class="labels required offsetOBDestination">OB DESTINATION</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-validate:destination="['required']" :disabled="offsetReference.type == 'OT'" v-model="offsetReference.OBDestination" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 pager">
                            <button class="btn btn-default btndefault" :disabled="! $offsetReferenceValidation.valid || processing || ! checkDateFormat" v-on:click="gatherData">SAVE</button>
                            <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                        </div>
                    </validator>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="offsetEditModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">EDIT TIME OFFSET REFERENCE DETAILS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <validator name="offsetReferenceValidationEdit">
                        <div class="col-md-4">
                            <label for="" class="labels required">OT/OB DATE</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="date_picker form-control input-small otDate" v-validate:otDate="['required']" v-model="forEditReference.OTDate">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar toggleDatePicker"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required offsetType">TYPE</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select name="" id="" class="form-control" v-model="forEditReference.type">
                                    <option value="OT">OVERTIME</option>
                                    <option value="OB">OB/HOLIDAYS</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">TIME IN/START TIME</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="time_picker form-control input-small" v-validate:timeStart="['required']" v-model="forEditReference.timeInStartTime">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">TIME OUT/END TIME</label>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" class="time_picker form-control input-small" v-validate:timeEnd="['required']" v-model="forEditReference.timeOutEndTime">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">OT/OB DURATION</label>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="number" class="form-control" v-model="forEditReference.OTDuration">
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="col-md-4">
                            <label for="" class="labels required">REASON</label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <textarea rows="3" v-model="forEditReference.reason" v-validate:reason="['required']" class="form-control" name="lrfReason"></textarea>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div v-if="forEditReference.type =='OB' ">
                            <div class="col-md-4">
                                <label for="" class="labels required offsetOBDestination">OB DESTINATION</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" v-validate:destination="['required']" :disabled="forEditReference.type == 'OT'" v-model="forEditReference.OBDestination" >
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 pager">
                            <button class="btn btn-default btndefault" :disabled="! $offsetReferenceValidationEdit.valid || processing || ! checkforEditDateFormat" @click="saveEdit">SAVE</button>
                            <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                        </div>
                    </validator>
                </div>
            </div>
        </div>
    </div>
</div>