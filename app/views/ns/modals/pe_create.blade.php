<div class="modal fade" id="addNotifDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog PECreationModal" role="document">
        <div class="modal-content">
            <validator name="peAddEmployee">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Notification Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NAME</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" style="width: 60%;">
                                    <select class="form-control"  style="width: 100%;"  v-validate:employee="['required']" v-model="employeeIndex" v-on:change="parseToAddData">
                                        <option v-for="employee in employees" value="@{{ $index }}">@{{ employee.firstname +' '+ employee.middlename +' '+ employee.lastname }}</option>
                                    </select>

                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NUMBER</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" style="width: 60%;">
                                    <input type="text" class="form-control input-xs" readonly value="@{{ toAdd.employeeNumber }}">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">NOTIFICATION TYPE</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" style="width: 60%;">
                                    {{ Form::select('notificationType', $notificationType, Input::old('notificationType', 'undertime'), ['v-model' =>'toAdd.notificationType', 'style' => 'width: 100%;','id' => 'notificationType','class'=> 'form-control text-uppercase','v-validate:notificationType'=>'["required"]' ]) }}
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">DATE</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" style="width: 60%;">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:date="['required']" type="text" value="{{ Input::old("date") }}" name="date" v-model="toAdd.date" class="date_picker form-control input-small">
                                        <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4"  style="padding-left : 100px">
                                <label for="" class="labels required">TIME IN</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group" style="width: 60%;">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:timeIn="['required']" type="text" value="{{ Input::old("timeIn") }}" name="timeIn" id="timeIn" v-model="toAdd.timeIn" class="time_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4"  style="padding-left : 100px">
                                <label for="" class="labels required">TIME OUT</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group"  style="width: 60%;">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:timeOut="['required']" type="text" value="{{ Input::old("timeOut") }}" name="timeOut" id="timeOut" v-model="toAdd.timeOut" class="time_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">REASON</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="reason" id="" class="form-control" cols="100" rows="10" v-validate:reason="['required']" v-model="toAdd.reason"></textarea>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-primary" @click="getToAddData" :disabled="! $peAddEmployee.valid || processing == true">SAVE</button>
                </div>
            </validator>
        </div>
    </div>
</div>

<div class="modal fade" id="editNotifDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog PECreationModal" role="document">
        <div class="modal-content">
            <validator name="peEditEmployee">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Notification Details</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline">
                        <div class="row">
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NAME</label>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text" class="form-control input-xs" readonly v-model="toEdit.employeeName">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">EMPLOYEE NUMBER</label>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input type="text" class="form-control input-xs" readonly v-model="toEdit.employeeNumber">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">NOTIFICATION TYPE</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    {{ Form::select('notificationType', $notificationType, '', ['id' => 'notificationType','class'=> 'form-control','v-model'=> 'toEdit.notificationType','v-validate:notificationType'=>'["required"]' ]) }}
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">DATE</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:date="['required']" type="text" name="date" v-model="toEdit.date" class="date_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4"  style="padding-left : 100px">
                                <label for="" class="labels required">TIME IN</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:timeIn="['required']" type="text" value="{{ Input::old("timeIn") }}" name="timeIn" id="timeIn" v-model="toEdit.timeIn" class="time_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4"  style="padding-left : 100px">
                                <label for="" class="labels required">TIME OUT</label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input v-validate:timeOut="['required']" type="text" value="{{ Input::old("timeOut") }}" name="timeOut" id="timeOut" v-model="toEdit.timeOut" class="time_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="col-md-4">
                                <label for="" class="labels required">REASON</label>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <textarea name="reason" id="" class="form-control" cols="100" rows="10" v-validate:reason="['required']" v-model="toEdit.reason"></textarea>
                                </div>
                            </div>
                            <br>
                            <br>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">CANCEL</button>
                    <button type="button" class="btn btn-primary" @click="saveEdit" :disabled="! $peEditEmployee.valid || processing == true">SAVE</button>
                </div>
            </validator>
        </div>
    </div>
</div>