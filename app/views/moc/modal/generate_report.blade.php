<div class="modal fade" id="generateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">GENERATE REPORT</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">FILE NAME : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" name="filename" class="form-control">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">STATUS : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <select name="status" id="status" v-model="status" name="status" class="form-control">
                                <option selected>ALL</option>
                                <option value="FOR ASSESSMENT">FOR ASSESSMENT</option>
                                <option value="FOR APPROVAL">FOR APPROVAL</option>
                                <option value="FOR ACKNOWLEDGEMENT">FOR ACKNOWLEDGEMENT</option>
                                <option value="ACKNOWLEDGED">ACKNOWLEDGED</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">FROM : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" name="from" v-model="from" id="from" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-4">
                        <label for="" class="labels">TO : </label>
                    </div>

                    <div class="col-md-8">
                        <div class="form-group">
                            <div style="width : 200px;" class="pull-left input-group bootstrap-timepicker timepicker">
                                <input type="text" name="to" v-model="to" id="to" class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 pager text-center">
                        <button class="btn btn-default btndefault" value="generate" name="action">GENERATE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

