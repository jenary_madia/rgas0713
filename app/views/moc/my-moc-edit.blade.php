@extends('template/header')

@section('content')
    <div id="moc_edit" v-cloak>
        {{--FOR for message prompt--}}
        <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
            <p>@{{ submitStatus.message }}</p>
        </div>
        <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
            <ul>
                <li v-for="error in submitStatus.errors">@{{ error }}</li>
            </ul>
        </div>
        {{--END for message prompt--}}
        {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>{{ MOC_FORM_TITLE }}</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="@{{ formDetails.employeeName }}" />
                </div>
                <div class="col-md-6 pull-left">
                    <label class="labels required">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" value="@{{ formDetails.referenceNo }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="@{{ formDetails.employeeNumber }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DATE FILED:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" value="@{{ formDetails.dateFiled }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">COMPANY:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="@{{ formDetails.company }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">STATUS:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="@{{ formDetails.status }}" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DEPARTMENT:</label>
                    <input v-model="formDetails.department" readonly="readonly" type="text" class="form-control pull-right" name="department" />
                </div>
                <div class="col-md-6">
                    <label class="labels">CONTACT NUMBER:</label>
                    <input v-model="formDetails.contactNumber"  type="text" class="form-control pull-right" name="contactNumber" maxlength="20" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels pull-left">SECTION:</label>
                    <input v-model="formDetails.section"  type="text" class="form-control pull-right" name="section" maxlength="25" />
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-12">
                    <div class="moc-sidebar">
                        <label class="labels required">TYPE OF REQUEST:</label>
                        <br>
                        <ul class="moc-list list-unstyled">
                            @foreach($requestTypes as $requestType)
                                <li class="text-uppercase"><input type="checkbox" v-model="mocDetails.requestTypes" value="{{ $requestType->item }}"> {{ $requestType->text }}</li>
                            @endforeach
                        </ul>
                        <label class="labels required pull-left">URGENCY:</label>
                        <select v-model="mocDetails.urgency" class="form-control">
                            <option value="immediate">IMMEDIATE ATTENTION NEEDED</option>
                            <option value="normal">NORMAL PRIORITY</option>
                        </select>
                    </div>
                    <div class="moc-main">
                        <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                        <div class="moc-inner-sidebar">
                            <label v-if="! enabledForRequest" class="labels">CHANGE IN:</label>
                            <label v-else class="labels required">CHANGE IN:</label>
                            <br>
                            <ul class="list-unstyled moc-list">
                                @foreach($changeIn as $change)
                                    @if($change->item == "others")
                                        <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox"  v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }} <input :disabled="! enabledForRequest || mocDetails.changeIn.indexOf('others') == -1" style="width : 140px; text-transform: none !important;" v-model="mocDetails.changeInOthersInput" type="text" class="pull-right form-control"></li>
                                    @else
                                        <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox" v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }}</li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="moc-inner-main">
                            <div class="moc-inner-sidebar">
                                <label v-if="! enabledForRequest" class="labels pull-left">CHANGE TYPE:</label>
                                <label v-else class="labels pull-left required">CHANGE TYPE:</label>
                                <select v-model="mocDetails.changeType" class="form-control" :disabled="! enabledForRequest">
                                    <option value="permanent">PERMANENT</option>
                                    <option value="temporary">TEMPORARY</option>
                                </select>
                            </div>
                            <div class="moc-inner-main">
                                <label v-if="! enabledForRequest" class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                                <label v-else class="labels pull-right required">TARGET IMPLEMENTATION DATE:</label>
                                <div class="input-group bootstrap-timepicker timepicker">
                                    <input :disabled="! enabledForRequest" v-model="mocDetails.targetDate" type="text" name="" id="UTdate" class="date_picker form-control input-small">
                                    <span v-if="! enabledForRequest" class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    <span v-else class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i>
                                </div>
                            </div>
                            <label v-if="! enabledForRequest" style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                            <label v-else style="margin-top: 10px" class="labels pull-left required">DEPARTMENTS INVOLVED:</label>
                            <div class="clearfix"></div>
                            <div class="moc-inner-main">
                                <select :disabled="! enabledForRequest" class="form-control" v-model="selectedDepartment.id" name="" id="">
                                    <option v-for="department in departments" value="@{{ $index }}">@{{ department.comp_code }} - @{{ department.dept_name }}</option>
                                </select>
                            </div>
                            <div class="moc-inner-sidebar">
                                <button :disabled="! enabledForRequest || ! selectedDepartment.id" class="btn btn-default btndefault pull-right" @click.prevent="addDeptInvolved">ADD</button>
                            </div>
                            <div class="clearfix"></div>
                            <br>
                            <div class="moc-whole" style="border : 1px solid #ccc;">
                                <ol class="moc-list">
                                    <li v-for="deptInvolved in mocDetails.departmentsInvolved">@{{ deptInvolved.comp_code }} - @{{ deptInvolved.dept_name }}<a @click="removeDeptInvolved($index)"> Remove</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                    <input v-model="mocDetails.title" style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS) : </label>
                    <textarea v-model="mocDetails.backgroundInfo" name="" id="" class="form-control" rows="7"></textarea>
                </div>
                @if(Session::get('is_ssmd') && Session::get('desig_level') == 'head' )
                    <div class="col-md-12">
                        <label class="labels pull-left" style="padding-top: 7px">SSMD ASSESSMENT/ACTION PLAN:</label>
                        <textarea name="" id="" class="form-control" rows="7" v-model="mocDetails.ssmdActionPlan">@{{ mocDetails.ssmd_action_plan }}</textarea>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                @endif
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-4">
                        <label class="attachment_note"><strong>ADD ATTACHMENTS </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div id="attachments">
                            <p v-for="attachment in attachmentsOld">
                                @{{ attachment.original_filename + ' | ' + attachment.filesize  }}
                                <input type='hidden' value="@{{ attachment | json }}">
                                <button class='btn btn-xs btn-danger' @click.prevent=removeAttachment($index)>DELETE</button>
                            </p>
                        </div>
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                    </div>
                    <div class="col-md-4">
                        @if((Session::get('is_ssmd') && Session::get('desig_level') == 'head'))
                            <label class="attachment_note"><strong>SSMD : </strong></label><br/>
                            <label class="attachment_note"><strong>ADD ATTACHMENTS </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                            <div id="SSMDattachments">
                                <p v-for="SSMDattachment in SSMDattachmentsOld">
                                    @{{ SSMDattachment.original_filename + ' | ' + SSMDattachment.filesize  }}
                                    <input type='hidden' value="@{{ SSMDattachment | json }}">
                                    <button class='btn btn-xs btn-danger remove-fn '>DELETE</button>
                                </p>
                            </div>
                            <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="SSMDfileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    <div v-if="formDetails.status != 'NEW'">
                        <label class="labels pull-left">MESSAGE:</label>
                        <textarea disabled rows="3" class="form-control pull-left" name="comment">@{{ messages }}</textarea>
                    </div>
                    <label class="labels pull-left">COMMENT:</label>
                    <textarea rows="3" class="form-control pull-left" name="comment" v-model="comment"></textarea>
                </div>
                @if(Session::get('company')== 'RBC-CORP')
                    @include('moc.actions.create.corp')
                @else
                    @include('moc.actions.create.sat')
                @endif
            </div>



        </div><!-- end of form_container -->
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    <script>
        var mocID = {{ $mocID }};
    </script>
    {{ HTML::script('/assets/js/moc/vue-edit.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
        var file_counter = 0;
        var allowed_file_count = 10;
        var allowed_total_filesize = 20971520;

        var SSMDfile_counter = 0;
        var SSMDallowed_file_count = 10;
        var SSMDallowed_total_filesize = 20971520;

        $('.date_picker').datepicker({
            dateFormat : 'yy-mm-dd'
        });
        $('body').on('click','.toggleDatePicker',function () {
            $(this).closest( "div" ).find(".date_picker").datepicker("show");
        });
    </script>
    {{ HTML::script('/assets/js/moc/file-upload.js') }}
@stop
