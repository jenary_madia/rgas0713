<div class="col-md-10 col-md-offset-1">
    <div class="row actions">
        @if(Session::get("company") == 'RBC-CORP')
            <div class="col-md-6">
                    <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO CSMD FOR PROCCESSING</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendMOCtoMD">SEND</button>
            </div>
        @else
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> TO ASSISTANT OPERATIONS MANAGER FOR APPROVAL</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendMOCtoMD">SEND</button>
            </div>
        @endif
    </div>
    <div class="row actions">
        <div class="col-md-6">
            <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
        </div>
        <div class="col-md-6 text-right">
            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnMOC">RETURN</button>
        </div>
        <div class="col-md-10 col-md-offset-1 text-center">
            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
        </div>
    </div>
</div>