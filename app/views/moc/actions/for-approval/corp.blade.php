<div class="col-md-10 col-md-offset-1">

    @if($mocDetails->requested_by == Session::get("employee_id"))
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes pull-left"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO</label>
                <select name="" id="" v-model="returnTo" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                    @foreach($lastTwoSigs as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnAction">RETURN</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button value="cancel" name="action" class="btn btn-default btndefault" @click.prevent="acknowledgeMOC">ACKNOWLEDGE</button>
                        <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO FILER FOR ACKNOWLEDGEMENT</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendToFilerAck">SEND</button>
            </div>
        </div>
        <div class="row actions">
            <div class="col-md-6">
                <label class="button_notes pull-left"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO</label>
                <select name="" id="" v-model="returnTo" class="form-control pull-left" style="margin: -8px 5px 0 5px; width : 200px;">
                    @foreach($lastTwoSigs as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="returnAction">RETURN</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        </div>
    @endif
</div>