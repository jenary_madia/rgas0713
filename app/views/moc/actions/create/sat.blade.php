<div class="col-md-10 col-md-offset-1">
    <div class="row actions">
        <div class="col-md-6">
            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
        </div>
        <div class="col-md-6 text-right">
            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="saveMOC">SAVE</button>
        </div>
    </div>
    <div class="row actions">
        <div class="col-md-8">
            @if(in_array(Session::get("desig_level"),['employee','supervisor']))
                <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR ENDORSEMENT</label>
            @else
                @if(Session::get('is_ssmd'))
                    <label class="button_notes">
                        <span class="pull-left"><strong>SEND</strong> TO ASSISTANT OPERATIONS MANAGER FOR APPROVAL</span>
                    </label>
                @else
                    <label class="button_notes"><strong>SEND</strong> TO SSMD FOR INITIAL ASSESSMENT</label>
                @endif
            @endif
        </div>
        <div class="col-md-4 text-right">
            <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendMOC">SEND</button>
        </div>
    </div>
</div>