<div class="col-md-10 col-md-offset-1">
    <div class="row actions">
        @if($mocDetails['requested_by'] == Session::get("employee_id"))
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="acknowledgeMOC">ACKNOWLEDGE</button>
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        @else
            <div class="col-md-6">
                <label class="button_notes"><strong>SEND</strong> TO FILER FOR ACKNOWLEDGEMENT</label>
            </div>
            <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-default btndefault" name="action" @click.prevent="sendToFilerAck">SEND</button>
            </div>
            <div class="col-md-10 col-md-offset-1 text-center">
                <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
            </div>
        @endif
    </div>
</div>