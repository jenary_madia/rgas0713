<div class="col-md-10 col-md-offset-1">
    <div class="row actions">
        <div class="col-md-12 text-center">
            <a value="cancel" name="action" href="{{ url('/moc/print',[$mocID]) }}" class="btn btn-default btndefault" >DOWNLOAD</a>
            <a class="btn btn-default btndefault" href="{{ URL::previous() }}">BACK</a>
        </div>
    </div>
</div>