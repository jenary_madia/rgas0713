<!doctype html>
<html>
<head>
    <title>{{-- Rebisco General Approval System --}}</title>

@section('head')

    <!-- General Style -->
        {{ Minify::stylesheet(
            array('/assets/css/bootstrap.css'
                 ,'/assets/css/metro-bootstrap.css'
                 ,'/assets/css/main.css'
                 ,'/assets/css/employee.css'
                 ,'/assets/css/validation_engine.css'
                 ,'/assets/css/datepicker.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
                 ,'/assets/css/chosen.min.css'
                 ,'/assets/css/bootstrap-timepicker.min.css'
                 ,'/assets/css/bootstrap-multiselect.css'
                 ,'/assets/css/new_style.css'
            ))->withFullUrl()
        }}

        <base href="<?php echo $base_url; ?>" />

    @show
    <link href="<?php echo $base_url; ?>/assets/css/font-awesome/css/font-awesome.css" rel="stylesheet">
    {{--<link href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />--}}
</head>
    <body>
        <div class="container">
            <div id="moc_edit">
            {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
            <div class="form_container msr-form-container">
                <div class="container-header"><h5 class="text-center"><strong>{{ MOC_FORM_TITLE }}</strong></h5></div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="row employee-details">
                    <table>
                        <tr>
                            <td><label class="labels required">EMPLOYEE NAME:</label></td>
                            <td><input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ $mocDetails['firstname'].' '.$mocDetails['middlename'].'. '.$mocDetails['lastname'] }}" /></td>
                            <td style="padding-left : 20px;">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </td>
                            <td>
                                <input readonly="readonly" type="text" class="form-control pull-right" value="{{ $mocDetails['transaction_code'] }}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                            </td>
                            <td>
                                <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ $mocDetails['employeeid'] }}" />
                            </td>

                            <td style="padding-left : 20px;">
                                <label class="labels required pull-left">DATE FILED:</label>
                            </td>
                            <td>
                                <input readonly="readonly" type="text" class="form-control pull-right" value="{{ $mocDetails['date'] }}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="labels required pull-left">COMPANY:</label>
                            </td>
                            <td>
                                <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ $mocDetails['company'] }}" />
                            </td>

                            <td style="padding-left : 20px;">
                                <label class="labels required pull-left">STATUS:</label>
                            </td>
                            <td>
                                <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="{{ $mocDetails['status'] }}" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="labels required pull-left">DEPARTMENT:</label>
                            </td>
                            <td>
                                <input value="{{ json_decode($mocDetails['dept_sec'])[0] }}" readonly="readonly" type="text" class="form-control pull-right" name="department" />
                            </td>

                            <td style="padding-left : 20px;">
                                <label class="labels">CONTACT NUMBER:</label>
                            </td>
                            <td>
                                <input value="{{ $mocDetails['contact_no'] }}" disabled type="text" class="form-control pull-right" name="contactNumber" maxlength="20" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label class="labels pull-left">SECTION:</label>
                            </td>
                            <td>
                                <input value="{{ json_decode($mocDetails['dept_sec'])[1] }}" disabled type="text" class="form-control pull-right" name="section" maxlength="25" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="row project-requirements">
                    <div class="col-md-12">
                        <div class="moc-sidebar">
                            <label class="labels required">TYPE OF REQUEST:</label>
                            <br>
                            <ul class="moc-list list-unstyled">
                                @if($requestTypes)
                                    @foreach($requestTypes as $requestType)
                                        @foreach($mocDetails['rcodes'] as $key)
                                            <li class="text-uppercase"  style="position: relative">
                                                <input disabled type="checkbox" {{ ($key->request_code == $requestType->item ? 'checked' : $key->request_desc) }} value="{{ $requestType->item }}"> {{ $requestType->text }}
                                            </li>
                                        @endforeach
                                    @endforeach
                                @endif
                            </ul>
                            <label class="labels required pull-left">URGENCY:</label>
                            <input value="{{ $mocDetails['urgency'] == 'immediate' ? 'IMMEDIATE ATTENTION NEEDED' : 'NORMAL PRIORITY' }}" readonly="readonly" type="text" class="form-control pull-right"/>
                            </select>
                        </div>
                        <div class="moc-main">
                            <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                            <div class="moc-inner-sidebar">
                                <label class="labels">CHANGE IN:</label>
                                <br>
                                <ul class="list-unstyled moc-list">
                                    @if($changeIn)
                                        @foreach($changeIn as $change)
                                            @if(count($mocDetails['ccodes']) > 0)
                                                @foreach($mocDetails['ccodes'] as $key)
                                                    @if($change->item == "others")
                                                        <li class="text-uppercase"><input disabled type="checkbox"  v-model="mocDetails.changeIn" {{ ($key->changein_code == $change->item ? 'checked' : '') }} value="{{ $change->item }}"> {{ $change->text }} <input disabled style="width : 140px; text-transform: none !important;" value="{{ $key->changein_desc }}" type="text" class="pull-right form-control"></li>
                                                    @else
                                                        <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.changeIn" {{ ($key->changein_code == $change->item ? 'checked' : '') }} value="{{ $change->item }}"> {{ $change->text }}</li>
                                                    @endif
                                                @endforeach
                                            @else
                                                @if($change->item == "others")
                                                    <li class="text-uppercase"><input disabled type="checkbox"  v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }} <input disabled style="width : 140px; text-transform: none !important;" value="{{ $key->changein_desc }}" type="text" class="pull-right form-control"></li>
                                                @else
                                                    <li class="text-uppercase"><input disabled type="checkbox" v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }}</li>
                                                @endif
                                            @endif

                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <div class="moc-inner-main">
                                <div class="moc-inner-sidebar">
                                    <label class="labels pull-left">CHANGE TYPE:</label>
                                    <input value="{{ $mocDetails['change_type'] }}" readonly="readonly" type="text" class="form-control pull-right"/>
                                </div>
                                <div class="moc-inner-main">
                                    <label class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                        <input disabled value="{{ ($mocDetails['implementation_date'] == '0000-00-00' ? '' : $mocDetails['implementation_date']) }}" type="text" name="" id="UTdate" readonly class="date_picker form-control input-small">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                                <label style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                                <div class="clearfix"></div>
                                <div class="moc-whole" style="border : 1px solid #ccc;">
                                    <ol class="moc-list">
                                        @if($dept_involved)
                                            @foreach($dept_involved as $key)
                                                <li>{{ $key->dept_code .' - '. $key->dept_name }}</li>
                                            @endforeach
                                        @endif
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                        <input value="{{ $mocDetails['title'] }}" disabled style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="col-md-12">
                        <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS)</label>
                        <textarea disabled name="" id="" class="form-control" rows="7">{{ $mocDetails['reason'] }}</textarea>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    @if($mocDetails["company"] != 'RBC-CORP' && ! in_array($mocDetails['status'],["FOR ENDORSEMENTS","NEW"]) || Session::get("is_ssmd"))
                        <div class="col-md-12">
                            <label class="labels required pull-left" style="padding-top: 7px">SSMD ASSESSMENT/ACTION PLAN:</label>
                            <textarea name="" id="" class="form-control" rows="7" disabled>{{ $mocDetails['ssmd_action_plan'] }}</textarea>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    @endif
                    @if($mocDetails["company"] == 'RBC-CORP' && ! in_array($mocDetails['status'],['NEW','FOR ENDORSEMENT'])  || $mocDetails["company"] != 'RBC-CORP' && $lastSignature > 2 || Session::get("dept_id") == CSMD_DEPT_ID)
                        <div class="col-md-12">
                            <label class="labels pull-left" style="padding-top: 7px">CSMD ASSESSMENT/ACTION PLAN:</label>
                            <textarea disabled name="" id="" class="form-control" rows="7">{{ $mocDetails['csmd_action_plan'] }}</textarea>
                        </div>
                    @endif
                    </div>
                <div class="clear_20"></div>
                <div>
                    <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                    <div class="row employee-details">
                        <table>
							<tr>
								<th style="width : 30%;"><label class="attachment_note"><strong>FILER : </strong></label></th>
								@if($mocDetails["company"] != 'RBC-CORP' && ! in_array($mocDetails['status'],["FOR ENDORSEMENTS","NEW"]) || Session::get("is_ssmd"))
									<th style="width : 30%;"><label class="attachment_note"><strong>SSMD : </strong></label></th>
								@endif
								@if($mocDetails["company"] != 'RBC-CORP' && ! in_array($mocDetails['status'],["FOR ENDORSEMENTS","NEW"]))
									<th style="width : 30%;"><label class="attachment_note"><strong>CSMD : </strong></label></th>
								@endif
							</tr>
                            <tr>
                                <td>
									@if($mocDetails["attachments"])
										@foreach($mocDetails["attachments"] as $key)
											@if($key['attachment_type'] == 0)
												<a style="display: block";> {{ json_decode($key['fn'])->original_filename }}</a>
											@endif
										@endforeach
									@endif
                                </td>
                            @if($mocDetails["company"] != 'RBC-CORP' && ! in_array($mocDetails['status'],["FOR ENDORSEMENTS","NEW"]) || Session::get("is_ssmd") && Session::get("desig_level") == "head")
								<td>
									@if($mocDetails["attachments"])
										@foreach($mocDetails["attachments"] as $key)
											@if($key['attachment_type'] == 1)
												<a style="display: block";> {{ json_decode($key['fn'])->original_filename }}</a>
											@endif
										@endforeach
									@endif
								</td>
                            @endif
                            @if($mocDetails["company"] != 'RBC-CORP' && ! in_array($mocDetails['status'],["FOR ENDORSEMENTS","NEW"]))
								<td>
									@if($mocDetails["attachments"])
										@foreach($mocDetails["attachments"] as $key)
											@if($key['attachment_type'] == 2)
												<a style="display: block";> {{ json_decode($key['fn'])->original_filename }}</a>
											@endif
										@endforeach
									@endif
								</td>
                            @endif
							</tr>
                        </table>
                    </div>
                </div>
                @if(count($mocDetails['signatories']) > 0)
                    <div>
                        <div class="container-header"><h5 class="text-center lined"><strong>SIGNATORIES</strong></h5></div>
                        <?php $RFSAcount = 0 ?>
                        <?php $SFRcount = 0 ?>
                        <?php $SFAcount = 0 ?>
                        <table>
                        @foreach($mocDetails['signatories'] as $signatory)
                            @if($mocDetails['company'] == 'RBC-CORP')
                                <tr>
                                    <td>
                                        @if($signatory['signature_type'] == 1)
                                            <label class="labels">
                                                ENDORSED :
                                            </label>
                                        @elseif($signatory['signature_type'] == 2)
                                            <label class="labels">
                                                RECEIVED FOR INITIAL ASSESSMENT :
                                            </label>
                                        @elseif($signatory['signature_type'] == 3)
                                            <label class="labels">
                                                SUBMITTED FOR INITIAL ASSESSMENT :
                                            </label>
                                        @elseif($signatory['signature_type'] == 4)
                                            <label class="labels">
                                                @if($RFSAcount == 0)
                                                    RECEIVED FOR FINAL ASSESSMENT :
                                                @endif
                                            </label>
                                            <?php $RFSAcount++ ?>
                                        @elseif($signatory['signature_type'] == 5)
                                            <label class="labels">
                                                @if($SFRcount == 0)
                                                    SUBMITTED FOR REVIEW :
                                                @endif
                                            </label>
                                            <?php $SFRcount++ ?>
                                        @elseif($signatory['signature_type'] == 6)
                                            <label class="labels">
                                                SUBMITTED FOR APPROVAL :
                                            </label>
                                        @elseif($signatory['signature_type'] == 7)
                                            <label class="labels">
                                                @if($SFAcount == 0)
                                                    SUBMITTED FOR ACKNOWLEDGEMENT :
                                                @endif
                                            </label>
                                            <?php $SFAcount++ ?>
                                        @elseif($signatory['signature_type'] == 8)
                                            <label class="labels">
                                                ACKNOWLEDGED :
                                            </label>
                                        @endif
                                    </td>
                                    <td width="400">
                                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}"class="form-control pull-right"/>
                                    </td>
                                    <td>
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}"class="pull-right form-control placeholders"/>
                                    </td>
                                </tr>
                            @else()
                                <tr>
                                    <td>
                                        @if($signatory['signature_type'] == 1)
                                            <label class="labels">
                                                ENDORSED :
                                            </label>
                                        @elseif($signatory['signature_type'] == 2)
                                            <label class="labels">
                                                SUBMITTED FOR INITIAL ASSESSMENT :
                                            </label>
                                        @elseif($signatory['signature_type'] == 3)
                                            <label class="labels">
                                                SUBMITTED FOR PROCESSING :
                                            </label>
                                        @elseif($signatory['signature_type'] == 4)
                                            <label class="labels">
                                                @if($RFSAcount == 0)
                                                    RECEIVED FOR FINAL ASSESSMENT :
                                                @endif
                                            </label>
                                            <?php $RFSAcount++ ?>
                                        @elseif($signatory['signature_type'] == 5)
                                            <label class="labels">
                                                @if($SFRcount == 0)
                                                    SUBMITTED FOR REVIEW :
                                                @endif
                                            </label>
                                            <?php $SFRcount++ ?>
                                        @elseif($signatory['signature_type'] == 6)
                                            <label class="labels">
                                                SUBMITTED FOR APPROVAL :
                                            </label>
                                        @elseif($signatory['signature_type'] == 7)
                                            <label class="labels">
                                                @if($SFAcount == 0)
                                                    SUBMITTED FOR ACKNOWLEDGEMENT :
                                                @endif
                                            </label>
                                            <?php $SFAcount++ ?>
                                        @elseif($signatory['signature_type'] == 8)
                                            <label class="labels">
                                                ACKNOWLEDGED :
                                            </label>
                                        @endif
                                    </td>
                                    <td>
                                        <input type="text" style="max-width: 300px;" readonly value="{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}" class="form-control text-center"/>
                                    </td>
                                    <td>
                                        <input type="text" readonly value="{{ $signatory['approval_date'] }}" class="pull-right form-control placeholders"/>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                        </table>
                    </div>
                @endif
            </div><!-- end of form_container -->
            <div class="clear_20"></div>
            <span class="action-label labels">ACTION</span>
            <div class="form_container msr-form-container">
                <div style="border : 1px solid #c7c5c7; padding : 10px; margin-top: 20px;">
                    <table>
                        <tr>
                            <td width="100" valign="top">
                                <label class="labels">MESSAGE:</label>
                            </td>
                            <td style="padding-top : 10px;">
                                @foreach($mocDetails['comments'] as $key)<p>{{ $key['employee']['firstname'].' '.$key['employee']['lastname'].' '.date('m/d/Y h:i A', strtotime($key['ts_comment'])).' : '.$key['comment']}} </p>@endforeach
                            </td>
                        </tr>
                    </table>
                </div>
            </div><!-- end of form_container -->
        </div>
        </div>
    </body>
</html>

