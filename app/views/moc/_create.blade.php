@extends('template/header')

@section('content')
<div id="moc_create" v-cloak>
    {{--FOR for message prompt--}}
    <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
        <p>@{{ submitStatus.message }}</p>
    </div>
    <div class="alert alert-info msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
        <p>@{{ submitStatus.message }}</p>
    </div>
    <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
        <ul>
            <li v-for="error in submitStatus.errors">@{{ error }}</li>
        </ul>
    </div>
    {{--END for message prompt--}}
    {{ Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
    <div class="form_container msr-form-container">
        <div class="container-header"><h5 class="text-center"><strong>{{ MOC_FORM_TITLE }}</strong></h5></div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="row employee-details">
            <div class="col-md-6">
                <label class="labels required pull-left">EMPLOYEE NAME:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ Session::get('employee_name') }}" />
            </div>
            <div class="col-md-6 pull-left">
                <label class="labels required">REFERENCE NUMBER:</label>
                <input readonly="readonly" type="text" class="form-control pull-right"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                <input  readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
            </div>
            <div class="col-md-6">
                <label class="labels required pull-left">DATE FILED:</label>
                <input readonly="readonly" type="text" class="form-control pull-right"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">COMPANY:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ Session::get('company') }}" />
            </div>
            <div class="col-md-6">
                <label class="labels required pull-left">STATUS:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="NEW" />
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">DEPARTMENT:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="department" value="{{ Session::get('dept_name') }}" />
            </div>
            <div class="col-md-6">
                <label class="labels">CONTACT NUMBER:</label>
                <input v-model="formDetails.contactNumber"  type="text" class="form-control pull-right" name="contactNumber" maxlength="20" value="{{ Input::old('contactNumber') }}"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels pull-left">SECTION:</label>
                <input v-model="formDetails.section"  type="text" class="form-control pull-right" name="section" maxlength="25" value="{{ Input::old('section', Session::get('sect_name')) }}" />
            </div>
        </div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="row project-requirements">
            <div class="col-md-12">
                <div class="moc-sidebar">
                    <label class="labels required pull-left">TYPE OF REQUEST:</label>
                    <br>
                    <ul class="moc-list list-unstyled">
                        @foreach($requestTypes as $requestType)
                            <li class="text-uppercase"><input type="checkbox" v-model="mocDetails.requestTypes" value="{{ $requestType->item }}"> {{ $requestType->text }}</li>
                        @endforeach
                    </ul>
                    <label class="labels required pull-left">URGENCY:</label>
                    <select v-model="mocDetails.urgency" class="form-control">
                        <option value="immediate">Immediate attention needed</option>
                        <option value="normal">Normal priority</option>
                    </select>
                </div>
                <div class="moc-main">
                    <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                    <div class="moc-inner-sidebar">
                        <label class="labels pull-left">CHANGE IN:</label>
                        <br>
                        <ul class="list-unstyled moc-list">
                            @foreach($changeIn as $change)
                                @if($change->item == "others")
                                    <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox"  v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }} <input :disabled="! enabledForRequest || mocDetails.changeIn.indexOf('others') == -1" style="width : 140px; text-transform: none !important;" v-model="mocDetails.changeInOthersInput" type="text" class="pull-right form-control"></li>
                                @else
                                    <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox" v-model="mocDetails.changeIn" value="{{ $change->item }}"> {{ $change->text }}</li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="moc-inner-main">
                        <div class="moc-inner-sidebar">
                            <label class="labels pull-left">CHANGE TYPE:</label>
                            <select v-model="mocDetails.changeType" class="form-control" :disabled="! enabledForRequest">
                                <option value="permanent">Permanent</option>
                                <option value="temporary">Temporary</option>
                            </select>
                        </div>
                        <div class="moc-inner-main">
                            <label class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input :disabled="! enabledForRequest" v-model="mocDetails.targetDate" type="text" name="" id="UTdate" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <label style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                        <div class="clearfix"></div>
                        <div class="moc-inner-main">
                            <select :disabled="! enabledForRequest" class="form-control" v-model="selectedDepartment.id" name="" id="">
                                <option v-for="department in departments" value="@{{ $index }}">@{{ department.comp_code }} - @{{ department.dept_name }}</option>
                            </select>
                        </div>
                        <div class="moc-inner-sidebar">
                            <button :disabled="! enabledForRequest  || ! selectedDepartment.id" class="btn btn-default btndefault pull-right" @click.prevent="addDeptInvolved">ADD</button>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="moc-whole" style="border : 1px solid #ccc;">
                            <ol class="moc-list">
                                <li v-for="deptInvolved in mocDetails.departmentsInvolved">@{{ deptInvolved.comp_code }} - @{{ deptInvolved.dept_name }}<a @click="removeDeptInvolved($index)"> Remove</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                <input v-model="mocDetails.title" style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS)</label>
                <textarea v-model="mocDetails.backgroundInfo" name="" id="" class="form-control" rows="7"></textarea>
            </div>
        </div>
        <div class="clear_20"></div>
        <div>
            <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-6">
                    <label class="attachment_note"><strong>ADD ATTACHMENTS </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div id="attachments">
                        @if(Input::old("files"))
                            @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                <p>
                                    {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                    <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                    <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                    <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                    <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                    <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                    <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                </p>
                            @endfor
                        @endif
                    </div>
                    <span class="btn btn-success btnbrowse fileinput-button">
                        <span>BROWSE</span>
                        <input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                    </span>
                </div>
            </div>
        </div>
    </div><!-- end of form_container -->
    <div class="clear_20"></div>

    <span class="action-label labels">ACTION</span>
    <div class="form_container msr-form-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 comment-box">
                <label class="labels pull-left">COMMENT:</label>
                <textarea rows="3" class="form-control pull-left" name="comment" v-model="comment"></textarea>
            </div>
            @if(Session::get('company')== 'RBC-CORP')
                @include('moc.actions.create.corp')
            @else
                @include('moc.actions.create.sat')
            @endif
        </div>
    </div><!-- end of form_container -->
</div>

@stop
@section('js_ko')
{{ HTML::script('/assets/js/notification/vue.js') }}
{{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
{{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
{{ HTML::script('/assets/js/moc/vue-create.js') }}
<script>
    $('body').on('click','.remove-fn',function () {
        $(this).closest( "p" ).remove();
    });
    @if(Input::old("files"))
        var file_counter = {{ $i }};
    @else
    var file_counter = 0;
    @endif
    var allowed_file_count = 10;
    var allowed_total_filesize = 20971520;
    $('.date_picker').datepicker({
        dateFormat : 'yy-mm-dd'
    });
    $('body').on('click','.toggleDatePicker',function () {
        $(this).closest( "div" ).find(".date_picker").datepicker("show");
    });
</script>
{{ HTML::script('/assets/js/moc/file-upload.js') }}
@stop
