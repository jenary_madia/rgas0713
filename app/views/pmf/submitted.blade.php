@extends('template/header')

@section('content')

<div id="wrap">
    <!-- @include('template/sidebar') -->
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">

    <label class="form_label pm_form_title">PERFORMACE MANAGEMENT REPORT</label>


    <div class="clear_10"></div>   
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels">DEPARTMENT:</label>
        </div>
        <div class="col2_form_container">
            <select class="department1 form-control"  name="department1" multiple="multiple" >
                @foreach($department as $key => $val)                                      
                <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="clear_10"></div>  

    <div class="" >
        <div class="datatable_holder">
            <table id="approval_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">FOR APPROVAL</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                    </tr>                           
                </thead>

                <tbody>
                    
                @foreach($record as $rs)
                    @if(Session::get("employee_id") == $rs[7] ||  strpos($rs[10] ,  ","  . Session::get("employee_id") . "," ) !== false   )
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] . " " .  (Session::get("employee_id") == $rs[7] ? $rs[6] : "" ) }}</td>
                    <td>{{ $rs[8] }}</td>  
                    </tr>
                    @endif
                @endforeach
                    
                </tbody>
            </table>
            </div>
    </div>

    @if(Session::get("is_pmf_receiver") == 0)
    <div class="clear_10"></div>   
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels">DEPARTMENT:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-dropdown form-control"  name="department" multiple="multiple" >
                @foreach($department as $key => $val)                                      
                <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="clear_10"></div>   
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels">YEAR:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-dropdown form-control"  name="year" multiple="multiple" >
               @foreach($period as $val)                                      
                <option  value="{{ strtoupper($val->year . " (" . $val->period . ")") }}">{{ $val->year . " (" . $val->period . ")" }}</option>
                @endforeach  
            </select>
        </div>
    </div>

    <div class="clear_10"></div>   
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels">STATUS:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-dropdown form-control"  name="status" multiple="multiple" >
               <option value="NEW">NEW</option>
               <option value="FOR APPROVAL">FOR APPROVAL</option>
               <option value="SUBMITTED">SUBMITTED</option>
               <option value="RETURNED">RETURNED</option>
               <option value="CALIBRATED">CALIBRATED</option> 
            </select>
        </div>
    </div>

    <div class="clear_10"></div>   
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels">PURPOSE:</label>
        </div>
        <div class="col2_form_container">
            <select class="form-dropdown form-control"  name="purpose" multiple="multiple" >
               <option value="YEARLY EVALUATION">YEARLY EVALUATION</option> 
               <option value="FOR REGULARIZATION">FOR REGULARIZATION</option> 
               <option value="FOR PROMOTION">FOR PROMOTION</option> 
               <option value="FOR LATERAL TRANSFER">FOR LATERAL TRANSFER</option> 
            </select>
        </div>

        <div class="col4_form_container">
        &nbsp;
           <button type="button" id="filter" class="btn btnsearch btn-default">FILTER</button>
        </div>

    </div>

    <div class="clear_10"></div>   
    <div class="" >
        <div class="datatable_holder">
            <table id="pmf_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">PMF</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                    </tr>                           
                </thead>

                <tbody>
                    
                  @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>
                    <td>{{ $rs[8] }}</td>
                    <td>{{ $rs[9] }}</td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            </div>
    </div>
    @endif
</div>
</div>
@stop
@section('js_ko')


{{ HTML::style('/assets/css/multiselect/bootstrap-multiselect.css') }}
{{ HTML::script('/assets/js/multiselect/bootstrap-multiselect.js') }}
{{ HTML::script('/assets/js/pmf/submitted.js?v1') }}
@stop