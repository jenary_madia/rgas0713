@extends('template/header')

@section('content')

<div id="wrap">
    <!-- @include('template/sidebar') -->
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">

    <label class="form_label pm_form_title">PERFORMACE MANAGEMENT REPORT</label>

    <div class="text-right">
            <a href="{{ url::to("pmf/export") }}" class="btn btn-default"  >GENERATE REPORT</a>
     </div>


    <div class="clear_10"></div>

    <div class="">  
        <div class="datatable_holder">
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="6">MY PMF</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ date("m/d/Y", strtotime($rs[1])) }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ trim($rs[4]) ? $rs[4] : Session::get('employee_name') }}</td>  
                    <td>{{ $rs[5] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  
    </div>

    <div class="clear_20"></div>  

    <div class="" >
        <div class="datatable_holder">
            <table id="sub_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">MY SUBORDINATE'S PMF</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                  @foreach($sub_record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ trim($rs[5]) ? $rs[5] : Session::get('employee_name') }}</td>  
                    <td>{{ $rs[6] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table> 
            </div>
    </div>


    <div class="clear_20"></div>  

    @if(Session::get('desig_level') == "president" || Session::get('desig_level') == "top mngt" )
    <div class="row_form_container">
        <div class="col4_form_container">
            <label class="labels ">DEPARTMENT:</label>
        </div>
        <div class="col2_form_container">
            <select class="department1 form-control"  name="department1" multiple="multiple" >
                @foreach($department as $key => $val)                                      
                <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="clear_10"></div>  
    @endif

    <div class="" >
        <div class="datatable_holder">
            <table id="approval_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row"><th class="text-left" role="columnheader" rowspan="1" colspan="7">FOR APPROVAL</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">EMPLOYEE NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COVERED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>                       
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                    </tr>                           
                </thead>

                <tbody>
                    
                  @foreach($approval_record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>
                    <td>{{ $rs[6] }}</td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            </div>
    </div>

</div>
</div>
@stop
@section('js_ko')

{{ HTML::style('/assets/css/multiselect/bootstrap-multiselect.css') }}
{{ HTML::script('/assets/js/multiselect/bootstrap-multiselect.js') }}
{{ HTML::script('/assets/js/pmf/filer.js') }}
@stop