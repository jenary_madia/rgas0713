@extends('template/header')

@section('content')
        {{ Form::open(array('url' => 'pmf/approval/action/' . $id, 'method' => 'post', 'files' => true)) }}
            <input type='hidden' value='{{ csrf_token() }}' />
            <div class="form_container">
                <label class="form_title ">PERFORMANCE MANAGEMENT FORM</label>
                    <div class="row">
                        <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="employee" value="{{ $record['employee_name'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="reference" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="employeeid" value="{{ $record['employeeid'] }}" />
                            <input readonly="readonly" type="hidden" class="form-control" name="superiorid" value="{{ $record['superiorid'] }}" />
                            <input readonly="readonly" type="hidden" class="form-control" name="email" value="{{ $record['email'] }}" />
                            <input readonly="readonly" type="hidden" class="form-control" name="employee_id" value="{{ $record['employee_id'] }}" />
                            <input readonly="readonly" type="hidden" class="form-control" name="superior" value="{{ $record['superiorid'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">CREATED BY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['created_name'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">POSITION TITLE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="designation" value="{{ $record['designation'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['date_filed'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="company" value="{{ $record['company'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DATE COVERED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input  readonly="readonly" maxlength="20" type="text" class="form-control" name="contact" value="{{ $record['cover'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="dept_name" value="{{ $record['dept_name'] }}" />
                            <input readonly="readonly" type="hidden" class="form-control" name="departmentid2" value="{{ $record['departmentid2'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="20" type="text" class="form-control" name="contact" value="{{ $record['status'] }}" readonly />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" readonly name="sect_name"  value="{{ $record['sect_name'] }}" />
                        </div>
                    </div>

                        <div class="clear_10"></div>
                    </div>


                    <div id="tabs" class="tabs-pmf hide">
                      <ul>
                        <li><a href="#tabs-1">PERFORMANCE OBJECTIVES</a></li>
                        <li><a href="#tabs-2">PERFORMANCE BEHAVIORS</a></li>
                        <li><a href="#tabs-3">SUMMARY</a></li>
                        <li><a href="#tabs-4">ATTACH FILES</a></li>
                      </ul>
                      

                      <div id="tabs-1">


                        <div class="clear_20"></div>
                        <label class="form_label pm_form_title">{{ $build['objectives']['component'] }} ({{ $build['objectives']['weight'] }}%)</label>
                        <div class="clear_10"></div>

                        {{ $build['objectives']['comments'] }}

                        <div class="clear_10"></div>

                        <table cellpadding = "0" class="tbl_pm_questions " border="1">
                            <tr>
                                <th width="150" class="pm_text_header ui-state-default" rowspan="2" >KEY RESULT AREAS (KRA's)</th>
                                <th class="pm_text_header ui-state-default" colspan="3" >KPI</th>
                                <th class="pm_text_header ui-state-default" colspan="3" >ACTUAL PERFORMANCE <br> (TARGET'S ACHIEVED?)</th>
                                <th class="pm_text_header ui-state-default" rowspan="2" >WEIGHT<br><W)</th>
                                <th class="pm_text_header ui-state-default" colspan="2" >RATING</th>
                                <th class="pm_text_header ui-state-default" rowspan="2" >WEIGHTED<br>RATING<br>(W*R)</th>
                                </tr>

                                <tr>
                                <th width="160" class="pm_text_header ui-state-default">GENERAL MEASURE</th>
                                <th class="pm_text_header ui-state-default">SPECIFIC MEASURE</th>
                                <th class="pm_text_header ui-state-default">ATTACHMENT</th>
                                <th class="pm_text_header ui-state-default">YES</th>
                                <th class="pm_text_header ui-state-default">NO</th>
                                <th class="pm_text_header ui-state-default">OTHER COMMENTS<br>(FOR EXCEEDS/ BELOW TARGET)</th>
                                <th class="pm_text_header ui-state-default">SELF</th>
                                <th class="pm_text_header ui-state-default">SUPERIOR<br>(R)</th>
                                </tr>

                            <?php $kpi_count = 0;?>
                            <?php $kpi_detail_count = 1;?>
                            @foreach($kra_cluster as $key)
                            <tr>
                            <td class="pm_tbl_labels">
                                KRA Cluster {{ $key->cluster }} :<br />
                                {{ $key->description }}<br />
                                @if(! $readonly )<button type="buttton" name="expand" class="add-cluster" cluster="{{ $key->cluster_id }}" /><i class="fa fa-plus-circle fa-xs faplus"></i></button>@endif
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>

                            @if(!empty( $record['kras'] ))                        
                            @foreach($record['kras'] as $kras)
                            @if($key->cluster_id == $kras->cluster_id)
                            <tr> 
                            <td colspan='11' class='pm_tbl_labels'> 
                                @if(! $readonly )<button type='buttton' name='expand' class='remove-kra' kpi='{{ $kpi_count++}}' ><i class='fa fa-minus-circle fa-xs faplus' /></i></button>@endif
                                <input type='hidden' name='kpi[]' value='{{ $kpi_count }}' /> 
                                <input type='hidden' name='cluster[]' value='{{  $kras->cluster_id }}' /> 
                                <textarea maxlength="5000" {{ $readonly }} class="kpi-textarea" rows='3' name='kra[]' >{{ $kras->kra }}</textarea> 
                            </td> 
                            </tr> 
                            <tr> 
                            <td></td> 
                            <td>@if(! $readonly )<button type='buttton' name='expand' class='add-kpi' kpi='{{ $kpi_count }}' ><i class='fa fa-plus-circle fa-xs faplus'></i></button>@endif</td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            </tr>

                            @if(!empty( $record['kpi'] ))
                            @foreach($record['kpi'] as $kpi)
                                @if($kpi->kra_id == $kras->kra_id && $kpi->cluster_id == $kras->cluster_id)
                                <tr>
                                <td></td>
                                <td class='pm_tbl_labels'>
                                <input type='hidden' name='kpi_index[]' value='{{ $kpi_detail_count++}}' />
                                @if(! $readonly )<button type='buttton' name='expand' class='remove_kpi remove_kpi_{{ $kpi_count }}'><i class='fa fa-minus-circle fa-xs faplus'></i></button>@endif
                                <?php $general = $kpi->general_measures ? explode(",",$kpi->general_measures) : [];?>
                                <input {{ $readonly }} type='checkbox' {{ in_array("QL",$general) ? "checked" : "" }} value=1 name='kpi_quality_{{ $kpi_count }}[{{ $kpi_detail_count }}]' />
                                Quality (QL)<br>
                                @if(! $readonly )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@endif<input {{ $readonly }} {{ in_array("CE",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_efficient_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                Cost Efficient (CE)<br>
                                @if(! $readonly )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@endif<input {{ $readonly }} {{ in_array("QT",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_quantity_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                Quantity (QL)<br>
                                @if(! $readonly )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@endif<input {{ $readonly }} {{ in_array("T",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_timeliness_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                Timeliness (T) 
                                </td>
                                <td class="pm_tbl_labels" ><textarea maxlength="5000" {{ $readonly }}  rows='3' class='kpi-textarea kpi_specific_{{ $kpi_count }}' name='kpi_specific_{{ $kpi_count }}[{{ $kpi_detail_count }}]' >{{ $kpi->specific_measures}}</textarea></td>
                                <td class='pm_tbl_labels' >
                                    @if(! $readonly )
                                    <span class='btn btn-default btn-sm fileinput-button'>
                                    <span >BROWSE</span>
                                        <input class='fileupload-kpi' kpi='{{ $kpi_count }}' index='{{ $kpi_detail_count }}' type='file' name='attachments[]' data-url='{{ route('file-uploader.store') }}' multiple />
                                    </span>
                                    @endif
                                    <div id='attachments-kpi_{{ $kpi_count }}_{{ $kpi_detail_count }}'>
                                        @if( $kpi->attachments )
                                        <?php $count = 1000;?>
                                        @foreach(json_decode( $kpi->attachments ) as $attach)
                                        <p>
                                        <input type='hidden' name='files_{{ $kpi_count }}[{{ $kpi_detail_count }}][{{ $count }}][filesize]' value='{{ $attach->filesize }}' />
                                        <input type='hidden' name='files_{{ $kpi_count }}[{{ $kpi_detail_count }}][{{ $count }}][mime_type]' value='{{ $attach->mime_type }}' />
                                        <input type='hidden' name='files_{{ $kpi_count }}[{{ $kpi_detail_count }}][{{ $count }}][original_extension]' value='{{ $attach->original_extension }}' />
                                        <input type='hidden' name='files_{{ $kpi_count }}[{{ $kpi_detail_count }}][{{ $count }}][original_filename]' value='{{ $attach->original_filename }}' />
                                        <input type='hidden' name='files_{{ $kpi_count }}[{{ $kpi_detail_count }}][{{ $count }}][random_filename]' value='{{ $attach->random_filename }}' />
                                        <?php $count++ ;?>
                                        <a href="{{ URL::to('/pmf/download/') . '/' . $record['reference_no'] . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attach->original_filename) }}">{{ $attach->original_filename }}</a>
                                        @if(! $readonly )<button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}"   >DELETE</button>@endif<br /></p>
                                        @endforeach 
                                        @endif  
                                    </div>
                                </td>
                                <td align='center'><input {{ $readonly }} {{ $kpi->achieved_actual_performances == 1 ? "checked" : "" }} type='radio' value=1 name='kpi_target_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  /></td>
                                <td align='center'><input {{ $readonly }} {{ $kpi->achieved_actual_performances == 0 ? "checked" : "" }} type='radio' value=0 name='kpi_target_{{ $kpi_count }}[{{ $kpi_detail_count }}]' /></td>
                                <td class="pm_tbl_labels" ><textarea maxlength="5000" {{ $readonly }} class="kpi-textarea" rows='3' name='kpi_other_{{ $kpi_count }}[{{ $kpi_detail_count }}]' >{{ $kpi->actual_performances }}</textarea></td>
                                <td align='center'><input {{ $readonly }} value="{{ $kpi->weights > 0 ? $kpi->weights : "" }}" type='text' name='kpi_weight_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-weight isDecimal' /></td>
                                <td align='center'><input {{ $readonly }} value="{{ $kpi->self_rating > 0? $kpi->self_rating : "" }}" type='text' name='kpi_self_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class="isDecimal" /></td>
                                <td align='center'><input {{ $readonly }} value="{{ $kpi->rating  > 0? $kpi->rating : "" }}" type='text' name='kpi_superior_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-superior isDecimal' /></td>
                                <td align='center'><input {{ $readonly }} value="{{ $kpi->weighted  > 0 ? $kpi->weighted : "" }}" readonly type='text' name='kpi_weighted_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-weighted' /></td>
                                </tr>
                                @endif
                            @endforeach
                            @endif

                            @endif
                            @endforeach
                            @endif


                            @endforeach

                            
                            <tr>
                            <td colspan="12">
                                <p class="pmf-total_weigh td_pm_style">Total Weigh: <input {{ $readonly }} readonly type="textbox" id="total-weight" name="total-weight" value="{{ $record["total"]["objectives"]["total"] }}" > &nbsp;&nbsp;</p> 
                                <p class="pmf-overall_rate_po td_pm_style">Overall Rating for Performance Objectives: <input {{ $readonly }} readonly type="textbox" id="total-weighted" name="total-weighted" value="{{ $record["total"]["objectives"]["score"] }}" > &nbsp;&nbsp;</p> 
                            </td>
                            </tr>
                        </table>

                        <div class="clear_10"></div>

                      </div>

                      <div id="tabs-2">

                        <div class="clear_20"></div>    
                        <label class="form_label pm_form_title">{{ $build['PERFORMANCE BEHAVIORS']['component'] }} ({{ $build['PERFORMANCE BEHAVIORS']['weight'] }}%)</label>
                        <div class="pcv th_style th_left" style="">{{ $build['core_values']['component'] }} ({{ $build['core_values']['weight'] }}%)</div>
                        <div class="pcv th_style th_left" style="">
                            {{ $build['core_values']['comments'] }}
                        </div>
                        <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                            <tr>
                            <th class="th_style pm_text_header th_left ui-state-default" width="100">CORE VALUES</th>
                            <th class="th_style pm_text_header th_left ui-state-default" colspan="2"l>CRITICAL INCIDENT (REQUIRED)</th>
                            <th class="th_style pm_text_header ui-state-default"  width="200">KEY ACTIONS OR BEHAVIORS</th>
                            <th class="th_style pm_text_header ui-state-default" width="100">CONSISTENTLY<br>OBSERVED</th>
                            <th class="th_style pm_text_header ui-state-default" width="100">OCCASIONALLY<br>OBSERVED</th>
                            <th class="th_style pm_text_header ui-state-default" width="100">NOT OBSERVED</th>
                            </tr>

                            @foreach($behavior['parent'] as $index=>$core)
                            <?php $core_count = 0;?>
                            @foreach($behavior['detail'][$index] as $did => $detail)
                            <tr>
                                @if($core_count == 0)
                                <td class="td_style text-center" rowspan="{{ count($behavior['detail'][$index]) }}">{{ $core }}</td>
                                <td class="td_style" rowspan="{{ count($behavior['detail'][$index]) }}">{{ isset($behavior['inc'][$index]) ? $behavior['inc'][$index] : "" }}</td>
                                <td class="td_style" rowspan="{{ count($behavior['detail'][$index]) }}" align="center" width="130" ><textarea maxlength="5000" {{ $readonly }} class="incident" name="incident[{{ $did }}]" rows="10" cols="15">{{ isset($record["core"][$did]["incident"]) ? $record["core"][$did]["incident"] : "" }}</textarea></td>
                                <?php $core_count = 1;?>
                                @endif

                                <td class="">
                                    <div class="td_pm_style">{{ $detail }}</div>
                                </td>
                                <td class="text-center"><input {{ $readonly }} {{ $record['employeeid'] != Session::get('employeeid') ? ""  : "disabled" }} type="radio" name="core-rate[{{ $did }}]" id="{{ $did }}"  {{ isset($record["core"][$did]["rating"]) && $record["core"][$did]["rating"] == 2 ? "checked" : "" }} value="2"  class="rating-consistent" /></td>
                                <td class="text-center"><input {{ $readonly }} {{ $record['employeeid'] != Session::get('employeeid') ? ""  : "disabled" }} type="radio" name="core-rate[{{ $did }}]" id="{{ $did }}"  {{ isset($record["core"][$did]["rating"]) && $record["core"][$did]["rating"] == 1 ? "checked" : "" }} value="1" class="rating-occasional" /></td>
                                <td class="text-center"><input {{ $readonly }} {{ $record['employeeid'] != Session::get('employeeid') ? ""  : "disabled" }} type="radio" name="core-rate[{{ $did }}]" id="{{ $did }}"  {{ isset($record["core"][$did]["rating"]) && $record["core"][$did]["rating"] == 0 ? "checked" : "" }} value="0" class="rating-not" /></td>
                            </tr>
                            @endforeach
                            @endforeach
                           
                            <tr>
                                <th class="text-right td_pm_style" colspan="4">RATING (%):</th>
                                <td class="text-center td_pm_style" colspan="3"><input readonly type="text" id="behavior-rating" name="behavior-rating" value="{{ $record["total"]["core_values"]["total"] }}" /></td>
                            </tr>
                            <tr>
                                <th class="text-right td_pm_style" colspan="4">QUALITATIVE RATING:</th>
                                <?php
                                    if($record["total"]["core_values"]["total"] >= 95)
                                        $rate = "ROLE MODELLING";
                                    else if($record["total"]["core_values"]["total"] >= 85)
                                        $rate = "DEMONSTRATING";
                                    else
                                        $rate = "PROGRESSING";
                                ?>
                                <td class="text-center td_pm_style" colspan="3"><input readonly type="text"  id="behavior-qualitative"  value="{{ $rate }}" /></th>
                            </tr>
                            <tr>
                                <th class="text-right td_pm_style" colspan="4">SCORE (10%):</th>
                                <td class="text-center td_pm_style" colspan="3"><input readonly type="text"  id="behavior-score" name="behavior-score"  value="{{ $record["total"]["core_values"]["score"] }}" /></th>
                            </tr>                       
                        </table>
                        
                        <div class="clear_20"></div>
                        <div class="clear_20"></div>
                        <table border = "1" cellpadding = "0" class="tbl_pm_questions" style="width:600px" >
                            <tr>
                                <th class="th_style  text-center pm_text_header ui-state-default" colspan="3" >LEGEND</th>
                            </tr>
                            <tr>
                                <th class="th_style  text-center pm_text_header"  >RATING PERCENTAGE(%)</th>
                                <th class="th_style  text-center pm_text_header" colspan="2" >QUALITATIVE RATING</th>
                            </tr>

                            <tr>
                                <th class="th_style text-center pm_text_header" width="150">95 - 100</th>
                                <tH class="th_style text-center pm_text_header" width="120">ROLE MODELLING</tH>
                                <td class="td_style">The employee consistently displays the leadership practices and fully lives up to
                                the values of the organization. He/she inspires others to demonstrate the appropriate behaviors/practices
                                aligned with the core values</td>
                            </tr>

                            <tr>
                                <th class="th_style text-center pm_text_header">85 - 94</th>
                                <tH class="th_style text-center pm_text_header">DEMONSTRATING</tH>
                                <td class="td_style">The employee display most of the leadership practices most of the time. He/she recognizes
                                the appropriate behaviors to be displayed and tries to live  up other values.</td>
                            </tr>

                            <tr>
                                <th class="th_style text-center pm_text_header">BELOW 85</th>
                                <tH class="th_style text-center pm_text_header">PROGRESSING</tH>
                                <td class="td_style">The employee fails to consistently demonstrate the leadership practices. He/she needs 
                                regular coaching and guidance to align behaviors with the organization's core values.</td>
                            </tr>
                        </table>
                        
                        <div class="clear_20"></div>
                        <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                            <th class="th_style th_left ui-state-default">{{ $build['discipline']['component'] }} ({{ $build['discipline']['weight'] }}%)</th>
                            <th class="th_style ui-state-default">Weight</th>
                            <th class="th_style ui-state-default">Demerit</th>
                            <th class="th_style ui-state-default">Score</th>
                            <tr>
                                <td class="td_style">
                                    {{ $build['discipline']['comments'] }}
                                </td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="5" readonly id="discipline-weight" ></td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="{{ $record["total"]["discipline"]["total"] }}" class="isDecimal" id="discipline-demerit" name="discipline-demerit" ></td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="{{ $record["total"]["discipline"]["score"] }}" readonly id="discipline-score" name="discipline-score" ></td>
                            </tr>
                        </table>

                        <div class="clear_20"></div>
                        <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                            <th class="th_style th_left ui-state-default">{{ $build['policies_and_procedures']['component'] }} ({{ $build['policies_and_procedures']['weight'] }}%)</th>
                            <th class="th_style ui-state-default">Weight</th>
                            <th class="th_style ui-state-default">Rating</th>
                            <th class="th_style ui-state-default">Score</th>
                            <tr>
                                <td class="td_style">
                                 {{ $build['policies_and_procedures']['comments'] }}
                                </td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="5"  readonly id="compliance-weight"  ></td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="{{ $record["total"]["policies_and_procedures"]["total"] }}" class="isDecimal" id="compliance-rating" name="compliance-rating" ></td>
                                <td class="td_style"><input {{ $readonly }} type="textbox" value="{{ $record["total"]["policies_and_procedures"]["score"] }}" readonly id="compliance-score" name="compliance-score" ></td>
                            </tr>
                        </table>
                        <div class="pcv_footer  th_left">                    
                            <div class="td_pm_style th_style">
                                <p class="pmf-overall_rate text-right ">OVERALL RATING FOR PERFORMANCE BEHAVIORS: 
                                <input disabled="" readonly="" type="textbox" class="" id="total-behavior" value="0">
                                &nbsp;&nbsp;&nbsp;</p> 
                            </div>  
                        </div>
                        <div class="clear_20"></div>

                      </div>

                      <div id="tabs-3">

                        <label class="form_label pm_form_title">SUMMARY OF PERFORMANCE</label>
                    
                        <div class="pm_form_container">
                            <table border = "1" cellpadding = "0" class="">
                                <th class="th_style">Component</th>
                                <th class="th_style" colspan="2">Score</th>                     
                                <tr>
                                    <td class="td_style pm_tbl_labels">{{ $build['objectives']['component'] }} ({{ $build['objectives']['weight'] }}%)</td>
                                    <td></td>
                                    <td class="td_style pm_po" class=""><input {{ $readonly }} readonly type="textbox" class="pm_po" id="overall-objective" name="" value="{{ $record["total"]["objectives"]["score"] }}" ></td>
                                </tr>
                                <tr>
                                    <td class="td_style pm_tbl_labels">{{ $build['PERFORMANCE BEHAVIORS']['component'] }} ({{ $build['PERFORMANCE BEHAVIORS']['weight'] }}%)</td>
                                    <td></td>
                                    <td class="td_style pm_po" class=""><input {{ $readonly }} readonly type="textbox" class="pm_po" id="overall-behavior" name="" value="{{ $record["total"]["core_values"]["score"] + $record["total"]["discipline"]["score"]  + $record["total"]["policies_and_procedures"]["score"]  }}" ></td>
                                </tr>
                                <tr>
                                    <td class="td_style pm_tbl_labels">{{ $build['core_values']['component'] }} ({{ $build['core_values']['weight'] }}%)</td>
                                    <td class="td_style pm_po" class=""><input {{ $readonly }} readonly type="textbox" class="pm_po" id="practice-core" name="" value="{{ $record["total"]["core_values"]["score"] }}" ></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="td_style pm_tbl_labels">{{ $build['discipline']['component'] }} ({{ $build['discipline']['weight'] }}%)</td>
                                    <td class="td_style pm_po" class=""><input {{ $readonly }} {{ $readonly }} readonly type="textbox" class="pm_po" id="summary-discipline" name="" value="{{ $record["total"]["discipline"]["score"] }}"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="td_style pm_tbl_labels">{{ $build['policies_and_procedures']['component'] }} ({{ $build['policies_and_procedures']['weight'] }}%)</td>
                                    <td class="td_style pm_po" class=""><input {{ $readonly }} readonly type="textbox" class="pm_po" id="summary-compliance" name="" value="{{ $record["total"]["policies_and_procedures"]["score"] }}"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right td_pm_style"  colspan="2"><b>TOTAL SCORE:</b> </td>
                                    <td id="total-score" class="td_pm_style">{{ $record["total"]["core_values"]["score"] + $record["total"]["discipline"]["score"]  + $record["total"]["policies_and_procedures"]["score"] + $record["total"]["objectives"]["score"]  }}</td>
                                </tr>

                                <tr>
                                    <td class="text-right td_pm_style" colspan="2"><b>EQUIVALENT RATING:</b> </td>
                                    <?php
                                    $tots = $record["total"]["core_values"]["score"] + $record["total"]["discipline"]["score"]  + $record["total"]["policies_and_procedures"]["score"] + $record["total"]["objectives"]["score"];
                                    if($tots >= 100.01)
                                        $rate = "A+";
                                    else if($tots >= 95)
                                        $rate = "A";
                                    else if($tots >= 90)
                                        $rate = "B+";
                                    else if($tots >= 85)
                                        $rate = "B";
                                    else if($tots >= 80)
                                        $rate = "C+";
                                    else if($tots >= 75)
                                        $rate = "C";
                                    else
                                        $rate = "D";
                                    ?>
                                    <td class="td_style pm_po td_pm_style" class=""><input {{ $readonly }} readonly type="textbox" class="pm_po" id="equivalent-rating" name="classification" value="{{ $rate }}" ></td>
                                </tr>

                            </table>
                            <div class="clear_20"></div>
                        </div>
                        
                        <div class="pm1_form_container">
                            <table border = "1" cellpadding = "0" class="">
                                <div class="border th_style rc">Rating Classifications</div>
                                <tr>
                                    <td class="td_style text-center">A+</td>
                                    <td class="td_style text-center">100.01% - 120.00%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">A</td>
                                    <td class="td_style text-center">95.00% - 100.00%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">B+</td>
                                    <td class="td_style text-center">90.00% - 94.99%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">B</td>
                                    <td class="td_style text-center">85.00% - 89.99%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">C+</td>
                                    <td class="td_style text-center">80.00% - 84.99%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">C</td>
                                    <td class="td_style text-center">75.00% - 79.99%</td>
                                </tr>
                                <tr>
                                    <td class="td_style text-center">D</td>
                                    <td class="td_style text-center">0.00% - 74.99%</td>
                                </tr>
                            </table>
                            <div class="clear_20"></div>
                        </div>

                        <div class="clear_10"></div>

                      </div>

                      <div id="tabs-4">

                        <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div class="attachment_container">
                                @if(! $readonly )
                                <span class="btn btn-success btnbrowse fileinput-button">
                                    <span >BROWSE</span>
                                    <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                </span>
                                @endif
                               @if($record['attachments'])
                                <?php $count = 1000;?>
                                @foreach(json_decode( $record['attachments'] ) as $attach)
                                <p>
                                <input type='hidden' name='files[{{ $count }}][filesize]' value='{{ $attach->filesize }}' />
                                <input type='hidden' name='files[{{ $count }}][mime_type]' value='{{ $attach->mime_type }}' />
                                <input type='hidden' name='files[{{ $count }}][original_extension]' value='{{ $attach->original_extension }}' />
                                <input type='hidden' name='files[{{ $count }}][original_filename]' value='{{ $attach->original_filename }}' />
                                <input type='hidden' name='files[{{ $count }}][random_filename]' value='{{ $attach->random_filename }}' />
                                <?php $count++ ;?>
                                <a href="{{ URL::to('/pmf/download/') . '/' . $record['reference_no'] . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attach->original_filename) }}">{{ $attach->original_filename }}</a>
                                <input class='attachment-filesize' type='hidden' value='{{ $attach->filesize }}' />
                                @if(! $readonly )<button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}"   >DELETE</button>@endif<br /></p>
                                @endforeach 
                                @endif   
                                
                                <div id="attachments"></div>                     
                        </div>   

                      </div>

                    </div>


                    
					
					
					
 

               @if(!in_array(Session::get('desig_level'), ["employee","supervisor"] ) && $record['employeeid'] != Session::get('employeeid') )
                <div class="clear_20"></div>
              <div class="container-header"><h5 class="text-center lined sub-header "><strong>RECOMMENDATION</strong></h5></div>
                
                <div class="clear_10"></div>
 
                @foreach($reccomendation as $rs)
                <input type="hidden" name="reccomendation[]" value="{{ $rs->recommendation_id }}" />
                @if($rs->type == "date")
                <div class="clear_10"></div>
                <div class="row3_form_container">
                        <div class="col21_form_container">
                            <input class="recommended" {{ $readonly }} type="checkbox" {{ isset($record["reccomendation"][$rs->recommendation_id]["approved"]) && $record["reccomendation"][$rs->recommendation_id]["approved"]  ? "checked" : "" }} value="1" name="check_{{ $rs->recommendation_id }}" />
                            <label class="labels">{{ $rs->recommendation }}</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input {{ isset($record["reccomendation"][$rs->recommendation_id]["approved"]) && $record["reccomendation"][$rs->recommendation_id]["approved"] ? "" : "disabled" }} {{ $readonly }} value="{{ isset($record["reccomendation"][$rs->recommendation_id]["recommendation"]) ? $record["reccomendation"][$rs->recommendation_id]["recommendation"] : "" }}" type="text" class="date_picker form-control" name="recomend_{{ $rs->recommendation_id }}"  id="recomend_{{ $rs->recommendation_id }}" />
     
                                <label class="input-group-addon btn" for="recomend_{{ $rs->recommendation_id }}">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>  
                            </div>
                </div>
                @else

                <div class="clear_10"></div>
                <div class="row3_form_container">
                        <div class="col21_form_container">
                        <input class="recommended" {{ $readonly }} type="checkbox" {{ isset($record["reccomendation"][$rs->recommendation_id]["approved"]) && $record["reccomendation"][$rs->recommendation_id]["approved"] ? "checked" : "" }} value="1"  name="check_{{ $rs->recommendation_id }}" />
                            <label class="labels">{{ $rs->recommendation }}</label>
                        </div>
                        <div class="row_form_container">
                            <input {{ isset($record["reccomendation"][$rs->recommendation_id]["approved"]) && $record["reccomendation"][$rs->recommendation_id]["approved"]  ? "" : "disabled" }} {{ $readonly }} type="text" value="{{ isset($record["reccomendation"][$rs->recommendation_id]["recommendation"]) ? $record["reccomendation"][$rs->recommendation_id]["recommendation"] : "" }}"  name="recomend_{{ $rs->recommendation_id }}"  class="form-control" maxlength="200">
                        </div>
                </div>
                @endif
                @endforeach
                @endif

					
	            <div class="clear_20"></div>
                <div class="textarea_messages_container" style="padding-top:0">
                    <div class="comment_area">
                    Employees's Comments / Suggestions
                    </div>
                    <div class="row">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <textarea maxlength="5000" {{ $readonly }} rows="3"  cols="120" name="employee_comment" {{ Session::get('desig_level') == "employee" ? "" : "readonly" }} >{{ $record["comment"]["employee"]["comment"]  }}</textarea>   
                        <input readonly="readonly" type="hidden" class="form-control" name="employee_comment_id"  value="{{ Session::get('desig_level') == "employee" ?  Session::get('employee_id') : $record["comment"]["employee"]["employee_id"]   }}" />                      
                    </div>
                    <div class="clear_10"></div>
                    <table border="1" cellspacing="1" class="comment_table">
                        <tr>
                        <td width="30%">Acknowledged:</td>
                        <td>{{ $record["comment"]["employee"]["name"]  }}</td>
                        </tr>
                    </table>

                    @if(in_array(Session::get('desig_level') ,["supervisor","head","top mngt","president"]))
                    <div class="clear_20"></div>
                    <div class="comment_area">
                    Immediate Superior's Comments / Suggestions
                    </div>
                    <i class="td_style">Describe the employee's overall performance. Please note down any other significant work contributions
                    of the employee that have not been included as part of the Performance Objectives. Indicate other relevant comments.
                    </i>
                    <div class="clear_10"></div>
                    <div class="row">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <textarea maxlength="5000" {{ $readonly }} {{ Session::get('desig_level') == "supervisor" ? "" : "readonly" }} rows="3"  cols="120" name="supervisor_comment">{{ $record["comment"]["supervisor"]["comment"]  }}</textarea>                        
                        <input readonly="readonly" type="hidden" class="form-control" name="supervisor_comment_id"  value="{{ Session::get('desig_level') == "supervisor" ? Session::get('employee_id') : $record["comment"]["supervisor"]["employee_id"] }}" />
                    </div>
                    <div class="clear_10"></div>
                    <table border="1" cellspacing="1" class="comment_table">
                        <tr>
                        <td width="30%">Noted:</td>
                        <td>{{ Session::get('desig_level') == "supervisor" ? Session::get('employee_name') : $record["comment"]["supervisor"]["name"] }}</td>
                        </tr>
                    </table>
                    @endif

                    @if(Session::get('desig_level') == "head" || Session::get('desig_level') == "top mngt" || Session::get('desig_level') == "president" )
                    <div class="clear_20"></div>
                    <div class="comment_area">
                    Department Head's Comments / Suggestions
                    </div>
                    <i class="td_style">This portion to be filled-in if employee is candidate for regularization , promotion , or transfer.
                    </i>
                    <div class="clear_10"></div>
                    <div class="row">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <textarea  maxlength="5000" {{ $readonly }} {{ Session::get('desig_level') == "head" ? "" : "readonly" }} rows="3"  cols="120" name="head_comment">{{ $record["comment"]["head"]["comment"]  }}</textarea>        
                        <input readonly="readonly" type="hidden" class="form-control" name="head_comment_id"  value="{{ Session::get('desig_level') == "head" ? Session::get('employee_id') : $record["comment"]["head"]["employee_id"]  }}" />                
                    </div>
                    <div class="clear_10"></div>
                    <table border="1" cellspacing="1" class="comment_table">
                        <tr>
                        <td width="30%">Reviewed:</td>
                        <td>{{ Session::get('desig_level') == "head" ? Session::get('employee_name') : $record["comment"]["head"]["name"] }}</td>
                        </tr>
                    </table>
                    @endif

                    @if(Session::get('desig_level') == "top mngt" || Session::get('desig_level') == "president" )
                    <div class="clear_20"></div>
                    <div class="comment_area">
                    Division Head's Comments / Suggestions
                    </div>
                    <i class="td_style">This portion to be filled-in if employee is candidate for regularization , promotion , or transfer.
                    </i>
                    <div class="clear_10"></div>
                    <div class="row">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <textarea maxlength="5000" {{ $readonly }} rows="3" {{ Session::get('desig_level') == "top mngt" ? "" : "readonly" }} cols="120" name="mngt_comment">{{ $record["comment"]["top-mngt"]["comment"]  }}</textarea>   
                        <input readonly="readonly" type="hidden" class="form-control" name="mngt_comment_id"  value="{{ Session::get('desig_level') == "top mngt" ? Session::get('employee_id') : $record["comment"]["top-mngt"]["employee_id"] }}" />                      
                    </div>
                    <div class="clear_10"></div>
                    <table border="1" cellspacing="1" class="comment_table">
                        <tr>
                        <td width="30%">Reviewed:</td>
                        <td>{{ Session::get('desig_level') == "top mngt" ? Session::get('employee_name') : $record["comment"]["top-mngt"]["name"] }}</td>
                        </tr>
                    </table>
                    @endif
                </div>

            </div><!-- end of form_container -->

            <input type="hidden" id="date-url" value="{{ route('file-uploader.store') }}" />

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">MESSAGE:</label>
                        <textarea maxlength="5000" rows="3" readonly class="form-control textarea_inside_width" name="message">{{ $record["comment"]["general"]["comment"]  }}</textarea>
                    </div>
                    <div class="clear_10"></div> 
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea  maxlength="5000" {{ $readonly }} rows="3" class="form-control textarea_inside_width" name="comment">{{ isset($record["comment"]["saved"]["comment"]) ? $record["comment"]["saved"]["comment"] : ""  }}</textarea>
                    </div>
                </div>

                    <div class="clear_10"></div>
                    <div class="row">
                    @if(! $readonly )

                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault button-witdh" name="action" value="save">SAVE</button>
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes"> 
                            <label class="button_notes"><strong>RETURN</strong> FOR REVISION</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault button-witdh" name="action" value="return">RETURN</button>
                        </div>
                    </div>

                    @if(Session::get('desig_level') == "head")
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes"> 
                            <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO DIVISION HEAD FOR APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault button-witdh" name="action" value="send">SEND</button>
                        </div>
                    </div>
                    @endif

                    @if(Session::get('desig_level') == "top mngt")
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes"> 
                            <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO SUPERIOR</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault button-witdh" name="action" value="send">APPROVE</button>
                        </div>
                    </div>
                    @endif

                    @if(Session::get('desig_level') == "president")
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes"> 
                            <label class="button_notes"><strong>APPROVE</strong> AND <strong>SEND</strong> TO CHRD-OD FOR FINAL APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault button-witdh" name="action" value="hr">APPROVE</button>
                        </div>
                    </div>
                    @endif


                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes"> 
                            <label class="button_notes">&nbsp;</label>
                        </div> 
                        <div class="comment_button">
                            <button type="button" class="btn btn-default btndefault  button-witdh"  value="print" id="print" >PRINT</button>
                        </div>
                    </div>
                @endif

					<div class="clear_10"></div>
                    <div class="row text-center">
                        <button type="button" class="btn btn-default btndefault" onclick="history.go(-1)">BACK</button>  
                        @if(! $readonly )
                        @if(Session::get('desig_level') == "supervisor")
                        <button value="send"  name="action" type="submit" class="btn btn-default btndefault-size" >SEND TO SUPERIOR</button>  
                        <button value="head"  name="action" type="submit" class="btn btn-default btndefault-size" >SEND TO DEPT HEAD</button>  
                        @endif
                        <button value="hr"  name="action" type="submit" class="btn btn-default btndefault-size" >SEND TO HR</button>                    
                        @endif
                    </div>

                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>
var max_filesize = 0;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
{{ HTML::script('/assets/js/pmf/create.js') }}
<script type="text/javascript">
$(function()
{
    $("#print").click(function()
    {
         window.location = $("#base").val() + "/pmf/print/{{ $id }}";
         return false;
    });
    
})
</script>
@stop