@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'pmf/export/action', 'method' => 'post')) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">PERFORMANCE MANAGEMENT FORM</label>
                <div class="row">
                    <div class="clear_20"></div>
                    <div class=""> 
                            <div class="text-right ">
                                <a href="{{ url::to("pmf/report") }}" class="btn btn-default" >GO TO LIST</a>&nbsp;&nbsp;
                                <a href="{{ url::to("os/food/report") }}" class="btn btn-default" >CLOSE</a>&nbsp;&nbsp;
                            </div>
                    </div>

                   <div class="clear_10"></div>
                    <div class="row_form_container"  >
                        <div class="col1_form_container">
                            <label class="labels text-danger">START DATE:</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input required type="text" class="date_picker form-control" name="date_from" id="date_from" />
                                <label class="input-group-addon btn" for="date_from">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>  
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container"  >
                        <div class="col1_form_container">
                            <label class="labels text-danger">END DATE:</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input required type="text" class="date_picker form-control" name="date_to" id="date_to" />
                                <label class="input-group-addon btn" for="date_to">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container" >
                        <div class="col1_form_container">
                            <label class="labels text-danger">PURPOSE:</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="checkbox"  name="purpose[]" value="Yearly Evaluation" /> YEARLY EVALUATION                              
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container"   >
                        <div class="col1_form_container">
                            <label class="labels text-danger">&nbsp;</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="checkbox"  name="purpose[]" value="For Regularization" /> FOR REGULARIZATION                              
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container"  >
                        <div class="col1_form_container">
                            <label class="labels text-danger">&nbsp;</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="checkbox"  name="purpose[]" value="For Promotion"  /> FOR PROMOTION                              
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container" >
                        <div class="col1_form_container">
                            <label class="labels text-danger">&nbsp;</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="checkbox"  name="purpose[]" value="For Lateral Transfer" /> FOR LATERAL TRANSFER                              
                        </div>
                    </div>
		

                    <div class="clear_20"></div>
                  	<div class="row_form_container"> 
		                    <div class="col1_form_container ">
		                        <button type="submit" class="btn btn-default btndefault" name="action" value="export">EXPORT</button>
		            </div>
                           
                                   
                </div>
            

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        
    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/pmf/export.js') }}
<script>
$(function()
{
	"use strict";
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

})
</script>
@stop