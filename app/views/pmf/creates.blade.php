@extends('template/header')

@section('content')

        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">CREATE PMF</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">YEAR COVERED:</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="form-control" name="year" id="year-covered">
                                <option value="" ></option>
                                @foreach($period as $val)                                      
                                <option  value="{{ $val->pmf_id }}">{{ $val->year . "(" . $val->period . ")" }}</option>
                                @endforeach                               
                            </select>
                        </div>
                    </div>
                    
                    @if( Session::get("desig_level") != "president" )
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">FOR:</label>
                        </div>
                        <div class="col2_form_container labels">
                            <input type="radio"  name="for" checked  value="my" /> MY PMF
                        </div>
                    </div>
                    @endif

                    @if( Session::get("desig_level") != "employee" )
                    <div class="clear_10"></div>
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels text-danger">  </label>
                        </div>
                        <div class="row10_form_container labels">
                            <input type="radio"  name="for" {{ Session::get("desig_level") == "president" ? "checked" : ""  }} value="sub"  /> MY SUBORDINATE 
                            <select  name="sub"  {{ Session::get("desig_level") == "president" ? "" : "disabled"  }} >
                                <option value="" ></option>
                                @foreach((isset($employee) ? $employee : []) as $index=>$rs)
                                <option value="{{ $index }}" >{{ $rs }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @endif
                
                    <?php $x="";?>
		            @foreach($purpose as $val)                                      
                    <div class="clear_10 purpose hide pmf_{{ $val->pmf_id }} {{ Session::get("desig_level") == "president" ? "" : "hidden" }}"  ></div>
                    <div class="row_form_container purpose hide pmf_{{ $val->pmf_id }} {{ Session::get("desig_level") == "president" ? "" : "hidden" }}" >
                        <div class="col1_form_container">
                            <label class="labels text-danger">@if($x != $val->pmf_id)PURPOSE:@endif</label>
                        </div>
                        <div class="col2_form_container labels">
                            <input type="radio" value="{{ $val->purpose_id }}" purpose="{{ $val->purpose }}" name="purpose"  /> {{ strtoupper($val->purpose) }}
                        </div>
                    </div>
                    <?php $x=$val->pmf_id;?>
                    @endforeach   

                    

                  

                    <div class="clear_10"></div>
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels text-danger">CONTENT:</label>
                        </div>
                        <div class="row10_form_container labels">
                            <input type="radio"   name="encode" checked value="copy" /> CREATE COPY FROM PREVIOUS YEAR
                            @if( Session::get("desig_level") != "employee" )
                            <select  name="copy_sub" {{ Session::get("desig_level") == "president" ? "" : "disabled"  }}  >
                                <option value="" ></option>
                                @foreach((isset($employee) ? $employee : []) as $index=>$rs)
                                <option value="{{ $index }}" >{{ $rs }}</option>
                                @endforeach
                            </select>
                            @endif
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">&nbsp;&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container labels">
                            &nbsp;&nbsp;&nbsp;<input type="radio"  value="2"  name="content" checked /> BOTH KRA AND KPI
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">&nbsp;&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container labels">
                            &nbsp;&nbsp;&nbsp;<input type="radio" value="1" name="content"  /> KRA ONLY
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger"></label>
                        </div>
                        <div class="col2_form_container labels">
                            <input type="radio"   name="encode" value="manual"  /> MANUALLY ENCODE ENTRIES
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger"></label>
                        </div>
                        <div class="col2_form_container labels">
                            <button type="button" class="btn btn-default btndefault" name="action" value="save">CREATE</button>
                        </div>
                    </div>


   
                                   
                </div>
            

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        
@stop
@section('js_ko')
{{ HTML::script('/assets/js/pmf/creates.js?v1') }}
@stop