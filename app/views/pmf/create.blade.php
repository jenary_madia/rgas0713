@extends('template/header')

@section('content')
        {{ Form::open(array('url' => 'pmf/create/action', 'method' => 'post', 'files' => true)) }}
            <input type="hidden" value="{{ csrf_token() }}">
            <input type="hidden" name="pmf" value="{{ $pmf }}">
            <input type="hidden" name="purpose" value="{{ $purpose }}">

            

            
            <div class="form_container">
                <label class="form_title ">PERFORMANCE MANAGEMENT FORM</label>
                    <div class="row">
                        <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="employee" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" />
                            <input type="hidden" value="{{ isset($record['reference_no']) ? $record['reference_no'] : "" }}" name="old_reference_no" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">CREATED BY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value=""/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">POSITION TITLE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('designation') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ date('Y-m-d') }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DATE COVERED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input  readonly="readonly" maxlength="20" type="text" class="form-control" name="contact" value="{{ $cover }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="dept_name" value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="20" type="text" class="form-control" name="contact" value="NEW" readonly  />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels text-danger">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" readonly  value="{{ Session::get('sect_name') }}" />
                        </div>
                    </div>

                        <div class="clear_10"></div>
                    </div>







                    <div id="tabs" class="tabs-pmf hide">
                      <ul>
                        <li><a href="#tabs-1">PERFORMANCE OBJECTIVES</a></li>
                        <li><a href="#tabs-2">PERFORMANCE BEHAVIORS</a></li>
                        <li><a href="#tabs-3">SUMMARY</a></li>
                        <li><a href="#tabs-4">ATTACH FILES</a></li>
                      </ul>
                      

                      <div id="tabs-1">
                      
                      <div class="clear_10"></div>
                      <label class="form_label pm_form_title">{{ $build['objectives']['component'] }} ({{ $build['objectives']['weight'] }}%)</label>
                     <div class="clear_10"></div>


                        {{ $build['objectives']['comments'] }}


                            <div class="clear_10"></div>

                            <table cellpadding = "0" class="tbl_pm_questions " border="1">
                                <tr>
                                <th width="150" class="pm_text_header ui-state-default" rowspan="2" >KEY RESULT AREAS (KRA's)</th>
                                <th class="pm_text_header ui-state-default" colspan="3" >KPI</th>
                                <th class="pm_text_header ui-state-default" colspan="3" >ACTUAL PERFORMANCE <br> (TARGET'S ACHIEVED?)</th>
                                <th class="pm_text_header ui-state-default" rowspan="2" >WEIGHT<br><W)</th>
                                <th class="pm_text_header ui-state-default" colspan="2" >RATING</th>
                                <th class="pm_text_header ui-state-default" rowspan="2" >WEIGHTED<br>RATING<br>(W*R)</th>
                                </tr>

                                <tr>
                                <th width="160" class="pm_text_header ui-state-default">GENERAL MEASURE</th>
                                <th class="pm_text_header ui-state-default">SPECIFIC MEASURE</th>
                                <th class="pm_text_header ui-state-default">ATTACHMENT</th>
                                <th class="pm_text_header ui-state-default">YES</th>
                                <th class="pm_text_header ui-state-default">NO</th>
                                <th class="pm_text_header ui-state-default">OTHER COMMENTS<br>(FOR EXCEEDS/ BELOW TARGET)</th>
                                <th class="pm_text_header ui-state-default">SELF</th>
                                <th class="pm_text_header ui-state-default">SUPERIOR<br>(R)</th>
                                </tr>

                                <?php $total_Weigh = array();$total_objective=array();?>
                                <?php $kpi_count = 0;?>
                                <?php $kpi_detail_count = 1;?>
                                @foreach($kra_cluster as $key)
                                <tr>
                                <td class="pm_tbl_labels">
                                    KRA Cluster {{ $key->cluster }} :<br />
                                    {{ $key->description }}<br />
                                    <button type="buttton" name="expand" class="add-cluster" cluster="{{ $key->cluster_id }}" ><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                </tr>

                                
                                @if(!empty( $record['kras'] ))                        
                                @foreach($record['kras'] as $kras)
                                @if($key->cluster == $kras->cluster)
                                <tr> 
                                <td colspan='11' class='pm_tbl_labels'> 
                                    <button type='buttton' name='expand' class='remove-kra' kpi='{{ $kpi_count++}}' ><i class='fa fa-minus-circle fa-xs faplus'></i></button> 
                                    <input type='hidden' name='kpi[]' value='{{ $kpi_count }}' > 
                                    <input type='hidden' name='cluster[]' value='{{  $key->cluster_id }}' > 
                                    <textarea class="kpi-textarea" rows='3' name='kra[]' maxlength="5000" >{{ $kras->kra }}</textarea> 
                                </td> 
                                </tr> 
                                <tr> 
                                <td></td> 
                                <td><button type='buttton' name='expand' class='add-kpi' kpi='{{ $kpi_count }}' ><i class='fa fa-plus-circle fa-xs faplus'></i></button></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                <td></td> 
                                </tr>

                                @if(!empty( $record['kpi'] ))
                                @foreach($record['kpi'] as $kpi)
                                    @if($kpi->kra_id == $kras->kra_id && $kpi->cluster_id == $kras->cluster_id)
                                    <tr>
                                    <td></td>
                                    <td class='pm_tbl_labels'>
                                    <input type='hidden' name='kpi_index[]' value='{{ $kpi_detail_count++}}' />
                                    <button type='buttton' name='expand' class='remove_kpi remove_kpi_{{ $kpi_count }}'><i class='fa fa-minus-circle fa-xs faplus'></i></button>
                                    <?php $general = $kpi->general_measures ? explode(",",$kpi->general_measures) : [];?>
                                    <input type='checkbox' {{ in_array("QL",$general) ? "checked" : "" }} value=1 name='kpi_quality_{{ $kpi_count }}[{{ $kpi_detail_count }}]' />
                                    Quality (QL)<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input {{ in_array("CE",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_efficient_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                    Cost Efficient (CE)<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input {{ in_array("QT",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_quantity_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                    Quantity (QL)<br>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input {{ in_array("T",$general) ? "checked" : "" }} type='checkbox' value=1 name='kpi_timeliness_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  />
                                    Timeliness (T) 
                                    </td>
                                    <td class="pm_tbl_labels" ><textarea rows='3' maxlength='5000' class='kpi-textarea kpi_specific_{{ $kpi_count }}' name='kpi_specific_{{ $kpi_count }}[{{ $kpi_detail_count }}]' >{{ $kpi->specific_measures}}</textarea></td>
                                    <td class='pm_tbl_labels' >
                                        <span class='btn btn-default btn-sm  fileinput-button'>
                                        <span >BROWSE</span>
                                            <input class='fileupload-kpi' kpi='{{ $kpi_count }}' index='{{ $kpi_detail_count }}' type='file' name='attachments[]' data-url='{{ route('file-uploader.store') }}' multiple>
                                        </span>
                                        <div id='attachments-kpi_{{ $kpi_count }}_{{ $kpi_detail_count }}'>

                                        </div>
                                    </td>
                                    <td align='center'><input   type='radio' value=1 name='kpi_target_{{ $kpi_count }}[{{ $kpi_detail_count }}]'  /></td>
                                    <td align='center'><input  type='radio' value=0 name='kpi_target_{{ $kpi_count }}[{{ $kpi_detail_count }}]' /></td>
                                    <td class="pm_tbl_labels" ><textarea class="kpi-textarea" rows='3' maxlength='5000' name='kpi_other_{{ $kpi_count }}[{{ $kpi_detail_count }}]' ></textarea></td>
                                    <td align='center'><input value="" type='text' name='kpi_weight_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-weight isDecimal' /></td>
                                    <td align='center'><input value="" type='text' name='kpi_self_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class="kpi-self isDecimal" /></td>
                                    <td align='center'><input value="" disabled type='text' name='kpi_superior_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-superior isDecimal' /></td>
                                    <td align='center'><input value="" readonly type='text' name='kpi_weighted_{{ $kpi_count }}[{{ $kpi_detail_count }}]' style='width:40px' class='kpi-weighted' /></td>
                                    </tr>
                                    @endif
                                @endforeach
                                @endif

                                @endif
                                @endforeach
                                @endif


                                @endforeach


                                
                                
                                <tr>
                                <td colspan="12">
                                    <p class="pmf-total_weigh td_pm_style">Total Weigh: <input readonly type="textbox" id="total-weight" value="" name="total-weight" ></p> 
                                    <p class="pmf-overall_rate_po td_pm_style">Overall Rating for Performance Objectives: <input readonly value="" type="textbox" id="total-weighted" name="total-weighted" ></p> 
                                </td>
                                </tr>
                            </table>


                      </div>

                      <div id="tabs-2">


                            <div class="clear_10"></div>
                    
                            <div class="clear_20"></div>    
                            <label class="form_label pm_form_title">{{ $build['PERFORMANCE BEHAVIORS']['component'] }} ({{ $build['PERFORMANCE BEHAVIORS']['weight'] }}%)</label>
                            <div class="pcv th_style th_left" style="">{{ $build['core_values']['component'] }} ({{ $build['core_values']['weight'] }}%)</div>
                            <div class="pcv th_style th_left" style="">
                                {{ $build['core_values']['comments'] }}
                            </div>
                            <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                                <tr>
                                <th class="th_style pm_text_header th_left ui-state-default" width="100">CORE VALUES</th>
                                <th class="th_style pm_text_header th_left ui-state-default" colspan="2"l>CRITICAL INCIDENT (REQUIRED)</th>
                                <th class="th_style pm_text_header ui-state-default"  width="200">KEY ACTIONS OR BEHAVIORS</th>
                                <th class="th_style pm_text_header ui-state-default" width="100">CONSISTENTLY<br>OBSERVED</th>
                                <th class="th_style pm_text_header ui-state-default" width="100">OCCASIONALLY<br>OBSERVED</th>
                                <th class="th_style pm_text_header ui-state-default" width="100">NOT OBSERVED</th>
                                </tr>

                                @foreach($behavior['parent'] as $index=>$core)
                                <?php $core_count = 0;?>
                                @foreach($behavior['detail'][$index] as $did => $detail)
                                <tr>
                                    @if($core_count == 0)
                                    <td class="td_style text-center" rowspan="{{ count($behavior['detail'][$index]) }}">{{ $core }}</td>
                                    <td class="td_style" rowspan="{{ count($behavior['detail'][$index]) }}">{{ isset($behavior['inc'][$index]) ? $behavior['inc'][$index] : "" }}</td>
                                    <td class="td_style" rowspan="{{ count($behavior['detail'][$index]) }}" align="center" width="130" ><textarea  maxlength='5000' class="incident" name="incident[{{ $did }}]" rows="10" cols="15"></textarea></td>
                                    <?php $core_count = 1;?>
                                    @endif

                                    <td class="">
                                        <div class="td_pm_style">{{ $detail }}</div>
                                    </td>
                                    <td class="text-center"><input type="radio" disabled name="core-rate[{{ $did }}]" id="{{ $did }}" value="2"  class="rating-consistent" /></td>
                                    <td class="text-center"><input type="radio" disabled name="core-rate[{{ $did }}]" id="{{ $did }}" value="1" class="rating-occasional" /></td>
                                    <td class="text-center"><input type="radio" disabled name="core-rate[{{ $did }}]" id="{{ $did }}" value="0" class="rating-not" /></td>
                                </tr>
                                @endforeach
                                @endforeach
                               
                                <tr>
                                    <th class="text-right td_pm_style" colspan="4">RATING (%):</th>
                                    <td class="text-center td_pm_style" colspan="3"><input readonly type="text" id="behavior-rating" name="behavior-rating" /></td>
                                </tr>
                                <tr>
                                    <th class="text-right td_pm_style" colspan="4">QUALITATIVE RATING:</th>
                                    <td class="text-center td_pm_style" colspan="3"><input readonly type="text"  id="behavior-qualitative" /></th>
                                </tr>
                                <tr>
                                    <th class="text-right td_pm_style" colspan="4">SCORE (10%):</th>
                                    <td class="text-center td_pm_style" colspan="3"><input readonly type="text"  id="behavior-score" name="behavior-score"  /></th>
                                </tr>                       
                            </table>
                            
                            <div class="clear_20"></div>


                            <div class="clear_20"></div>
                            <table border = "1" cellpadding = "0" class="tbl_pm_questions" style="width:600px" >
                                <tr>
                                    <th class="th_style  text-center pm_text_header ui-state-default" colspan="3" >LEGEND</th>
                                </tr>
                                <tr>
                                    <th class="th_style  text-center pm_text_header"  >RATING PERCENTAGE(%)</th>
                                    <th class="th_style  text-center pm_text_header" colspan="2" >QUALITATIVE RATING</th>
                                </tr>

                                <tr>
                                    <th class="th_style text-center pm_text_header" width="150">95 - 100</th>
                                    <tH class="th_style text-center pm_text_header" width="120">ROLE MODELLING</tH>
                                    <td class="td_style">The employee consistently displays the leadership practices and fully lives up to
                                    the values of the organization. He/she inspires others to demonstrate the appropriate behaviors/practices
                                    aligned with the core values</td>
                                </tr>

                                <tr>
                                    <th class="th_style text-center pm_text_header">85 - 94</th>
                                    <tH class="th_style text-center pm_text_header">DEMONSTRATING</tH>
                                    <td class="td_style">The employee display most of the leadership practices most of the time. He/she recognizes
                                    the appropriate behaviors to be displayed and tries to live  up other values.</td>
                                </tr>

                                <tr>
                                    <th class="th_style text-center pm_text_header">BELOW 85</th>
                                    <tH class="th_style text-center pm_text_header">PROGRESSING</tH>
                                    <td class="td_style">The employee fails to consistently demonstrate the leadership practices. He/she needs 
                                    regular coaching and guidance to align behaviors with the organization's core values.</td>
                                </tr>
                            </table>
                            
                            <div class="clear_20"></div>
                            <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                                <th class="th_style th_left ui-state-default">{{ $build['discipline']['component'] }} ({{ $build['discipline']['weight'] }}%)</th>
                                <th class="th_style ui-state-default">Weight</th>
                                <th class="th_style ui-state-default">Demerit</th>
                                <th class="th_style ui-state-default">Score</th>
                                <tr>
                                    <td class="td_style ">
                                        {{ $build['discipline']['comments'] }}
                                    </td>
                                    <td class="td_style "><input type="textbox" name="" value="5" readonly id="discipline-weight" ></td>
                                    <td class="td_style "><input type="textbox" value="0" class="isDecimal" id="discipline-demerit" name="discipline-demerit" ></td>
                                    <td class="td_style "><input type="textbox"  readonly id="discipline-score" name="discipline-score" ></td>
                                </tr>
                            </table>

                            <div class="clear_20"></div>
                            <table border = "1" cellpadding = "0" class="tbl_pm_questions">
                                <th class="th_style th_left  ui-state-default">{{ $build['policies_and_procedures']['component'] }} ({{ $build['policies_and_procedures']['weight'] }}%)</th>
                                <th class="th_style  ui-state-default">Weight</th>
                                <th class="th_style  ui-state-default">Rating</th>
                                <th class="th_style  ui-state-default">Score</th>
                                <tr>
                                    <td class="td_style">
                                     {{ $build['policies_and_procedures']['comments'] }}
                                    </td>
                                    <td class="td_style"><input type="textbox" name=""  value="5"  readonly id="compliance-weight"  ></td>
                                    <td class="td_style"><input type="textbox" class="isDecimal" id="compliance-rating" name="compliance-rating" ></td>
                                    <td class="td_style"><input type="textbox" readonly id="compliance-score" name="compliance-score" ></td>
                                </tr>
                            </table>
                            <div class="pcv_footer  th_left">                    
                            <div class="td_pm_style th_style">
                                <p class="pmf-overall_rate text-right ">OVERALL RATING FOR PERFORMANCE BEHAVIORS: 
                                <input disabled="" readonly="" type="textbox" class="" id="total-behavior" value="0">
                                &nbsp;&nbsp;&nbsp;</p> 
                            </div>  
                        </div>
                      
                      </div>

                      <div id="tabs-3">
                        

                            <div class="clear_20"></div>
                            <label class="form_label pm_form_title">SUMMARY OF PERFORMANCE</label>
                            
                            <div class="pm_form_container">
                                <table border = "1" cellpadding = "0" class="">
                                    <th class="th_style">Component</th>
                                    <th class="th_style" colspan="2">Score</th>                     
                                    <tr>
                                        <td class="td_style pm_tbl_labels">{{ $build['objectives']['component'] }} ({{ $build['objectives']['weight'] }}%)</td>
                                        <td></td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="overall-objective" name="" value=""></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style pm_tbl_labels">{{ $build['PERFORMANCE BEHAVIORS']['component'] }} ({{ $build['PERFORMANCE BEHAVIORS']['weight'] }}%)</td>
                                        <td></td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="overall-behavior" name=""></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style pm_tbl_labels">{{ $build['core_values']['component'] }} ({{ $build['core_values']['weight'] }}%)</td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="practice-core" name=""></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style pm_tbl_labels">{{ $build['discipline']['component'] }} ({{ $build['discipline']['weight'] }}%)</td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="summary-discipline" name=""></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="td_style pm_tbl_labels">{{ $build['policies_and_procedures']['component'] }} ({{ $build['policies_and_procedures']['weight'] }}%)</td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="summary-compliance" name=""></td>
                                        <td></td>
                                    </tr>
                                    <tr class="hide"  >
                                        <td class="text-right"  colspan="2"><b>TOTAL SCORE:</b> </td>
                                        <td><input type="hidden" class="pm_po" readonly id="total-score"></td>
                                    </tr>

                                    <tr class="hide" >
                                        <td class="text-right" colspan="2"><b>EQUIVALENT RATING:</b> </td>
                                        <td class="td_style pm_po" class=""><input readonly type="textbox" class="pm_po" id="equivalent-rating" name="classification" value=""></td>
                                    </tr>

                                </table>
                                <div class="clear_20"></div>
                            </div>
                            
                            <div class="pm1_form_container">
                                <table border = "1" cellpadding = "0" class="">
                                    <div class="border th_style rc">Rating Classifications</div>
                                    <tr>
                                        <td class="td_style text-center">A+</td>
                                        <td class="td_style text-center">100.01% - 120.00%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">A</td>
                                        <td class="td_style text-center">95.00% - 100.00%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">B+</td>
                                        <td class="td_style text-center">90.00% - 94.99%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">B</td>
                                        <td class="td_style text-center">85.00% - 89.99%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">C+</td>
                                        <td class="td_style text-center">80.00% - 84.99%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">C</td>
                                        <td class="td_style text-center">75.00% - 79.99%</td>
                                    </tr>
                                    <tr>
                                        <td class="td_style text-center">D</td>
                                        <td class="td_style text-center">0.00% - 74.99%</td>
                                    </tr>
                                </table>
                                <div class="clear_20"></div>
                            </div>

                        <div class="clear_10"></div>

                      </div>

                      <div id="tabs-4">
                            
                        <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div class="attachment_container">
                                <!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                                <div id="attachments"></div>    
                                <span class="btn btn-success btnbrowse fileinput-button">
                                    <span >BROWSE</span>
                                    <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                                </span>
                                                    
                        </div>   


                      </div>
                    
                    </div>






























                    

                    
                    
                    
                    


					
	            <div class="clear_20"></div>
                <div class="textarea_messages_container" style="padding-top:0">
                    <div class="comment_area">
                    Employees's Comments / Suggestions
                    </div>
                    <div class="row">
                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        <textarea maxlength='5000' rows="3"  cols="120" name="employee_comment"></textarea> 
                        <input readonly="readonly" type="hidden" class="form-control" name="employee_comment_id"  value="{{ Session::get('employee_id') }}" />                       
                    </div>
                    <div class="clear_10"></div>
                    <table border="1" cellspacing="1" class="comment_table">
                        <tr>
                        <td width="30%">Acknowledged:</td>
                        <td>{{ Session::get('employee_name') }}</td>
                        </tr>
                    </table>

                    
                </div>



            </div><!-- end of form_container -->

            <input type="hidden" id="date-url" value="{{ route('file-uploader.store') }}" />

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea maxlength='5000' rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>
var max_filesize = 0;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
{{ HTML::script('/assets/js/pmf/create.js?v943') }}
@stop