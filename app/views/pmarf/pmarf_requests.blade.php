@extends('template/header')

@section('content')
	<div class="wrapper">
		<div class="clear_10"></div>
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY PMARF</span>
			<table id="my_pmarf_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="my_pmarf_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center">CURRENT</th>
						<th align="center" width="240">ACTION</th>
					</tr>							
				</thead>
			</table>
		<div class="clear_20"></div>

		@if(Session::get('desig_level') == 'supervisor' && in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
			<div class="clear_20"></div>
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
			<table id="imm_sup_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="imm_sup_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">REQUESTOR</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center" width="240">ACTION</th>
					</tr>							
				</thead>
			</table>
			<?php echo "<!-- " ;?>
			<div class="clear_20"></div>
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >CLOSED REQUESTS</span>
			<table id="approver_closed_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="approver_closed_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">REQUESTOR</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center" width="240">ACTION</th>
					</tr>							
				</thead>
			</table>
			<?php echo "-->" ;?>
		@endif
		
		@if(Session::get('desig_level') == 'head' && in_array(Session::get('dept_id'), json_decode(PMARF_ALLOWED_DEPT) ))
		<hr/>
		<div class="clear_20"></div>
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
			<table id="head_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="head_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">REQUESTOR</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center" width="240">ACTION</th>
					</tr>							
				</thead>
			</table>
			<?php echo "<!-- " ;?>
			<div class="clear_20"></div>
			<hr/>
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >CLOSED REQUESTS</span>
			<table id="approver_closed_requests_dh" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="approver_closed_requests_dh">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">REQUESTOR</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center" width="240">ACTION</th>
					</tr>							
				</thead>
			</table>
			<?php echo "-->" ;?>
		@endif
		
		
		
	</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this request "+$(this).data('request-ref-num')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop
