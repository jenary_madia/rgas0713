
@extends('template/header')

@section('content')
<form class="form-inline" action="{{URL::current()}}" method="post" enctype="multipart/form-data">
	<input type="hidden" value="{{ csrf_token() }}" />
	<div class="form_container">
		<div class="text-center">
			<img src="<?php echo $base_url;?>/assets/img/rebisco_logo.png" width="72" height="71" class="text-center"/>
			<label class="text-center pmarf_print">Rebisco Group of Food Companies</label>
		</div>
		<div class="clear_10"></div>
		<label class="form_title">Promo & Merchandising Activity Request Form</label>
		<div class="row">
			<div class="pmarfdivider"></div>
		</div>
		<div class="clear_20"></div>
		<div class="row">
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Requestor:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" id="emp_name" name="emp_name" value="{{ $request->pmarf_emp_name }}" />
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Reference Number:</label>
				</div>
				<div class="col2_form_container">
				   <input readonly="readonly" type="text" class="form-control" name="pmarf_refno" value="{{ $request->pmarf_ref_num }}" />
				   <input readonly="readonly" type="hidden" class="form-control" name="pmarf_id" value="{{ $request->pmarf_id }}" />
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Employee Number:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="hidden" class="form-control" name="emp_id" value="{{ $request->pmarf_emp_id }}" />
					<input readonly="readonly" type="text" class="form-control" name="employees_id" value="{{ $request->pmarf_employees_id }}" />	
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Date Filed:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->pmarf_date_filed }}" id="pmarf_date_filed" name="pmarf_date_filed"/>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Company:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_company }}" id="emp_company" name="emp_company"/>
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Date Acknowledged:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_modified_date }}" name="pmarf_modified_date" id="pmarf_modified_date"/>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Department:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_department }}" name="emp_department" id="emp_department"/>
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels printlabels">Contact Number:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" name="emp_contactno" id="emp_contactno" value="{{ $request->pmarf_contact_no }}" />
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels printlabels">Section:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_section }}" id="emp_section" name="emp_section" />
				</div>
			</div>
		</div><!--endoffirstrow-->
		
		<div class="row">
			<div class="pmarfdivider"></div>
		</div>
		<div class="clear_20"></div>
		<label class="form_title">Request Details / Specification</label>
		<div class="row">
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Type of Activity:</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="{{ $request->pmarf_activity_type }}" style="width:722px" class="form-control" disabled>
				</div>
			</div>
			@if($request->pmarf_activity_type == "Others")
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Activity Details</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="{{ $request->pmarf_activity_details }}" style="width:722px" class="form-control" disabled>
				</div>
			</div>
			@endif
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Area Coverage:</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="{{ $request->pmarf_activity_area }}" style="width:722px" class="form-control" disabled>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Implementer:</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="{{ $request->pmarf_implementer }}" style="width:722px" class="form-control" disabled>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Initiated By:</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="<?php foreach ($init as $in){ echo $in . ', ';}?>" class="form-control" style="width:722px" disabled>
				
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required printlabels">Date of Activity:</label>
				</div>
				<div class="col2_form_container">
					<input type="text" value="{{ $request['pmarf_activity_dates'] }}" class="form-control" style="width:255px" disabled>
				</div>
			</div>
			
			<div class="row_form_container">
				<div class="col4_form_container">
					<label class="labels required printlabels">Completion Date:</label>
				</div>
				<div class="col5_form_container" style="width: 329px">
					 <input readonly="readonly" id="req_agency_name" name="req_agency_name" type="text" class="form-control" value="{{ $request->pmarf_completion_date }}"/ disabled>
				</div>
			</div>
			
		</div><!--endofsecondrow-->
			 
		<div class="clear_10"></div>
		<label class="pmarf_textarea_labels required printlabels">PROPOSAL OBJECTIVES</label>
		<div class="pmarf_textarea">
			<textarea style="max-height:90px" onkeyup="AutoGrowTextArea(this)" id="specs_proposal" name=""  cols="10" class="form-control" disabled value="">{{ $request->pmarf_proposal_objective }} </textarea>
		</div>
		<div class="clear_10"></div>
		<label class="pmarf_textarea_labels required printlabels">BRIEF DESCRIPTION OF PROPOSAL / SPECIFICATIONS</label>
		<div class="pmarf_textarea">
			<textarea style="max-height:180px" onkeyup="AutoGrowTextArea(this)" id="specs_proposal_desc" name=""  cols="10" class="form-control" disabled>  {{ $request->pmarf_proposal_description }} </textarea>
		</div>
		<div class="clear_10"></div>
		<label class="pmarf_textarea_labels required printlabels">ADDITIONAL SUPPORT ACTIVITIES</label>
		<div class="pmarf_textarea">
			<textarea style="max-height:90px"  onkeyup="AutoGrowTextArea(this)" id="specs_additional_support" name=""  cols="10" class="form-control" disabled>{{ $request->pmarf_add_support_act }}</textarea>
		</div>
				
			<div class="clear_20"></div>
			<div class="pmarf_textarea">
				<table border = "1" cellpadding = "0" class="" id="tbl_brands">
					<label class="pmarf_textarea_labels required">PARTICIPATING PRODUCTS</label>
					<th class="th_style quantity ">Quantity</th>
					<th class="th_style quantity">Unit</th>
					<th class="th_style">Product Description</th>
					@foreach ($brands as $row=>$aFlds)
						<?php 
							$qty     = "qty_".$row;
							$unit    = "unit_".$row;
							$product = "product_".$row;
						?>
						<tr>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $qty }}" id="{{ $qty }}" class="quantity_textbox" value="{{ $aFlds[$qty] }}"/></td>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $unit }}" id="{{ $unit }}" class="quantity_textbox" value="{{ $aFlds[$unit] }}" /></td>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $product }}" id="{{ $product }}" class="product_textbox"  value="{{ $aFlds[$product] }}"/></td>
						</tr>
					@endforeach
				</table>
			</div>
			
		<div class="clear_20"></div>
		<div class="row">
			<div class="pmarfdivider"></div>
		</div>
		
	<div class="clear_20"></div>
		<div class="col-md-12">
			<label class="col-md-4 printlabels" style="width: 250px">Prepared By:</label>
			<label class="col-md-4 printlabels" style="width: 250px">Reviewed By:</label>
			<label class="col-md-4 printlabels" style="width: 250px">Noted By:</label>
		</div>
		<div class="clear_60"></div>
		<div class="col-md-12">
			<label class="col-md-4 printlabels" style="width: 250px">{{ $request['pmarf_emp_name'] }}</label>
			<label class="col-md-4 printlabels" style="width: 250px">{{ $supInfo->firstname }} {{ $supInfo->middlename }} {{ $supInfo->lastname }}</label>
			<label class="col-md-4 printlabels" style="width: 250px">{{ $depInfo->firstname }} {{ $depInfo->middlename }} {{ $depInfo->lastname }}</label>
		</div>
		<div class="clear_20"></div>
		<div class="col-md-12">
			<label class="col-md-4 printlabels" style="width: 250px">Requestor</label>
			<label class="col-md-4 printlabels" style="width: 250px">Immediate Superior</label>
			<label class="col-md-4 printlabels" style="width: 250px">Department Head</label>
		</div>
	<div class="clear_20"></div>
	
	<div class="row">
		<div class="pmarfdivider"></div>
	</div>
	</div><!-- end of first form_container-->
	
	<div class="form_container">
		<div class="clear_10"></div>
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label printlabels">Comments</label><br />
				<textarea name="" rows="15" cols="7" class="form-control" style="width: 868px; margin-left: 20px" disabled>@if( $request['pmarf_comments'] != '')@foreach(json_decode($request['pmarf_comments']) as $comment)@if(@$comment->status != "draft"){{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10;@endif{{-- don't forget this hack --}}@endforeach{{-- don't forget this hack --}}@endif</textarea>
			</div>
		</div>

		<div class="clear_10"></div>
		@if($request['pmarf_status'] == "Closed")
			<div class="row">
				<div class="comment_container noprint">
					<div class="print">
						<a class="btn btn-default btndefault" id="printThis" href="{{ URL::to('pmarf/print/').'/'.$request['pmarf_id'] }}">PRINT</a>
						<!--<a class="btn btn-default btndefault" id="printThis" href="{{ URL::to('te/print-request/').'/'.$request['te_id'] }}">PRINT</a>-->
					</div>
				</div>
			</div>
		@endif
	</div><!-- end of second form_container-->
	
</form>
@stop
@section('js_ko')
	{{ HTML::script('/assets/js/pmarf.js?v1') }}
	<script type="text/javascript">
		$("a.tooltipLink").tooltip();

		$("#printThis").click(function () {
			window.print();
		});	


		$(function()
	{
		$("#specs_proposal").trigger("keyup");
		$("#specs_proposal_desc").trigger("keyup");
		$("#specs_additional_support").trigger("keyup");
	
	})

	function AutoGrowTextArea(textField)
		{
		  if (textField.clientHeight < textField.scrollHeight)
		  {
		    textField.style.height = textField.scrollHeight + "px";
		    if (textField.clientHeight < textField.scrollHeight)
		    {
		      textField.style.height = 
		        (textField.scrollHeight * 2 - textField.clientHeight) + "px";
		    }
		  }
		}
			
	</script>
@stop



