@extends('template/header')
@section('content')
        <form class="form-inline" method="post" enctype="multipart/form-data" name="">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container"> 	 	
                <label class="form_title">PROMO AND MERCHANDISING ACTIVITY REQUEST FORM</label>
                    <div class="row">
					
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" id="emp_name" name="emp_name" value="{{ Session::get('employee_name') }}" />
                            </div>
                        </div>
						
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="pmarf_refno" />
                            </div>
                        </div>
						
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="hidden" class="form-control" name="emp_id" value="{{ Session::get('employee_id') }}" />
								<input readonly="readonly" type="text" class="form-control" name="employees_id" value="{{ Session::get('employeeid') }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE FILED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control text-left" value="{{ date('Y-m-d') }}" id="pmarf_date_filed" name="pmarf_date_filed"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">COMPANY:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" id="emp_company" name="emp_company"/>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">STATUS:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" name="pmarf_status" id="pmarf_status" value="New"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" value="{{ Session::get('dept_name') }}" name="emp_department" id="emp_department"/>   
							</div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">CONTACT NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
								<input type="text" class="form-control" name="emp_contactno" id="emp_contactno" value="{{ Input::old('emp_contactno') }}"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input type="text" class="form-control" value="{{ Input::old('emp_section') ? Input::old('emp_section') : Session::get('sect_name') }}" id="emp_section" name="emp_section" />
                            </div>
                        </div>
                    </div>

                    <div class="clear_20"></div>
					 <div class="container-header"><h5 class="text-center lined sub-header"><strong>REQUEST DETAILS</strong></h5></div>
                    
					<div class="clear_20"></div>

                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">TYPE OF ACTIVITY:</label>
                            </div>
                            <div class="col2_form_container">
                                <select id='act_type' name='req_activity_type' class="form-control" style="width: 241px">
                                             <option value="" ></option>
                                    @foreach ($activity_type as $aKey=>$aVal)
										@if (Input::old('req_activity_type') == $aVal)
											  <option value="{{ $aVal }}" selected>{{ $aVal }}</option>
										@else
											  <option value="{{ $aVal }}">{{ $aVal }}</option>
										@endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
						@if(Input::old('req_activity_type') == 'Others')
							<div class="row_form_container" id="div_act_type">
								<div class="col4_form_container">
									<label class="labels required">ACTIVITY DETAILS:</label>
								</div>
								<div class="col5_form_container">
									<input id='req_activity_details' name='req_activity_details' type="text" class="form-control" value="{{ Input::old('req_activity_details') }}"/>
								</div>
							</div>
						@else
							<div class="row_form_container" style="display: none" id="div_act_type">
								<div class="col4_form_container">
									<label class="labels required">ACTIVITY DETAILS:</label>
								</div>
								<div class="col5_form_container">
									<input id='req_activity_details' name='req_activity_details' type="text" class="form-control" value="{{ Input::old('req_activity_details') }}"/>
								</div>
							</div>
						@endif
                        <div class="clear_10"></div>
                        <div class="row_form_container">			
                            <div class="col1_form_container">
                                <label class="labels required">DATE OF ACTIVITY:</label>
                            </div>
                            <div class="col2_form_container">
								<select id='req_activity_date' name='req_activity_date' class="form-control" style="width: 241px">
                                     <option value="" ></option>
                                    @foreach ($activity_date as $sKey=>$sDates)
										@if(Input::old('req_activity_date') == $sDates)
											<option value="{{ $sDates }}" selected>{{ $sDates }}</option>
										@else
											<option value="{{ $sDates }}">{{ $sDates }}</option>
										@endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
						@if(Input::old('req_activity_date') == 'Multiple days-Staggered')
							<div class="row_form_container div_staggered">
								<div class="col4_form_container">
									<label class="labels required">DATES:</label>
								</div>
								<div class="col5_form_container">
									<input id="req_date" name="multiple_dates" type="text" class="form-control" value="{{ Input::old('multiple_dates') }}" />
								</div>
							</div>
						@else
							<div class="row_form_container div_staggered" style="display:none">
								<div class="col4_form_container">
									<label class="labels required">DATES:</label>
								</div>
								<div class="col5_form_container">
									<input id="req_date" name="multiple_dates" type="text" class="form-control" value="{{ Input::old('multiple_dates') }}" />
								</div>
							</div>
						@endif
                        <div class="clear_10"></div>
						@if(Input::old('req_activity_date') == 'Multiple days-Staggered')
							<div class="row_form_container div_straight" style="display:none">
								<div class="col1_form_container">
									<label class="labels1 required">FROM:</label>
								</div>
								<div class="col2_form_container input-group">
									<input type="text" class="form-control date_picker_" name="req_from" id="req_from" value="{{ Input::old('req_from') }}"/>
									 <label class="input-group-addon btn" for="req_from">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
								</div>
								<div class="clear_10"></div>
								<div class="col1_form_container">
									<label class="labels1 required">TO:</label>
								</div>
								<div class="col2_form_container input-group">
									 <input id="req_to" name="req_to" type="text" class="form-control date_picker_" value="{{ Input::old('req_to') }}" />
									  <label class="input-group-addon btn" for="req_to">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
								</div>
							</div>
							<div class="row2_form_container div_straight_no"  style="display:none">
								<div class="col4_form_container">
									<label class="labels">NO. OF DAYS:</label>
								</div>
								<div class="col6_form_container">
									 <input id="req_noOfDays" name="req_noOfDays" type="text" class="form-control" value="{{ Input::old('req_noOfDays') }}"/>
								</div>
							</div>
						@else
						<div class="row_form_container div_straight" {{ in_array(Input::old('req_activity_date'),['Multiple days-Staggered',""]) ? 'style="display: none;"' : "" }} >
                            <div class="col1_form_container">
                                <label class="labels1 required">FROM:</label>
                            </div>
                            <div class="col2_form_container input-group">
                                <input type="text" class="form-control date_picker_" name="req_from" id="req_from" value="{{ Input::old('req_from') }}"/>
								 <label class="input-group-addon btn" for="req_from">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
                            </div>
							<div class="clear_10"></div>
                            <div class="col1_form_container">
                                <label class="labels1 required">TO:</label>
                            </div>
                            <div class="col2_form_container input-group">
                                 <input id="req_to" name="req_to" type="text" class="form-control date_picker_" value="{{ Input::old('req_to') }}" />
								  <label class="input-group-addon btn" for="req_to">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
                            </div>
                        </div>
						<div class="row2_form_container div_straight_no" {{ in_array(Input::old('req_activity_date'),['Multiple days-Staggered',""]) ? 'style="display: none;"' : "" }} >
                            <div class="col4_form_container">
                                <label class="labels">NO. OF DAYS:</label>
                            </div>
                            <div class="col6_form_container">
                                 <input id="req_noOfDays" name="req_noOfDays" type="text" class="form-control" value="{{ Input::old('req_noOfDays') }}"/>
                            </div>
                        </div>
						@endif
						
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">AREA COVERAGE:</label>
                            </div>
                            <div class="col2_form_container">
                                <input id="req_area_coverage" name="req_area_coverage" type="text" class="form-control" value="{{ Input::old('req_area_coverage') }}"/>
                            </div>
                        </div>
	
						<div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">IMPLEMENTER:</label>
                            </div>
                            <div class="col2_form_container">
                               <select id='req_implementer' name='req_implementer' class="form-control" style="width: 241px">
                                        <option value=""></option>
                                    @foreach ($implementer as $sK=>$sImplementer)
										@if(Input::old('req_implementer') == $sImplementer)
											<option value="{{ $sImplementer }}" selected>{{ $sImplementer }}</option>
										@else
											<option value="{{ $sImplementer }}">{{ $sImplementer }}</option>
										@endif
									@endforeach
                                </select>
                            </div>
							<a data-toggle="tooltip" data-target="#tooltip" >
							<abbr id="tooltip" title="Internal - NPMD will be the implementer/ manpower for the activity &#10;External - NPMD will supervise/ oversee the implementation of the activity by selected third party agency"><span class="glyphicon glyphicon-question-sign" style="margin-left: 5px;margin-top: 7px"></span></abbr></a>


                        </div>
                        <div class="row_form_container">
                            <div class="col4_form_container">
                                <label class="labels" id="agency">AGENCY NAME:</label>
                            </div>
                            <div class="col5_form_container agency_textbox">
                                 <input id="req_agency_name" name="req_agency_name" type="text" class="form-control" value="{{ Input::old('req_agency_name') }}"/>
                            </div>
                        </div>
						<div class="clear_20"></div>
                        <div class="row3_form_container">
                            <div class="col7_form_container">
                                <label class="labels required">INITIATED BY:</label>
                            </div>
							<div class="col2_form_container">
								<input type="checkbox" name="initiatedby[distributor]" value="Distributor" @if(@Input::old('initiatedby')['distributor'] == 'Distributor') checked="checked" @endif />
								<label class="labels" for="">Distributor</label>
								<br />
								<input type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager" @if(@Input::old('initiatedby')['brand_manager'] == 'Brand Manager') checked="checked" @endif />
								<label class="labels" for="">Brand Management</label>
								<br />
								<input type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media" @if(@Input::old('initiatedby')['ad_and_media'] == 'Ad and Media') checked="checked" @endif />
								<label class="labels" for="">Ad and Media</label>
								<br />
							 </div>
							 <div class="col2_form_container">
								<input type="checkbox" name="initiatedby[trade]" value="Trade" @if(@Input::old('initiatedby')['trade'] == 'Trade') checked="checked" @endif />
								<label class="labels" for="">Trade</label>
								<br />
								<input type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development" @if(@Input::old('initiatedby')['strategic_corporate_development'] == 'Strategic Corporate Development') checked="checked" @endif />
								<label class="labels" for="">Strategic Corporate Development</label>
								<br />
								<input type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager" @if(@Input::old('initiatedby')['key_accounts_manager'] == 'Key Accounts Manager') checked="checked" @endif />
								<label class="labels" for="">Key Accounts Manager</label>
								<br />
							 </div>
							  <div class="col2_form_container">
								<input type="checkbox" name="initiatedby[others]" value="Others" @if(@Input::old('initiatedby')['others'] == 'Others') checked="checked" @endif />
								<label class="labels" for="">Others</label><input type="text" name="initiatedby[initiatedby_others]" value="{{ Input::old('initiatedby')['initiatedby_others'] }}"/>
							  </div>
								{{--
								@foreach ($initiated_by as $sI=>$initiated_by)
									<div class="col10_form_container">
										<div class="col2_checkbox">
											<input type="checkbox" id="chk_{{ $initiated_by }}" name="chk_{{ $initiated_by }}"/>
										</div>
										<label class="labels" for="chkCoe">
											{{ $initiated_by }} 
											@if ($initiated_by === 'Others')
												<input type="textbox" id="chk_others_desc" name="chk_others_desc" />
											@endif
										</label>
									 </div>
								@endforeach
								--}}
						</div>
                    </div>
                    
                    <div class="clear_20"></div>
					 
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>SPECIFICATIONS</strong></h5></div>
                    
					<div class="clear_20"></div>

                    <div class="clear_10"></div>

                    <label class="pmarf_textarea_labels required">PROPOSAL OBJECTIVES</label>
                    <div class="pmarf_textarea">
                        <textarea style="max-height:90px"  onkeyup="AutoGrowTextArea(this)" id="specs_proposal" name="specs_proposal" rows="2" cols="10" class="form-control" value="">{{ Input::old('specs_proposal') }}</textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels required">BRIEF DESCRIPTION OF PROPOSAL</label>
                    <div class="pmarf_textarea">
                        <textarea style="max-height:180px"  onkeyup="AutoGrowTextArea(this)" id="specs_proposal_desc" name="specs_proposal_desc" rows="2" cols="10" class="form-control" value="">{{ Input::old('specs_proposal_desc') }}</textarea>
                    </div>
                    <div class="clear_10"></div>
                    <label class="pmarf_textarea_labels required">ADDITIONAL SUPPORT ACTIVITIES</label>
                    <div class="pmarf_textarea">
                        <textarea style="max-height:90px"  onkeyup="AutoGrowTextArea(this)" id="specs_additional_support" name="specs_additional_support" rows="2" cols="10" class="form-control" value="">{{ Input::old('specs_additional_support') }}</textarea>
                    </div>
					<div class="clear_20"></div>
                    <label class="pmarf_textarea_labels required">PARTICIPATING BRANDS</label>
                    <div class="pmarf_textarea">
                        <table border = "1" cellpadding = "0" class="" id="tbl_brands">
                            <th class="th_style quantity ">QUANTITY</th>
                            <th class="th_style quantity">UNIT</th>
                            <th class="th_style">PRODUCT NAME</th>
							@if (Input::old('qty_'))
							@foreach(Input::old('qty_') as $qty_key => $qty_val)
                            <tr class="tr_pb" id="nc">	
                                <td class="td_style"><input  type="textbox" name="qty_[]" id="qty_1" class="quantity_textbox" value="{{ $qty_val }}"/></td>
                                <td class="td_style"><input  type="textbox" name="unit_[]" id="unit_1" class="quantity_textbox" value="{{ Input::old('unit_')[$qty_key] }}" /></td>
                                <td class="td_style"><input  type="textbox" name="product_[]" id="product_1" class="product_textbox" value="{{ Input::old('product_')[$qty_key] }}" /></td>
                            </tr>							
							@endforeach
							@else
                            <tr class="tr_pb" id="nc">	
                                <td class="td_style"><input  type="textbox" name="qty_[]" id="qty_1" class="quantity_textbox" value="{{ Input::old('qty_1') }}"/></td>
                                <td class="td_style"><input  type="textbox" name="unit_[]" id="unit_1" class="quantity_textbox" value="{{ Input::old('unit_1') }}" /></td>
                                <td class="td_style"><input  type="textbox" name="product_[]" id="product_1" class="product_textbox" value="{{ Input::old('product_1') }}" /></td>
                            </tr>
							@endif
                        </table>
                    </div>
					
					 <div class="pmarf_btns btn-group">
                        <button type="button" id="brands_add" name="brands_add"  class="btn btn-xs btn2 btn-default" value="">ADD</button>
                        <button type="button" id="brands_del" name="brands_del" class="btn btn-xs btn2 btn-default" value="">DELETE</button>
                    </div>

                    <div class="clear_20"></div>
					<div class="clear_20"></div>
					 
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>

					<div class="attachment_container">
						<div id="attachments">
							@if(Input::old('files'))
								@foreach(Input::old('files') as $a => $b)
								<p>
									{{ $b['original_filename'] }} | {{ FileSizeConverter::convert_size($b['filesize']) }}
									<input type="hidden" name="files[{{ $a }}][filesize]" value="{{ $b['filesize'] }}">
									<input type="hidden" name="files[{{ $a }}][mime_type]" value="{{ $b['mime_type'] }}">
									<input type="hidden" name="files[{{ $a }}][original_extension]" value="{{ $b['original_extension'] }}">
									<input type="hidden" name="files[{{ $a }}][original_filename]" value="{{ $b['original_filename'] }}">
									<input type="hidden" name="files[{{ $a }}][random_filename]" value="{{ $b['random_filename'] }}"> 
									<button class="btn btn-xs btn-danger remove-fn">DELETE</button>
								</p>
								@endforeach
							@endif
						</div>	
						<span class="btn btn-success btnbrowse fileinput-button">
							<span >BROWSE</span>
							<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}">
						</span>
											
                    </div>
					
            </div> <!-- end of form_container-->
			
            <div class="clear_20"></div>
            <div class="form_container"><span class="legend-action">ACTION</span>
				<div class="clear_10"></div>
				<div class="textarea_messages_container">
					<div class="row">
						<label class="textarea_inside_label">COMMENT:</label>
						<textarea name="comments" rows="3" class="form-control textarea_inside_width">{{ Input::old('comments') }}</textarea>
					</div>
				</div>
			<div class="clear_10"></div>
				<div class="row">
					<div class="comment_container">
						<div class="comment_notes">
							<label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
						</div> 
						<div class="comment_button">
							<button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
						</div>
					</div>
					<div class="clear_10"></div>
					<div class="comment_container">
						<div class="comment_notes">
							@if(Session::get('desig_level') == 'head')
							<label class="button_notes"><strong> SEND</strong> TO NPMD HEAD FOR PROCESSING</label>
							@else
							<label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
							@endif
						</div> 
						<div class="comment_button">
							<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
						</div>
					</div>
				</div>
            </div>
	
<div class="modal fade" id="tooltip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <i>Internal - NPMD will be the implementer/ manpower for the activity &#10;External - NPMD will supervise/ oversee the implementation of the activity by selected third party agency</i>
      </div>     
    </div>
  </div>
</div>

		
		
        </form>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/pmarf.js?') }}
{{ HTML::script('/assets/js/touchHover.js?') }}

<script type="text/javascript">
	var allowed_file_count = 10;
	var allowed_total_filesize = 20971520;
	$(function () {
	
		$("#btnAddAttachment").click(function(){
			if($("#attachments p").length < 10){
				$("#attachments").append("<div><input type='file' name='attachments["+($("#attachments p").length + 1 )+"]' /><button class='remove_file_attachment btn-danger btn btn-xs' type='button'>DELETE</button></div>");
			}
		});
		
		
		$(".remove-fn").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$('#act_type').on('change', function() {
			  if ( this.value == 'Others'){
				$('#div_act_type').show();
			  }
			  else{
				$('#div_act_type').hide();
			  }
		});
		
		$('#req_activity_date').on('change', function() {
			  if ( this.value == 'Multiple days-Staggered'){
				$('#div_staggered').show();
			  }
			  else{
				$('#div_staggered').hide();
			  }
		});
		
		$('#req_implementer').on('change', function() {
			  if ( this.value == 'External'){
				$('#agency').css("color","#c80b31");
			  }
			  else{
				$('#agency').css("color","#383c40");
			  }
		});
		
		$("a.tooltipLink").tooltip();
		
	});

    function AutoGrowTextArea(textField)
        {
          if (textField.clientHeight < textField.scrollHeight)
          {
            textField.style.height = textField.scrollHeight + "px";
            if (textField.clientHeight < textField.scrollHeight)
            {
              textField.style.height = 
                (textField.scrollHeight * 2 - textField.clientHeight) + "px";
            }
          }
        }
</script>
@stop