@extends('template/header')

@section('content')
	<div class="wrapper">
		<div class="clear_10"></div>

		
		@if(Session::get('is_pmarf_npmd_head'))
		<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >PENDING PMARF</span>
			<table id="npmd_head_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="npmd_head_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center">REQUESTOR</th>
						<th align="center">DEPARTMENT</th>
						<th align="center" width="200">ACTION</th>
					</tr>							
				</thead>
			</table>
		</div>
		@endif
		
		@if(Session::get('is_pmarf_npmd_personnel'))
		<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >PENDING PMARF</span>
			<table id="npmd_personnel_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="npmd_personnel_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center">REQUESTOR</th>
						<th align="center">DEPARTMENT</th>
						<th align="center" width="200">ACTION</th>
					</tr>							
				</thead>
			</table>
		</div>
		@endif

		@if(Session::get('is_pmarf_npmd_head'))

			
		<hr/>
		<div class="clear_20"></div>
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >ALL PMARF REQUESTS</span>
			<table id="all_pmarf_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="all_pmarf_requests">
				<thead> 
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center">REQUESTOR</th>
						<th align="center">DEPARTMENT</th>
						<th align="center" width="200">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
		@endif
		
		@if(Session::get('is_pmarf_npmd_personnel'))

		<hr/>
		<div class="clear_20"></div>
			<div class="datatable_holder">
			<span class="list-title" role="columnheader" rowspan="1" colspan="5" >ALL PMARF REQUESTS</span>
			<table id="all_pmarf_requests" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="all_pmarf_requests">
				<thead>
					<tr role="row">
						<th align="center">REFERENCE NO.</th>
						<th align="center">DATE FILED</th>
						<th align="center">TYPE OF ACTIVITY</th>
						<th align="center">STATUS</th>
						<th align="center">REQUESTOR</th>
						<th align="center">DEPARTMENT</th>
						<th align="center" width="200">ACTION</th>
					</tr>							
				</thead>
			</table>
			</div>
		@endif
		
	</div>
@stop
@section('js_ko')
<script type="text/javascript">
	$(".remove-request").live('click', function() {
		var a = confirm("Are you sure you want to delete this request "+$(this).data('request-ref-num')+"?");
		if (a == true) {
			window.location.href($(this).attr("href"));
		}
		return a;
	});
</script>
@stop
