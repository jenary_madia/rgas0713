<html>
<head></head>
<body>
Good day!
<br /><br />
You have 1 new PMARF for assessment.
<br /><br />
Reference No.: {{ $ref_num }}<br />
Requestor: {{ $requestor }}<br />
Department: {{ $department }}<br />
Type of Activity: {{ $activity_type }}<br />
<br /><br />
Click <a href="{{ URL::to('/') }}">here </a>to log-in and check the file.<br />
<br /><br />
DO NOT REPLY. THIS IS A SYSTEM-GENERATED MESSAGE.<br />
Service made possible by CSMD - Application Development.
</body>
<html>

