@extends('template/header')
@section('content')
<form class="form-inline" method="post" enctype="multipart/form-data" name="">
	<input type="hidden" value="{{ csrf_token() }}">
	<div class="form_container"> 	 	
		<label class="form_title">PROMO AND MERCHANDISING ACTIVITY REQUEST FORM</label>
			<div class="row">
			
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">EMPLOYEE NAME:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control" id="emp_name" name="emp_name" value="{{ $request->pmarf_emp_name }}" />
					</div>
				</div>
				
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">REFERENCE NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control" name="pmarf_refno" value="{{ $request->pmarf_ref_num }}" />
						<input readonly="readonly" type="hidden" class="form-control" name="pmarf_id" value="{{ $request->pmarf_id }}" />
					</div>
				</div>
				
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">EMPLOYEE NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="hidden" class="form-control" name="emp_id" value="{{ $request->pmarf_emp_id }}" />
						<input readonly="readonly" type="text" class="form-control" name="employees_id" value="{{ $request->pmarf_employees_id }}" />
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">DATE FILED:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->pmarf_date_filed }}" id="pmarf_date_filed" name="pmarf_date_filed"/>
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">COMPANY:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_company }}" id="emp_company" name="emp_company"/>
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">STATUS:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_status }}" name="pmarf_status" id="pmarf_status"/>
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">DEPARTMENT:</label>
					</div>
					<div class="col2_form_container">
						<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_department }}" name="emp_department" id="emp_department"/> 
					</div>
				</div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">CONTACT NUMBER:</label>
					</div>
					<div class="col2_form_container">
						<input type="text" class="form-control" name="emp_contactno" id="emp_contactno" value="@if(Input::old('emp_contactno') != ""){{ Input::old('emp_contactno') }}@else{{ $request->pmarf_contact_no }}@endif" />
					</div>
				</div>
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels">SECTION:</label>
					</div>
					<div class="col2_form_container">
						<input  type="text" class="form-control" value="{{ Input::old('emp_section') ? Input::old('emp_section') :  $request->pmarf_section }}" id="emp_section" name="emp_section" />
					</div>
				</div>
			</div>

			<div class="clear_20"></div>
			 <div class="container-header"><h5 class="text-center lined sub-header"><strong>REQUEST DETAILS</strong></h5></div>
                    
			<div class="clear_20"></div>

			<div class="row">
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">TYPE OF ACTIVITY:</label>
					</div>
					<div class="col2_form_container">
						<select id='act_type' name='req_activity_type' class="form-control" style="width: 241px">
						 <option value="" ></option>
							@foreach ($activity_type as $aKey=>$aVal)
								@if(Input::old('req_activity_type') == $aVal)
									<option selected="selected" value="{{ $aVal }}">{{ $aVal }}</option>
								@elseif($aVal == $request->pmarf_activity_type && !Input::old('req_activity_type'))
									<option selected="selected" value="{{ $aVal }}">{{ $aVal }}</option>
								@else
									<option value="{{ $aVal }}">{{ $aVal }}</option>
								@endif								
							@endforeach
							
						</select>
					</div>
				</div>
				@if($request['pmarf_activity_type'] == "Others" || Input::old('req_activity_type') == "Others")
				<div class="row_form_container" id="div_act_type">
					<div class="col4_form_container">
						<label class="labels required">ACTIVITY DETAILS:</label>
					</div>
					<div class="col5_form_container">
						<input id='req_activity_details' name='req_activity_details' type="text" class="form-control" value="@if(Input::old('req_activity_details') != ""){{ Input::old('req_activity_details')}}@else{{ $request->pmarf_activity_details }}@endif"{{ $request->pmarf_activity_details }}"/>
					</div>
				</div>
				@else
				<div class="row_form_container" id="div_act_type" style="display: none">
					<div class="col4_form_container">
						<label class="labels required">ACTIVITY DETAILS:</label>
					</div>
					<div class="col5_form_container">
						<input id='req_activity_details' name='req_activity_details' type="text" class="form-control" value="@if(Input::old('req_activity_details') != ""){{ Input::old('req_activity_details')}}@else{{ $request->pmarf_activity_details }}@endif"/>
					</div>
				</div>
				@endif
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">DATE OF ACTIVITY:</label>
					</div>
					<div class="col2_form_container">
						<select id='req_activity_date' name='req_activity_date' class="form-control" style="width: 241px">
							<!--@foreach ($activity_date as $aKey=>$sDates)
								@if($sDates == $request->pmarf_activity_dates)
									<option selected="selected" value="{{ $sDates }}">{{ $sDates }}</option>
								@else
									<option value="{{ $sDates}}">{{ $sDates}}</option>
								@endif
							@endforeach-->
							 <option value="" ></option>
							@foreach ($activity_date as $aKey=>$sDates)
								@if(Input::old('req_activity_date') == $sDates)
									<option selected="selected" value="{{ $sDates }}">{{ $sDates }}</option>
								@elseif($sDates == $request->pmarf_activity_dates && !Input::old('req_activity_date'))
									<option selected="selected" value="{{ $sDates }}">{{ $sDates }}</option>
								@else
									<option value="{{ $sDates }}">{{ $sDates }}</option>
								@endif								
							@endforeach
						</select>		
					</div>
				</div>
					@if($request['pmarf_activity_dates'] == "Multiple days-Staggered")
					<div class="row_form_container div_staggered">
						<div class="col4_form_container">
							<label class="labels required">DATES:</label>
						</div>
						<div class="col5_form_container">
							<input id="multiple_dates" name="multiple_dates" type="text" class="form-control" value="@if(Input::old('multiple_dates') != ""){{ Input::old('multiple_dates') }} @else {{ $request->pmarf_multiple_dates }} @endif" />
						</div>
					</div>

					@else
					<div class="row_form_container div_staggered" style="display:none">
						<div class="col4_form_container">
							<label class="labels required">DATES:</label>
						</div>
						<div class="col5_form_container">
							<input id="multiple_dates" name="multiple_dates" type="text" class="form-control" value="@if(Input::old('multiple_dates') != ""){{ Input::old('multiple_dates') }} @else {{ $request->pmarf_multiple_dates }} @endif" />
						</div>
					</div>
					@endif
				<div class="clear_10"></div>
				
				@if($request['pmarf_activity_dates'] != "Multiple days-Staggered")
				<div class="row_form_container div_straight">
					<div class="col1_form_container">
						<label class="labels1 required">FROM:</label>
					</div>
					<div class="col2_form_container input-group">
						@if($request->pmarf_activity_to != "0000-00-00")
						<input type="text" class="form-control date_picker_" name="req_from" id="req_from" value="@if(Input::old('req_from') != ""){{ Input::old('req_from') }} @else {{ $request->pmarf_activity_from }} @endif"/>
						@else
						<input type="text" class="form-control date_picker_" name="req_from" id="req_from" value="{{ Input::old('req_from') }}"/>
						@endif
						<label class="input-group-addon btn" for="req_from">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
					</div>
					<div class="clear_10"></div>
					<div class="col1_form_container">
						<label class="labels1 required">TO:</label>
					</div>
					<div class="col2_form_container input-group">
						 @if($request->pmarf_activity_to != "0000-00-00")
						<input type="text" class="form-control date_picker_" name="req_to" id="req_to" value="@if(Input::old('req_to') != ""){{ Input::old('req_to') }} @else {{ $request->pmarf_activity_to }} @endif"/>
						@else
						<input type="text" class="form-control date_picker_" name="req_to" id="req_to" value="{{ Input::old('req_to') }}"/>
						@endif
						<label class="input-group-addon btn" for="req_to">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
					</div>
				</div>
				<div class="row2_form_container div_straight_no">
					<div class="col4_form_container">
						<label class="labels">NO. OF DAYS:</label>
					</div>
					<div class="col6_form_container">
						 <input id="req_noOfDays" name="req_noOfDays" type="text" class="form-control"  value="@if(Input::old('req_noOfDays') != ""){{ Input::old('req_noOfDays') }}@else{{ $request->pmarf_activity_no_of_days }}@endif"/>
					</div>
				</div>
				@elseif($request['pmarf_activity_dates'] == "Multiple days-Staggered")
				<div class="row_form_container div_straight" style="display:none">
					<div class="col1_form_container">
						<label class="labels1 required">FROM:</label>
					</div>
					<div class="col2_form_container input-group">
						@if($request->pmarf_activity_to != "0000-00-00")
						<input type="text" class="form-control date_picker_" name="req_from" id="req_from" value="@if(Input::old('req_from') != ""){{ Input::old('req_from') }} @else {{ $request->pmarf_activity_from }} @endif"/>
						@else
						<input type="text" class="form-control date_picker_" name="req_from" id="req_from" value=""/>
						@endif
						<label class="input-group-addon btn" for="req_from">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
					</div>
					<div class="clear_10"></div>
					<div class="col1_form_container">
						<label class="labels1 required">TO:</label>
					</div>
					<div class="col2_form_container input-group">
						 @if($request->pmarf_activity_to != "0000-00-00")
						<input type="text" class="form-control date_picker_" name="req_to" id="req_to" value="@if(Input::old('req_to') != ""){{ Input::old('req_to') }} @else {{ $request->pmarf_activity_to }} @endif"/>
						@else
						<input type="text" class="form-control date_picker_" name="req_to" id="req_to" value=""/>
						@endif
						 <label class="input-group-addon btn" for="req_to">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                    </label> 
					</div>
				</div>
				<div class="row2_form_container div_straight_no" style="display:none">
					<div class="col4_form_container">
						<label class="labels">NO. OF DAYS:</label>
					</div>
					<div class="col6_form_container">
						 <input id="req_noOfDays" name="req_noOfDays" type="text" class="form-control"  value="@if(Input::old('req_noOfDays') != ""){{ Input::old('req_noOfDays') }}@else{{ $request->pmarf_activity_no_of_days }}@endif"/>
					</div>
				</div>
				@endif
				
				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">AREA COVERAGE:</label>
					</div>
					<div class="col2_form_container">
						<input id="req_area_coverage" name="req_area_coverage" type="text" class="form-control" value="@if(Input::old('req_area_coverage') != ""){{ Input::old('req_area_coverage') }}@else{{ $request->pmarf_activity_area }}@endif"/>
					</div>
				</div>

				<div class="clear_10"></div>
				<div class="row_form_container">
					<div class="col1_form_container">
						<label class="labels required">IMPLEMENTER:</label>
					</div>
					<div class="col2_form_container">
						<select id='req_implementer' name='req_implementer' class="form-control" style="width: 241px">
							<!--@foreach ($implementer as $sK=>$sImplementer)
								@if($sImplementer == $request->pmarf_implementer)
									<option selected="selected" value="{{ $sImplementer }}">{{ $sImplementer }}</option>
								@else
									<option value="{{ $sImplementer}}">{{ $sImplementer}}</option>
								@endif
							@endforeach-->
							<option value=""></option>
							@foreach ($implementer as $sK=>$sImplementer)
								@if(Input::old('req_implementer') == $sImplementer)
									<option selected="selected" value="{{ $sImplementer }}">{{ $sImplementer }}</option>
								@elseif($sImplementer == $request->pmarf_implementer && !Input::old('req_implementer'))
									<option selected="selected" value="{{ $sImplementer }}">{{ $sImplementer }}</option>
								@else
									<option value="{{ $sImplementer }}">{{ $sImplementer }}</option>
								@endif							
							@endforeach
						</select>
						
					</div>
					<a data-toggle="tooltip" data-target="#tooltip" >
							<abbr id="tooltip" title="Internal - NPMD will be the implementer/ manpower for the activity &#10;External - NPMD will supervise/ oversee the implementation of the activity by selected third party agency"><span class="glyphicon glyphicon-question-sign" style="margin-left: 5px;margin-top: 7px"></span></abbr></a>


				</div>
				<div class="row_form_container">
					<div class="col4_form_container">
						@if($request['pmarf_implementer'] == "External")
							<label class="labels required" id="agency">AGENCY NAME:</label>
						@else
							<label class="labels" id="agency">AGENCY NAME:</label>
						@endif
					</div>
					<div class="col5_form_container agency_textbox">
						 <input id="req_agency_name" name="req_agency_name" type="text" class="form-control" value="@if(Input::old('req_agency_name') != ""){{ Input::old('req_agency_name') }}@else{{ $request->pmarf_agency_name }}@endif"/>
					</div>
				</div>
				<div class="clear_20"></div>

				<div class="row3_form_container">
					<div class="col7_form_container">
						<label class="labels required">INITIATED BY:</label>
					</div>
					<div class="col2_form_container">
						<!--@if (@array_key_exists("distributor",$init))
							<input  type="checkbox" name="initiatedby[distributor]" value="Distributor" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[distributor]" value="Distributor">
						@endif-->
						@if(@Input::old('initiatedby')['distributor'] == 'Distributor') 
							<input type="checkbox" name="initiatedby[distributor]" value="Distributor" checked="checked" />
						@elseif(@array_key_exists("distributor", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[distributor]" value="Distributor" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[distributor]" value="Distributor">
						@endif							
							<label class="labels" for="">Distributor</label>
							<br />
							
						<!--@if (@array_key_exists("brand_manager",$init))					
						<input  type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager">
						@endif-->
						@if(@Input::old('initiatedby')['brand_manager'] == 'Brand Manager') 
							<input type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager" checked="checked" />
						@elseif(@array_key_exists("brand_manager", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager">
						@endif
							<label class="labels" for="">Brand Management</label>
							<br />
							
						<!--@if (@array_key_exists("ad_and_media", $init))	
							<input  type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media">
						@endif-->
						@if(@Input::old('initiatedby')['ad_and_media'] == 'Ad and Media') 
							<input type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media" checked="checked" />
						@elseif(@array_key_exists("ad_and_media", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media">
						@endif
							<label class="labels" for="">Ad and Media</label>
							<br />
					 </div>
					 
					 <div class="col2_form_container">
						<!--@if (@array_key_exists("trade", $init))
						<input  type="checkbox" name="initiatedby[trade]" value="Trade" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[trade]" value="Trade">
						@endif-->
						@if(@Input::old('initiatedby')['trade'] == 'Trade') 
							<input type="checkbox" name="initiatedby[trade]" value="Trade" checked="checked" />
						@elseif(@array_key_exists("trade", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[trade]" value="Trade" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[trade]" value="Trade">
						@endif
							<label class="labels" for="">Trade</label>
						<br />
						
						<!--@if (@array_key_exists("strategic_corporate_development", $init))
							<input  type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development">
						@endif-->
						@if(@Input::old('initiatedby')['strategic_corporate_development'] == 'Strategic Corporate Development') 
							<input type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development" checked="checked" />
						@elseif(@array_key_exists("strategic_corporate_development", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development">
						@endif
							<label class="labels" for="">Strategic Corporate Development</label>
							<br />
							
						<!--@if (@array_key_exists("key_accounts_manager", $init))
							<input  type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager" checked="checked">
						@else
							<input  type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager">
						@endif-->
						@if(@Input::old('initiatedby')['key_accounts_manager'] == 'Key Accounts Manager') 
							<input type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager" checked="checked" />
						@elseif(@array_key_exists("key_accounts_manager", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager">
						@endif
							<label class="labels" for="">Key Accounts Manager</label>
							<br />
					 </div>
					 
					  <div class="col2_form_container">
						<!--<input type="checkbox" name="initiatedby['others']" value="Others">
						<label class="labels" for="">Others</label> <input type="text" name="initiatedby_others" class=""/>
						<input  type="checkbox" name="initiatedby['others']" value="Others">
						<label class="labels" for="">Others</label> <input  type="text" name="initiatedby_others" class=""/>-->
				
						@if(@Input::old('initiatedby')['others'] == 'Others') 
							<input type="checkbox" name="initiatedby[others]" value="Others" checked="checked" />
						@elseif(@array_key_exists("others", $init) && !@Input::old()) 
							<input type="checkbox" name="initiatedby[others]" value="Others" checked="checked" />
						@else
							<input type="checkbox" name="initiatedby[others]" value="Others">
						@endif							
						<label class="labels" for="">Others</label> <input type="text" name="initiatedby[initiatedby_others]" value="@if( Input::old('initiatedby')['initiatedby_others'] != ""){{ Input::old('initiatedby')['initiatedby_others'] }}@else{{ @$init['initiatedby_others'] }}@endif" />
					  </div>
						
				</div>

			</div>
			
			 <div class="clear_20"></div>
			 <div class="container-header"><h5 class="text-center lined sub-header"><strong>SPECIFICATIONS</strong></h5></div>
                    
			 <div class="clear_20"></div>

			<div class="clear_10"></div>

			<label class="pmarf_textarea_labels required">PROPOSAL OBJECTIVES</label>
			<div class="pmarf_textarea">
				<textarea style="max-height:90px" onkeyup="AutoGrowTextArea(this)" id="specs_proposal" name="specs_proposal" rows="2" cols="10" class="form-control">@if(Input::old('specs_proposal') != ""){{ Input::old('specs_proposal') }} @else{{ $request->pmarf_proposal_objective  }}@endif</textarea>
			</div>
			<div class="clear_10"></div>
			<label class="pmarf_textarea_labels required">BRIEF DESCRIPTION OF PROPOSAL</label>
			<div class="pmarf_textarea">
				<textarea style="max-height:180px"  onkeyup="AutoGrowTextArea(this)" id="specs_proposal_desc" name="specs_proposal_desc" rows="2" cols="10" class="form-control">@if(Input::old('specs_proposal_desc') != ""){{ Input::old('specs_proposal_desc') }}@else{{ $request->pmarf_proposal_description }}@endif</textarea>
			</div>
			<div class="clear_10"></div>
			<label class="pmarf_textarea_labels required">ADDITIONAL SUPPORT ACTIVITIES</label>
			<div class="pmarf_textarea">
				<textarea style="max-height:90px"  onkeyup="AutoGrowTextArea(this)" id="specs_additional_support" name="specs_additional_support" rows="2" cols="10" class="form-control">@if(Input::old('specs_additional_support') != ""){{ Input::old('specs_additional_support') }}@else{{ $request->pmarf_add_support_act }}@endif</textarea>
			</div>
			<div class="clear_20"></div>
			<label class="pmarf_textarea_labels required">PARTICIPATING BRANDS</label>
			<div class="pmarf_textarea">
				<table border = "1" cellpadding = "0" class="" id="tbl_brands">
					<th class="th_style quantity">QUANTITY</th>
					<th class="th_style quantity">UNIT</th>
					<th class="th_style">PRODUCT NAME</th>
					<?php $ctr = 0; ?>
					@if (Input::old('qty_'))
						@foreach(Input::old('qty_') as $qty_key => $qty_val)
						<tr class="tr_pb" id="nc">	
							<td class="td_style"><input  type="textbox" name="qty_[]" id="qty_1" class="quantity_textbox" value="{{ $qty_val }}"/></td>
							<td class="td_style"><input  type="textbox" name="unit_[]" id="unit_1" class="quantity_textbox" value="{{ Input::old('unit_')[$qty_key] }}" /></td>
							<td class="td_style"><input  type="textbox" name="product_[]" id="product_1" class="product_textbox" value="{{ Input::old('product_')[$qty_key] }}" /></td>
						</tr>							
						@endforeach
					@elseif($brands)
					@foreach ($brands as $row=>$aFlds)
						<?php 
							$qty     = "qty_".$row;
							$unit    = "unit_".$row;
							$product = "product_".$row;
							$ctr++;
						?>
						<tr class="tr_pb" id="nc">
							<td class="td_style"><input type="textbox" name="qty_[]" id="qty_{{ $ctr }}" class="quantity_textbox" value="{{ $aFlds[$qty] }}"/></td>
							<td class="td_style"><input type="textbox" name="unit_[]" id="unit_{{ $ctr }}" class="quantity_textbox" value="{{ $aFlds[$unit] }}" /></td>
							<td class="td_style"><input type="textbox" name="product_[]" id="product_{{ $ctr }}" class="product_textbox"  value="{{ $aFlds[$product] }}"/></td>
						</tr>
					@endforeach
					@else
						<tr class="tr_pb" id="nc">
							<td class="td_style"><input type="textbox" name="qty_[]" id="qty_{{ $ctr }}" class="quantity_textbox" /></td>
							<td class="td_style"><input type="textbox" name="unit_[]" id="unit_{{ $ctr }}" class="quantity_textbox" /></td>
							<td class="td_style"><input type="textbox" name="product_[]" id="product_{{ $ctr }}" class="product_textbox" /></td>
						</tr>
					@endif
				</table>
			</div>
			 <div class="pmarf_btns btn-group">
				<button type="button" id="brands_add" name="brands_add"  class="btn btn-xs btn2 btn-default" value="">ADD</button>
				<button type="button" id="brands_del" name="brands_del" class="btn btn-xs btn2 btn-default" value="">DELETE</button>
			</div>

			<div class="clear_20"></div>
			<div class="clear_20"></div>
			 <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
			<label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>

			<!--<div class="attachment_container">
				<div id="attachments">
					@if(Input::old('files'))
						@foreach(Input::old('files') as $a => $b)
						<p>
							{{ $b['original_filename'] }} | {{ FileSizeConverter::convert_size($b['filesize']) }}
							<input type="hidden" name="files[{{ $a }}][filesize]" value="{{ $b['filesize'] }}">
							<input type="hidden" name="files[{{ $a }}][mime_type]" value="{{ $b['mime_type'] }}">
							<input type="hidden" name="files[{{ $a }}][original_extension]" value="{{ $b['original_extension'] }}">
							<input type="hidden" name="files[{{ $a }}][original_filename]" value="{{ $b['original_filename'] }}">
							<input type="hidden" name="files[{{ $a }}][random_filename]" value="{{ $b['random_filename'] }}"> 
							<button class="btn btn-xs btn-danger remove-fn">DELETE</button>
						</p>
						@endforeach
					@endif
				</div>	
				<span class="btn btn-success btnbrowse fileinput-button">
					<span>BROWSE</span>
					<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
				</span>					
			</div>-->
			<div class="row">
				<div class="container">
					<div class="col-md-6">
						<div class="attachment_container">
							@if(count(json_decode($request['pmarf_attachments'])) > 0)
								@foreach(json_decode($request['pmarf_attachments']) as $attachment)
								<div>
								<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
								<a href="{{ URL::to('/pmarf/download/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
								<button class="remove-fn btn btn-xs btn-danger btn_del">DELETE</button><br />
								
								</div>
								@endforeach
							@endif
							<div id="attachments">
								@if(Input::old('files'))
									@foreach(Input::old('files') as $a => $b)
									<p>
										{{ $b['original_filename'] }} | {{ FileSizeConverter::convert_size($b['filesize']) }}
										<input type="hidden" name="files[{{ $a }}][filesize]" value="{{ $b['filesize'] }}">
										<input type="hidden" name="files[{{ $a }}][mime_type]" value="{{ $b['mime_type'] }}">
										<input type="hidden" name="files[{{ $a }}][original_extension]" value="{{ $b['original_extension'] }}">
										<input type="hidden" name="files[{{ $a }}][original_filename]" value="{{ $b['original_filename'] }}">
										<input type="hidden" name="files[{{ $a }}][random_filename]" value="{{ $b['random_filename'] }}"> 
										<button class="btn btn-xs btn-danger remove-fn">DELETE</button>
									</p>
									@endforeach
								@endif
							</div>
							<span class="btn btn-success btnbrowse fileinput-button">
								<span>BROWSE</span>
								<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}">
							</span>		   
						</div>
					</div>

					@if($request['pmarf_emp_id'] != Session::get('employee_id'))
						@if($request['pmarf_status'] != 'New' && $request['pmarf_status'] != 'For Approval' && $request['pmarf_status'] != 'Disapproved' )
						<div class="col-md-6">
							<label class="attachment_note"><strong>NPMD</strong></label><br/>
							<label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
							<div class="attachment_container">
								@if(count(json_decode($request['pmarf_npmd_attachments'])) > 0)
									@foreach(json_decode($request['pmarf_npmd_attachments']) as $attachment)
									<a href="{{ URL::to('/pmarf/download_npmd/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
									@endforeach
								@endif
							</div>
						</div>
						@endif
					@else
						@if($request['pmarf_status'] == 'Closed' || $request['pmarf_status'] == 'For Conforme' || ($request['pmarf_status'] == 'Returned' && count(json_decode($request['pmarf_npmd_attachments'])) > 0)  )
							<div class="col-md-6">
								<label class="attachment_note"><strong>NPMD</strong></label><br/>
								<div class="attachment_container">
									@if(count(json_decode($request['pmarf_npmd_attachments'])) > 0)
										@foreach(json_decode($request['pmarf_npmd_attachments']) as $attachment)
										<a href="{{ URL::to('/pmarf/download_npmd/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
										@endforeach
									@endif
								</div>
							</div>
						@endif
					@endif
					
				</div>
			</div>
			
	</div> <!-- end of form_container-->
	
	<div class="clear_20"></div>
	<div class="form_container"><span class="legend-action">ACTION</span>
		<div class="clear_10"></div>
		@if($request['pmarf_status'] == "Disapproved" || $request['pmarf_status'] == "Returned")
		<div class="textarea_messages_container">
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea name="comments" rows="3" class="form-control textarea_inside_width" readonly="readonly">@if( $request['pmarf_status'] != 'New')@if( $request['pmarf_comments'] != '')@foreach(json_decode($request['pmarf_comments']) as $comment)@if(@$comment->status != "draft"){{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10;@endif{{-- don't forget this hack --}}@endforeach{{-- don't forget this hack --}}@endif{{-- don't forget this hack --}}@endif</textarea>
			</div>
		</div>
		@endif
		
		<div class="clear_10"></div>
		<div class="textarea_messages_container">
			@if($request['pmarf_status'] == 'New')							
				<div class="row">
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea name="comments" rows="3" class="form-control textarea_inside_width">@if(Input::old('comments') != ""){{ Input::old('comments') }}@elseif( $request['pmarf_comments'] != '')@foreach(json_decode($request['pmarf_comments']) as $comment){{ $comment->message }}@endforeach{{-- don't forget this hack --}}@endif</textarea>
				</div>
			@else
				<div  class="row">
					<!--<label class="textarea_inside_label">COMMENT: {{ count(json_decode($request['pmarf_comments'])) - 1 }}</label>-->
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea name="comments" rows="3" class="form-control textarea_inside_width">@if(Input::old('comments')){{ Input::old('comments') }}@else @if(@json_decode($request['pmarf_comments'])[count(json_decode($request['pmarf_comments'])) - 1]->status == "draft") {{json_decode($request['pmarf_comments'])[count(json_decode($request['pmarf_comments'])) - 1]->message}}@endif @endif</textarea>
				</div>
			@endif
		</div>
		<div class="clear_10"></div>
		<div class="row">
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
				</div> 
				<div class="comment_button">
					<button type="submit" class="btn btn-default btndefault" name="action" value="update">SAVE</button>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="comment_container">
				<div class="comment_notes">
					@if(Session::get('desig_level') == 'employee')
						<label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
					@elseif(Session::get('desig_level') == 'supervisor')
						<label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR APPROVAL</label>
					@elseif(Session::get('desig_level') == 'head')
						<label class="button_notes"><strong>SEND</strong> TO NPMD HEAD FOR PROCESSING</label>
					@endif
				</div> 
				<div class="comment_button">
					<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="text-center">
				<a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
			</div>
		</div>
	</div><!-- end of form_container -->


<div class="modal fade" id="tooltip" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <i>Internal - NPMD will be the implementer/ manpower for the activity &#10;External - NPMD will supervise/ oversee the implementation of the activity by selected third party agency</i>
      </div>     
    </div>
  </div>
</div>

</form>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/pmarf.js') }}
{{ HTML::script('/assets/js/touchHover.js?') }}

<script type="text/javascript">
	var allowed_file_count = 10;
	var allowed_total_filesize = 20971520;
	$(function () {
	
		$("#btnAddAttachment").click(function(){
			if($("#attachments input:file").length < 10){
				$("#attachments").append("<div><input type='file' name='attachments[]' /><button class='remove_file_attachment btn btn-xs btn-danger btn_del'>DELETEs</button></div>");
			}
		});
		
		$(".remove-fn").live('click', function() {
			console.log("aaa");
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$(".remove_file_attachment").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$('#act_type').on('change', function() {
			  if ( this.value == 'Others'){
				console.log('tsts');
				$('#div_act_type').show();
			  }
			  else{
				$('#div_act_type').hide();
			  }
		});
		
		$('#req_activity_date').on('change', function() {
			  if ( this.value == 'Multiple days-Staggered'){
				$('#div_staggered').show();
			  }
			  else{
				$('#div_staggered').hide();
			  }
		});
		
		$('#req_implementer').on('change', function() {
			  if ( this.value == 'External'){
				$('#agency').css("color","#c80b31");
			  }
			  else{
				$('#agency').css("color","#383c40");
			  }
		});
		
		$("a.tooltipLink").tooltip();
		
		
	});

$(function()
	{
		$("#specs_proposal").trigger("keyup");
		$("#specs_proposal_desc").trigger("keyup");
		$("#specs_additional_support").trigger("keyup");
	})

function AutoGrowTextArea(textField)
		{
		  if (textField.clientHeight < textField.scrollHeight)
		  {
		    textField.style.height = textField.scrollHeight + "px";
		    if (textField.clientHeight < textField.scrollHeight)
		    {
		      textField.style.height = 
		        (textField.scrollHeight * 2 - textField.clientHeight) + "px";
		    }
		  }
		}
</script>
@stop