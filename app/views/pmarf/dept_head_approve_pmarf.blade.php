@extends('template/header')

@section('content')
<form class="form-inline" action="{{URL::current()}}" method="post" enctype="multipart/form-data">
	<input type="hidden" value="{{ csrf_token() }}" />
	<div class="form_container">
		<label class="form_title">PROMO AND MERCHANDISING ACTIVITY REQUEST FORM</label>
		<div class="row">
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">EMPLOYEE NAME:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" id="emp_name" name="emp_name" value="{{ $request->pmarf_emp_name }}" />
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">REFERENCE NUMBER:</label>
				</div>
				<div class="col2_form_container">
				   <input readonly="readonly" type="text" class="form-control" name="pmarf_refno" value="{{ $request->pmarf_ref_num }}" />
				   <input readonly="readonly" type="hidden" class="form-control" name="pmarf_id" value="{{ $request->pmarf_id }}" />
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">EMPLOYEE NUMBER:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="hidden" class="form-control" name="emp_id" value="{{ $request->pmarf_emp_id }}" />
					<input readonly="readonly" type="text" class="form-control" name="employees_id" value="{{ $request->pmarf_employees_id }}" />
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">DATE FILED:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control text-left" value="{{ $request->pmarf_date_filed }}" id="pmarf_date_filed" name="pmarf_date_filed"/>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">COMPANY:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_company }}" id="emp_company" name="emp_company"/>
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">STATUS:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_status }}" name="pmarf_status" id="pmarf_status"/>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">DEPARTMENT:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_department }}" name="emp_department" id="emp_department"/>
				</div>
			</div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels">CONTACT NUMBER:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" name="emp_contactno" id="emp_contactno" value="{{ $request->pmarf_contact_no }}" />
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels">SECTION:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control" value="{{ $request->pmarf_section }}" id="emp_section" name="emp_section" />
				</div>
			</div>
		</div><!--endoffirstrow-->
		
		<div class="clear_20"></div>
	 <div class="container-header"><h5 class="text-center lined sub-header"><strong>REQUEST DETAILS</strong></h5></div>
                    
		<div class="clear_20"></div>

		<div class="row">
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">TYPE OF ACTIVITY:</label>
				</div>
				<div class="col2_form_container">
					<select readonly="readonly" id='req_activity_type' name='req_activity_type' class="form-control" style="width: 241px" disabled>
						<option value="{{ $request->pmarf_activity_type }}">{{ $request->pmarf_activity_type }}</option>
						@foreach ($request as $aKey=>$aVal)
							@if ($request->pmarf_activity_type != $aVal)
								<option value="{{ $aVal }}">{{ $aVal }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			@if($request['pmarf_activity_type'] == "Others")
			<div class="row_form_container">
				<div class="col4_form_container">
					<label class="labels required">ACTIVITY DETAILS:</label>
				</div>
				<div class="col5_form_container">
					<input readonly="readonly" id='req_activity_details' name='req_activity_details' type="text" class="form-control" value="{{ $request->pmarf_activity_details }}"/>
				</div>
			</div>
			@endif

			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">DATE OF ACTIVITY:</label>
				</div>
				<div class="col2_form_container">
				   <select readonly="readonly" id='req_activity_date' name='req_activity_date' class="form-control" style="width: 241px" disabled>
						<option value="{{ $request->pmarf_activity_dates }}">{{ $request->pmarf_activity_dates }}</option>
						@foreach ($request as $sKey=>$sDates)
							@if ($request->pmarf_activity_dates != $sDates)
								<option value="{{ $sDates }}">{{ $request->pmarf_activity_dates }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			
			@if($request['pmarf_activity_dates'] == "Multiple days-Staggered")
			<div class="row_form_container div_staggered">
				<div class="col4_form_container">
					<label class="labels required">DATES:</label>
				</div>
				<div class="col5_form_container">
					<input readonly="readonly" id="req_date" name="req_date" type="text" class="form-control" value="{{$request->pmarf_multiple_dates }}"/>
				</div>
			</div>
			@endif
			
			<div class="clear_10"></div>

			@if($request['pmarf_activity_dates'] != "Multiple days-Staggered")
			<div class="row_form_container div_straight">
				<div class="col1_form_container">
					<label class="labels1 required">FROM:</label>
				</div>
				<div class="col2_form_container input-group">
					<input disabled type="text" class="form-control date_picker" name="req_from" id="req_from" value="{{ $request->pmarf_activity_from }}" />
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
				<div class="clear_10"></div>
				<div class="col1_form_container">
					<label class="labels1 required">TO:</label>
				</div>
				<div class="col2_form_container input-group">
					 <input disabled id="req_to" name="req_to" type="text" class="form-control date_picker" value="{{ $request->pmarf_activity_to }}"/>
					 <span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
			</div>
			<div class="row2_form_container div_straight_no">
				<div class="col4_form_container">
					<label class="labels">NO. OF DAYS:</label>
				</div>
				<div class="col6_form_container">
					 <input readonly="readonly" id="req_noOfDays" name="req_noOfDays" type="text" class="form-control"  value="{{ $request->pmarf_activity_no_of_days }}"/>
				</div>
			</div>
			@endif
			
			@if($request['pmarf_completion_date'] != "")
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">COMPLETION DATE:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" type="text" class="form-control date_picker" id="" name="completion_date" value="{{ $request->pmarf_completion_date }}"/>
				</div>
			</div>
			@endif
			
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">AREA COVERAGE:</label>
				</div>
				<div class="col2_form_container">
					<input readonly="readonly" id="req_area_coverage" name="req_area_coverage" type="text" class="form-control" value="{{ $request->pmarf_activity_area }}"/>
				</div>
			</div>
			
			<div class="clear_10"></div>
			<div class="row_form_container">
				<div class="col1_form_container">
					<label class="labels required">IMPLEMENTER:</label>
				</div>
				<div class="col2_form_container">
				   <select readonly="readonly" id='req_implementer' name='req_implementer' class="form-control" style="width: 241px" disabled>
						<option value="{{ $request->pmarf_implementer }}">{{ $request->pmarf_implementer }}</option>
						@foreach ($implementer as $sK=>$sImplementer)
							@if ($request->pmarf_implementer != $sImplementer)
								<option value="{{ $sImplementer }}">{{ $sImplementer }}</option>
							@endif
						@endforeach
					</select>
				</div>
				<a data-toggle="tooltip" title='Internal - NPMD will be the implementer/ manpower for the activity &#10;External - NPMD will supervise/ oversee the implementation of the activity by selected third party agency'>
				<span class="glyphicon glyphicon-question-sign" style="margin-left: 5px;margin-top: 7px"></span></a>
			</div>
			
			<div class="row_form_container">
				<div class="col4_form_container">
				@if($request['pmarf_implementer'] == "External")
					<label class="labels required">AGENCY NAME:</label>
				@else
					<label class="labels">AGENCY NAME:</label>
				@endif
				</div>
				<div class="col5_form_container agency_textbox">
					 <input readonly="readonly" id="req_agency_name" name="req_agency_name" type="text" class="form-control" value="{{ $request->pmarf_agency_name }}"/>
				</div>
			</div>
			
			<div class="clear_20"></div>
			<div class="row3_form_container">
				<div class="col7_form_container">
					<label class="labels required">INITIATED BY:</label>
				</div>
				<div class="col2_form_container">
					@if (@array_key_exists("distributor",$init))
						<input disabled type="checkbox" name="initiatedby[distributor]" value="Distributor" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[distributor]" value="Distributor">
					@endif
						<label class="labels" for="">Distributor</label>
						<br />

					@if (@array_key_exists("brand_manager",$init))					
						<input disabled type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[brand_manager]" value="Brand Manager">
					@endif
						<label class="labels" for="">Brand Management</label>
						<br />
						
					@if (@array_key_exists("ad_and_media", $init))	
						<input disabled type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[ad_and_media]" value="Ad and Media">
					@endif
						<label class="labels" for="">Ad and Media</label>
						<br />
				 </div>
				 
				 <div class="col2_form_container">
					@if (@array_key_exists("trade", $init))
						<input disabled type="checkbox" name="initiatedby[trade]" value="Trade" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[trade]" value="Trade">
					@endif
						<label class="labels" for="">Trade</label>
					<br />
					
					@if (@array_key_exists("strategic_corporate_development", $init))
						<input disabled type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[strategic_corporate_development]" value="Strategic Corporate Development">
					@endif
						<label class="labels" for="">Strategic Corporate Development</label>
						<br />
						
					@if (@array_key_exists("key_accounts_manager", $init))
						<input disabled type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager" checked="checked">
					@else
						<input disabled type="checkbox" name="initiatedby[key_accounts_manager]" value="Key Accounts Manager">
					@endif
						<label class="labels" for="">Key Accounts Manager</label>
						<br />
				 </div>
				  <div class="col2_form_container">
				  @if (@array_key_exists("others", $init))
					<input disabled type="checkbox" name="initiatedby[others]" value="Others" checked="checked">
				  @else
					<input disabled type="checkbox" name="initiatedby[others]" value="Others">
				  @endif
					<label class="labels" for="">Others</label> <input disabled type="text" name="initiatedby['initiatedby_others']" value="{{ @$init['initiatedby_others'] }}"/>
				  </div>
			</div>
		</div><!--endofsecondrow-->
			<div class="clear_20"></div>
			 <div class="container-header"><h5 class="text-center lined sub-header"><strong>SPECIFICATIONS</strong></h5></div>
                    
			<div class="clear_20"></div>
			
			<label class="pmarf_textarea_labels required printlabels">PROPOSAL OBJECTIVES</label>
			<div class="pmarf_textarea">
				<textarea id="specs_proposal" name=""  cols="10" class="form-control" disabled value="">{{ $request->pmarf_proposal_objective }} </textarea>
			</div>
			<div class="clear_10"></div>
			<label class="pmarf_textarea_labels required printlabels">BRIEF DESCRIPTION OF PROPOSAL / SPECIFICATIONS</label>
			<div class="pmarf_textarea">
				<textarea id="specs_proposal_desc" name=""  cols="10" class="form-control" disabled>  {{ $request->pmarf_proposal_description }} </textarea>
			</div>
			<div class="clear_10"></div>
			<label class="pmarf_textarea_labels required printlabels">ADDITIONAL SUPPORT ACTIVITIES</label>
			<div class="pmarf_textarea">
				<textarea id="specs_additional_support" name=""  cols="10" class="form-control" disabled>{{ $request->pmarf_add_support_act }}</textarea>
			</div>
				
			<div class="clear_20"></div>
			<label class="pmarf_textarea_labels required">PARTICIPATING BRANDS</label>
			<div class="pmarf_textarea">
				<table border = "1" cellpadding = "0" class="" id="tbl_brands">
					<th class="th_style quantity ">QUANTITY</th>
					<th class="th_style quantity">UNIT</th>
					<th class="th_style">PRODUCT NAME</th>
					@foreach ($brands as $row=>$aFlds)
						<?php 
							$qty     = "qty_".$row;
							$unit    = "unit_".$row;
							$product = "product_".$row;
						?>
						<tr>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $qty }}" id="{{ $qty }}" class="quantity_textbox" value="{{ $aFlds[$qty] }}"/></td>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $unit }}" id="{{ $unit }}" class="quantity_textbox" value="{{ $aFlds[$unit] }}" /></td>
							<td class="td_style"><input readonly="readonly" class="form-control" type="text" name="{{ $product }}" id="{{ $product }}" class="product_textbox"  value="{{ $aFlds[$product] }}"/></td>
						</tr>
					@endforeach
				</table>
			</div>
			
			<div class="clear_20"></div>
		 <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                    
			<div class="clear_20"></div>
			
			<div class="row">
				<div class="container">
					<div class="col-md-6">
						<label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label>
						<div class="attachment_container">
						@if(count(json_decode($request['pmarf_attachments'])) > 0)
							@foreach(json_decode($request['pmarf_attachments']) as $attachment)
							<div>
							<input type="hidden" name="current_files[]" value='{{ json_encode($attachment) }}' />
							<a href="{{ URL::to('/pmarf/download/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a>
							<!--<button class='remove-fn btn btn-xs btn-danger btn_del'>DELETE</button><br />-->
							</div>
							@endforeach
						@endif
						
						<div class="clear_10"></div>
						<div id="attachments"></div>
						<span class="btn btn-success btnbrowse fileinput-button">
							<span>BROWSE</span>
							<input id="fileupload" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
						</span>		   
					</div>
					</div>
					
					@if($request['pmarf_emp_id'] != Session::get('employee_id'))
						@if($request['pmarf_status'] != 'New' && count(json_decode($request['pmarf_npmd_attachments'])) > 0) 
						<div class="col-md-6">
							<label class="attachment_note"><strong>NPMD</strong></label><br/>
							<div class="attachment_container">
								@if(count(json_decode($request['pmarf_npmd_attachments'])) > 0)
									@foreach(json_decode($request['pmarf_npmd_attachments']) as $attachment)
									<a href="{{ URL::to('/pmarf/download/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
									@endforeach
								@endif
							</div>
						</div>
						@endif
					@else
						@if($request['pmarf_status'] == 'For Conforme')
							<div class="col-md-6">
								<label class="attachment_note"><strong>NPMD</strong></label><br/>
								<div class="attachment_container">
									@if(count(json_decode($request['pmarf_npmd_attachments'])) > 0)
										@foreach(json_decode($request['pmarf_npmd_attachments']) as $attachment)
										<a href="{{ URL::to('/pmarf/download/') . '/' . $request['pmarf_ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename }} | {{ FileSizeConverter::convert_size($attachment->filesize) }}</a><br />
										@endforeach
									@endif
								</div>
							</div>
						@endif
					@endif
					
				</div>
			</div>
	</div><!-- end of first form_container-->
	
	<div class="clear_20"></div>
	
	<div class="form_container"><span class="legend-action">ACTION</span>
		<div class="clear_10"></div>
		<div class="textarea_messages_container">
			@if($request['pmarf_status'] == 'Disapproved' || $request['pmarf_status'] == 'Returned')
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea name="" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['pmarf_comments'] != '')@foreach(json_decode($request['pmarf_comments']) as $comment)@if(@$comment->status != "draft"){{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10;@endif{{-- don't forget this hack --}}@endforeach{{-- don't forget this hack --}}@endif</textarea>
				

			</div>
			@else
			<div class="row">
				<label class="textarea_inside_label">MESSAGE:</label>
				<textarea name="" rows="3" class="form-control textarea_inside_width" disabled>@if( $request['pmarf_issent'] == 1 )@if( $request['pmarf_comments'] != '')@foreach(json_decode($request['pmarf_comments']) as $comment)@if(@$comment->status != "draft"){{ $comment->name }} {{ $comment->datetime }}: {{ $comment->message }}&#13;&#10;@endif{{-- don't forget this hack --}}@endforeach{{-- don't forget this hack --}}@endif{{-- don't forget this hack --}}@endif</textarea>
			</div>
			@endif
		</div>
		
		@if($request['pmarf_status'] == 'For Approval' || $request['pmarf_status'] == 'Returned')
			<div class="clear_10"></div>
			<div class="textarea_messages_container">
				<div class="row">
					<label class="textarea_inside_label">COMMENT:</label>
					<textarea name="comments" rows="3" class="form-control textarea_inside_width">{{ Input::old('comments') }}</textarea>
				</div>
			</div>
		@endif
		
		<div class="clear_10"></div>
		<div class="row">
			
			
			@if(Session::get('employee_id') != $request['pmarf_emp_id'] )
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes"><strong>APPROVE </strong>AND<strong> SEND</strong> TO NPMD HEAD FOR PROCESSING</label>
				</div> 
				<div class="comment_button">
					<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
				</div>
			</div>
			<div class="clear_10"></div>
			<div class="comment_container">
				<div class="comment_notes">
					<label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
				</div> 
				<div class="comment_button">
					<button type="submit" class="btn btn-default btndefault" name="action" value="return">RETURN</button>
				</div>
			</div>
			<div class="clear_10"></div>
			@else
				<div class="comment_container">
					<div class="comment_notes">
						<label class="button_notes"><strong> SEND</strong> TO NPMD HEAD FOR PROCESSING</label>
					</div> 
					<div class="comment_button">
						<button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
					</div>
				</div>
			@endif
			<div class="clear_10"></div>
			<div class="text-center">
				<a class="btn btn-default back_buttons_spacing" href="{{ URL::previous() }}">BACK</a>
			</div>
		</div>
		
	</div><!-- end of second form_container-->
	
</form>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/pmarf.js') }}
<script type="text/javascript">
	var allowed_file_count = 10;
	var allowed_total_filesize = 20971520;
	
	$(function () {
		$("#btnAddAttachment").click(function(){
			if($("#attachments p").length < 10){
				$("#attachments").append("<div><input type='file' name='attachments["+($("#attachments p").length + 1 )+"]' /><button class='remove_file_attachment btn-danger btn btn-xs' type='button'>DELETE</button></div>");
			}
		});
		
		$(".remove-fn").live('click', function() {
			var a = confirm("Are you sure you want to delete this attachment?");
			if (a == true) {
				$(this).parent().remove();
			}
			return a;
		});
		
		$('#act_type').on('change', function() {
			  if ( this.value == 'Others'){
				$('#div_act_type').show();
			  }
			  else{
				$('#div_act_type').hide();
			  }
		});
		
		$('#req_activity_date').on('change', function() {
			  if ( this.value == 'Multiple days-Staggered'){
				$('#div_staggered').show();
			  }
			  else{
				$('#div_staggered').hide();
			  }
		});
		
		$('#req_implementer').on('change', function() {
			  if ( this.value == 'External'){
				$('#agency').css("color","#c80b31");
			  }
			  else{
				$('#agency').css("color","#383c40");
			  }
		});
		
		$("a.tooltipLink").tooltip();
		
		
	});
</script>
@stop


