@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'os/create/action', 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">OFFICE SALES ORDER FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ date('Y-m-d') }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="NEW" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="contact" value="" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="section"  value="{{ Session::get('sect_name') }}" />
                        </div>
                    </div>

                    <div class="clear_20"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">OFFICE SALES TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" name="sales_type">
                                <option value="FOOD" selected>FOOD</option>
                                <option value="NON-FOOD" >NON-FOOD</option>
                            </select>
                        </div>
                    </div>

                    
                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>DELIVERY DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" name="delivery_to">
                                <option value=""></option>
                                <option value="RBC-Ortigas">RBC-Ortigas</option>
                                <option value="RBC-Novaliches" >RBC-Novaliches</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TELEPHONE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_number" class="form-control"  maxlength="50" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY ADDRESS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_address" class="form-control"  maxlength="50" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_remarks" class="form-control" maxlength="50"  />
                        </div>
                    </div>

                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>ORDER DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                       
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SALES TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select  class="form-control" name="type">
                                    <option value=""></option>
                                    @foreach($so_code as $code=>$fs)                                      
                                    <option value="{{ $code }}" >{{ $fs }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="office">
                    <thead>
                    <tr>
                        <th ><span class='labels2'>QUANTITY</span></th>
                        <th ><span class='labels2'>UNIT</span></th>
                        <th ><span class='labels2'>PRODUCT DESCRIPTION</span></th>
                        <th ><span class='labels2' >PRICE</span></th>
                        <th ><span class='labels2'>AMOUNT</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                <div style="background-color:#d9d9d9;border-top:1px solid black;">
                    <table>
                    <thead>
                    <tr>
                        <th ><span class='labels2'>TOTAL QUANTITY:</span></th>
                        <th ><input readonly class="form-control"  type="text" id="total-qty" /></th>
                        <th width="40%"></th>
                        <th ><span class='labels2'>TOTAL AMOUNT:</span></th>
                        <th ><input readonly class="form-control" type="text" id="total-amount"  /></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" value="">ADD</button>
                            <button type="button" name="edit" value="">EDIT</button>
                            <button type="button" name="delete" value="">DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

            <div class="note-food">
                <div>NOTE:</div>
                <div>*SCHEDULE FOR SENDING OFFICE SALES ORDER TO SSRV (<a href="#" data-toggle="modal" data-target="#reminder-modal" >CLICK HERE</a> )</div>
                <div>*LIMIT IN THE AMOUNT OF ORDER THAT MAY BE DEDUCTED FROM SALARY EMPLOYEE PER PAYROLL PERIOD IS <b>P2,000.00</b></div>             
                <div>ORDERS EXCEEDING SAID AMOUNT SHALL BE PAID IN CASH</div>
                <div>FOR <b>CASH PAYMENTS</b>, AMOUNT OF CASH TO BE SENT TO SSRV SHALL BE NOTED IN THE <b>COMMENT</b> FIELD.</div>
                <div>CASH PAYMENTS MUST BE FORWARDED TO SSRV SAME DAY OF ORDERING.</div>
                <div>FOR <b>EMERGENCY ORDERS</b>, THE EMPLOYEE IS REQUIRED TO SUBMIT A WRITTEN LETTER ADDRESSED TO SSRV HEAD STATING THE REASON FOR SUCH.</div>
            </div>

            <div class="note-non-food hide">
                <div>NOTE:</div>
                <div>*SCHEDULE FOR SENDING OFFICE SALES ORDER TO SSRV (<a href="#" data-toggle="modal" data-target="#reminder-modal" >CLICK HERE</a>)</div>
                <div>*LIMIT IN THE AMOUNT OF ORDER THAT MAY BE DEDUCTED FROM SALARY EMPLOYEE PER PAYROLL PERIOD IS <b>P2,000.00</b></div>             
                <div>ORDERS EXCEEDING SAID AMOUNT SHALL BE PAID IN CASH</div>
                <div>FOR <b>CASH PAYMENTS</b>, AMOUNT OF CASH TO BE SENT TO CHRD – ADMIN SHALL BE NOTED IN THE <b>REMARK</b> FIELD.</div>
                <div>CASH PAYMENTS MUST BE FORWARDED ON THE SAME DAY OF ORDERING.</div>                
                <div>FOR <b>EMERGENCY ORDERS</b>, THE EMPLOYEE IS REQUIRED TO SUBMIT A WRITTEN LETTER ADDRESSED TO SSRV HEAD STATING THE REASON FOR SUCH.</div>
            </div>


        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

<div class="modal fade" id="office-food-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel-food">ADD OFFICE SALES DETAIL</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-4"><label class="labels">COMPANY </label></div>
                <div class="col-sm-8">
                    <select class="form-control" id="food-company"  >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option  value="{{ $rs->comp_code }}">{{ $rs->comp_name }}</option>
                        @endforeach                               
                    </select>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-4"><label class="labels">SEARCH </label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="food-search" />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6 text-center"><label class="labels required">PRODUCT </label></div>
                <div class="col-sm-6 text-center">
                    <label class="labels required">UNIT OF MEASUREMENT </label>
                </div>
              </div>

              <div class="clear_20"></div>

    
              <div class="row">
                <div class="col-sm-6">
                    <select class="form-control"  size="15" id="food-product">
                        @foreach($food as $code=>$fs)                                      
                        <option comp_code="{{ $fs["company"] }}" def_code="{{ $fs["def_code"] }}" uom_code="{{ $fs["uom_code"] }}" uom_name ="{{ $fs["uom_name"] }}" price="{{ $fs["selling_price"] }}"  value="{{ $code }}" >{{ $fs["name"] }}</option>
                        @endforeach                               
                    </select>
                </div>

                <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-12">
                            <select class="form-control" id="food-uom" ></select>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">QUANTITY </label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control isNumeric" id="food-qty" maxlength="5" />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12  text-center">
                            <label class="labels required">PRICE</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="food-price" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">AMOUNT</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="food-amount" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save_food" >Save</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="labels2">SCHEDULE OF SUBMISSION OF ORDERS TO SSRV</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row " style="border:1px solid black;padding:5px;">
       

              <div class="clear_10"></div>

    
              <div class="row">
                <div class="col-sm-6">
                        <input disabled style="height:50px;" type="button" class="form-control labels2" value="COMPANY  / EMPLOYEES" />
                </div>

                <div class="col-sm-6">
                        <button disabled="disabled" style="height:50px;" class="form-control labels2" >SUBMISSION SCHEDULE OF OFFICE SALES <br> ORDERS</button>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    NOVALICHES Corporate & RBC Satellite Office Employees
               </div>

                <div class="col-sm-6">
                    Thursday-Friday,8:00am-4:00pm <i>except for emergency or special cases (e.g. bereavement, other fortuitous events)</i>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    Ortigas Office Employee <sup>*</sup>
               </div>

                <div class="col-sm-6">
                    Wednesday-Thursday, 8:00am-4:00pm 
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    SFI and PFI Office Employees
               </div>

                <div class="col-sm-6">
                    Thursday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    MFC Office Employees
               </div>

                <div class="col-sm-6">
                    Wednesday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    SPI Office Employees
               </div>

                <div class="col-sm-6">
                    Monday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    BUK Office Employees
               </div>

                <div class="col-sm-6">
                    Friday, 8:00am-11:00am <i>except for emergeny or special case (e.g. bereavement, other furtuitous events)</i>
                </div>
              </div>

          </div>
      </div>

      <div class="text-left  " style="padding:15px"> 
       <i><b>*Ortigas-based employees</b> shall indicate in the Remarks field whether their orders are to be delivered to Ortigas or to be picked up personally from the Office Sales Warehouse (RBC Main-Nova)</i>
      </div>


    </div>
  </div>
</div>

<div class="modal fade" id="office-non-food-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel-non-food">ADD OFFICE SALES DETAIL</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
                <div class="row">
                <div class="col-sm-4"><label class="labels">COMPANY </label></div>
                <div class="col-sm-8">
                    <select class="form-control" disabled="" >
                        <option value="" ></option>                            
                    </select>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-4"><label class="labels">SEARCH </label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="non-food-search" />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6 text-center"><label class="labels required">PRODUCT </label></div>
                <div class="col-sm-6 text-center">
                    <label class="labels required">UNIT OF MEASUREMENT </label>
                </div>
              </div>

              <div class="clear_20"></div>

    
              <div class="row">
                <div class="col-sm-6">
                    <select class="form-control"  size="15" id="non-food-product">
                        @foreach($non_food as $code=>$fs)                                      
                        <option  def_code="{{ $fs["uom_code"] }}" uom_code="{{ $fs["uom_code"] }}" uom_name ="{{ $fs["uom_name"] }}" price="{{ $fs["selling_price"] }}"  value="{{ $code }}" >{{ $fs["name"] }}</option>
                        @endforeach                               
                    </select>
                </div>

                <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-12">
                            <select class="form-control" id="non-food-uom" ></select>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">QUANTITY </label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-qty" maxlength="5" />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12  text-center">
                            <label class="labels required">PRICE</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-price" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>

                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">AMOUNT</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-amount" readonly />
                        </div>
                     </div>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save_non_food" >Save</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
      </div>
     <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/create.js') }}
@stop