@extends('template/header')

@section('content')
    {{ Form::open(array('url' => "os/non-food/edit/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">ADD PROJECT</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PROJECT OWNER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="owner" maxlength="100"  value="{{ $record['owner'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PROJECT CODE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" readonly name="code" value="{{ $record['code'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">CHRD-IN-CHARGE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" readonly name="incharge" value="{{ $record['incharge'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                 
                    <div class="row3_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PROJECT DESCRIPTION</label>
                        </div>
                        <div class="row10_form_container">
                            <input type="text" class="form-control" name="desc" value="{{ $record['name'] }}" maxlength="100" />
                        </div>
                    </div>
                            
                </div>
            
            <div class="clear_20 "></div>
          
            
            <div class="row ">            



                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels required">SELLING PRICE:</label>
                        </div>
                        <div class="col4_form_container">
                           &nbsp;
                        </div>
                    </div>

                    <div class="col1_form_container">
                        <div class="">
                            <label class="labels required">ALLOWABLE PAYMENT TERMS</label>
                        </div>
                    </div>    

                     <div class="">
                        <div class="col7_form_container">
                            <label class="labels required">PROJECT STATUS: </label>
                        </div>
                        <div class="col1_form_container">
                            <select class="form-control" name="status">
                                <option {{ $record['status'] == 1 ? "selected" : "" }} value ="1">Active</option>
                                <option  {{ $record['status'] == 0 ? "selected" : "" }} value="0">Inactive</option>
                            </select>
                        </div>
                    </div>                
            
                    <div class="clear_10"></div>
                    
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels required">FROM :</label>
                        </div>
        
                        <div class="col4_form_container input-group">
                            <input type="text" class="date_picker form-control" name="selling_from" id="selling_from"  value="{{ $record['selling_from'] }}" />
                             <label class="input-group-addon btn" for="selling_from">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label> 
                        </div>
                    </div>

                    <div class="col1_form_container">
                        <div class="">
                            <input  type="checkbox"  {{ isset($record['cash']) ? "checked" : "" }} name="allowable[]" value="cash" />
                            <label class="labels">&nbsp;&nbsp;CASH</label>
                        </div>
                    </div>  

                    <div class="">
                        <div class="col7_form_container">
                            <label class="labels required">MAXIMUM ORDER LIMIT: </label>
                        </div>
                        <div class="col1_form_container">
                            <input  type="text" class="form-control isDecimal" name="maximum" maxlength="8"  value="{{ $record['limit'] }}"  />
                        </div>
                    </div>                     
            
                    <div class="clear_10"></div>
                    
                   <div class="">
                        <div class="col1_form_container">
                            <label class="labels required">TO :</label>
                        </div>
                 
                        <div class="col4_form_container input-group">
                            <input type="text" class="date_picker form-control" id="selling_to"  name="selling_to" value="{{ $record['selling_to'] }}" />
                             <label class="input-group-addon btn" for="selling_to">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label> 
                        </div>
                    </div>

                    <div class="col1_form_container">
                        <div class="">
                            <input  type="checkbox"  {{ isset($record['charge']) ? "checked" : "" }} name="allowable[]" value="charge" />
                            <label class="labels">&nbsp;&nbsp;CHARGE</label>
                        </div>
                    </div>  
                    
          </div>

                <div class="clear_20"></div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" value="">ADD ITEM</button>
                        </div>
                </div>

                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="item">
                    <tr>
                        <th align="center" ><span class='labels2'>ITEM CODE</span></th>
                        <th align="center"><span class='labels2'>ITEM DESCRIPTION</span></th>
                        <th align="center"><span class='labels2'>UNIT OF MEASURE</span></th>
                        <th align="center" ><span class='labels2' >SELLING PRICE</span></th>
                        <th align="center"><span class='labels2'>ITEM STATUS</span></th>
                    </tr>
                    @foreach( $record['item-code'] as $index => $code )
                    <tr>
                        <td><input type="hidden" name="item-code[]" value="{{ $code }}" />{{ $code }}</td>
                        <td><input type="hidden" name="item-desc[]" value="{{ $record['item-desc'][$index] }}" />{{ $record['item-desc'][$index] }}</td>
                        <td><input type="hidden" name="uom-code[]" value="{{ $record['uom-code'][$index] }}" />{{ $record['uom-label'][$index] }}</td>
                        <td><input type="hidden" name="selling-price[]" value="{{ $record['selling-price'][$index] }}" />{{ $record['selling-price'][$index] }}</td>
                        <td><input type="hidden" name="item-status[]" value="{{ $record['item-status'][$index] }}" />{{ $record['item-stats'][$index] }}</td>
                    </tr>
                    @endforeach
                </table>
                </div>

                <div class="clear_10"></div>

                <div class="row"> 
                            <div class="text-center">
                                <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                                <a href="{{ url::to("os/non-food/report") }}" class="btn btn-default btndefault" >CANCEL</a>
                            </div>
                    </div>
                                
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

<div class="modal fade" id="product-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">ADD ITEM</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-3"><label class="labels required">ITEM CODE: </label></div>
                <div class="col-sm-9">
                      <input type="text" class="form-control" id="item-code" maxlength="6" />
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">ITEM DESCRIPTION: </label></div>
                <div class="col-sm-9">
                      <input type="text" class="form-control" id="item-desc" maxlength="100" />
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">UOM CODE: </label></div>
                <div class="col-sm-9">
                      <select class="form-control" id="uom-code">
                        <option value="" ></option>
                        @foreach($uom as $key=>$val)                                      
                        <option  value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">SELLING PRICE: </label></div>
                <div class="col-sm-9">
                      <input type="text" class="form-control isDecimal" id="selling-price" maxlength="10" />
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">ITEM STATUS: </label></div>
                <div class="col-sm-9">
                      <select class="form-control" id="item-status" >
                        <option></option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                      </select>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save" >Save</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>        

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/non-food/create.js?V4') }}
@stop