@extends('template/header')

@section('content')

<form class="form-inline" method="post" enctype="multipart/form-data">
    <input type="hidden" value="{{ csrf_token() }}">

    <label class="form_label pm_form_title">PRODUCT BUILD-UP FOOD</label>
               
    <div class="text-left">
            <a href="{{ url::to("os/food/create") }}" class="btn btn-default"  >ADD PRODUCT</a>
     </div>

    <div class="clear_10"></div>                 
    <div class="row">  
        <div class="datatable_holder">
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable">
                <thead>
                    <tr role="row"><th class="" role="columnheader" rowspan="1" colspan="7">&nbsp;</th></tr>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CODE</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="200">PRODUCT NAME</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">UNIT OF MEASUREMENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"></th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs["code"] }}</td>  
                    <td>{{ $rs["name"] }}</td>  
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["uom"] }}</td>  
                    <td>{{ $rs["status"] }}</td> 
                    <td>{{ $rs["id"] }}</td> 
                    </tr>
                    <?php if(!isset($arr[$rs["code"]])): $arr[$rs["code"]] = $filter[] = $rs["code"];endif;?>
                    <?php if(!isset($arr[$rs["name"]])): $arr[$rs["name"]] = $filter[] = $rs["name"];endif;?>
                    <?php if(!isset($arr[$rs["company"]])): $arr[$rs["company"]] = $filter[] = $rs["company"];endif;?>
                    <?php if(!isset($arr[$rs["uom"]])): $arr[$rs["uom"]] = $filter[] = $rs["uom"];endif;?>
                    <?php if(!isset($arr[$rs["status"]])): $arr[$rs["status"]] = $filter[] = $rs["status"];endif;?>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  
    </div>

    <div class="clear_20"></div>  

    
</form>


@stop
@section('js_ko')
<script>
var available_tags= {{ str_replace("{}","[]",json_encode(isset($filter) ? $filter : [] )) }}
</script>
{{ HTML::script('/assets/js/os/food/report.js') }}
@stop