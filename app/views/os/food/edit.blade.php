@extends('template/header')

@section('content')
	{{ Form::open(array('url' => "os/food/edit/action/$id", 'method' => 'post')) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">EDIT PRODUCT</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">CODE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="code" maxlength="15" value="{{ $record['prod_code'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PRODUCT NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" maxlength="50" name="name"  value="{{ $record['prod_name'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="form-control" name="company"  >
		                        <option value="" ></option>
		                        @foreach($company as $rs)                                      
		                        <option {{ $record['comp_code'] == $rs->comp_code ? "selected" : "" }} value="{{ $rs->comp_code }}">{{ $rs->comp_code }}</option>
		                        @endforeach                               
		                    </select>
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEFAULT UOM:</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="form-control" name="measure"  >
		                        <option value="" ></option>
		                        @foreach($uom as $key=>$val)                                      
		                        <option {{ $record['def_uom_code'] == $key ? "selected" : "" }}  value="{{ $key }}">{{ $val }}</option>
		                        @endforeach                               
		                    </select>
                        </div>
                    </div>
              

                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <select class="form-control" name="status" >
		                        <option value="" ></option>
		                        <option {{ $record['status'] == "Y" ? "selected" : "" }} value="Y" >Active</option>
		                        <option {{ $record['status'] == "N" ? "selected" : "" }} value="N" >Inactive</option>                             
		                    </select>
                        </div>
                    </div>

                    @foreach( $record['uom'] as $index => $measure )
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">UOM {{ $index + 1 }}:</label>
                        </div>
                        <div class="col2_form_container">
                             <select class="form-control" name="uom[]" id="uom-cmb">
		                        <option value="" ></option>
		                        @foreach($uom as $key=>$val)                                      
		                        <option {{ $measure == $key ? "selected" : "" }} value="{{ $key }}">{{ $val }}</option>
		                        @endforeach                               
		                    </select>
                        </div>
                    </div>

                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">FACTORY PRICE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control isDecimal" name="factory[]" value="{{ $record['fprice'][$index]  }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">SELLING PRICE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control isDecimal" name="selling[]" value="{{ $record['sprice'][$index]  }}" />
                        </div>
                    </div>
                    @endforeach

                    <div id="uom-list">

                    </div>

                    <div class="clear_20"></div>
                  	<div class="row"> 
		                    <div class="text-center">
		                        <button type="button" id="add-uom" class="btn btn-default btndefault" >ADD UOM</button>
                                <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
		                        <a href="{{ url::to("os/food/report") }}" class="btn btn-default btndefault" >CANCEL</a>
                              <!--  <button type="submit" class="btn btn-default btndefault" name="action" value="delete">DELETE</button>                                -->
		                    </div>
		            </div>
                           
                                   
                </div>
            

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        
    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/food/create.js') }}
@stop