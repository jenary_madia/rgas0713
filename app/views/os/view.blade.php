@extends('template/header')

@section('content')
    {{ Form::open(array('url' => "os/view/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">OFFICE SALES ORDER FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $readonly }} type="text" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $readonly }} type="text" class="form-control" name="section"  value="{{ $record['section'] }}" />
                        </div>
                    </div>

                    <div class="clear_20"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">OFFICE SALES TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  {{ $readonly }} class="form-control" name="sales_type">
                                <option {{ $record['sales_type'] == "FOOD" ? "selected" : "" }} value="FOOD" selected>FOOD</option>
                                <option {{ $record['sales_type'] == "NON-FOOD" ? "selected" : "" }} value="NON-FOOD" >NON-FOOD</option>
                            </select>
                        </div>
                    </div>

                    
                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>DELIVERY DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <select {{ $readonly }} class="form-control" name="delivery_to">
                                <option value=""></option>
                                <option {{ $record['deliver_to'] == "RBC-Ortigas" ? "selected" : "" }} value="RBC-Ortigas">RBC-Ortigas</option>
                                <option {{ $record['deliver_to'] == "RBC-Novaliches" ? "selected" : "" }} value="RBC-Novaliches" >RBC-Novaliches</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TELEPHONE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $readonly }} type="text" name="delivery_number" class="form-control" value="{{ $record['delivery_tel'] }}" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY ADDRESS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $readonly }} type="text" name="delivery_address" class="form-control" value="{{ $record['delivery_add'] }}" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $readonly }} type="text" name="delivery_remarks" class="form-control" value="{{ $record['delivery_add'] }}" />
                        </div>
                    </div>

                     </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>ORDER DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                       
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SALES TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select  {{ $readonly }} class="form-control" name="type">
                                    <option value=""></option>
                                    @foreach($so_code as $code=>$fs)                                      
                                    <option {{ $record['so_code'] == $code ? "selected" : "" }} value="{{ $code }}" >{{ $fs }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="office">
                    <thead>
                    <tr>
                        <th ><span class='labels2'>QUANTITY</span></th>
                        <th ><span class='labels2'>UNIT</span></th>
                        <th ><span class='labels2'>PRODUCT DESCRIPTION</span></th>
                        <th ><span class='labels2' >PRICE</span></th>
                        <th ><span class='labels2'>AMOUNT</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $total_qty = array();
                        $total_amount = array();
                    ?>
                    @foreach($record["detail"] as $rs)
                    @if(isset($rs->selling_from))
                    @if($rs->selling_from <= date("Y-m-d") && $rs->selling_to >= date("Y-m-d"))
                    <tr>    
                       <td class="text-center" ><input type="hidden" name="qty[]" value="{{ $rs->order_qnty }}" >{{ $total_qty[] = $rs->order_qnty }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="uom[]" value="{{ $rs->uom_code }}" >{{ $uom[$rs->uom_code] }}</td>                                              
                       <td><input type="hidden" name="product[]" value="{{ $rs->prod_code }}" >{{ $rs->prod_name }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{  $rs->price }}</td>     
                       <td class="text-center" ><input type="hidden" name="amount[]" value="{{ $rs->price * $rs->order_qnty }}" >{{ $total_amount[] = $rs->price * $rs->order_qnty }}</td>                                                                       
                    </tr>
                    @endif
                    @else
                    <tr>    
                       <td class="text-center" ><input type="hidden" name="qty[]" value="{{ $rs->order_qnty }}" >{{ $total_qty[] = $rs->order_qnty }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="uom[]" value="{{ $rs->uom_code }}" >{{ $uom[$rs->uom_code] }}</td>                                              
                       <td><input type="hidden" name="product[]" value="{{ $rs->prod_code }}" >{{ $rs->prod_name }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{  $rs->price }}</td>     
                       <td class="text-center" ><input type="hidden" name="amount[]" value="{{ $rs->price * $rs->order_qnty }}" >{{ $total_amount[] = number_format($rs->price * $rs->order_qnty,2, ".","") }}</td>                                                                       
                    </tr>
                    @endif
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div style="background-color:#d9d9d9;border-top:1px solid black;">
                    <table>
                    <thead>
                    <tr>
                        <th ><span class='labels2'>TOTAL QUANTITY:</span></th>
                        <th ><input value="{{ array_sum($total_qty) }}" readonly class="form-control"  type="text" id="total-qty" /></th>
                        <th width="40%"></th>
                        <th ><span class='labels2'>TOTAL AMOUNT:</span></th>
                        <th ><input value="{{ number_format(array_sum($total_amount),2) }}" readonly class="form-control" type="text" id="total-amount"  /></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button {{ $readonly }} type="button" name="add" value="">ADD</button>
                            <button {{ $readonly }} type="button" name="edit" value="">EDIT</button>
                            <button {{ $readonly }} type="button" name="delete" value="">DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

            <div class="note-food {{ $record['sales_type'] == "FOOD" ? "" : "hide" }}">
                <div>NOTE:</div>
                <div>*SCHEDULE FOR SENDING OFFICE SALES ORDER TO SSRV (<a>CLICK HERE</a>)</div>
                <div>*LIMIT IN THE AMOUNT OF ORDER THAT MAY BE DEDUCTED FROM SALARY EMPLOYEE PER PAYROLL PERIOD IS <b>P2,000.00</b></div>             
                <div>ORDERS EXCEEDING SAID AMOUNT SHALL BE PAID IN CASH</div>
                <div>FOR <b>CASH PAYMENTS</b>, AMOUNT OF CASH TO BE SENT TO SSRV SHALL BE NOTED IN THE <b>COMMENT</b> FIELD.</div>
                <div>CASH PAYMENTS MUST BE FORWARDED TO SSRV SAME DAY OF ORDERING.</div>
                <div>FOR <b>EMERGENCY ORDERS</b>, THE EMPLOYEE IS REQUIRED TO SUBMIT A WRITTEN LETTER ADDRESSED TO SSRV HEAD STATING THE REASON FOR SUCH.</div>
            </div>

            <div class="note-non-food {{ $record['sales_type'] == "NON-FOOD" ? "" : "hide" }}">
                <div>NOTE:</div>
                <div>*SCHEDULE FOR SENDING OFFICE SALES ORDER TO SSRV (<a href="#" data-toggle="modal" data-target="#reminder-modal" >CLICK HERE</a>)</div>
                <div>*LIMIT IN THE AMOUNT OF ORDER THAT MAY BE DEDUCTED FROM SALARY EMPLOYEE PER PAYROLL PERIOD IS <b>P2,000.00</b></div>             
                <div>ORDERS EXCEEDING SAID AMOUNT SHALL BE PAID IN CASH</div>
                <div>FOR <b>CASH PAYMENTS</b>, AMOUNT OF CASH TO BE SENT TO CHRD – ADMIN SHALL BE NOTED IN THE <b>REMARK</b> FIELD.</div>
                <div>CASH PAYMENTS MUST BE FORWARDED ON THE SAME DAY OF ORDERING.</div>                
                <div>FOR <b>EMERGENCY ORDERS</b>, THE EMPLOYEE IS REQUIRED TO SUBMIT A WRITTEN LETTER ADDRESSED TO SSRV HEAD STATING THE REASON FOR SUCH.</div>
            </div>


        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                <div class="clear_10"></div> 
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea {{ $record['status'] != "Ordered" && $readonly ? "disabled" : "" }} rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            @if( ($record['status'] == "Ordered" ) )                
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>CANCEL</strong> ORDER</label>
                        </div> 
                        <div class="comment_button" style="padding-left:170px;" >
                            <button type="submit" style="width:120px" class="btn btn-default btndefault" name="action" value="cancel">CANCEL REQUEST</button>
                        </div>
                    </div>
                </div>
            @endif
            @include('cc/template/back')
        </div><!-- end of form_container -->
<div class="modal fade" id="reminder-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="labels2">SCHEDULE OF SUBMISSION OF ORDERS TO SSRV</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row " style="border:1px solid black;padding:5px;">
       

              <div class="clear_10"></div>

    
              <div class="row">
                <div class="col-sm-6">
                        <input disabled style="height:50px;" type="button" class="form-control labels2" value="COMPANY  / EMPLOYEES" />
                </div>

                <div class="col-sm-6">
                        <button disabled="disabled" style="height:50px;" class="form-control labels2" >SUBMISSION SCHEDULE OF OFFICE SALES <br> ORDERS</button>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    NOVALICHES Corporate & RBC Satellite Office Employees
               </div>

                <div class="col-sm-6">
                    Thursday-Friday,8:00am-4:00pm except for emergency or special cases (e.g. bereavement, other fortuitous events)
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    Ortigas Office Employee <sup>*</sup>
               </div>

                <div class="col-sm-6">
                    Wednesday-Thursday, 8:00am-4:00pm 
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    SFI and PFI Office Employees
               </div>

                <div class="col-sm-6">
                    Thursday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    MFC Office Employees
               </div>

                <div class="col-sm-6">
                    Wednesday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    SPI Office Employees
               </div>

                <div class="col-sm-6">
                    Monday
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-6">
                    BUK Office Employees
               </div>

                <div class="col-sm-6">
                    Friday, 8:00am-11:00am except for emergeny or special case (e.g. bereavement, other furtuitous events)
                </div>
              </div>

          </div>
      </div>

      <div class="text-left  " style="padding:15px"> 
       <i><b>*Ortigas-base employees</b> shall indicate in the Remarks field whether their orders are to be delivered to Ortigas or to be picked up personally from the Office Sales Warehouse (RBC Main-Nova)</i>
      </div>


    </div>
  </div>
</div>

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/create.js') }}
@stop