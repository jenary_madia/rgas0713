@extends('template/header')

@section('content')

<div id="wrap">
 
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">

    <div class="">   
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY OFFICE SALES</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>                        
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["trans_date"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["requestedby"] }}</td>                      
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

    <div class="clear_20"></div>   

    @if(Session::get("is_office_receiver") || Session::get("is_office_receiver_non_food"))            
    <div class="">   
           
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company1 form-control" name="" >
                    <option value="">All</option>
                    @foreach($company_arr as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    

        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department1 form-control" name="" >
                    <option value="">All</option>
                    @foreach($department as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >TO BE RECEIVED FOR EMPLOYEES</span>
            <table id="myrecord1" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                         <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record_employee as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["trans_date"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["department"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

    <div class="clear_20"></div>   

    @if(Session::get("is_office_receiver") != "BUK") 
    <div class="">   
           
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company2 form-control" name="" >
                    <option value="">All</option>
                    @foreach($company_arr_aff as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >TO BE RECEIVED FOR AFFILIATES</span>
            <table id="myrecord2" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                         <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                         
                </thead>

                <tbody>
                    
                     @foreach($record_affiliate as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["trans_date"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["department"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>
        @endif

    </div>

    <div class="clear_20"></div>   
    @endif
</div>
</div>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/report.js') }}
@stop