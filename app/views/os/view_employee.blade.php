@extends('template/header')

@section('content')
	{{ Form::open(array('url' => "os/edit/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">OFFICE SALES ORDER FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['emp_name'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ $record['emp_no'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['company'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ $record['department'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" class="form-control" name="section"  value="{{ $record['section'] }}" />
                        </div>
                    </div>

                    <div class="clear_20"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">OFFICE SALES TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" disabled  name="sales_type">
                                <option {{ $record['sales_type'] == "FOOD" ? "selected" : "" }} value="FOOD" selected>FOOD</option>
                                <option {{ $record['sales_type'] == "NON-FOOD" ? "selected" : "" }} value="NON-FOOD" >NON-FOOD</option>
                            </select>
                        </div>
                    </div>

                    
                  </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>DELIVERY DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" disabled  name="delivery_to">
                                <option value=""></option>
                                <option {{ $record['deliver_to'] == "RBC-Ortigas" ? "selected" : "" }} value="RBC-Ortigas">RBC-Ortigas</option>
                                <option {{ $record['deliver_to'] == "RBC-Novaliches" ? "selected" : "" }} value="RBC-Novaliches" >RBC-Novaliches</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TELEPHONE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_number" class="form-control" value="{{ $record['delivery_tel'] }}" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY ADDRESS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_address" class="form-control" value="{{ $record['delivery_add'] }}" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_remarks" class="form-control" value="{{ $record['delivery_add'] }}" />
                        </div>
                    </div>

                    </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>ORDER DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                       
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SALES TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select  class="form-control" disabled  name="type">
                                    <option value=""></option>
                                    @foreach($so_code as $code=>$fs)                                      
                                    <option {{ $record['so_code'] == $code ? "selected" : "" }} value="{{ $code }}" >{{ $fs }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="office">
                    <thead>
                    <tr>
                        <th ><span class='labels2'>QUANTITY</span></th>
                        <th ><span class='labels2'>UNIT</span></th>
                        <th ><span class='labels2'>PRODUCT DESCRIPTION</span></th>
                        <th ><span class='labels2' >PRICE</span></th>
                        <th ><span class='labels2'>AMOUNT</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $total_qty = array();
                        $total_amount = array();
                    ?>
                    @foreach($record["detail"] as $rs)
                    <tr>    
                       <td class="text-center" ><input type="hidden" name="qty[]" value="{{ $rs->order_qnty }}" >{{ $total_qty[] = $rs->order_qnty }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="uom[]" value="{{ $rs->uom_code }}" >{{ $uom[$rs->uom_code] }}</td>                                              
                       <td><input type="hidden" name="product[]" value="{{ $rs->prod_code }}" >{{ $rs->prod_name }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{ $rs->price }}</td>     
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{ $total_amount[] = $rs->price * $rs->order_qnty }}</td>                                                                       
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div style="background-color:#d9d9d9;border-top:1px solid black;">
                    <table>
                    <thead>
                    <tr>
                        <th ><span class='labels2'>TOTAL QUANTITY:</span></th>
                        <th ><input value="{{ array_sum($total_qty) }}" readonly class="form-control"  type="text" id="total-qty" /></th>
                        <th width="40%"></th>
                        <th ><span class='labels2'>TOTAL AMOUNT:</span></th>
                        <th ><input value="{{ number_format(array_sum($total_amount),2) }}" readonly class="form-control" type="text" id="total-amount"  /></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button disabled type="button" name="add" value="">ADD</button>
                            <button disabled type="button" name="edit" value="">EDIT</button>
                            <button disabled type="button" name="delete" value="">DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                <div class="clear_10"></div> 
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>

            @include('cc/template/back')
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop