@extends('template/header')

@section('content')
	{{ Form::open(array('url' => "os/process/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">OFFICE SALES ORDER FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">AFFILIATE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['emp_name'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" name="reference" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ $record['company'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ $record['department'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" class="form-control" name="section"  value="{{ $record['section'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
		
			<div class="clear_20"></div>
                    <div class="row_form_container hide">
                        <div class="col1_form_container">
                            <label class="labels required">OFFICE SALES TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select disabled   class="form-control" name="sales_type">
                                <option value="FOOD" selected>FOOD</option>
                            </select>
                        </div>
                    </div>

                    
                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>DELIVERY DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <select disabled   class="form-control" name="delivery_to">
                                <option value=""></option>
                                <option {{ $record['deliver_to'] == "RBC-Ortigas" ? "selected" : "" }} value="RBC-Ortigas">RBC-Ortigas</option>
                                <option {{ $record['deliver_to'] == "RBC-Novaliches" ? "selected" : "" }} value="RBC-Novaliches" >RBC-Novaliches</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TELEPHONE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_number" class="form-control" value="{{ $record['delivery_tel'] }}" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY ADDRESS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_address" class="form-control" value="{{ $record['delivery_add'] }}" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="delivery_remarks" class="form-control" value="{{ $record['delivery_remarks'] }}" />
                        </div>
                    </div>

                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY DATE</label>
                        </div>
                        <div class="col2_form_container input-group">
                            <input type="text" {{ $record["payment_status"] ? "readonly" : "" }} value="{{ $record['delivery_date'] }}" class="date_picker form-control" name="delivery_date" id ="delivery_date" />
                             <label class="input-group-addon btn" for="delivery_date">
                                   <span class="glyphicon glyphicon-calendar"></span>
                             </label>  
                        </div>
                    </div>

                     </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>ORDER DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                       
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SALES TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select disabled   class="form-control" name="type">
                                    <option value=""></option>
                                    @foreach($so_code as $code=>$fs)                                      
                                    <option {{ $record['so_code'] == $code ? "selected" : "" }} value="{{ $code }}" >{{ $fs }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>

                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">ORDER TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select disabled   class="form-control" name="order_type">
                                    <option value=""></option>
                                    <option {{ $record['order_type'] == "Company Sales" ? "selected" : "" }} value="Company Sales">Company Sales</option>
                                    <option {{ $record['order_type'] == "Top Management" ? "selected" : "" }} value="Top Management" >Top Management</option>
                                    <option {{ $record['order_type'] == "Employee Vale" ? "selected" : "" }} value="Employee Vale" >Employee Vale</option>
                                </select>
                            </div>
                        </div>
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="office">
                    <thead>
                    <tr>
                        <th ><span class='labels2'>QUANTITY</span></th>
                        <th ><span class='labels2'>UNIT</span></th>
                        <th ><span class='labels2'>PRODUCT DESCRIPTION</span></th>
                        <th ><span class='labels2' >PRICE</span></th>
                        <th ><span class='labels2'>AMOUNT</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $total_qty = array();
                        $total_amount = array();
                    ?>
                    @foreach($record["detail"] as $rs)
                    <tr>    
                       <td class="text-center" ><input type="hidden" name="qty[]" value="{{ $rs->order_qnty }}" >{{ $total_qty[] = $rs->order_qnty }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="uom[]" value="{{ $rs->uom_code }}" >{{ $uom[$rs->uom_code] }}</td>                                              
                       <td><input type="hidden" name="product[]" value="{{ $rs->prod_code }}" >{{ $rs->prod_name }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{ $rs->price }}</td>     
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{ $total_amount[] = $rs->price * $rs->order_qnty }}</td>                                                                       
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div style="background-color:#d9d9d9;border-top:1px solid black;">
                    <table>
                    <thead>
                    <tr>
                        <th ><span class='labels2'>TOTAL QUANTITY:</span></th>
                        <th ><input value="{{ array_sum($total_qty) }}" readonly class="form-control"  type="text" id="total-qty" /></th>
                        <th width="40%"></th>
                        <th ><span class='labels2'>TOTAL AMOUNT:</span></th>
                        <th ><input value="{{ number_format(array_sum($total_amount),2) }}" readonly class="form-control" type="text"    />
                        <input value="{{ (array_sum($total_amount)) }}" readonly class="form-control" type="hidden" id="total-amount"  /></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button disabled type="button" name="add" value="">ADD</button>
                            <button disabled type="button" name="edit" value="">EDIT</button>
                            <button disabled type="button" name="delete" value="">DELETE</button> 
                        </div>
                </div>

                 <div class="clear_20"></div>
                        <div class="container-header"><h5 class="text-center lined sub-header "><strong>PAYMENT DETAILS</strong></h5></div>
              
                    <div class="clear_10"></div>
                <div class="row" > 
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PAYMENT TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select {{ $record["payment_status"] ? "disabled" : "" }} class="form-control" name="payment_type">
                                <option value=""></option>
                                <option {{ $record['payment_type'] == "Cash" ? "selected" : "" }} value="Cash">Cash</option>
                                <option {{ $record['payment_type'] == "Check" ? "selected" : "" }} value="Check">Check</option>
                                <option {{ $record['payment_type'] == "Bank-to-bank" ? "selected" : "" }} value="Bank-to-bank" >Bank-to-bank</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PAYMENT DATE:</label>
                        </div>
                        <div class="col2_form_container input-group">
                            <input type="text" {{ $record["payment_status"] ? "readonly" : "" }} value="{{ $record['payment_date'] == "0000-00-00" ? "" : $record['payment_date'] }}" class="date_picker form-control" name="payment_date" id ="payment_date" />
                            <label class="input-group-addon btn" for="payment_date">
                                   <span class="glyphicon glyphicon-calendar"></span>
                             </label>  
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <input type="hidden" name="payment" class="form-control" value="{{ $record['payment'] }}" />
                        

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PAYMENT AMOUNT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $record["payment_status"] ? "readonly" : "" }} type="text" name="payment_amount" class="form-control isDecimal" maxlength="50" value="{{ $record['payment_amount'] }}" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PAYMENT REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $record["payment_status"] ? "readonly" : "" }} maxlength="100" type="text" name="payment_reference_num" class="form-control" value="{{ $record['payment_reference_num'] }}" />
                        </div>
                    </div>

                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input {{ $record["payment_status"] ? "readonly" : "" }} type="text" name="payment_remarks" class="form-control" value="{{ $record['payment_remarks'] }}" />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">AGE:</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="age" class="form-control" value="{{ $record["payment_status"] ? "" :  (strtotime(date("Y-m-d")) - strtotime($record['delivery_date'])) / 86400  }}" />
                        </div>
                    </div>

                    <div class="clear_20"></div>
                    <label class="attachment_note"><strong>ATTACHMENTS</strong></label><br/>
                    <div class="attachment_container">
                        @if($attachments)
                        <?php $count = 1000;?>
                        @foreach($attachments as $attachment)
                        @if($attachment->attachment_type == 2)
                        <?php $attach = json_decode($attachment->code);?>
                        <p>
                        <a href="{{ URL::to('/gd/download/') . '/' . $record['reference_no'] . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attachment->fn) }}">{{ $attachment->fn . " " . number_format($attach->filesize / 1024 ,1) . "KB"}}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attach->filesize }}' />
                        @if(!$record["payment_status"])
                        <button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}" attach-id="{{ $attachment->id }}" >DELETE</button><br /></p>
                        @endif
                        @endif
                        @endforeach
                        @endif

                        @if(!$record["payment_status"])
                        <div id="attachments"></div>    
                            <span class="btn btn-success btnbrowse fileinput-button">
                                <span >BROWSE</span>
                                <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                            </span>
                         @endif                                  
                    </div>      
                    
                     @if(!$record["payment_status"])
                    <div class="text-center" >
                    <button type="submit" class="btn btn-default " name="action" value="save">SAVE</button>
                    <button type="submit" class="btn btn-default " name="action" value="close">CLOSE PAYMENT</button>
                    </div>
                    @endif
                </div>
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container" style="border: 0;">
                <div class="row">
                    <textarea readonly="readonly"  rows="3" class="form-control" name="">{{ $record['remarks'] }}</textarea>
                </div>
            </div>


            <div class="clear_10"></div>
            
            @include('cc/template/back')
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/process.js') }}
<script>
var max_filesize = 20;//mb
var max_upload = 5;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop