@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'os/export/affiliate/action', 'method' => 'post')) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">EXPORT OFFICE SALES - AFFILIATE</label>
                <div class="row">
            
                    <div class="row_form_container"  style="margin:0 auto;float:none;">
                        <div class="col1_form_container">
                            <label class="labels required">FROM:</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="text" class="date_picker form-control" name="date_from" id="date_from" />
                                <label class="input-group-addon btn" for="date_from">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label> 
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container"  style="margin:0 auto;float:none;" >
                        <div class="col1_form_container">
                            <label class="labels required">TO:</label>
                        </div>
                        <div class="col2_form_container input-group">
                                <input type="text" class="date_picker form-control" name="date_to" id="date_to" />
                                <label class="input-group-addon btn" for="date_to">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>
                        </div>
                    </div>
		

                    <div class="clear_20"></div>
                  	<div class="row"> 
		                    <div class="text-center">
		                        <button type="submit" class="btn btn-default btndefault" name="action" value="export">GENERATE</button>
		                        <a href="{{ url::to("os/food/report") }}" class="btn btn-default btndefault" >BACK</a>
		                    </div>
		            </div>
                           
                                   
                </div>
            

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        
    {{ Form::close() }}
@stop
@section('js_ko')
<script>
$(function()
{
	"use strict";
    $(".date_picker").datepicker({ dateFormat: 'yy-mm-dd' });

})
</script>
@stop