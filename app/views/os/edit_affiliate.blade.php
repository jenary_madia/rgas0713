@extends('template/header')

@section('content')
	{{ Form::open(array('url' => "os/edit/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">OFFICE SALES ORDER FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">AFFILIATE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="section"  value="{{ $record['section'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
		
			<div class="clear_20"></div>
                    <div class="row_form_container hide">
                        <div class="col1_form_container">
                            <label class="labels required">OFFICE SALES TYPE:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" name="sales_type">
                                <option value="FOOD" selected>FOOD</option>
                            </select>
                        </div>
                    </div>

                    
                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>DELIVERY DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TO:</label>
                        </div>
                        <div class="col2_form_container">
                            <select  class="form-control" name="delivery_to">
                                <option value=""></option>
                                <option {{ $record['deliver_to'] == "RBC-Ortigas" ? "selected" : "" }} value="RBC-Ortigas">RBC-Ortigas</option>
                                <option {{ $record['deliver_to'] == "RBC-Novaliches" ? "selected" : "" }} value="RBC-Novaliches" >RBC-Novaliches</option>
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY TELEPHONE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_number" class="form-control" value="{{ $record['delivery_tel'] }}"  maxlength="50" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY ADDRESS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_address" class="form-control" value="{{ $record['delivery_add'] }}" maxlength="50"  />
                        </div>
                    </div>
                           
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY REMARKS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="50" type="text" name="delivery_remarks" class="form-control" value="{{ $record['delivery_remarks'] }}"  maxlength="50" />
                        </div>
                    </div>

                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">DELIVERY DATE</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" name="delivery_date" class="form-control" disabled value="{{ $record['delivery_date'] }}" />
                        </div>
                    </div>

                     </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>ORDER DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                       
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">SALES TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select  class="form-control" name="type">
                                    <option value=""></option>
                                    @foreach($so_code as $code=>$fs)                                      
                                    <option {{ $record['so_code'] == $code ? "selected" : "" }} value="{{ $code }}" >{{ $fs }}</option>
                                    @endforeach 
                                </select>
                            </div>
                        </div>

                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">ORDER TYPE:</label>
                            </div>
                            <div class="col2_form_container">
                                <select  class="form-control" name="order_type">
                                    <option value=""></option>
                                    <option {{ $record['order_type'] == "Company Sales" ? "selected" : "" }} value="Company Sales">Company Sales</option>
                                    <option {{ $record['order_type'] == "Top Management" ? "selected" : "" }} value="Top Management" >Top Management</option>
                                    <option {{ $record['order_type'] == "Employee Vale" ? "selected" : "" }} value="Employee Vale" >Employee Vale</option>
                                </select>
                            </div>
                        </div>
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="office">
                    <thead>
                    <tr>
                        <th ><span class='labels2'>QUANTITY</span></th>
                        <th ><span class='labels2'>UNIT</span></th>
                        <th ><span class='labels2'>PRODUCT DESCRIPTION</span></th>
                        <th ><span class='labels2' >PRICE</span></th>
                        <th ><span class='labels2'>AMOUNT</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $total_qty = array();
                        $total_amount = array();
                    ?>
                    @foreach($record["detail"] as $rs)
                    <tr>    
                       <td class="text-center" ><input type="hidden" name="qty[]" value="{{ $rs->order_qnty }}" >{{ $total_qty[] = $rs->order_qnty }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="uom[]" value="{{ $rs->uom_code }}" >{{ $uom[$rs->uom_code] }}</td>                                              
                       <td><input type="hidden" name="product[]" value="{{ $rs->prod_code }}" >{{ $rs->prod_name }}</td>                                                
                       <td class="text-center" ><input type="hidden" name="price[]" value="{{ $rs->price }}" >{{ $rs->price }}</td>     
                       <td class="text-center" ><input type="hidden" name="amount[]" value="{{ $rs->price * $rs->order_qnty }}" >{{ $total_amount[] = $rs->price * $rs->order_qnty }}</td>                                                                       
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                <div style="background-color:#d9d9d9;border-top:1px solid black;">
                    <table>
                    <thead>
                    <tr>
                        <th class="text-center" ><span class='labels2'>TOTAL QUANTITY:</span></th>
                        <th class="text-center" ><input value="{{ array_sum($total_qty) }}" readonly class="form-control"  type="text" id="total-qty" /></th>
                        <th width="40%"></th>
                        <th class="text-center" ><span class='labels2'>TOTAL AMOUNT:</span></th>
                        <th class="text-center" ><input value="{{ number_format(array_sum($total_amount),2) }}" readonly class="form-control" type="text" id="total-amount"  /></th>
                        
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" value="">ADD</button>
                            <button type="button" name="edit" value="">EDIT</button>
                            <button type="button" name="delete" value="">DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

            <div class="">
                <div>NOTE:</div>
                <div>*SCHEDULE FOR SENDING OFFICE SALES ORDER TO SSRV (<a href="#" data-toggle="modal" data-target="#reminder-modal" >CLICK HERE</a> )</div>
                <div>*LIMIT IN THE AMOUNT OF ORDER THAT MAY BE DEDUCTED FROM SALARY EMPLOYEE PER PAYROLL PERIOD IS <b>P2,000.00</b></div>             
                <div>ORDERS EXCEEDING SAID AMOUNT SHALL BE PAID IN CASH</div>
                <div>FOR <b>CASH PAYMENTS</b>, AMOUNT OF CASH TO BE SENT TO SSRV SHALL BE NOTED IN THE <b>COMMENT</b> FIELD.</div>
                <div>CASH PAYMENTS MUST BE FORWARDED TO SSRV SAME DAY OF ORDERING.</div>
                <div>FOR <b>EMERGENCY ORDERS</b>, THE EMPLOYEE IS REQUIRED TO SUBMIT A WRITTEN LETTER ADDRESSED TO SSRV HEAD STATING THE REASON FOR SUCH.</div>
            </div>


        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                <div class="clear_10"></div> 
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ $record['comment_save'] }}</textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

<div class="modal fade" id="office-food-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel-food">ADD OFFICE SALES DETAIL</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-4"><label class="labels required">COMPANY </label></div>
                <div class="col-sm-8">
                    <select class="form-control" id="food-company"  >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option  value="{{ $rs->comp_code }}">{{ $rs->comp_name }}</option>
                        @endforeach                               
                    </select>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-4"><label class="labels required">SEARCH </label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="food-search" />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6 text-center"><label class="labels required">PRODUCT </label></div>
                <div class="col-sm-6 text-center">
                    <label class="labels required">UNIT OF MEASUREMENT </label>
                </div>
              </div>

              <div class="clear_20"></div>

    
              <div class="row">
                <div class="col-sm-6">
                    <select class="form-control"  size="15" id="food-product">
                        @foreach($food as $code=>$fs)                                      
                        <option comp_code="{{ $fs["company"] }}" def_code="{{ $fs["def_code"] }}" uom_code="{{ $fs["uom_code"] }}" uom_name ="{{ $fs["uom_name"] }}" price="{{ $fs["selling_price"] }}"  value="{{ $code }}" >{{ $fs["name"] }}</option>
                        @endforeach                               
                    </select>
                </div>

                <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-12">
                            <select class="form-control" id="food-uom" ></select>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">QUANTITY </label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control isNumeric" id="food-qty" maxlength="5" />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12  text-center">
                            <label class="labels required">PRICE</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="food-price" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">AMOUNT</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="food-amount" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save_food" >Save</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="office-non-food-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel-non-food">ADD OFFICE SALES DETAIL</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
                <div class="row">
                <div class="col-sm-4"><label class="labels text-default">COMPANY </label></div>
                <div class="col-sm-8">
                    <select class="form-control" disabled="" >
                        <option value="" ></option>                            
                    </select>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-4"><label class="labels required">SEARCH </label></div>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="non-food-search" />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6 text-center"><label class="labels required">PRODUCT </label></div>
                <div class="col-sm-6 text-center">
                    <label class="labels required">UNIT OF MEASUREMENT </label>
                </div>
              </div>

              <div class="clear_20"></div>

    
              <div class="row">
                <div class="col-sm-6">
                    <select class="form-control"  size="15" id="non-food-product">
                        @foreach($non_food as $code=>$fs)                                      
                        <option  def_code="{{ $fs["uom_code"] }}" uom_code="{{ $fs["uom_code"] }}" uom_name ="{{ $fs["uom_name"] }}" price="{{ $fs["selling_price"] }}"  value="{{ $code }}" >{{ $fs["name"] }}</option>
                        @endforeach                               
                    </select>
                </div>

                <div class="col-sm-6">
                     <div class="row">
                        <div class="col-sm-12">
                            <select class="form-control" id="non-food-uom" ></select>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">QUANTITY </label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-qty" maxlength="5" />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12  text-center">
                            <label class="labels required">PRICE</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-price" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12 text-center">
                            <label class="labels required">AMOUNT</label>
                        </div>
                     </div>
                     <div class="clear_10"></div>
                     <div class="row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="non-food-amount" readonly />
                        </div>
                     </div>
                     <div class="clear_10"></div>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save_non_food" >Save</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">Cancel</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/os/create.js') }}
@stop