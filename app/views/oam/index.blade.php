@extends('template/header')

@section('content')

        <form class="form-inline" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_title">ATTENDANCE MONITORING REPORT</label>


                    
                    <div class="row">
                        <!-- Show if above is selected -->
                        <div id="divEmaf" style="">

                            <div class="tbl">
                                    <label class="" ><a href="{{ $base_url }}/oam" >MY ATTENDANCE RECORD</a></label> |
                                    <label class="" >MY SUBORDINATE'S ATTENDANCE RECORD</label>
                            </div>
                            
                            <div class="clear_10"></div>

                            <div class="tbl">
                                <form id='form' name="form" method="get">
                                
                                <div {{ $div_head ? "" :  'style="display:none"' }} >
                                    <div class="">
                                        <label class="labels required" >DEPARTMENT:</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <select class="form-control" style="width: 241px" id="department" name="department">
                                            <option value="">All</option>
                                             @foreach ($department as $aKey=>$aVal)
                                                <option  value="{{ $aKey }}">{{ $aVal }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="clear_10"></div>
                                </div>

                                <div {{ in_array($access, array("top mngt","head")) || $div_head ? "" :  'style="display:none"' }} >
                                    <div class="">
                                        <label class="labels required"  >SECTION:</label>
                                    </div>
                                    <div class="col2_form_container">
                                        <select class="form-control" style="width: 241px" id="section" name="section">
                                            <option value="">All</option>
                                             @foreach ($section as $aKey=>$aVal)
                                                @foreach ($section[$aKey] as $aKey2=>$aVal2)
                                                    <option id="{{ $aKey }}" value="{{ $aKey2 }}">{{ $aVal2 }}</option>
                                                @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="clear_10"></div>
                                </div>          

                                <div class="">
                                    <label class="labels required">MONTH/YEAR:</label>
                                </div>
                                <div class="col4_form_container">
                                    <select class="form-control" style="width: 121px" id="month" name="month" >
                                        @foreach ($months as $aKey=>$aVal)
                                            <option {{ $aKey == date("m") ? "selected" : "" }} value="{{ $aKey }}">{{ $aVal }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col4_form_container">
                                    <select class="form-control" style="width: 121px" name="year" id="year">
                                        @foreach ($years as $aKey=>$aVal)
                                            <option {{ $aKey == date("Y") ? "selected" : "" }} value="{{ $aKey }}">{{ $aVal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                </form>

                                <div class="clear_10"></div>
                                <div class="hide">
                                    <button type="button" class="btn btnsearch btn-default">SEARCH</button>
                                </div>
                            </div>
                            <div class="clear_10"></div>
                            <div class="" style="margin:30px 30px 0 ;overflow-x:auto;" >
                                <table style="width:2665px;font-size:10px;" border = "1" cellpadding = "0" class="tbl tbl_list">
                                    <tr>
                                        <th class="labels2 text-left" rowspan="2" width="160" >EMPLOYEE NAME</th>
                                        <th class="th_style" rowspan="2" width="140">ATTENDANCE LOGS</th>
                                        <th class="th_style">1</th>
                                        <th class="th_style">2</th>
                                        <th class="th_style">3</th>
                                        <th class="th_style">4</th>
                                        <th class="th_style">5</th>
                                        <th class="th_style">6</th>
                                        <th class="th_style">7</th>
                                        <th class="th_style">8</th>
                                        <th class="th_style">9</th>
                                        <th class="th_style">10</th>

                                        <th class="th_style">5</th>
                                        <th class="th_style">6</th>
                                        <th class="th_style">7</th>
                                        <th class="th_style">8</th>
                                        <th class="th_style">9</th>
                                        <th class="th_style">10</th>
                                        <th class="th_style">5</th>
                                        <th class="th_style">6</th>
                                        <th class="th_style">7</th>
                                        <th class="th_style">8</th>
                                        <th class="th_style">9</th>
                                        <th class="th_style">10</th>
                                        <th class="th_style">5</th>
                                        <th class="th_style">6</th>
                                        <th class="th_style">7</th>
                                        <th class="th_style">8</th>
                                        <th class="th_style">9</th>
                                        <th class="th_style">10</th>
                                      
                                        <th class="th_style">26</th>
                                        <th class="th_style">27</th>
                                        <th class="th_style">28</th>
                                        <th class="th_style">29</th>
                                        <th class="th_style">30</th>
                                        <th class="th_style">31</th>
                                        <th class="th_style"  rowspan="2" width="80">NUMBER OF DAYS WORKED</th>
                                        <th class="th_style"  colspan="2">LEAVE</th>
                                        <th class="th_style"  colspan="2">LATE</th>
                                        <th class="th_style"  rowspan="2">FREQUENCY OF LATES</th>
                                    </tr>

                                    <tr>
                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>
                                        <th class="th_style">SAT</th>
                                        <th class="th_style">SUN</th>

                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>
                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>
                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>

                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>
                                        <th class="th_style">SAT</th>
                                        <th class="th_style">SUN</th>
                                        <th class="th_style">MON</th>
                                        <th class="th_style">TUE</th>
                                        <th class="th_style">WED</th>
                                        <th class="th_style">THURS</th>
                                        <th class="th_style">FRI</th>

                                        <th class="th_style">EXCUSED</th>
                                        <th class="th_style">UNEXCUSED</th>
                                        <th class="th_style">EXCUSED</th>
                                        <th class="th_style">UNEXCUSED</th>
                                    </tr>

                                 

                                </table>
                            </div>
                        </div>
                        <div class="clear_10"></div>

                    </div><!-- end of checkbox_container -->

                  


            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                <div class="textarea_messages_container">
                    <table>
                    <tr>
                            <td width="10%"><label class="labels">LEGEND:</label></td>
                            <td width="20%"><label class="labels"><span class="text-primary">ML</span> - MATERNITY LEAVE</label></td>
                            <td width="30%"><label class="labels"><span class="text-primary">SL</span> - SICK LEAVE</label></td>
                            <td width="20%"><label class="labels"><span class="text-primary">VL</span> - VACATION LEAVE</label></td>
                            
                            <td width="20%"><label class="labels"><span class="text-primary">PL</span> - PATERNITY LEAVE</label></td>
                             
                    </tr>
                    <tr>         
                        <td><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
                        <td><label class="labels"><span class="text-primary">EL</span> - EMERGENCY LEAVE</label>    </td>  
                        <td><label class="labels">SHADED - REST DAY AND HOLIDAY</label>  
                        <td><label class="labels"><span class="text-primary">BL</span> - BIRTHDAY LEAVE</label></td>
                        
                               
                        <td><label class="labels">OB - ON OFFICIAL BUSINESS</label>      </td>               
                    </tr>
                    <tr>     
                        <td><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>   </td>
                                    
                        <td><label class="labels">OS - OFFSET</label></td>
                        <td><label class="labels required">A - ABSENT</label></td>
                        <td><label class="labels ">UT - UNDERTIME</label></td>
                    </tr>
                    </table>

		 <div class="clear_10"></div>

                    <div class="row">
                            <div class="form_label">
                                <label class="">*ABSENCES TAGGED AS UNEXCESED DUE TO NON-SUBMISSION AND/OR NON-APPROVAL OF RGAS-LEAVE AND NOTIFICATION TO HR-CBRM</label>
                            </div>
                    </div>
		
                </div>

                <div class="clear_10"></div>
                
                <div class="">
                        <table id="legend">
                         <tr>
                                <td width="10%"><label class="labels"></label></td>
                                <td width="20%"><label class="labels"> <label class="labels">REGULAR HOLIDAYS</label></td>
                                <td width="30%"><label class="labels"><label class="labels">SPECIAL (NON WORKING) DAYS</label></td>
                                <td width="20%"><label class="labels"><label class="labels">WORK SUSPENSION</label></td>                                
                                <td width="20%"><label class="labels"><label class="labels">DAYS FOR EXCUSED LATES</label></td>                                 
                        </tr>

                        <tr>
                                <td width="10%"></td>
                                <td width="20%"><span class="tbl_fonts">&nbsp;</span></td>
                                <td width="30%"></td>
                                <td width="20%"></td>                                
                                <td width="20%"></td>                                 
                        </tr>
                        </table>
                                             
                </div>
                <div class="clear_10"></div>
            </div>

        </form>


        <input type="hidden" id="access" value="{{ $access }}" />
@stop

@section('js_ko')
{{ HTML::script('/assets/js/oam/index.js') }}
@stop