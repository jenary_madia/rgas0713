@extends('template/header')

@section('content')

        <form class="form-inline" method="post" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_title">ATTENDANCE MONITORING REPORT</label>

                    
                    <div class="row">
                        <!-- Show if above is selected -->
                        <div id="divEmaf" style="">
                            @if($access != "employee" && !empty($subordinate))
                            <div class="tbl">
                                    <label class="" >MY ATTENDANCE RECORD</label> |
                                    <label class="" ><a href="{{ $base_url }}/oam/list" >MY SUBORDINATE'S ATTENDANCE RECORD</a></label>
                            </div>
                            @endif
                            <div class="clear_10"></div>

                            <div class="tbl">
                                <div class="">
                                    <label class="labels required">MONTH/YEAR:</label>
                                </div>
                                <div class="col4_form_container">
                                    <select class="form-control" style="width: 121px" id="month">
                                        @foreach ($months as $aKey=>$aVal)
                                            <option {{ $aKey == date("m") ? "selected" : "" }} value="{{ $aKey }}">{{ $aVal }}</p>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="col4_form_container">
                                    <select class="form-control" style="width: 121px" id="year" >
                                        @foreach ($years as $aKey=>$aVal)
                                            <option {{ $aKey == date("Y") ? "selected" : "" }} value="{{ $aKey }}">{{ $aVal }}</p>
                                        @endforeach
                                    </select>
                                </div>

                                 <div class="clear_10"></div>

                                <div class="hide">
                                    <button type="button" class="btn btnsearch btn-default" >SEARCH</button>
                                </div>
                            </div>
                            <div class="clear_10"></div>

                            <div class="tbl">
                                <table border = "0" cellpadding = "0" class="tbl">
                                    <tr>
                                        <th class="th_style">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</th>
                                        <th class="th_style" width="30%" >ATTENDANCE SUMMARY</th>
                                        <th class="th_style">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</th>
                                    </tr>
                                </table>
                            </div>

                            <div class="clear_10"></div>

                            <div class="tbl ">
                                <table border = "1" cellpadding = "0" class="tbl tbl_total">
                                    <tr>
                                        <th class="th_style" rowspan="2">NUMBER OF DAYS WORKED</th>
                                        <th class="th_style" colspan="2">LEAVE</th>
                                        <th class="th_style" colspan="2">LATE</th>
                                        <th class="th_style" rowspan="2">FREQUENCY OF LATES</th>
                                    </tr>

                                    <tr>
                                        <th class="th_style">EXCUSED</th>
                                        <th class="th_style">UNEXCUSED</th>
                                        <th class="th_style">EXCUSED</th>
                                        <th class="th_style">UNEXCUSED</th>
                                    </tr>

                                    
                                </table>
                            </div>

                            <div class="clear_10"></div>

                            <div class="tbl">
                                <table border = "0" cellpadding = "0" class="tbl">
                                    <tr>
                                        <th class="th_style">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</th>
                                        <th class="th_style" width="30%" >ATTENDANCE LOGS</th>
                                        <th class="th_style">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;</th>
                                    </tr>
                                </table>
                            </div>

                            <div class="clear_10"></div>

                            <div class="tbl">
                                <table border = "1" cellpadding = "0" class="tbl tbl_logs">
                                    <tr>
                                        <th class="th_style">DATE</th>
                                        <th class="th_style">DAY</th>
                                        <th class="th_style">TIME IN</th>
                                        <th class="th_style">TIME OUT</th>
                                        <th class="th_style">WORKED HOURS</th>
                                        <th class="th_style">LATE(MINUTES)</th>
                                        <th class="th_style">REMARKS</th>
                                    </tr>

                                  
                                </table>
                            </div>

					</div>
				</div>
            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
               
                <div class="textarea_messages_container">
                    <table>
                    <tr>
                            <td width="10%"><label class="labels">LEGEND:</label></td>
                            <td width="20%"><label class="labels"><span class="text-primary">ML</span> - MATERNITY LEAVE</label></td>
                            <td width="30%"><label class="labels"><span class="text-primary">SL</span> - SICK LEAVE</label></td>
                            <td width="20%"><label class="labels"><span class="text-primary">VL</span> - VACATION LEAVE</label></td>
                            
                            <td width="20%"><label class="labels"><span class="text-primary">PL</span> - PATERNITY LEAVE</label></td>
                             
                    </tr>
                    <tr>         
                        <td><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label></td>
                        <td><label class="labels"><span class="text-primary">EL</span> - EMERGENCY LEAVE</label>    </td>  
                        <td><label class="labels">SHADED - REST DAY AND HOLIDAY</label>  
                        <td><label class="labels"><span class="text-primary">BL</span> - BIRTHDAY LEAVE</label></td>
                        
                               
                        <td><label class="labels">OB - ON OFFICIAL BUSINESS</label>      </td>               
                    </tr>
                    <tr>     
                        <td><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>   </td>
                                    
                        <td><label class="labels">OS - OFFSET</label></td>
                        <td><label class="labels required">A - ABSENT</label></td>
                        <td><label class="labels ">UT - UNDERTIME</label></td>
                    </tr>
                    </table>

                    <div class="clear_10"></div>

                    <div class="row">
                            <div class="form_label">
                                <label class="">*ABSENCES TAGGED AS UNEXCESED DUE TO NON-SUBMISSION AND/OR NON-APPROVAL OF RGAS-LEAVE AND NOTIFICATION TO HR-CBRM</label>
                            </div>
                    </div>
                </div>

                <div class="clear_10"></div>
                
                <div class="">
                        <table id="legend">
                         <tr>
                                <td width="10%"><label class="labels"></label></td>
                                <td width="20%"><label class="labels"> <label class="labels">REGULAR HOLIDAYS</label></td>
                                <td width="30%"><label class="labels"><label class="labels">SPECIAL (NON WORKING) DAYS</label></td>
                                <td width="20%"><label class="labels"><label class="labels">WORK SUSPENSION</label></td>                                
                                <td width="20%"><label class="labels"><label class="labels">DAYS FOR EXCUSED LATES</label></td>                                 
                        </tr>

                        <tr>
                                <td width="10%"></td>
                                <td width="20%"><span class="tbl_fonts">&nbsp;</span></td>
                                <td width="30%"></td>
                                <td width="20%"></td>                                
                                <td width="20%"></td>                                 
                        </tr>
                        </table>
                                             
                </div>
                <div class="clear_10"></div>
                
            </div><!-- end of form_container -->
        </form>

 <div class="modal fade" id="RedirectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title text-center" id="exampleModalLabel">FILE FOR</h4>
        </div>
        <div class="modal-body text-center">
             <a class="btn  btn-default " href="{{ $base_url }}/lrf/create" >Leave</a>
             <a class="btn  btn-primary " href="{{ $base_url }}/ns/create"  >Notification</a>
        </div>
       
      </div>
    </div>
  </div>

@stop

@section('js_ko')
{{ HTML::script('/assets/js/oam/self.js') }}
@stop