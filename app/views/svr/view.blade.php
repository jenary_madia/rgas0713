@extends('template/header')

@section('content')
    {{ Form::open(array('url' => "svr/view/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">SERVICE VEHICLE REQUEST FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" disabled class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" disabled class="form-control" name="section"  value="{{ $record['section'] }}" />
                        </div>
                    </div>

                    
                   </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>REQUEST DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REQUEST TYPE</label>
                        </div>
                        <div class="col4_form_container">
                            <input disabled type="radio" name="requestype" value="SERVICE" {{ $record['requestfor'] == "SERVICE" ? "checked" : "" }} /> SERVICE
                        </div>
                        <div class="col4_form_container">
                            <input disabled type="radio"  name="requestype" value="TRUCK" {{ $record['requestfor'] == "TRUCK" ? "checked" : "" }} /> TRUCK
                        </div>
                    </div>

                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PURPOSE</label>
                        </div>
                        <div class="col2_form_container">
                            <input disabled type="text" name="purpose" class="form-control" value="{{ $record['purpose'] }}" />
                        </div>
                    </div>
                           
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="vehicle">
                    <tbody>
                    <tr>
                        <th  class="text-center"  width="80"><span class='labels2'>DATE</span></th>
                        <th  class="text-center"  width="80"><span class='labels2'>PICK-UP TIME</span></th>
                        <th  class="text-center" ><span class='labels2'>PICK-UP PLACE</span></th>
                        <th  class="text-center"  ><span class='labels2' >DESTINATION</span></th>
                        <th  class="text-center" ><span class='labels2'>NUMBER OF PASSENGERS</span></th>
                        <th  class="text-center"  width="120"><span class='labels2'>ESTIMATED TRIP TIME</span></th>
                        <th  class="text-center" ><span class='labels2'>VEHICLE TYPE</span></th>
                        <th  class="text-center" ><span class='labels2'>PLATE NUMBER</span></th>
                    </tr>
                    @foreach($detail as $rs)
                    <tr>                        
                        <td><input type="hidden" name="pickup-date[]" value="{{ $rs->date }}">{{ $rs->date }}</td>                        
                        <td><input type="hidden" name="pickup-time[]" value="{{ $rs->pickuptime }}" >{{ $rs->pickuptime }}</td>                        
                        <td><input type="hidden" name="pickup-company[]" value="{{ $rs->pickupplace_id }}">                            
                            <input type="hidden" name="pickup-company-other[]" value="{{ $rs->pickupplace }}">                            
                            <input type="hidden" name="pickup-location[]" value="{{ $rs->pickupplace_location_id }}">                            
                            <input type="hidden" name="pickup-location-other[]" value="{{ $rs->pickupplace_location }}">                            
                             {{ trim( ($rs->pickupplace_id ? "" : $rs->pickupplace ) . $company_arr[$rs->pickupplace_id]) . " - " . trim( ($rs->pickupplace_location_id ? "" :  $rs->pickupplace_location) . $location[$rs->pickupplace_location_id])}}                      
                       </td>
                        <td>
                            <input type="hidden" name="destin-company[]" value="{{ $rs->destination_id }}">                            
                            <input type="hidden" name="destin-company-other[]" value="{{ $rs->destination }}">                            
                            <input type="hidden" name="destin-location[]" value="{{ $rs->destination_location_id }}">                            
                            <input type="hidden" name="destin-location-other[]" value="{{ $rs->destination_location }}">                            
                            {{ trim($company_arr[$rs->destination_id] . ($rs->destination_id ? "" : $rs->destination)   ) . " - " . trim(($rs->destination_location_id ? "" :  $rs->destination_location) . $location[$rs->destination_location_id])}}                        
                         </td>                        
                        <td><input type="hidden" name="passenger[]" value="{{ $rs->noofpassengers }}">{{ $rs->noofpassengers }}</td>                        
                        <td><input type="hidden" name="triptime[]" value="{{ $rs->estimated_trip_time }}">{{ $rs->estimated_trip_time }}</td>                        
                        <td>{{ isset($vehicle[$rs->vehicletype]) ? $vehicle[$rs->vehicletype] : "" }}</td>                        
                        <td>{{ $rs->plateno }}</td>                   
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" disabled >ADD</button>
                            <button type="button" name="edit" disabled >EDIT</button>
                            <button type="button" name="delete" disabled >DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                 <div class="clear_10"></div>       
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea {{  in_array($record['status'], ['FOR APPROVAL']) ? "" : "disabled" }} {{ $readonly }} rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                        
                 

                 <div class="row text-center">
                         @if( in_array($record['status'], ['FOR APPROVAL']) )    
                            <button type="submit" style="width:120px" class="btn btn-default btndefault" name="action" value="cancel">CANCEL REQUEST</button>
                        @endif
                        <button type="button" class="btn btn-default btndefault" onclick="window.location='{{ url::previous("")  }}'">BACK</button>                    
                </div>
            </div>
        </div><!-- end of form_container -->



    {{ Form::close() }}
@stop