@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'svr/create/action', 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">SERVICE VEHICLE REQUEST FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ date('Y-m-d') }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="NEW" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="contact" value="" maxlength="20" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="section"  value="{{ Session::get('sect_name') }}" maxlength="25" />
                        </div>
                    </div>

                 </div>

                   <div class="clear_20"></div>
	   
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>REQUEST DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   


                 <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REQUEST TYPE</label>
                        </div>
                        <div class="col4_form_container">
                            <input type="radio" name="requestype" value="SERVICE" checked="" /> SERVICE
                        </div>
                        <div class="col4_form_container">
                            <input type="radio"  name="requestype" value="TRUCK" /> TRUCK
                        </div>
                    </div>

                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PURPOSE</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" name="purpose" class="form-control"   />
                        </div>
                    </div>
                           
                                   
                </div>
            
                <div class="clear_20"></div>

                <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="vehicle">
                    <tr>
                        <th class="text-center" width="80"><span class='labels2'>DATE</span></th>
                        <th class="text-center" width="80"><span class='labels2'>PICK-UP TIME</span></th>
                        <th class="text-center" ><span class='labels2'>PICK-UP PLACE</span></th>
                        <th class="text-center" ><span class='labels2' >DESTINATION</span></th>
                        <th class="text-center" ><span class='labels2'>NUMBER OF PASSENGERS</span></th>
                        <th class="text-center"  width="120"><span class='labels2'>ESTIMATED TRIP TIME</span></th>
                        <th class="text-center" ><span class='labels2'>VEHICLE TYPE</span></th>
                        <th class="text-center" ><span class='labels2'>PLATE NUMBER</span></th>
                    </tr>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" value="">ADD</button>
                            <button type="button" disabled name="edit" value="">EDIT</button>
                            <button type="button" disabled name="delete" value="">DELETE</button> 
                        </div>
                </div>

                <div class="clear_10"></div>

        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ Input::old("lrfComment") }}</textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        @if(in_array(Session::get("desig_level"), ["vpo","om","aom","plant-aom","admin-aom","top mngt","president" , "head"])
                            && ( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"]) ))
                        <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
                        @elseif(Session::get("desig_level") == "head")
                        <label class="button_notes"><strong>SEND</strong> FOR PROCESSING</label>
                        @elseif(in_array(Session::get("desig_level"), ["employee","supervisor"])
                            && ( in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"]) ))
                        
                        <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        @else
                        <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR APPROVAL</label>
                        @endif
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

<div class="modal fade" id="service-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">ADD SERVICE VEHICLE DETAILS</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP DATE: </label></div>
                <div class="col-sm-3">
                    <div class=" input-group">
                        <input type="text" class="date_picker form-control"  id="pickup-date" />
                        <label class="input-group-addon btn" for="pickup-date">
                           <span class="glyphicon glyphicon-calendar"></span>
                        </label>  
                    </div>
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP TIME: </label></div>
                <div class="col-sm-3">
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input type="text"  class="time_picker form-control input-small"  id="pickup-time" >
                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP PLACE: </label></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels kepp-all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPANY/PLANT/ENTITY: </label></div>
                <div class="col-sm-3">
                    <select id="pickup-company" >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option loc="{{ $rs->location_id }}" value="{{ $rs->company_id }}">{{ $rs->company }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="pickup-company-other-tick" /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="pickup-company-other" /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOCATION: </label></div>
                <div class="col-sm-3">
                    <select id="pickup-location" >
                        @foreach($location as $key => $val)                                      
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="pickup-location-other-tick" /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="pickup-location-other"  /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">DESTINATION</label></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels kepp-all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPANY/PLANT/ENTITY: </label></div>
                <div class="col-sm-3">
                    <select id="destin-company" >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option loc="{{ $rs->location_id }}" value="{{ $rs->company_id }}">{{ $rs->company }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="destin-company-other-tick" /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="destin-company-other"  /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOCATION: </label></div>
                <div class="col-sm-3">
                    <select id="destin-location" >
                        <option value="" ></option>
                        @foreach($location as $key => $val)                                      
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="destin-location-other-tick" /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="destin-location-other" /></div>
              </div> 

              <div class="clear_20"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">NUMBER&nbsp;OF&nbsp;PASSENGERS: </label></div>
                <div class="col-sm-3">
                    <input type="text" id="passenger" maxlength="10" class="isNumeric" />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">ESTIMATED TRIP TIME: </label></div>
                <div class="col-sm-3">
                    <input type="text" id="triptime"  class="isDecimal"  />
                </div>
              </div>


              <div class="row">
                <div class="col-sm-3"><label class="labels text-disable">VEHICLE TYPE: </label></div>
                <div class="col-sm-3">
                    <select  disabled="disabled">
                        <option value="" ></option>                                 
                    </select>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels text-disable">PLATE NUMBER: </label></div>
                <div class="col-sm-3">
                    <select  disabled="disabled">
                        <option value="" ></option>                                 
                    </select>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save" >SAVE</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">CANCEL</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/svr/create.js') }}
@stop