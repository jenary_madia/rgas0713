@extends('template/header')

@section('content')
<div id="wrap">
 
    <div class="wrapper">

<form class="form-inline" method="post" enctype="multipart/form-data">
    <input type="hidden" value="{{ csrf_token() }}">

   <!-- <label class="form_label pm_form_title">SERVICE VEHICLE REQUEST FORM</label>-->
               
    <div class="clear_10"></div>                 
 
        <div class="datatable_holder">
             <span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY SERVICE VEHICLE</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td>  
                    <td>{{ $rs["date"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["current"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  

    </div>

   <div class="clear_20"></div>  

   @if(in_array(Session::get("dept_name") ,["OFFICE OF THE EXECUTIVES", "OPERATION MANAGEMENT"] ) || Session::get("desig_level") == "head"  )
     <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
            <table id="reviewer_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>                        
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record_approval as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td>  
                    <td>{{ $rs["date"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["from"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  

    @endif

    <div class="clear_20"></div>  
    <div class="clear_20"></div>  
</form>
</div>
</div>

@stop
@section('js_ko')
{{ HTML::script('/assets/js/art/filer.js') }}
@stop