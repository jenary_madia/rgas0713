@extends('template/header')

@section('content')
    {{ Form::open(array('url' => "svr/accept/action/$id", 'method' => 'post', 'files' => true)) }}
        <input readonly="readonly"  type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">SERVICE VEHICLE REQUEST FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control" name="empname" value="{{ $record['name'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control" name="reference" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control"  value="{{ $record['emp_no'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control" name="company" value="{{ $record['company'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  readonly="readonly" type="text" class="form-control" name="department" value="{{ $record['department'] }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  type="text" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly"  type="text" class="form-control" readonly  value="{{ $record['section'] }}" />
                        </div>
                    </div>
                </div>

                   <div class="clear_20"></div>
       
                    <div class="container-header"><h5 class="text-center lined sub-header "><strong>REQUEST DETAILS</strong></h5></div>
                    
                    <div class="clear_10"></div>
                   





                 <div class="row">
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REQUEST TYPE</label>
                        </div>
                        <div class="col4_form_container">
                            <input type="radio" disabled name="requestype" value="SERVICE" {{ $record['requestfor'] == "SERVICE" ? "checked" : "" }} /> SERVICE
                        </div>
                        <div class="col4_form_container">
                            <input type="radio" disabled name="requestype" value="TRUCK" {{ $record['requestfor'] == "TRUCK" ? "checked" : "" }} /> TRUCK
                        </div>
                    </div>

                    <input type="hidden" name="requestype" value="{{ $record['requestfor'] }}" />
                    <input type="hidden" name="purpose" value="{{ $record['purpose'] }}" />
                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PURPOSE</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" disabled name="purpose" class="form-control" value="{{ $record['purpose'] }}" />
                        </div>
                    </div>

                    <div class="clear_20"></div>
</div>
                    <div style="height:200px;background-color:#d9d9d9;overflow:auto">
                <table border="1" id="vehicle">
                    <tbody>
                    <tr>
                        <th  class="text-center"  width="80"><span class='labels2'>DATE</span></th>
                        <th  class="text-center"  width="80"><span class='labels2'>PICK-UP TIME</span></th>
                        <th  class="text-center" ><span class='labels2'>PICK-UP PLACE</span></th>
                        <th  class="text-center"  ><span class='labels2' >DESTINATION</span></th>
                        <th  class="text-center" ><span class='labels2'>NUMBER OF PASSENGERS</span></th>
                        <th  class="text-center"  width="120"><span class='labels2'>ESTIMATED TRIP TIME</span></th>
                        <th  class="text-center" ><span class='labels2'>VEHICLE TYPE</span></th>
                        <th  class="text-center" ><span class='labels2'>PLATE NUMBER</span></th>
                    </tr>
                    @foreach($detail as $rs)
                    <tr>              
                        <td><input type="hidden" name="pickup-date[]" value="{{ $rs->date }}">{{ $rs->date }}</td>                        
                        <td><input type="hidden" name="pickup-time[]" value="{{ $rs->pickuptime }}" >{{ $rs->pickuptime }}</td>                        
                        <td><input type="hidden" name="pickup-company[]" value="{{ $rs->pickupplace_id }}">                            
                            <input type="hidden" name="pickup-company-other[]" value="{{ $rs->pickupplace }}">                            
                            <input type="hidden" name="pickup-location[]" value="{{ $rs->pickupplace_location_id }}">                            
                            <input type="hidden" name="pickup-location-other[]" value="{{ $rs->pickupplace_location }}">                            
                                 {{ trim( ($rs->pickupplace_id ? "" : $rs->pickupplace ) . $company_arr[$rs->pickupplace_id]) . " - " . trim( ($rs->pickupplace_location_id ? "" :  $rs->pickupplace_location) . $location[$rs->pickupplace_location_id])}}                      
                    </td>
                        <td>
                            <input type="hidden" name="vehicleid[]" value="{{ $rs->id }}">   
                            <input type="hidden" name="destin-company[]" value="{{ $rs->destination_id }}">                            
                            <input type="hidden" name="destin-company-other[]" value="{{ $rs->destination }}">                            
                            <input type="hidden" name="destin-location[]" value="{{ $rs->destination_location_id }}">                            
                            <input type="hidden" name="destin-location-other[]" value="{{ $rs->destination_location }}">                            
                            {{ trim($company_arr[$rs->destination_id] . ($rs->destination_id ? "" : $rs->destination)   ) . " - " . trim(($rs->destination_location_id ? "" :  $rs->destination_location) . $location[$rs->destination_location_id])}}                        
                         </td>                        
                        <td><input type="hidden" name="passenger[]" value="{{ $rs->noofpassengers }}">{{ $rs->noofpassengers }}</td>                        
                        <td><input type="hidden" name="triptime[]" value="{{ $rs->estimated_trip_time }}">{{ $rs->estimated_trip_time }}</td>                        
                        <td><input type="hidden" name="vtype[]" placeholder="vtype[]" ></td>                        
                        <td>
                            <input type="hidden" name="plateid[]" placeholder="plateid[]" >
                            <input type="hidden" name="plateno[]"  placeholder="plateno[]"  >
                        </td>                    
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>

                <div class="row_form_container">
                        <div class="">
                            <button type="button" name="add" value="" disabled="" >ADD</button>
                            <button type="button" name="edit" value="edit" >EDIT</button>
                            <button type="button" name="delete" value="" disabled="" >DELETE</button> 
                        </div>
                </div>
<div class="row" >
                <div class="clear_10"></div>
        </div>
               </div>

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                 <div class="clear_10"></div>       
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea {{ $readonly }} rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                </div>
            </div>

            <div class="clear_10"></div>            
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>ACCEPT</strong> FOR PROCESSING</label>
                    </div> 
                    <div class="comment_button">
                        <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="send">ACCEPT</button>
                    </div>
                </div>
                <div class="clear_10"></div> 
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO FILER</label>
                    </div> 
                    <div class="comment_button">
                        <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="return">RETURN</button>
                    </div>
                </div>  
            </div>


             @include('cc/template/back')
        </div><!-- end of form_container -->


<div class="modal fade" id="service-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">ADD SERVICE VEHICLE DETAILS</h4>
      </div>

      <div class="modal-body">
        <div class="container-fluid bd-example-row">
              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP DATE: </label></div>
                <div class="col-sm-3">
                    <input type="text" id="pickup-date" class="date_picker" disabled />
                </div>
              </div>

              <div class="clear_10"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP TIME: </label></div>
                <div class="col-sm-3">
                    <input type="text" id="pickup-time" disabled />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">PICK-UP PLACE: </label></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels kepp-all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPANY/PLANT/ENTITY: </label></div>
                <div class="col-sm-3">
                    <select id="pickup-company" disabled >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option loc="{{ $rs->location_id }}" value="{{ $rs->company_id }}">{{ $rs->company }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="pickup-company-other-tick" disabled /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="pickup-company-other" disabled /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOCATION: </label></div>
                <div class="col-sm-3">
                    <select id="pickup-location" disabled >
                        @foreach($location as $key => $val)                                      
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="pickup-location-other-tick" disabled /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="pickup-location-other" disabled  /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">DESTINATION</label></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels kepp-all">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;COMPANY/PLANT/ENTITY: </label></div>
                <div class="col-sm-3">
                    <select id="destin-company" disabled >
                        <option value="" ></option>
                        @foreach($company as $rs)                                      
                        <option loc="{{ $rs->location_id }}" value="{{ $rs->company_id }}">{{ $rs->company }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="destin-company-other-tick" disabled /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="destin-company-other" disabled /></div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;LOCATION: </label></div>
                <div class="col-sm-3">
                    <select id="destin-location" disabled >
                        <option value="" ></option>
                        @foreach($location as $key => $val)                                      
                        <option value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
                <div class="col-sm-2 text-right">
                    <input type="checkbox" id="destin-location-other-tick" disabled /> <label class="labels">OTHER</label>
                </div>
                <div class="col-sm-4"><input type="text" id="destin-location-other" disabled /></div>
              </div> 

              <div class="clear_20"></div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">NUMBER&nbsp;OF&nbsp;PASSENGERS:</label></div>
                <div class="col-sm-3">
                    <input type="text" id="passenger" disabled />
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">ESTIMATED TRIP TIME: </label></div>
                <div class="col-sm-3">
                    <input type="text" id="triptime" disabled />
                </div>
              </div>


              <div class="row">
                <div class="col-sm-3"><label class="labels required">VEHICLE TYPE: </label></div>
                <div class="col-sm-3">
                    <select id="vehicletype" >
                        @foreach($vehicle as $key => $val)                                      
                        <option type="{{ $key }}" value="{{ $key }}">{{ $val }}</option>
                        @endforeach                               
                    </select>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-3"><label class="labels required">PLATE NUMBER: </label></div>
                <div class="col-sm-3">
                    <select id="plateno" >
                        <option value="" ></option>
                        @foreach($plateno as $rs)                                      
                        <option type="{{ $rs->vehicle_type_id }}" value="{{ $rs->vehicle_id }}">{{ $rs->plate_no }}</option>
                        @endforeach                               
                    </select>
                </div>
              </div>
          </div>
      </div>

      <div class="text-center  ">
        <button type="button" class="btn btn-secondary text-center" id="save" >SAVE</button>
        <button type="button" class="btn btn-secondary text-center" data-dismiss="modal">CANCEL</button>        
      </div>
      <div class="text-center modal-footer ">
      </div>
    </div>
  </div>
</div>

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/svr/reviewform.js') }}
@stop