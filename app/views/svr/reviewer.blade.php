@extends('template/header')

@section('content')

<div id="wrap">
 
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">

           
    <div class="">   
           
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company1 form-control" name="" >
                    <option value="">All</option>
                    <?php $comp_arr_1 = array();?>
                    @foreach($record as $rs)
                    @if($rs["status"] == "FOR ACCEPTANCE" && !in_array($rs["company"] , $comp_arr_1))
                    <option value="{{ $rs["company"] }}">{{ $comp_arr_1[] = $rs["company"] }}</option>
                    @endif
                    @endforeach

                </select>
            </div>
        </div>

  

        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department1 form-control" name="" >
                    <option value="">All</option>
                    <?php $dep_arr_1 = array();?>
                    @foreach($record as $rs)
                    @if($rs["status"] == "FOR ACCEPTANCE" && !in_array($rs["department"] , $dep_arr_1))
                    <option comp="{{ $rs["company"] }}" value="{{ $rs["department"] }}">{{ $dep_arr_1[] = $rs["department"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR ACCEPTANCE</span>
            <table id="myrecord1" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                     @if($rs["status"] == "FOR ACCEPTANCE")
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["approveddate"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>FOR ACCEPTANCE</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["department"] }}</td>
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endif
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

    <div class="clear_20"></div>   

    <div class="">   
           
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                <select class="company2 form-control" name="" >
                    <option value="">All</option>
                    <?php $comp_arr_2 = array();?>
                    @foreach($record as $rs)
                    @if($rs["status"] == "FOR PROCESSING" && !in_array($rs["company"] , $comp_arr_2))
                    <option value="{{ $rs["company"] }}">{{ $comp_arr_2[] = $rs["company"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>


        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department2 form-control" name="" >
                    <option value="">All</option>
                    <?php $dep_arr_2 = array();?>
                    @foreach($record as $rs)
                    @if($rs["status"] == "FOR PROCESSING" && !in_array($rs["department"] , $dep_arr_2))
                    <option comp="{{ $rs["company"] }}" value="{{ $rs["department"] }}">{{ $dep_arr_2[] = $rs["department"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_20"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR PROCESSING</span>
            <table id="myrecord2" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                     @if($rs["status"] == "FOR PROCESSING")
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["approveddate"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>FOR PROCESSING</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["department"] }}</td>
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endif
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

    <div class="clear_20"></div>   

    <div class="">   
          {{ Form::open(array('url' => 'svr/delete/rows', 'method' => 'post')) }}       
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                
                <select class="company3 form-control" name="" >
                    <option value="">All</option>
                    <?php $comp_arr_3 = array();?>
                    @foreach($record as $rs)
                    @if(($rs["status"] == "SERVED" || $rs["status"] == "SERVED/DELETED" )  && !in_array($rs["company"] , $comp_arr_3))
                    <option value="{{ $rs["company"] }}">{{ $comp_arr_3[] = $rs["company"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>

     

        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department3 form-control" name="" >
                    <option value="">All</option>
                    <?php $dep_arr_3 = array();?>
                    @foreach($record as $rs)
                    @if(($rs["status"] == "SERVED" || $rs["status"] == "SERVED/DELETED" ) && !in_array($rs["department"] , $dep_arr_3))
                    <option comp="{{ $rs["company"] }}" value="{{ $rs["department"] }}">{{ $dep_arr_3[] = $rs["department"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_20"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >SERVED SERVICE VEHICLE</span>
            <table id="myrecord3" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                     @if($rs["status"] == "SERVED" || $rs["status"] == "SERVED/DELETED" )
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["approveddate"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>SERVED</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["department"] }}</td>
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endif
                    @endforeach
                    
                </tbody>
            </table>  
        </div>
    {{ Form::close() }}
    </div>

    <div class="clear_20"></div>   

    <div class="">   
        {{ Form::open(array('url' => 'svr/delete/rows', 'method' => 'post')) }}     
        <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">COMPANY:</label>
            </div>
            <div class="col2_form_container">
                
                <select class="company4 form-control" name="" >
                    <option value="">All</option>
                    <?php $comp_arr_4 = array();?>
                    @foreach($record as $rs)
                    @if(($rs["status"] == "CANCELLED" || $rs["status"] == "CANCELLED/DELETED")  && !in_array($rs["company"] , $comp_arr_4))
                    <option value="{{ $rs["company"] }}">{{ $comp_arr_4[] = $rs["company"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>

 
        <div class="clear_10"></div> 

        <div class="row_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department4 form-control" name="" >
                    <option value="">All</option>
                    <?php $dep_arr_4 = array();?>
                    @foreach($record as $rs)
                    @if(($rs["status"] == "CANCELLED" || $rs["status"] == "CANCELLED/DELETED") && !in_array($rs["department"] , $dep_arr_4))
                    <option comp="{{ $rs["company"] }}" value="{{ $rs["department"] }}">{{ $dep_arr_4[] = $rs["department"] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
    
        <div class="clear_20"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >CANCELLED SERVICE VEHICLE</span>
            <table id="myrecord4" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">COMPANY</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                     @if($rs["status"] == "CANCELLED" || $rs["status"] == "CANCELLED/DELETED")
                    <tr>
                    <td>{{ $rs["reference"] }}</td> 
                    <td>{{ $rs["approveddate"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>CANCELLED</td>  
                    <td>{{ $rs["requestedby"] }}</td>  
                    <td>{{ $rs["department"] }}</td>
                    <td>{{ $rs["company"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endif
                    @endforeach
                    
                </tbody>
            </table>  
        </div>
     {{ Form::close() }}   
    </div>

    <div class="clear_20"></div>   
</div>
</div>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/svr/reviewer.js?v9') }}
@stop