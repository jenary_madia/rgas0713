@extends('template/header')

@section('content')

<div id="wrap">
	<!-- @include('template/sidebar') -->
	<div class="wrapper">
		<div class="clear_20"></div>
			@if(Session::get('is_cbr_staff'))
				<hr/>		
				<h3>Waiting for your Approval</h3>
				<table id="requests_dashboard_staff_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_dashboard_staff_list">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">CBR REQUESTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">REQUESTOR</th>
							<th align="center">DATE REQUESTED</th>
							<th align="center">STATUS</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
			@endif
			
			@if(Session::get('designation') == 'VP FOR CORP. HUMAN RESOURCE' || Session::get('designation') == 'SVP FOR CORP. SERVICES' || Session::get('designation') == 'PRESIDENT')
				<hr/>		
				<h3>Waiting for your Approval</h3>
				<table id="approver_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="approver_dashboard">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">TRAINING ENDORSEMENTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">REQUESTOR</th>
							<th align="center">DATE REQUESTED</th>
							<th align="center">STATUS</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
			@endif
			
				<div class="clear_20"></div>
				<h3>Pending</h3>
				<table id="requests_dashboard_filer_list" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="requests_dashboard_filer_list">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">MY CBR REQUESTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">REQUESTOR</th>
							<th align="center">STATUS</th>
							<th align="center">DATE REQUESTED</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
				<div class="clear_20"></div>
				
			@if(Session::get('dept_id') == 24 || Session::get('dept_id') == 144)
				<div class="clear_20"></div>
				<table id="pmarf_filer" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="pmarf_filer">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">PMARF REQUESTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">DATE REQUESTED</th>
							<th align="center">STATUS</th>
							<th align="center">CURRENT</th>
							<th align="center">ACTIVITY TYPE</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
				@if(Session::get('desig_level') != 'employee')
				<h3>Waiting for your Approval</h3>
				<div class="clear_20"></div>
				<table id="pmarf_approver" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="pmarf_approver">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">PMARF REQUESTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">DATE FILED</th>
							<th align="center">REQUESTOR</th>
							<th align="center">TYPE OF ACTIVITY</th>
							<th align="center">STATUS</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
				@endif
				
			@endif
			
			@if(Session::get('desig_level') == 'head')
				<div class="clear_20"></div>
				<table id="filer_dashboard" cellpadding="0" cellspacing="0" border="0" class="display dataTable tbl_fonts" width="100%" aria-describedby="filer_dashboard">
					<thead>
						<tr role="row">
							<th align="" width="" class="" role="columnheader" rowspan="1" colspan="4" aria-label="Company">TRAINING ENDORSEMENT REQUESTS</th>
						</tr>
						<tr role="row">
							<th align="center">REFERENCE NO.</th>
							<th align="center">DATE REQUESTED</th>
							<th align="center">STATUS</th>
							<th align="center">CURRENT</th>
							<th align="center">ACTION</th>
						</tr>							
					</thead>
				</table>
			@endif
			
			
		<div class="clear_60"></div>
	</div>
</div>
@stop

