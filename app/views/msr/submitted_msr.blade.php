@extends('template/header')

@section('content')
    {{ Form::open(array('url' => 'msr/export', 'method' => 'post', 'files' => true ,'id' => 'receiverSearch')) }}
    <div id="wrap">
        <div class="wrapper">
            DEPARTMENT : <select name="" id="department">
                        <option value="0">ALL</option>
                        @foreach($departments as $department)
                            <option value="{{ $department['id'] }}">{{ $department['dept_name'] }}</option>
                        @endforeach
                        </select>
            <div class="datatable_holder">
                <table style="table-layout : fixed;word-wrap:break-word;" id="submittedMSR" cellpadding="0" cellspacing="0" border="0" class="display dataTable leaves" aria-describedby="submittedMSR">
                    <thead>
                    <tr role="row">
                        <th  style="text-align :left" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reference Number</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Date Filed</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">Reason of Request</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Status</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >From</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >Action</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        @include("msr.modal.generate_report")
    </div>
    {{ Form::close() }}
@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/msr/msr_datatables.js') }}
@stop
