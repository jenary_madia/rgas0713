@extends('template/header')

@section('content')
    <div id="msr_create" v-cloak>
        {{ Form::open(['url' => 'msr/smis/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']) }}
        <div class="form_container msr-form-container">
            <div class="container-header"><h5 class="text-center"><strong>MANPOWER SERVICE REQUEST FORM</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row employee-details">
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NAME:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="{{ Session::get('employee_name') }}" />
                </div>
                <div class="col-md-6 pull-left">
                    <label class="labels required">REFERENCE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="{{ Session::get('employeeid') }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DATE FILED:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">COMPANY:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="{{ Session::get('company') }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels required pull-left">STATUS:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="NEW" />
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels required pull-left">DEPARTMENT:</label>
                    <input readonly="readonly" type="text" class="form-control pull-right" name="department" value="{{ Session::get('dept_name') }}" />
                </div>
                <div class="col-md-6">
                    <label class="labels">CONTACT NUMBER:</label>
                    <input type="text" class="form-control pull-right" name="contactNumber" maxlength="20" value="{{ Input::old('contactNumber') }}"/>
                </div>
                <div class="clear_10"></div>
                <div class="col-md-6">
                    <label class="labels pull-left">SECTION:</label>
                    <input type="text" class="form-control pull-right" name="section" maxlength="25" value="{{ Input::old('section', Session::get('sect_name')) }}" />
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>REQUEST DETAILS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-4">
                    <label class="labels required">SOURCE : </label>
                    <div class="pull-right" style="display:inline-block;">
                        {{ Form::radio('source', 'internal', (Input::old('source') == 'internal')) }} <label class="radio-label">INTERNAL(I-QUEST)</label>
                        {{ Form::radio('source', 'external', (Input::old('source') == 'external')) }} <label class="radio-label">EXTERNAL</label>
                    </div>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">PANELS : </label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" name="panel" >{{ Input::old("panel") }}</textarea>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-4">
                    <label class="labels required">URGENCY:</label>
                    <div class="pull-right" style="display:inline-block;">
                        {{ Form::radio('urgency', 'rush', (Input::old('urgency') == 'rush')) }} <label class="radio-label">RUSH</label>
                        {{ Form::radio('urgency', 'normal', (Input::old('urgency') == 'normal')) }} <label class="radio-label">NORMAL PRIORITY</label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">PROJECT TYPE/NAME : &nbsp;</label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" maxlength="100" name="projType" >{{ Input::old("projType") }}</textarea>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">SAMPLE SIZE PER PANEL : </label>
                    <textarea style="max-width : 239px;" name="sizePerPanel" id="" class="form-control pull-right" cols="10">{{ Input::old("sizePerPanel") }}</textarea>
                </div>

                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-5">
                    <label class="labels required pull-left">BACKGROUND / OBJECTIVE : &nbsp;</label>
                    <textarea id="" style="max-width : 239px;" class="form-control pull-right" name="objective" >{{ Input::old("objective") }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>PROJECT REQUIREMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row project-requirements">
                <div class="col-md-12" style="overflow-x: scroll;">
                    <table class="table table-bordered" style=" width : 1600px; ">
                        <thead>
                            <tr>
                                <th rowspan="2">JOB TITLE/JOB POSITION</th>
                                <th colspan="2">RECRUITMENT</th>
                                <th colspan="2">PREPARATION</th>
                                <th colspan="2">BRIEFING</th>
                                <th colspan="3">FIELD WORK</th>
                                <th colspan="2">EDITING</th>
                                <th colspan="2">CODING</th>
                                <th colspan="2">ENCODING</th>
                                <th colspan="2">DATA CLEANING</th>
                            </tr>
                            <tr>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>AREA</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                                <th>DATE NEEDED</th>
                                <th>NUMBER OF PAX</th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr v-for="req in projectRequirements" v-bind:class="{ 'active' : req.active }" @click="getForEdit($index,req)">
                            <td>@{{ req.jobTitle }}</td>
                            <td v-if="req.recruitment.to != '' ">@{{ req.recruitment.from }} - @{{ req.recruitment.to }}</td>
                            <td v-else>@{{ req.recruitment.from }}</td>
                            <td>@{{ req.recruitment.pax }}</td>
                            <td v-if="req.preparation.to != '' ">@{{ req.preparation.from }} - @{{ req.preparation.to }}</td>
                            <td v-else>@{{ req.preparation.from }}</td>
                            <td>@{{ req.preparation.pax }}</td>
                            <td v-if="req.briefing.to != '' ">@{{ req.briefing.from }} - @{{ req.briefing.to }}</td>
                            <td v-else>@{{ req.briefing.from }}</td>
                            <td>@{{ req.briefing.pax }}</td>
                            <td v-if="req.fieldWork.to != '' ">@{{ req.fieldWork.from }} - @{{ req.fieldWork.to }}</td>
                            <td v-else>@{{ req.fieldWork.from }}</td>
                            <td>@{{ req.fieldWork.pax }}</td>
                            <td>@{{ req.fieldWork.area }}</td>
                            <td v-if="req.editing.to != '' ">@{{ req.editing.from }} - @{{ req.editing.to }}</td>
                            <td v-else>@{{ req.editing.from }}</td>
                            <td>@{{ req.editing.pax }}</td>
                            <td v-if="req.coding.to != '' ">@{{ req.coding.from }} - @{{ req.coding.to }}</td>
                            <td v-else>@{{ req.coding.from }}</td>
                            <td>@{{ req.coding.pax }}</td>
                            <td v-if="req.encoding.to != '' ">@{{ req.encoding.from }} - @{{ req.encoding.to }}</td>
                            <td v-else>@{{ req.encoding.from }}</td>
                            <td>@{{ req.encoding.pax }}</td>
                            <td v-if="req.dataCleaning.to != '' ">@{{ req.dataCleaning.from }} - @{{ req.dataCleaning.to }}</td>
                            <td v-else>@{{ req.dataCleaning.from }}</td>
                            <td>@{{ req.dataCleaning.pax }}</td>
                        </tr>
                        </tbody>
                        <input type="hidden" name="projectRequirements" value="@{{ projectRequirements | json }}">
                        <input type="hidden" v-model="projectRequirementsOld" value='{{ Input::old("projectRequirements","[]") }}'>
                        <input type="hidden" name="agencies" value="@{{ agencies | json }}">
                        <input type="hidden" v-model="agenciesOld"  value='{{ Input::old("agencies","[]") }}'>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-12">
                    <a class="btn btn-default btndefault" data-toggle="modal" data-target="#addProjectRequirement">ADD</a>
                    <a class="btn btn-default btndefault" @click="toggleEditProjReq">EDIT</a>
                    <a class="btn btn-default btndefault offsetDelete" @click="toggleDeleteProjReq">DELETE</a>
                </div>
                <div class="clear_20"></div>
                <div class="clear_20"></div>
                <div class="col-md-2">
                    <label class="labels required">OTHER INFORMATION:</label>
                </div>
                <div class="col-md-10">
                    <textarea class="form-control" id="" cols="30" name="otherInformation">{{ Input::old("otherInformation") }}</textarea>
                </div>
            </div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="container-header"><h5 class="text-center lined"><strong>RECOMMENDED MANPOWER AGENCIES</strong></h5></div>
            <div class="clear_20"></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>MANPOWER PROVIDER/AGENCY</th>
                            <th>CONTACT INFORMATION (ADDRESS/CONTACT NUMBER/EMAIL)</th>
                            <th>MANPOWER REMARKS (TO BE FILLED OUT BY THE FINAL RECIPIENT)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="agency in agencies" v-bind:class="{ 'active' : agency.active }" @click="getForEditAgency($index,agency)">
                        <td>@{{ agency.agency }}</td>
                        <td>@{{ agency.contactInfo }}</td>
                        <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clear_20"></div>
                <div class="clear_10"></div>
                <div class="col-md-12">
                    <a class="btn btn-default btndefault" data-toggle="modal" data-target="#addManPowerAgency">ADD</a>
                    <a class="btn btn-default btndefault" @click="toggleEditAgency">EDIT</a>
                    <a class="btn btn-default btndefault offsetDelete" @click="toggleDeleteAgency">DELETE</a>
                </div>
                <div class="clear_20"></div>
            </div>
            <div class="clear_20"></div>
            <div>
                <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
                <div class="clear_20"></div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div id="attachments">
                            @if(Input::old("files"))
                                @for ($i = 1; $i <= count(Input::old("files")); $i++)
                                    <p>
                                        {{ Input::old("files")[$i]["original_filename"] }} | {{ Input::old("files")[$i]["filesize"] }}
                                        <input type='hidden' name='files[{{ $i }}][random_filename]' value='{{ Input::old("files")[$i]["random_filename"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_filename]' value='{{ Input::old("files")[$i]["original_filename"] }}'>
                                        <input type='hidden' class='attachment_filesize' name='files[{{ $i }}][filesize]' value='{{ Input::old("files")[$i]["filesize"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][mime_type]' value='{{ Input::old("files")[$i]["mime_type"] }}'>
                                        <input type='hidden' name='files[{{ $i }}][original_extension]' value='{{ Input::old("files")[$i]["original_extension"] }}'>
                                        <button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>
                                    </p>
                                @endfor
                            @endif
                        </div>
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="fileupload" type="file" multiple name="attachments[]" data-url="{{ route('file-uploader.store') }}">
                        </span>
                    </div>
                </div>
            </div>


        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <span class="action-label labels">ACTION</span>
        <div class="form_container msr-form-container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 comment-box">
                    <label class="labels pull-left">COMMENT:</label>
                    <textarea rows="3" class="form-control pull-left" name="comment">{{ Input::old("comment") }}</textarea>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="row actions">
                        <div class="col-md-6">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="row actions">
                        <div class="col-md-6">
                            <label class="button_notes"><strong>SEND</strong> TO IMMEDIATE SUPERIOR FOR APPROVAL</label>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div>



        </div><!-- end of form_container -->
        {{ Form::close() }}
        @include("msr.smis.modal.modal_create")
    </div>

@stop
@section('js_ko')
    {{ HTML::script('/assets/js/msr/general.js') }}
    {{ HTML::script('/assets/js/notification/vue.js') }}
    {{ HTML::script('/assets/js/notification/vue-validator.min.js') }}
    {{ HTML::script('/assets/js/notification/vue-resource.min.js') }}
    {{ HTML::script('/assets/js/msr/vue_smis.js') }}
    <script>
        $('body').on('click','.remove-fn',function () {
            $(this).closest( "p" ).remove();
        });
                @if(Input::old("files"))
        var file_counter = {{ $i }};
                @else
        var file_counter = 0;
                @endif
        var allowed_file_count = 10;
        var allowed_total_filesize = 20971520;
    </script>
    {{ HTML::script('/assets/js/leave-notif-file-upload-generic.js') }}
@stop
