<div class="modal fade" id="addProjectRequirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">PROJECT REQUIREMENT</h4>
            </div>
            <div class="modal-body">
                <validator name="addProjReqValidator">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="" class="labels required">JOB TITLE/POSITION : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="" id="" v-validate:jobTitle = "['required']" v-model='toAddProjectReq.jobTitle' class='form-control'>
                                    <option value="@{{jobPosition['item']}}"  v-for="jobPosition in latestJobPositions">
                                        @{{jobPosition['text']}}
                                    </option>
                                </select>
                                <input type="hidden" v-model="jobPositions" value='{{ json_encode($jobTitle) }}' >
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>RECRUITMENT DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.recruitment.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.recruitment.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">RECRUITMENT PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.recruitment.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>PREPARATION DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.preparation.from"  type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.preparation.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">PREPARATION PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.preparation.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>BRIEFING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.briefing.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.briefing.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">BRIEFING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.briefing.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>FIELDWORK DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.fieldWork.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.fieldWork.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">FIELDWORK PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.fieldWork.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="" class="labels">AREA : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.fieldWork.area" type="text" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>EDITING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.editing.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.editing.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">EDITING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.editing.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>CODING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.coding.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.coding.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">CODING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.coding.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>ENCODING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.encoding.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.encoding.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">ENCODING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.encoding.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>DATA CLEANING NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.dataCleaning.from" min="1" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toAddProjectReq.dataCleaning.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">DATA CLEANING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toAddProjectReq.dataCleaning.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12 pager">
                            <button class="btn btn-default btndefault" :disabled="! $addProjReqValidator.valid" @click="addProjectReq">SAVE</button>
                            <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                </validator>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editProjectRequirement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">PROJECT REQUIREMENT</h4>
            </div>
            <div class="modal-body">
                <validator name="editProjReqValidator">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="" class="labels required">JOB TITLE/POSITION : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" readonly value="@{{ toEditProjectReq.jobTitle }}" class='form-control'>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>RECRUITMENT DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.recruitment.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.recruitment.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">RECRUITMENT PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.recruitment.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>PREPARATION DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.preparation.from"  type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.preparation.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">PREPARATION PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.preparation.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h5><strong>BRIEFING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.briefing.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.briefing.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">BRIEFING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.briefing.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>FIELDWORK DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.fieldWork.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.fieldWork.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">FIELDWORK PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.fieldWork.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label for="" class="labels">AREA : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.fieldWork.area" type="text" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>EDITING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.editing.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.editing.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">EDITING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.editing.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>CODING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.coding.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.coding.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">CODING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.coding.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>ENCODING DATE NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.encoding.from" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.encoding.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">ENCODING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.encoding.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h5><strong>DATA CLEANING NEEDED</strong></h5>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <label for="" class="labels">FROM : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.dataCleaning.from" min="1" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">TO : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="pull-right input-group bootstrap-timepicker timepicker">
                                <input v-model="toEditProjectReq.dataCleaning.to" type="text" readonly class="date_picker form-control input-small">
                                <span class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-12 spacer"></div>
                        <div class="col-md-4">
                            <label for="" class="labels">DATA CLEANING PAX : </label>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <input v-model="toEditProjectReq.dataCleaning.pax" min="1" type="number" max="99"  class="form-control numeric">
                            </div>
                        </div>

                        <div class="col-md-12 pager">
                            <button class="btn btn-default btndefault" :disabled="! $editProjReqValidator.valid" data-dismiss="modal" @click="savePRUpdate">SAVE</button>
                            <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                        </div>
                    </div>
                </validator>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addManPowerAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD RECOMMENDED MANPOWER AGENCY</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">AGENCY : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toAddAgency.agency">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">CONTACT INFORMATION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toAddAgency.contactInfo">
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="addAgency">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editManPowerAgency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ADD TIME OFFSET REFERENCE DETAILS</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <label for="" class="labels">AGENCY : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditAgency.agency">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <label for="" class="labels">CONTACT INFORMATION : </label>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" v-model="toEditAgency.contactInfo">
                        </div>
                    </div>

                    <div class="col-md-12 pager">
                        <button class="btn btn-default btndefault" data-dismiss="modal" @click="saveAgencyUpdate">SAVE</button>
                        <button class="btn btn-default btndefault" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

