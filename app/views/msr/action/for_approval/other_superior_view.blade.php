{{ Form::open(array('url' => array('msr/for_approval_action',$MSRDetails['id']), 'method' => 'post', 'files' => true)) }}
<div class="clear_20"></div>
<span class="action-label labels">ACTION</span>
<div class="form_container msr-form-container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 comment-box">
            <label class="labels pull-left">MESSAGE:</label>
            <textarea rows="3" class="form-control pull-left" readonly>@foreach (explode('|',$MSRDetails['comment']) as $message){{ json_decode(str_replace("\\","/",$message),true)['name'].' '.json_decode(str_replace("\\","/",$message),true)['datetime'].' : '.json_decode(str_replace("\\","/",$message),true)['message'] }} &#013;@endforeach</textarea>
            <label class="labels pull-left">COMMENT:</label>
            <textarea rows="3" class="form-control pull-left" name="comment"></textarea>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes pull-left" style=" margin: 5px 0 0 0"><strong>REQUEST</strong> FOR APPROVAL OF </label>
                    <select class="form-control pull-right" style="width: 200px; margin: 0 0 0 50px;" name="otherSuperior">
                        <option value="" selected disabled></option>
                        @foreach($otherIs as $IS)
                            <option value="{{ $IS['id'] }}">{{ $IS['firstname'].' '.$IS['middlename'].' '.$IS['lastname'] }} ({{ $IS['desig_level'] }})</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="request">REQUEST</button>
                </div>
            </div>
            <div class="row actions">
                <div class="col-md-6">
                    <label class="button_notes pull-left" style=" margin: 5px 0 0 0"><strong>DISAPPROVE</strong> AND <strong>RETURN</strong> TO </label>
                    <select class="form-control pull-right" style="width:200px; margin: 0 0 0 50px;" name="retOtherSuperior">
                        <option selected disabled></option>
                        <option value="{{ $MSRDetails['employeeid']}}|{{ $MSRDetails['owner']['firstname'].' '.$MSRDetails['owner']['middlename'].' '.$MSRDetails['owner']['lastname'] }}">{{ $MSRDetails['owner']['firstname'].' '.$MSRDetails['owner']['middlename'].' '.$MSRDetails['owner']['lastname'] }}</option>
                    @foreach($MSRDetails['signatories'] as $signatory)
                            <option value="{{ $signatory['employee']['id']}}|{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}">{{ $signatory['employee']['firstname'].' '.$signatory['employee']['middlename'].' '.$signatory['employee']['lastname'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-default btndefault" name="action" value="returnToIS">RETURN</button>
                </div>
            </div>
            <div class="row actions">
                <div class="col-md-12 text-center">
                    <a class="btn btn-default btndefault" href="{{ URL::previous() }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div><!-- end of form_container -->
{{ Form::close() }}