@extends('template/header')

@section('content')

<div id="wrap">
 
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">
               
    <div class="clear_10"></div>                 
    <div class="">  
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >MY DESIGNS</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>                        
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td>  
                    <td>{{ $rs["date"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["current"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  
    </div>

    <div class="clear_20"></div>  

    @if(in_array(Session::get("desig_level") ,["head","president","top mngt"]))
    <div class="">  
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >FOR APPROVAL</span>
            <table id="reviewer_record" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead> 
                    <tr role="row">
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REFERENCE NUMBER</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE FILED</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">REQUEST TYPE</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FROM</th>                        
                        <th class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record_approval as $rs)
                    <tr>
                    <td>{{ $rs["reference"] }}</td>  
                    <td>{{ $rs["date"] }}</td>  
                    <td>{{ $rs["type"] }}</td>  
                    <td>{{ $rs["status"] }}</td>  
                    <td>{{ $rs["from"] }}</td>  
                    <td>{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>  
    </div>
    @endif
</div>
</div>


@stop
@section('js_ko')
{{ HTML::script('/assets/js/art/filer.js') }}
@stop