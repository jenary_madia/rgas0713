@extends('template/header')

@section('content')
    {{ Form::open(array('url' => "art/edit/action/$id", 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">ART DESIGN REQUEST FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" name="reference" class="form-control" value="{{ $record['reference_no'] }}" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ $record['date_filed'] }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="{{ $record['status'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" maxlength="20" class="form-control" name="contact" value="{{ $record['contact'] }}" />
                        </div>
                    </div>
        
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control"  name="section"   value="{{  $record['section'] }}" />
                        </div>
                    </div>

                    
                   <div class="clear_20"></div>
                     </div>
                      <div class="container-header"><h5 class="text-center lined sub-header "><strong>REQUEST DETAILS</strong></h5></div>
                    <div class="row" >
                    <div class="clear_20"></div>

                   <div class="row3_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PROJECT NAME:</label>
                        </div>
                        <div class="row_form_container">
                            <input maxlength="100" type="text" name="project" value="{{ $record['project'] }}" class="form-control"/>
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DUE DATE:</label>
                        </div>
                        <div class=" input-group col2_form_container">
                            <input  type="text" id="duedate" name="duedate" value="{{ $record['duedate'] }}"  class="form-control date_picker"/>
                            <label class="input-group-addon btn" for="duedate">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </label>  
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REQUEST TYPE:</label>
                        </div>
                        <div class="col4_form_container">
                            <input type="radio" name="requestype" value="design" {{ $record['request'] == "design" ? "checked" : "" }} /> DESIGN
                        </div>
                        <div class="col4_form_container">
                            <input type="radio"  name="requestype" value="mockup" {{ $record['request'] == "mockup" ? "checked" : "" }}  /> MOCK UP
                        </div>
                    </div>

                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">SERVICE REQUESTED:</label>
                        </div>
                        <div class="col2_form_container">
                            <select name="servicerequested"  class="form-control" >
                                    <option value="" ></option>
                                    <option {{ $record['service_requested'] == "New Product Packaging Design" ? "selected" : "" }} value="New Product Packaging Design" >New Product Packaging Design</option>
                                    <option {{ $record['service_requested'] == "Existing Product Packaging Design Enhancement/Improvement" ? "selected" : "" }} value="Existing Product Packaging Design Enhancement/Improvement" >Existing Product Packaging Design Enhancement/Improvement</option>
                                    <option {{ $record['service_requested'] == "Promo & Merchandising Materials" ? "selected" : "" }} value="Promo & Merchandising Materials" >Promo & Merchandising Materials</option>
                                    <option {{ $record['service_requested'] == "Event Materials" ? "selected" : "" }} value="Event Materials" >Event Materials</option>
                            </select>
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PACKAGING/PROMO MATERIAL:</label>
                        </div>
                        <div class="col2_form_container">
                            <select name="packaging"  class="form-control" >
                                <option value="" ></option>
                                <option {{ $record['packaging_materials'] == "Primary Wrapper" ? "selected" : "" }} value="Primary Wrapper" >Primary Wrapper</option>
                                <option {{ $record['packaging_materials'] == "Secondary Wrapper" ? "selected" : "" }} value="Secondary Wrapper" >Secondary Wrapper</option>
                                <option {{ $record['packaging_materials'] == "Carton/Case" ? "selected" : "" }} value="Carton/Case" >Carton/Case</option>
                                <option {{ $record['packaging_materials'] == "Gondola" ? "selected" : "" }} value="Gondola" >Gondola</option>  
                                <option {{ $record['packaging_materials'] == "Banner" ? "selected" : "" }} value="Banner" >Banner</option>
                                <option {{ $record['packaging_materials'] == "Merchandising Materials" ? "selected" : "" }} value="Merchandising Materials" >Merchandising Materials</option>  
                                <option {{ $record['packaging_materials'] == "Others" ? "selected" : "" }} value="Others" >Others</option>                                  
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container othermaterial {{ $record['packaging_materials'] != "Others" ? "hide" : "" }} ">
                        <div class="col1_form_container text-right">
                            <label class="labels text-right required">OTHERS: &nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" name="othermaterial" value="{{ $record['othermaterials'] }}"  class="form-control"  maxlength="100"  />
                        </div>
                    </div>                    
                </div>
            
            <div class="clear_20 materialspecification {{ $record['service_requested'] == "Event Materials" ? "hide" : "" }}" ></div>
            <div class="  materialspecification {{ $record['service_requested'] == "Event Materials" ? "hide" : "" }}">
                         <div class="container-header"><h5 class="text-center lined sub-header "><strong>MATERIAL SPECIFICATION</strong></h5></div>
                
            </div>
            <div class="clear_10 materialspecification {{ $record['service_requested'] == "Event Materials" ? "hide" : "" }}"></div>
            
            <div style="float: left;width: 600px;margin-left: -12px;" class="materialspecification {{ $record['service_requested'] == "Event Materials" ? "hide" : "" }}">                    
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels required">PRODUCT NAME:</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="100" type="text" class="form-control" value="{{ $record['productname'] }}"  name="productname" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label id="lenght" class="labels {{ in_array($record['packaging_materials'],["Primary Wrapper","Carton/Case","Secondary Wrapper","Gondola","Banner"]) ? "required" : "" }}"  >LENGTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['length'] }}"  name="lenght" value="" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>                  
            
                    <div class="clear_10"></div>
                    
                    <div class="">
                        <div class="col1_form_container">
                            <label id="weight" class="labels {{ in_array($record['packaging_materials'],["Primary Wrapper","Carton/Case","Secondary Wrapper"]) ? "required" : "" }}">NET WEIGHT:</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['netweight'] }}"  name="weight" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label id="width" class="labels {{ in_array($record['packaging_materials'],["Primary Wrapper","Carton/Case","Secondary Wrapper","Gondola","Banner"]) ? "required" : "" }}">WIDTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['width'] }}"  name="width" value="" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>                      
            
                    <div class="clear_10"></div>
                    
                    <div class="">
                        <div class="col1_form_container">&nbsp;</div>
                        <div class="col4_form_container"> &nbsp;</div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label id="height" class="labels {{ in_array($record['packaging_materials'],["Primary Wrapper","Carton/Case","Secondary Wrapper","Gondola"]) ? "required" : "" }}">HEIGHT:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['height'] }}"  name="height" value="" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>                      
          </div>

          <div style="float: left;" class="materialspecification {{ $record['service_requested'] == "Event Materials" ? "hide" : "" }}">                    
                    <div class="">
                        <div class="col5_form_container">
                            <label class="labels">OTHER DETAILS:</label>
                        </div>
                    </div>
            
                    <div class="clear_10"></div>

                    <div class="">
                        <div class="col5_form_container">
                            <textarea maxlength="5000" name="otherspecification"  class="form-control" {{ $record['service_requested'] == "Event Materials" ? "disabled" : "" }}  >{{ $record['otherspecifications'] }}</textarea>
                        </div>
                    </div>

            </div>


            <div class="clear_20 designspecification {{ $record['service_requested'] != "Event Materials" ? "hide" : "" }} "></div>
            <div class="  designspecification {{ $record['service_requested'] != "Event Materials" ? "hide" : "" }}">
                          <div class="container-header"><h5 class="text-center lined sub-header "><strong>DESIGN SPECIFICATION</strong></h5></div>
                
            </div>  
            <div class="clear_10 designspecification {{ $record['service_requested'] != "Event Materials" ? "hide" : "" }}"></div>

            <div style="float: left;width: 450px;margin-left: -12px;" class="designspecification {{ $record['service_requested'] != "Event Materials" ? "hide" : "" }}">                    
                    <div class="row_form_container">
                        <div class="col1_form_container ">
                            <label class="labels required">LENGTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['length'] }}"  name="lenght" value="" {{ $record['service_requested'] != "Event Materials" ? "disabled" : "" }}  />
                        </div>
                    </div>                  
            
                    <div class="clear_10"></div>
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">WIDTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['width'] }}"  name="width" value="" {{ $record['service_requested'] != "Event Materials" ? "disabled" : "" }} />
                        </div>
                    </div>                      
            
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">HEIGHT:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" value="{{ $record['height'] }}"  name="height" value="" {{ $record['service_requested'] != "Event Materials" ? "disabled" : "" }} />
                        </div>
                    </div>                      
          </div>

          <div style="float: left;" class="designspecification {{ $record['service_requested'] != "Event Materials" ? "hide" : "" }}">                    
                   <div class="row_form_container">
                        <div class="col2_form_container  text-center">
                            <label class="labels">OTHER DETAILS:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <textarea maxlength="5000" name="otherspecification"   class="form-control" {{ $record['service_requested'] != "Event Materials" ? "disabled" : "" }} >{{ $record['otherspecifications'] }}</textarea>
                        </div>
                    </div> 
            </div>


                    <div class="clear_20"></div>

                    <div class="row_form_container">
                        <div class="col2_form_container">
                            <label class="labels required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BRIEF DESCRIPTION/ILLUSTRATIONS:</label>
                        </div>
                    </div>
                    
                    <div class="textarea_messages_container" style="border: 0;">
                        <div class="row">
                            <textarea style="max-height: 100px; "  onkeyup="AutoGrowTextArea(this)"  maxlength="5000" rows="3" class="form-control" name="briefdesc">{{ $record['briefdescription'] }}</textarea>
                        </div>
                    </div>

                
            
                <div class="clear_20"></div>
              <div class="container-header"><h5 class="text-center lined sub-header "><strong>ATTACHMENTS</strong></h5></div>
                
                <div class="clear_20"></div>
                
                <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                <div class="attachment_container">
                    @if($record['attachments'])
                    <?php $count = 1000;?>
                    @foreach(json_decode( $record['attachments'] ) as $attach)
                    <p>
                    <input type='hidden' name='files[{{ $count }}][filesize]' value='{{ $attach->filesize }}'>
                    <input type='hidden' name='files[{{ $count }}][mime_type]' value='{{ $attach->mime_type }}'>
                    <input type='hidden' name='files[{{ $count }}][original_extension]' value='{{ $attach->original_extension }}'>
                    <input type='hidden' name='files[{{ $count }}][original_filename]' value='{{ $attach->original_filename }}'>
                    <input type='hidden' name='files[{{ $count }}][random_filename]' value='{{ $attach->random_filename }}'>
                    <?php $count++ ;?>
                    <a href="{{ URL::to('/art/download/') . '/' . $record['reference_no'] . '/' . $attach->random_filename .'/' . CIEncrypt::encode($attach->original_filename) }}">{{ $attach->original_filename }}</a>
                    <input class='attachment-filesize' type='hidden' value='{{ $attach->filesize }}' />
                    <button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attach->original_filename }}"   >DELETE</button><br /></p>
                    @endforeach 
                    @endif

                    <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                                       
                </div>                
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">MESSAGE:</label>
                    <textarea rows="3" disabled class="form-control textarea_inside_width" name="comment">{{ $record['comment'] }}</textarea>
                </div>
                <div class="clear_10"></div> 
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ $record['comment_save'] }}</textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        @if(in_array(Session::get("desig_level") ,["head","president"]))
                        <label class="button_notes"><strong>SEND</strong> TO MKTG-ART FOR PROCESSING</label>
                        @else
                        <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR APPROVAL</label>
                        @endif
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/art/edit.js?v2') }}
<script>
var max_filesize = 0;//mb
var max_upload = 5;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop