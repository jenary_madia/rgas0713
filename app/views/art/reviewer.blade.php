@extends('template/header')

@section('content')

<div id="wrap">
 
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">



             
    <div  >   

          <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department form-control" name="" >
                    <option value="">All</option>
                    @foreach($department as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
             <span class="list-title" role="columnheader" rowspan="1" colspan="5" >PENDING DESIGNS</span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>              
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER &nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED &nbsp;&nbsp;&nbsp;</th>
                        <th width="120" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DUE DATE &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE &nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FILER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record_holder as $rs)
                    <tr>
                    <td class="text-center" >{{ $rs["reference"] }}</td> 
                    <td class="text-center" >{{ $rs["approveddate"] }}</td>  
                    <td class="text-center" >{{ $rs["duedate"] }}</td>   
                    <td class="text-center" >{{ $rs["type"] }}</td>  
                    <td class="text-center" >{{ $rs["status"] }}</td>  
                    <td class="text-center" >{{ $rs["requestedby"] }}</td>  
                    <td class="text-center" >{{ $rs["department"] }}</td>  
                    <td class="text-center" >{{ $rs["current"] }}</td>  
                    <td class="text-center" >{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

        @if( Session::get("is_art_receiver") )
        <div class="clear_20"></div>   
        <div class="datatable_holder">
             <span class="list-title" role="columnheader" rowspan="1" colspan="5" >ALL SUBMITTED DESIGNS</span>
            <table id="myrecord_submitted" border="0" cellspacing="0" class="display dataTable tbl_fonts">
                <thead>              
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REFERENCE NUMBER &nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE APPROVED &nbsp;&nbsp;&nbsp;</th>
                        <th width="120" class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DUE DATE &nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >REQUEST TYPE &nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">FILER</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">CURRENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="" >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                     @foreach($record as $rs)
                    <tr>
                    <td class="text-center" >{{ $rs["reference"] }}</td> 
                    <td class="text-center" >{{ $rs["approveddate"] }}</td>  
                    <td class="text-center" >{{ $rs["duedate"] }}</td>   
                    <td class="text-center" >{{ $rs["type"] }}</td>  
                    <td class="text-center" >{{ $rs["status"] }}</td>  
                    <td class="text-center" >{{ $rs["requestedby"] }}</td>  
                    <td class="text-center" >{{ $rs["department"] }}</td>  
                    <td class="text-center" >{{ $rs["current"] }}</td>  
                    <td class="text-center" >{{ $rs["action"] }}</td> 
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>
        @endif

        <div class="clear_20"></div>   
    </div>

     <div class="clear_20"></div>   

</div>
</div>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/art/reviewer.js') }}
@stop