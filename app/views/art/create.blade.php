@extends('template/header')

@section('content')
	{{ Form::open(array('url' => 'art/create/action', 'method' => 'post', 'files' => true)) }}
        <input type="hidden" value="{{ csrf_token() }}">
        <div class="form_container">
            <label class="form_title ">ART DESIGN REQUEST FORM</label>
                <div class="row">
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NAME:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('employee_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REFERENCE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" />
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">EMPLOYEE NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('employeeid') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DATE FILED:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ date('Y-m-d') }}"/>
                        </div>
                    </div>
                    
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">COMPANY:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control" value="{{ Session::get('company') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">STATUS:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"   value="NEW" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DEPARTMENT:</label>
                        </div>
                        <div class="col2_form_container">
                            <input readonly="readonly" type="text" class="form-control"  value="{{ Session::get('dept_name') }}" />
                        </div>
                    </div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">CONTACT NUMBER:</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="20" type="text" class="form-control" name="contact" value="" />
                        </div>
                    </div>
		
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">SECTION:</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" class="form-control" name="section"  value="{{ Session::get('sect_name') }}" />
                        </div>
                    </div>

                    
                   <div class="clear_20"></div>
	              
                 </div>
                      <div class="container-header"><h5 class="text-center lined sub-header "><strong>REQUEST DETAILS</strong></h5></div>
                    <div class="row" >


                    <div class="clear_20"></div>

                   <div class="row3_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PROJECT NAME:</label>
                        </div>
                        <div class="row_form_container">
                            <input type="text" name="project" class="form-control" maxlength="100" />
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">DUE DATE:</label>
                        </div>
    

                        <div class=" input-group col2_form_container">
                            <input type="text" id="duedate" name="duedate"  class="form-control date_picker"/>
                            <label class="input-group-addon btn" for="duedate">
                               <span class="glyphicon glyphicon-calendar"></span>
                            </label>  
                        </div>

                    </div>
                               
                    <div class="clear_10"></div>
                   
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">REQUEST TYPE:</label>
                        </div>
                        <div class="col4_form_container">
                            <input type="radio" name="requestype" value="design"  /> DESIGN
                        </div>
                        <div class="col4_form_container">
                            <input type="radio"  name="requestype" value="mockup" /> MOCK UP
                        </div>
                    </div>

                               
                    <div class="clear_10"></div>

                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">SERVICE REQUESTED:</label>
                        </div>
                        <div class="col2_form_container">
                            <select name="servicerequested"  class="form-control" >
                                    <option value="" ></option>
                                    <option value="New Product Packaging Design" >New Product Packaging Design</option>
                                    <option value="Existing Product Packaging Design Enhancement/Improvement" >Existing Product Packaging Design Enhancement/Improvement</option>
                                    <option value="Promo & Merchandising Materials" >Promo & Merchandising Materials</option>
                                    <option value="Event Materials" >Event Materials</option>
                            </select>
                        </div>
                    </div>
                               
                    <div class="clear_10"></div>
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">PACKAGING/PROMO MATERIAL:</label>
                        </div>
                        <div class="col2_form_container">
                            <select name="packaging"  class="form-control" >
                                <option value="" ></option>
                                <option value="Primary Wrapper" >Primary Wrapper</option>
                                <option value="Secondary Wrapper" >Secondary Wrapper</option>
                                <option value="Carton/Case" >Carton/Case</option>
                                <option value="Gondola" >Gondola</option>  
                                <option value="Banner" >Banner</option>
                                <option value="Merchandising Materials" >Merchandising Materials</option>  
                                <option value="Others" >Others</option>                                  
                            </select>
                        </div>
                    </div>

                    <div class="row_form_container othermaterial hide">
                        <div class="col1_form_container text-right">
                            <label class="labels text-right required">OTHERS: &nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input type="text" name="othermaterial" class="form-control" maxlength="100"  />
                        </div>
                    </div>                    
                </div>
            


	        <div class="clear_20 materialspecification hide"></div>
		    <div class="  materialspecification hide">
                         <div class="container-header"><h5 class="text-center lined sub-header "><strong>MATERIAL SPECIFICATION</strong></h5></div>
                
            </div>

            <div class="clear_20 materialspecification hide"></div>
            
            <div style="float: left;width: 600px;margin-left: -12px;" class="materialspecification hide">                    
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels required">PRODUCT NAME:</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="100" type="text" class="form-control" name="productname" disabled />
                        </div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label class="labels" id="lenght">LENGTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" name="lenght" value="" disabled />
                        </div>
                    </div>					
			
                    <div class="clear_10"></div>
                    
                    <div class="">
                        <div class="col1_form_container">
                            <label class="labels" id="weight">NET WEIGHT:</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" name="weight" disabled />
                        </div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label class="labels" id="width" >WIDTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" name="width" value="" disabled />
                        </div>
                    </div>			   			
			
                    <div class="clear_10"></div>
                    
                    <div class="">
                        <div class="col1_form_container">&nbsp;</div>
                        <div class="col4_form_container"> &nbsp;</div>
                    </div>
                    <div class="col2_form_container">
                        <div class="col4_form_container  text-right">
                            <label class="labels" id="height" >HEIGHT:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col4_form_container">
                            <input maxlength="10" type="text" class="form-control" name="height" value="" disabled />
                        </div>
                    </div>						
		  </div>

          <div style="float: left;" class="materialspecification hide">                    
                    <div class="">
                        <div class="col5_form_container">
                            <label class="labels">OTHER DETAILS:</label>
                        </div>
                    </div>
            
                    <div class="clear_10"></div>

                    <div class="">
                        <div class="col5_form_container">
                            <textarea maxlength="5000" name="otherspecification" class="form-control" disabled ></textarea>
                        </div>
                    </div>

            </div>


            <div class="clear_20 designspecification hide"></div>
            <div class="  designspecification hide">
                       <div class="container-header"><h5 class="text-center lined sub-header "><strong>DESIGN SPECIFICATION</strong></h5></div>
                
            </div>  
            <div class="clear_20 designspecification hide"></div>

            <div style="float: left;width: 450px;margin-left: -12px;" class="designspecification hide">                    
                    <div class="row_form_container">
                        <div class="col1_form_container ">
                            <label class="labels required">LENGTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" name="lenght" value="" disabled />
                        </div>
                    </div>                  
            
                    <div class="clear_10"></div>
                    
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels required">WIDTH:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" name="width" value="" disabled />
                        </div>
                    </div>                      
            
                    <div class="clear_10"></div>
                    <div class="row_form_container">
                        <div class="col1_form_container">
                            <label class="labels">HEIGHT:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <input maxlength="10" type="text" class="form-control" name="height" value="" disabled />
                        </div>
                    </div>                      
          </div>

          <div style="float: left;" class="designspecification hide">                    
                   <div class="row_form_container">
                        <div class="col2_form_container  text-center">
                            <label class="labels">OTHER DETAILS:&nbsp;&nbsp;</label>
                        </div>
                        <div class="col2_form_container">
                            <textarea maxlength="5000" name="otherspecification" class="form-control" disabled ></textarea>
                        </div>
                    </div> 
            </div>


                    <div class="clear_20"></div>

                    <div class="row_form_container">
                        <div class="col2_form_container">
                            <label class="labels required">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BRIEF DESCRIPTION/ILLUSTRATIONS:</label>
                        </div>
                    </div>
                    
                    <div class="textarea_messages_container" style="border: 0;">
                        <div class="row">
                            <textarea style="max-height: 100px;" onkeyup="AutoGrowTextArea(this)" maxlength="5000" class="form-control" name="briefdesc"></textarea>
                        </div>
                    </div>

                
			
                <div class="clear_20"></div>
               <div class="container-header"><h5 class="text-center lined sub-header "><strong>ATTACHMENTS</strong></h5></div>
                
                <div class="clear_20"></div>
                
                <label class="attachment_note"><strong>ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                <div class="attachment_container">
                        <!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>              
        </div><!-- end of form_container -->

        <div class="clear_20"></div>

        <div class="form_container">
            <span class="legend-action">ACTION</span>
            <div class="textarea_messages_container">
                <div class="row">
                    <label class="textarea_inside_label">COMMENT:</label>
                    <textarea rows="3" class="form-control textarea_inside_width" name="comment">{{ Input::old("lrfComment") }}</textarea>
                </div>
            </div>

            <div class="clear_10"></div>
            <div class="row">
                <div class="comment_container">
                    <div class="comment_notes">
                        <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                    </div>
                </div>
                <div class="clear_10"></div>
                <div class="comment_container">
                    <div class="comment_notes">
                        @if(in_array(Session::get("desig_level") ,["head","president"]))
                        <label class="button_notes"><strong>SEND</strong> TO MKTG-ART FOR PROCESSING</label>
                        @else
                        <label class="button_notes"><strong>SEND</strong> TO DEPARTMENT HEAD FOR APPROVAL</label>
                        @endif
                    </div> 
                    <div class="comment_button">
                        <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                    </div>
                </div>
            </div>
        </div><!-- end of form_container -->

    {{ Form::close() }}
@stop
@section('js_ko')
{{ HTML::script('/assets/js/art/create.js?v2') }}
<script>
var max_filesize = 0;//mb
var max_upload = 5;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop