@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/acknowledge_action/$id") }}" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
           
                   <div class="container-header"><h5 class="text-center lined sub-header"><strong>TERMS AND CONDITIONS</strong></h5></div>
                   
                  <div class="clear_20"></div>
                     <div class="note">
                        <div class="attachment_note text-center" >
                            <p>My resignation/retirement/separation from the Company has been approved effective 
                                <span class="required text-danger">{{ $record['effective_date'] }}</span>, Should I have unclear accountabilities,
                                the value of which may be deducted from the amount due me from my resignation/retirement except for Cash Advances,
                                Revolving Fund, and Petty Cash which I am required to turnover to my Department Head as prerequisite to the
                                processing of this Clearance Certification.</p>
                        </div>
                    </div>
					
                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
	  
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 5 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' .  $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB"}}</a>
                        @if($attach->cca_department == "Resignee")<input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        <button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type="hidden" class="remove-fn" />@endif<br />
                        </p>
                        @endforeach 

                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>ACKNOWLEDGE</strong> THE TERMS AND CONDITION</label>
                        </div> 
                        <div class="comment_button" style="padding-left:150px;">
                            <button type="submit" class="btn btn-default btndefault" style="width:120px" name="action" value="acknowledge">ACKNOWLEDGE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>RETURN</strong> TO CHDR FOR REVISION</label>
                        </div> 
                        <div class="comment_button" style="padding-left:150px;">
                            <button type="submit" class="btn btn-default btndefault"  style="width:120px" name="action" value="return">RETURN</button>
                        </div>
                    </div>
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
{{ HTML::script('/assets/js/cc/resignee.js') }}
<script>
var max_filesize = 20;//mb
var max_upload = 5;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop