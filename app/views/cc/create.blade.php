@extends('template/header')

@section('content')
        <select class="form-control hidden dept-cmb" id="dept-cmb">
                <option value=""></option>
                @foreach($department as $key => $val)                                      
                <option value="{{ $key }}">{{ $val }}</option>
                @endforeach
        </select>

        <form class="form-inline" method="post" action="{{ url('cc/create_action') }}" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                <label class="form_label pm_form_title ">CLEARANCE CERTIFICATION</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <select class="employee form-control" id="employee" name="employee" >
                                        <option value=""></option>
                                        @foreach($employees as $key => $val)                                      
                                        <option value="{{ $key }}">{{ $val }}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DIVISION HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="div_head" value="" />
                                <input readonly="readonly" type="hidden" class="form-control" name="div_head_id" value="" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="emp_no" value="" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="dept_head"/>
                                <input readonly="readonly" type="hidden" class="form-control" name="dept_head_id"/>
                            </div>
                        </div>
						<div class="clear_10"></div>
						<div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DIVISION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" name="division" value="" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">IMMEDIATE HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="" name="supervisor"/>
                                <input readonly="readonly" type="hidden" class="form-control" value="" name="supervisor_id"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="" name="department"/>
                                <input readonly="readonly" type="hidden" class="form-control" value="" name="department_id"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">DATE HIRED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control " name="date_hired"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="hidden" class="form-control" value="" name="section" />
                                <input readonly="readonly" type="text" class="form-control" value="" name="section_name" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">EFFECTIVE DATE OF RESIGNATION:</label>
                            </div>
                       

                            <div class=" input-group col2_form_container">
                                <input type="text" class="date_picker form-control"   name="effective_date" id="effective_date" />
                                <label class="input-group-addon btn" for="effective_date">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>  
                            </div>

                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">POSITION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="" name="position" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels required">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
								<input readonly="readonly" type="text" class="form-control" name="ref_num"/>
                            </div>
                        </div>
                        <div class="clear_10"></div>
                    </div>

                    <div class="clear_20"></div>
                  
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>FORM ACCOMPLISHMENT</strong></h5></div>
                   
                    <div class="clear_10"></div>
					
                    <div class="note">
                        <label class="attachment_note">
                            <strong>OTHER DEPARTMENTS/UNITS THAT NEED TO REVIEW RESIGNING EMPLOYEE'S CLEARANCE</strong>
                            <p><strong>(Aside from <span id="aside" >Resigning Employees Department</span>, CITD, SMDD, ERPD, SSRV, & CHRD)</strong></p>
                        </label>
                    </div>

                   

                    <div class="other_dept">

                    </div>

                    <div class="clear_20"></div>
                    <button type="button" class="btn btn-success add-dept">ADD DEPARTMENT</button><br/>
                        
                    <div class="clear_20"></div> 
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
	  
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        <!--<button type="button" class="btn btnbrowse btn-success" id="btnCbrAddAttachment">BROWSE</button><br/>-->
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <span class="legend-action">ACTION</span>
                <div class="textarea_messages_container">
                    <div class="row">
                        <label class="textarea_inside_label">COMMENT:</label>
                        <textarea rows="3" class="form-control textarea_inside_width" name="comment"></textarea>
                    </div>
                </div>

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO RESIGNEE FOR ACKNOWLEDGEMENT</label>
                        </div> 
                        <div class="comment_button">
                            <button type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<link href="public/assets/css/chosen.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/js/chosen.jquery.min.js" type="text/javascript"></script>
{{ HTML::script('/assets/js/cc/create.js?v1') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop