@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/department/head/action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>EMPLOYEE REQUIREMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center" width="300" ><span class='labels2'>EMPLOYEE REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>

                    @foreach($record['ccd_requirements'] as $key=>$rs)
                    <tr>
                        <td valign="top" ><input {{ $readonly }} readonly class="" type="text" name="employeeRequirement[{{ $key }}]" value="{{ $rs->employee }}" /></td>
                        <td valign="top"  align="center" class="labels2" ><input  {{ $readonly }} type="radio" value="yes" name="availability[{{$key}}]" /></td>
                        <td valign="top"  align="center" class="labels2" ><input  {{ $readonly }} type="radio" value="no" name="availability[{{$key}}]"  /></td>
                        <td valign="top"  align="center" class="labels2" ><input  {{ $readonly }} type="radio" value="not" name="availability[{{$key}}]"  /></td>
                        <td valign="top"  align="center" class="labels2" ><input  {{ $readonly }} class="date_picker" type="text" name="date[{{ $key }}]"  /></td>
                        <td valign="top"  align="center" class="labels2" ><input  {{ $readonly }} type="text" name="amount[{{ $key }}]" class="isDecimal" /></td>
                        <td valign="top"  align="center" class="labels2" >
                            <input  {{ $readonly }} type="text" name="remarks[{{ $key }}]" />
                            <input readonly {{ $readonly }} readonly="" type="{{ $rs->chrd_remarks ? "text" : "hidden" }}" name="other_remarks[{{ $key }}]" value="{{ $rs->chrd_remarks }}" />
                        </td>
                    </tr>
                    @endforeach
                  
                    <tr>
                        <td>
                            <p class="labels2" >OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                    </tr>

                    <tr>
                        <td class="labels2">
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text"  class=" requirement_txt" name="requirement[0]" />
                        </td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} type="radio" value="yes" name="requirement_app[0]" /></td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} type="radio" value="no" name="requirement_app[0]" /></td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} type="radio" value="not" name="requirement_app[0]"  /></td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} class="date_picker" type="text" name="requirement_date[0]" /></td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} type="text" name="requirement_amount[0]" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input   {{ $readonly }} type="text" name="requirement_remarks[0]" /></td>
                    </tr>


                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        @if(!$readonly)
                        @if($attach->cca_eid == Session::get("employee_id") && $attach->cca_department == Session::get("dept_id") )<button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type='hidden' class='remove-fn' />@endif
                        @endif
                        <br />
                        </p>
                        @endforeach 

                        @if(!$readonly)
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif                   
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>ASSIGN</strong> FOR PROCESSING TO</label>
                            <select {{ $receiver->isEmpty() ? "disabled" :""  }} name="assign" {{ $readonly }} >
                                <option></option>
                                @foreach($receiver as $rs)
                                <option value="{{ $rs->id }}">{{ $rs->firstname . " " . $rs->lastname }}</option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="comment_button">
                            <button {{ $receiver->isEmpty() ? "disabled" :""  }} {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="assign">ASSIGN</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    @include('cc/template/send')
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/department/head.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop