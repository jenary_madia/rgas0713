@extends('template/header')

@section('content')

<div id="wrap">
    <!-- @include('template/sidebar') -->
    <div class="wrapper">
    <input type="hidden" value="{{ csrf_token() }}">
 
    <div class="">   


         <div class="row4_form_container">
            <div class="col4_form_container">
                <label class="labels text-danger">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                 <select class="department1 form-control" name="" >
                    <option value="">All</option>
                    <?php $arr_dept = array() ;?>
                    @foreach($record as $rs)
                        @if(!in_array($rs[2],$arr_dept))
                           <?php  $arr_dept[] = $rs[2] ; ?>
                        @endif
                    @endforeach
                    <?php asort($arr_dept);?>
                    @foreach($arr_dept as $val)
                    <option value="{{ $val }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>
           
 
    
        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <span class="list-title" role="columnheader" rowspan="1" colspan="5" >CLEARANCE CERTIFICATION </span>
            <table id="myrecord" border="0" cellspacing="0" class="display dataTable tbl_fonts" width="100%"  >
                <thead>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >REFERENCE NUMBER &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">RESIGNEE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" width="150">DEPARTMENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >EFFECTIVE DATE OF RESIGNATION &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1"  >ACTION</th>
                    </tr>                           
                </thead>

                <tbody>
                    
                    @foreach($record as $rs)
                    <tr>
                    <td>{{ $rs[0] }}</td>  
                    <td>{{ $rs[1] }}</td>  
                    <td>{{ $rs[2] }}</td>  
                    <td>{{ $rs[3] }}</td>  
                    <td>{{ $rs[4] }}</td>  
                    <td>{{ $rs[5] }}</td>   
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>  
        </div>

    </div>

</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="width:100%;" >
    <div class="modal-content" >
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Clearance Logs</h4>
      </div>
      <div class="modal-body">
        <div class="row_form_container">
            <div class="col1_form_container">
                <label class="labels required">DEPARTMENT:</label>
            </div>
            <div class="col2_form_container">
                <select class="department2 form-control" name="" >
                    <option value="">All</option>
                    @foreach($department as $key => $val)                                      
                    <option value="{{ $key }}">{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

    
        <div class="clear_10"></div>   

        <div class="clear_10"></div>    
    
        <div class="datatable_holder">
            <table id="myrecordlogs" border="0" cellspacing="0" class="display dataTable">
                <thead>
                    <tr role="row">
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >DATE RECEIVED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">AGE DAYS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">STATUS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >ASSIGNED TO</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DEPARTMENT</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1" >DETAIL/REMARKS</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">DATE COMPLETED</th>
                        <th  class="sorting_disabled td_center" role="columnheader" rowspan="1" colspan="1">TIME</th>
                    </tr>                           
                </thead>

                <tbody>
                    
              
                    
                </tbody>
            </table>  
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@stop
@section('js_ko')
{{ HTML::script('/assets/js/cc/report.js?v1') }}
@stop