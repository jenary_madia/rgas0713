@extends('template/header')

@section('content')
        <form class="form-inline" method="post" enctype="multipart/form-data" action="{{ url("cc/csdh/action/$id") }}" >
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record' , ["view"=>1])

                    <div class="clear_20"></div>
                    <label class="form_label pm_form_title">EMPLOYEE REQUIREMENTS</label>
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center" width="300" ><span class='labels2'>EMPLOYEE REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center" width="80"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED BY</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED DATE</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2">EMPLOYEES DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>MANUALS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio"  value="yes" name="manual_surrender" {{ $record['manual']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="manual_surrender" {{ $record['manual']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="manual_surrender"  {{ $record['manual']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['manual']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['manual']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>WORK FILES SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="work_files" {{ $record['workfiles']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="work_files" {{ $record['workfiles']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="work_files"  {{ $record['workfiles']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['workfiles']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['workfiles']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>DRAWER/CABINET KEYS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="drawer" {{ $record['drawer']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="drawer" {{ $record['drawer']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="drawer"   {{ $record['drawer']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['drawer']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['drawer']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>FIXED ASSETS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="assets" {{ $record['assets']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="assets" {{ $record['assets']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="assets" {{ $record['assets']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['assets']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['assets']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>OFFICE SUPPLIES SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="supplies" {{ $record['supplies']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="supplies" {{ $record['supplies']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="supplies" {{ $record['supplies']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['supplies']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['supplies']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>ACCOUNTABLE FORMS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="accountable" {{ $record['accountable']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="accountable" {{ $record['accountable']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="accountable" {{ $record['accountable']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['accountable']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['accountable']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOLVING FUND/PETTY CASH SURRENDERED 
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#revolving" ></i>
                            </span>
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="revolving" {{ $record['revolving']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="revolving" {{ $record['revolving']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="revolving" {{ $record['revolving']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['revolving']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['revolving']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>COMPANY CAR SURRENDERED
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#surrender" ></i>
                            </span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="car" {{ $record['car']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="car" {{ $record['car']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="car" {{ $record['car']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['car']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['car']->remarks }}</td>
                    </tr>

                    @if(!empty($record['resignee_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['resignee_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2">INFORMATION TECHNOLOGY DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR INTERNET</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="internet" {{ $record['internet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="internet" {{ $record['internet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="internet" {{ $record['internet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['internet']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['internet']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['internet']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR INTRANET</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="intranet" {{ $record['intranet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="intranet" {{ $record['intranet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="intranet" {{ $record['intranet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['intranet']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['intranet']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['intranet']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR INTERNAL EMAIL</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="internal" {{ $record['internal']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="internal" {{ $record['internal']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="internal" {{ $record['internal']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['internal']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['internal']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['internal']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR EXTERNAL EMAIL</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="external" {{ $record['external']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="external" {{ $record['external']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="external" {{ $record['external']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['external']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['external']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['external']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REMOVED FROM SMS BULK MESSAGING</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="sms" {{ $record['sms']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="sms" {{ $record['sms']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="sms" {{ $record['sms']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sms']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['sms']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['sms']->remarks }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>PC/LAPTOP IN GOOD WORKING CONDITION</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="laptop" {{ $record['laptop']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="laptop" {{ $record['laptop']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="laptop" {{ $record['laptop']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['laptop']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['laptop']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['laptop']->remarks }}</td>
                    </tr>

                    @if(!empty($record['citd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['citd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['citd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2">SYSTEM MANAGEMENT DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR RGAS</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="rgas" {{ $record['rgas']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="rgas" {{ $record['rgas']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="rgas" {{ $record['rgas']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['rgas']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['smd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['smd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['rgas']->remarks }}</td>
                    </tr> 

                    @if(!empty($record['smdd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['smdd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['smd_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['smd_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2">ERP DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOKED ACCESSED FOR SAP</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="sap" {{ $record['sap']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="sap" {{ $record['sap']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="sap" {{ $record['sap']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sap']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['erp_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['erp_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['sap']->remarks }}</td>
                    </tr>  

                    @if(!empty($record['erpd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['erpd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['erp_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['erp_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2">SALES SERVICE DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>UNPAID OFFICE SALES/VALE</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="vale" {{ $record['vale']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="vale" {{ $record['vale']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="vale" {{ $record['vale']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['vale']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['vale']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['ssrv_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['ssrv_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['vale']->remarks }}</td>
                    </tr>   

                    @if(!empty($record['ssrv_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['ssrv_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['ssrv_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['ssrv_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2" >HUMAN RESOURCES DEPARTMENT</th>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2">EMPLOYEE RELATIONS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>CONDUCTED EXIT INTERVIEW</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="exit" {{ $record['exit']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="exit" {{ $record['exit']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="exit" {{ $record['exit']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['exit']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['exit']->remarks }}</td>
                    </tr> 

                    @if(!empty($record['training1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['training1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">PAYROLL BENEFITS & ADMINISTRATION</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>HMO CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo_surrender']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['hmo_surrender']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>HMO CANCELLATION NOTICE</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo_cancel']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['hmo_cancel']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>ID CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="card" {{ $record['card']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="card" {{ $record['card']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="card" {{ $record['card']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['card']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['card']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>BUILDING ACCESS CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="building" {{ $record['building']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="building" {{ $record['building']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="building" {{ $record['building']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['building']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['building']->remarks }}</td>
                    </tr>          

                     @if(!empty($record['cb_personnel1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>                        
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['cb_personnel1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td><span class='required labels2'>ACCOUNT IN BIOMETRICS DELETED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="biometrics" {{ $record['biometrics']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="biometrics" {{ $record['biometrics']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="biometrics" {{ $record['biometrics']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['biometrics']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['recruitment_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['recruitment_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['biometrics']->remarks }}</td>
                    </tr>     

                    @if(!empty($record['recruitment_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['recruitment_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['recruitment_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['recruitment_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">TRAINING</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>SERVICE AGREEMENT FULFILLED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="service" {{ $record['service']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="service" {{ $record['service']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="service" {{ $record['service']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['service']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['service']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>TRAINING MATERIALS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="material" {{ $record['material']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="material" {{ $record['material']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="material" {{ $record['material']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['material']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['material']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>TRAINING EQUIPMENT SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="equipment" {{ $record['equipment']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="equipment" {{ $record['equipment']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="equipment" {{ $record['equipment']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['equipment']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['equipment']->remarks }}</td>
                    </tr>      

                    @if(!empty($record['training2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['training2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">LOANS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING SSS LOAN</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="sss" {{ $record['sss']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="sss" {{ $record['sss']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="sss" {{ $record['sss']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sss']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['sss']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['sss']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING AUB LOAN</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="aub" {{ $record['aub']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="aub" {{ $record['aub']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="aub" {{ $record['aub']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['aub']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['aub']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['aub']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="hmo" {{ $record['hmo']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="hmo" {{ $record['hmo']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="hmo" {{ $record['hmo']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['hmo']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['hmo']->remarks }}</td>
                    </tr>       

                    @if(!empty($record['cb_personnel2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['cb_personnel2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->amount }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">FOR RESIGNING SUPERVISOR AND UP ONLY</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>STAFF'S EMPLOYEE RECORDS(201 FILE) & PERFORMANCE
                        EVALUATION TURNED-OVER TO HRD</span></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="staff" {{ $record['staff']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="staff" {{ $record['staff']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="staff" {{ $record['staff']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['staff']->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['manager_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['manager_date'] }}</td>
                        <td align="center" class="labels3" >{{ $record['staff']->remarks }}</td>
                    </tr> 

                    @if(!empty($record['cb_manager_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    
                    @foreach($record['cb_manager_others'] as $key=>$rs)
                    <tr>
                        <td class="labels3" >{{ $rs->requirement }}</td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['manager_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['manager_date'] }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                    @if(!empty($other_dept))
                    @foreach($other_dept as $field)
                        <tr>
                            <th colspan="9" class="text-center labels2">{{ $field->dept_name }}</th>
                        </tr>
                        <?php $req = json_decode($field->ccd_requirements);?>
                         @foreach($req as $key=>$rs)
                        <tr>
                            <td>{{ $rs->employee }}</td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" >{{ $rs->date }}</td>
                            <td align="center" class="labels3" >{{ $rs->amount }}</td>
                            <td align="center" class="labels3" >{{ $field->firstname . " " . $field->lastname }}</td>
                            <td align="center" class="labels3" >{{ $field->ccd_date }}</td>  
                            <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                        </tr>
                        @endforeach

                         <?php $oth = json_decode($field->ccd_others);?>
                         @if(!empty( $oth ))
                        <tr>
                            <td>
                                <p class="labels2">OTHERS</p>
                            </td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                        </tr>

                       
                        @foreach($oth as $key=>$rs)
                        <tr>
                            <td>
                                {{ $rs->requirement }}
                            </td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input {{ $readonly }} type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" >{{ $rs->date }}</td>
                            <td align="center" class="labels3" >{{ $rs->amount }}</td>                        
                            <td align="center" class="labels3" >{{ $field->firstname . " " . $field->lastname }}</td>
                            <td align="center" class="labels3" >{{ $field->ccd_date }}</td>   
                            <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                        </tr>
                        @endforeach
                        @endif
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    @endif
                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
      
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        </p>
                        @endforeach       
                    </div>
            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">                 
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"> </label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} {{ $record['overall_status'] == 1   ? "" : "disabled" }} type="button" class="btn btn-default btndefault" name="action" value="send" onClick="window.location='{{ url("cc/print/$id") }}'" >PRINT</button>
                        </div>
                    </div>
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>




@include('cc/template/modal')
@stop