@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/citd/staff/action/$id") }}" action="{{ url("cc/technical/action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>SYSTEM ACCESS</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center"><span class='labels2'>SYSTEM ACCESS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>


                    <tr>
                        <td><span class='required labels2 '>REVOKED ACCESSED FOR INTERNET</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="internet" {{ $record['internet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="internet" {{ $record['internet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="internet" {{ $record['internet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['internet']->date }}" name="internet_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['internet']->amount }}" name="internet_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['internet']->remarks }}" name="internet_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2 '>REVOKED ACCESSED FOR INTRANET</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="intranet" {{ $record['intranet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="intranet" {{ $record['intranet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="intranet" {{ $record['intranet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['intranet']->date }}" name="intranet_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['intranet']->amount }}" name="intranet_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['intranet']->remarks }}" name="intranet_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2 '>REVOKED ACCESSED FOR INTERNAL EMAIL</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="internal" {{ $record['internal']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="internal" {{ $record['internal']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="internal" {{ $record['internal']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['internal']->date }}" name="internal_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['internal']->amount }}" name="internal_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['internal']->remarks }}" name="internal_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2 '>REVOKED ACCESSED FOR EXTERNAL EMAIL</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="external" {{ $record['external']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="external" {{ $record['external']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="external" {{ $record['external']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['external']->date }}" name="external_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['external']->amount }}" name="external_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['external']->remarks }}" name="external_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2 '>REMOVED FROM SMS BULK MESSAGING</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="sms" {{ $record['sms']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="sms" {{ $record['sms']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="sms" {{ $record['sms']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['sms']->date }}" name="sms_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['sms']->amount }}" name="sms_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['sms']->remarks }}" name="sms_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2 '>PC/LAPTOP IN GOOD WORKING CONDITION</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="laptop" {{ $record['laptop']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="laptop" {{ $record['laptop']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="laptop" {{ $record['laptop']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['laptop']->date }}" name="laptop_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['laptop']->amount }}" name="laptop_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['laptop']->remarks }}" name="laptop_remarks" /></td>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2" >OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                    </tr>

                    @if(!empty($record['citd_others']))
                    @foreach($record['citd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" value="{{ $rs->requirement }}" name="requirement[{{$key}}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $rs->date }}" name="requirement_date[{{$key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $rs->amount }}" name="requirement_amount[{{$key}}]" class="isDecimal"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $rs->remarks }}" name="requirement_remarks[{{$key}}]" /></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" class="requirement_txt" name="requirement[{{ $key = 0 }}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{ $key}}]"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" name="requirement_date[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" name="requirement_amount[{{ $key}}]" class="isDecimal"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" name="requirement_remarks[{{ $key}}]" /></td>
                    </tr>
                    @endif

                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        @if(!$readonly)
                        @if($attach->cca_eid == Session::get("employee_id") && $attach->cca_department == "CITD" )<button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type='hidden' class='remove-fn' />@endif
                        @endif
                        <br />
                        </p>
                        @endforeach 

                        @if(!$readonly)
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif 
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    @include('cc/template/save')
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO TECHNICAL HEAD FOR APPROVAL</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
                @include('cc/template/back')
                 
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/citd/staff.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop