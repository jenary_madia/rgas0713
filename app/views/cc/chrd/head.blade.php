@extends('template/header')

@section('content')
        <form class="form-inline" method="post" enctype="multipart/form-data" action="{{ url("cc/chrd/action/$id") }}" >
            <input {{ $readonly }} class="cc"readonly  type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>EMPLOYEE REQUIREMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    @if($record['resignee_head'] == 3 && $record['dept_head'] == Session::get("employee_id"))
                    <input {{ $readonly }} class="cc"readonly  type="hidden" name="resignee_head" value="1" />
                    <tr>
                        <th class="text-center"><span class='labels2'>DELIVERABLES</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED BY</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED DATE</span></th>                        
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>MANUALS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled   value="yes" name="manual_surrender" {{ $record['manual']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="manual_surrender" {{ $record['manual']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="manual_surrender"  {{ $record['manual']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['manual']->date }}" name="manual_surrender_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['manual']->remarks }}" name="manual_surrender_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>WORK FILES SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="work_files" {{ $record['workfiles']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="work_files" {{ $record['workfiles']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="work_files"  {{ $record['workfiles']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['workfiles']->date }}" name="work_files_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['workfiles']->remarks }}" name="work_files_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>DRAWER/CABINET KEYS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="drawer" {{ $record['drawer']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="drawer" {{ $record['drawer']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="drawer"   {{ $record['drawer']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['drawer']->date }}" name="drawer_date"  /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['drawer']->remarks }}" name="drawer_remarks"  /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>FIXED ASSETS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="assets" {{ $record['assets']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="assets" {{ $record['assets']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="assets" {{ $record['assets']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['assets']->date }}" name="assets_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['assets']->remarks }}" name="assets_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>OFFICE SUPPLIES SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="supplies" {{ $record['supplies']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="supplies" {{ $record['supplies']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="supplies" {{ $record['supplies']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['supplies']->date }}" name="supplies_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['supplies']->remarks }}" name="supplies_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>ACCOUNTABLE FORMS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="accountable" {{ $record['accountable']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="accountable" {{ $record['accountable']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="accountable" {{ $record['accountable']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['accountable']->date }}" name="accountable_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['accountable']->remarks }}" name="accountable_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOLVING FUND/PETTY CASH SURRENDERED 
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#revolving" ></i>
                            </span>
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="revolving" {{ $record['revolving']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="revolving" {{ $record['revolving']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="revolving" {{ $record['revolving']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['revolving']->date }}" name="revolving_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['revolving']->remarks }}" name="revolving_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>COMPANY CAR SURRENDERED
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#surrender" ></i>
                            </span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="car" {{ $record['car']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="car" {{ $record['car']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="car" {{ $record['car']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $record['car']->date }}" name="car_date" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $record['car']->remarks }}" name="car_remarks" /></td>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2" >OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                    </tr>

                    @if(!empty($record['resignee_others']))
                    @foreach($record['resignee_others'] as $key=>$rs)
                    <tr>
                        <td>
                            <input {{ $readonly }} class="cc"readonly  class=""  type="text" value="{{ $rs->requirement }}" name="requirement[{{$key}}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="radio" disabled  value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="date" value="{{ $rs->date }}" name="requirement_date[{{$key}}]" /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" >{{ $record['resignee_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['resignee_date'] }}</td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="cc"readonly  type="text" value="{{ $rs->remarks }}" name="requirement_remarks[{{$key}}]" /></td>
                    </tr>
                    @endforeach
                    @endif
                    @endif

                    <tr>
                        <th class="text-center" width="300" ><span class='labels2'>EMPLOYEE REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center" width="80"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED BY</span></th>
                        <th class="text-center"><span class='labels2'>ACCOMPLISHED DATE</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2">EMPLOYEE RELATIONS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>CONDUCTED EXIT INTERVIEW</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="exit" {{ $record['exit']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="exit" {{ $record['exit']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="exit" {{ $record['exit']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['exit']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training1_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['exit']->remarks }}</td>
                    </tr> 

                    @if(!empty($record['training1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>    
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['training1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training1_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}   {{ $record['training1'] == 0 ? "disabled" : "" }} type="button" by="{{ $record['training1_by'] }}" value="RETURN" class="return" id="training1" /></td>
                    </tr>










                    <tr>
                        <td>
                            <p class="labels2">PAYROLL BENEFITS & ADMINISTRATION</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>HMO CARD SURRENDERED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['hmo_surrender']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['hmo_surrender']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>HMO CANCELLATION NOTICE</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['hmo_cancel']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['hmo_cancel']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>ID CARD SURRENDERED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="card" {{ $record['card']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="card" {{ $record['card']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="card" {{ $record['card']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['card']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['card']->remarks }}</td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>BUILDING ACCESS CARD SURRENDERED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="building" {{ $record['building']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="building" {{ $record['building']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="building" {{ $record['building']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['building']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['building']->remarks }}</td>
                    </tr>          

                     @if(!empty($record['cb_personnel1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>                        
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['cb_personnel1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['personnel1_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel1_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}   {{ $record['personnel1'] == 0 ? "disabled" : "" }} by="{{ $record['personnel1_by'] }}" type="button" value="RETURN" class="return" id="personnel1" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>ACCOUNT IN BIOMETRICS DELETED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="biometrics" {{ $record['biometrics']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="biometrics" {{ $record['biometrics']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="biometrics" {{ $record['biometrics']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['biometrics']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['recruitment_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['recruitment_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['biometrics']->remarks }}</td>
                    </tr>     

                    @if(!empty($record['recruitment_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['recruitment_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['recruitment_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['recruitment_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}   {{ $record['recruitment'] == 0 ? "disabled" : "" }} by="{{ $record['recruitment_by'] }}" type="button" value="RETURN" class="return" id="recruitment"/></td>
                    </tr>










                    <tr>
                        <td>
                            <p class="labels2">TRAINING</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>SERVICE AGREEMENT FULFILLED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="service" {{ $record['service']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="service" {{ $record['service']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="service" {{ $record['service']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['service']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['service']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>TRAINING MATERIALS SURRENDERED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="material" {{ $record['material']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="material" {{ $record['material']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="material" {{ $record['material']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['material']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['material']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>TRAINING EQUIPMENT SURRENDERED</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="equipment" {{ $record['equipment']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="equipment" {{ $record['equipment']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="equipment" {{ $record['equipment']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['equipment']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['equipment']->remarks }}</td>
                    </tr>      

                    @if(!empty($record['training2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['training2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['training2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['training2_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}  {{ $record['training2'] == 0 ? "disabled" : "" }} by="{{ $record['training2_by'] }}" type="button" value="RETURN" class="return" id="training2" /></td>
                    </tr>








                    <tr>
                        <td>
                            <p class="labels2">LOANS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING SSS LOAN</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="sss" {{ $record['sss']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="sss" {{ $record['sss']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="sss" {{ $record['sss']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['sss']->date }}</td>
                        <td align="center" class="labels3">{{ $record['sss']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['sss']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING AUB LOAN</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="aub" {{ $record['aub']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="aub" {{ $record['aub']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="aub" {{ $record['aub']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['aub']->date }}</td>
                        <td align="center" class="labels3">{{ $record['aub']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['aub']->remarks }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels2'>WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="hmo" {{ $record['hmo']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="hmo" {{ $record['hmo']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="hmo" {{ $record['hmo']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['hmo']->date }}</td>
                        <td align="center" class="labels3">{{ $record['hmo']->amount }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['hmo']->remarks }}</td>
                    </tr>       

                    @if(!empty($record['cb_personnel2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['cb_personnel2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['personnel2_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->amount }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}   {{ $record['personnel2'] == 0 ? "disabled" : "" }} by="{{ $record['personnel2_by'] }}" type="button" value="RETURN" class="return" id="personnel2" /></td>
                    </tr>


                    <tr>
                        <td>
                            <p class="labels2">FOR RESIGNING SUPERVISOR AND UP ONLY</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>STAFF'S EMPLOYEE RECORDS(201 FILE & PERFORMANCE
                        EVALUATION TURNED-OVER TO HRD</span></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="staff" {{ $record['staff']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="staff" {{ $record['staff']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="staff" {{ $record['staff']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $record['staff']->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['manager_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['manager_date'] }}</td>
                        <td align="center" class="labels3">{{ $record['staff']->remarks }}</td>
                    </tr> 

                    @if(!empty($record['cb_manager_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3"></td>
                    </tr>

                    
                    @foreach($record['cb_manager_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3"><input {{ $readonly }} class="cc"readonly  type="radio" disabled value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3">{{ $rs->date }}</td>
                        <td align="center" class="labels3"></td>
                        <td align="center" class="labels3" >{{ $record['manager_by'] }}</td>
                        <td align="center" class="labels3" >{{ $record['manager_date'] }}</td>
                        <td align="center" class="labels3">{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td align="center" class="labels3"><input {{ $readonly }}   {{ $record['manager'] == 0 ? "disabled" : "" }} by="{{ $record['manager_by'] }}" type="button" value="RETURN" class="return" id="manager" /></td>
                    </tr>

                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        </p>
                        @endforeach       
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">                 
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO CORPORATE SERVICES DIVISON HEAD FOR REVIEW</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} {{ in_array(0,array($record['personnel1'],$record['personnel2'] ,$record['training1'] ,$record['training2'] ,$record['manager'],$record['recruitment']) ) ? "disabled" : "" }} type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>


<!-- Modal -->
<div class="modal fade" id="returnModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form class="form-inline" method="post" action="{{ url("cc/chrd/action/$id") }}" >
      <div class="modal-body">
        REASON FOR RETURN TO <span id="department_name"></span>
            <div class="text-center">
                <input {{ $readonly }} class="cc"readonly  type="hidden" name="personnel" />
                <textarea cols="70" rows="7" name="remarks"></textarea>
                <input name="name" readonly="readonly" type="hidden" class="form-control"  value="{{ $record['emp_name'] }}" />
                <input name="department" readonly="readonly" type="hidden" class="form-control" value="{{ $record['department'] }}" name="department"/>
                <input type="hidden"  class="form-control" name="effective_date" value="{{ date("m/d/Y",strtotime($record['effective_date'])) }}" />
                <input name="reference" readonly="readonly" type="hidden" class="form-control" value="{{ $record['ref_num'] }}" /> 
            </div>
            <div class="clear_20"></div>
            <div class="row">
              <div class="col-md-6"><b>RETURN</b> TO CONCERNED PERSONNEL</div>
              <div class="col-md-6 text-right"><button {{ $readonly }} type="submit" name="action" value="RETURN" >RETURN</button></div>
            </div>
            <div class="clear_20"></div>
            <div class="text-center">
                <button {{ $readonly }} type="button" class="" data-dismiss="modal">BACK</button>
            </div>
      </div>
      </form>
    </div>
  </div>
</div>


@include('cc/template/modal')
@stop
@section('js_ko')
{{ HTML::script('/assets/js/cc/chrd/head.js') }}
@stop