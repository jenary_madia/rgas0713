@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/chrd/cbr/personnel1/action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                   
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>EMPLOYEE REQUIREMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center"><span class='labels2'>EMPLOYEE REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2">PAYROLL BENEFITS & ADMINISTRATION</p>
                        </td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>HMO CARD SURRENDERED</span></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="yes" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="no" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="not" name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" value="{{ $record['hmo_surrender']->date }}" name="hmo_surrender_date" /></td>
                        <td align="center"><input {{ $readonly }} type="text" value="{{ $record['hmo_surrender']->remarks }}" name="hmo_surrender_remarks" /></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>HMO CANCELLATION NOTICE</span></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="yes" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="no" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="not" name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" value="{{ $record['hmo_cancel']->date }}" name="hmo_cancel_date" /></td>
                        <td align="center"><input {{ $readonly }} type="text" value="{{ $record['hmo_cancel']->remarks }}" name="hmo_cancel_remarks" /></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>ID CARD SURRENDERED</span></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="yes" name="card" {{ $record['card']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="no" name="card" {{ $record['card']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="not" name="card" {{ $record['card']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" value="{{ $record['card']->date }}" name="card_date" /></td>
                        <td align="center"><input {{ $readonly }} type="text" value="{{ $record['card']->remarks }}" name="card_remarks" /></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels2'>BUILDING ACCESS CARD SURRENDERED</span></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="yes" name="building" {{ $record['building']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="no" name="building" {{ $record['building']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} type="radio" value="not" name="building" {{ $record['building']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" value="{{ $record['building']->date }}" name="building_date" /></td>
                        <td align="center"><input {{ $readonly }} type="text" value="{{ $record['building']->remarks }}" name="building_remarks" /></td>
                    </tr>                

                    <tr>
                        <td>
                            <p class="labels2">OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                        <td align="center"></td>
                    </tr>

                    @if(!empty($record['cb_personnel1_others']))
                    @foreach($record['cb_personnel1_others'] as $key=>$rs)
                    <tr>
                        <td class="labels2">
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" value="{{ $rs->requirement }}" name="requirement[{{$key}}]" />
                        </td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" value="{{ $rs->date }}" name="requirement_date[{{$key}}]" /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="text" value="{{ $rs->remarks }}" name="requirement_remarks[{{$key}}]" /></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td class="labels2">
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" class="requirement_txt" name="requirement[{{ $key = 0 }}]" />
                        </td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{ $key}}]" /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{ $key}}]" /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{ $key}}]"  /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} class="date_picker" class="date_picker" type="text" name="requirement_date[{{ $key}}]" /></td>
                        <td class="labels2" align="center"><input {{ $readonly }} type="text" name="requirement_remarks[{{ $key}}]" /></td>
                    </tr>
                    @endif

                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        @if(!$readonly)
                        @if($attach->cca_eid == Session::get("employee_id") && $attach->cca_department == "Personnel1" )<button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type='hidden' class='remove-fn' />@endif
                        @endif
                        <br />
                        </p>
                        @endforeach 

                        @if(!$readonly)
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif 
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    @include('cc/template/save')
                    @include('cc/template/send_head')
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/chrd/compensation1.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop