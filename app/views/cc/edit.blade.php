@extends('template/header')

@section('content')
        <select class="form-control hidden dept-cmb" id="dept-cmb">
            <option value=""></option>
            @foreach($department as $key => $val)                                      
            <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
         </select>

        <form class="form-inline" method="post" action="{{ url("cc/edit_action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                  <div class="container-header"><h5 class="text-center lined sub-header"><strong>FORM ACCOMPLISHMENT</strong></h5></div>
                   
                    <div class="clear_10"></div>
					
                    <div class="note">
                        <label class="attachment_note">
                            <strong>OTHER DEPARTMENTS/UNITS THAT NEED TO REVIEW RESIGNING EMPLOYEE'S CLEARANCE</strong>
                            <p><strong>(Aside from {{ $record['department'] }}, CITD, SMDD, ERPD, SSRV, & CHRD)</strong></p>
                        </label>
                    </div>

                   

                    <div class="other_dept">
                        @foreach($other_department as $key => $val)  
                             
                            <div class="clear_20"></div>
                                <div class="row ">
                                    <div class="col-md-8">
                                        <div class="col1_form_container">
                                            <label class="labels">Department Name:</label>
                                        </div>
                                        <div class="col2_form_container">
                                            <select {{ $readonly }} class="form-control dept-cmb newdept" name="other_dept[]">
                                                    <option value=""></option>
                                                    @foreach($department as $key2 => $val2)                                      
                                                    <option {{ $val->ccd_dep_id == $key2 ? "selected" : "" }} value="{{ $key2 }}">{{ $val2 }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="attachment_container">
                                            <button {{ $readonly }} type="button" class="btn btnbrowse btn-danger btn-delete-dept" id="">Delete</button>
                                        </div>
                                    </div>
                                </div>
                                <table border="1">
                                    <tr>
                                        <th class="text-center"><span class='labels2'>EMPLOYEE REQUIREMENTS</span></th>
                                        <th class="text-center"><span class='labels2'>YES</span></th>
                                        <th class="text-center"><span class='labels2'>NO</span></th>
                                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                                        <th class="text-center"><span class='labels2'>DATE</span></th>
                                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                                    </tr>
                                    
                                    <?php $req = json_decode($val->ccd_requirements);?>
                                    @foreach($req  as $key=>$rs)
                                    <tr>
                                        <td class="td_style">
                                                    <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                                                    <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                                                    <input value="{{ $rs->employee }}" {{ $readonly }} type="text" class="" name="employee_{{  $val->ccd_dep_id }}[]"  />
                                                </td>
                                        <td align="center"><input {{ $readonly }} type="radio" disabled /></td>
                                        <td align="center"><input {{ $readonly }} type="radio" disabled /></td>
                                        <td align="center"><input {{ $readonly }} type="radio" disabled value="checked" /></td>
                                        <td align="center"><input {{ $readonly }} class="date_picker" type="text" disabled /></td>
                                        <td align="center"><input {{ $readonly }} type="text" disabled /></td>
                                        <td align="center" class="td_style" ><input {{ $readonly }} type="text" value="{{ $rs->chrd_remarks }}" name="remarks_{{  $val->ccd_dep_id }}[]" /></td>
                                    </tr>    
                                    @endforeach
                                </table>                        
                        @endforeach
                                
                    </div>

                    <div class="clear_20"></div>
                    <button {{ $readonly }} type="button" class="btn btn-success add-dept">ADD DEPARTMENT</button><br/>
                        
                    <div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
	  
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' .  $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        <button class="btn btn-sm btn-danger remove-fn" attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" {{ $readonly }} >DELETE</button><br />
                        </p>
                        @endforeach 

                        <div id="attachments"></div>    
                        <span {{ $readonly }} class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input {{ $readonly }} id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">
                <span class="legend-action">ACTION</span>
                
                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>
                    <div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SEND</strong> TO RESIGNEE FOR ACKNOWLEDGEMENT</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="send">SEND</button>
                        </div>
                    </div>
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<link href="public/assets/css/chosen.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/js/chosen.jquery.min.js" type="text/javascript"></script>
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/edit.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop