<!-- General Style -->
        {{ Minify::stylesheet(
            array('/assets/css/bootstrap.css'
                 ,'/assets/css/metro-bootstrap.css'
                 ,'/assets/css/main.css'
                 ,'/assets/css/jquery-ui-1.10.4.custom.min.css'
                 ,'/assets/css/jquery.dataTables_themeroller.css'
                ,'/assets/css/new_style.css'
            ))->withFullUrl()
        }}

        <style type="text/css">
            table, tr, td, th, tbody, thead, tfoot {
    page-break-inside: avoid !important;
}
        </style>

        <form class="form-inline" method="post" enctype="multipart/form-data" action="{{ url("cc/csdh/action/$id") }}" >
            <input type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    <table width="100%">
                    <tr>
                        <td align="right" width="50%"><img src="{{ asset('assets/img/rebisco_logo.png') }}" /></td>
                        <th align="left">jm Group of food Companies</th>
                    </tr>
                    </table>

                    <div class="clear_10"></div>
                    <p class="form_label text-center ">CLEARANCE CERTIFICATION</p>
                    <table style="background-color:white;" border="1">
                        <tr>
                            <td class="bg-gray">Employee Name:</td>
                            <td >{{ $record['emp_name'] }}</td>
                            <td class="bg-gray">Division:</td>
                            <td>{{ $record['division'] }}</td>
                        </tr>

                        <tr>
                            <td class="bg-gray">Employee Number:</td>
                            <td>{{ $record['emp_no'] }}</td>
                            <td class="bg-gray">Immidiate Head:</td>
                            <td>{{ $record['supervisor'] }}</td>
                        </tr>

                        <tr>
                            <td class="bg-gray">Position:</td>
                            <td>{{ $record['position'] }}</td>
                            <td class="bg-gray">Division Head:</td>
                            <td>{{ $record['div_head'] }}</td>
                        </tr>

                        <tr>
                            <td class="bg-gray">Department:</td>
                            <td>{{ $record['department'] }}</td>
                            <td class="bg-gray">Date Hired:</td>
                            <td>{{ $record['date_hired'] }}</td>
                        </tr>

                        <tr>
                            <td class="bg-gray">Section:</td>
                            <td>{{ $record['section'] }}</td>
                            <td class="bg-gray">Effecive Date of Resignation:</td>
                            <td>{{ $record['effective_date'] }}</td>
                        </tr>

                    </table>

                    <div class="clear_10"></div>

                    <div class="note">
                        <label class="attachment_note text-center" >
                            <p>My resignation/retirement/separation from the Company has been approved effective 
                                <span class="required">{{ $record['effective_date'] }}</span>, Should I have unclear accountabilities,
                                the value of which may be deducted from the amount due me from my resignation/retirement except for Cash Advances,
                                Revolving Fund, and Petty Cash which I am required to turnover to my Department Head as prerequisite to the
                                processing of this Clearance Certification.</p>
                        </label>
                    </div>

                    <div class="clear_10"></div>

                    <table style="background-color:white;" border="1">
                        <tr>
                            <td class="bg-gray">Acknowledged By:</td>
                            <td>{{ $record['emp_name'] }}</td>
                            <td class="bg-gray">Date:</td>
                            <td>{{ $record['acknowledged_date'] }}</td>
                        </tr>


                    </table>


                    <div class="clear_10"></div>
                    
                    <div class="">

                    <table border="1">
                    

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">EMPLOYEES DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>MANUALS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio"  value="yes" name="manual_surrender" {{ $record['manual']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="manual_surrender" {{ $record['manual']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="manual_surrender"  {{ $record['manual']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['manual']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>WORK FILES SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="work_files" {{ $record['workfiles']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="work_files" {{ $record['workfiles']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="work_files"  {{ $record['workfiles']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['workfiles']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>DRAWER/CABINET KEYS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="drawer" {{ $record['drawer']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="drawer" {{ $record['drawer']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="drawer"   {{ $record['drawer']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['drawer']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>FIXED ASSETS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="assets" {{ $record['assets']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="assets" {{ $record['assets']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="assets" {{ $record['assets']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['assets']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>OFFICE SUPPLIES SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="supplies" {{ $record['supplies']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="supplies" {{ $record['supplies']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="supplies" {{ $record['supplies']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['supplies']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>ACCOUNTABLE FORMS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="accountable" {{ $record['accountable']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="accountable" {{ $record['accountable']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="accountable" {{ $record['accountable']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['accountable']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOLVING FUND/PETTY CASH SURRENDERED</span>
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="revolving" {{ $record['revolving']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="revolving" {{ $record['revolving']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="revolving" {{ $record['revolving']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['revolving']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>COMPANY CAR SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="car" {{ $record['car']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="car" {{ $record['car']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="car" {{ $record['car']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['car']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    @if(!empty($record['resignee_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['resignee_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td colspan="6">
                            Noted By:
                            <br>
                            <br>
                            {{ $record['dep_head'] }}<br>
                            {{ $record['department'] }} Head
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">INFORMATION TECHNOLOGY DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR INTERNET</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="internet" {{ $record['internet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="internet" {{ $record['internet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="internet" {{ $record['internet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['internet']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['internet']->amount }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR INTRANET</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="intranet" {{ $record['intranet']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="intranet" {{ $record['intranet']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="intranet" {{ $record['intranet']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['intranet']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['intranet']->amount }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR INTERNAL EMAIL</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="internal" {{ $record['internal']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="internal" {{ $record['internal']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="internal" {{ $record['internal']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['internal']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['internal']->amount }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR EXTERNAL EMAIL</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="external" {{ $record['external']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="external" {{ $record['external']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="external" {{ $record['external']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['external']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['external']->amount }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REMOVED FROM SMS BULK MESSAGING</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="sms" {{ $record['sms']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="sms" {{ $record['sms']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="sms" {{ $record['sms']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sms']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['sms']->amount }}</td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>PC/LAPTOP IN GOOD WORKING CONDITION</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="laptop" {{ $record['laptop']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="laptop" {{ $record['laptop']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="laptop" {{ $record['laptop']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['laptop']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['laptop']->amount }}</td>
                    </tr>

                    @if(!empty($record['citd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['citd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->amount }}</td>
                    </tr>
                    @endforeach
                    @endif

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $receiver["CC_CITD_HEAD"] }}<br>
                            Information Technology Department Head
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">SYSTEM MANAGEMENT DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR RGAS</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="rgas" {{ $record['rgas']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="rgas" {{ $record['rgas']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="rgas" {{ $record['rgas']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['rgas']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr> 

                    @if(!empty($record['smdd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['smdd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $receiver["CC_SMDD_HEAD"] }}<br>
                            System Management Department Head
                        </td>
                    </tr>

                    <tr>

                        <td colspan="9" align="center" class="labels3" >&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">ERP DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>REVOKED ACCESS FOR SAP</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="sap" {{ $record['sap']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="sap" {{ $record['sap']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="sap" {{ $record['sap']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sap']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>  

                    @if(!empty($record['erpd_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['erpd_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $receiver["CC_ERP_HEAD"] }}<br>
                            ERP Department Head
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">SALES SERVICE DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>UNPAID OFFICE SALES/VALE</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="vale" {{ $record['vale']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="vale" {{ $record['vale']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="vale" {{ $record['vale']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['vale']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['vale']->amount }}</td>
                    </tr>   

                    @if(!empty($record['ssrv_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['ssrv_others'] as $key=>$rs)
                    <tr>
                        <td>
                            {{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->amount }}</td>
                    </tr>
                    @endforeach
                    @endif

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br/><br/>
                            {{ $receiver["CC_SSRV_HEAD"] }}<br/>
                            Sales Service Department Head
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray" >HUMAN RESOURCES DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2">EMPLOYEE RELATIONS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    <tr>
                        <td><span class='required labels text-left'>CONDUCTED EXIT INTERVIEW</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="exit" {{ $record['exit']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="exit" {{ $record['exit']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="exit" {{ $record['exit']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['exit']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr> 

                    @if(!empty($record['training1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['training1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">PAYROLL BENEFITS & ADMINISTRATION</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    <tr>
                        <td><span class='required labels text-left'>HMO CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="hmo_surrender" {{ $record['hmo_surrender']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo_surrender']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels text-left'>HMO CANCELLATION NOTICE</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="hmo_cancel" {{ $record['hmo_cancel']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo_cancel']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels text-left'>ID CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="card" {{ $record['card']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="card" {{ $record['card']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="card" {{ $record['card']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['card']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>       

                    <tr>
                        <td><span class='required labels text-left'>BUILDING ACCESS CARD SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="building" {{ $record['building']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="building" {{ $record['building']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="building" {{ $record['building']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['building']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>          

                     @if(!empty($record['cb_personnel1_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['cb_personnel1_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td><span class='required labels text-left'>ACCOUNT IN BIOMETRICS DELETED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="biometrics" {{ $record['biometrics']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="biometrics" {{ $record['biometrics']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="biometrics" {{ $record['biometrics']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['biometrics']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>     

                    @if(!empty($record['recruitment_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['recruitment_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">TRAINING</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    <tr>
                        <td><span class='required labels text-left'>SERVICE AGREEMENT FULFILLED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="service" {{ $record['service']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="service" {{ $record['service']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="service" {{ $record['service']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['service']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>  

                    <tr>
                        <td><span class='required labels text-left'>TRAINING MATERIALS SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="material" {{ $record['material']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="material" {{ $record['material']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="material" {{ $record['material']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['material']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>  

                    <tr>
                        <td><span class='required labels text-left'>TRAINING EQUIPMENT SURRENDERED</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="equipment" {{ $record['equipment']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="equipment" {{ $record['equipment']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="equipment" {{ $record['equipment']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['equipment']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>      

                     @if(!empty($record['training2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                   
                    @foreach($record['training2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">LOANS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    <tr>
                        <td><span class='required labels text-left'>WITH OUTSTANDING SSS LOAN</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="sss" {{ $record['sss']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="sss" {{ $record['sss']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="sss" {{ $record['sss']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['sss']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['sss']->amount }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels text-left'>WITH OUTSTANDING AUB LOAN</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="aub" {{ $record['aub']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="aub" {{ $record['aub']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="aub" {{ $record['aub']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['aub']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['aub']->amount }}</td>
                    </tr>  

                    <tr>
                        <td><span class='required labels text-left'>WITH OUTSTANDING HMO PREMIUM FOR DEPENDENTS</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="hmo" {{ $record['hmo']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="hmo" {{ $record['hmo']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="hmo" {{ $record['hmo']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['hmo']->date }}</td>
                        <td align="center" class="labels3" >{{ $record['hmo']->amount }}</td>
                    </tr>       

                    @if(!empty($record['cb_personnel2_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['cb_personnel2_others'] as $key=>$rs)
                    <tr>
                        <td>{{ $rs->requirement }}
                        </td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" >{{ $rs->remarks }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <tr>
                        <td>
                            <p class="labels2">FOR RESIGNING SUPERVISOR AND UP ONLY</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    <tr>
                        <td><span class='required labels text-left'>STAFF'S EMPLOYEE RECORDS(201 FILE) & PERFORMANCE
                        EVALUATION TURNED-OVER TO HRD</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="staff" {{ $record['staff']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="staff" {{ $record['staff']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="staff" {{ $record['staff']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $record['staff']->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr> 

                    @if(!empty($record['cb_manager_others']))
                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        </tr>

                    
                    @foreach($record['cb_manager_others'] as $key=>$rs)
                    <tr>
                        <td class="labels3" >{{ $rs->requirement }}</td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels3" >{{ $rs->date }}</td>
                        <td align="center" class="labels3" ></td>
                    </tr>
                    @endforeach
                    @endif

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $receiver["CC_CHRD_HEAD"] }}<br>
                            Human Resources Department Head
                        </td>
                    </tr>

                    <tr>
                        <td colspan="9">&nbsp;</td>
                        </tr>

                    @foreach($other_dept as $field)
                        <tr>
                            <th colspan="9" class="text-center labels2 bg-gray">{{ $field->dept_name }}</th>
                        </tr>

                        <tr>
                            <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                            <th class="text-center"><span class='labels2'>YES</span></th>
                            <th class="text-center"><span class='labels2'>NO</span></th>
                            <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                            <th class="text-center"><span class='labels2'>DATE</span></th>
                            <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        </tr>

                        <?php $req = json_decode($field->ccd_requirements);?>
                         @foreach($req as $key=>$rs)
                        <tr>
                            <td>{{ $rs->employee }}</td>
                            <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="availability[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" >{{ $rs->date }}</td>
                            <td align="center" class="labels3" >{{ $rs->amount }}</td>
                        </tr>
                        @endforeach

                        <?php $oth = json_decode($field->ccd_others);?>
                        @if(!empty( $oth ))
                        <tr>
                            <td>
                                <p class="labels2">OTHERS</p>
                            </td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                            <td align="center" class="labels3" ></td>
                        </tr>

                        
                        @foreach($oth as $key=>$rs)
                        <tr>
                            <td>
                                {{ $rs->requirement }}
                            </td>
                            <td align="center" class="labels3" ><input type="radio" value="yes" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input type="radio" value="no" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" ><input type="radio" value="not" disabled  name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                            <td align="center" class="labels3" >{{ $rs->date }}</td>
                            <td align="center" class="labels3" >{{ $rs->amount }}</td>
                        </tr>
                        @endforeach
                        @endif 
                        
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>

                         <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $field->firstname . " " . $field->lastname }}<br>
                            {{ $field->dept_name }} Head
                        </td>
                    </tr>
                    @endforeach

                    <tr>
                          <td colspan="9">&nbsp;</td>
                    </tr>

                    <tr>
                        <th colspan="9" class="text-center labels2 bg-gray">FINANCE DEPARTMENT</th>
                    </tr>

                    <tr>
                        <th class="text-left"  ><span class='labels2'>REQUIREMENTS</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>UNLIQUIDATED ADVANCES</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>   

                    <tr>
                        <td>
                            <p class="labels2">LOANS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td><span class='required labels text-left'>COMPANY LOAN</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>   

                    <tr>
                        <td><span class='required labels text-left'>CAR PLAN</span></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>   

                    <tr>
                        <td>
                            <p class="labels2">OTHERS</p>
                        </td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled   /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled    /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled  /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled    /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled    /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled    /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                    <tr>
                        <td></td>
                        <td align="center" class="labels3" ><input type="radio" value="yes" disabled  /></td>
                        <td align="center" class="labels3" ><input type="radio" value="no" disabled    /></td>
                        <td align="center" class="labels3" ><input type="radio" value="not" disabled   /></td>
                        <td align="center" class="labels3" ></td>
                        <td align="center" class="labels3" ></td>
                    </tr>

                     <tr>
                        <td colspan="6">
                            Noted By:
                            <br><br>
                            {{ $receiver["CC_FDV_HEAD"] }}<br>
                            Finance Division Head
                        </td>
                    </tr>

                    <tr>
                           <td colspan="9">&nbsp;</td>
                    </tr>

                    <tr>
                        <td colspan="6">
                            <p class="text-center">Clearance approved and payment of accrued salary/wages authorized.</p>
                            <p>Approved By:</p>
                            <p>&nbsp;</p>
                            <p>{{ $receiver["CC_EVF_FINANCE"] }}</p>
                            <p>Executive Vice President for Finance</p>
                        </td>
                    </tr>

                    </table>

                    </div>
                    
                 

            </div><!-- end of form_container -->

        </form>