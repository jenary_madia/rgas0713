Good day!
<br><br>
You have 1 new Clearance Certification for Checking.<br>
<br>
STATUS: {{ $status == "Approved" ? "APPROVED" : "FOR " .  $status }}<br>
REFERENCE NO:{{ $reference }}<br>
RESIGNEE:{{ $name }}<br>
DEPARTMENT:{{ $department }}<br>
EFFECTIVE DATE OF RESIGNATION:{{ $effective }}<br>
<br>
Click <a href='{{ URL::to('/') }}' >here</a> to log-in and check the file<br>
<br>
<b>DO NOT REPLY. THIS IS A SYSTEM GENERATED EMAIL.</b>
<br>Service made possible by CSMD - Application Development<br>