@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/resignee/supervisor/action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                    <div class="container-header"><h5 class="text-center lined sub-header"><strong>DELIVERABLES</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center"><span class='labels2'>DELIVERABLES</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>


                    <tr>
                        <td><span class='required labels2'>MANUALS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio"  value="yes" name="manual_surrender" {{ $record['manual']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="manual_surrender" {{ $record['manual']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="manual_surrender"  {{ $record['manual']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['manual']->date }}" name="manual_surrender_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['manual']->remarks }}" name="manual_surrender_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>WORK FILES SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="work_files" {{ $record['workfiles']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="work_files" {{ $record['workfiles']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="work_files"  {{ $record['workfiles']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['workfiles']->date }}" name="work_files_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['workfiles']->remarks }}" name="work_files_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>DRAWER/CABINET KEYS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="drawer" {{ $record['drawer']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="drawer" {{ $record['drawer']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="drawer"   {{ $record['drawer']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['drawer']->date }}" name="drawer_date"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['drawer']->remarks }}" name="drawer_remarks"  /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>FIXED ASSETS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="assets" {{ $record['assets']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="assets" {{ $record['assets']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="assets" {{ $record['assets']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['assets']->date }}" name="assets_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['assets']->remarks }}" name="assets_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>OFFICE SUPPLIES SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="supplies" {{ $record['supplies']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="supplies" {{ $record['supplies']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="supplies" {{ $record['supplies']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['supplies']->date }}" name="supplies_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['supplies']->remarks }}" name="supplies_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>ACCOUNTABLE FORMS SURRENDERED</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="accountable" {{ $record['accountable']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="accountable" {{ $record['accountable']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="accountable" {{ $record['accountable']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['accountable']->date }}" name="accountable_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['accountable']->remarks }}" name="accountable_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>REVOLVING FUND/PETTY CASH SURRENDERED 
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#revolving" ></i>
                            </span>
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="revolving" {{ $record['revolving']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="revolving" {{ $record['revolving']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="revolving" {{ $record['revolving']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['revolving']->date }}" name="revolving_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['revolving']->remarks }}" name="revolving_remarks" /></td>
                    </tr>

                    <tr>
                        <td><span class='required labels2'>COMPANY CAR SURRENDERED
                                    <i class="fa fa-question-circle fa-xs question text-primary" data-toggle="modal" data-target="#surrender" ></i>
                            </span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="car" {{ $record['car']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="car" {{ $record['car']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="car" {{ $record['car']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['car']->date }}" name="car_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['car']->remarks }}" name="car_remarks" /></td>
                    </tr>

                    <tr>
                        <td>
                            <p class="labels2" >OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                    </tr>

                    @if(!empty($record['resignee_others']))
                    @foreach($record['resignee_others'] as $key=>$rs)
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" value="{{ $rs->requirement }}" name="requirement[{{$key}}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $rs->date }}" name="requirement_date[{{$key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $rs->remarks }}" name="requirement_remarks[{{$key}}]" /></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" class="requirement_txt" name="requirement[{{ $key = 0 }}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{ $key}}]"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" name="requirement_date[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" name="requirement_remarks[{{ $key}}]" /></td>
                    </tr>
                    @endif
                    
                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                   <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        @if(!$readonly)
                        @if($attach->cca_eid == Session::get("employee_id") && $attach->cca_department == "Resignee's Department" )<button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type='hidden' class='remove-fn' />@endif
                        @endif
                        <br />
                        </p>
                        @endforeach 

                        @if(!$readonly)
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif 
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    @include('cc/template/save')
                    @include('cc/template/send_head')
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>

@include('cc/template/modal')        
@stop
@section('js_ko')
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/resignee/supervisor.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop