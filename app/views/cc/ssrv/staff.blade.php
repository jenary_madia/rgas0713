@extends('template/header')

@section('content')
        <form class="form-inline" method="post" action="{{ url("cc/ssrv/staff/action/$id") }}" enctype="multipart/form-data">
            <input {{ $readonly }} type="hidden" value="{{ csrf_token() }}">
            <div class="form_container">
                    @include('cc/template/emp_record')

                    <div class="clear_20"></div>
                   <div class="container-header"><h5 class="text-center lined sub-header"><strong>OFFICE SALES</strong></h5></div>
                   
                    <div class="clear_10"></div>
                    

                    <div class="">

                    <table border="1">
                    <tr>
                        <th class="text-center"><span class='labels2'>OFFICE SALES</span></th>
                        <th class="text-center"><span class='labels2'>YES</span></th>
                        <th class="text-center"><span class='labels2'>NO</span></th>
                        <th class="text-center"><span class='labels2'>NOT APPLICABLE</span></th>
                        <th class="text-center"><span class='labels2'>DATE</span></th>
                        <th class="text-center"><span class='labels2'>AMOUNT</span></th>
                        <th class="text-center"><span class='labels2'>REMARKS</span></th>
                    </tr>


                    <tr>
                        <td><span class='required labels2'>UNPAID OFFICE SALES/VALE</span></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="vale" {{ $record['vale']->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="vale" {{ $record['vale']->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="vale" {{ $record['vale']->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $record['vale']->date }}" name="vale_date" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['vale']->amount }}" name="vale_amount" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $record['vale']->remarks }}" name="vale_remarks" /></td>
                    </tr>                

                    <tr>
                        <td>
                            <p class="labels2">OTHERS (MAXIMUM OF 5 FIELDS)</p>
                        </td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                        <td align="center" class="labels2" ></td>
                    </tr>

                    @if(!empty($record['ssrv_others']))
                    @foreach($record['ssrv_others'] as $key=>$rs)
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" value="{{ $rs->requirement }}" name="requirement[{{$key}}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{$key}}]" {{ $rs->applicable == "yes" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{$key}}]" {{ $rs->applicable == "no" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{$key}}]" {{ $rs->applicable == "not" ? "checked" : "" }} /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" value="{{ $rs->date }}" name="requirement_date[{{$key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $rs->amount }}" name="requirement_amount[{{$key}}]" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" value="{{ $rs->remarks }}" name="requirement_remarks[{{$key}}]" /></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>
                            <button {{ $readonly }} type="button" name="" class="add-row"><i class="fa fa-plus-circle fa-xs faplus"></i></button>
                            <button {{ $readonly }} type="button" name="" class="delete-row"><i class="fa fa-minus-circle fa-xs faplus"></i></button>
                            <input {{ $readonly }} type="text" class="requirement_txt" name="requirement[{{ $key = 0 }}]" />
                        </td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="yes" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="no" name="requirement_app[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="radio" value="not" name="requirement_app[{{ $key}}]"  /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} class="date_picker" type="text" name="requirement_date[{{ $key}}]" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" name="requirement_amount[{{ $key}}]" class="isDecimal" /></td>
                        <td align="center" class="labels2" ><input {{ $readonly }} type="text" name="requirement_remarks[{{ $key}}]" /></td>
                    </tr>
                    @endif

                    </table>

                    </div>
                    
                    <div class="clear_20"></div>
                     <div class="container-header"><h5 class="text-center lined sub-header"><strong>ATTACHMENTS</strong></h5></div>
                   
                    <div class="clear_10"></div>
      
                    <label class="attachment_note"><strong>ADD ATTACHMENTS:</strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div class="attachment_container">
                        @foreach($attachments as $attach)
                        <?php $attachment = json_decode($attach->cca_attachments) ;?>
                        <p>
                        <a href="{{ URL::to('/cc/download/') . '/' . $record['ref_num'] . '/' . $attachment->random_filename .'/' . CIEncrypt::encode($attachment->original_filename) }}">{{ $attachment->original_filename . " " . number_format($attachment->filesize / 1024 ,1) . "KB" }}</a>
                        <input class='attachment-filesize' type='hidden' value='{{ $attachment->filesize }}' />
                        @if(!$readonly)
                        @if($attach->cca_eid == Session::get("employee_id") && $attach->cca_department == "SSRV" )<button class="btn btn-sm btn-danger remove-fn"  attach-name="{{ $attachment->original_filename }}" attach-id="{{ $attach->cca_id }}" >DELETE</button>@else<input type='hidden' class='remove-fn' />@endif
                        @endif
                        <br />
                        </p>
                        @endforeach 

                        @if(!$readonly)
                        <div id="attachments"></div>    
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span >BROWSE</span>
                            <input id="fileupload-attachment" type="file" name="attachments[]" data-url="{{ route('file-uploader.store') }}" multiple>
                        </span>
                        @endif 
                                            
                    </div>

            </div><!-- end of form_container -->

            <div class="clear_20"></div>

            <div class="form_container">

                @include('cc/template/comment')

                <div class="clear_10"></div>
                <div class="row">
                    @include('cc/template/save')
                    @include('cc/template/send_head')
                </div>
                @include('cc/template/back')
            </div><!-- end of form_container -->

        </form>
@stop
@section('js_ko')
<script>var x = {{ $key }};</script>
{{ HTML::script('/assets/js/cc/ssrv/staff.js?') }}
<script>
var max_filesize = 20;//mb
var max_upload = 10;
</script>
{{ HTML::script('/assets/js/gd/upload.js') }}
@stop