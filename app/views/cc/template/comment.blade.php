<div class="textarea_messages_container">
    <div class="row">
        <label class="textarea_inside_label">MESSAGE:</label>
        <?php
            $message = "";
            foreach($comments as $rs):
                $last_comment_id = !$rs->ccc_status ? ($rs->ccc_id) : "";
                $last_comment = !$rs->ccc_status ? trim($rs->ccc_comment) : "";
                $message .= $rs->ccc_status ?  trim($rs->ccc_comment) . "\n" : "";
            endforeach
        ?>
        <textarea rows="3" class="form-control textarea_inside_width" disabled>{{ $message }}</textarea>
    </div>
    <div class="clear_20"></div>
    <div class="row">
        <input type="hidden" name="commentid" value="{{ $last_comment_id }}" />
        <label class="textarea_inside_label">COMMENT:</label>
        <textarea {{ $readonly }} rows="3" class="form-control textarea_inside_width" name="comment">{{ Input::old("comment") ? Input::old("comment") : $last_comment }}</textarea>
    </div>
</div>