<!-- Modal -->
<div class="modal fade" id="surrender" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <i>Employee must fully settle (outright) the balance of his/her Car Plan if he wishes to buy it, otherwise it must be returned together with the set of keys to MTPL on or before the concerned employee’s last day of employment with the Company.</i>
      </div>     
    </div>
  </div>
</div>

<div class="modal fade" id="revolving" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <i>Should be surrendered immediately after notifying the resignation.</i>
      </div>     
    </div>
  </div>
</div>