                    <label class="form_label pm_form_title ">CLEARANCE CERTIFICATION</label>
                    <div class="row">
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NAME:</label>
                            </div>
                            <div class="col2_form_container">
                                <input name="name" readonly="readonly" type="text" class="form-control"  value="{{ $record['emp_name'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DIVISION HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['div_head'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EMPLOYEE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['emp_no'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DEPARTMENT HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['dep_head'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DIVISION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['division'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">IMMEDIATE HEAD:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['supervisor'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DEPARTMENT:</label>
                            </div>
                            <div class="col2_form_container">
                                <input name="department" readonly="readonly" type="text" class="form-control" value="{{ $record['department'] }}" name="department"/>
                            </div>
                        </div>
                        <div class="col3_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">DATE HIRED:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ date("Y-m-d",strtotime($record['date_hired'])) }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels">SECTION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['section'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">EFFECTIVE DATE OF RESIGNATION:</label>
                            </div>
                            <div class="col2_form_container input-group">
                                @if(Session::get("is_cc_cbr_filer") && !isset($view))
                                <input type="text" {{ $readonly }} class="form-control date_picker"  id="effective_date" name="effective_date" value="{{ $record['effective_date'] }}" />
                                @else
                                <input type="text" readonly="" class="form-control" name="effective_date" value="{{ $record['effective_date'] }}" />
                                @endif
                                <label class="input-group-addon btn" for="effective_date">
                                   <span class="glyphicon glyphicon-calendar"></span>
                                </label>  
                            </div>    

                        </div>
                        <div class="clear_10"></div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">POSITION:</label>
                            </div>
                            <div class="col2_form_container">
                                <input readonly="readonly" type="text" class="form-control" value="{{ $record['position'] }}" />
                            </div>
                        </div>
                        <div class="row_form_container">
                            <div class="col1_form_container">
                                <label class="labels text-danger">REFERENCE NUMBER:</label>
                            </div>
                            <div class="col2_form_container">
                                <input name="reference" readonly="readonly" type="text" class="form-control" value="{{ $record['ref_num'] }}" />
                            </div>
                        </div>
                        <div class="clear_10"></div>
                    </div>