				<div class="comment_container">
                        <div class="comment_notes">
                            <label class="button_notes"><strong>SAVE</strong> TO EDIT LATER</label>
                        </div> 
                        <div class="comment_button">
                            <button {{ $readonly }} type="submit" class="btn btn-default btndefault" name="action" value="save">SAVE</button>
                        </div>
                    </div>
                    <div class="clear_10"></div>