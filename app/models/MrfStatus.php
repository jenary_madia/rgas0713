<?php


class MrfStatus extends \Eloquent {


    protected $table = "mrf_status";

    protected $primaryKey = "id";
    protected $guarded = array('id');
    public $incrementing = true;

    public $timestamps = false;

}

