<?php
use app\RGAS;

class AuditTrails extends \Eloquent 
{   
	protected $primaryKey = '';
    protected $table = 'audit_trails';
    public $incrementing = false;
    public $timestamps = false;

    //added by jma
    public function ScopeModule($query , $q)
	{
		if($q != "null" && !is_array($q))       
                {
                    return $query->whereIn("module_id" , explode(",",$q) );
                }
	}


	public function ScopeEmployee($query , $q)
	{
		if($q != "null" && !is_array($q))       
                {
                    return $query->whereIn("user_id" , explode(",",$q) );
                }
	}        
       
        
    public function scopeSearch($query)
    {
        $sSearch = Input::get('sSearch', TRUE);
        if (!empty($sSearch) )
            {
            
            return $query->whereRaw("(ref_num like '%$sSearch%' or action_code like '%$sSearch%' or table_name like '%$sSearch%' or params like '%$sSearch%'  or old like '%$sSearch%'  or new like '%$sSearch%')" );
        }
    }
	
	public static function records()
	{
        $input = Input::all();
        $val_data = ['from' => 'required|date',
                     'to' => 'required|date'];
        
        $validator = Validator::make($input , $val_data);
        //RUN LARAVEL VALIDATION    
        if ($validator->fails()) return ;

        $start = Input::get("from");
        $end = Input::get("to");
        $module = Input::get("module");
        $employee = Input::get("employee");

        $requests = AuditTrails::join('modules','modules.id','=','module_id' )
                ->join('employees','employees.id','=','user_id' )
                ->module($module)
                ->employee($employee)
                ->search()
                ->whereBetween('date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end))] )
                ->get(["firstname","lastname","new","old","params","table_name","action_code","ref_num","date","time","user_id","desc"]); 

    	 $result_data = array();
    	if ( count($requests) > 0)
        {
                $ctr = 0;
                foreach($requests as $req) 
                 {				
                    $result_data[$ctr][] = $req->date;
    				$result_data[$ctr][] = $req->time ;
    				$result_data[$ctr][] = $req->firstname . " " . $req->lastname;
    				$result_data[$ctr][] = $req->desc;
    				$result_data[$ctr][] = $req->ref_num;
    				$result_data[$ctr][] = AuditTrails::actions($req->action_code);
    				$result_data[$ctr][] = $req->table_name;
    				
                    $result_data[$ctr][] = AuditTrails::extract($req->params);
    				$result_data[$ctr][] = AuditTrails::extract($req->old);
    				$result_data[$ctr][] = AuditTrails::extract($req->new);
                    $ctr++;
                 }
        }

		return $result_data;
	}

	public static function modules()
	{
            $table = DB::table("modules")->get();
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->desc;
            }
            
            return $arr;
        }
        

	public static function employees()
	{
        $table = DB::table("employees")->get();
        $arr = array();
        foreach($table as $key=>$val)
        {
            $arr[$val->id] = $val->firstname . " " . $val->lastname;
        }
        
        return $arr;
    }

    
    public static function extract($var)
    {
        $show = " ";
        if(trim($var))
        {
		    $param = json_decode($var);
                        if(is_object($param))
                        {
                            foreach($param as $key=>$val)
                            {
                                $show .=  (!is_object($key) ? (is_array($key) ? "key array" : $key) : "key object") . ": " . (!is_object($val) ? (is_array($val) ? AuditTrails::extract_array($val) : $val) : AuditTrails::extract_object($val)  )   . "\n";
                            }		        
                        }
                        else
                        {
                            $show = (is_array($var) ? "var array" : $var) . "\n";
                        }
        }
        
        return substr($show, 0 , strlen($show) - 1);
    }
    

     public static function extract_object($param)
    {
        $text = "";
        foreach($param as $key=>$val)
        {
            $text .= ( is_object($val) ? AuditTrails::extract_object($val) : $val) .  "\n";                            
        }

        return $text;
    }

     public static function extract_array($param)
    {
        $text = "";
        foreach($param as $key=>$val)
        {
            $text .= ( is_object($val) ? AuditTrails::extract_object($val) : $val) .  "\n";                            
        }

        return $text;
    }
    
    public static function actions($var)        
    {
        $action["AU001"] = "Create";
        $action["AU002"] = "Upload File";
        $action["AU003"] = "Retreive";
        $action["AU004"] = "Update";
        $action["AU005"] = "Delete";
        $action["AU006"] = "Print";
        $action["AU007"] = "Export";
        $action["AU008"] = "Download";
        $action["AU009"] = "Login";
        $action["AU010"] = "Login Failed";
        $action["AU011"] = "Logout";
        $action["AU012"] = "Invalid Access";
        
        $action["Create"] = "AU001";
        $action["Upload File"] = "AU002";
        $action["Retreive"] = "AU003";
        $action["Update"] = "AU004";
        $action["Delete"] = "AU005";
        $action["Print"] = "AU006";
        $action["Export"] = "AU007";
        $action["Download"] = "AU008";
        $action["Login"] = "AU009";
        $action["Login Failed"] = "AU010";
        $action["Logout"] = "AU011";
        $action["Invalid Access"] = "AU012";
        
        return isset($action[ $var ]) ? $action[ $var ] : "";
        
    }
    //end
	
}