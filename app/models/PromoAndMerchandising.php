<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Repositories\PMARFRepository;

class PromoAndMerchandising extends \Eloquent {

	protected $table = "promoandmerchandising";
	protected $primaryKey = 'pmarf_id';
	public $incrementing = true;
	public $timestamps = false;
	
	// public function __destruct()
	// {
		// // $queries = DB::getQueryLog();
		// // print_r(end($queries));
	// }
	private $attachments_path = '/rgas/pmarf/';
	
	
	public function edit_pmarf($id_pmarf){
        $requests = $this->where('pmarf_id', '=', $id_pmarf)->first();
        return $requests;
	}
    
    function rebuild_brands($participating_brands){
		
        $brands = json_decode($participating_brands);
		
        $aQty       = explode(",",$this->cleanMe($brands->qty));
        $aUnit      = explode(",",$this->cleanMe($brands->unit));
        $aProduct   = explode(",",$this->cleanMe($brands->product));
        
		
		$ctr = 0;
		$aBrands = false;
		foreach($brands as $sTitle=>$sData){
			$last_key_id = json_decode($sData,1);
			if($last_key_id){
				$keys = array_keys($last_key_id);
				if($keys){
					foreach($keys as $key){
						list($sTitle, $index) = explode('_',$key);
						$aBrands[$index][$sTitle."_".$index] = json_decode($sData)->{$sTitle."_".$index};
					}
				}
			}
			
		}
		return $aBrands;	
        for($a=1;$a<=count($aQty);$a++){
            foreach($brands as $sTitle=>$sData){
				echo $sTitle;
                $sTitled = $sTitle.'_'.$a;
                if(strpos($sData,$sTitled) >= 0){
                    $aData = json_decode($sData);
                    $aBrands[$a][$sTitled] = $aData->$sTitled;  
                }
            }
        } 
        
    }
    
    function cleanMe($str){
        $str = str_replace('{','',$str);
        $str = str_replace('}','',$str);
        $str = str_replace('"','',$str);
        
        return $str;
    }
    
}/* End of file */
?>
