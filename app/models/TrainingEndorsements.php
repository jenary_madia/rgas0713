<?php
use RGAS\Libraries;

class TrainingEndorsements extends \Eloquent {
    
	protected $primaryKey = 'te_id';
    protected $table = 'trainingendorsements';
    public $incrementing = true;
    public $timestamps = true;
	
	public function tep()
	{
		return $this->hasMany('TrainingEndorsementsParticipants','tep_te_id');
	}
	
	public function __destruct()
	{
		// $queries = DB::getQueryLog();
		// print_r(end($queries));
	}
	
}