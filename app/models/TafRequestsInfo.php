<?php


class TafRequestsInfo extends \Eloquent {

    
	 protected $table = 'taf_requests_info';
	 protected $primaryKey = 'taf_info_id';
	 
	 public $incrementing = true;
	 
	 public $timestamps = false;
	 
	 
	 //
	public static function get_traveler_info($taf_no)
	{
		
		
		$traveler_info = TafRequestsInfo::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "taf_requests_info.employee_id");
			
		})->leftJoin("departments as dept", function($join){
			
			$join->on("dept.id", "=", "emp.departmentid");
		
		})->where("taf_requests_info.taf_no", "=", "{$taf_no}")->get([
		
			 "taf_requests_info.taf_info_id"
			,"taf_requests_info.taf_no"
			,"taf_requests_info.employee_id"
			,"taf_requests_info.contact_no"
			,"emp.id"
			,"emp.employeeid"
			,"emp.firstname"
			,"emp.middlename"
			,"emp.lastname"
			,"emp.designation"
			,"emp.departmentid"
			,"dept.id as dept_id"
			,"dept.dept_name"
		
		]);
		
		return $traveler_info;
		
	}
	 
	 
	 
}
/* End of file */