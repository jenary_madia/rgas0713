<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\CC;

class ClearanceCertification extends \Eloquent 
{
    
    protected $table = 'clearancecertification';
    protected $guarded = array('cc_id');
    public $timestamps = false;

    

    public function ScopeCondition($query)
    {
        if( Session::get("is_cc_epr_head") )         
            return $query->where("cc_erp_status" , 2);
        else if( Session::get("is_cc_ssrv_head") )     
            return $query->where("cc_ssrv_status" , 2);  
        else if( Session::get("is_cc_smdd_head") )     
            return $query->where("cc_smd_status" , 2);  
        else if( Session::get("is_cc_epr_staff") )         
            return $query->where("cc_erp_status" , 0);
        else if( Session::get("is_cc_ssrv_staff") )     
            return $query->where("cc_ssrv_status" , 0);  
        else if( Session::get("is_cc_smdd_staff") )     
            return $query->where("cc_smd_status" , 0);
        else if( Session::get("is_cc_training1") )     
            return $query->where("cc_training1_status" , 0); 
        else if( Session::get("is_cc_training2") )     
            return $query->where("cc_training2_status" , 0); 
        else if( Session::get("is_cc_cbr_personnel1") )     
            return $query->where("cc_cb_personnel1_status" , 0); 
        else if( Session::get("is_cc_cbr_personnel2") )     
            return $query->where("cc_cb_personnel2_status" , 0); 
        else if( Session::get("is_cc_cbr_manager") )     
            return $query->where("cc_cb_manager_status" , 0);   
        else if( Session::get("is_cc_recruitment") )     
            return $query->where("cc_recruitment_status" , 0);
        else if( Session::get("is_cc_chrd_head") )     
            return $query->where("cc_chrd_status" , 2);   
        else if( Session::get("is_cc_it_head") )     
            return $query->wherein("cc_technical_status" , [0,3]);   
        else if( Session::get("is_cc_citd_staff") )     
            return $query->where("cc_technical_status" , 2)->where("cc_technical_assigned",Session::get("employee_id"));
        else if( Session::get("is_cc_citd_head") )     
            return $query->where("cc_technical_status" , 4);   
        else if(Session::get("desig_level") == "head")
            return $query->join("clearancecertificationdepartments" , "ccd_cc_id","=","cc_id")
                ->wherein("ccd_status", [0,3])  
                ->where("ccd_dep_id", Session::get("dept_id")) 
                ->where("cc_others_status", 0)
                ->select(["ccd_status","cc_technical_status","cc_ref_num","cc_resignee_status","cc_id","firstname","lastname","dept_name",
                "cc_create_date","cc_resignation_date"]);
        else if(Session::get("desig_level") == "supervisor")
            return $query->join("clearancecertificationdepartments" , "ccd_cc_id","=","cc_id")
                ->where("ccd_assigned_id",Session::get("employee_id"))
                ->where("ccd_status", 2)
                ->where("ccd_dep_id", Session::get("dept_id")) 
                ->where("cc_others_status", 0)
                ->select(["ccd_status","cc_technical_status","cc_ref_num","cc_resignee_status","cc_id","firstname","lastname","dept_name",
                "cc_create_date","cc_resignation_date"]);
        else
            return $query->whereRaw(" 1 != 1");   
    }

    

    public function ScopeResignee_condition($query)
    {

        if(in_array(Session::get("desig_level") , ["head","top mngt","president"])) 
            return $query->where("cc_dept_head",Session::get("employee_id"))
                ->wherein("cc_resignee_status", [0,3]);
               // ->where("cc_emp_departmentid", Session::get("dept_id"));
        else if(Session::get("desig_level") == "supervisor")
            //return $query->where("cc_resignee_assigned",Session::get("employee_id"))
            return $query->whereRaw("(cc_resignee_assigned = ? or cc_dept_head = ?)",[Session::get("employee_id"),Session::get("employee_id")])
                ->wherein("cc_resignee_status", [0,2,3])
                ->where("cc_emp_departmentid", Session::get("dept_id"));
        else
            return $query->whereRaw(" 1 != 1");   
    }

  
	public static function employees()
	{
            $table = DB::table("employees")
                ->where("company","RBC-CORP")
                ->orderby("firstname")
                ->get(["firstname" , "lastname","id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->firstname . " " . $val->lastname;
            }
            
            return $arr;
    }

    public static function department_list()
    {
            $table = DB::table("departments")
                ->where("active",1)
                ->where("comp_code","RBC-CORP")
                ->orderby("dept_name")
                ->get(["dept_name" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->dept_name;
            }
            
            return $arr;
    }

    
    public static function department()
    {
            $table = DB::table("departments")
                ->where("active",1)
                ->where("comp_code","RBC-CORP")
                ->wherenotin("dept_code",["HRD" , "CITD" , "ERPD" ,"SMDD" , "SSD"] )
                ->orderby("dept_name")
                ->get(["dept_name" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->dept_name;
            }
            
            return $arr;
    }

    
    public static function supervisor_list( $eid )
    {
        $deptid = Session::get("dept_id");
        $rec = Employees::where("desig_level","supervisor")
            ->where("departmentid" , $deptid)
            ->where("employeeid" , "!=" , $eid)
            ->get(["firstname","lastname","employees.id"]);

        return $rec;
    }

    
    public static function receivers_list($code , $eid )
    {
        $rec = Receivers::wherein("code",$code)
            ->join("employees","receivers.employeeid","=","employees.id")
            ->where("employees.employeeid" , "!=" , $eid)
            ->get(["firstname","lastname","employees.id" , "code"]);

        return $rec;
    }

    public static function receivers_list_head($code , $eid ,$id)
    {
        $rec = Receivers::wherein("code",$code)
            ->join("employees","receivers.employeeid","=","employees.id")
            ->where("employees.employeeid" , "!=" , $eid)
            ->whereRaw(" employees.id in (select ccl_assigned from clearancecertificationlogs
                where ccl_date_complete is not null and ccl_cc_id='$id')")
            ->get(["firstname","lastname","employees.id" , "code"]);

        return $rec;
    }
    
    public static function receivers_clearance($eid)
    {
        $rec = Receivers::where("module","clearancecertification")
            ->join("employees","receivers.employeeid","=","employees.id")
            ->where("employees.employeeid" , "!=" , $eid)
            ->get(["firstname","lastname" , "code"]);

        $data["CC_ERP_HEAD"] = $data["CC_SSRV_HEAD"]= $data["CC_SMDD_HEAD"]
        = $data["CC_CHRD_HEAD"]= $data["CC_CITD_HEAD"] = $data["CC_FDV_HEAD"]= $data["CC_EVF_FINANCE"] = "";

        foreach ($rec as $key => $rs) 
        {
          $data[$rs->code] = $rs->firstname . " " . $rs->lastname;
        }

        return $data;
    }

    
    public static function record($resignee)
    {
        $rec = Employees::wherein("id" , [$resignee[0]->cc_emp_id , $resignee[0]->cc_division_head,
                $resignee[0]->cc_immediate_head,$resignee[0]->cc_dept_head])
                ->orwherein("desig_level",["head","supervisor","employee","president"])
                ->get(["firstname" , "lastname" , "id" ,"employeeid"]);

        foreach ($rec as $key => $value) 
        {
            $emp[$value->id]["name"] = $value->firstname . " " . $value->lastname;
            $emp[$value->id]["emp_no"] = $value->employeeid;
        }

        $data["emp_name"] =  $emp[$resignee[0]->cc_emp_id]["name"];
        $data["emp_no"] =  $emp[$resignee[0]->cc_emp_id]["emp_no"];
        $data["division"] = $resignee[0]->cc_div_code;
        $data["department"] = $resignee[0]->dept_name;
        $data["section"] = $resignee[0]->sect_name;
        $data["position"] = $resignee[0]->cc_position;
        $data["div_head"] =  isset($emp[$resignee[0]->cc_division_head]["name"]) ? $emp[$resignee[0]->cc_division_head]["name"] : "";
        $data["dep_head"] =  isset($emp[$resignee[0]->cc_dept_head]["name"]) ? $emp[$resignee[0]->cc_dept_head]["name"] : "";
        $data["supervisor"] =  isset($emp[$resignee[0]->cc_immediate_head]["name"]) ? $emp[$resignee[0]->cc_immediate_head]["name"] : "";
        $data["date_hired"] = $resignee[0]->cc_date_hired;
        $data["effective_date"] = $resignee[0]->cc_resignation_date;
        $data["ref_num"] = $resignee[0]->cc_ref_num;

        if(isset($resignee[0]->cc_status)) $data["overall_status"] = $resignee[0]->cc_status;
        if(isset($resignee[0]->cc_resignee_status)) $data["resignee_head"] = $resignee[0]->cc_resignee_status;
        if(isset($resignee[0]->cc_dept_head)) $data["dept_head"] = $resignee[0]->cc_dept_head;

        if(isset($resignee[0]->cc_resignee_assigned)) $data["resignee_by"] = $emp[$resignee[0]->cc_resignee_assigned]["name"];
        if(isset($resignee[0]->cc_ssrv_assigned)) $data["ssrv_by"] = $emp[$resignee[0]->cc_ssrv_assigned]["name"];
        if(isset($resignee[0]->cc_erp_assigned)) $data["erp_by"] = $emp[$resignee[0]->cc_erp_assigned]["name"];
        if(isset($resignee[0]->cc_smd_assigned)) $data["smd_by"] = $emp[$resignee[0]->cc_smd_assigned]["name"];
        if(isset($resignee[0]->cc_technical_assigned)) $data["citd_by"] = $emp[$resignee[0]->cc_technical_assigned]["name"];
      
        if(isset($resignee[0]->cc_resignee_date)) $data["resignee_date"] = $resignee[0]->cc_resignee_date;
        if(isset($resignee[0]->cc_ssrv_date)) $data["ssrv_date"] = $resignee[0]->cc_ssrv_date;
        if(isset($resignee[0]->cc_erp_date)) $data["erp_date"] = $resignee[0]->cc_erp_date;
        if(isset($resignee[0]->cc_smd_date)) $data["smd_date"] = $resignee[0]->cc_smd_date;
        if(isset($resignee[0]->cc_technical_date)) $data["citd_date"] = $resignee[0]->cc_technical_date;
      
        if(isset($resignee[0]->cc_training1_assigned)) $data["training1_by"] = $emp[$resignee[0]->cc_training1_assigned]["name"];
        if(isset($resignee[0]->cc_training2_assigned)) $data["training2_by"] = $emp[$resignee[0]->cc_training2_assigned]["name"];
        if(isset($resignee[0]->cc_cb_personnel1_assigned)) $data["personnel1_by"] = $emp[$resignee[0]->cc_cb_personnel1_assigned]["name"];
        if(isset($resignee[0]->cc_cb_personnel2_assigned)) $data["personnel2_by"] = $emp[$resignee[0]->cc_cb_personnel2_assigned]["name"];
        if(isset($resignee[0]->cc_cb_manager_assigned)) $data["manager_by"] = $emp[$resignee[0]->cc_cb_manager_assigned]["name"];
        if(isset($resignee[0]->cc_recruiment_assigned)) $data["recruitment_by"] = $emp[$resignee[0]->cc_recruiment_assigned]["name"];

        if(isset($resignee[0]->cc_training1_date)) $data["training1_date"] = $resignee[0]->cc_training1_date;
        if(isset($resignee[0]->cc_training2_date)) $data["training2_date"] = $resignee[0]->cc_training2_date;
        if(isset($resignee[0]->cc_cb_personnel1_date)) $data["personnel1_date"] = $resignee[0]->cc_cb_personnel1_date;
        if(isset($resignee[0]->cc_cb_personnel2_date)) $data["personnel2_date"] = $resignee[0]->cc_cb_personnel2_date;
        if(isset($resignee[0]->cc_cb_manager_date)) $data["manager_date"] = $resignee[0]->cc_cb_manager_date;
        if(isset($resignee[0]->cc_recruitment_date)) $data["recruitment_date"] = $resignee[0]->cc_recruitment_date;

        if(isset($resignee[0]->cc_training1_status)) $data["training1"] = $resignee[0]->cc_training1_status;
        if(isset($resignee[0]->cc_training2_status)) $data["training2"] = $resignee[0]->cc_training2_status;
        if(isset($resignee[0]->cc_cb_personnel1_status)) $data["personnel1"] = $resignee[0]->cc_cb_personnel1_status;
        if(isset($resignee[0]->cc_cb_personnel2_status)) $data["personnel2"] = $resignee[0]->cc_cb_personnel2_status;
        if(isset($resignee[0]->cc_cb_manager_status)) $data["manager"] = $resignee[0]->cc_cb_manager_status;
        if(isset($resignee[0]->cc_recruitment_status)) $data["recruitment"] = $resignee[0]->cc_recruitment_status;

        if(isset($resignee[0]->cc_others_status)) $data["others_stats"] = $resignee[0]->cc_others_status;
        if(isset($resignee[0]->cc_resignee_status)) $data["resignee"] = $resignee[0]->cc_resignee_status;
        if(isset($resignee[0]->cc_smd_status)) $data["smdd"] = $resignee[0]->cc_smd_status;
        if(isset($resignee[0]->cc_technical_status)) $data["citd"] = $resignee[0]->cc_technical_status;
        if(isset($resignee[0]->cc_erp_status)) $data["erpd"] = $resignee[0]->cc_erp_status;
        if(isset($resignee[0]->cc_ssrv_status)) $data["ssrv"] = $resignee[0]->cc_ssrv_status;
        if(isset($resignee[0]->cc_chrd_status)) $data["chrd"] = $resignee[0]->cc_chrd_status;


        if(isset($resignee[0]->cc_manual_surrendered)) $data["manual"] = ClearanceCertification::return_json_decode($resignee[0]->cc_manual_surrendered);
        if(isset($resignee[0]->cc_workfiles)) $data["workfiles"] = ClearanceCertification::return_json_decode($resignee[0]->cc_workfiles);
        if(isset($resignee[0]->cc_drawer)) $data["drawer"] = ClearanceCertification::return_json_decode($resignee[0]->cc_drawer);
        if(isset($resignee[0]->cc_fixed_assets)) $data["assets"] = ClearanceCertification::return_json_decode($resignee[0]->cc_fixed_assets);
        if(isset($resignee[0]->cc_office_supplies)) $data["supplies"] = ClearanceCertification::return_json_decode($resignee[0]->cc_office_supplies);
        if(isset($resignee[0]->cc_accountableforms)) $data["accountable"] = ClearanceCertification::return_json_decode($resignee[0]->cc_accountableforms);
        if(isset($resignee[0]->cc_revolvingfund)) $data["revolving"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revolvingfund);
        if(isset($resignee[0]->cc_companycar)) $data["car"] = ClearanceCertification::return_json_decode($resignee[0]->cc_companycar);
        if(isset($resignee[0]->cc_other_resignee_deliverables)) $data["resignee_others"] = json_decode($resignee[0]->cc_other_resignee_deliverables);

        if(isset($resignee[0]->cc_revoke_internet)) $data["internet"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_internet);
        if(isset($resignee[0]->cc_revoke_intranet)) $data["intranet"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_intranet);
        if(isset($resignee[0]->cc_revoke_internal)) $data["internal"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_internal);
        if(isset($resignee[0]->cc_revoke_external)) $data["external"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_external);
        if(isset($resignee[0]->cc_remove_sms)) $data["sms"] = ClearanceCertification::return_json_decode($resignee[0]->cc_remove_sms);
        if(isset($resignee[0]->cc_pc_laptop)) $data["laptop"] = ClearanceCertification::return_json_decode($resignee[0]->cc_pc_laptop);
        if(isset($resignee[0]->cc_other_it_deliverables)) $data["citd_others"] = json_decode($resignee[0]->cc_other_it_deliverables);

        if(isset($resignee[0]->cc_revoke_rgas)) $data["rgas"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_rgas);
        if(isset($resignee[0]->cc_other_smd_deliverables)) $data["smdd_others"] = json_decode($resignee[0]->cc_other_smd_deliverables);
       
        if(isset($resignee[0]->cc_revoke_sap)) $data["sap"] = ClearanceCertification::return_json_decode($resignee[0]->cc_revoke_sap);
        if(isset($resignee[0]->cc_other_erp_deliverables)) $data["erpd_others"] = json_decode($resignee[0]->cc_other_erp_deliverables);
       
        if(isset($resignee[0]->cc_unpaid_vale)) $data["vale"] = ClearanceCertification::return_json_decode($resignee[0]->cc_unpaid_vale);
        if(isset($resignee[0]->cc_other_ssrv_deliverables)) $data["ssrv_others"] = json_decode($resignee[0]->cc_other_ssrv_deliverables);
       
        if(isset($resignee[0]->cc_conducted_exit)) $data["exit"] = ClearanceCertification::return_json_decode($resignee[0]->cc_conducted_exit);
        if(isset($resignee[0]->cc_other_training1_deliverables)) $data["training1_others"] = json_decode($resignee[0]->cc_other_training1_deliverables);

        if(isset($resignee[0]->cc_hmo_card)) $data["hmo_surrender"] = ClearanceCertification::return_json_decode($resignee[0]->cc_hmo_card);
        if(isset($resignee[0]->cc_hmo_cancellation)) $data["hmo_cancel"] = ClearanceCertification::return_json_decode($resignee[0]->cc_hmo_cancellation);
        if(isset($resignee[0]->cc_id_card)) $data["card"] = ClearanceCertification::return_json_decode($resignee[0]->cc_id_card);
        if(isset($resignee[0]->cc_building_access)) $data["building"] = ClearanceCertification::return_json_decode($resignee[0]->cc_building_access);
        if(isset($resignee[0]->cc_other_cb_personnel1_deliverables)) $data["cb_personnel1_others"] = json_decode($resignee[0]->cc_other_cb_personnel1_deliverables);
        
        if(isset($resignee[0]->cc_biometrics)) $data["biometrics"] = ClearanceCertification::return_json_decode($resignee[0]->cc_biometrics);
        if(isset($resignee[0]->cc_other_recruitment)) $data["recruitment_others"] = json_decode($resignee[0]->cc_other_recruitment);
        
        if(isset($resignee[0]->cc_service_aggreement)) $data["service"] = ClearanceCertification::return_json_decode($resignee[0]->cc_service_aggreement);
        if(isset($resignee[0]->cc_training_materials)) $data["material"] = ClearanceCertification::return_json_decode($resignee[0]->cc_training_materials);
        if(isset($resignee[0]->cc_training_equipment)) $data["equipment"] = ClearanceCertification::return_json_decode($resignee[0]->cc_training_equipment);
        if(isset($resignee[0]->cc_other_training2_deliverables)) $data["training2_others"] = json_decode($resignee[0]->cc_other_training2_deliverables);
        
        if(isset($resignee[0]->cc_outstanding_sss)) $data["sss"] = ClearanceCertification::return_json_decode($resignee[0]->cc_outstanding_sss);
        if(isset($resignee[0]->cc_outstanding_aub)) $data["aub"] = ClearanceCertification::return_json_decode($resignee[0]->cc_outstanding_aub);
        if(isset($resignee[0]->cc_outstanding_hmo)) $data["hmo"] = ClearanceCertification::return_json_decode($resignee[0]->cc_outstanding_hmo);
        if(isset($resignee[0]->cc_other_cb_personnel2_deliverables)) $data["cb_personnel2_others"] = json_decode($resignee[0]->cc_other_cb_personnel2_deliverables);
        
        if(isset($resignee[0]->cc_staff_employee)) $data["staff"] = ClearanceCertification::return_json_decode($resignee[0]->cc_staff_employee);
        if(isset($resignee[0]->cc_other_cb_manager_deliverables)) $data["cb_manager_others"] = json_decode($resignee[0]->cc_other_cb_manager_deliverables);

         if(isset($resignee[0]->ccd_requirements)) $data["ccd_requirements"] = ClearanceCertification::return_json_decode($resignee[0]->ccd_requirements);
        if(isset($resignee[0]->ccd_others)) $data["ccd_others"] = json_decode($resignee[0]->ccd_others);

        if(isset($resignee[0]->cc_acknowledgement_date)) $data["acknowledged_date"] = ($resignee[0]->cc_acknowledgement_date);
        return $data;
    }

    
    public static function return_json_decode($val)
    {
        $out = $val ? $val : json_encode(array("applicable"=>"","date"=>"","remarks"=>"","amount"=>""));
        return json_decode($out);        
    }

    
    public static function view_attachments($id)
    {
            $table = DB::table("clearancecertificationattachments")
                ->where("cca_cc_id",$id)
                ->get(["cca_attachments","cca_id","cca_department","cca_eid"]);
            
            return $table;
    }

    
    public static function view_chrd_attachments($id)
    {
            $table = DB::table("clearancecertificationattachments")
                ->where("cca_cc_id",$id)
                ->where(function ($query) {
                    $query->wherein("cca_department",["Training1","Training2","Personnel1","Personnel2","Manager","Recruitment","CHRD"])                        
                          ->orWhere('cca_position', '=', "All");
                      })
                ->get(["cca_attachments","cca_id","cca_department","cca_eid"]);
            
            return $table;
    }    

    
    public static function view_department_attachments($id, $department)
    {
            $table = DB::table("clearancecertificationattachments")
                ->where("cca_cc_id",$id)
                ->whereRaw("(cca_position = 'All' or  cca_department = ? )",[$department])
                ->get(["cca_attachments","cca_id","cca_department","cca_eid"]);
            
            return $table;
    }

    
    public static function view_chrd_comment($id)
    {        
        $comment = DB::table("clearancecertificationcomments")->where("ccc_cc_id" , $id)
        ->where(function ($query) {
                    $query->wherein("ccc_department",["Training1","Training2","Personnel1","Personnel2","Manager","Recruitment","CHRD"])                        
                          ->orWhere('ccc_position', '=', "All");
                      })
        ->get(["ccc_comment","ccc_status","ccc_id"]);

        return $comment;
    }

    
    public static function view_resignee_comment($id)
    {        
        $comment = DB::table("clearancecertificationcomments")->where("ccc_cc_id" , $id)
        ->where("ccc_position","All")
        ->get(["ccc_comment","ccc_status","ccc_id"]);

        return $comment;
    }

    
    public static function view_csdh_comment($id)
    {        
        $comment = DB::table("clearancecertificationcomments")->where("ccc_cc_id" , $id)
        ->whereRaw("(ccc_position = 'All' or  ccc_position = 'Head'  or  ccc_position = 'CSDH' )")
        ->get(["ccc_comment","ccc_status","ccc_id"]);

        return $comment;
    }

    
    public static function view_department_comment($id  , $department)
    {        
        $comment = DB::table("clearancecertificationcomments")->where("ccc_cc_id" , $id)
        ->whereRaw("(ccc_position = 'All' or  ccc_department = ? )",[$department])
        ->orderby("ccc_id","asc")
        ->get(["ccc_comment","ccc_status","ccc_id"]);

        return $comment;
    }


    
    public static function csdh_record($id , $status)
    {        
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",$status)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["cc_status","sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num", "cc_conducted_exit",
            "cc_other_training1_deliverables", "cc_hmo_card",
            "cc_hmo_cancellation","cc_id_card","cc_building_access","cc_other_cb_personnel1_deliverables"
            , "cc_biometrics", "cc_other_recruitment" , "cc_service_aggreement",
            "cc_training_materials","cc_training_equipment","cc_other_training2_deliverables"
            , "cc_outstanding_sss", "cc_outstanding_aub","cc_outstanding_hmo","cc_other_cb_personnel2_deliverables"
            , "cc_staff_employee", "cc_other_cb_manager_deliverables"
            , "cc_manual_surrendered", "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"
            , "cc_revoke_internet","cc_revoke_intranet","cc_revoke_internal","cc_revoke_external","cc_remove_sms","cc_pc_laptop"
            ,"cc_other_it_deliverables" , "cc_revoke_rgas",  "cc_other_smd_deliverables"   , "cc_revoke_sap",
            "cc_other_erp_deliverables" , "cc_unpaid_vale",  "cc_other_ssrv_deliverables",
            "cc_others_status","cc_resignee_status","cc_technical_status","cc_erp_status","cc_smd_status"
            ,"cc_ssrv_status","cc_chrd_status"
            ,"cc_cb_manager_assigned","cc_cb_manager_date","cc_cb_personnel2_assigned","cc_cb_personnel2_date"
            ,"cc_resignee_assigned","cc_resignee_date"
            ,"cc_ssrv_assigned","cc_ssrv_date","cc_erp_assigned","cc_erp_date"
            ,"cc_smd_assigned","cc_smd_date","cc_technical_assigned","cc_technical_date"
            ,"cc_training1_assigned","cc_training1_date","cc_cb_personnel1_assigned","cc_cb_personnel1_date"
            ,"cc_training2_assigned","cc_training2_date","cc_recruiment_assigned","cc_recruitment_date","cc_acknowledgement_date"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }


    public static function csdh_other_department($id)
    {
         $data = DB::table("clearancecertificationdepartments")
            ->join("employees","ccd_assigned_id","=","employees.id")
            ->join("departments" , "ccd_dep_id" , "=" , "departments.id")
            ->where("ccd_cc_id",$id)
            ->get(["ccd_date","firstname","lastname","departments.id" , "dept_name","ccd_requirements","ccd_others","ccd_dep_id","ccd_status"]);

        return $data;
    }

    
    public static function chrd_record($id)
    {        
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_chrd_status",2)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num", "cc_conducted_exit",
            "cc_other_training1_deliverables", "cc_hmo_card",
            "cc_hmo_cancellation","cc_id_card","cc_building_access","cc_other_cb_personnel1_deliverables"
            , "cc_biometrics", "cc_other_recruitment" , "cc_service_aggreement",
            "cc_training_materials","cc_training_equipment","cc_other_training2_deliverables"
            , "cc_outstanding_sss", "cc_outstanding_aub","cc_outstanding_hmo","cc_other_cb_personnel2_deliverables"
            , "cc_staff_employee", "cc_other_cb_manager_deliverables","cc_training1_status","cc_training2_status",
            "cc_cb_personnel1_status","cc_cb_personnel2_status","cc_cb_manager_status","cc_recruitment_status"
            ,"cc_cb_manager_assigned","cc_cb_manager_date","cc_cb_personnel2_assigned","cc_cb_personnel2_date"
            ,"cc_training1_assigned","cc_training1_date","cc_cb_personnel1_assigned","cc_cb_personnel1_date"
            ,"cc_training2_assigned","cc_training2_date","cc_recruiment_assigned","cc_recruitment_date",
            "cc_resignee_status","cc_resignee_assigned","cc_resignee_date","cc_dept_head", "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function department_head_record($id)
    {        
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("ccd_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->join('clearancecertificationdepartments', function ($join) 
            {
                $deptid = Session::get("dept_id");
                $join->on('cc_id', '=', 'ccd_cc_id')
                     ->where('ccd_dep_id', '=', $deptid);
            })
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num","ccd_requirements"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function department_personnel_record($id)
    {        
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("ccd_status" , 2)
        ->where("ccd_assigned_id" ,  Session::get("employee_id") )
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->join('clearancecertificationdepartments', function ($join) 
            {
                $deptid = Session::get("dept_id");
                $join->on('cc_id', '=', 'ccd_cc_id')
                     ->where('ccd_dep_id', '=', $deptid);
            })
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num","ccd_requirements","ccd_others"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function department_review_record($id)
    {        
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("ccd_status" , 3)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->join('clearancecertificationdepartments', function ($join) 
            {
                $deptid = Session::get("dept_id");
                $join->on('cc_id', '=', 'ccd_cc_id')
                     ->where('ccd_dep_id', '=', $deptid);
            })
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num","ccd_requirements","ccd_others"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function resignee_head_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_resignee_status" , 0)
        ->where("cc_dept_head" , Session::get("employee_id") )
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function resignee_supervisor_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_resignee_status" , 2)
        ->where("cc_resignee_assigned" , Session::get("employee_id") )
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function resignee_review_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_resignee_status" , 3)
        ->where("cc_dept_head" , Session::get("employee_id") )
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function resignee_record($id)
    {
        $loguser = Session::get("employee_id");
        $resignee = ClearanceCertification::where("cc_id" , $id)->where("cc_status",2)
        ->where("cc_emp_id" , $loguser)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function returned_record($id)
    {
        $resignee = ClearanceCertification::where("cc_id" , $id)->wherein("cc_status",[3,0,2,4])
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num"]);

       if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }






    
    public static function citd_technical_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_technical_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_internet",
            "cc_revoke_intranet","cc_revoke_internal","cc_revoke_external","cc_remove_sms","cc_pc_laptop"
            ,"cc_other_it_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function citd_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_technical_status" , 2)
        ->where("cc_technical_assigned" , Session::get("employee_id") )
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_internet",
            "cc_revoke_intranet","cc_revoke_internal","cc_revoke_external","cc_remove_sms","cc_pc_laptop"
            ,"cc_other_it_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function citd_techreview_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_technical_status" , 3)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_internet",
            "cc_revoke_intranet","cc_revoke_internal","cc_revoke_external","cc_remove_sms","cc_pc_laptop"
            ,"cc_other_it_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function citd_headreview_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_technical_status" , 4)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_internet",
            "cc_revoke_intranet","cc_revoke_internal","cc_revoke_external","cc_remove_sms","cc_pc_laptop"
            ,"cc_other_it_deliverables","cc_resignee_status","cc_dept_head", "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }


    
    public static function smdd_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_smd_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_rgas",
            "cc_other_smd_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function smdd_head_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_smd_status" , 2)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_rgas",
            "cc_other_smd_deliverables","cc_resignee_status","cc_dept_head", "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function erpd_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_erp_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_sap",
            "cc_other_erp_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function erpd_head_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_erp_status" , 2)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_revoke_sap",
            "cc_other_erp_deliverables","cc_resignee_status","cc_dept_head", "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function srvv_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_ssrv_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_unpaid_vale",
            "cc_other_ssrv_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function srvv_head_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_ssrv_status" , 2)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_unpaid_vale",
            "cc_other_ssrv_deliverables","cc_resignee_status","cc_dept_head", "cc_manual_surrendered",
            "cc_workfiles","cc_drawer","cc_fixed_assets","cc_office_supplies","cc_accountableforms","cc_revolvingfund"
            ,"cc_companycar","cc_other_resignee_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function training1_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_training1_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_conducted_exit",
            "cc_other_training1_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function cbrpersonnel1_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_cb_personnel1_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_hmo_card",
            "cc_hmo_cancellation","cc_id_card","cc_building_access","cc_other_cb_personnel1_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }


    
    public static function recruitment_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_recruitment_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_biometrics",
            "cc_other_recruitment"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }


    
    public static function training2_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_training2_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_service_aggreement",
            "cc_training_materials","cc_training_equipment","cc_other_training2_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }

    
    public static function cbrpersonnel2_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_cb_personnel2_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_outstanding_sss",
            "cc_outstanding_aub","cc_outstanding_hmo","cc_other_cb_personnel2_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }


    
    public static function cbrmanager_staff_record($id)
    {
        
        $resignee = ClearanceCertification::where("cc_id" , $id)
        ->wherein("cc_status",[5,4])
        ->where("cc_cb_manager_status" , 0)
        ->join('departments','cc_emp_departmentid','=','departments.id' )
        ->join('sections','cc_emp_sectionid','=','sections.id' ,"left")
        ->get(["sect_name","dept_name","cc_emp_id" , "cc_div_code","cc_position","cc_division_head","cc_dept_head",
            "cc_immediate_head","cc_date_hired","cc_resignation_date","cc_ref_num" , "cc_staff_employee",
            "cc_other_cb_manager_deliverables"]);

        if($resignee->isEmpty()) return Redirect::to('/');

        $data = ClearanceCertification::record($resignee);

        return $data;
    }



    
    public static function clearance_department($id)
    {
        $dept = DB::table("clearancecertificationdepartments")
            ->where("ccd_cc_id",$id)
            ->orderby("ccd_dep_id","asc")
            ->get(["ccd_dep_id","ccd_requirements"]);

        return $dept;
    }

    
    public static function update_chrd_status($id,$field)
    {
        $stats = ClearanceCertification::where("cc_id" , $id)
                ->where("cc_training1_status",2)
                ->where("cc_cb_personnel1_status",2)
                ->where("cc_recruitment_status",2)
                ->where("cc_training2_status",2)
                ->where("cc_cb_personnel2_status",2)
                ->where("cc_cb_manager_status",2)
                ->update(["cc_chrd_status"=>2]);

        $emp = Employees::where("code","CC_CHRD_HEAD")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email","receivers.employeeid"])->toArray();

        //INSERT AUDIT
        $attach_audit = new CC\CC; 
        $old = [$field=>Session::get("employee_id")];
        $new = [$field=>$emp[0]["employeeid"]];
        $attach_audit->setTable('clearancecertification');
        $attach_audit->setPrimaryKey('cc_id');
        $attach_audit->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

        if($stats)
        {
            //INSERT LOGS
            ClearanceCertification::insert_logs($id,4, $emp[0]["employeeid"]);  

            //SEND EMAIL NOTIFICATION
            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Assesment");
        }
    }

    
    public static function update_clearance_status($id,$field=null)
    {
        $stats = ClearanceCertification::where("cc_id" , $id)
                ->where("cc_others_status",1)
                ->where("cc_resignee_status",1)
                ->where("cc_technical_status",1)
                ->where("cc_smd_status",1)
                ->where("cc_erp_status",1)
                ->where("cc_ssrv_status",1)
                ->where("cc_training1_status",1)
                ->where("cc_cb_personnel1_status",1)
                ->where("cc_recruitment_status",1)
                ->where("cc_training2_status",1)
                ->where("cc_cb_personnel2_status",1)
                ->where("cc_cb_manager_status",1)
                ->update(["cc_status"=>5]);

        $emp = Employees::where("code","CC_CSDH_HEAD")
                ->join("receivers","employees.id","=", "receivers.employeeid")
                ->get(["firstname","email","receivers.employeeid"])->toArray();

        //INSERT AUDIT
        $attach_audit = new CC\CC; 
        $old = [$field=>Session::get("employee_id")];
        $new = [$field=>$emp[0]["employeeid"]];
        $attach_audit->setTable('clearancecertification');
        $attach_audit->setPrimaryKey('cc_id');
        if($field) $attach_audit->AU004($id , json_encode($old),  json_encode($new) , Input::get("reference") );

        if($stats)
        {
            //INSERT LOGS
            ClearanceCertification::insert_logs($id,5,$emp[0]["employeeid"]);

            //SEND EMAIL NOTIFICATION
            ClearanceCertification::send_email($emp[0]["firstname"],$emp[0]["email"],"Approval");  
        }

        return $stats;
    }

    

    public static function update_others()
    {
        $others= array();
        if(isset($_POST['requirement'])):
            $other_app = Input::get("requirement_app");
            $other_date = Input::get("requirement_date");
            $other_remarks = Input::get("requirement_remarks");
            foreach (Input::get("requirement") as $key => $req) {
                if(trim($req))
                {                   
                    $others[] = array(
                            'requirement'=>$req,
                            'applicable'=>isset($other_app[$key]) ? $other_app[$key] : "",
                            'date'=>$other_date[$key],
                            'remarks'=>$other_remarks[$key]
                        );
                }
            }
        endif;

        return $others;
    }

    

    public static function update_others_amount()
    {
        $others= array();
        if(isset($_POST['requirement'])):
            $other_app = Input::get("requirement_app");
            $other_date = Input::get("requirement_date");
            $other_amount = Input::get("requirement_amount");
            $other_remarks = Input::get("requirement_remarks");
            foreach (Input::get("requirement") as $key => $req) {
                if(trim($req))
                {                   
                    $others[] = array(
                            'requirement'=>$req,
                            'applicable'=> isset($other_app[$key]) ? $other_app[$key] : "",
                            'date'=>$other_date[$key],
                            'amount'=>$other_amount[$key],
                            'remarks'=>$other_remarks[$key]
                        );
                }
            }
        endif;

        return $others;
    }

    

    public static function update_department()
    {
        $date = Input::get("date");
        $applicable = Input::get("availability");
        $amount = Input::get("amount");
        $remarks = Input::get("remarks");
        $chrd_remarks = Input::get("other_remarks");
        foreach(Input::get("employeeRequirement") as $key=>$req)
        {
            $data["requirements"][$key]["employee"] = $req;
            $data["requirements"][$key]["applicable"] = isset($applicable[$key]) ? $applicable[$key] : "";
            $data["requirements"][$key]["date"] = $date[$key];
            $data["requirements"][$key]["amount"] = $amount[$key];
            $data["requirements"][$key]["remarks"] = $remarks[$key];
            $data["requirements"][$key]["chrd_remarks"] = $chrd_remarks[$key];
        }
        

        $data["others"] = ClearanceCertification::update_others_amount();

        return $data;
    }

    

    public static function update_resignee()
    {
        $data["manual"]["applicable"] = Input::get("manual_surrender");
        $data["manual"]["date"] = Input::get("manual_surrender_date");
        $data["manual"]["remarks"] = Input::get("manual_surrender_remarks");

        $data["workfiles"]["applicable"] = Input::get("work_files");
        $data["workfiles"]["date"] = Input::get("work_files_date");
        $data["workfiles"]["remarks"] = Input::get("work_files_remarks");

        $data["drawer"]["applicable"] = Input::get("drawer");
        $data["drawer"]["date"] = Input::get("drawer_date");
        $data["drawer"]["remarks"] = Input::get("drawer_remarks");

        $data["assets"]["applicable"] = Input::get("assets");
        $data["assets"]["date"] = Input::get("assets_date");
        $data["assets"]["remarks"] = Input::get("assets_remarks");

        $data["office"]["applicable"] = Input::get("supplies");
        $data["office"]["date"] = Input::get("supplies_date");
        $data["office"]["remarks"] = Input::get("supplies_remarks");

        $data["accountable"]["applicable"] = Input::get("accountable");
        $data["accountable"]["date"] = Input::get("accountable_date");
        $data["accountable"]["remarks"] = Input::get("accountable_remarks");

        $data["revolving"]["applicable"] = Input::get("revolving");
        $data["revolving"]["date"] = Input::get("revolving_date");
        $data["revolving"]["remarks"] = Input::get("revolving_remarks");

        $data["car"]["applicable"] = Input::get("car");
        $data["car"]["date"] = Input::get("car_date");
        $data["car"]["remarks"] = Input::get("car_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_citd()
    {
        $data["internet"]["applicable"] = Input::get("internet");
        $data["internet"]["date"] = Input::get("internet_date");
        $data["internet"]["remarks"] = Input::get("internet_remarks");
        $data["internet"]["amount"] = Input::get("internet_amount");

        $data["intranet"]["applicable"] = Input::get("intranet");
        $data["intranet"]["date"] = Input::get("intranet_date");
        $data["intranet"]["remarks"] = Input::get("intranet_remarks");
        $data["intranet"]["amount"] = Input::get("intranet_amount");

        $data["internal"]["applicable"] = Input::get("internal");
        $data["internal"]["date"] = Input::get("internal_date");
        $data["internal"]["remarks"] = Input::get("internal_remarks");
        $data["internal"]["amount"] = Input::get("internal_amount");

        $data["external"]["applicable"] = Input::get("external");
        $data["external"]["date"] = Input::get("external_date");
        $data["external"]["remarks"] = Input::get("external_remarks");
        $data["external"]["amount"] = Input::get("external_amount");

        $data["sms"]["applicable"] = Input::get("sms");
        $data["sms"]["date"] = Input::get("sms_date");
        $data["sms"]["remarks"] = Input::get("sms_remarks");
        $data["sms"]["amount"] = Input::get("sms_amount");

        $data["laptop"]["applicable"] = Input::get("laptop");
        $data["laptop"]["date"] = Input::get("laptop_date");
        $data["laptop"]["remarks"] = Input::get("laptop_remarks");
        $data["laptop"]["amount"] = Input::get("laptop_amount");

        $data["others"] = ClearanceCertification::update_others_amount();

        return $data;
    }

    

    public static function update_erpd()
    {
        $data["sap"]["applicable"] = Input::get("sap");
        $data["sap"]["date"] = Input::get("sap_date");
        $data["sap"]["remarks"] = Input::get("sap_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_smdd()
    {
        $data["rgas"]["applicable"] = Input::get("rgas");
        $data["rgas"]["date"] = Input::get("rgas_date");
        $data["rgas"]["remarks"] = Input::get("rgas_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_ssrv()
    {
        $data["vale"]["applicable"] = Input::get("vale");
        $data["vale"]["date"] = Input::get("vale_date");
        $data["vale"]["amount"] = Input::get("vale_amount");
        $data["vale"]["remarks"] = Input::get("vale_remarks");

        $data["others"] = ClearanceCertification::update_others_amount();

        return $data;
    }

    

    public static function update_cb1()
    {
        $data["hmo_surrender"]["applicable"] = Input::get("hmo_surrender");
        $data["hmo_surrender"]["date"] = Input::get("hmo_surrender_date");
        $data["hmo_surrender"]["remarks"] = Input::get("hmo_surrender_remarks");

        $data["hmo_cancel"]["applicable"] = Input::get("hmo_cancel");
        $data["hmo_cancel"]["date"] = Input::get("hmo_cancel_date");
        $data["hmo_cancel"]["remarks"] = Input::get("hmo_cancel_remarks");

        $data["card"]["applicable"] = Input::get("card");
        $data["card"]["date"] = Input::get("card_date");
        $data["card"]["remarks"] = Input::get("card_remarks");

        $data["building"]["applicable"] = Input::get("building");
        $data["building"]["date"] = Input::get("building_date");
        $data["building"]["remarks"] = Input::get("building_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_cb2()
    {
        $data["sss"]["applicable"] = Input::get("sss");
        $data["sss"]["date"] = Input::get("sss_date");
        $data["sss"]["remarks"] = Input::get("sss_remarks");
        $data["sss"]["amount"] = Input::get("sss_amount");

        $data["aub"]["applicable"] = Input::get("aub");
        $data["aub"]["date"] = Input::get("aub_date");
        $data["aub"]["remarks"] = Input::get("aub_remarks");
        $data["aub"]["amount"] = Input::get("aub_amount");

        $data["hmo"]["applicable"] = Input::get("hmo");
        $data["hmo"]["date"] = Input::get("hmo_date");
        $data["hmo"]["remarks"] = Input::get("hmo_remarks");
        $data["hmo"]["amount"] = Input::get("hmo_amount");

        $data["others"] = ClearanceCertification::update_others_amount();

        return $data;
    }

    

    public static function update_training1()
    {
        $data["exit"]["applicable"] = Input::get("exit");
        $data["exit"]["date"] = Input::get("exit_date");
        $data["exit"]["remarks"] = Input::get("exit_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_training2()
    {
        $data["service"]["applicable"] = Input::get("service");
        $data["service"]["date"] = Input::get("service_date");
        $data["service"]["remarks"] = Input::get("service_remarks");

        $data["material"]["applicable"] = Input::get("material");
        $data["material"]["date"] = Input::get("material_date");
        $data["material"]["remarks"] = Input::get("material_remarks");

        $data["equipment"]["applicable"] = Input::get("equipment");
        $data["equipment"]["date"] = Input::get("equipment_date");
        $data["equipment"]["remarks"] = Input::get("equipment_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function update_cbmanager()
    {
        $data["staff"]["applicable"] = Input::get("staff");
        $data["staff"]["date"] = Input::get("staff_date");
        $data["staff"]["remarks"] = Input::get("staff_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

     

    public static function update_recruitment()
    {
        $data["biometrics"]["applicable"] = Input::get("biometrics");
        $data["biometrics"]["date"] = Input::get("biometrics_date");
        $data["biometrics"]["remarks"] = Input::get("biometrics_remarks");

        $data["others"] = ClearanceCertification::update_others();

        return $data;
    }

    

    public static function insert_comment($id , $dept , $pos ,$stats,  $commentid , $name="comment")
    {
        $comment["ccc_comment"] = ($stats ?  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) 
                          . " " 
                          . date("m/d/Y h:i A") 
                          . ": " : "")
                          . Input::get( $name );
        $comment["ccc_status"] = $stats;

        if($commentid)
        {
             DB::table("clearancecertificationcomments")
                ->where("ccc_id",$commentid)
                ->where("ccc_cc_id",$id)
                ->where("ccc_position",$pos)
                ->where("ccc_department",$dept)
                ->update($comment);
        }
        else
        {
            $comment["ccc_cc_id"] = $id;
            $comment["ccc_department"] = $dept;
            $comment["ccc_position"] = $pos;            

            DB::table("clearancecertificationcomments")->insert($comment);
        }
    }


    

    public static function insert_attachment($id , $dept , $pos ,$ref=null)
    {
        //ADD ATTACHMENT
        $files = Input::get("files");
        $attach = array();
        $fm = new Libraries\FileManager;
        $reference = $ref ? $ref : Input::get("reference");
        if(!empty($files)):
            foreach($files as $file)
            {
                $fm->move($file['random_filename'], Config::get('rgas.rgas_storage_path'). 'cc/' .  $reference );            
                $attach[] = array(
                        "cca_cc_id" => $id,
                        "cca_attachments" => json_encode($file),
                        "cca_department" => $dept ,
                        "cca_position" => $pos,
                        "cca_eid" => Session::get("employee_id")
                    );

                $attach_audit = new CC\CC; 

                $attach_audit->setTable('clearancecertificationattachments');
                $attach_audit->setPrimaryKey('cca_cc_id');
                $attach_audit->AU002($id , $file["original_filename"] , Input::get("reference") );

            }

            DB::table("clearancecertificationattachments")->insert( $attach ); 
        endif;

        
    }

    

    public static function delete_attachment($id)
    {
        $attachment = Input::get("attachment_id");
        $name = Input::get("attachment_name");
        if(!empty($attachment))
        {
            DB::table("clearancecertificationattachments")
                ->where("cca_cc_id" , $id)
                ->wherein("cca_id",$attachment)
                ->delete(); 

            $attach_audit = new CC\CC; 
            foreach($attachment as $index=>$file)
            {
                $attach_audit->setTable('clearancecertificationattachments');
                $attach_audit->setPrimaryKey('cca_cc_id');
                $attach_audit->AU005($id , $name[$index] , Input::get("reference") );
            }
        }
    }

    

    public static function insert_logs($id , $status , $assign,$time=null)
    {
        $logs["ccl_cc_id"] = $id;
        $logs["ccl_date_received"] = $time ? $time : date("Y-m-d H:i:s");
        $logs["ccl_status"] = $status;
        $logs["ccl_assigned"] = $assign;
        //$logs["ccl_remarks"] = Input::get( "comment" ) . (is_array(Input::get( "remarks" )) ? "" : Input::get( "remarks" ));
        DB::table("clearancecertificationlogs")->insert( $logs );
    }

    

    public static function update_logs($id )
    {
        $upd["ccl_date_complete"] = date("Y-m-d");
        $upd["ccl_remarks"] = Input::get( "comment" ) . (is_array(Input::get( "remarks" )) ? "" : Input::get( "remarks" ));

        DB::table("clearancecertificationlogs")
            ->where("ccl_cc_id" , $id)
            ->where("ccl_assigned", Session::get("employee_id"))
            ->whereNull("ccl_date_complete")
            ->update($upd);
    }

    

    public static function get_reference_no()
    {
        $year = date("Y");
        $ref = ClearanceCertification::whereRaw("SUBSTR(cc_ref_num, 1, 4) = ?" , [$year])
            ->orderby("cc_ref_num","desc")
            ->limit(1)
            ->get(["cc_ref_num"])->toArray();

       $no = isset($ref[0]["cc_ref_num"]) ? $ref[0]["cc_ref_num"] : "$year-00000";   
       $num = sprintf("%'.05d", (int)substr($no,5) + 1);

       return "$year-$num";
    }

    

    public static function send_email($name,$email , $status=null)
    {   
        Mail::send('cc.email.status', array(
                    'status' =>  strtoupper($status),
                    'reference' => Input::get("reference"),
                    'name' => Input::get("name"),
                    'department' => Input::get("department"),
                    'effective' => Input::get("effective_date")),
                    function($message ) use ($name, $email, $status)
                    {
                        $message->to( $email ,  $name )
                        ->subject("RGAS Notification Alert: Clearance Certification $status");
                    }
                );
    }


    
}