<?php

use Carbon\Carbon;

class Ntem extends \Eloquent {

    
    protected $table = "nte";

    protected $primaryKey = "nte_id";
    protected $hidden = array("password");

    public $timestamps = false;

    public static function _pullNteAll($where_condition, $limit)
    {
        if (Cache::has("nte_list") )
        {
            $nte_list = Cache::get("crr_lists_all");
        }
        else
        {
            $nte_list = Ntem::whereRaw("{$where_condition}")->leftJoin("employees as emp", function($join){
                
                $join->on("emp.id", "=", "nte.nte_employee_id");
                
            })->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "nte.nte_department_id");
                
            })->leftJoin("crrbuildup as infraction", function($join){
                
                $join->on("infraction.cb_crr_id", "=", "nte.cb_crr_id");
                
            })->leftJoin("nte_rdas as rdas", function($join){
                
                $join->on("rdas.id", "=", "nte.nte_recommended_disciplinary_action");
                
            })->leftJoin("nte_status as status", function($join){
                
                $join->on("status.id", "=", "nte.nte_status");
                
            })->leftJoin("nte_status as stats", function($join){
                
                $join->on("stats.id", "=", "nte.nda_status");
                
            })->orderBy("nte_reference_number", "desc");

            if ($limit)
                $nte_list = $nte_list->limit(5);

            $nte_list = $nte_list->get([
                 "nte.nte_id"
                ,"nte.nte_reference_number"
                ,"nte.nda_reference_number"
                ,"emp.firstname"
                ,"emp.middlename"
                ,"emp.lastname"
                ,"dept.dept_name as Department"
                ,"nte.nte_month"
                ,"nte.nte_day"
                ,"nte.nte_year"
                ,"infraction.cb_rule_name"
                ,"rdas.name as Recommended"
                ,"status.name as nte_status"
                ,"stats.name as nda_status"
                ,"status.id as Stat"
                ,"stats.id as NdaStat"
                ,"nte.nte_filer_id"
                ,"nte.nda_filer_id"
                ,"nte.nte_return_filer_id"
                ,"nte.nte_current_id"
                ,"nte.nte_employee_id"
                ,"nte.nte_department_id"
                ,"nte.nte_disciplinary_action"
            ]);
             
            Cache::forever("crr_lists_all", $nte_list);
        }

        return $nte_list;   
    }

    public static function _pullNteAllApproval($limit)
    {
        if (Cache::has("nte_list") )
        {
            $nte_list = Cache::get("crr_lists_all");
        }
        else
        {
            $nte_list = Ntem::selectRaw("DISTINCT(CONCAT(nte_department_id,nte_month,nte_year,nte_filer_id)), dept_name, count(*) as count, nte_month, nte_year, nte.nte_department_id, rec.value as ftype")->leftJoin("departments as dept", function($join){
                
                $join->on("dept.id", "=", "nte.nte_department_id");
                
            })->leftJoin("receivers as rec", function($join){
                
                $join->on("rec.employeeid", "=", "nte.nte_filer_id");
                
            })->orderBy("nte_year", "desc")->orderBy("nte_month", "desc")->orderBy("dept.dept_name", "asc")->where(['rec.value' => (Session::get('is_chrd_aer_staff') - 1), 'nte.nte_status' => 2, 'nte.is_deleted' => 0])->groupBy(array('nte_department_id', 'nte_month', 'nte_year'));

            if ($limit)
                $nte_list = $nte_list->limit(5);

            $nte_list = $nte_list->get();
             
            Cache::forever("crr_lists_all", $nte_list);
        }
        
        return $nte_list;
    }

    public static function _pullNteAllNotation()
    {
        $yesterday = Carbon::yesterday();
        $now = Carbon::now();

        $diff = $now->diffInHours(Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 00:00'));
        
        if ($diff < 12) {
            $start = Carbon::createFromFormat('Y-m-d H:i', $yesterday->year.'-'.$yesterday->month.'-'.$yesterday->day.' 12:00')->toDateTimeString();
            $end = Carbon::createFromFormat('Y-m-d H:i', $yesterday->year.'-'.$yesterday->month.'-'.$yesterday->day.' 23:59')->toDateTimeString();
        }
        else
        {
            $start = Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 00:00')->toDateTimeString();
            $end = Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 11:59')->toDateTimeString();
        }

        $nte_list = Ntem::selectRaw("DISTINCT(CONCAT(nte.nte_department_id,nte_month,nte_year,nte_filer_id)), dept_name, count(*) as count, nte.nte_department_id, nte.nte_filer_id, (SELECT r.employeeid FROM receivers as r WHERE r.value = (rec.value+1)) as approver, rec.value as ftype")->leftJoin("departments as dept", function($join){
            
            $join->on("dept.id", "=", "nte.nte_department_id");
            
        })->leftJoin("receivers as rec", function($join){
            
            $join->on("rec.employeeid", "=", "nte.nte_filer_id");
            
        })->orderBy("nte_month", "asc")->where(['nte.nte_status' => 2, 'is_deleted' => 0])->whereBetween('nte_submitted_chrd_aer', array($start, $end))->where('is_deleted', 0)->groupBy(array('nte.nte_department_id', 'nte_month', 'nte_year'))->get();
             
        return $nte_list;
    }

    public static function _pullNdaAllCHRD($type)
    {
        $yesterday = Carbon::yesterday();
        $now = Carbon::now();

        if ($type == 'head')
        {
            $start = Carbon::createFromFormat('Y-m-d H:i', $yesterday->year.'-'.$yesterday->month.'-'.$yesterday->day.' 12:00')->toDateTimeString();
            $end = Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 11:59')->toDateTimeString();
            $status = 12;
        }
        elseif ($type == 'manager') 
        {
            $start = Carbon::createFromFormat('Y-m-d H:i', $yesterday->year.'-'.$yesterday->month.'-'.$yesterday->day.' 09:00')->toDateTimeString();
            $end = Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 08:59')->toDateTimeString();
            $status = 10;
        }

        $nte_list = Ntem::selectRaw("DISTINCT(CONCAT(nte_department_id,nte_month,nte_year,nte_filer_id)), dept_name, count(*) as count, nte.nte_id, nte.nte_department_id, nte.nte_filer_id, (SELECT r.employeeid FROM receivers as r WHERE r.value = (rec.value+1)) as approver, rec.value as ftype")->leftJoin("departments as dept", function($join){
            
            $join->on("dept.id", "=", "nte.nte_department_id");
            
        })->leftJoin("receivers as rec", function($join){
            
            $join->on("rec.employeeid", "=", "nte.nte_filer_id");
            
        })->orderBy("nte_month", "asc")->where(['nte.nte_status' => $status, 'nte.is_deleted' => 0])->whereBetween('nte_submitted_chrd', array($start, $end))->groupBy(array('nte_department_id', 'nte_month', 'nte_year'))->get();
             
        return $nte_list;
    }

    public static function _pullNdaDepartmentsNotation()
    {
        $yesterday = Carbon::yesterday();
        $now = Carbon::now();

        $start = Carbon::createFromFormat('Y-m-d H:i', $yesterday->year.'-'.$yesterday->month.'-'.$yesterday->day.' 15:00')->toDateTimeString();
        $end = Carbon::createFromFormat('Y-m-d H:i', $now->year.'-'.$now->month.'-'.$now->day.' 14:59')->toDateTimeString();

        $data = Ntem::select('nte.nte_employee_id', 'nte.nte_department_id', DB::raw("CONCAT(IFNULL(emp.firstname, ''), ' ', IFNULL(emp.middlename, ''), ' ', IFNULL(emp.lastname, '')) name"))
        ->where(['nte.status' => 13, 'nte.is_deleted' => 0])
        ->leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "nte.nte_employee_id");
            
        })->get();

        return $data;
    }

    public static function _pullNteAccomplishmentByNteId($nte_id)
    {
        $data = Ntem::select('nte.nte_id', 'nte.nte_reference_number', 'nte.nte_status', 'stat.name as status_name', 'nte.nte_filer_id','nte.nte_department_id','nte.cb_crr_id', 'crr.cb_rule_name', 'nte.nte_employee_id', 'emp.email',DB::raw("CONCAT(IFNULL(emp.firstname, ''), ' ', IFNULL(emp.middlename, ''), ' ', IFNULL(emp.lastname, '')) name"))->where(['nte_id' => $nte_id, 'is_deleted' => 0])->leftJoin("nte_status as stat", function($join){
            
            $join->on("stat.id", "=", "nte.nte_status");
            
        })->leftJoin("crrbuildup as crr", function($join){
            
            $join->on("crr.cb_crr_id", "=", "nte.cb_crr_id");
            
        })->leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "nte.nte_employee_id");
            
        })->first();

        return $data;
    }
}