<?php
use app\RGAS;

class TMSHoliday extends \Eloquent 
{
    protected $table = 'TMS_Holiday';
    public $timestamps = false;
  	protected $connection = 'sqlsrv';


	public static function gethol($start , $end )
	{
		    $requests = TMSHoliday::whereBetween('Hol_Date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end)) ])
        ->where("Comp_Id","RBC")
        ->whereRaw("[TMS_Holiday].Hol_BranchID = (SELECT TOP 1 Branch_ID FROM RBC_HRMS.DBO.empjob_Info as a WHERE a.Employee_ID = '" . Session::get("new_employeeid") . "' and a.Current_Job ='Y') and 
                [TMS_Holiday].TMS_Group = (SELECT TOP 1 TMS_Group FROM TMS_Employee WHERE TMS_Employee.Employee_ID = '" . Session::get("new_employeeid") . "') 
               ORDER BY [TMS_Holiday].Hol_WithPay ASC ")
        ->distinct()
        ->get(["Hol_Description","Hol_Date","Hol_Type","Hol_WithPay"]);
        $hol = array();
        foreach($requests as $req) 
        {				
            $date = date("m/d/Y", strtotime($req->Hol_Date));
            if(in_array( trim($req->Hol_Type)  , ["LGRST_HR","LGH_HR"]))
                $hol["holiday"][] = $date .  " " . $req->Hol_Description;

            else if(in_array( trim($req->Hol_Type) , ["SH_HR","SRSP_HR","SRST_HR"]))
                $hol["special"][] = $date .  " " . $req->Hol_Description;

            else 
                $hol["suspension"][] = $date .  " " . $req->Hol_Description;
        }

        return $hol;
	}	




}