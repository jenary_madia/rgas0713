<?php


class CrrBuildUp extends \Eloquent {

    
    protected $table = "crrbuildup";

    protected $primaryKey = "cb_crr_id";
    protected $hidden = array("password");

    public $timestamps = false;


    public static function get_crrs()
    {
        if (Cache::has("crr_lists") )
        {
            $crr_lists = Cache::get("crr_lists");
        }
        else
        {
            $crr_lists = CrrBuildUp::where("cb_rule_status", "=", "1")->get([
                 "cb_crr_id"
                ,"cb_rule_name"
                ,"cb_rule_number"
                ,"cb_rule_description"
                ,"cb_rule_status"
            ]);
             
            Cache::forever("crr_lists", $crr_lists);
        }
        
        return $crr_lists;
    }
    
    public static function get_all_crrs($where_condition)
    {
        
        if (Cache::has("crr_lists") )
        {
            $crr_lists = Cache::get("crr_lists_all");
        }
        else
        {
            /*$crr_lists = CrrBuildUp::whereRaw("{$where_condition}")->orderBy("cb_rule_name", "asc")->get([
                 "crrbuildup.cb_crr_id"
                ,"crrbuildup.cb_rule_name"
                ,"crrbuildup.cb_rule_number"
                ,"crrbuildup.cb_rule_description"
                ,"crrbuildup.cb_rule_status"
            ]);*/

            $crr_lists = DB::select(DB::raw('SELECT * FROM (SELECT *, IF (cb_rule_status = 1, "ACTIVE", "INACTIVE") as status_name FROM `crrbuildup`) as crrbuildup WHERE' . $where_condition));
             
            Cache::forever("crr_lists_all", $crr_lists);
        }
        
        return $crr_lists;
    }
}
/* End of file */