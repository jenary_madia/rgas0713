<?php

class Notifications extends \Eloquent {

    protected $table = 'notifications';
    public $incrementing = true;
    public $guarded =  array('id');
    public $timestamps = false;

    public function generateCodeNumber()
    {
        $documentCodeToday = date('Y-m');
        $latestDocumentCode = Notifications::max('documentcode');
        if ($latestDocumentCode >= $documentCodeToday)
        {
            $latestCodenumber = Notifications::where('documentcode','=',$latestDocumentCode)->max('codenumber');
            if (! $latestCodenumber) {
                return '00000';
            }else{
                return str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
            }
        }else{

            return str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

        }

    }

    public function myNotifications()
    {
        $requests = Notifications::with("employee")
            ->where('ownerid',Session::get("employee_id"))
            ->where("isdeleted",0)
            ->with("notifName")
//            ->forMyNotifications()
            ->get();
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
        $result_data["iTotalRecords"] = count($requests);
        if ( count($requests) > 0){
            $ctr = 0;
            foreach($requests as $req) {
                $deletableNotifications = (in_array($req['status'], json_decode(DELETABLE,true)));
                $editableNotifications = (in_array($req['status'], json_decode(EDITABLE,true)));
                $btnDelete = "<button ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' name='action' value='softDelete|{$req->id}'>Delete</button>";
                $btnView = "<a class='btn btn-default btndefault' href=".url('/ns/my_notifications/'.$req->id.'/view').">View</a>";
                $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' href=".url('/ns/my_notifications/'.$req->id.'/edit').">Edit</a>";
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = (in_array($req['noti_type'],['offset','official'])? ($req['duration_type'] == 'multiple' ? $req['from_date'].' to '.$req['to_date'] : $req['from_date']) : (($req['noti_type'] == "cut_time") ? $req['from_date'].' to '.$req['to_date'] : $req['from_date']));
                $result_data["aaData"][$ctr][] = $req['notifName']['text'];
                $result_data["aaData"][$ctr][] = (strlen($req['reason']) >= 100 ? substr($req['reason'],0,100) : $req['reason']);
                $result_data["aaData"][$ctr][] = $req['status'];
                $result_data["aaData"][$ctr][] = ($req['employee ']? $req['employee']['firstname']." ".$req['employee']['lastname'] : "");
                $result_data["aaData"][$ctr][] = array($btnDelete.' '.$btnView.' '.$btnEdit);
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $requests;
        }
        return $result_data;
    }

    public function scopeForMyNotifications($query)
    {
//        if (Input::get('sSearch'))
//        {
//            $skey = Input::get('sSearch');
//            return $query->whereRaw("(reason like '%".$skey."%' or datecreated like '%".$skey."%' or concat(documentcode,'-',codenumber) like '%".$skey."%' or status like '%".$skey."%')");
//        }
    }

    /*--------------------------for data table of "for submitted notifications to Receiver"-----------------------------*/
    public function getNotif()
    {
        $forApproveNotifications = Notifications::whereIn('status', ['APPROVED','APPROVED/DELETED'])
            ->where('curr_emp', Session::get('employee_id'))
            ->getNotif()
            ->get();
        return $forApproveNotifications;
    }

    public function scopeGetNotif($query)
    {
        if (Session::has("department")) {
            if (! Session::get("department") == 'all') {
                $query->where("department",Session::get("department"));
            }
        }

        if (Session::has("lastName")) {
            $query->where("lastname",Session::get("lastName"));
        }

        if (Session::has("firstName")) {
            $query->where("firstname",Session::get("firstName"));
        }

        if (Session::has("middleName")) {
            $query->where("middlename",Session::get("middleName"));
        }
    }


    /*--------------------------for data table of "for approve notifications"-----------------------------*/
    public function superiorNotifications($usage = null)
    {
        $forApproveNotifications = Notifications::forApproveNotificationSearch()
            ->where('curr_emp', Session::get('employee_id'))
            ->where('status', 'FOR APPROVAL')
            ->with("notifName")
            ->get();
        $result_data["iTotalDisplayRecords"] = count($forApproveNotifications); //total count filtered query
        $result_data["iTotalRecords"] = count($forApproveNotifications);
        if ( count($forApproveNotifications) > 0){
            $ctr = 0;
            foreach($forApproveNotifications as $req) {
                if($usage) {
                    $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                }else{
                    $result_data["aaData"][$ctr][] = '<input type="checkbox"  name="chosenNotifs[]" value="'.$req['id'].'" class="chosenSuperiorNotification"> '.$req['documentcode'].'-'.$req['codenumber'];
                }
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['notifName']['text'];
                $result_data["aaData"][$ctr][] = (in_array($req['noti_type'],['offset','official'])? ($req['duration_type'] == 'multiple' ? $req['from_date'].' to '.$req['to_date'] : $req['from_date']) : (($req['noti_type'] == "cut_time") ? $req['from_date'].' to '.$req['to_date'] : $req['from_date']));
                $result_data["aaData"][$ctr][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                $result_data["aaData"][$ctr][] = $req->status;
                $result_data["aaData"][$ctr][] = $req->firstname.' '.$req->lastname;
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/ns/for_approval/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/ns/for_approval/'.$req->id.'/approve').">Approve</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $forApproveNotifications;
        }
        return $result_data;
    }

    public function scopeForApproveNotificationSearch($query)
    {
        if (Input::get('sSearch'))
        {
            return $query->whereRaw('concat(firstname," ",middlename," ",lastname) like "%'.Input::get('sSearch').'%" ');
        }
    }



    /*--------------------Getting specific Leave for-------------------*/

    public static function getNotifications($id,$purpose)
    {
        /*
            PURPOSE
            1 = myleaves
            2 = forApproveLeaves
        */
        $result = Notifications::purpose($purpose)
            ->with('notifName')
            ->with('toCorrect')
            ->where("id",$id)->first();
        if (!$result) {
            return 0;
        }

        return $result;

    }


    public function scopePurpose($query,$purpose)
    {
        if ($purpose == 1)
        {
            return $query->where("ownerid", Session::get("employee_id"));
        }
        return $query->where("curr_emp", Session::get("employee_id"));
    }


    /****************************FILTER SEARCH FOR RECEIVER************************************/

    public function filterNotifForReceiver() {
        $notifications = Notifications::where('curr_emp', Session::get('employee_id'))
            ->whereIn('status', ['APPROVED','APPROVED/DELETED'])
            ->forReceiver()
            ->get();
        $notifData = [
            "cut_time" => [],
            "TKCorction" => [],
            "official" => [],
            "undertime" => [],
            "offset"   => []
        ];
        if ( count($notifications) > 0){
            $ctrCT = 0;
            $ctrTK = 0;
            $ctrOB = 0;
            $ctrUT = 0;
            $ctrOS = 0;
            foreach($notifications as $req) {
                switch ($req->noti_type) {
                    case "cut_time";
                        $notifData["cut_time"][$ctrCT][] = $req->documentcode.'-'.$req->codenumber;
                        $notifData["cut_time"][$ctrCT][] = $req->datecreated;
                        $notifData["cut_time"][$ctrCT][] = $req->dateapproved;
                        $notifData["cut_time"][$ctrCT][] = $req->firstname.' '.$req->lastname;
                        $notifData["cut_time"][$ctrCT][] = $req->totaldays;
                        $notifData["cut_time"][$ctrCT][] = $req->totalhours;
                        $notifData["cut_time"][$ctrCT][] = $req->from_date.' TO '.$req->to_date;
                        $notifData["cut_time"][$ctrCT][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                        $notifData["cut_time"][$ctrCT][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                        $ctrCT++;
                        break;
                    case "TKCorction";
                        if ($req->to_correct == 'both'){
                            $inOut =  $req->from_inout.'/'.$req->to_inout;
                        }elseif ($req->to_correct == 'no_time_in'){
                            $inOut =  $req->from_inout;
                        }elseif ($req->to_correct == 'no_time_out'){
                            $inOut =  $req->to_inout;
                        }else{
                            $inOut = '------';
                        }

                        $notifData["TKCorction"][$ctrTK][] = $req->documentcode.'-'.$req->codenumber;
                        $notifData["TKCorction"][$ctrTK][] = $req->datecreated;
                        $notifData["TKCorction"][$ctrTK][] = $req->dateapproved;
                        $notifData["TKCorction"][$ctrTK][] = $req->firstname.' '.$req->lastname;
                        $notifData["TKCorction"][$ctrTK][] = $req->from_date;
                        $notifData["TKCorction"][$ctrTK][] = $inOut;
                        $notifData["TKCorction"][$ctrTK][] = ($req->salary_adjustment ? "SALARY ADJUSTMENT" : "");
                        $notifData["TKCorction"][$ctrTK][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                        $notifData["TKCorction"][$ctrTK][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                        $ctrTK++;
                        break;
                    case "official";
                        $notifData["official"][$ctrOB][] = $req->documentcode.'-'.$req->codenumber;
                        $notifData["official"][$ctrOB][] = $req->datecreated;
                        $notifData["official"][$ctrOB][] = $req->dateapproved;
                        $notifData["official"][$ctrOB][] = $req->firstname.' '.$req->lastname;
                        $notifData["official"][$ctrOB][] = $req->duration_type;
                        $notifData["official"][$ctrOB][] = ($req->duration_type == 'multiple' ? $req->from_date.' TO '.$req->to_date : $req->from_date);
                        $notifData["official"][$ctrOB][] = $req->totaldays;
                        $notifData["official"][$ctrOB][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                        $notifData["official"][$ctrOB][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                        $ctrOB++;
                        break;
                    case "undertime";
                        $notifData["undertime"][$ctrUT][] = $req->documentcode.'-'.$req->codenumber;
                        $notifData["undertime"][$ctrUT][] = $req->datecreated;
                        $notifData["undertime"][$ctrUT][] = $req->dateapproved;
                        $notifData["undertime"][$ctrUT][] = $req->firstname.' '.$req->lastname;
                        $notifData["undertime"][$ctrUT][] = $req->from_date;
                        $notifData["undertime"][$ctrUT][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                        $notifData["undertime"][$ctrUT][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                        $ctrUT++;
                        break;
                    case "offset";
                        $notifData["offset"][$ctrOS][] = $req->documentcode.'-'.$req->codenumber;
                        $notifData["offset"][$ctrOS][] = $req->datecreated;
                        $notifData["offset"][$ctrOS][] = $req->dateapproved;
                        $notifData["offset"][$ctrOS][] = $req->firstname.' '.$req->lastname;
                        $notifData["offset"][$ctrOS][] = $req->duration_type;
                        $notifData["offset"][$ctrOS][] = ($req->duration_type == 'multiple' ? $req->from_date.' TO '.$req->to_date : $req->from_date);
                        $notifData["offset"][$ctrOS][] = $req->totaldays;
                        $notifData["offset"][$ctrOS][] = $req->totalhours;
                        $notifData["offset"][$ctrOS][] = (strlen($req->reason) >= 100 ? substr($req->reason,0,100) : $req->reason);
                        $notifData["offset"][$ctrOS][] = "<a class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/notification/'.$req->id.'/process').">Process</a>";
                        $ctrOS++;
                        break;
                }
            }

        }
        return $notifData;
    }

    public function scopeForReceiver($query) {
        if (Input::has("department")) {
            if(Input::get("department") != "all") {
                $query->where("department",Input::get("department"));
            }
        }

        if (Input::has("dateFrom")) {
            if(Input::has("dateTo")) {
                $query->whereBetween("dateapproved",[Input::get("dateFrom"),Input::get("dateTo")]);
            }else{
                $query->where("dateapproved",Input::get("dateFrom"));
            }
        }

        if (Input::has("firstName")) {
            $query->where("firstname", 'LIKE' , '%'.Input::get('firstName').'%');
        }
        if (Input::has("middleName")) {
            $query->where("middlename", 'LIKE' , '%'.Input::get('middleName').'%');
        }
        if (Input::has("lastName")) {
            $query->where("lastname", 'LIKE' , '%'.Input::get('lastName').'%');
        }

        if (Input::has("notificationType")) {
            if(! in_array("on",Input::get("notificationType"))) {
                $query->whereIn("noti_type",Input::get("notificationType"));
            }
        }
    }


    public function scopeFilterDetails($query)
    {
        $query->where("date",Input::get("OTDate"));
        // $query->where('time_start',date("H:i:s", strtotime(Input::get("OTStart"))));
        // $query->where('time_end',date("H:i:s", strtotime(Input::get("OTEnd")));
    }

    /*--------------------Join with Notification-------------------*/
    public function signatories()
    {
        return $this->hasMany('NotifSignatory','notification_id','id');
    }

    public function notifName() {
        return $this->hasOne("SelectItems","item","noti_type");
    }

    public function employee()
    {
        return $this->hasOne('Employees','id','curr_emp')->select(array('firstname','middlename','lastname','id'));
    }

    public function owner(){
        return $this->hasOne('Employees','id','ownerid')->with('department');
    }

    public function toCorrect() {
        return $this->hasOne("SelectItems","item","to_correct");
    }

    public function notifAdditionalDetails() {
        return $this->hasMany("NotificationDetails","notificationid","id");
        // ->where("date",Input::get("OTDate"))
        // ->where('time_start',date("H:i:s", strtotime(Input::get("OTStart"))))
        // ->where('time_end',date("H:i:s", strtotime(Input::get("OTEnd"))));
    }



}