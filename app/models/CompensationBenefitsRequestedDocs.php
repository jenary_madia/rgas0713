<?php

// use app\CBR;

class CompensationBenefitsRequestedDocs extends \Eloquent {
    
	protected $primaryKey = 'cbrd_id';
    protected $table = 'compensationbenefitsrequesteddocs';
    public $incrementing = true;
    public $timestamps = false;
	protected $fillable = array('cbrd_id','cbrd_cb_id', 'cbrd_req_docs', 'cbrd_ref_num', 'cbrd_assigned_to', 'cbrd_assigned_date', 'cbrd_current', 'cbrd_status', 'cbrd_remarks', 'cbrd_processed_date', 'cbrd_date_filed');
	
	// public $_cbrd_id;
	// public $_cbrd_ref_num;
	// public $parent_ref_num;
	// public $_cbrd_status;
	
	public function employees()
	{
		return $this->has('employees','id','cbrd_assigned_to');
	}
	
	public function cb()
	{
		return $this->belongsTo('CompensationBenefits','cbrd_cb_id','cb_id');
	}
	
	
	// public function setCbrd_cb_id($cbrd_id)
	// {
		// $this->_cbrd_id = $cbrd_id;
	// }
	
	// public function setParent_ref_num($ref_num)
	// {
		// $this->parent_ref_num = $ref_num;
	// }
	
	// public function setCbrdStatus($status)
	// {
		// $this->_cbrd_status = $status;
	// }
	
	// public function returned()
	// {
		// echo Input::get('cbrd_cb_id');
		// die();
		// $cbrd = $this->find(Input::get('cbrd_cb_id'));
		// $cbrd->cbrd_status = 'Returned';
		// $cbrd->cbrd_remarks = Input::get('cbrd_remarks');
		// $cbrd->save();
	// }
	
	
	// public function getRequestForProcessing($cbrd_ref_num)
	// {
		// $this->_cbrd_ref_num = $cbrd_ref_num;
		// // $requests = DB::table('compensationbenefits')
        // // ->join('compensationbenefitsrequesteddocs', function($join)
        // // {
            // // $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                // // ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				// // ->where('compensationbenefitsrequesteddocs.cbrd_ref_num','=',$cbrd_ref_num)
				// // ->where('compensationbenefitsrequesteddocs.cbrd_assigned_to','=',Session::get("employee_id"))
				
        // // })->get();
		// $request = DB::table('compensationbenefits')
        // ->join('compensationbenefitsrequesteddocs', function($join)
        // {
            // $join->on('compensationbenefits.cb_id', '=', 'compensationbenefitsrequesteddocs.cbrd_cb_id')
                // ->where('compensationbenefitsrequesteddocs.cbrd_status', '=', 'For Processing')
				// ->where('compensationbenefitsrequesteddocs.cbrd_ref_num','=',$this->_cbrd_ref_num)
				// // ->where('compensationbenefits.cb_ref_num','=',$this->_cb_ref_num)
				// ->where('compensationbenefitsrequesteddocs.cbrd_assigned_to','=',Session::get("employee_id"));
        // })->get();
		// return $request;
	// }
	
	// public function getRequestedDocument($cbrd_id)
	// {
		// $cbrd = $this->where("cbrd_ref_num","=",$cbrd_id)->first();
		// // echo "<pre>",print_r($cbrd),"</pre>";
		// return $cbrd;
	// }

	
	

}