<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\GD;

class GeneralDocuments extends \Eloquent 
{
    
    protected $table = 'documents';
    protected $guarded = array('id');
    public $timestamps = false;

    
    public static function reviewer_record($id)
    {
        $reviewer = GeneralDocuments::join("document_signatories","documents.id" , "=","document_id")
                ->whereNull("approval_date")
               // ->where("status_id" , "!=",5)
                ->where("hidden" , 0)
                ->where("document_id" ,$id)
                ->limit(1)
                ->get(["signature_type_seq"])->toArray();

        //if(empty($reviewer)) return Redirect::to('/');

        $record = GeneralDocuments::join("document_signatories","documents.id" , "=","document_id")
                ->join("employees","employees.id" , "=","created_by")
                ->join("sections","section_id" , "=","sections.id" , "left")
                ->join("document_types","document_types.id" , "=","document_type_id")
                ->join("departments","departments.id" , "=","employees.departmentid")
              //  ->whereNull("approval_date")
                //->where("status_id" , "!=",5)
                ->whereRaw("(signature_type_seq = ? or signed = 1)",[isset($reviewer[0]["signature_type_seq"]) ? $reviewer[0]["signature_type_seq"] : 0 ] )
                ->where("document_id" ,$id)
                ->whereRaw("(employee_id = ? or notify_id = ? or checker_id = ?)" , [Session::get("employee_id"),Session::get("employee_id"),Session::get("employee_id")] )
        ->get(["company","dept_name","employees.departmentid","div_code","notify_id","notify_view","comments","comments_ea","current_id","checker_id","sect_name","contact_no","reference_no" , "document_name","document_type_id","document_type_name",
            "other_document_type","confidential","created_by","firstname","lastname","status_id","trans_date","employeeid","departmentid2"]);

       if($record->isEmpty()) return Redirect::to('/');
       if($record[0]->status_id == 8 && Session::get("employee_id") == 1) return Redirect::to('/');

        $status[1] = "New";
        $status[2] = "For Review";
        $status[3] = "For Acknowledgement";
        $status[4] = "For Approval";
        $status[5] = "Approved";
        $status[6] = "Disapproved";
        $status[7] = "Published";
        $status[8] = "Pending at PRES";
        $status[9] = "For Clarification or Revision";
        $status[10] = "Cancelled";
        $status[11] = "For Recommendation";

        $data["trans_date"] = $record[0]->trans_date;
        $data["status"] = $status[$record[0]->status_id];
        $data["section"] = $record[0]->sect_name;
        $data["contact"] = $record[0]->contact_no;
        $data["reference_no"] = $record[0]->reference_no;
        $data["document_name"] = $record[0]->document_name;
        $data["doc_type"] = $record[0]->document_type_id;
        $data["doc_name"] = $record[0]->document_type_name;
        $data["doc_others"] = $record[0]->other_document_type;
        $data["confidential"] = $record[0]->confidential;
        $data["employee"] = $record[0]->firstname . " " . $record[0]->lastname;
        $data["employeeid"] = $record[0]->created_by;
        $data["employeeno"] = $record[0]->employeeid;
        $data["current"] = $record[0]->checker_id ? $record[0]->checker_id : $record[0]->current_id;
        $data["comments"] = $record[0]->comments;
        $data["comments_ea"] = $record[0]->comments_ea;
        $data["notifier"] = $record[0]->notify_id == Session::get("employee_id") ? 1 : 0;
        $data["notify_view"] = $record[0]->notify_view;
        $data["div_code"] = $record[0]->div_code;
        $data["departmentid"] = $record[0]->departmentid;
        $data["dept_name"] = $record[0]->dept_name;
        $data["company"] = $record[0]->company;
        $data["departmentid2"] = $record[0]->departmentid2;
        return $data;
    }

    
    public static function view_record($id)
    {
        $record = GeneralDocuments::Leftjoin("document_signatories","documents.id" , "=","document_id")
                ->where("documents.id" , $id)
                ->join("employees","employees.id" , "=","created_by")
                ->Leftjoin("sections","section_id" , "=","sections.id", "left")
                ->Leftjoin("document_types","document_types.id" , "=","document_type_id")
                ->get(["comments_save","comments","sect_name","contact_no","reference_no" , "document_name","document_type_id","document_type_name",
            "other_document_type","confidential","created_by","firstname","lastname","documents.status_id","trans_date","employeeid"]);

       if($record->isEmpty()) return Redirect::to('/');
   
        $status[1] = "New";
        $status[2] = "For Review";
        $status[3] = "For Acknowledgement";
        $status[4] = "For Approval";
        $status[5] = "Approved";
        $status[6] = "Disapproved";
        $status[7] = "Published";
        $status[8] = "Pending at PRES";
        $status[9] = "For Clarification or Revision";
        $status[10] = "Cancelled";
        $status[11] = "For Recommendation";

        $data["trans_date"] = $record[0]->trans_date ;
        $data["status"] = $status[$record[0]->status_id];
        $data["section"] = $record[0]->sect_name;
        $data["contact"] = $record[0]->contact_no;
        $data["reference_no"] = $record[0]->reference_no;
        $data["document_name"] = $record[0]->document_name;
        $data["doc_type"] = $record[0]->document_type_id;
        $data["doc_name"] = $record[0]->document_type_name;
        $data["doc_others"] = $record[0]->other_document_type;
        $data["confidential"] = $record[0]->confidential;
        $data["employee"] = $record[0]->firstname . " " . $record[0]->lastname;
        $data["employeeid"] = $record[0]->created_by;
        $data["employeeno"] = $record[0]->employeeid;
        $data["comments_save"] = $record[0]->comments_save;
        $data["comments"] = $record[0]->comments;
        return $data;
    }

    
    public static function filer_record($id)
    {
        $record = GeneralDocuments::where("id" , $id)->wherein("status_id",[1,6,10,9])
        ->whereRaw("(current_id = ? or current_id = 0)",[Session::get("employee_id")])
        ->where("created_by" , Session::get("employee_id"))
        ->get(["section_id","contact_no","reference_no" , "document_name","document_type_id",
            "other_document_type","confidential","status_id","trans_date","comments_save","comments"]);

       if($record->isEmpty()) return Redirect::to('/');

       $status[1] = "New";
        $status[2] = "For Review";
        $status[3] = "For Acknowledgement";
        $status[4] = "For Approval";
        $status[5] = "Approved";
        $status[6] = "Disapproved";
        $status[7] = "Published";
        $status[8] = "Pending at PRES";
        $status[9] = "For Clarification or Revision";
        $status[10] = "Cancelled";
        $status[11] = "For Recommendation";

        $data["section"] = $record[0]->section_id;
        $data["contact"] = $record[0]->contact_no;
        $data["reference_no"] = $record[0]->reference_no;
        $data["document_name"] = $record[0]->document_name;
        $data["doc_type"] = $record[0]->document_type_id;
        $data["doc_others"] = $record[0]->other_document_type;
        $data["confidential"] = $record[0]->confidential;
        $data["status"] = $status[$record[0]->status_id];
        $data["trans_date"] = $record[0]->trans_date ;
        $data["comments_save"] = $record[0]->comments_save;
        $data["comments"] = $record[0]->comments;
        return $data;
    }

    
    public static function filer_signatory($id)
    {
        $record = DB::table("document_signatories")->where("document_id" , $id)
        ->join("employees" , "employees.id","=","document_signatories.employee_id")
        ->join("employees as e" , "e.id","=","document_signatories.notify_id","left")
        ->get(["signature_type","document_signatories.employee_id","notify_id" , "notify_view","employees.firstname","employees.lastname","e.firstname as first","e.lastname as last","employees.desig_level","employees.designation","employees.departmentid2","e.desig_level as desig_level2","e.designation as designation2","e.departmentid2 as departmentid22"]);

        $data = array();
        $type[1] = 0;
        $type[2] = 0;
        $type[3] = 0;
        $type[4] = 0;
        $type[5] = 0;
        foreach($record as $key=>$rs)
        {
            $data[$rs->signature_type][$type[$rs->signature_type]]["name"] = ucwords(strtolower($rs->firstname . " " . $rs->lastname)) . "(" . (in_array($rs->desig_level, ["top mngt","president"]) ? ucwords(strtolower($rs->designation)) : $rs->departmentid2) . ")";
            $data[$rs->signature_type][$type[$rs->signature_type]]["employee"] = $rs->employee_id;
            $data[$rs->signature_type][$type[$rs->signature_type]]["notify"] = $rs->notify_id;
            $data[$rs->signature_type][$type[$rs->signature_type]]["notify_name"] = ucwords(strtolower($rs->first . " " . $rs->last)) . "(" . (in_array($rs->desig_level2, ["top mngt","president"]) ? ucwords(strtolower($rs->designation2)) : $rs->departmentid22) . ")";
            $data[$rs->signature_type][$type[$rs->signature_type]]["view"] = $rs->notify_view;
            $type[$rs->signature_type] = $type[$rs->signature_type] + 1;
            $data["reviewer"][] = $rs->employee_id;
        }

        return $data;
    }

    
    public static function reviewer_signatory($id)
    {
        $record = DB::table("document_signatories")->where("document_id" , $id)
        ->join("employees" , "employees.id","=","document_signatories.employee_id")
        ->get(["desig_level","designation","departmentid2","employee_id","signature_type","employees.firstname","employees.lastname","approval_date"]);

        $data = array();
        $type[1] = 0;
        $type[2] = 0;
        $type[3] = 0;
        $type[4] = 0;
        $type[5] = 0;
        foreach($record as $key=>$rs)
        {
            $data[$rs->signature_type][$type[$rs->signature_type]]["name"] = ucwords(strtolower($rs->firstname . " " . $rs->lastname)) . "(" . (in_array($rs->desig_level, ["top mngt","president"]) ? ucwords(strtolower($rs->designation)) : $rs->departmentid2) . ")";
            $data[$rs->signature_type][$type[$rs->signature_type]]["date"] = $rs->approval_date ? date("Y-m-d",strtotime($rs->approval_date)): "";
            $type[$rs->signature_type] = $type[$rs->signature_type] + 1;
            $data["reviewer"][] = $rs->employee_id;
        }

        return $data;
    }

    

    public static function InsertSignatory($id)
    {
        $signature = array();
        $x = 0;
              
        if(!empty(Input::get("txtPrepared"))):
        foreach (Input::get("txtPrepared") as $index=>$emp) 
        {

            $data[] = array(
                        'document_id'=>$id, 
                        'signature_type'=>1,
                        'signature_type_seq'=>$x + 1,
                        'employee_id'=>$emp,
                        'signed'=> $index == 0 && $emp == Session::get("employee_id") ? 1  : "",
                        'approval_date'=> $index == 0 && $emp == Session::get("employee_id") ? date("Y-m-d")  : null,                        
                        'notify_id'=>Input::get("Prepared_$emp"),
                        'notify_view'=>Input::get("notify_$emp")
                        ); 
                     $x++;     

            if($index == 0 && $emp == Session::get("employee_id")){}
            else if(!isset($email) && Input::get('action') == "send")
            {
                $emp_rec = Employees::whereIn("id",[$emp,Input::get("Prepared_$emp")])
                    ->get(["firstname","email","lastname"]);

                $email = true;
                foreach($emp_rec as $rs)
                {
                    //SEND EMAIL NOTIFICATION
                    Mail::send('gd.email', array(
                        'status' => "Review",
                        'reference' => Input::get("reference"),
                        'filer' => Input::get("filer"),
                        'department' => Input::get("dept_name")),
                        function($message ) use ($rs)
                        {
                            $message->to($rs->email ,  $rs->firstname . " " . $rs->lastname )->subject('RGAS Notification Alert: Document for Review');
                        }
                    );

                }

                $prepared = Input::get("txtPrepared");
                if($prepared[0] == Session::get("employee_id"))
                {
                    GeneralDocuments::where("id" , $id)
                        ->where("created_by" , Session::get("employee_id"))
                        ->update(["current_id"=> $emp]);
                }

           }
        }
        endif;

        $reviewer = array(2=>"Reviewed",5=>"Recommended",3=>"Acknowledged",4=>"Approved");
        $reviewer_stats = array(2=>"Review",5=>"Recommendation",3=>"Acknowledgement",4=>"Approval");
        foreach ($reviewer as $type => $value) 
        {
            $subject = "RGAS Notification Alert: Document for " . $reviewer_stats[$type];          

            if(isset($_POST["txt$value"]))
            {
                foreach (Input::get("txt$value") as $emp) 
                {
                    $data[] = array(
                        'document_id'=>$id, 
                        'signature_type'=>$type,
                        'signature_type_seq'=>$x + 1,
                        'employee_id'=>$emp,
                        'signed'=>'',
                        'approval_date'=> null,                                                
                        'notify_id'=>Input::get($value . "_$emp"),
                        'notify_view'=>Input::get("notify_$emp")
                        );
                     $x++;      

                    if(!isset($email) && Input::get('action') == "send")
                    {
                        if($emp == 1 && !Input::get("confidential") )
                        {
                            $ea_id = DB::table("employees")
                            ->join('document_dept_lists', function ($join) 
                            {
                                $join->on('dept_hrms_code', '=', 'departmentid2')
                                     ->on('department_id', '=', "departmentid");
                            })->where("employees.id" , Session::get("employee_id") )
                            ->get(["document_dept_lists.employee_id","firstname","lastname","email"]);
                            if(isset($ea_id[0]->employee_id))
                            {
                                $email = true;
                                $eapersonnel = Employees::where("id",$ea_id[0]->employee_id)
                                ->get(["firstname","lastname","id","email"]);

                                //pending at pres
                                $param["status_id"] = 8;
                                $param["checker_id"] = $ea_id[0]->employee_id;//ea personnel

                                //SEND EMAIL NOTIFICATION TO EA
                                Mail::send('gd.email.approve', array(
                                            'reference' => Input::get("reference"),
                                            'filer' => Input::get("filer"),
                                            'department' => Input::get("dept_name"),
                                            'status' =>  $reviewer_stats[$type]  ),
                                            function($message ) use ($eapersonnel , $subject )
                                            {
                                                $message->to($eapersonnel[0]->email , $eapersonnel[0]->firstname . " " . $eapersonnel[0]->lastname  )
                                                    ->subject( $subject );
                                            }
                                        );

                              //  exit();//dito dapat papasok
                                GeneralDocuments::where("id" , $id)
                                ->where("created_by" , Session::get("employee_id"))
                                ->update(["current_id"=> $emp,"status_id"=>8 , "checker_id"=>$ea_id[0]->employee_id ]);
                            }

                        }


                        if(!isset($email))
                        {
                            $emp_rec = Employees::whereIn("id",[$emp,Input::get($value . "_$emp")])
                            ->get(["firstname","email","lastname"]);

                            $email = true;
                            foreach($emp_rec as $rs)
                            {
                                //SEND EMAIL NOTIFICATION
                                Mail::send('gd.email', array(
                                    'status' => $reviewer_stats[$type],
                                    'reference' => Input::get("reference"),
                                    'filer' => Input::get("filer"),
                                    'department' => Input::get("dept_name")),
                                    function($message)  use ($rs , $subject)
                                    {
                                        $message->to($rs->email ,  $rs->firstname . " " . $rs->lastname )->subject( $subject );
                                    }
                                );
                            }

                            $prepared = Input::get("txtPrepared");
                            if($prepared[0] == Session::get("employee_id"))
                            {
                                GeneralDocuments::where("id" , $id)
                                    ->where("created_by" , Session::get("employee_id"))
                                    ->update(["current_id"=> $emp,"status_id"=>($type ==5 ? 11  : $type ) ]);
                            }
                        }
                        
                   }
                }            
            }
        }

        if(isset($data))
            DB::table("document_signatories")->insert( $data );       
    }

    
    public static function section()
    {
            $table = DB::table("sections")
                ->where("active",1)
                ->where("departmentid", Session::get("dept_id"))
                ->orderby("sect_name")
                ->get(["sect_name" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->sect_name;
            }
            
            return $arr;
    }

    
    public static function doc_type()
    {
            $table = DB::table("document_types")
                ->orderby("id")
                ->get(["document_type_name" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->document_type_name;
            }
            
            return $arr;
    }

    
    public static function approved_signatory($id)
    {
            $table = Employees::join("document_signatories","employee_id","=","employees.id")
                ->orderby("signature_type_seq","desc")
                ->whereNotNull("approval_date")
                ->where("document_id",$id)
                ->get(["desig_level","designation","departmentid2","firstname" , "lastname", "employee_id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->employee_id] = ucwords(strtolower($val->firstname . " " . $val->lastname)) . "(" . (in_array($val->desig_level, ["top mngt","president"]) ? ucwords(strtolower($val->designation)) : $val->departmentid2) . ")";
            }
            
            return $arr;
    }

    
    public static function gd_receivers()
    {
            $arr = array();

            //load receiver if user is president
            if(Session::get("employee_id") == 1)
            {
                $table = Employees::join("receivers","receivers.employeeid","=","employees.id")
                    ->orderby("firstname")
                    ->where("code", "GEN_DOCS_SPL_RECEIVER")
                    ->get(["firstname" , "lastname", "receivers.employeeid"]);

                foreach($table as $key=>$val)
                {
                    $arr[$val->employeeid] = $val->firstname . " " . $val->lastname;
                }
            }

            return $arr;
    }

    
    public static function employees()
    {
            $table = DB::table("employees")
                ->where("company","RBC-CORP")
                ->orderby("firstname")
                ->get(["firstname" , "lastname","id","desig_level","designation","departmentid2"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = ucwords(strtolower($val->firstname . " " . $val->lastname)) . "(" . (in_array($val->desig_level, ["top mngt","president"]) ? ucwords(strtolower($val->designation)) : $val->departmentid2) . ")";
            }
            
            return $arr;
    }

    
    public static function attachments($id)
    {
            $table = DB::table("document_attachments")
                ->where("document_id",$id)
                ->get(["type","fn","code","id"]);
            
            return $table;
    }

    

    public static function insert_attachment($id)
    {
        //ADD ATTACHMENT
        $files_sig = Input::get("files-signatory");
        $files = Input::get("files");
        $attach = array();
        $fm = new Libraries\FileManager;
        $attach = array();

        $attach_audit = new GD\GD; 

        $attach_audit->setTable('document_attachments');
        $attach_audit->setPrimaryKey('document_id');

        if(!empty($files_sig)):
            foreach($files_sig as $file)
            {
                $fm->move($file['random_filename'], Config::get('rgas.rgas_storage_path'). 'gd/' . $id );
                $attach[] = array(
                        "document_id" => $id,
                        "type" => 1,
                        "fn" => $file['original_filename'],
                        "attached_by" => Session::get("employee_id"),
                        "attached_date" => date("Y-m-d H:i:s"),
                        "code" => json_encode($file)
                    );

                $attach_audit->AU002($id , $file["original_filename"] , Input::get("reference") );
            }
        endif;
        
        if(!empty($files)):
            foreach($files as $file)
            {
                $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'gd/' .  $id );
                $attach[] = array(
                        "document_id" => $id,
                        "type" => 2,
                        "fn" => $file['original_filename'],
                        "attached_by" => Session::get("employee_id"),
                        "attached_date" => date("Y-m-d H:i:s"),
                        "code" => json_encode($file)
                    );

                $attach_audit->AU002($id , $file["original_filename"] , Input::get("reference") );
            }
        endif;

        if(!empty($attach))  DB::table("document_attachments")->insert( $attach ); 
    }

    

    public static function delete_attachment($id)
    {
        $attachment = Input::get("attachment_id");
        $name = Input::get("attachment_name");
        if(!empty($attachment))
        {
            DB::table("document_attachments")
                ->where("document_id" , $id)
                ->wherein("id",$attachment)
                ->delete(); 

            $attach_audit = new GD\GD; 
            foreach($attachment as $index=>$file)
            {
                $attach_audit->setTable('document_attachments');
                $attach_audit->setPrimaryKey('document_id');
                $attach_audit->AU005($id , $name[$index] , Input::get("reference") );
            }
        }
    }

    
}