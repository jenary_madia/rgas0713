<?php


class CrrBuildUpDetail extends \Eloquent {

    
    protected $table = "crrbuildupdetail";

    protected $primaryKey = "cbd_detail_id";
    protected $hidden = array("password");

    public $timestamps = false;


    public static function get_crr_details()
    {
        if (Cache::has("crr_lists") )
        {
            $crr_lists = Cache::get("crr_lists");
        }
        else
        {
            $crr_lists = CrrBuildUp::where("cbd_section_status", "=", "1")->get([
                 "cbd_detail_id"
                ,"cbd_section_number"
                ,"cbd_section_description"

            ]);
             
            Cache::forever("crr_lists", $crr_lists);
        }
        
        return $crr_lists;
    }
    
    public static function get_all_crr_details($where_condition)
    {
        
        if (Cache::has("crr_lists") )
        {
            $crr_lists = Cache::get("crr_lists_all");
        }
        else
        {
            $crr_lists = CrrBuildUpDetail::whereRaw("{$where_condition}")->orderBy("cbd_section_number", "asc")->get([
                 "crrbuildupdetail.cbd_detail_id"
                ,"crrbuildupdetail.cb_crr_id"
                ,"crrbuildupdetail.cbd_section_description"
                ,"crrbuildupdetail.cbd_section_number"
                ,"crrbuildupdetail.cbd_section_status"
            ]);
             
            Cache::forever("crr_lists_all", $crr_lists);
        }
        
        return $crr_lists;
    }
}
/* End of file */