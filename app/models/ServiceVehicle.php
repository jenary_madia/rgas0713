<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\SV;

class ServiceVehicle extends \Eloquent 
{
    
    protected $table = 'servicevehicles';
    protected $guarded = array('id');
    public $timestamps = false;

    public static function filer_record($id)
    {
        $record = ServiceVehicle::where("id" , $id)->wherein("status",["NEW","DISAPPROVED"])
        ->where("ownerid" , Session::get("employee_id"))
        ->get(["controlno","status","section","contact_no","purpose","requestfor","comment","date","comment_save"]);

       if($record->isEmpty()) return Redirect::to('/');

        $data["contact"] = $record[0]->contact_no;
        $data["requestfor"] = $record[0]->requestfor;
        $data["purpose"] = $record[0]->purpose;
        $data["section"] = $record[0]->section;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->controlno;
        $data["date_filed"] = $record[0]->date;
        $data["comment_save"] = $record[0]->comment_save;
        return $data;
    }

    public static function view_record($id)
    {
        $record = ServiceVehicle::where("id" , $id)
        ->where("ownerid" , Session::get("employee_id"))
        ->get(["controlno","status","section","contact_no","purpose","requestfor","comment","date","comment_save"]);

       if($record->isEmpty()) return Redirect::to('/');

        $data["contact"] = $record[0]->contact_no;
        $data["requestfor"] = $record[0]->requestfor;
        $data["purpose"] = $record[0]->purpose;
        $data["section"] = $record[0]->section;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->controlno;
        $data["date_filed"] = $record[0]->date;
        $data["comment_save"] = $record[0]->comment_save;
        return $data;
    }

     public static function vehicle_detail($id)
    {
        $record = DB::table("servicevehicledetails")
        ->where("servicevehicleid" , $id)
         ->get(["vehicletype","plateno","id","pickuptime","pickupplace","pickupplace_id","pickupplace_location","pickupplace_location_id","date",
            "destination","destination_id","destination_location","destination_location_id","noofpassengers","estimated_trip_time"]);
     
        return $record;
    }

     public static function head_record($id , $status)
    {
        $status = $status == "SERVED_" ? "SERVED/DELETED" : ( $status == "CANCELLED_" ? "CANCELLED/DELETED" :  $status);
        $record = ServiceVehicle::where("servicevehicles.id" , $id)
        ->where("curr_emp" , Session::get("employee_id"))
        ->where("status" ,  $status)
        ->join("employees" , "employees.id","=","servicevehicles.ownerid")
        ->get();

       if($record->isEmpty()) return Redirect::to('/');

        $data["purpose"] = $record[0]->purpose;
        $data["requestfor"] = $record[0]->requestfor;
        $data["comment"] = $record[0]->comment;
        $status = $record[0]->status == "SERVED/DELETED" ? "SERVED" : ($record[0]->status == "CANCELLED/DELETED" ? "CANCELLED" :  $record[0]->status );
        $data["status"] = $status;
        $data["contact"] = $record[0]->contact_no;

        $data["reference_no"] = $record[0]->controlno;
        $data["name"] = $record[0]->requestor;
        $data["emp_no"] = $record[0]->employeeid;
        $data["company"] = $record[0]->company;
        $data["department"] = $record[0]->department;
        $data["section"] = $record[0]->section;
        $data["date_filed"] = $record[0]->date;

        return $data;
    }
    
    public static function company()
    {
            $rec = DB::table("servicevehicle_companies")
                ->where("status",1)
                ->orderby("company")
                ->get(["company_id" , "company","location_id"]);
            
            return $rec;
    }

    public static function company_list()
    {
            $rec = DB::table("companylists")
                ->where("active",1)
                ->orderby("comp_code")
                ->get(["comp_code"]);
            
            return $rec;
    }

    public static function location()
    {
            $table = DB::table("servicevehicle_locations")
                ->where("location" , "!=" ,"")
                ->orderby("location")
                ->get(["location" , "location_id"]);
            
            $arr[0] = "";
            foreach($table as $key=>$val)
            {
                $arr[$val->location_id] = $val->location;
            }
            
            return $arr;
    }



    public static function plateno()
    {
            $rec = DB::table("servicevehicle_vehicles")
                ->where("status_id",1)
                ->orderby("plate_no")
                ->get(["vehicle_id" , "vehicle_type_id","plate_no"]);
            
            return $rec;
    }

    public static function vehicle()
    {
            $table = DB::table("servicevehicle_vehicletypes")
                ->where("type" , "!=" ,"")
                ->orderby("type")
                ->get(["type" , "vehicle_type_id"]);
            
            $arr[0] = "";
            foreach($table as $key=>$val)
            {
                $arr[$val->vehicle_type_id] = $val->type;
            }
            
            return $arr;
    }


    public static function superior($sid)
    {
        $sup = Employees::where("id" , $sid)
            ->first(["firstname","lastname","email", "id","desig_level","superiorid"]);

        return $sup;  
    }



    public static function get_reference_no()
    {
        $year = date("Y");
        $month = date("m");
        $ref = ServiceVehicle::whereRaw("SUBSTR(controlno, 1, 4) = ?" , [$year])
            ->orderby("controlno","desc")
            ->limit(1)
            ->get(["controlno"])->toArray();

       $no = isset($ref[0]["controlno"]) ? $ref[0]["controlno"] : "$year-$month-00000";   
       $num = sprintf("%'.05d", (int)substr($no,8) + 1);
//echo "$year-$month-$num";
//exit();
       return "$year-$month-$num";
    }

    public static function department()
    {
            $table = DB::table("departments")
                ->where("active",1)
                ->orderby("dept_name")
                ->get(["dept_name" , "id","comp_code"]);
      
      
            
            return $table;
    }
}