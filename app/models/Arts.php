<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\ART;

class Arts extends \Eloquent 
{    
    protected $table = 'arts';
    protected $guarded = array('id');
    public $timestamps = false;

    
    public static function filer_record($id)
    {
        $record = Arts::where("id" , $id)->wherein("status",["New","Disapproved"])
        ->where("ownerid" , Session::get("employee_id"))
        ->get(["attachments2","contactno","project_name","duedate","requestfor","service_requested","packaging_materials","briefdescription","othermaterials"
            ,"productname","netweight","length","width","width","height","otherspecifications","comment","section"
            ,"status","reference_no","attachments","attach1","attach2","attach3","attach4","attach5","daterequested","comment_save"]);

       if($record->isEmpty()) return Redirect::to('/');

       $data["section"] = $record[0]->section;
        $data["contact"] = $record[0]->contactno;
        $data["project"] = $record[0]->project_name;
        $data["duedate"] = $record[0]->duedate;
        $data["request"] = $record[0]->requestfor;
        $data["service_requested"] = $record[0]->service_requested;
        $data["packaging_materials"] = $record[0]->packaging_materials;
        $data["briefdescription"] = $record[0]->briefdescription;
        $data["othermaterials"] = $record[0]->othermaterials;
        $data["productname"] = $record[0]->productname;
        $data["netweight"] = $record[0]->netweight;
        $data["length"] = $record[0]->length;
        $data["width"] = $record[0]->width;
        $data["height"] = $record[0]->height;
        $data["otherspecifications"] = $record[0]->otherspecifications;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->reference_no;
        $data["attachments"] = $record[0]->attachments;
        $data["date_filed"] = $record[0]->daterequested;
        $data["comment_save"] = $record[0]->comment_save;

        $data["attach1"] = $record[0]->attach1;
        $data["attach2"] = $record[0]->attach2;
        $data["attach3"] = $record[0]->attach3;
        $data["attach4"] = $record[0]->attach4;
        $data["attach5"] = $record[0]->attach5;

        $data["attachments2"] = $record[0]->attachments2;
        return $data;
    }

    
    public static function filer_view_record($id)
    {
        $record = Arts::where("id" , $id)
        ->where("ownerid" , Session::get("employee_id"))
        ->get(["section","attachments2","contactno","project_name","duedate","requestfor","service_requested","packaging_materials","briefdescription","othermaterials"
            ,"productname","netweight","length","width","width","height","otherspecifications","comment"
            ,"status","reference_no","attachments","attach1","attach2","attach3","attach4","attach5","daterequested","comment_save"]);

       if($record->isEmpty()) return Redirect::to('/');

         $data["section"] = $record[0]->section;
        $data["contact"] = $record[0]->contactno;
        $data["project"] = $record[0]->project_name;
        $data["duedate"] = $record[0]->duedate;
        $data["request"] = $record[0]->requestfor;
        $data["service_requested"] = $record[0]->service_requested;
        $data["packaging_materials"] = $record[0]->packaging_materials;
        $data["briefdescription"] = $record[0]->briefdescription;
        $data["othermaterials"] = $record[0]->othermaterials;
        $data["productname"] = $record[0]->productname;
        $data["netweight"] = $record[0]->netweight;
        $data["length"] = $record[0]->length;
        $data["width"] = $record[0]->width;
        $data["height"] = $record[0]->height;
        $data["otherspecifications"] = $record[0]->otherspecifications;
        $data["comment"] = $record[0]->comment;
        $data["comment_save"] = $record[0]->comment_save;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->reference_no;
        $data["attachments"] = $record[0]->attachments;
        $data["date_filed"] = $record[0]->daterequested;

        $data["attach1"] = $record[0]->attach1;
        $data["attach2"] = $record[0]->attach2;
        $data["attach3"] = $record[0]->attach3;
        $data["attach4"] = $record[0]->attach4;
        $data["attach5"] = $record[0]->attach5;

        $data["attachments2"] = $record[0]->attachments2;
        return $data;
    }



    
    public static function head_record($id)
    {
        if(!in_array(Session::get("desig_level") ,["head","president","top mngt"])) return "";

        $record = Arts::where("id" , $id)
        ->where("curr_emp" , Session::get("employee_id"))
        ->get();

       if($record->isEmpty()) return Redirect::to('/');

        $data["contact"] = $record[0]->contactno;
        $data["project"] = $record[0]->project_name;
        $data["duedate"] = $record[0]->duedate;
        $data["request"] = $record[0]->requestfor;
        $data["service_requested"] = $record[0]->service_requested;
        $data["packaging_materials"] = $record[0]->packaging_materials;
        $data["briefdescription"] = $record[0]->briefdescription;
        $data["othermaterials"] = $record[0]->othermaterials;
        $data["productname"] = $record[0]->productname;
        $data["netweight"] = $record[0]->netweight;
        $data["length"] = $record[0]->length;
        $data["width"] = $record[0]->width;
        $data["height"] = $record[0]->height;
        $data["otherspecifications"] = $record[0]->otherspecifications;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->reference_no;

        $data["name"] = $record[0]->requestedby;
        $data["emp_no"] = $record[0]->employeeid;
        $data["company"] = $record[0]->company;
        $data["department"] = $record[0]->department;
        $data["section"] = $record[0]->section;
        $data["date_filed"] = $record[0]->daterequested;
        $data["attachments"] = $record[0]->attachments;
		
		$data["attach1"] = $record[0]->attach1;
        $data["attach2"] = $record[0]->attach2;
        $data["attach3"] = $record[0]->attach3;
        $data["attach4"] = $record[0]->attach4;
        $data["attach5"] = $record[0]->attach5;
        return $data;
    }

     
    public static function art_head_record($id)
    {
        $record = Arts::where("id" , $id)
        //->where("curr_emp" , Session::get("employee_id"))
        ->whereIn("status" ,["For Processing","Closed","For Conforme"])
       // ->whereNull("assignto")
        ->get();

       if($record->isEmpty()) return Redirect::to('/');

        $data["curr_emp"] = $record[0]->curr_emp;
        $data["assignto"] = $record[0]->assignto;
        $data["contact"] = $record[0]->contactno;
        $data["project"] = $record[0]->project_name;
        $data["duedate"] = $record[0]->duedate;
        $data["request"] = $record[0]->requestfor;
        $data["service_requested"] = $record[0]->service_requested;
        $data["packaging_materials"] = $record[0]->packaging_materials;
        $data["briefdescription"] = $record[0]->briefdescription;
        $data["othermaterials"] = $record[0]->othermaterials;
        $data["productname"] = $record[0]->productname;
        $data["netweight"] = $record[0]->netweight;
        $data["length"] = $record[0]->length;
        $data["width"] = $record[0]->width;
        $data["height"] = $record[0]->height;
        $data["otherspecifications"] = $record[0]->otherspecifications;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->reference_no;

        $data["name"] = $record[0]->requestedby;
        $data["emp_no"] = $record[0]->employeeid;
        $data["company"] = $record[0]->company;
        $data["department"] = $record[0]->department;
        $data["section"] = $record[0]->section;
        $data["date_filed"] = $record[0]->daterequested;
        $data["attachments"] = $record[0]->attachments;
		
		$data["attach1"] = $record[0]->attach1;
        $data["attach2"] = $record[0]->attach2;
        $data["attach3"] = $record[0]->attach3;
        $data["attach4"] = $record[0]->attach4;
        $data["attach5"] = $record[0]->attach5;

        $data["attachments2"] = $record[0]->attachments2;
        return $data;
    }

    
    public static function art_artist_record($id)
    {
        $record = Arts::where("id" , $id)
     //   ->where("curr_emp" , Session::get("employee_id"))
        ->whereIn("status" , ["For Processing","For Conforme"])
        ->where("assignto",Session::get("employee_id"))
        ->get();

       if($record->isEmpty()) return Redirect::to('/');

        $data["contact"] = $record[0]->contactno;
        $data["project"] = $record[0]->project_name;
        $data["duedate"] = $record[0]->duedate;
        $data["request"] = $record[0]->requestfor;
        $data["service_requested"] = $record[0]->service_requested;
        $data["packaging_materials"] = $record[0]->packaging_materials;
        $data["briefdescription"] = $record[0]->briefdescription;
        $data["othermaterials"] = $record[0]->othermaterials;
        $data["productname"] = $record[0]->productname;
        $data["netweight"] = $record[0]->netweight;
        $data["length"] = $record[0]->length;
        $data["width"] = $record[0]->width;
        $data["height"] = $record[0]->height;
        $data["otherspecifications"] = $record[0]->otherspecifications;
        $data["comment"] = $record[0]->comment;
        $data["status"] = $record[0]->status;
        $data["reference_no"] = $record[0]->reference_no;

        $data["name"] = $record[0]->requestedby;
        $data["emp_no"] = $record[0]->employeeid;
        $data["company"] = $record[0]->company;
        $data["department"] = $record[0]->department;
        $data["section"] = $record[0]->section;
        $data["date_filed"] = $record[0]->daterequested;
        $data["attachments"] = $record[0]->attachments;

        $data["attachments2"] = $record[0]->attachments2;
        return $data;
    }

    public static function get_reference_no()
    {
        $year = date("Y");
        $ref = Arts::whereRaw("SUBSTR(reference_no, 1, 4) = ?" , [$year])
            ->orderby("reference_no","desc")
            ->limit(1)
            ->get(["reference_no"])->toArray();

       $no = isset($ref[0]["reference_no"]) ? $ref[0]["reference_no"] : "$year-00000";   
       $num = sprintf("%'.05d", (int)substr($no,5) + 1);

       return "$year-$num";
    }

    public static function department()
    {
            $table = DB::table("departments")
                ->where("active",1)
                ->where("comp_code","RBC-CORP")
                ->orderby("dept_name")
                ->get(["dept_name" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[trim($val->dept_name)] = trim($val->dept_name);
            }
            
            return $arr;
    }

    public static function artist()
    {
            $table = Employees::where("active",1)
                ->where("company","RBC-CORP")
                ->where("departmentid",17)
                ->orderby("firstname")
                ->orderby("lastname")
                ->get(["firstname" , "lastname" , "id"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->id] = $val->firstname . " " . $val->lastname;
            }
            
            return $arr;
    }

    public static function revision($id)
    {
            $rev = DB::table("art_revisions")
                ->where("ar_art_id",$id)
                ->get(["ar_received_date","ar_completion_date","ar_submmited_date",
                    "ar_status","ar_attachments","ar_detail","ar_revision_degree"]);
      
            return $rev;
    }

    public static function delete_attachment($id)
    {
        $attachment = Input::get("attachment_name");
        if(!empty($attachment))
        {

            $attach_audit = new ART\ART; 
            foreach($attachment as $index=>$file)
            {
                $attach_audit->setTable('arts');
                $attach_audit->setPrimaryKey('id');
                $attach_audit->AU005($id , $file , Input::get("reference") );
            }
        }
    }


    public static function superior($sid)
    {
        $sup = Employees::where("id" , $sid)
            ->first(["firstname","lastname","email", "id","desig_level","superiorid"]);

        if(!in_array($sup["desig_level"], ["president","top mngt"]))
        {
             return Arts::superior( $sup["superiorid"]  ); 
        }
        else
        {
             return $sup;  
        }

       
    }


    
}