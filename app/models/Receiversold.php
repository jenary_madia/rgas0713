<?php


class Receivers extends \Eloquent {


	protected $table = "receivers";
	public $incrementing = true;
	 
	public $timestamps = false;
	
	// For TE (temporary)
	
	public static function isHead($employeeid) //for head
	{
		$isHead = Employees::where("desig_level","=","head")
				->where("employeeid","=",$employeeid)
				->first();
		if($isHead) return true;
		return false;
	}
	
	public static function isCHRDTrainee($employeeid) //for CHRD Assessment
	{
		$isCHRDEmp = Employees::where("desig_level","=","employee")
				->where("departmentid2","=","CHRD")
				->where("employeeid","=",$employeeid)
				->first();
		if($isCHRDEmp) return true;
		return false;
	}
	
	// public static function isChrdHead($employeeid) //for CHRD Head
	// {
		// $isHeadHr = Employees::where("desig_level","=","head")
					// ->where("departmentid2","=","CHRD")
					// ->where("employeeid","=",$employeeid)
					// ->first();
		// if($isHeadHr)
			// return true;
		// return false;
	// }
	
	public static function isVpForChrd($employeeid) //for VP for corp services
	{
		$isChrdVp = Employees::where("designation","=","VP FOR CORP. HUMAN RESOURCE")
					->where("desig_level","=","head")
					->where("departmentid2","=","CHRD")
					->where("employeeid","=",$employeeid)
					->first();
		if($isChrdVp)
			return true;
		return false;
		//return $isChrdVp;
	}
	
	public static function isSvpForCorpServ($employeeid) //for SVP for corp services
	{
		$isSvpCs = Employees::where("designation","=","SVP FOR CORP. SERVICES")
					->where("employeeid","=",$employeeid)
					->first();
		if($isSvpCs)
			return true;
		return false;
	}	
	
	public static function isPresident($employeeid) //for President
	{
		$isPres = Employees::where("designation","=","PRESIDENT")
					->where("employeeid","=",$employeeid)
					->first();
		if($isPres)
			return true;
		return false; 
	}
	
	//For CBR
    public static function get_cbr_staff()
    {
        $cbr_list = Receivers::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.module", "=", "cbr")->where("emp.active", "=", 1)->orderBy("emp.firstname", "asc")->get([
        
             "emp.id"
            ,"emp.firstname"
            ,"emp.lastname"
        
        ]);
        
		return $cbr_list;
       
    }
	
	public static function is_cbr_receiver($employee_id)
    {
        
        $cbr_receiver = Receivers::where("code", "=", "CBR_RECEIVER")->where("module", "=", "cbr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($cbr_receiver))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
	
	public static function is_cbr_staff($employee_id)
    {
        
        $cbr_staff = Receivers::where("code", "=", "CBR_STAFF")->where("module", "=", "cbr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($cbr_staff))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
	
	//For TAF
	public static function is_taf_receiver($employee_id)
	{
        
		$taf_receiver = Receivers::where("employeeid", "=", "{$employee_id}")->where(function ($query){
	 
			$query->where("description", "=", "TAF-ACT")->orWhere("description", "=", "TAF-FNC");
			
		})->get(["employeeid"]);
		
		//
		if($taf_receiver->count() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
    
    
    //For ITr
    public static function is_itr_bsa($employee_id)
    {
        
        $itr_bsa = Receivers::where("code", "=", "MOC-BSA")->where("module", "=", "moc")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
		if(!empty($itr_bsa))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
    
    
    //For ITR
    public static function is_webdis_personnel($employee_id)
    {
        
        $itr_webdis_receiver = Receivers::where("code", "=", "ITR-WEBDIS")->where("employeeid", "=", "{$employee_id}")->get(['employeeid']);
        
		if($itr_webdis_receiver->count() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
        
    }
    
    
    //For ITR
    public static function get_user_support_primary()
    {
        
        $user_support = Receivers::where("code", "=", "ITR-USS")->where("value", "=", "pr")->first(["employeeid"]);
        
        $emp_id = $user_support->employeeid;
        
        return $emp_id;
    }
 
 
    //For ITR
    public static function get_user_support()
    {
        $user_support = Receivers::where("code", "=", "ITR-USS")->where("module", "=", "itr")->first(["employeeid"]);   
        return $user_support;  
    }
    
    
    //For ITR
    public static function get_manager($request_type)
    {
        if ($request_type == "AD")
        {
            $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("title", "=" ,"Application Manager")->get(["employeeid"]);
        }
        else if ($request_type == "TS")
        {
            $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("title", "=" ,"Technical Manager")->get(["employeeid"]);
        }
        return $manager;
    }
    
    
    //For ITR
    public static function check_manager($employee_id)
    {
        $manager = Receivers::where("code", "=", "ITR-MAN")->where("module", "=", "itr")->where("employeeid", "=", "{$employee_id}")->first(["employeeid", "title"]);
        
        return $manager;
    }
    
    
    //For ITR
    public static function get_csmd_adh_id()
    {
        $csmd_adh = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.module", "=" ,"moc")->where("receivers.code", "=", "MOC-ADH")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csmd_adh;
    }
    
    //for ITR
    public static function get_avp_it()
    {
        $avp_it = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-ADH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $avp_it;
    }
    
    //for ITR
    public static function get_vp_it()
    {
        $vp_it = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-DH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $vp_it;
        
    }
    
    //for ITR
    public static function get_csdvh()
    {
        $csdvh = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "ITR-CSDVH")->where("receivers.module", "=", "itr")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csdvh;
    }
    
    
    //For ITR
    public static function get_forward_itr_receivers($filter = "")
    {
        
        if ($filter == "")
        {
            $reciever_code = ['MOC-ADH', 'MOC-SMDDDH', 'ITR-MAN', 'ITR-DH', 'ITR-ADH', 'MOC-CSDVH'];
        }
        else
        {
            $reciever_code = $filter;
        }
        
        
        $reciever = Receivers::leftJoin("employees as emp", function($join){
           
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->whereIn("receivers.module", array('itr', 'moc'))->whereIn("receivers.code", $reciever_code)->where("emp.active", "=", "1")->groupBy("receivers.employeeid")->get([
        
            "receivers.employeeid"
           ,"receivers.code"
           ,"emp.firstname"
           ,"emp.lastname"
           
        ]);
        
        return $reciever;
        
    }
    

    //For ITR
    public static function get_bsa_lists()
    {
        
        $bsa_list = Receivers::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "MOC-BSA")->where("emp.active", "=", 1)->orderBy("emp.firstname", "asc")->get([
        
             "emp.id"
            ,"emp.firstname"
            ,"emp.lastname"
        
        ]);
        
       return $bsa_list;
       
    }
    
    
    
    //For ITR
    public static function get_csmd_head()
    {
        $csmd_head = Receivers::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "receivers.employeeid");
            
        })->where("receivers.code", "=", "MOC-SMDDDH")->first(["receivers.employeeid", "emp.firstname", "emp.middlename", "emp.lastname"]);
        
        return $csmd_head;   
    }
    
    
    //For ITR
    public static function get_satellite_systems($company)
    {
        
        $receiver_code = "";
        
        if (strtolower($company) == "rbc-sat")
        {
            $receiver_code = "ITR-SD-RBC";
        }
        else if (strtolower($company) == "sfi")
        {
            $receiver_code = "ITR-SD-SFI";
        }
        else if (strtolower($company) == "pfi")
        {
            $receiver_code = "ITR-SD-PFI";
        }
        else if (strtolower($company) == "mfc")
        {
            $receiver_code = "ITR-SD-MFC";
        }
        else if (strtolower($company) == "bbfi")
        {
            $receiver_code = "ITR-SD-BBFI";
        }
        else if (strtolower($company) == "spi")
        {
            $receiver_code = "ITR-SD-SPI";
        }
        else if (strtolower($company) == "buk")
        {
            $receiver_code = "ITR-SD-BUK";
        }
        
        $systems_rec = Receivers::where("code", "=", "{$receiver_code}")->get(["employeeid"]);
        
        return $systems_rec;
        
    }
    
    public static function get($module, $code, $company)
    {
        $receiver = Receivers::where("module", "=", "{$module}"
                            )
                            ->with("employee")
                            ->where("code", "=", "{$code}"
                            )->where("company", "=", "{$company}"
                            )->first();
                            
        return $receiver;
    }



// Added by Jenary
    public static function is_lrf_receiver($employee_id)
    {
        $is_lrf_receiver = Receivers::where("code","HR_LEAVE_RECEIVER")->where("module", "leaves")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
        if(!empty($is_lrf_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public static function is_clinic_receiver($employee_id)
    {
        $is_clinic_receiver = Receivers::where("code","CLINIC_LEAVE_RECEIVER")->where("module", "leaves")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        //
        if(!empty($is_clinic_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // Added by Jenary
    public static function is_notif_receiver($employee_id)
    {
        $is_lrf_receiver = Receivers::where("code","HR_NOTIFICATION_RECEIVER")->where("module", "notifications")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        //
        if(!empty($is_lrf_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function employee()
    {
        return $this->hasOne('Employees','id','employeeid')->select(array('firstname','middlename','lastname','id','email'));
    }

    public static function is_msr_receiver($employee_id)
    {
        $is_receiver = Receivers::whereIn("code",[MSR_RECEIVER_CODE,MSR_IQUEST_RECEIVER_CODE])->where("module", "manpowerservices")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        //
        if(!empty($is_receiver))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }


    // added for moc
    public static function is_div_head($employee_id) {
        $is_div_head = Receivers::whereIn("code",[CSMD_CORP_DIVHEAD])->where("module", "MOC")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        if(!empty($is_div_head))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public static function is_bsa_receiver($employee_id) {
        $is_bsa = Receivers::whereIn("code",[CSMD_BSA])->where("module", "MOC")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);

        if(!empty($is_bsa))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    //end added moc

 
}
/* End of file */