<?php

class Departments extends \Eloquent {
    
    protected $table = 'departments';
    public $incrementing = false;
    public $timestamps = false;
    
    
    public static function get_departments()
    {
        
        if ( Cache::has("departments") )
        {
            $departments = Cache::get("departments");
        }
        else
        {
            $departments = Departments::where("active", "=", "1")->whereIn('comp_code', array('RBC-CORP', 'RBC-SAT', 'SFI', 'MFC', 'PFI', 'SPI', 'BBFI', 'BUK-MFC', 'BUK'))
            ->orderBy("dept_name", "asc")->get([
                "id"
               ,"dept_code"
               ,"dept_name"
               ,"comp_code"
            ]);
            Cache::add("departments", $departments, 1440);
        }
        
        return $departments;
        
    }
	
	 public static function getDepartmentsByCompany($company_code)
    {
        return Departments::where("active",1)
                            ->where("comp_code",$company_code)
                            ->get(array("id","dept_name","dept_code"));
    }

    public function sections()
    {
        return $this->hasMany('Sections','departmentid');
    }
    
    
}