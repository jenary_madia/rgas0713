<?php


class ItrTrails extends \Eloquent { 

    
    protected $table = 'itr_trails';
    protected $primaryKey = 'trail_no';
  
    public $incrementing = false;

    public $timestamps = false;
  
  
    //
    public static function get_itr_trails($itr_no)
    {
        
        $itr_trails = ItrTrails::where("itr_trails.itr_no", "=", "{$itr_no}")->leftJoin("employees as emp", function($join){
          
            $join->on("emp.id", "=", "itr_trails.assign_to");
            
        })->leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_trails.itr_status");
            
        })->orderBy("itr_trails.trail_no", "Asc")->orderBy("itr_trails.date_from", "Asc")->get([
            
            "itr_trails.trail_no"
           ,"itr_trails.itr_no"
           ,"itr_trails.itr_status"
           ,"itr_trails.rgas_status"
           ,"itr_trails.assign_to"
           ,"itr_trails.date_from"
           ,"itr_trails.date_to"
           ,"itr_trails.remarks"
           ,"itr_trails.via_memo"
           ,"itr_trails.del_tag"
           ,"itr_trails.mem_no"
           ,"itr_trails.disable_delete"
           ,"emp.id as employee_id"
           ,"emp.firstname"
           ,"emp.middlename"
           ,"emp.lastname"
           ,"config.system_desc"
           
        ]);
        
        
        return $itr_trails;
        
    }
  
    
    //
    public static function insert_itr_trail($itr_no, $itr_status, $rgas_status, $assign_to, $date_from, $date_to, $remarks, $via_memo, $del_tag, $mem_no, $disable_delete = 1)
    {
      
        $trail_no = ItrTrails::generate_trail_no();

        $itr_trails = new ItrTrails;
    
        $itr_trails->trail_no = $trail_no;
        
        $itr_trails->itr_no = $itr_no;
        $itr_trails->itr_status = $itr_status;
        $itr_trails->rgas_status = $rgas_status;
        $itr_trails->assign_to = $assign_to;
        $itr_trails->date_from = $date_from;
        $itr_trails->date_to = $date_to;
        $itr_trails->remarks = $remarks;
        $itr_trails->via_memo = $via_memo;
        $itr_trails->del_tag = $del_tag;
        $itr_trails->mem_no = $mem_no;
        $itr_trails->disable_delete = $disable_delete;
        
        $itr_trails->save();
        
        
        return $itr_trails;
    }
  
  
    //
    public static function get_last_itr_trail_no($itr_no)
    {
        
        $itr_trail = ItrTrails::where("itr_no", "=", "{$itr_no}")->orderBy("trail_no", "desc")->first(["trail_no"]);
        
        return $itr_trail->trail_no;
        
    }
    
    
    
    //
    public static function get_last_itr_trail_no_status($itr_no)
    {
         $itr_trail = ItrTrails::where("itr_no", "=", "{$itr_no}")->orderBy("trail_no", "desc")->first(["itr_status"]);
        
        return $itr_trail->itr_status;
    }
  
  
    //
    public static function get_next_record_date($trail_no, $itr_no)
    {
        $next_date = "";
        $itr_trail = ItrTrails::where("trail_no", ">", "{$trail_no}")->where("itr_no", "=", "{$itr_no}")->first(["date_from"]);
        if (!empty($itr_trail))
        {
            $next_date = $itr_trail->date_from;
            return $next_date;
        }
        
    }
  
  
  
    //
    public static function get_itr_no_trail($employee_id)
    {
        
        $itr_no_lists =ItrTrails::where("assign_to", "=", "{$employee_id}")->get(["itr_no"]);
        
        return $itr_no_lists;
    }
  
    //
    private static function generate_trail_no()
    {
        
        $year_no = date('y');
        
        $last_trail_no = ItrTrails::where("trail_no", "LIKE", "%_{$year_no}")->max("trail_no");
        
        $code_suffx = "";
        if($last_trail_no != "")
        {
            $count_trail_no_arr = explode("_", $last_trail_no);
            $code_suffx = $count_trail_no_arr[1] + 1;
        }
        else
        {
            $code_suffx = 1;    
        }
        $filled_int = sprintf("%05d", $code_suffx); 
        
        
        return "tr_".$filled_int.'_'.$year_no;
        
    }
  
  
   
  
}