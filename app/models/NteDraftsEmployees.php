<?php


class NteDraftsEmployees extends \Eloquent {

    
    protected $table = "nte_drafts_employees";

    protected $primaryKey = "nde_id";
    protected $hidden = array("password");

    public $timestamps = false;


}
?>