<?php
use app\RGAS;

class TMSExcusedNotify extends \Eloquent 
{
    protected $table = 'TMS_ExcusedNotify';
    public $timestamps = false;
  	protected $connection = 'sqlsrv';


	public static function getnotify($start , $end )
	{
		$requests = TMSExcusedNotify::whereBetween('Exc_Date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end)) ])
        ->whereRaw("TMS_ExcusedNotify.Exc_BranchID = (SELECT TOP 1 Branch_ID FROM RBC_HRMS.DBO.empjob_Info as a WHERE a.Employee_ID = '" . Session::get("new_employeeid") . "' and a.Current_Job ='Y') and 
                TMS_ExcusedNotify.TMS_Group = (SELECT TOP 1 TMS_Group FROM TMS_Employee WHERE TMS_Employee.Employee_ID = '" . Session::get("new_employeeid") . "') 
             ")
        ->where("Comp_Id","RBC")
        ->distinct()
        ->get(["Exc_Description","Exc_Date","Exc_DayTag"]);
        $notif = array();
        foreach($requests as $req) 
        {				
            $date = date("m/d/Y", strtotime($req->Exc_Date));
            if(in_array($req->Exc_DayTag, [1,2]))
                $notif["suspension"][] = $date .  " " . $req->Exc_Description;
            else 
                $notif["excused"][] = $date .  " " . $req->Exc_Description;
        }

        return $notif;
	}	




}