<?php

class ItrApplications extends \Eloquent {
    
    protected $table = 'itr_applications';
    public $incrementing = true;
    public $timestamps = false;
    
    
    //
    public static function get_application_lists($itr_no)
    {
        $application_lists = ItrApplications::leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_applications.application");
            
        })->where("itr_applications.itr_no", "=", "{$itr_no}")->get([
        
             "itr_applications.itr_no"
            ,"itr_applications.application"
            ,"config.system_desc"
            
        ]);
        
        return $application_lists;
        
    }
 
    
}
//End of file.