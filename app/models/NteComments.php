<?php


class NteComments extends \Eloquent {

    
    protected $table = "nte_comments";

    protected $primaryKey = "id";
    protected $hidden = array("password");

    public $timestamps = false;

}