<?php

class TrainingEndorsementsParticipants extends \Eloquent {
    
	protected $primaryKey = 'tep_id';
    protected $table = 'trainingendorsementsparticipants';
    public $incrementing = true;
    public $timestamps = false;
	protected $fillable = array('tep_id', 'tep_te_id', 'tep_ref_num', 'tep_emp_id', 'tep_emp_name', 'tep_dep_id', 'tep_dep_name', 'tep_designation');
	
}