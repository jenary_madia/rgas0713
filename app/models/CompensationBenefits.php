<?php
// use RGAS\Modules\CBR;
// use RGAS\Libraries;

class CompensationBenefits extends \Eloquent {
    
	protected $primaryKey = 'cb_id';
    protected $table = 'compensationbenefits';
    public $incrementing = true;
    public $timestamps = true;
	
	// protected $attachments_path = '/cbr/attachments';
	
	// public $_cb_status;
	
	// public $_cb_issent;
	
	public function cbrd()
	{
		return $this->hasMany('CompensationBenefitsRequestedDocs','cbrd_cb_id');
	}
	
	// public function getRequestByCbrdRefNum($cbrd_ref_num)
	// {
		// $cbrd = CompensationBenefitsRequestedDocs::where("cbrd_ref_num","=",$cbrd_ref_num)->first();
		// $requests = $this->with("cbrd")->where("cb_id","=",$cbrd->cbrd_cb_id)->first();

		// $cbrd = $this->with("cbrd", function($query) use ($cbrd_ref_num){
			// $query->where('cbrd_ref_num', '=', $cbrd_ref_num);
		// })->get();

		// return $requests;
	// }
	
}