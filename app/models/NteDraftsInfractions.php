<?php


class NteDraftsInfractions extends \Eloquent {

    
    protected $table = "nte_drafts_infractions";

    protected $primaryKey = "ndi_id";
    protected $hidden = array("password");

    public $timestamps = false;


}
?>