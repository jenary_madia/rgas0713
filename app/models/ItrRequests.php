<?php

class ItrRequests extends \Eloquent {
    
    protected $table = 'itr_requests';
    protected $primaryKey = 'itr_no';
    
    
    public $incrementing = false;
    public $timestamps = false;
    
    //
    public static function get_itr_details($itr_no)
    {
        
        $itr_request = ItrRequests::leftJoin("employees as emp", function($join){
          
            $join->on("emp.id", "=", "itr_requests.requestor");
            
        })->leftJoin("employees as emp2", function($join){
          
            $join->on("emp2.id", "=", "itr_requests.endorsed_by");
            
        })->leftJoin("employees as emp3", function($join){
          
            $join->on("emp3.id", "=", "itr_requests.endorsed2_by");
            
        })->leftJoin("employees as emp4", function($join){
          
            $join->on("emp4.id", "=", "itr_requests.bsa_curr_emp");
            
        })->leftJoin("employees as emp5", function($join){
          
            $join->on("emp5.id", "=", "itr_requests.conforme_by");
            
        })->leftJoin("departments as dep", function($join){
          
            $join->on("dep.id", "=", "itr_requests.departmentid");
            
        })->leftJoin("companylists as comp", function($join){
          
            $join->on("comp.comp_code", "=", "itr_requests.company");
            
        })->leftJoin("itr_custom_request as custom_itr", function($join){
          
            $join->on("custom_itr.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("itr_custom_companies as custom_comp", function($join){
          
            $join->on("custom_comp.custom_comp_id", "=", "itr_requests.company");
            
        })->leftJoin("employees as approved_emp", function($join){
          
            $join->on("approved_emp.id", "=", "itr_requests.approved_by");
            
        })->leftJoin("departments as dep2", function($join){
          
            $join->on("dep2.id", "=", "custom_itr.custom_dept_id");
            
        })->leftJoin("itr_recommendation as reco", function($join){
          
            $join->on("reco.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("itr_system_config as config", function($join){

            $join->on("config.system_code", "=", "itr_requests.request_type_desc");
          
        })->where("itr_requests.itr_no", "=", "{$itr_no}")->first([
        
             "itr_requests.trans_date"
            ,"itr_requests.requestor"
            ,"itr_requests.expected_date"
            ,"itr_requests.req_description"
            ,"itr_requests.departmentid"
            ,"itr_requests.company"
            ,"itr_requests.location"
            ,"itr_requests.section"
            ,"itr_requests.endorsed_by"
            ,"itr_requests.endorsed_date"
            ,"itr_requests.endorsed2_by"
            ,"itr_requests.endorsed2_date"
            ,"itr_requests.contact_no"
            ,"itr_requests.computer_code"
            ,"itr_requests.request_type"
            ,"itr_requests.request_type_desc"
            ,"itr_requests.request_type_desc_text"
            ,"itr_requests.application"
            ,"itr_requests.complexity"
            ,"itr_requests.urgency"
            ,"itr_requests.urgency_others"
            ,"itr_requests.urgency_code"
            ,"itr_requests.curr_emp"
            ,"itr_requests.bsa_completed"
            ,"itr_requests.citd_completed"
            ,"itr_requests.status"
            ,"itr_requests.itr_status"
            ,"itr_requests.rgs_status"
            ,"itr_requests.reviewed_by"
            ,"itr_requests.reviewed_by2"
            ,"itr_requests.reviewed_by3"
            ,"itr_requests.approved_by"
            ,"itr_requests.received_by"
            ,"itr_requests.received_date"
            ,"itr_requests.conforme_date"
            ,"itr_requests.conforme_by"
            ,"itr_requests.smdd_approved"
            ,"itr_requests.bsa_curr_emp"
            ,"itr_requests.bsa_assigned_date"
            ,"itr_requests.smdd_approved_date"
            ,"itr_requests.disabled_disapprove"
            ,"itr_requests.disabled_forward"
            ,"emp.employeeid"
            ,"emp.firstname"
            ,"emp.middlename"
            ,"emp.lastname"
            ,"emp.desig_level"
            ,"emp.company"
            ,"emp.designation"
            ,"dep.dept_name"
            ,"emp2.firstname as endrosed_firstname"
            ,"emp2.lastname as endrosed_lastname"
            ,"emp3.firstname as endrosed2_firstname"
            ,"emp3.lastname as endrosed2_lastname"
            ,"emp3.designation as endrosed2_designation"
            ,"emp3.desig_level as endrosed2_desig_level"
            ,"emp4.firstname as bsa_firstname"
            ,"emp4.middlename as bsa_middlename"
            ,"emp4.lastname as bsa_lastname"
            ,"custom_comp.custom_comp_nm"
            ,"reco.reference_no"
            ,"reco.status as reco_status"
            ,"reco.curr_emp as reco_curr_emp"
            ,"comp.comp_name"
            ,"custom_itr.requestor_name as custom_requestor_name"
            ,"custom_itr.custom_dept_id"
            ,"custom_itr.custom_designation"
            ,"config.system_desc as request_type_full_text"
            ,"approved_emp.firstname as approved_firstname"
            ,"approved_emp.middlename as approved_middlename"
            ,"approved_emp.lastname as approved_lastname"
            ,"emp5.firstname as conforme_firstname"
            ,"emp5.middlename as conforme_middlename"
            ,"emp5.lastname as conforme_lastname"
            ,"dep2.dept_name as custom_dept_name"
            
        ]);
        
        
        return $itr_request;
        
    }
    
    
    
    //Get User Itr Requests for datatables;
    public static function get_user_itr_requests($where_condition, $order_by_col, $order_by_col2 ,$order_by_sort)
    {
        
        $itr_lists = ItrRequests::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_requests.curr_emp");
            
        })->leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_system_config as config2", function($join){
            
            $join->on("config2.system_code", "=", "itr_requests.request_type");
            
        })->leftJoin("itr_system_config as config3", function($join){
            
            $join->on("config3.system_code", "=", "itr_requests.request_type_desc");
            
        })->leftJoin("itr_system_config as config4", function($join){
            
            $join->on("config4.system_code", "=", "itr_requests.curr_emp");
            
        })->leftJoin("itr_assigned as assd", function($join){
            
            $join->on("assd.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("employees as emp2", function($join){
            
            $join->on("emp2.id", "=", "itr_requests.requestor"); 
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort)->groupBy("itr_requests.itr_no")->get([
        
             "itr_requests.itr_no"
            ,"itr_requests.trans_date"
            ,"itr_requests.request_type"
            ,"itr_requests.request_type_desc"
            ,"itr_requests.rgs_status"
            ,"itr_requests.itr_status"
            ,"itr_requests.curr_emp"
            ,"itr_requests.uss_curr_emp"
            ,"itr_requests.bsa_curr_emp"
            ,"itr_requests.req_description"
            ,"itr_requests.uss_view"
            ,"itr_requests.request_type"
            ,"emp.firstname as current_fname"
            ,"emp.lastname as current_lname"
            ,"config.system_desc"
            ,"config2.system_desc as request_type_text"
            ,"config3.system_desc"
            ,"config4.system_desc"
            ,"emp2.firstname as requestor_fname"
            ,"emp2.lastname as requestor_lastname"
            
        ]);
        
        
        return $itr_lists; 
        
    }
    
    
    
    //
    public static function get_itr_for_approval($where_condition, $order_by_col, $order_by_col2, $order_by_sort)
    {
        
        $itr_approval_lists = ItrRequests::leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_requests.requestor");
            
        })->leftJoin("itr_system_config as config", function($join){
            
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_system_config as config2", function($join){
            
            $join->on("config2.system_code", "=", "itr_requests.request_type");
            
        })->leftJoin("itr_system_config as config3", function($join){
            
            $join->on("config3.system_code", "=", "itr_requests.request_type_desc");
            
        })->leftJoin("itr_custom_request as custom", function($join){
            
            $join->on("custom.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("itr_custom_request as custom_itr", function($join){
          
            $join->on("custom_itr.itr_no", "=", "itr_requests.itr_no");
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort)->groupBy("itr_requests.itr_no")->get([
        
             "itr_requests.itr_no"
            ,"itr_requests.requestor"
            ,"itr_requests.trans_date"
            ,"itr_requests.request_type"
            ,"itr_requests.request_type_desc"
            ,"itr_requests.rgs_status"
            ,"itr_requests.itr_status"
            ,"itr_requests.req_description"
            ,"itr_requests.curr_emp"
            ,"itr_requests.uss_curr_emp"
            ,"itr_requests.bsa_curr_emp"
            ,"itr_requests.uss_view"
            ,"emp.firstname as requestor_fname"
            ,"emp.lastname as requestor_lname"
            ,"config.system_desc"
            ,"config2.system_desc as request_type_text"
            ,"custom_itr.requestor_name as custom_requestor_name"
            ,"custom_itr.custom_dept_id"
            ,"custom_itr.custom_designation"
            
        ]);
        
        return $itr_approval_lists;
    }
    
    
    
    //Get assigned itr
    public static function get_assigned_itr ($where_condition, $order_by_col, $order_by_col2, $order_by_col3,  $order_by_sort, $order_by_sort2, $order_by_sort3)
    {
        
        $assigned_itr = ItrRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "itr_requests.requestor");
            
        })->leftJoin("itr_system_config as config", function($join){
        
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_assigned as assigned", function($join){
            
            $join->on("assigned.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("employees as assigned_emp", function($join){
            
            $join->on("assigned_emp.id", "=", "assigned.employee_id");
            
        })->leftJoin("itr_custom_request as itr_custom", function($join){
        
            $join->on("itr_custom.custom_requestor_id", "=", "itr_requests.requestor");
            
        })->leftJoin("itr_trails as trail", function($join){
        
            $join->on("trail.itr_no", "=", "itr_requests.itr_no");
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort2)->orderBy($order_by_col3, $order_by_sort3)->groupBy("itr_requests.itr_no")->get([
        
            "itr_requests.itr_no"
           ,"itr_requests.req_description"
           ,"itr_requests.itr_status"
           ,"itr_requests.trans_date"
           ,"itr_requests.conforme_date"
           ,"itr_requests.rgs_status"
           ,"itr_requests.citd_completed"
           ,"itr_requests.status"
           ,"itr_requests.request_type"
           ,"config.system_desc as itr_status_text"
           ,"emp.firstname"
           ,"emp.lastname"
           ,"itr_custom.requestor_name as custom_requestor"
           
        ]);
        
        return $assigned_itr;
        
    }
    
    
    
    //Get ITR for csv reporting
    public static function get_itr_report($where_condition, $order_by_col, $order_by_col2, $order_by_col3,  $order_by_sort, $order_by_sort2, $order_by_sort3)
    {
        
         $assigned_itr = ItrRequests::leftJoin("employees as emp", function($join){
        
            $join->on("emp.id", "=", "itr_requests.requestor");
            
        })->leftJoin("itr_custom_request as itr_custom", function($join){
        
            $join->on("itr_custom.custom_requestor_id", "=", "itr_requests.requestor");
            
        })->leftJoin("itr_custom_companies as custom_comp", function($join){
                
            $join->on("custom_comp.custom_comp_id", "=", "itr_requests.company");
            
        })->leftJoin("itr_system_config as config", function($join){
        
            $join->on("config.system_code", "=", "itr_requests.itr_status");
            
        })->leftJoin("itr_assigned as assigned", function($join){
            
            $join->on("assigned.itr_no", "=", "itr_requests.itr_no");
            
        })->leftJoin("employees as assigned_emp", function($join){
            
            $join->on("assigned_emp.id", "=", "assigned.employee_id");
            
        })->leftJoin("employees as endorsed_emp", function($join){
          
            $join->on("endorsed_emp.id", "=", "itr_requests.endorsed_by");
            
        })->leftJoin("departments as dept", function($join){
            
            $join->on("dept.id", "=", "emp.departmentid");
            
        })->leftJoin("itr_system_config as config2", function($join){
            
            $join->on("config2.system_code", "=", "itr_requests.request_type_desc");
            
        })->whereRaw("{$where_condition}")->orderBy($order_by_col, $order_by_sort)->orderBy($order_by_col2, $order_by_sort2)->orderBy($order_by_col3, $order_by_sort3)->groupBy("itr_requests.itr_no")->get([
        
            "itr_requests.itr_no"
           ,"itr_requests.req_description"
           ,"itr_requests.itr_status"
           ,"itr_requests.trans_date"
           ,"itr_requests.company"
           ,"itr_requests.conforme_date"
           ,"itr_requests.endorsed_date"
           ,"itr_requests.rgs_status"
           ,"itr_requests.location"
           ,"itr_requests.citd_completed"
           ,"itr_requests.status"
           ,"itr_requests.request_type"
           ,"itr_requests.request_type_desc"
           ,"itr_requests.request_type_desc_text"
           ,"itr_requests.complexity"
           ,"itr_requests.urgency_code"
           ,"itr_requests.smdd_approved_date"
           ,"itr_custom.requestor_name as custom_requestor"
           ,"config.system_desc as itr_status_text"
           ,"emp.firstname"
           ,"emp.lastname"
           ,"dept.dept_name"
           ,"endorsed_emp.firstname as endorsed_firstname"
           ,"endorsed_emp.lastname as endorsed_lastname"
           ,"config2.system_desc as request_for"
           ,"custom_comp.custom_comp_nm"
           
        ]);
        
        return $assigned_itr;
        
    }
    
    
    
    //
	public static function get_sys_config_desc($system_code)
	{
        $description = "";
		$system_description = DB::table("itr_system_config")->where("system_code", "=", "{$system_code}")->first(["system_desc"]);
        
        if(!empty($system_description))
        {
            $description = $system_description->system_desc;
        }
        
        return $description;
	}
    
    
    
    //
    public static function get_emp_details($employee_id)
    {
        $emp = Employees::leftJoin("departments as dept", function($join){
            
            $join->on("dept.id", "=", "employees.departmentid");
                
        })->where("employees.id", "=", "{$employee_id}")
        
        ->first(["employees.id", "employees.firstname", "employees.middlename", "employees.lastname", "employees.desig_level", "employees.departmentid", "employees.designation", "employees.company", "dept.dept_name"]);
        
        return $emp;
    }
    
    
    //Get superiorid
    public static function get_superior($employee_id)
    {
     
        $superior_id = "";
        
        //Get department head and substitute approver as DH.
        $approver = Employees::leftJoin("employees as emp2", function($join){
        
            $join->on("employees.superiorid", "=", "emp2.id");
            
        })->where("employees.id", "=", "{$employee_id}")->where("emp2.active", "=", "1")->first([
             "employees.superiorid"
            ,"emp2.onleave"
            ,"emp2.artapprovalid"
        ]);
        
        if($approver->onleave != 1) {
            
            $superior_id = $approver->superiorid;
        }
        else {
        
            $superior_id = $approver->artapprovalid;
        }   
        
        return $superior_id;
        
    }
    
    
    
    //get department head id
    public static function get_department_head($employee_id, $department_id)
    {
        
        $department_head_id = "";
        
        //Get department head and substitute approver as DH.
        $approver = Employees::whereRaw("desig_level = 'head' AND departmentid = {$department_id} AND (active = 1 || active = 2)")->first([
             'employees.id'
            ,'employees.onleave'
            ,'employees.artapprovalid'						
        ]); 
        
        if(!empty($approver))
        {
        
            if($approver->onleave != 1){
            
                $department_head_id = $approver->id;
            }
            else{
            
                $department_head_id = $approver->artapprovalid;
            }   
           
            
            //if $curr_emp is same as the employee id of the requestor. get the next approver of his/her superior.
            if($department_head_id == $employee_id)
            {
                //Get department head superior id
                $approver = Employees::leftJoin("employees as employees2", function($join){
                
                    $join->on('employees2.id', '=', 'employees.superiorid');
                
                })->whereRaw("employees.id = {$approver->id} AND employees.active <> 0")->first([
                
                     'employees.superiorid'
                    ,'employees2.id'
                    ,'employees2.onleave'
                    ,'employees2.artapprovalid'
                    
                ]);
            
                $department_head_id = $approver->superiorid;
            }
            
        }
        else
        {
            $department_head_id = "";
        }
        
        
        return $department_head_id;
        
    }
    
    
    //
    public static function get_division_head($employee_id, $desig_level, $department_id)
    {
        
        $div_head_id = "";
        
        if (strtolower($desig_level) == "head")
        {
            
            $approver = Employees::leftJoin("employees as emp", function($join){
               
                $join->on("emp.id", "=", "employees.superiorid");
                
            })->where("employees.id", "=", "{$employee_id}")->first([
            
                 "employees.superiorid"
                ,"emp.id"
                ,"emp.desig_level"
                
            ]);
            
        }
        else if (strtolower($desig_level) == "supervisor")
        {
            
            $head_id = ItrRequests::get_department_head($employee_id, $department_id);
            
            $approver = Employees::leftJoin("employees as emp", function($join){
               
                $join->on("emp.id", "=", "employees.superiorid");
                
            })->where("employees.id", "=", "{$head_id}")->first([
            
                 "employees.superiorid"
                ,"emp.id"
                ,"emp.desig_level"
                
            ]);
            
        }
        
        //
        if ( preg_replace('/\s+/', '', $approver->desig_level)  == "topmngt" || preg_replace('/\s+/', '', $approver->desig_level)  == "supervisor")
        {
            $div_head_id = $approver->id;
        }
        
        return $div_head_id;
    }
    

    
    //
    public static function get_vpo($company)
    {
        
        $vpo_id = "";
        
        $approver = Employees::whereRaw("desig_level = 'vpo' AND company = '{$company}' AND (active = 1 || active = 2)")->first([
             'employees.id'
            ,'employees.onleave'
            ,'employees.artapprovalid'						
        ]); 
        
        if(!empty($approver))
        {
        
            if($approver->onleave != 1){
            
                $vpo_id = $approver->id;
            }
            else{
            
                $vpo_id = $approver->artapprovalid;
            }
        }
        else
        {
            $vpo_id = "";
        }
        
        return $vpo_id;;
        
    }

    
    //Check if has itr for approval
    public static function check_for_itr_approval($employee_id)
    {   
        // where("requestor", "<>", "{$employee_id}")
        $itr_for_approval = ItrRequests::where("curr_emp", "=", "{$employee_id}")->get(["itr_no"]);
        
		return $itr_for_approval;
    }
    
     
    //get itr assigned lists
    public static function get_itr_assigned_list($itr_no)
    {
        
        $itr_assigned_lists = DB::table("itr_assigned")->leftJoin("employees as emp", function($join){
            
            $join->on("emp.id", "=", "itr_assigned.employee_id");
            
        })->where("itr_assigned.itr_no", "=", "{$itr_no}")->get([
        
            "itr_assigned.itr_no"
           ,"itr_assigned.employee_id"
           ,"itr_assigned.assigned_date"
           ,"itr_assigned.ts_assigned"
           ,"emp.firstname"
           ,"emp.lastname"
           
        ]);
        
        return $itr_assigned_lists;
        
    }
    
    
    //
    public static function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }
    

    //
    public static function check_citd_employee($employee_id)
    {
        
        $citd_staff = Employees::where("id", "=" ,"{$employee_id}")->where(function ($query){
            
            $query->where("departmentid", "=", 7)->orWhere("departmentid", "=", 126)->orWhere("sectionid", "=", 1);
            
        })->first(["id"]);
        
        
        if ( !empty($citd_staff) )
        {
            return TRUE;
        }
        else
        {
            return false;
        }
        
    }
    
    
    //
    public static function get_citd_employee($employee_id)
    {
        
        if ( Cache::has ("citd_staff") )
        {
            $citd_staff = Cache::get("citd_staff");
        }
        else
        {
            $citd_staff = Employees::where(function ($query){
                
                $query->where("departmentid", "=", 7)->where("active" ,"=", "1")->orWhere("departmentid", "=", 126)->orWhere("sectionid", "=", 1);
                
            })->orderBy("firstname", "asc")->get(["id", "firstname", "lastname"]);
            
            Cache::add("citd_staff", $citd_staff, 1440);
        }
        
        return $citd_staff;
        
    }
    
    
    //
    public static function check_user_support($employee_id)
    {
       
        $user_support = Receivers::where("employeeid", "=", "{$employee_id}")->where("code", "=", "ITR-USS")->first(["employeeid"]);
        
        if ( !empty($user_support) ) //if user support
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    

    public static function get_plant_aom($company)
    {
        
        $plant_aom_id = "";
        
        try {
            
            $aom_reciever = Employees::where("desig_level", "=", "plant-aom")->where("company", "=", "{$company}")->first(["id"]);
            
            $plant_aom_id = $aom_reciever->id;
            
        }catch (\Exception $e) {
                
            throw $e;
            die;
            
        }
        
        return $plant_aom_id;
        
    }
    
    
    public static function get_plant_om($company)
    {
        
        $plant_om_id = "";
        
        try {
            
            $om_reciever = Employees::where("desig_level", "=", "om")->where("company", "=", "{$company}")->first(["id"]);
            
            $plant_om_id = $om_reciever->id;
            
        }catch (\Exception $e) {
            
            throw $e;
            die;
            
        }
        
        return $plant_om_id;
        
    }
    
    
    //
    public static function get_it_buk()
    {
        
        $it_buk_id = "";
        
        try {
        
            $it_buk = Receivers::where("module", "=", "itr")->where("code", "=", "ITR-IT-BUK")->first(["employeeid"]);
            $it_buk_id = $it_buk->employeeid;
        
        }catch (\Exception $e) {
            
            throw $e;
            die;
            
        }
        
        return $it_buk_id;
        
    }
    
    
    //@return bool
    public static function check_satellite_systems($employee_id)
    {
        
        $systems_sat = Receivers::where("code", "Like", "ITR-SD%")->where("employeeid", "=", "{$employee_id}")->first(["employeeid"]);
        
        if ( count($systems_sat) > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        
    }
    
    //
    public static function get_email_add($employee_id)
    {
    
        $email_add = "";
        
        $emp = new Employees;
        $emp_query = $emp->where("employees.id", "=", "{$employee_id}")->first(["employees.email"]);
        
        $email_add = $emp_query->email;
        
        return $email_add ;
    }
    
}
//End of file.