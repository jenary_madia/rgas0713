<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 03/08/2016
 * Time: 11:31 AM
 */
class NotificationBatch extends \Eloquent
{
    protected $table = 'notification_batch';
    public $incrementing = true;
    public $timestamps = false;
    public $guarded =  array('id');

    public function generateCodeNumber()
    {
        $documentCodeToday = date('Y-m');
        $latestDocumentCode = NotificationBatch::max('documentcode');
        if ($latestDocumentCode >= $documentCodeToday)
        {
            $latestCodenumber = NotificationBatch::where('documentcode','=',$latestDocumentCode)->max('codenumber');
            if (! $latestCodenumber) {
                return '00000';
            }else{
                return str_pad($latestCodenumber + 1, 5, '0', STR_PAD_LEFT);
            }

        }else{

            return str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

        }

    }
    
    public function newBatch()
    {
        $notificationBatch = new NotificationBatch();
        $notificationBatch->documentcode = date('Y-m');
        $notificationBatch->codenumber = $this->generateCodeNumber();
        $notificationBatch->company = Session::get("company");
        $notificationBatch->departmentid = Input::get("department");
        $notificationBatch->sectionid = Input::get("section");
        $notificationBatch->status = 'For Approval';
        $notificationBatch->comment = json_encode(array(
            'name'=>Session::get('employee_name'),
            'datetime'=>date('m/d/Y g:i A'),
            'message'=> Input::get("comment")
        ));
        $notificationBatch->datecreated = date('Y-m-d');
        $notificationBatch->ownerid = Session::get("employee_id");
        $notificationBatch->curr_emp = Session::get("superiorid");
        
        if ($notificationBatch->save()) {
            return $notificationBatch->id;
        }else{
            return 0;
        }
    }

    public function myNotifications()
    {
        $requests = NotificationBatch::with("employee")
            ->where('ownerid',Session::get("employee_id"))
            ->where('status','NOT LIKE','%DELETED%')
            ->with('department')
            ->with('section')
            ->get();
        $result_data["iTotalDisplayRecords"] = count($requests); //total count filtered query
        $result_data["iTotalRecords"] = count($requests);
        if ( count($requests) > 0){
            $ctr = 0;
            foreach($requests as $req) {
                $deletableNotifications = (in_array($req['status'], json_decode(DELETABLE,true)));
                $editableNotifications = (in_array($req['status'], json_decode(EDITABLE,true)));
                $btnDelete = "<button ". ($deletableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' name='action' value='softDelete|{$req['id']}'>Delete</button>";
                $btnView = "<a class='btn btn-default btndefault' href=".url('/ns/pe_view_my_request/'.$req['id'].'/view').">View</a>";
                $btnEdit = "<a ". ($editableNotifications ? '' : 'disabled' ) ." class='btn btn-default btndefault' href=".url('/ns/pe_view_my_request/'.$req['id'].'/edit').">Edit</a>";
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '');
                $result_data["aaData"][$ctr][] = $req['status'];
                $result_data["aaData"][$ctr][] = ($req['employee']? $req['employee']['firstname']." ".$req['employee']['lastname']: "");
                $result_data["aaData"][$ctr][] = $btnDelete.' '.$btnView.' '.$btnEdit;

                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $requests;
        }
        return $result_data;
    }

    public function employee()
    {
        return $this->hasOne('Employees','id','curr_emp')->select(array('firstname','middlename','lastname','id'));
    }

    /*--------------------------for data table of for approve notifications"-----------------------------*/
    public function superiorNotifications()
    {
        $forApproveNotifications = NotificationBatch::where('curr_emp', Session::get('employee_id'))
            ->whereIn('status', array('FOR APPROVAL','RETURNED'))
            ->with('department')
            ->with('section')
            ->with('owner')
            ->get();
        $result_data["iTotalDisplayRecords"] = count($forApproveNotifications); //total count filtered query
        $result_data["iTotalRecords"] = count($forApproveNotifications);
        if ( count($forApproveNotifications) > 0){
            $ctr = 0;
            foreach($forApproveNotifications as $req) {
                $result_data["aaData"][$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data["aaData"][$ctr][] = $req['datecreated'];
                $result_data["aaData"][$ctr][] = $req['department']['dept_name'];
                $result_data["aaData"][$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data["aaData"][$ctr][] = $req['status'] ;
                $result_data["aaData"][$ctr][] = ($req['owner '] ? $req['owner']['firstname']." ".$req['owner']['lastname']: "----");
                $result_data["aaData"][$ctr][] = "<a class='btn btn-default btndefault' href=".url('/ns/pe_view/'.$req['id']).">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/ns/pe_view/'.$req['id'].'/approve').">Approve</a>";
                $ctr++;
            }
        }
        else {
            $result_data["aaData"] = $forApproveNotifications;
        }
        return $result_data;
    }
    
/*--------------------------for data table of "for submitted notifications to Receiver"-----------------------------*/
    public function getNotif()
    {
        $receiverLists = NotificationBatch::whereIn('status', ['APPROVED','APPROVED/DELETED'])
            ->where('curr_emp', Session::get('employee_id'))
            ->get();
        return $receiverLists;
    }

    /*-------------------SEARCH PE RECEIVER ----------------*/
    public function searchPEReceiver()
    {
        $receiverLists = NotificationBatch::whereIn('status',['APPROVED','APPROVED/DELETED'])
            ->where('curr_emp', Session::get('employee_id'))
            ->with('owner')
            ->with('department')
            ->with('section')
            ->forReceiver()
            ->get();
        $result_data = [];
        if ( count($receiverLists) > 0){
            $ctr = 0;
            foreach($receiverLists as $req) {
                $result_data[$ctr][] = $req['documentcode'].'-'.$req['codenumber'];
                $result_data[$ctr][] = $req['datecreated'];
                $result_data[$ctr][] = $req['dateapproved'];
                $result_data[$ctr][] = $req['department']['dept_name'];
                $result_data[$ctr][] = ($req['section']['sect_name'] ? $req['section']['sect_name'] : '---');
                $result_data[$ctr][] = $req['owner']['firstname'].' '.$req['owner']['lastname'];
                $result_data[$ctr][] = "<a class='btn btn-default btndefault' href=".url('/submitted/pe_notification/'.$req['id'].'/view').">View</a><a style='margin-left: 3px' class='btn btn-default btndefault' href=".url('/submitted/pe_notification/'.$req['id'].'/process').">Process</a>";
                $ctr++;
            }
        }

        return $result_data;

    }

    public function scopeForReceiver($query) {
        if (Input::has("department")) {
            if(Input::get("department") != 0) {
                $query->where("departmentid",Input::get("department"));
            }
        }

        if (Input::has("section")) {
            $query->where("sectionid",Input::get("section"));
        }

        if (Input::has("dateFrom")) {
            if(Input::has("dateTo")) {
                $query->whereBetween("dateapproved",[Input::get("dateFrom"),Input::get("dateTo")]);
            }else{
                $query->where("dateapproved",Input::get("dateFrom"));
            }
        }

    }

    /*-------------------PRINT pe RECEIVER------------------------------*/

    public function printPEReceiver()
    {
        $receiverLists = NotificationBatch::whereIn('status',['APPROVED','APPROVED/DELETED'])
            ->where('curr_emp', Session::get('employee_id'))
            ->with('owner')
            ->with('department')
            ->with('section')
            ->with('batchDetails')
            ->forReceiver()
            ->get();


        return $receiverLists;

    }

/*-------------------PRINT RECEIVER------------------------------*/
    public function printNotif($data)
    {
        $receiverLists = NotificationBatch::where('status', 'APPROVED')
            ->where('curr_emp', Session::get('employee_id'))
            ->printNotif($data[0])
            ->get();
        return $receiverLists;
    }

    public function scopePrintNotif($query,$data)
    {
        if (! $data->department === 0) {
            $query->where("departmentid",$data->department);
        }

//        if (property_exists($data,'section')) {
//            $query->where("sectionid",$data->section);
//        }
//
//        if (property_exists($data,'dateFrom')) {
//            if (property_exists($data,'dateTo')) {
//                $query->whereBetween('datecreated', array($data->dateFrom, $data->dateTo));
//            }else{
//                $query->where('datecreated', $data->dateFrom);
//            }
//        }

    }
/*--------------------Join with Notification-------------------*/
    public function batchDetails()
    {
        return $this->hasMany('NotificationBatchDetails','notification_batch_id','id')->with("notif");
    }

    public function department()
    {
        return $this->hasOne('Departments','id','departmentid');
    }

    public function section()
    {
        return $this->hasOne('Sections','id','sectionid');
    }

    public function owner()
    {
        return $this->hasOne('Employees','id','ownerid')->select(array('firstname','middlename','lastname','id'));
    }
}
