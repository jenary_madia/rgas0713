<?php


class Signatory extends \Eloquent {
    
    protected $table = 'leave_signatories';
    public $incrementing = false;
    public $timestamps = false;
    public $guarded = ["id"];
    
    /*-------------ADDING SIGNATORY--------------*/
    public static function store($paramsSignatory)
    {   
        $toInsert = [];
        $leaves = [];
        if (! is_array($paramsSignatory["leaveId"])) {
            array_push($leaves,$paramsSignatory["leaveId"]);
        }else{
            $leaves = $paramsSignatory["leaveId"];
        }

        foreach ($leaves as $leave){
            array_push($toInsert, array(
                    'leave_id' => $leave,
                    'signature_type' => $paramsSignatory["signatureType"],
                    'signature_type_seq' => Signatory::checkSignatorySeq($leave),
                    'employee_id' => Session::get('employee_id'),
                    'approval_date' => date('Y-m-d')
                ));
        }

        if ($toInsert != []) {
            Signatory::insert($toInsert);
            return 1;
        }
            return 0;
    }

    private static function checkSignatorySeq($id){
        $maxSeq = Signatory::where('leave_id',$id)->max('signature_type_seq');
        if (! $maxSeq )
        {
            return 1;
        }else{
            return $maxSeq + 1;
        }
    }

    /*-------------Join with employees' table--------------*/
    public function employee()
    {
        return $this->belongsTo('Employees','employee_id')->select(array('firstname','middlename','lastname','id'));
    }

}