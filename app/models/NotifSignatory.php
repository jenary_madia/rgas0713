<?php

/**
 * Created by PhpStorm.
 * User: Jenary
 * Date: 23/07/2016
 * Time: 5:58 PM
 */
class NotifSignatory extends \Eloquent
{
    protected $table = 'notification_signatories';
    public $incrementing = false;
    public $timestamps = false;
    public $guarded = ["id"];

    public static function checkSignatorySeq($id){
        $maxSeq = NotifSignatory::where('notification_id',$id)->max('signature_type_seq');
        if (! $maxSeq )
        {
            return 1;
        }else{
            return $maxSeq + 1;
        }
    }

    /*-------------Join with employees' table--------------*/
    public function employee()
    {
        return $this->belongsTo('Employees','employee_id')->select(array('firstname','middlename','lastname','id'));
    }
    /*-------------ADDING SINGLE SIGNATORY--------------*/
    public static function store($paramsSignatory)
    {
        $toInsert = [];
        $notifs = [];
        if (! is_array($paramsSignatory["notifID"])) {
            array_push($notifs,$paramsSignatory["notifID"]);
        }else{
            $notifs = $paramsSignatory["notifID"];
        }

        foreach ($notifs as $notif){
            array_push($toInsert, array(
                'notification_id' => $notif,
                'signature_type' => $paramsSignatory["signatureType"],
                'signature_type_seq' => NotifSignatory::checkSignatorySeq($notif),
                'employee_id' => Session::get('employee_id'),
                'approval_date' => date('Y-m-d')
            ));
        }

        if ($toInsert != []) {
            NotifSignatory::insert($toInsert);
            return 1;
        }
        return 0;
    }

    public static function deleteSignatories($id) {
        if ($id) {
            NotifSignatory::where('notification_id',$id)->delete();
        }
    }
}