<?php


class ItrComments extends \Eloquent {
    
    protected $table = "itr_comments";
    public $incrementing = true;
    public $timestamps = false;
     
    
    //    
    public static function get_comments($itr_no)
    {
        
		$comment_list = ItrComments::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "itr_comments.employee_id");
		
		})->where("itr_comments.itr_no", "=", "{$itr_no}")->get([
		
			 "itr_comments.employee_id"
			,"itr_comments.comment"
            ,"itr_comments.ts_comment"
			,"emp.firstname"
			,"emp.lastname"
			
		]);
		
		return $comment_list;
        
    }
    
    
     
    //
    public static function insert_comment($employee_id, $itr_no, $comment)
    {
        
        $itr_comment = new ItrComments;
        
        $itr_comment->itr_no = $itr_no;
        $itr_comment->employee_id = $employee_id;
        $itr_comment->comment = $comment;
        
        $itr_comment->save();
        
    }
   
}

//End of file