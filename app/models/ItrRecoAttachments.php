<?php

class ItrRecoAttachments extends \Eloquent {
    
    protected $table = 'itr_reco_attachments';
    protected $primaryKey = 'attachment_id';
    
    public $incrementing = true;
    public $timestamps = false;
    
    
    public static function insert_reco_attachment($reference_no, $attachment, $employee_id, $date)
    {
        
        $reco_attachment = new ItrRecoAttachments;
        
        $reco_attachment->reference_no = $reference_no;
        $reco_attachment->fn = $attachment;
        $reco_attachment->uploaded_by = $employee_id;
        $reco_attachment->uploaded_date = $date;
        
        $reco_attachment->save();
        
    }
 
    //
    public static function get_reco_attachments($reference_no)
    {
        
        $reco_attachment = ItrRecoAttachments::where("reference_no", "=", "{$reference_no}")->get([
            "attachment_id"
           ,"fn"
        ]);
        
        return $reco_attachment;   
    }
    
}