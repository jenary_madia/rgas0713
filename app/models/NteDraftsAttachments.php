<?php


class NteDraftsAttachments extends \Eloquent {

    
    protected $table = "nte_drafts_attachments";

    protected $primaryKey = "nda_id";
    protected $hidden = array("password");

    public $timestamps = false;


}
?>