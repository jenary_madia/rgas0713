<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\OS;

class OfficeSales extends \Eloquent 
{
	protected $table = 'orderentries';
    protected $guarded = array('id');
    public $timestamps = false;

    public static function filer_record($id)
    {
        $record = OfficeSales::where("orderentries.id" , $id)
        ->where("ownerid" , Session::get("employee_id"))
        ->where("isdeleted",0)
        ->leftJoin("officesale_payments","orderentryid","=","orderentries.id")
        ->get(["officesale_payments.status as pstats","officesale_payments.id as payment","payment_type","officesale_payments.remarks as pmark","payment_ref_no","payment_date","payment_amount",
            "orderentries.status","deliver_to","sales_type","section","contact_no","delivery_add","delivery_remarks"
            ,"delivery_tel","so_code","order_type","reference_no","delivery_date","section","company","comment_save","orderentries.remarks","trans_date"]);

       if($record->isEmpty()) return Redirect::to('/');
        $status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
        $data["status"] = $status[$record[0]->status];

        $data["section"] = $record[0]->section;
        $data["contact"] = $record[0]->contact_no;
        $data["sales_type"] = $record[0]->sales_type;
        $data["deliver_to"] = $record[0]->deliver_to;
        $data["section"] = $record[0]->section;
        $data["delivery_add"] = $record[0]->delivery_add;
        $data["delivery_tel"] = $record[0]->delivery_tel;
        $data["delivery_remarks"] = $record[0]->delivery_remarks;
        $data["so_code"] = $record[0]->so_code;
        $data["order_type"] = $record[0]->order_type;
        $data["delivery_date"] = $record[0]->delivery_date;
        $data["reference_no"] = $record[0]->reference_no;
        $data["comment_save"] = $record[0]->comment_save;
        $data["comment"] = $record[0]->remarks ;
        $data["date_filed"] = $record[0]->trans_date;

        $data["payment_status"] = $record[0]->pstats;
        $data["payment"] = $record[0]->payment;
        $data["payment_type"] = $record[0]->payment_type;
        $data["payment_amount"] = $record[0]->payment_amount;
        $data["payment_date"] = $record[0]->payment_date;
        $data["payment_reference_num"] = $record[0]->payment_ref_no;
        $data["payment_remarks"] = $record[0]->pmark;

        $company = $record[0]->company == "BUK" ? "BUK" : "RBC-CORP";
        if($record[0]->sales_type == "FOOD")
        {
            $record = DB::table("orderdetails")
            ->join("products","products.prod_code" , "=" , "orderdetails.prod_code")
            ->where("orderentryid" , $id)
            ->where("comp_group", $company)
            ->get(["prod_name","orderdetails.prod_code","uom_code","order_qnty","price"]);

             $data["detail"] = $record;
        }
        else
        {
            $record = DB::table("orderdetails")
            ->join("officesale_project_items","officesale_project_items.code" , "=" , "orderdetails.prod_code")
            ->join("officesale_projects","officesale_project_items.project_id" , "=" , "officesale_projects.project_id")
            ->where("orderentryid" , $id)
            ->where("officesale_project_items.comp_group", $company)
            ->get(["desc as prod_name","orderdetails.prod_code","uom_code","order_qnty","officesale_project_items.price","selling_from","selling_to"]);

             $data["detail"] = $record;
        }


        return $data;
    }

    public static function employee_record($id)
    {
        $record = OfficeSales::where("orderentries.id" , $id)
        ->where("curr_emp" , Session::get("employee_id"))
        //->where("isdeleted",0)
        ->leftJoin("officesale_payments","orderentryid","=","orderentries.id")
        ->get(["officesale_payments.status as pstats","officesale_payments.id as payment","payment_type","officesale_payments.remarks","payment_ref_no","payment_date","payment_amount",
            "cust_code","cust_name","department","company","trans_date","orderentries.status","orderentries.remarks as comment",
            "deliver_to","sales_type","section","contact_no","delivery_add","delivery_remarks"
            ,"delivery_tel","so_code","order_type","reference_no","delivery_date","section","company"]);

       if($record->isEmpty()) return Redirect::to('/');


        $status = ["N"=>"New" , "R"=>"Received", "O"=>"Ordered", "C"=>"Cancelled"];
        $data["emp_name"] = $record[0]->cust_name;
        $data["emp_no"] = $record[0]->cust_code;
        $data["department"] = $record[0]->department;
        $data["company"] = $record[0]->company;
        $data["date_filed"] = $record[0]->trans_date;
        $data["status"] = $status[$record[0]->status];

        $data["payment_status"] = $record[0]->pstats;
        $data["payment"] = $record[0]->payment;
        $data["payment_type"] = $record[0]->payment_type;
        $data["payment_amount"] = $record[0]->payment_amount;
        $data["payment_date"] = $record[0]->payment_date;
        $data["payment_reference_num"] = $record[0]->payment_ref_no;
        $data["payment_remarks"] = $record[0]->remarks;
        $data["remarks"] = $record[0]->comment;
        $data["comment"] = $record[0]->comment;
        $data["section"] = $record[0]->section;
        $data["contact"] = $record[0]->contact_no;
        $data["sales_type"] = $record[0]->sales_type;
        $data["deliver_to"] = $record[0]->deliver_to;
        $data["section"] = $record[0]->section;
        $data["delivery_add"] = $record[0]->delivery_add;
        $data["delivery_tel"] = $record[0]->delivery_tel;
        $data["delivery_remarks"] = $record[0]->delivery_remarks;
        $data["so_code"] = $record[0]->so_code;
        $data["order_type"] = $record[0]->order_type;
        $data["delivery_date"] = $record[0]->delivery_date;
        $data["reference_no"] = $record[0]->reference_no;

        $company = $record[0]->company == "BUK" ? "BUK" : "RBC-CORP";
        if($record[0]->sales_type == "FOOD")
        {
            $record = DB::table("orderdetails")
            ->join("products","products.prod_code" , "=" , "orderdetails.prod_code")
            ->where("orderentryid" , $id)
            ->where("comp_group", $company)
            ->get(["prod_name","orderdetails.prod_code","uom_code","order_qnty","price"]);

             $data["detail"] = $record;
        }
        else
        {
            $record = DB::table("orderdetails")
            ->join("officesale_project_items","officesale_project_items.code" , "=" , "orderdetails.prod_code")
            ->where("orderentryid" , $id)
            ->where("comp_group", $company)
            ->get(["desc as prod_name","orderdetails.prod_code","uom_code","order_qnty","officesale_project_items.price"]);

             $data["detail"] = $record;
        }


        return $data;
    }

    public static function attachments($id)
    {
            $table = DB::table("officesale_attachments")
                ->where("orderentryid",$id)
                ->where("attachment_type",2)
                ->get(["fn","code","id","attachment_type"]);
            
            return $table;
    }

    public static function food_record($id)
    {
        $record = DB::table("products")
        ->where("id" , $id)
        ->where("comp_group" , Session::get("company"))
        ->get(["prod_code","comp_code","prod_name","def_uom_code","status","comp_group"]);

       if(empty($record)) return Redirect::to('/');

        $data["prod_code"] = $record[0]->prod_code;
        $data["comp_code"] = $record[0]->comp_code;
        $data["prod_name"] = $record[0]->prod_name;
        $data["def_uom_code"] = $record[0]->def_uom_code;
        $data["status"] = $record[0]->status;

        $detail = DB::table("productprices")
        ->where("comp_code", $record[0]->comp_code)
        ->where("prod_code", $record[0]->prod_code)
        ->where("comp_group", $record[0]->comp_group)
        ->get(["factory_price","selling_price","uom_code"]);

        foreach ($detail as $key => $rs) 
        {
        	$data["uom"][] = $rs->uom_code;
        	$data["fprice"][] = $rs->factory_price;
        	$data["sprice"][] = $rs->selling_price;
        }

        return $data;
    }

    public static function non_food_record($id)
    {
        $record = DB::table("officesale_projects")
        ->where("project_id" , $id)
        ->where("comp_group" , Session::get("company"))
        ->get(["owner","code","incharge","name","selling_from","selling_to","allowable","status","limit"]);

       if(empty($record)) return Redirect::to('/');

        if($record[0]->allowable == 0)
        {
            $data["cash"] = 1;
            $data["charge"] = 1;
        }
        else if($record[0]->allowable == 1)
        {
            $data["cash"] = 1;
        }
        else
        {
            $data["charge"] = 1;
        }

        $data["owner"] = $record[0]->owner;
        $data["code"] = $record[0]->code;
        $data["incharge"] = $record[0]->incharge;
        $data["name"] = $record[0]->name;
        $data["selling_from"] = $record[0]->selling_from;
        $data["selling_to"] = $record[0]->selling_to;
        $data["status"] = $record[0]->status;
        $data["limit"] = $record[0]->limit;

        $detail = DB::table("officesale_project_items")
        ->join("systemconfigs","uom" , "=" , "syscon_code")    
        ->where("project_id" , $id)
        ->get(["code","desc","uom","price","status","syscon_desc"]);

        $stats[1] = "Active";
        $stats[0] = "Inactive";
        foreach ($detail as $key => $rs) 
        {
            $data["item-code"][] = $rs->code;
            $data["item-desc"][] = $rs->desc;
            $data["item-status"][] = $rs->status;
            $data["selling-price"][] = $rs->price;
            $data["uom-code"][] = $rs->uom;
            $data["uom-label"][] = $rs->syscon_desc;
            $data["item-stats"][] = $stats[$rs->status];
        }

        return $data;
    }

    public function ScopeBukCompany($query)
    {
        if(Session::get("is_office_receiver") == "BUK")    
            return $query->where("employees.company" , "BUK");
        else
            return $query->where("employees.company" ,"!=" ,  "BUK");
    }

    public function ScopeProjectCondition($query,$id)
    {
        if( $id )    
            return $query->where("project_id" , $id);
    }

    public static function so_code($group)
    {
            $table = DB::table("sotypes")
                ->where("status","Y")
                ->where("group", $group )
                ->orderby("so_name")
                ->get(["so_name" , "so_code"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->so_code] = $val->so_name;
            }
            
            return $arr;
    }

    public static function uom()
    {
            $table = DB::table("systemconfigs")
            	->where("active_flag","Y")
                ->orderby("syscon_desc")
                ->get(["syscon_desc" , "syscon_code"]);
            $arr = array();
            foreach($table as $key=>$val)
            {
                $arr[$val->syscon_code] = $val->syscon_desc;
            }
            
            return $arr;
    }

    public static function project()
    {
            $table = DB::table("officesale_projects")
                ->where("status",1)
                ->orderby("name")
                ->get(["project_id" , "name"]);
            $arr = array();

            $arr[ 0 ] = "All";
            foreach($table as $key=>$val)
            {
                $arr[$val->project_id] = $val->name;
            }
            
            return $arr;
    }


    public static function company()
    {
            $rec = DB::table("companies")
                ->where("status","Y")
                ->orderby("comp_code")
                ->get(["comp_code" , "comp_name"]);
            
            return $rec;
    }

    public static function food()
    {
            $comp = Session::get("company") == "BUK" ? "BUK" : "RBC-CORP";
            $uom = OfficeSales::uom();
            $rec = DB::table("products")
                ->where("status","Y")
                ->where("products.comp_group" , $comp )
                ->orderby("prod_name")
                ->join('productprices', function ($join) 
                    {
                        $join->on("products.comp_code","=","productprices.comp_code")
                        ->on("products.prod_code","=","productprices.prod_code")
                        ->on("products.comp_group","=","productprices.comp_group");
                    })
                ->get(["products.prod_code","products.comp_code","selling_price","uom_code","def_uom_code","prod_name"]);
            
            foreach($rec as $rs)
            {
                $product[$rs->prod_code]["name"] = $rs->prod_name;
                $product[$rs->prod_code]["company"] = $rs->comp_code;
                $product[$rs->prod_code]["uom_code"] = (isset($product[$rs->prod_code]["uom_code"]) ? ($product[$rs->prod_code]["uom_code"] . "/") : "") .  $rs->uom_code;
                $product[$rs->prod_code]["uom_name"] = (isset($product[$rs->prod_code]["uom_name"]) ? ($product[$rs->prod_code]["uom_name"] . "/") : "") .  $uom[$rs->uom_code];
                $product[$rs->prod_code]["def_code"] = $rs->def_uom_code;
                $product[$rs->prod_code]["selling_price"] = (isset($product[$rs->prod_code]["selling_price"]) ? ($product[$rs->prod_code]["selling_price"] . "/") : "") .  $rs->selling_price;                
            }

            return $product;
    }

    public static function non_food()
    {
            $date = date("Y-m-d");
            $comp = Session::get("company") == "BUK" ? "BUK" : "RBC-CORP";
            $uom = OfficeSales::uom();
            $rec = DB::table("officesale_projects")
                ->where("officesale_project_items.status",1)
                ->where("officesale_projects.status",1)
                ->where("officesale_projects.comp_group" , $comp )
                ->whereRaw("'$date' between selling_from and selling_to")
                ->orderby("desc")
                ->join('officesale_project_items', "officesale_project_items.project_id" , "=" , "officesale_projects.project_id")
                ->get(["officesale_project_items.code","uom","price","desc"]);
            
            $product = array();
            foreach($rec as $rs)
            {
                $product[$rs->code]["name"] = $rs->desc;
                $product[$rs->code]["uom_code"] = (isset($product[$rs->code]["uom_code"]) ? ($product[$rs->code]["uom_code"] . "/") : "") .  $rs->uom;
                $product[$rs->code]["uom_name"] = (isset($product[$rs->code]["uom_name"]) ? ($product[$rs->code]["uom_name"] . "/") : "") .  $uom[$rs->uom];
                $product[$rs->code]["selling_price"] = (isset($product[$rs->code]["selling_price"]) ? ($product[$rs->code]["selling_price"] . "/") : "") .  $rs->price;                
            }

            return $product;
    }


    public static function get_employee_reference_no()
    {
        $year = date("Y");
        $month = date("m");
        $ref = OfficeSales::whereRaw("SUBSTR(reference_no, 1, 4) = ?" , [$year])
            ->whereRaw("SUBSTR(reference_no, 6, 2) = ?" , [$month])
            ->orderby("reference_no","desc")
            ->limit(1)
            ->whereRaw("CHAR_LENGTH(reference_no) = ?" , [13])
            ->get(["reference_no"])->toArray();

       $no = isset($ref[0]["reference_no"]) ? $ref[0]["reference_no"] : "$year-$month-00000";   
       $num = sprintf("%'.05d", (int)substr($no,8) + 1);

       return "$year-$month-$num";
    }

    public static function get_affiliate_reference_no()
    {
        $year = date("y");
        $month = date("m");
        $ref = OfficeSales::whereRaw("CHAR_LENGTH(reference_no) != ?" , [13])
        //make order by as substr if not per company of affiliate
            ->where("company" , Session::get('company'))
            ->orderby("reference_no","desc")
            ->limit(1)
            ->get(["reference_no"])->toArray();

        if(isset($ref[0]["reference_no"]))
        {
            $reference = str_replace(Session::get('company'), "company", $ref[0]["reference_no"]);
            $arr = explode("-",$reference );
            if($arr[1] == $year && $arr[2] == $month)
            {
                $no = $arr[3];
            }
            else
            {
                $no = 0;
            }
        }   
        else
        {
            $no = 0;
        }         

       $num = sprintf("%'.05d", $no + 1);

       $comp = Session::get('company');
       return "$comp-$year-$month-$num";
    }

    public function ScopeCutoffPeriod($query)
    {
        if(date("d") <= 15 )    
            return $query->whereBetween("trans_date" , [date("Y-m-1"), date("Y-m-15")]);
        else
            return $query->whereBetween("trans_date" , [date("Y-m-16"), date("Y-m-t")]);
    }

     public static function company_list()
    {
            $rec = DB::table("companylists")
                ->where("active",1)
               // ->where("affiliate",1)
                ->orderby("comp_code")
                ->get(["comp_code","affiliate"]);
            
            return $rec;
    }

}