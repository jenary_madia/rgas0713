<?php

class Modules extends \Eloquent {

    protected $primaryKey = '';
    protected $table = 'modules';
    public $incrementing = false;
    public $timestamps = false;

    public function sample() {
        return "this!";
    }

}