<?php
use app\RGAS;

class TMSTimeOutLog extends \Eloquent 
{
    protected $table = 'TMS_TimeOutLogs';
    public $timestamps = false;
	  protected $connection = 'sqlsrv';

  //PASS ALL LOGS TO VARIABLE AND PASS IT TO CORRESPONDING FIELD. TO: CURRENT USER
	public static function getlog($start , $end , $eid=null)
	{
      //JMA 01182017 ADD LOG_MODE ON select query
		$requests = TMSTimeOutLog::WhereEmployee($eid)->whereBetween('Log_Date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end)) ])
            ->get(["Employee_ID","Log_Date","Log_Mode"]);
        $employee = array();
        foreach($requests as $req) 
        {				
           // if($req->Log_Mode == "I") 
           // 	{
           // 		if(isset($employee[ $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date))])  > strtotime($req->Log_Date) )
           // 			$employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 		elseif(!isset($employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ]))
           // 			$employee[ $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           		
           // 	}
           // 	elseif($req->Log_Mode == "O") 
           // 	{
           // 		if(isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime($req->Log_Date) )
           // 			$employee[ $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 		elseif(!isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]))
           // 			$employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 	}

            //set time-in automatically if not yet set.
             if(!isset( $employee[ $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] ))
             {
                $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
             }
             else
             {
                //check if time logs has interval of 2 minutes or more. If yes count as time-out
                if(strtotime( date("h:i a",strtotime($req->Log_Date))  ) - strtotime( $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] ) >= 120 )
                {
                    if(isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime(date("h:i a",strtotime($req->Log_Date)) ) )
                      $employee[ $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                    elseif(!isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]))
                      $employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                }
                else if( strtotime( $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] )  > strtotime( date("h:i a",strtotime($req->Log_Date))  ) )
                {
                  //transfer time-in to time-out record since previous record is late that current
    //              if(date("m/d/Y",strtotime($req->Log_Date)) == $previous_date && $previous_id == $req->Employee_ID )
      //  				  {
                    if(!isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]))
                        $employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
                    else if(isset($employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime( $previous ) )
        					     $employee[  $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
//        				  }
				  
				            //set time-in again if logs is earlier than first record
                    $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                }
             }
	
	
			$previous_date = date("m/d/Y",strtotime($req->Log_Date));
			$previous_id = $req->Employee_ID;
             $previous = $employee[  $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
        }

        return $employee;
	}	


  //PASS ALL LOGS TO VARIABLE AND PASS IT TO CORRESPONDING FIELD. TO: ALL SUBORDINATE EMPLOYEE OF CURRENT USER
	public static function getlogs($start , $end , $eid=null)
	{
		$employee_name = Cache::get("cache_employee_lastname");
		$requests = TMSTimeOutLog::WhereEmployee($eid)->whereBetween('Log_Date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end)) ])->get();
        $employee = array();
        foreach($requests as $req) 
        {				
           // if($req->Log_Mode == "I") 
           // 	{
           // 		if(isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date))])  > strtotime($req->Log_Date) )
           // 			$employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 		elseif(!isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ]))
           // 			$employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           		
           // 	}
           // 	elseif($req->Log_Mode == "O") 
           // 	{
           // 		if(isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime($req->Log_Date) )
           // 			$employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 		elseif(!isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]))
           // 			$employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
           // 	}

            //02232017


            if(Session::get("new_employeeid") != $req->Employee_ID && trim($req->Employee_ID))
            {
                  //set time-in automatically if not yet set.
                 if(!isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ]))
                 {
                    $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                 }
                 else
                 {
                    //check if time logs has interval of 2 minutes or more. If yes count as time-out
                    if(strtotime( date("h:i a",strtotime($req->Log_Date))  ) - strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date))]) >= 120 )
                    {
                        if(isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime( date("h:i a",strtotime($req->Log_Date)) ) )
                         $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                       elseif(!isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]))
                         $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                    }
                    else if( strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date))])  > strtotime( date("h:i a",strtotime($req->Log_Date))  ) )
                    {
                      //transfer time-in to time-out record since previous record is late that current
                  //    if(date("m/d/Y",strtotime($req->Log_Date)) == $previous_date && $previous_id == $req->Employee_ID )
          				//	  {
                         if(!isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) )
                             $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
                         else if(isset($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ]) && strtotime($employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date))] )  < strtotime( date("h:i a",strtotime(  $previous )) ) )
          					   	     $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timeout"][ date("d",strtotime($req->Log_Date)) ] = $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
              //        }
					  
					  //set time-in again if logs is earlier than first record
                      $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ] = date("h:i a",strtotime($req->Log_Date));
                    }
                 }

        				 $previous_date = date("m/d/Y",strtotime($req->Log_Date));
        				 $previous_id = $req->Employee_ID;
				 
                 $previous = $employee[ $employee_name[$req->Employee_ID] . "/" . $req->Employee_ID ]["timein"][ date("d",strtotime($req->Log_Date)) ];
                //add this to loop on employee w/out record
                if(isset($eid[$req->Employee_ID])) unset($eid[$req->Employee_ID]);
            }


             
        }


        //add this to loop on employee w/out record
        foreach ($eid as $id) 
        {
          if(Session::get("new_employeeid") != $id)              
            $employee[ $employee_name[ $id ] . "/" .$id ]["timein"][  ] = "";
        }


        if(!empty($employee)) ksort($employee);
        return $employee;
	}	


  //ADD CONDITIONAL WHERE
	public function ScopeWhereEmployee($query , $id)
	{
		if(is_array($id)) return $query->whereIn("Employee_ID" , $id);
		else return $query->where("Employee_ID", "=" , $id);
	}


	public static function month()
	{
		$data[1] = "January";
		$data[2] = "February";
		$data[3] = "March";
		$data[4] = "April";
		$data[5] = "May";
		$data[6] = "June";
		$data[7] = "July";
		$data[8] = "August";
		$data[9] = "September";
		$data[10] = "October";
		$data[11] = "November";
		$data[12] = "December";
	
		return $data;
	}


	public static function year()
	{
		$curr = date("Y");
		for($x=5;$x>0;$x--)
			$data[$curr - $x] = $curr - $x;

		for($x=0;$x<5;$x++)
			$data[$curr + $x] = $curr + $x;

		return $data;
	}
	
}