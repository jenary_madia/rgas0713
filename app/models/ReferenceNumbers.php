<?php

class ReferenceNumbers extends \Eloquent {
    
	protected $primaryKey = null;
    protected $table = 'referencenumbers';
    public $incrementing = false;
    public $timestamps = false;
	public $fillable = array('rn_module','rn_sequence');

	public static function increment_reference_number($module)
	{
		// sample code 
		// ReferenceNumbers::increment_reference_number('cbr');
		DB::update(
			"UPDATE referencenumbers 
			SET rn_sequence = rn_sequence + 1 
			WHERE rn_module = '$module'"
		);
	}
	
	public static function get_current_reference_number($module)
	{
		// sample code 
		// ReferenceNumbers::get_current_reference_number('cbr');
		$current_sequence_number = ReferenceNumbers::where("rn_module", "=", $module)->first();
		
		if($current_sequence_number)
			return date('Y').'-'.str_pad($current_sequence_number->rn_sequence, 5, "0", STR_PAD_LEFT); 
	}
	
	
   
    
    public static function get_current_reference_number_pmarf()
    {
        $year = date("Y");
        $ref = PromoAndMerchandising::whereRaw("SUBSTR(pmarf_ref_num, 1, 4) = ?" , [$year])
            ->orderby("pmarf_ref_num","desc")
            ->limit(1)
            ->get(["pmarf_ref_num"])->toArray();

       $no = isset($ref[0]["pmarf_ref_num"]) ? $ref[0]["pmarf_ref_num"] : "$year-00000";   
       $num = sprintf("%'.05d", (int)substr($no,5) + 1);

       return "$year-$num";
    }

     public static function get_current_reference_number_cbr()
    {
        $year = date("Y");
        $ref = CompensationBenefits::whereRaw("SUBSTR(cb_ref_num, 1, 4) = ?" , [$year])
            ->orderby("cb_ref_num","desc")
            ->limit(1)
            ->get(["cb_ref_num"])->toArray();

       $no = isset($ref[0]["cb_ref_num"]) ? $ref[0]["cb_ref_num"] : "$year-00000";   
       $num = sprintf("%'.05d", (int)substr($no,5) + 1);

       return "$year-$num";
    }
    
}