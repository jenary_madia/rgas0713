<?php


class NteDrafts extends \Eloquent {

    
    protected $table = "nte_drafts";

    protected $primaryKey = "nd_id";
    protected $hidden = array("password");

    public $timestamps = false;


}
?>