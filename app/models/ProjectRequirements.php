<?php

class ProjectRequirements extends \Eloquent {

    protected $table = 'projectrequirements';
    public $incrementing = false;
    public $timestamps = false;
    
    public function fetch_req($manpowerID) {
        $projectReq =  ProjectRequirements::where('manpowerserviceid',$manpowerID)
            ->get();
        $fetched = [];
        if ($projectReq) {
            for ($x = 0; $x <= count($projectReq) - 1; $x++) {
                $fetched[$x]['jobTitle'] = $projectReq[$x]['position_jobtitle'];
                $fetched[$x]['recruitment']['from'] = $projectReq[$x]['rec_dateneeded'];
                $fetched[$x]['recruitment']['to'] = $projectReq[$x]['rec_dateneeded_to'];
                $fetched[$x]['recruitment']['pax'] = $projectReq[$x]['rec_pax'];
                $fetched[$x]['preparation']['from'] = $projectReq[$x]['prep_dateneeded'];
                $fetched[$x]['preparation']['to'] = $projectReq[$x]['prep_dateneeded_to'];
                $fetched[$x]['preparation']['pax'] = $projectReq[$x]['prep_pax'];
                $fetched[$x]['briefing']['from'] = $projectReq[$x]['brif_dateneeded'];
                $fetched[$x]['briefing']['to'] = $projectReq[$x]['brif_dateneeded_to'];
                $fetched[$x]['briefing']['pax'] = $projectReq[$x]['brif_pax'];
                $fetched[$x]['fieldWork']['from'] = $projectReq[$x]['fw_dateneeded'];
                $fetched[$x]['fieldWork']['to'] = $projectReq[$x]['fw_dateneeded_to'];
                $fetched[$x]['fieldWork']['pax'] = $projectReq[$x]['fw_pax'];
                $fetched[$x]['fieldWork']['area'] = $projectReq[$x]['fw_area'];
                $fetched[$x]['editing']['from'] = $projectReq[$x]['edit_dateneeded'];
                $fetched[$x]['editing']['to'] = $projectReq[$x]['edit_dateneeded_to'];
                $fetched[$x]['editing']['pax'] = $projectReq[$x]['edit_pax'];
                $fetched[$x]['coding']['from'] = $projectReq[$x]['code_dateneeded'];
                $fetched[$x]['coding']['to'] = $projectReq[$x]['code_dateneeded_to'];
                $fetched[$x]['coding']['pax'] = $projectReq[$x]['code_pax'];
                $fetched[$x]['encoding']['from'] = $projectReq[$x]['enc_dateneeded'];
                $fetched[$x]['encoding']['to'] = $projectReq[$x]['enc_dateneeded_to'];
                $fetched[$x]['encoding']['pax'] = $projectReq[$x]['enc_pax'];
                $fetched[$x]['dataCleaning']['from'] = $projectReq[$x]['dc_dateneeded'];
                $fetched[$x]['dataCleaning']['to'] = $projectReq[$x]['dc_dateneeded_to'];
                $fetched[$x]['dataCleaning']['pax'] = $projectReq[$x]['dc_pax'];
            }
        }

        return $fetched;
    }
    
    public function projectDesc() {
        return $this->hasOne('SelectItems','item','position_jobtitle');
    }
    
 }

