<?php


class NteDraftsDepartments extends \Eloquent {

    
    protected $table = "nte_drafts_departments";

    protected $primaryKey = "ndd_id";
    protected $hidden = array("password");

    public $timestamps = false;


}
?>