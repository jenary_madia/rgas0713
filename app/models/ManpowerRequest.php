<?php


class ManpowerRequest extends \Eloquent {


    protected $table = "manpowerrequests";

    protected $primaryKey = "id";
    protected $guarded = array('id');
    public $incrementing = true;

    public $timestamps = false;

    public function generateRefNo()
    {
        $documentCodeToday = date('Y-m');
        $latestRefNo = ManpowerRequest::orderBy('reference_no','DESC')
            ->limit(1)
            ->first()['reference_no'];

        if ($latestRefNo) {
            $latestDocumentCode = explode('-',$latestRefNo)[0].'-'.explode('-',$latestRefNo)[1];
            $refno = explode('-',$latestRefNo)[2];
            if ($latestDocumentCode >= $documentCodeToday)
            {
                return $latestDocumentCode.'-'.str_pad($refno + 1, 5, '0', STR_PAD_LEFT);
            }
        }

        return $documentCodeToday.'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

    }

    public function current_emp(){
        return $this->hasOne('Employees','id','curr_emp');
    }

    public function owner(){
        return $this->hasOne('Employees','id','requestedby');
    }

    public function department(){
        return $this->hasOne('Departments','id','departmentid');
    }

    public function section(){
        return $this->hasOne('Sections','id','sectionid');
    }

    public function requestCode(){
        return $this->hasOne('MrfRequests','id','request_nature');
    }

    public function statusCode(){
        return $this->hasOne('MrfStatus','id','status');
    }

    public function attachments(){
        return $this->hasMany('MrfAttachments','manpowerrequestid','id');
    }
    
    public function signatories(){
        return $this->hasMany('MrfSignatories','manpowerrequestid','id');
    }

    public function comments(){
        return $this->hasMany('MrfComments','manpowerrequestid','id')->with("employee");
    }

    public function candidates() {
        return $this->hasMany('MRFRecruitmentDetails','manpowerrequestid','id');
    }
}

