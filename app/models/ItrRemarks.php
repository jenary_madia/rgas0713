<?php

class ItrRemarks extends \Eloquent {
    
    protected $table = 'itr_remarks';
    public $incrementing = false;
    public $timestamps = false;
    
   
    //    
    public static function get_remarks($itr_no)
    {
        
		$remark_lists = ItrRemarks::leftJoin("employees as emp", function($join){
		
			$join->on("emp.id", "=", "itr_remarks.employee_id");
		
		})->where("itr_remarks.itr_no", "=", "{$itr_no}")->get([
		
			 "itr_remarks.employee_id"
			,"itr_remarks.remarks"
            ,"itr_remarks.ts_remarks"
			,"emp.firstname"
			,"emp.lastname"
			
		]);
		
		return $remark_lists;
        
    }
   
   
   
    //
    public static function insert_remarks($employee_id, $itr_no, $remarks)
    {   
        $itr_remarks = new ItrRemarks;
        
        $itr_remarks->itr_no = $itr_no;
        $itr_remarks->employee_id = $employee_id;
        $itr_remarks->remarks = $remarks;
        
        $itr_remarks->save();
        
    }
    
    
}