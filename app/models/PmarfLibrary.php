<?php
class PmarfLibrary extends \Eloquent {
    
	// protected $table = "receivers";
	// public $incrementing = true;
	// public $timestamps = false;
	
    public static function defined_lib($sType){
        $aReturn = false;        
        switch($sType){
            case 'activity_type':
                $aReturn = array( 'In Store Display',
                                  'In Store Promo',
                                  'In Store Merchandising Materials',
                                  'Sponsorship via TV Network',
                                  'Events Sponsorship',
                                  'Festival Sponsorship',
                                  'Sari-sari Store Convention',
                                  'Street Sampling Activity',
                                  'Street Selling Activity',
                                  'School Sampling',
                                  'Product Sampling During Outlet Opening/Anniv.',
                                  'Sponsorship on Outlet Activities',
                                  'Integrated Marketing Communication', 
                                  'Others');

                break;
                
             case 'activity_date':
                $aReturn = array( 'One Day',
                                  'Multiple days-Straight',
                                  'Multiple days-Staggered');
                break;
            
             case 'implementer':
                $aReturn = array( 'Internal',
                                  'External');
                break;
            
            case 'initiated_by':
                $aReturn = array('Distributor',
                                 'Brand Management',
                                 'Ad and Media',
                                 'Trade',
                                 'Strategic Corporate Development',
                                 'Key Accounts Manager',
                                 'Others');
                break;
        }
        return $aReturn;
    }
 
}
/* End of file */