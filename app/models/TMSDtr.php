<?php
use app\RGAS;

class TMSDtr extends \Eloquent 
{
    protected $table = 'TMS_Dtr';
    public $timestamps = false;
  	protected $connection = 'sqlsrv';


  //GET LOGS IN SUMMARY TABLE
	public static function getlogs($start , $end ,$eid=null)
	{
		    $requests = TMSDtr::WhereEmployee($eid)->whereBetween('Sched_Date', [date("Y-m-d",strtotime($start)),  date("Y-m-d 23:59:59",strtotime($end)) ])
        ->selectRaw("COALESCE((SELECT TOP 1 Holiday.Hol_WithPay 
              FROM Tms_Holiday as Holiday 
              WHERE Holiday.Comp_Id = 'RBC' and 
                CONVERT(DATETIME,CONVERT(VARCHAR,Holiday.Hol_Date,101),101) = CONVERT(DATETIME,CONVERT(VARCHAR,TMS_Dtr.Sched_Date,101),101) and 
                Holiday.Hol_BranchID = (SELECT TOP 1 Branch_ID FROM RBC_HRMS.DBO.empjob_Info as a WHERE a.Employee_ID = TMS_Dtr.Employee_ID and a.Current_Job ='Y') and 
                Holiday.TMS_Group = (SELECT TOP 1 TMS_Group FROM TMS_Employee WHERE TMS_Employee.Employee_ID = TMS_Dtr.Employee_Id) 
               ORDER BY Holiday.Hol_WithPay ASC),'') as Hol_WithPay ,
         WorkDayCode,ShiftCd_1,Actual_Days,Leave_Type,Employee_Id,Minutes_Excused,
          WReg_Hrs,Sched_Date,Minutes_Late,Offset_Filed,Undertime_Filed,OB_Filed_Date,
          Leave_Excused,HD_Excused
         ")
		->get(["WorkDayCode","ShiftCd_1","Actual_Days","Leave_Type","Employee_Id","Minutes_Excused",
          "WReg_Hrs","Sched_Date","Minutes_Late","Offset_Filed","Undertime_Filed","OB_Filed_Date",
          "Leave_Excused","HD_Excused","Hol_WithPay"]);
        $employee = array();
        foreach($requests as $req) 
        {				
            //declare leave variables
            $decAbsUnEx = 0;
            $decAbsEx = 0;


            $employee[$req->Employee_Id]["actualdays"][] = $req->Actual_Days;
          //  if(trim($req->Leave_Type) && $req->Leave_Type == "ABS")
          //  {
             // $employee[$req->Employee_Id]["unexcusedleave"][] = $req->Actual_Days;//change 1 to actual days
            
                  if($req->Leave_Excused == "Y")
                  {
                       $strOfset = trim($req->Offset_Filed);
                       $strOB =  str_replace("-", "", trim($req->OB_Filed_Date) );

                      if($strOfset != "" || $strOB != "" )
                      {
                          // Count only the leave filed excluding "OB" and "OS"
                          if($strOB != "" &&  $req->Actual_Days == "0.5" )
                          {
                              if($req->Leave_Type != "")
                              {
                                $decAbsEx += 0.5;
                              }
                              else
                              {
                                $decAbsUnEx += 0.5;
                              }
                          }

                          if($strOfset == "OS" &&  $req->Actual_Days == "0.5" )
                          {
                              if($req->Leave_Type != "")
                              {
                                $decAbsEx += 0.5;
                              }
                              else
                              {
                                $decAbsUnEx += 0.5;
                              }
                          }
                      }
                      else
                      {
                          //by roland 03/11/2014 dont count if days falls on unworked by corp-hr
                          if(trim($req->WorkDayCode) == "UWRK_HR"){}
                          else
                          {                     
                              // Add by roland 03/12/2014 less halfday those only tag as "Y" excused "N" un excused else "" w/o HD
                              if(strtoupper(trim($req->HD_Excused)) == "Y")
                              {
                                  $decAbsEx += 0.5;
                              }
                              else
                              {
                                  //Dont count if restday and holiday
                                  if(strtoupper(trim($req->Hol_WithPay)) == "Y"){}
                                  else
                                  {
                                      if(trim($req->ShiftCd_1) != "RD")
                                      {
                                          switch ( trim($req->Actual_Days) ) 
                                          {
                                            case 1:
                                                 if(trim($req->Leave_Excused) == "Y")
                                                 {
                                                  $decAbsEx += 1;
                                                 }
                                            break;
                                            
                                            case 0:
                                                  $decAbsEx += 1;
                                            break;

                                            case 0.5:
                                              $decAbsEx += 0.5;
                                            break;

               
                                          }
                                      }
                                  }
                              }

                          }
                      }
                  }
                  else if($req->Leave_Excused == "N" || strtoupper(trim($req->HD_Excused)) == "N" )
                  {
                    //Addy by roland 04/21/2014 dont count leave excused or un excused if worked during holiday
                    if( strtoupper(trim($req->Hol_WithPay)) == "Y"){}
                    else
                    {
                        //Add by roland 03/12/2014 less halfday those only tag as "Y" excused "N" un excused else "" w/o HD
                        if(strtoupper(trim($req->HD_Excused)) == "N")
                        {
                            $decAbsUnEx += 0.5;
                        }
                        else
                        {
                            $decAbsUnEx += 1;
                        }
                        
                    }

                  }

           // }

            $employee[$req->Employee_Id]["unexcusedleave"][] = $decAbsUnEx;
            $employee[$req->Employee_Id]["excusedleave"][] =  $decAbsEx ;


           // if(trim($req->Leave_Type) && $req->Leave_Type != "ABS")$employee[$req->Employee_Id]["excusedleave"][] = $req->Actual_Days;

          	if($req->WReg_Hrs > 0) $employee[$req->Employee_Id]["workhours"][ date("d",strtotime($req->Sched_Date)) ] = round($req->WReg_Hrs,2);
            $employee[$req->Employee_Id]["remarks"][ date("d",strtotime($req->Sched_Date)) ] = trim($req->Leave_Type == "ABS" ? "A" : $req->Leave_Type) . (trim($req->ShiftCd_1)=="RD" ? " RD ": "") . trim($req->Offset_Filed ).  trim($req->Undertime_Filed) . (strlen($req->OB_Filed_Date ) > 1 ? "OB" : "") . (in_array(trim($req->WorkDayCode),["LGH_HR","LGRST_HR","SH_HR","SRST_HR","UWRK_HR"]) ? trim($req->WorkDayCode): "")  ;
            if($req->Minutes_Late > 0 && $req->Minutes_Excused == "N") $employee[$req->Employee_Id]["unexcusedlate"][ date("d",strtotime($req->Sched_Date)) ] = $req->Minutes_Late;
            if($req->Minutes_Late > 0 && $req->Minutes_Excused == "Y") $employee[$req->Employee_Id]["excusedlate"][ date("d",strtotime($req->Sched_Date)) ] = $req->Minutes_Late;
   
        }

        return $employee;
	}	

  //ADD CONDITIONAL WHERE WHEN OAM IS FOR CURRENT USER
  public function ScopeWhereEmployee($query , $id)
  {
    if(is_array($id)) return $query->whereIn("Employee_Id" , $id);
    else return $query->where("Employee_Id", "=" , $id);
  }


}