<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:39 PM
 */
use RGAS\Modules\MOC\MOC;
class MOCRequests extends \Eloquent {
    protected $table = 'moc_requests';
    public $incrementing = true;
    public $guarded =  array('id');
    public $timestamps = false;

    public function generateRefNo()
    {
        $documentCodeToday = date('Y-m');
        $latestRefNo = MOCRequests::orderBy('transaction_code','DESC')
            ->limit(1)
            ->first()['transaction_code'];

        if ($latestRefNo) {
            $latestDocumentCode = explode('-',$latestRefNo)[0].'-'.explode('-',$latestRefNo)[1];
            $refno = explode('-',$latestRefNo)[2];
            if ($latestDocumentCode >= $documentCodeToday)
            {
                return $latestDocumentCode.'-'.str_pad($refno + 1, 5, '0', STR_PAD_LEFT);
            }
        }

        return $documentCodeToday.'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

    }

    public static function search() {
        $company = Input::get('company');
        $department = Input::get('department');
        $status = Input::get('status');
        $action = Input::get('action');
        $SSMDs = DB::table('employees')
            ->join('sections', 'employees.departmentid', '=', 'sections.departmentid')
            ->where("sections.sect_name","SATELLITE SYSTEMS")
            ->select('employees.id as empID')
            ->get();
        $SSMDids = [];
        foreach($SSMDs as $SSMD) {
            array_push($SSMDids,$SSMD->empID);
        }
        $result = DB::table('moc_requests')
            ->leftjoin("moc_signatories",'moc_requests.request_id','=','moc_signatories.moc_request_id')
            ->leftjoin('employees','moc_requests.curr_emp','=','employees.id')
            ->selectRaw('DISTINCT(moc_requests.request_id),moc_requests.*,moc_signatories.*,employees.id as curr_emp_id,employees.firstname as curr_emp_firstname,employees.middlename as curr_emp_middlename,employees.lastname as curr_emp_lastname');
        if (Session::get("company") != 'RBC-CORP') {
            if($company) {
                $result->where('moc_requests.company',$company);
                if($department) {
                    $result->where('moc_requests.dept_sec','like',"%$department%");
                }
            }else{
                $result->where('moc_requests.company', Session::get("company"));
            }
            $result->whereIn('moc_requests.status', ["FOR ASSESSMENT", "FOR ACKNOWLEDGEMENT","ACKNOWLEDGED","FOR APPROVAL"]);
        }else{
            if($company) {
                $result->where('moc_requests.company',$company);
                if($department) {
                    $result->where('moc_requests.dept_sec','like',"%$department%");
                }
            }
            
            $result->where(function ($query) use ($SSMDids){
                return  $query->whereNotIn('moc_requests.curr_emp',$SSMDids)
                    ->whereIn('moc_requests.status', ["FOR ASSESSMENT"])
                    ->orwhere(['moc_requests.status' => 'FOR APPROVAL','moc_signatories.signature_type' => 6])
                    ->orwhere(['moc_requests.status' => "FOR ACKNOWLEDGEMENT",'moc_signatories.signature_type' => 7])
                    ->orwhere(['moc_requests.status' => "ACKNOWLEDGED",'moc_signatories.signature_type' => 8]);
            });

            // $result->whereRaw("(moc_requests.status in ('FOR ASSESSMENT','ACKNOWLEDGED') or moc_requests.status in ('FOR APPROVAL','FOR ACKNOWLEDGEMENT') AND moc_signatories.signature_type >= 6 or moc_requests.status = '' AND moc_signatories.signature_type = 7)");
            // $result->orwhere(['moc_requests.status' => 'FOR APPROVAL','moc_signatories.signature_type' => 6]);
        }


        if($action == 'generate') {
            if($status && $status != "ALL") {
                $result->where('moc_requests.status',$status);
            }

            if(Input::has('from')) {
                if(Input::has('to')) {
                    $result->whereBetween('moc_requests.date',[Input::get('from'),Input::get('to')]);
                }else{
                    $result->where('moc_requests.date',Input::get('from'));
                }
            }
        }
        return $result->get();
    }
    /*************************JOIN WITH OTHER TABLE*****************************/

    public function owner(){
        return $this->hasOne('Employees','id','requested_by')
            ->with('department');
    }

    public function current_emp(){
        return $this->hasOne('Employees','id','curr_emp');
    }

    public function attachments(){
        return $this->hasMany('MOCAttachments','request_id','request_id');
    }

    public function ccodes(){
        return $this->hasMany('MOCChangeInCodes','request_id','request_id');
    }

    public function comments(){
        $comments = $this->hasMany('MOCComments','request_id','request_id')
            ->select('id','request_id','employee_id','comment','ts_comment')
            ->with("employee");
        return $comments;
    }

    public function rcodes(){
        return $this->hasMany('MOCRequestCodes','request_id','request_id');
    }

    public function signatories(){
        return $this->hasMany('MOCSignatories','moc_request_id','request_id')->with("employee");
    }

//    public function departments(){
//        return $this->hasMany('MOCSignatories','moc_request_id','request_id');
//    }

    public function comp_name(){
        return $this->hasOne('Companies','comp_code','company')->select(['comp_code','comp_name']);
    }
}