<?php

/**
 * Created by PhpStorm.
 * User: octal
 * Date: 21/09/2016
 * Time: 9:47 AM
 */
class ManpowerAgency extends \Eloquent
{
    protected $table = 'manpowerservicesagencies';
    protected $guarded = array('id');
    public $incrementing = true;
    public $timestamps = false;
}