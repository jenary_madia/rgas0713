<?php

class ItrPabxServices extends \Eloquent {
    
    protected $table = 'itr_pabx_services';
    
    public $incrementing = false;
    public $timestamps = false;
    
    
    //
    public static function get_pabx_services($itr_no)
    {
        $pabx_services = ItrPabxServices::where("itr_no", "=", "{$itr_no}")->get(["pabx_opt","add_feat"]);
        return $pabx_services;    
    }
    
    
    //
    public static function clear_pabx_services($itr_no)
    {
        ItrPabxServices::where("itr_no", "=", "{$itr_no}")->delete();   
    }
    
}
//End of file