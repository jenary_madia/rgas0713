<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/26/2016
 * Time: 3:47 PM
 */
class MOCSignatories extends \Eloquent {
    protected $table = 'moc_signatories';
    public $incrementing = true;
    public $guarded =  array('attachment_id');
    public $timestamps = false;

    public function addSignatories($mocID,$type,$id = null) {
        $maxSeq = $this->where('moc_request_id',$mocID)->max('signature_type_seq');
        if (! $maxSeq )
        {
            $maxSeq =  1;
        }else{
            $maxSeq += 1;
        }
        $this->moc_request_id = $mocID;
        $this->signature_type = $type;
        $this->signature_type_seq = $maxSeq;
        $this->employee_id = (is_null($id) ? Session::get("employee_id") : $id);
        $this->approval_date = date('Y-m-d');
        $this->save();
    }

//    old
//    public function rollbackSignatories($mocID,$id) {
//        //If signatory has duplicate signatory type
//        $maxID = $this->where('moc_request_id',$mocID)
//            ->where('signature_type', '=' ,$id)->max('id');
//
//        $signatories = $this->where('moc_request_id',$mocID)
//            ->where('id', '>=' ,$maxID);
//
//        $signatories->delete();
//    }

    public function rollbackSignatories($mocID,$id,$multiple=null) {
        if($multiple) {
            $signatories = $this->where('moc_request_id',$mocID)
                ->where('signature_type', '>=' ,$id); 
        }else{
            $maxID = $this->where('moc_request_id',$mocID)
            ->where('signature_type', '=' ,$id)->max('id');
    
            $signatories = $this->where('moc_request_id',$mocID)
                ->where('id', '>=' ,$maxID);
        }
        $signatories->delete();
    }

    public function employee() {
        return $this->hasOne('Employees','id','employee_id')->select(['id','firstname','lastname','middlename']);
    }

}