<?php


class TafAttachments extends \Eloquent {

    
	 protected $table = 'taf_attachments';
	 protected $primaryKey = 'attachment_id';
	 
	 public $incrementing = true;
	 public $timestamps = false;
	
	
	//
	public static function get_attachments($taf_no)
	{
	
		$attachment_list = TafAttachments::leftJoin("taf_requests as taf", function($join){
		
			$join->on("taf.taf_no", "=", "taf_attachments.taf_no");
			
		})->where("taf_attachments.taf_no", "=", "{$taf_no}")->get([
		
			 "taf_attachments.attachment_id"
			,"taf.id as taf_id"
			,"taf_attachments.taf_no"
			,"taf_attachments.fn"
			,"taf_attachments.employee_id"
		
		]);
	
		return $attachment_list;
	}
	
    
    //
    public static function remove_attachment($attachment_id)
    {
        $delete_attachment = TafAttachments::where("attachment_id", "=", "{$attachment_id}")->delete();   
        return $delete_attachment;
    }
    
}
/* End of file */