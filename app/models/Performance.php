<?php
use app\RGAS;
use RGAS\Libraries;
use RGAS\Modules\PMF;

class Performance extends \Eloquent 
{    
    protected $table = 'pmf_employees';
    protected $guarded = array('id');
    public $timestamps = false;

    public static function employee_record($id)
    {
        $record = Employees::where("employees.id" , $id)
                ->join("departments as dept","employees.departmentid", "=", "dept.id")
                ->leftJoin("sections as sect","employees.sectionid", "=", "sect.id")
                ->get(["firstname" , "middlename","lastname"  , "employeeid" , "designation" , "company" , "dept_name",
                    "sect_name" , "dept.id as dept_id" ,"departmentid2","email"]);

       if($record->isEmpty()) return Redirect::to('/');

        $data["employee_name"] = $record[0]->firstname . " " . ((!empty($record[0]->middlename)) ? $record[0]->middlename . ". " : "") . $record[0]->lastname;
        $data["employeeid"] = $record[0]->employeeid;
        $data["designation"] = $record[0]->designation;
        $data["company"] = $record[0]->company;
        $data["dept_name"] = $record[0]->dept_name;
        $data["sect_name"] = $record[0]->sect_name;
        $data["dept_id"] = $record[0]->dept_id;
        $data["departmentid2"] = $record[0]->departmentid2;

        $data["email"] = $record[0]->email;
        $data["employee_id"] = $id;
        return $data;
    }


    public static function filer_record($id)
    {
        $emp = Session::get('employee_id');
        $record = Performance::join("employees","pmf_employees.employee_id" , "=" , "employees.id")
                ->join("departments as dept","employees.departmentid", "=", "dept.id")
                ->leftJoin("sections as sect","employees.sectionid", "=", "sect.id")
                ->join("pmf","pmf.pmf_id" , "=" , "pmf_employees.pmf_id")
                ->where("pmf_employee_id" , $id )
                ->whereRaw("(created_by = $emp || pmf_employees.employee_id = $emp)")
                ->get(["firstname","middlename","lastname","employeeid","designation","company","dept_name","sect_name",
                    "dept.id as dept_id","departmentid2","reference_no","year","period","pmf.pmf_id","attachments","status_id","ts_created",
                    "email","employees.id","pmf_employee_id","employees.id"]);


        if($record->isEmpty()) return Redirect::to('/');

        $status[1] = "New";
        $status[2] = "Completed";
        $status[3] = "For Approval";
        $status[4] = "Submitted";
        $status[5] = "Ongoing Review";
        $status[6] = "Approved";
        $status[7] = "Returned";
        $status[8] = "For Acknowledgement";
        $status[9] = "Acknowledged";
        $status[10] = "For HROD's Review";
        $status[11] = "Calibrated";

        $data["employee_name"] = $record[0]->firstname . " " . ((!empty($record[0]->middlename)) ? $record[0]->middlename . ". " : "") . $record[0]->lastname;
        $data["employeeid"] = $record[0]->employeeid;
        $data["designation"] = $record[0]->designation;
        $data["company"] = $record[0]->company;
        $data["dept_name"] = $record[0]->dept_name;
        $data["sect_name"] = $record[0]->sect_name;
        $data["dept_id"] = $record[0]->dept_id;
        $data["departmentid2"] = $record[0]->departmentid2;
        $data["reference_no"] =  $record[0]->reference_no;
        $data["cover"] = $record[0]->year . " (" . $record[0]->period . ")";
        $data["pmf"] =  $record[0]->pmf_id;
        $data["attachments"] = $record[0]->attachments;
        $data["status"] = strtoupper($status[$record[0]->status_id]);
        $data["date_filed"] = date("Y-m-d",strtotime($record[0]->ts_created));
        $data["email"] = $record[0]->email;
        $data["employee_id"] = $record[0]->id;

        $kras = DB::table("pmf_employee_kras")
                ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                ->get( );

        $data["kras"] = $kras;

        $kpi = DB::table("pmf_employee_kpis")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get( );

        $data["kpi"] = $kpi;

        $data["core"] = array();
        $core = DB::table("pmf_employee_core_values")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["core_id","critical_incident","rating"]);

                foreach($core as $req) 
                {
                    $data["core"][$req->core_id]["rating"] = $req->rating;
                    $data["core"][$req->core_id]["incident"] = $req->critical_incident;
                }

        $data["total"] = array();
        $core = DB::table("pmf_employee_totals")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["method","total","score"]);

                foreach($core as $req) 
                {
                    $data["total"][$req->method]["score"] = $req->score;
                    $data["total"][$req->method]["total"] = $req->total;
                }

        $data["reccomendation"] = array();
        $core = DB::table("pmf_employee_recommendations")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["recommendation_id","recommendation","approved"]);

                foreach($core as $req) 
                {
                    $data["reccomendation"][$req->recommendation_id]["recommendation"] = $req->recommendation;
                    $data["reccomendation"][$req->recommendation_id]["approved"] = $req->approved;
                }


        $data["comment"] = array();
        $comment = DB::table("pmf_employee_comments")
                    ->join("employees","employees.id","=","pmf_employee_comments.employee_id","left")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["firstname","lastname","pmf_employee_comments.designation","comment","pmf_employee_comments.employee_id"]);

                foreach($comment as $req) 
                {
                    $data["comment"][$req->designation]["comment"] = $req->comment;
                    $data["comment"][$req->designation]["employee_id"] = $req->employee_id;
                    $data["comment"][$req->designation]["name"] = $req->firstname . " " . $req->lastname;
                }


       return $data;
    }



    public static function approval_record($id)
    {

        $record = Performance::join("employees as e","pmf_employees.employee_id" , "=" , "e.id")
                ->join("employees as c","pmf_employees.created_by" , "=" , "c.id")
                ->join("departments as dept","e.departmentid", "=", "dept.id")
                ->leftJoin("sections as sect","e.sectionid", "=", "sect.id")
                ->join("pmf","pmf.pmf_id" , "=" , "pmf_employees.pmf_id")
                ->where("pmf_employee_id" , $id )
                ->whereIn("status_id" , [3,7])
                ->where("current_id" , Session::get('employee_id'))
                ->get(["c.firstname as cfirstname","c.middlename as cmiddlename","c.lastname as clastname","e.firstname","e.firstname","e.middlename","e.lastname","e.superiorid","e.employeeid","e.designation"
                    ,"e.email","e.company","dept_name","sect_name","dept.id as dept_id","e.departmentid2","reference_no","period",
                    "pmf.pmf_id","attachments","ts_created","status_id","pmf_employee_id","year","employee_id"]);


        if($record->isEmpty()) return Redirect::to('/');

        $status[1] = "New";
        $status[2] = "Completed";
        $status[3] = "For Approval";
        $status[4] = "Submitted";
        $status[5] = "Ongoing Review";
        $status[6] = "Approved";
        $status[7] = "Returned";
        $status[8] = "For Acknowledgement";
        $status[9] = "Acknowledged";
        $status[10] = "For HROD's Review";
        $status[11] = "Calibrated";

        $data["employee_name"] = $record[0]->firstname . " " . ((!empty($record[0]->middlename)) ? $record[0]->middlename . ". " : "") . $record[0]->lastname;
        $data["created_name"] = $record[0]->cfirstname . " " . ((!empty($record[0]->cmiddlename)) ? $record[0]->cmiddlename . ". " : "") . $record[0]->clastname;
        

        $data["employeeid"] = $record[0]->employeeid;
        $data["superiorid"] = $record[0]->superiorid;
        $data["employee_id"] = $record[0]->employee_id;
        $data["email"] = $record[0]->email;
        $data["designation"] = $record[0]->designation;
        $data["company"] = $record[0]->company;
        $data["dept_name"] = $record[0]->dept_name;
        $data["sect_name"] = $record[0]->sect_name;
        $data["dept_id"] = $record[0]->dept_id;
        $data["departmentid2"] = $record[0]->departmentid2;
        $data["reference_no"] =  $record[0]->reference_no;
        $data["cover"] = $record[0]->year . " (" . $record[0]->period . ")";
        $data["pmf"] =  $record[0]->pmf_id;
        $data["attachments"] = $record[0]->attachments;
        $data["date_filed"] = date("Y-m-d",strtotime($record[0]->ts_created));
        $data["status"] = strtoupper($status[$record[0]->status_id]);

        $kras = DB::table("pmf_employee_kras")
                ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                ->get( );

        $data["kras"] = $kras;

        $kpi = DB::table("pmf_employee_kpis")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get( );

        $data["kpi"] = $kpi;

        $data["core"] = array();
        $core = DB::table("pmf_employee_core_values")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["core_id","critical_incident","rating"]);

                foreach($core as $req) 
                {
                    $data["core"][$req->core_id]["rating"] = $req->rating;
                    $data["core"][$req->core_id]["incident"] = $req->critical_incident;
                }

        $data["total"] = array();
        $core = DB::table("pmf_employee_totals")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["method","total","score"]);

                foreach($core as $req) 
                {
                    $data["total"][$req->method]["score"] = $req->score;
                    $data["total"][$req->method]["total"] = $req->total;
                }

        $data["reccomendation"] = array();
        $core = DB::table("pmf_employee_recommendations")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["recommendation_id","recommendation","approved"]);

                foreach($core as $req) 
                {
                    $data["reccomendation"][$req->recommendation_id]["recommendation"] = $req->recommendation;
                    $data["reccomendation"][$req->recommendation_id]["approved"] = $req->approved;
                }

        $data["comment"] = array();
        $comment = DB::table("pmf_employee_comments")
                    ->join("employees","employees.id","=","pmf_employee_comments.employee_id","left")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["firstname","lastname","pmf_employee_comments.designation","comment","pmf_employee_comments.employee_id"]);

                foreach($comment as $req) 
                {
                    $data["comment"][$req->designation]["comment"] = $req->comment;
                    $data["comment"][$req->designation]["employee_id"] = $req->employee_id;
                    $data["comment"][$req->designation]["name"] = $req->firstname . " " . $req->lastname;
                }

       return $data;
    }

    public static function submmited_record($id)
    {

        $record = Performance::join("employees as e","pmf_employees.employee_id" , "=" , "e.id")
                ->join("employees as c","pmf_employees.created_by" , "=" , "c.id")
                ->join("departments as dept","e.departmentid", "=", "dept.id")
                ->leftJoin("sections as sect","e.sectionid", "=", "sect.id")
                ->join("pmf","pmf.pmf_id" , "=" , "pmf_employees.pmf_id")
                ->where("pmf_employee_id" , $id )
                ->get(["c.firstname as cfirstname","c.middlename as cmiddlename","c.lastname as clastname","e.firstname","e.middlename","e.lastname","e.superiorid","e.employeeid","e.designation"
                    ,"e.email","e.company","dept_name","sect_name","dept.id as dept_id","e.departmentid2","reference_no","period",
                    "pmf.pmf_id","attachments","ts_created","status_id","pmf_employee_id","year"]);

        if($record->isEmpty()) return Redirect::to('/');

        $status[1] = "New";
        $status[2] = "Completed";
        $status[3] = "For Approval";
        $status[4] = "Submitted";
        $status[5] = "Ongoing Review";
        $status[6] = "Approved";
        $status[7] = "Returned";
        $status[8] = "For Acknowledgement";
        $status[9] = "Acknowledged";
        $status[10] = "For HROD's Review";
        $status[11] = "Calibrated";

        $data["employee_name"] = $record[0]->firstname . " " . ((!empty($record[0]->middlename)) ? $record[0]->middlename . ". " : "") . $record[0]->lastname;
        $data["created_name"] = $record[0]->cfirstname . " " . ((!empty($record[0]->cmiddlename)) ? $record[0]->cmiddlename . ". " : "") . $record[0]->clastname;
        
        $data["employeeid"] = $record[0]->employeeid;
        $data["superiorid"] = $record[0]->superiorid;
        $data["employee_id"] = $record[0]->employee_id;
        $data["email"] = $record[0]->email;
        $data["designation"] = $record[0]->designation;
        $data["company"] = $record[0]->company;
        $data["dept_name"] = $record[0]->dept_name;
        $data["sect_name"] = $record[0]->sect_name;
        $data["dept_id"] = $record[0]->dept_id;
        $data["departmentid2"] = $record[0]->departmentid2;
        $data["reference_no"] =  $record[0]->reference_no;
        $data["cover"] = $record[0]->year . " (" . $record[0]->period . ")";
        $data["pmf"] =  $record[0]->pmf_id;
        $data["attachments"] = $record[0]->attachments;
        $data["date_filed"] = date("Y-m-d",strtotime($record[0]->ts_created));
        $data["status"] = $status[$record[0]->status_id];

        $kras = DB::table("pmf_employee_kras")
                ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                ->get( );

        $data["kras"] = $kras;

        $kpi = DB::table("pmf_employee_kpis")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get( );

        $data["kpi"] = $kpi;

        $data["core"] = array();
        $core = DB::table("pmf_employee_core_values")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["core_id","critical_incident","rating"]);

                foreach($core as $req) 
                {
                    $data["core"][$req->core_id]["rating"] = $req->rating;
                    $data["core"][$req->core_id]["incident"] = $req->critical_incident;
                }

        $data["total"] = array();
        $core = DB::table("pmf_employee_totals")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["method","total","score"]);

                foreach($core as $req) 
                {
                    $data["total"][$req->method]["score"] = $req->score;
                    $data["total"][$req->method]["total"] = $req->total;
                }

        $data["reccomendation"] = array();
        $core = DB::table("pmf_employee_recommendations")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["recommendation_id","recommendation","approved"]);

                foreach($core as $req) 
                {
                    $data["reccomendation"][$req->recommendation_id]["recommendation"] = $req->recommendation;
                    $data["reccomendation"][$req->recommendation_id]["approved"] = $req->approved;
                }

        $data["comment"] = array();
        $comment = DB::table("pmf_employee_comments")
                    ->join("employees","employees.id","=","pmf_employee_comments.employee_id","left")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get(["firstname","lastname","pmf_employee_comments.designation","comment","pmf_employee_comments.employee_id"]);

                foreach($comment as $req) 
                {
                    $data["comment"][$req->designation]["comment"] = $req->comment;
                    $data["comment"][$req->designation]["employee_id"] = $req->employee_id;
                    $data["comment"][$req->designation]["name"] = $req->firstname . " " . $req->lastname;
                }

       return $data;
    }



    public static function copy_record(  $purpose , $content , $copy ,$sub=null)
    {
        if(!$content ) return array();

        $purpose = DB::table("pmf_purposes")
                ->where("purpose_id", $purpose)
                ->get(["purpose"]);

        if($sub):
            $record = DB::table("pmf_employees")->join("pmf_purposes","pmf_purposes.pmf_id","=","pmf_employees.pmf_id")
                ->where("pmf_purposes.purpose",$purpose[0]->purpose)
                ->join("employees","pmf_employees.employee_id" , "=" , "employees.id")
                ->where("id" , $sub )
                ->whereIn("status_id" , [4,11])
                ->orderby("pmf_employees.pmf_id","desc")
                ->orderby("pmf_employee_id","desc")
                ->limit(1)
                ->get(["pmf_employee_id" , "reference_no"]);
        else:
        $record = DB::table("pmf_employees")->where("employee_id" , Session::get('employee_id'))
                ->join("pmf_purposes","pmf_purposes.pmf_id","=","pmf_employees.pmf_id")
                ->where("pmf_purposes.purpose",$purpose[0]->purpose)
                ->whereIn("status_id" , [4,11])
                ->orderby("pmf_employees.pmf_id","desc")
                ->orderby("pmf_employee_id","desc")
                ->limit(1)
                ->get(["pmf_employee_id" , "reference_no"]);
        endif;

        if(!isset( $record[0]->reference_no )) return array();
        $data["reference_no"] =  $record[0]->reference_no;

        $kras = DB::table("pmf_employee_kras")
                ->join("pmf_kra_clusters","pmf_kra_clusters.cluster_id","=","pmf_employee_kras.cluster_id")
                ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                ->get(["cluster","pmf_employee_kras.cluster_id","kra_id","kra"]);

        $data["kras"] = $kras;

    
       if($copy == 2):
           $kpi = DB::table("pmf_employee_kpis")
                    ->where("pmf_employee_id" , $record[0]->pmf_employee_id )
                    ->get( );

       $data["kpi"] = $kpi;
       endif;

       return $data;
    }

    public static function delete_attachment($id)
    {
        $attachment = Input::get("attachment_name");
        if(!empty($attachment))
        {

             $attach_audit = new ART\ART; 
            foreach($attachment as $index=>$file)
            {
                $attach_audit->setTable('pmf_employees');
                $attach_audit->setPrimaryKey('pmf_employee_id');
                $attach_audit->AU005($id , $file , Input::get("reference") );
            }
        }
    }

    public static function insert_detail($id , $reference , $status = 1)
    {
        $purpose["pmf_employee_id"] = $id;
        $purpose["purpose_id"] = Input::get('purpose');
        DB::table("pmf_employee_purposes")->insert( $purpose ); 


        $objectives["pmf_employee_id"] = $id;
        $objectives["method"] = 'objectives';
        $objectives["total"] = Input::get('total-weight');
        $objectives["score"] = Input::get('total-weighted');

        $core_values["pmf_employee_id"] = $id;
        $core_values["method"] = 'core_values';
        $core_values["total"] = Input::get('behavior-rating');
        $core_values["score"] = Input::get('behavior-score');

        $emp_totals = array(
                $objectives,
                $core_values,
                array(
                        "pmf_employee_id" =>  $id,
                        "method" => 'discipline',
                        "total" => Input::get('discipline-demerit'),
                        "score" => Input::get('discipline-score')
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "method" => 'policies_and_procedures',
                        "total" => Input::get('compliance-rating'),
                        "score" => Input::get('compliance-score')
                    )
            );

        DB::table("pmf_employee_discipline")->insert( array("pmf_employee_id"=>$id ,
                        "demerit" => Input::get('discipline-demerit'),
                        "score" => Input::get('discipline-score')  ) ); 

        DB::table("pmf_employee_policies_and_procedures")->insert( array("pmf_employee_id"=>$id ,
                        "rating" => Input::get('compliance-rating'),
                        "score" => Input::get('compliance-score')  ) );  

        DB::table("pmf_employee_totals")->insert( $emp_totals ); 

        $cluster = Input::get("cluster");
        $kra = Input::get("kra");
        $kpi = Input::get("kpi");
        $kpi_index = Input::get("kpi_index");
        $kra_id = 1;
        $clus = "";
        $x = 1;

        if(!empty($cluster)):
        foreach ($cluster as $key => $value) 
        {
            $kra_id = $clus == $value ?  $kra_id + 1 : 1;
            $clus = $value;
      
            $quality = Input::get("kpi_quality_" . $kpi[$key]  );
            $efficient = Input::get("kpi_efficient_" . $kpi[$key]  );
            $quantity = Input::get("kpi_quantity_" . $kpi[$key]  );
            $timeliness = Input::get("kpi_timeliness_" . $kpi[$key]  );

            $specific = Input::get("kpi_specific_" . $kpi[$key]  );
            $target = Input::get("kpi_target_" . $kpi[$key]  );
            $other = Input::get("kpi_other_" . $kpi[$key]  );
            $weight = Input::get("kpi_weight_" . $kpi[$key]  );
            $self = Input::get("kpi_self_" . $kpi[$key]  );
            $superior = Input::get("kpi_superior_" . $kpi[$key]  );
            $weighted = Input::get("kpi_weighted_" . $kpi[$key]  );
            $attach = Input::get("files_" . $kpi[$key]  );


            $db_kras[] = array(
                    "pmf_employee_id" => $id,
                    "cluster_id" => $value,
                    "kra_id" => $kra_id ,
                    "kra" => $kra[$key]
                );

            $x++;
            $kpi_id = 1;
            if(!empty($specific)):
            foreach ($specific as $i => $e) 
            {            
               $general = array();

               if(isset( $quality[$i]) ) $general[] = "QL";
               if(isset( $efficient[$i]) ) $general[] = "CE";
               if(isset( $quantity[$i]) ) $general[] = "QT";
               if(isset( $timeliness[$i]) ) $general[] = "T";



               if(!empty($attach[$i])):
                    $fm = new Libraries\FileManager;

                    foreach($attach[$i] as $file)
                    {
                        $fm->move($file['random_filename'],Config::get('rgas.rgas_storage_path'). 'pmf/' . $reference , Config::get('rgas.rgas_storage_path'). 'pmf/' . Input::get('old_reference_no') );
                    }

                     $attachments = json_encode($attach[$i]);
                else:
                     $attachments = "";
                endif;

             
               $db_kpi[] = array(
                    "pmf_employee_id" => $id,
                    "cluster_id" => $value,
                    "kra_id" => $kra_id ,
                    "kpi_id" => $kpi_id++,
                    "general_measures" => implode(",",$general) ,
                    "specific_measures" => $specific[$i] ,
                    "actual_performances" => $other[$i]  ,
                    "weights" =>$weight[$i] ,
                    "self_rating" => $self[$i] ,
                    "rating" => isset($superior[$i])  ? $superior[$i] : ""  ,
                    "weighted" =>$weighted[$i] ,
                    "achieved_actual_performances" => (isset($target[$i]) ? $target[$i] : null) ,
                    "attachments" => $attachments   
                );
            }
            endif;
        }
        endif;

        if(!empty($db_kras )) DB::table("pmf_employee_kras")->insert( $db_kras ); 
        if(!empty($db_kpi )) DB::table("pmf_employee_kpis")->insert( $db_kpi ); 


        $core = Input::get("core-rate");
        $incident = Input::get("incident");
        $core_values = array();

        if(!empty($core)):
        foreach ($core as $i => $e) 
        {
            $db_cores[] = array(
                    "pmf_employee_id" => $id,
                    "core_id" => $i,
                    "critical_incident" => (isset($incident[$i]) ? $incident[$i] : "") ,
                    "rating" => $e
                );

            //ADD THIS TO CHECK IF CORE RATING HAS VALUE 
             $core_values[] = $i;
        }
        endif;

        if(!empty($incident)):
        foreach ($incident as $i => $e) 
        {
            if(!in_array($i, $core_values)):
                $db_cores[] = array(
                        "pmf_employee_id" => $id,
                        "core_id" => $i,
                        "critical_incident" => $e ,
                        "rating" => 999 //USE 999 TO AVOID CONFLICT IF NO VALUE IS DEFINED.
                    );
            endif;
        }

        DB::table("pmf_employee_core_values")->insert( $db_cores ); 
        endif;


        $reccomendation = Input::get("reccomendation");

        if(!empty($reccomendation)):
        foreach ($reccomendation as $i => $e) 
        {
            $db_rec[] = array(
                    "pmf_employee_id" => $id,
                    "recommendation_id" => $e,
                    "recommendation" => Input::get("recomend_$e") ,
                    "approved" => Input::get("check_$e")
                );
        }


        DB::table("pmf_employee_recommendations")->insert( $db_rec ); 
        endif;


        $comments = array(
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Input::get('employee_comment_id') ,
                        "designation" => "employee",
                        "comment" => Input::get('employee_comment') ,
                        "ts_updated" => date("Y-m-d H:i:s")                        
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Input::get('supervisor_comment_id') ,
                        "designation" => "supervisor",
                        "comment" => Input::get('supervisor_comment') ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Session::get('employee_id'),
                        "designation" =>  "general" ,
                        "comment" => Input::get('message') .($status == 1 ?  ucwords(strtolower(Session::get("firstname")." ".Session::get("lastname") )) . " " . date("m/d/Y h:i A: ") . trim(Input::get('comment')) .   "\n" : ""),
                        "ts_updated" => date("Y-m-d H:i:s")
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Session::get('employee_id'),
                        "designation" =>($status == 1 ? "" : "saved"),
                        "comment" => Input::get('comment')  ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Input::get('head_comment_id') ,
                        "designation" => "head",
                        "comment" => Input::get('head_comment') ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    ),
                array(
                        "pmf_employee_id" =>  $id,
                        "employee_id" => Input::get('mngt_comment_id') ,
                        "designation" => "top-mngt",
                        "comment" => Input::get('mngt_comment') ,
                        "ts_updated" => date("Y-m-d H:i:s")
                    ),
            );

        DB::table("pmf_employee_comments")->insert(  $comments ); 



    }

    public static function purpose()
    {
            $rec = DB::table("pmf_purposes")
                ->orderby("pmf_id")
                ->get(["pmf_id" , "purpose_id","purpose"]);
            
            return $rec;
    }

    public static function period()
    {
            $rec = DB::table("pmf")
                ->where("status", 1)
                ->orderby("period")
                ->get(["period" , "pmf_id","year"]);
            
            return $rec;
    }

     public static function cover($pmf)
    {
            $rec = DB::table("pmf")
                ->where("pmf_id", $pmf)
                ->orderby("period")
                ->get(["period" , "pmf_id","year"]);
            
            return $rec[0]->year . " (" . $rec[0]->period . ")";
    }

     public static function cluster($pmf)
    {
            $rec = DB::table("pmf_kra_clusters")
                ->where("pmf_id", $pmf)
                ->orderby("cluster")
                ->get(["cluster_id" , "description","cluster"]);
            
            return $rec;
    }

     public static function reccomendation($pmf)
    {
            $rec = DB::table("pmf_recommendations")
                ->where("pmf_id", $pmf)
                ->orderby("recommendation_id")
                ->get(["recommendation_id" , "recommendation","type"]);
            
            return $rec;
    }

    public static function behavior($pmf)
    {
            $rec = DB::table("pmf_core_values")
                ->where("pmf_id", $pmf)
                ->orderby("parent_core_id")
                ->get(["core_id" , "parent_core_id","value"]);

            $inc = DB::table("pmf_core_incidents")
                ->where("pmf_id", $pmf)
                ->orderby("core_id")
                ->get(["core_id" ,"value"]);

            $arr = array();
            foreach($inc as $rs)
            {
                $arr["inc"][$rs->core_id] = $rs->value;
            }
            
            
            foreach($rec as $rs)
            {
            	if($rs->parent_core_id == 0)
            	{
            		$arr["parent"][$rs->core_id] = $rs->value;
            	}
            	else
            	{
            		$arr["detail"][$rs->parent_core_id][$rs->core_id] = $rs->value;
            	}
            }

            return $arr;
    }

    public static function build($pmf)
    {
            $rec = DB::table("pmf_weights")
                ->where("pmf_id", $pmf)
                ->get(["component" ,"weight","method","comments"]);
            
            $arr = array();
            foreach($rec as $rs)
            {
                $arr[ $rs->component ][ "weight" ] = $rs->weight;
                $arr[ $rs->component ][ "comments" ] = $rs->comments;
                $arr[ $rs->component ][ "component" ] = $rs->component;
                $arr[ $rs->method ][ "weight" ] = $rs->weight;
                $arr[ $rs->method ][ "comments" ] = $rs->comments;
                $arr[ $rs->method ][ "component" ] = $rs->component;
            }

            return $arr;
    }


    public static function assign()
    {
            $rec = DB::table("pmf_users")
                ->join("employees","employees.id","=","pmf_users.employee_id")
              //  ->where("lvl", 1)
                ->where("status", 1)
                ->get(["firstname" ,"lastname","pmf_users.employee_id"]);
            
            $arr = array();
            foreach($rec as $rs)
            {
                $arr[ $rs->employee_id ] = $rs->firstname . " " . $rs->lastname;
            }

            return $arr;
    }

     public static function get_reference_no()
    {
        $year = date("Y");
        $ref = Performance::whereRaw("SUBSTR(reference_no, 1, 4) = ?" , [$year])
            ->orderby("reference_no","desc")
            ->limit(1)
            ->get(["reference_no"])->toArray();

       $no = isset($ref[0]["reference_no"]) ? $ref[0]["reference_no"] : "$year-00000";   
       $num = sprintf("%'.05d", (int)substr($no,5) + 1);

       return "$year-$num";
    }

}