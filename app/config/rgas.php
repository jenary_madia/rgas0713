<?php

return array(
	
	'rgas_storage_path' => storage_path() . "/rgas/",

	'rgas_temp_storage_path' => storage_path() . "/rgas/tmp_files_path/",
	
	'module_id_te' => 13,
	
	'module_id_cbr' => 14,

	'module_id_cc' => 15,

	'module_id_gd' => 10,

	'module_id_oam' => 16,

	'module_id_art' => 7,

	'module_id_os' => 8,
	
	'module_id_sv' => 9,
	
	'module_id_pmarf' => 17,
	
	'module_id_pmf' => 6,
);
