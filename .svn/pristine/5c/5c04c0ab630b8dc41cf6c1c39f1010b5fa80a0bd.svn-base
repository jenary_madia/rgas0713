<?php $__env->startSection('content'); ?>
<div id="moc_create" v-cloak>
    <?php /*FOR for message prompt*/ ?>
    <div class="alert alert-success msr-form-container" v-if="submitStatus.submitted && submitStatus.success">
        <p>{{ submitStatus.message }}</p>
    </div>
    <div id="global_message" class="alert alert-danger msr-form-container" v-if="submitStatus.submitted && ! submitStatus.success">
        <ul>
            <li v-for="error in submitStatus.errors">{{ error }}</li>
        </ul>
    </div>
    <?php /*END for message prompt*/ ?>
    <?php echo Form::open(['url' => 'msr/create', 'method' => 'post', 'files' => true,'class' => 'form-horizontal']); ?>

    <div class="form_container msr-form-container">
        <div class="container-header"><h5 class="text-center"><strong><?php echo MOC_FORM_TITLE; ?></strong></h5></div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="row employee-details">
            <div class="col-md-6">
                <label class="labels required pull-left">EMPLOYEE NAME:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="employeeName" value="<?php echo Session::get('employee_name'); ?>" />
            </div>
            <div class="col-md-6 pull-left">
                <label class="labels required">REFERENCE NUMBER:</label>
                <input readonly="readonly" type="text" class="form-control pull-right"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">EMPLOYEE NUMBER:</label>
                <input  readonly="readonly" type="text" class="form-control pull-right" name="employeeNumber" value="<?php echo Session::get('employeeid'); ?>" />
            </div>
            <div class="col-md-6">
                <label class="labels required pull-left">DATE FILED:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" value="<?php echo date('Y-m-d'); ?>"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">COMPANY:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="company" value="<?php echo Session::get('company'); ?>" />
            </div>
            <div class="col-md-6">
                <label class="labels required pull-left">STATUS:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="status" value="NEW" />
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels required pull-left">DEPARTMENT:</label>
                <input readonly="readonly" type="text" class="form-control pull-right" name="department" value="<?php echo Session::get('dept_name'); ?>" />
            </div>
            <div class="col-md-6">
                <label class="labels">CONTACT NUMBER:</label>
                <input v-model="formDetails.contactNumber"  type="text" class="form-control pull-right" name="contactNumber" maxlength="20" value="<?php echo Input::old('contactNumber'); ?>"/>
            </div>
            <div class="clear_10"></div>
            <div class="col-md-6">
                <label class="labels pull-left">SECTION:</label>
                <input v-model="formDetails.section"  type="text" class="form-control pull-right" name="section" maxlength="25" value="<?php echo Input::old('section', Session::get('sect_name')); ?>" />
            </div>
        </div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="container-header"><h5 class="text-center lined"><strong>PSR/MOC DETAILS</strong></h5></div>
        <div class="clear_20"></div>
        <div class="clear_20"></div>
        <div class="row project-requirements">
            <div class="col-md-12">
                <div class="moc-sidebar">
                    <label class="labels required">TYPE OF REQUEST:</label>
                    <br>
                    <ul class="moc-list list-unstyled">
                        <?php foreach($requestTypes as $requestType): ?>
                            <li class="text-uppercase" style="position: relative">
                                <input type="checkbox" v-model="mocDetails.requestTypes" value="<?php echo $requestType->item; ?>"> <span><?php echo $requestType->text; ?></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <label class="labels required pull-left">URGENCY:</label>
                    <select v-model="mocDetails.urgency" class="form-control">
                        <option value="immediate">IMMEDIATE ATTENTION NEEDED</option>
                        <option value="normal">NORMAL PRIORITY</option>
                    </select>
                </div>
                <div class="moc-main">
                    <label class="labels pull-left">FOR REQUEST ON <span>PROCESS DESIGN/IMPROVEMENT</span> AND <span>REVIEW OF CHANGE</span></label>
                    <div class="moc-inner-sidebar">
                        <label v-if="! enabledForRequest" class="labels">CHANGE IN:</label>
                        <label v-else class="labels required">CHANGE IN:</label>
                        <br>
                        <ul class="list-unstyled moc-list">
                            <?php foreach($changeIn as $change): ?>
                                <?php if($change->item == "others"): ?>
                                    <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox"  v-model="mocDetails.changeIn" value="<?php echo $change->item; ?>"> <?php echo $change->text; ?> <input :disabled="! enabledForRequest || mocDetails.changeIn.indexOf('others') == -1" style="width : 140px; text-transform: none !important;" v-model="mocDetails.changeInOthersInput" type="text" class="pull-right form-control"></li>
                                <?php else: ?>
                                    <li class="text-uppercase"><input :disabled="! enabledForRequest" type="checkbox" v-model="mocDetails.changeIn" value="<?php echo $change->item; ?>"> <?php echo $change->text; ?></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="moc-inner-main">
                        <div class="moc-inner-sidebar">
                            <label v-if="! enabledForRequest" class="labels pull-left">CHANGE TYPE:</label>
                            <label v-else class="labels pull-left required">CHANGE TYPE:</label>
                            <select v-model="mocDetails.changeType" class="form-control" :disabled="! enabledForRequest">
                                <option value="permanent">PERMANENT</option>
                                <option value="temporary">TEMPORARY</option>
                            </select>
                        </div>
                        <div class="moc-inner-main">
                            <label v-if="! enabledForRequest" class="labels pull-right">TARGET IMPLEMENTATION DATE:</label>
                            <label v-else class="labels pull-right required">TARGET IMPLEMENTATION DATE:</label>
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input :disabled="! enabledForRequest" v-model="mocDetails.targetDate" type="text" name="" id="UTdate" class="date_picker form-control input-small">
                                <span v-if="! enabledForRequest" class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                <span v-else class="input-group-addon toggleDatePicker"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <label v-if="! enabledForRequest" style="margin-top: 10px" class="labels pull-left">DEPARTMENTS INVOLVED:</label>
                        <label v-else style="margin-top: 10px" class="labels pull-left required">DEPARTMENTS INVOLVED:</label>
                        <div class="clearfix"></div>
                        <div class="moc-inner-main">
                            <select :disabled="! enabledForRequest" class="form-control" v-model="selectedDepartment.id" name="" id="">
                                <option v-for="department in departments" value="{{ $index }}">{{ department.comp_code }} - {{ department.dept_name }}</option>
                            </select>
                        </div>
                        <div class="moc-inner-sidebar">
                            <button :disabled="! enabledForRequest  || ! selectedDepartment.id" class="btn btn-default btndefault pull-right" @click.prevent="addDeptInvolved">ADD</button>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                        <div class="moc-whole" style="border : 1px solid #ccc;">
                            <ol class="moc-list">
                                <li v-for="deptInvolved in mocDetails.departmentsInvolved">{{ deptInvolved.comp_code }} - {{ deptInvolved.dept_name }}<a @click="removeDeptInvolved($index)"> Remove</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <label class="labels required pull-left" style="padding-top: 7px">TITLE/SUBJECT:</label>
                <input v-model="mocDetails.title" style="width : 90%;" type="text" class="form-control pull-right" name="employeeName"/>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-md-12">
                <label class="labels required pull-left">BACKGROUND INFORMATION &nbsp</label><label class="labels pull-left"> (REASON FOR CHANGE/EFFECTS ON CURRENT OPERATIONS/DESIRED RESULTS) : </label>
                <textarea v-model="mocDetails.backgroundInfo" name="" id="" class="form-control" rows="7"></textarea>
            </div>
            <?php if(Session::get('is_ssmd') && Session::get('desig_level') == 'head' ): ?>
                <div class="col-md-12">
                    <label class="labels pull-left" style="padding-top: 7px">SSMD ASSESSMENT/ACTION PLAN:</label>
                    <textarea name="" id="" class="form-control" rows="7" v-model="mocDetails.ssmdActionPlan">{{ mocDetails.ssmd_action_plan }}</textarea>
                </div>
                <div class="clearfix"></div>
                <br>
            <?php endif; ?>
        </div>
        <div class="clear_20"></div>
        <div>
            <div class="container-header"><h5 class="text-center lined"><strong>ATTACHMENTS</strong></h5></div>
            <div class="clear_20"></div>
            <div class="row">
                <div class="col-md-4">
                    <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                    <div id="attachments">
                        <?php /*<?php if(Input::old("files")): ?>*/ ?>
                            <?php /*<?php for($i = 1; $i <= count(Input::old("files")); $i++): ?>*/ ?>
                                <?php /*<p>*/ ?>
                                    <?php /*<?php echo Input::old("files")[$i]["original_filename"]; ?> | <?php echo Input::old("files")[$i]["filesize"]; ?>*/ ?>
                                    <?php /*<input type='hidden' name='files[<?php echo $i; ?>][random_filename]' value='<?php echo Input::old("files")[$i]["random_filename"]; ?>'>*/ ?>
                                    <?php /*<input type='hidden' name='files[<?php echo $i; ?>][original_filename]' value='<?php echo Input::old("files")[$i]["original_filename"]; ?>'>*/ ?>
                                    <?php /*<input type='hidden' class='attachment_filesize' name='files[<?php echo $i; ?>][filesize]' value='<?php echo Input::old("files")[$i]["filesize"]; ?>'>*/ ?>
                                    <?php /*<input type='hidden' name='files[<?php echo $i; ?>][mime_type]' value='<?php echo Input::old("files")[$i]["mime_type"]; ?>'>*/ ?>
                                    <?php /*<input type='hidden' name='files[<?php echo $i; ?>][original_extension]' value='<?php echo Input::old("files")[$i]["original_extension"]; ?>'>*/ ?>
                                    <?php /*<button class='btn btn-xs btn-danger remove-fn confirm-delete'>DELETE</button>*/ ?>
                                <?php /*</p>*/ ?>
                            <?php /*<?php endfor; ?>*/ ?>
                        <?php /*<?php endif; ?>*/ ?>
                    </div>
                    <span class="btn btn-success btnbrowse fileinput-button">
                        <span>BROWSE</span>
                        <input id="fileupload" type="file" name="attachments[]" data-url="<?php echo route('file-uploader.store'); ?>" multiple>
                    </span>
                </div>
                <div class="col-md-4">
                    <?php if((Session::get('is_ssmd') && Session::get('desig_level') == 'head')): ?>
                        <label class="attachment_note"><strong>SSMD : </strong></label><br/>
                        <label class="attachment_note"><strong>ADD ATTACHMENT/S </strong><i>(MAXIMUM OF 10 ATTACHMENTS)</i></label><br/>
                        <div id="SSMDattachments">
                        </div>
                        <span class="btn btn-success btnbrowse fileinput-button">
                            <span>BROWSE</span>
                            <input id="SSMDfileupload" type="file" name="attachments[]" data-url="<?php echo route('file-uploader.store'); ?>" multiple>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div><!-- end of form_container -->
    <div class="clear_20"></div>

    <span class="action-label labels">ACTION</span>
    <div class="form_container msr-form-container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 comment-box">
                <label class="labels pull-left">COMMENT:</label>
                <textarea rows="3" class="form-control pull-left" name="comment" v-model="comment"></textarea>
            </div>
            <?php if(Session::get('company')== 'RBC-CORP'): ?>
                <?php echo $__env->make('moc.actions.create.corp', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php else: ?>
                <?php echo $__env->make('moc.actions.create.sat', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <?php endif; ?>
        </div>
    </div><!-- end of form_container -->
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('js_ko'); ?>
<?php echo HTML::script('/assets/js/notification/vue.js'); ?>

<?php echo HTML::script('/assets/js/notification/vue-validator.min.js'); ?>

<?php echo HTML::script('/assets/js/notification/vue-resource.min.js'); ?>

<?php echo HTML::script('/assets/js/moc/vue-create.js'); ?>

<script>
    $('body').on('click','.remove-fn',function () {
        $(this).closest( "p" ).remove();
    });
    <?php /*<?php if(Input::old("files")): ?>*/ ?>
        <?php /*var file_counter = <?php echo $i; ?>;*/ ?>
    <?php /*<?php else: ?>*/ ?>
    <?php /*<?php endif; ?>*/ ?>
    var file_counter = 0;
    var allowed_file_count = 10;
    var allowed_total_filesize = 20971520;

    var SSMDfile_counter = 0;
    var SSMDallowed_file_count = 10;
    var SSMDallowed_total_filesize = 20971520;

    $('.date_picker').datepicker({
        dateFormat : 'yy-mm-dd'
    });
    $('body').on('click','.toggleDatePicker',function () {
        $(this).closest( "div" ).find(".date_picker").datepicker("show");
    });
</script>
<?php echo HTML::script('/assets/js/moc/file-upload.js'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('template/header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>